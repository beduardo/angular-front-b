let jsdom = require("jsdom");
let fs = require('fs');
let copydir = require('copy-dir');

let { JSDOM } = jsdom;

let rootPath = __dirname;
let distPath = rootPath + '/dist';

let appsConfig = [
  {
    name: 'portal-client',
    route: '',
    root: true
  },
  {
    name: 'fast-trade',
    route: 'fast-trade',
  }
];

let load = () => {
  let apps = [];
  let changeAppFile = 'change-app.js';
  let changeAppPath = distPath + '/' + changeAppFile;

  // LER ARQUIVO
  if (appsConfig.length > 0) {
    for(let appConfig of appsConfig) {
      try {

        let indexPath = distPath + '/' + (!appConfig.root ? (appConfig.name + '/') : '') + 'index.html';

        //COPY BUILD TO DIST
        if (!appConfig.root) {
          let origin = rootPath + '/transition-apps/' + appConfig.name;
          let destiny = distPath + '/' + appConfig.name;

          copydir.sync(origin, destiny, null, (err) => {
            if (err) console.log(err);
          });

          replaceFileContent(indexPath, changeAppFile, changeAppFile);
        }

        let file = fs.readFileSync(indexPath);

        let document = new JSDOM(file.toString()).window.document;

        let app = {
          name: appConfig.name,
          route: appConfig.route,
          baseHref: !appConfig.route ? '' : ('/'  + appConfig.route + '/'),
          selector: null,
          styles: [],
          scripts: []
        };

        if (document) {
          addInApp(app, document.head.children, ['../' + changeAppFile]);
          addInApp(app, document.body.children);
          app.selector = getSelectorApp(document.body.children);

          apps.push(app);
        }
        else
          console.log('ERROR: There was an error reading the file: ' + indexPath);
      }
      catch(ex) { console.log('ERROR in application "' + appConfig.name + '": ' + ex); }
    }
  }

  // SALVAR ARQUIVO
  try {
    fs.writeFileSync(changeAppPath, getChangeAppScript(apps));
    console.log('The file "' + changeAppFile + '" was maked!');
  }
  catch(ex) { console.log('' + ex); }
}

let isException = (exceptions, text) => {
  return exceptions && exceptions.filter(x => x === text).length > 0;
}

let getSelectorApp = (elements) => {
  for (el of elements)
    if (el.localName.indexOf('app-') >= 0)
      return el.localName;
  return '';
}

let addInApp = (app, elements, exceptions) => {
  for (el of elements) {
    if (el.localName === 'link' && el.rel === 'stylesheet') {
      if (!isException(exceptions, el.src))
        app.styles.push(el.href.replace(app.name + '/', ''));
    }
    else if (el.localName === 'script' && el.src) {
      if (!isException(exceptions, el.src))
        app.scripts.push(el.src.replace(app.name + '/', ''));
    }
  }
}

let replaceFileContent = (filePath, oldValue, newValue) => {
  try {
    let file = fs.readFileSync(filePath);

    let newContent = file.toString().replace(new RegExp(oldValue, 'g'), newValue);

    fs.writeFileSync(filePath, newContent, 'utf8');
  }
  catch(ex) { console.log('ERROR: ' + ex); }
}

let makeRandom = () => {
  var text = "";
  var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

  for (var i = 0; i < 5; i++)
    text += possible.charAt(Math.floor(Math.random() * possible.length));

  return text;
}

let getChangeAppScript = (apps) => {
  let objStr = apps.map(app => JSON.stringify(app)).join(',\n\t');

  return `
var apps = [
  ${objStr}
];

var path = document.location.pathname.replace(/\\\//g, '');

var loadingEl;
var styleEl;

getCurrentAppElement = function() {
  var elements = document.body.children;
  for(var i = 0; i < elements.length; i++)
      if (elements[i].localName.indexOf('app-') >= 0)
          return elements[i];

  return null;
}

var currentAppElement;

var loadApp = function(app) {
  if (!currentAppElement) currentAppElement = getCurrentAppElement();

  //GENERATE IMAGE
  var scripts = app.scripts;
  var styles = app.styles;

  var mainScript = null;

  var loadScripts = function (scripts) {
    if (scripts && scripts.length > 0) {
      for(var i = 0; i < scripts.length; i++) {
        var src = scripts[i];

        if (app.nameAsPrefix && src.indexOf('http') !== 0)
          src = app.name + '/' + src;

        var script = document.createElement('script');
        script.type = 'text/javascript';
        script.src = src;

        document.head.appendChild(script);

        if (!mainScript && scripts[i] && scripts[i].indexOf('main.') >= 0)
          mainScript = script;
      }
    }
  }

  var loadStyles = function (styles) {
    if (styles && styles.length > 0) {
      for(var i = 0; i < styles.length; i++) {
        var href = styles[i];

        if (app.nameAsPrefix && href.indexOf('http') !== 0)
          href = app.name + '/' + href;

        var link = document.createElement('link');
        link.rel = 'stylesheet';
        link.href = href;

        document.head.appendChild(link);
      }
    }
  }

  var changeBaseHref = function() {
    var baseEl = null;

    if (getCurrentElements(document.head, 'base').length > 0)
      baseEl = getCurrentElements(document.head, 'base')[0];
    else {
      baseEl = document.createElement('base');
      document.head.appendChild(baseEl);
    }

    baseEl.href = app.baseHref;
  }

  var fadeOutOnElement = function(el) {
    showChangeAppLoading();
    if (el) {
      el.style.zIndex = '999999';
      el.style.position = 'absolute';
      el.style.top = 0;
      el.style.left = 0;
      el.style.bottom = 0;
      el.style.right = 0;

      el.className = "change-app-fadeOut";
    }
  }

  // INCLUDE COMPONENT
  var appElement = document.createElement(app.selector);
  appElement.style.display = 'none';
  document.body.appendChild(appElement);

  // CHANGE BASE-HREF
  changeBaseHref();

  // REMOVE SCRIPTS OF CURRENT APP
  var elementsRemove = getCurrentElements(document.head, 'scripts');
  elementsRemove = elementsRemove.concat(getCurrentElements(document.body, 'scripts'));
  elementsRemove = elementsRemove.concat(getCurrentElements(document.head, 'styles'));

  // REMOVE ELEMENT AND STYLES OF CURRENT APP
  fadeOutOnElement(currentAppElement);
  setTimeout(function() {
    removeElements(elementsRemove);
    if (currentAppElement) {
      currentAppElement.remove();
      currentAppElement = null;
    }
  }, 500);

  //LOAD SCRIPTS APP
  loadScripts(scripts);

  //LOAD STYLES APP
  loadStyles(styles);

  if (mainScript)
    mainScript.onload = function() {
      appElement.style.display = 'initial';
      setTimeout(function() { hiddenChangeAppLoading(); }, 500);
    };
};

var removeElements = function(elements) {
  elements.forEach(function(el){ el.remove(); });
}

var getCurrentElements = function(parent, type) {
  var removeList = [];
  for(var i = 0; i < parent.children.length; i++) {
    var el = parent.children[i];

    if (type === 'styles') {
      if ((el.nodeName === 'LINK' || el.nodeName === 'STYLE') && el.id.indexOf('change-app') < 0)
        removeList.push(el)
    }
    else if (type === 'scripts') {
      if (el.nodeName === 'SCRIPT' && el.src.indexOf('change-app.js') < 0)
        removeList.push(el);
    }
    else if (type === el.localName)
      removeList.push(el);
  };
  return removeList;
}

var createChangeAppStyle = function() {
  styleEl = document.createElement('style');
  styleEl.setAttribute('id', 'change-app-style');
  styleEl.innerHTML = ''+
    '@keyframes changeAppSpin {'+
      '0% { transform: rotate(0deg); }' +
      '40% { transform: rotate(120deg); }' +
      '80% { transform: rotate(180deg); }' +
      '90% { transform: rotate(230deg); }' +
      '100% { transform: rotate(360deg); }' +
    '}'+
    '@keyframes changeAppSpin2 {'+
      'to { transform: rotate(360deg); }' +
    '}'+
    '@-webkit-keyframes changeAppfadeIn {' +
      'from { opacity: 0; }' +
      'to { opacity: 1; }' +
    '}' +
    '@keyframes changeAppfadeIn {' +
      'from { opacity: 0; }' +
      'to { opacity: 1; }' +
    '}' +
    '@-webkit-keyframes changeAppfadeOut {' +
      'from { opacity: 1; }' +
      'to { opacity: 0; }' +
    '}' +
    '@keyframes changeAppfadeOut {' +
      'from { opacity: 1; }' +
      'to { opacity: 0; }' +
    '}' +
    '.change-app-fadeIn {' +
      '-webkit-animation-duration: .5s;' +
      'animation-duration: .5s;' +
      '-webkit-animation-fill-mode: both;' +
      'animation-fill-mode: both;' +
      '-webkit-animation-name: changeAppfadeIn;' +
      'animation-name: changeAppfadeIn;' +
    '}' +
    '.change-app-fadeOut {' +
      '-webkit-animation-duration: .5s;' +
      'animation-duration: .5s;' +
      '-webkit-animation-fill-mode: both;' +
      'animation-fill-mode: both;' +
      '-webkit-animation-name: changeAppfadeOut;' +
      'animation-name: changeAppfadeOut;' +
    '}' +
    '.change-app-loading {'+
      'z-index: 999999;' +
      'position: fixed;'+
      'left: 0;'+
      'top: 0;'+
      'right: 0;'+
      'bottom: 0;'+
      'background-color: #FFF;'+
    '}'+
    '.change-app-loading > .change-app-spinner {'+
      'position: absolute;'+
      'left: calc(50% - 30px);'+
      'top: calc(50% - 30px);'+
      'animation: changeAppSpin2 5s linear infinite;'+
    '}'+
    '.change-app-loading > .change-app-spinner > div {'+
      'border: 8px solid rgba(0, 0, 0, 0.1);'+
      'border-left-color: #555;'+
      'border-radius: 50%;'+
      'width: 60px;'+
      'height: 60px;'+
      'box-sizing: border-box;' +
      'animation: changeAppSpin 1.2s linear infinite;'+
    '}'+
  '';

  document.head.appendChild(styleEl);
}

var showChangeAppLoading = function() {
  loadingEl = document.createElement('div');
  loadingEl.setAttribute('id', 'change-app-element');
  loadingEl.setAttribute('class', 'change-app-loading change-app-fadeIn');
  loadingEl.innerHTML = '' +
  '<div class="change-app-spinner">' +
    '<div></div>'+
  '</div>';

  document.body.appendChild(loadingEl);
  document.body.style.overflow = 'hidden';
}

var hiddenChangeAppLoading = function() {
  loadingEl.setAttribute('class', 'change-app-loading change-app-fadeOut');
  document.body.style.overflow = 'initial';
  setTimeout(function() { loadingEl.remove(); }, 500);
}

var goPath = function(path, el) {
  if (path || path === '') {
    if (path === '')
      path = '/';
    else {
      if (path.indexOf('/') === 0) path = path.replace('/', '');
      if (path.indexOf('/') !== (path.length-1)) path += '/';

      path = '/' + path;
    }

    if (window.location.pathname === path) return;

    history.pushState(null, document.title, path);

    if (el) {
      if (el.id === 'fast-trade') {
        el.style.display = 'none';
        document.getElementById('portal-client').style.display = 'block';
      }
      if (el.id === 'portal-client') {
        el.style.display = 'none';
        document.getElementById('fast-trade').style.display = 'block';
      }
    }
  }
};

var checkUrl = function() {
  var _path = document.location.pathname.replace(/\\//g, '');

  if (_path !== path) {
    path = _path;

    apps.forEach((app) => {
      if (path == app.route) {
        localStorage.setItem('LOCAL_TRANSITION', true);
        loadApp(app);
        return;
      }
    });
  }

  setTimeout(checkUrl, 100);
};

createChangeAppStyle();
setTimeout(checkUrl, 100);`;
}

load();
