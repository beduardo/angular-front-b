"use strict";
/*
	This class implements interaction with UDF-compatible datafeed.

	See UDF protocol reference at
	https://github.com/tradingview/charting_library/wiki/UDF
*/

function parseJSONorNot(mayBeJSON) {
  if (typeof mayBeJSON === 'string') {
    return JSON.parse(mayBeJSON);
  } else {
    return mayBeJSON;
  }
}

function getCookie(name) {
  var valueTemp = window.sessionStorage.getItem(name);
  // var value = "; " + document.cookie;
  // var parts = value.split("; " + name + "=");
  // if (parts.length == 2) {
  if (valueTemp) {
    return valueTemp;
    // return parts.pop().split(";").shift();
  } else {
    return null;
  }
}

function valideCookie() {
  if (getCookie(TKN_API) !== null) {
    return true;
  } else {
    return false;
  }
}

function isChart() {
  var url = window.location.href;
  var name = "chart";
  if (url && url.indexOf(name) > 0) {
    return true;
  }
  return false;
}

function getCurrentInterval(resolution, daysInMonth) {
  var currInterval = 0;
  switch (resolution) {
    case '1': // 1 minute
      currInterval = 60;
      break;
    case '5': // 5 minutes
      currInterval = 300;
      break;
    case '15': // 5 minutes
      currInterval = 900;
      break;
    case '30': // 30 minutes
      currInterval = 1800;
      break;
    case '45': // 45 minutes
      currInterval = 2700;
      break;
    case '60': // 1 hour
      currInterval = 3600;
      break;
    case '300': // 5 hour
      currInterval = 18000;
      break;
    case 'D': // 1 day
      currInterval = 86400;
      break;
    case 'W': // 1 week
      currInterval = 86400 * 7;
      break;
    case 'M': // 1 month
      currInterval = daysInMonth * 86400;
      break;
  }
  return currInterval;
}

function getCandleMin(last_trade, identifier) {
  // if (open < candleMin[identifier]) {
  //     candleMin[identifier] = open;
  // }
  if (last_trade < candleMin[identifier]) {
    candleMin[identifier] = last_trade;
  }
  return candleMin[identifier];
}

function getCandleMax(last_trade, identifier) {
  // if (open > candleMax[identifier]) {
  //     candleMax[identifier] = open;
  // }
  if (last_trade > candleMax[identifier]) {
    candleMax[identifier] = last_trade;
  }
  return candleMax[identifier];
}


// var _webSocket;
var Datafeeds = {};
var interval;
var TKN_API = 'TKNAPI';
var TKN_WS = 'TKNWFCHT';
var LOGIN_TYPE = 'HB_LOGIN_TYPE_ENABLED';

var candleMin = {};
var candleMax = {};
var candleOpen = {};
var isNewCandle = false;
var fixedCandleTime;

Datafeeds.UDFCompatibleDatafeed = function (datafeedURL, webSocketURL, tokenWS, symbol, updateFrequency) {
  this.URL_WS = webSocketURL;
  if (getCookie(LOGIN_TYPE) !== 'LoginW') {
    this._datafeedURL = datafeedURL + '/services/chart';
  } else {
    this._datafeedURL = datafeedURL + '/services/TradingView';
  }
  this._configuration = undefined;

  this._webSocket = null;
  this._tokenWS = tokenWS;
  this._symbol = symbol;
  this._msgQuote = null;
  this._msgTimeUpdate = null;
  this._symbolSearch = null;
  this._symbolsStorage = null;
  this._barsPulseUpdater = new Datafeeds.DataPulseUpdater(this, updateFrequency || 300);
  this._quotesPulseUpdater = new Datafeeds.QuotesPulseUpdater(this);

  this._enableLogging = false;
  this._initializationFinished = false;
  this._callbacks = {};
  this._initialize();
};

Datafeeds.UDFCompatibleDatafeed.prototype.defaultConfiguration = function () {
  return {
    supports_search: false,
    supports_group_request: true,
    supported_resolutions: ["1", "5", "10", "15", "30", "60", "1D", "1W", "1M"],
    supports_marks: false,
    supports_timescale_marks: false
  };
};

Datafeeds.UDFCompatibleDatafeed.prototype.getServerTime = function (callback) {
  if (this._configuration.supports_time) {
    this._send(this._datafeedURL + "/time", {})
      .done(function (response) {
        var time = +response;
        if (!isNaN(time)) {
          callback(time);
        }
      })
      .fail(function () { });
  }
};

Datafeeds.UDFCompatibleDatafeed.prototype.on = function (event, callback) {
  if (!this._callbacks.hasOwnProperty(event)) {
    this._callbacks[event] = [];
  }

  this._callbacks[event].push(callback);
  return this;
};

Datafeeds.UDFCompatibleDatafeed.prototype._fireEvent = function (event, argument) {
  if (this._callbacks.hasOwnProperty(event)) {
    var callbacksChain = this._callbacks[event];
    for (var i = 0; i < callbacksChain.length; ++i) {
      callbacksChain[i](argument);
    }

    this._callbacks[event] = [];
  }
};

Datafeeds.UDFCompatibleDatafeed.prototype.onInitialized = function () {
  this._initializationFinished = true;
  this._fireEvent("initialized");
};

Datafeeds.UDFCompatibleDatafeed.prototype._logMessage = function (message) {
  if (this._enableLogging) {
    var now = new Date();
    // console.log(now.toLocaleTimeString() + "." + now.getMilliseconds() + "> " + message);
  }
};

Datafeeds.UDFCompatibleDatafeed.prototype._send = function (url, params) {
  var request = url;
  if (params) {
    for (var i = 0; i < Object.keys(params).length; ++i) {
      var key = Object.keys(params)[i];
      var value = encodeURIComponent(params[key]);
      request += (i === 0 ? "?" : "&") + key + "=" + value;
    }
  }
  this._logMessage("New request: " + request);

  if (getCookie(LOGIN_TYPE) !== 'LoginW') {
    // console.log("Authorization", "Bearer " + getCookie(TKN_API));
    return $.ajax({
      type: "GET",
      url: request,
      contentType: "text/plain",
      beforeSend: function (request) {
        request.setRequestHeader("Authorization", "Bearer " + getCookie(TKN_API));
      },
    });
  } else {
    // console.log("Authorization", "Bearer " + decodeURIComponent(getCookie(TKN_API)));
    return $.ajax({
      type: "GET",
      url: request,
      contentType: "text/plain",
      beforeSend: function (request) {
        request.setRequestHeader("Authorization", "Bearer " + decodeURIComponent(getCookie(TKN_API)));
      },
    });
  }
};

Datafeeds.UDFCompatibleDatafeed.prototype._SendMessageWS = function (symbol, type) {
  this._msgQuote = {
    timeUpdate: "",
    dateTrade: "",
    lastTrade: "",
    volumeAmount: "",
    volumeFinancier: ""
  };
  // console.log('\n<<<< _SendMessageWS >>>> - SYMBOL: ' + symbol + ' - TOKEN: ' + this._tokenWS + '\n');  
  this._webSocket.send('{"token":"' + this._tokenWS + '","module":"quotes","service":"quote","parameterGet":"' + symbol.toLowerCase() + '","parameters":{"subsbribetype":"' + type + '","filter":"0,1,2,9,10","delay":"250"}}');
  // 3,4,5,6,7,8,
  // ,11,12,13,14,15,16,17,18,19,20,21,36,37,38,39,40,41,42,43,44,45,46,47,48,49,54,57,58,59,64,65,66,67,68,69,70,71,72,82,83,84,85,86,88,89,94,95,97,98,99,100,101,102,103,104,105,107,108,110,112,116,118,121,123,134,135,10097,10098,10099
};

Datafeeds.UDFCompatibleDatafeed.prototype._CreateWS = function (callback) {
  var that = callback;
  // console.log('CONNECTING'); // : [ ' + this.URL_WS  + ' ]');
  this._webSocket = new WebSocket(this.URL_WS); //, 'echo-protocol');
  this._webSocket.onopen = function () {
    // console.log('OPEN_WS');
    that._SendMessageWS(that._symbol, "1");
  };

  this._webSocket.onmessage = function (evt) {
    // console.log("onMessage = " + evt.data);
    if (evt && evt.data && evt.data.split(' ')[2] !== 'inválido') {
      const data = JSON.parse(evt.data);
      if (data && data.type === 'QuoteType') {
        if (data.parameter && that._symbol && data.parameter.toLowerCase() === that._symbol.toLowerCase()) {
          // console.log(data);
          if (data.values[0] !== undefined) {
            that._msgQuote.timeUpdate = data.values[0];
          }
          if (data.values[1] !== undefined) {
            that._msgQuote.dateTrade = data.values[1];
          }
          if (data.values[2] !== undefined) {
            that._msgQuote.lastTrade = data.values[2];
          }
          if (data.values[9] !== undefined) {
            that._msgQuote.volumeAmount = data.values[9];
          }
          if (data.values[10] !== undefined) {
            that._msgQuote.volumeFinancier = data.values[10];
          }
          that._msgQuote.symbol = data.parameter;
          // console.log('WS->', that._msgQuote);
        }
      }
    }
  };

  this._webSocket.onclose = function () {
    // websocket is closed.
    console.log("CLOSE_WS.");
  };

};


Datafeeds.UDFCompatibleDatafeed.prototype._initialize = function () {
  var that = this;
  this._send(this._datafeedURL + "/config")
    .done(function (response) {
      // console.log(response);
      if (response == "Not Permit lastTime") {
        return;
      }
      if (response && JSON.parse(response)) {
        var configurationData = JSON.parse(response);
        // console.log(configurationData);
        that._setupWithConfiguration(configurationData);
      }
    })
    .fail(function (reason) {
      that._setupWithConfiguration(that.defaultConfiguration());
    });
};

Datafeeds.UDFCompatibleDatafeed.prototype.onReady = function (callback) {
  var that = this;
  this._CreateWS(that);
  if (this._configuration) {
    setTimeout(function () {
      callback(that._configuration);
    }, 0);
  } else {
    this.on("configuration_ready", function () {
      callback(that._configuration);
    });
  }
};

Datafeeds.UDFCompatibleDatafeed.prototype._setupWithConfiguration = function (
  configurationData
) {
  this._configuration = configurationData;
  if (!configurationData.exchanges) {
    configurationData.exchanges = [];
  }

  //	@obsolete; remove in 1.5
  var supportedResolutions =
    configurationData.supported_resolutions ||
    configurationData.supportedResolutions;
  configurationData.supported_resolutions = supportedResolutions;

  //	@obsolete; remove in 1.5
  var symbolsTypes =
    configurationData.symbols_types || configurationData.symbolsTypes;
  configurationData.symbols_types = symbolsTypes;

  if (
    !configurationData.supports_search &&
    !configurationData.supports_group_request
  ) {
    throw new Error(
      "Unsupported datafeed configuration. Must either support search, or support group request"
    );
  }

  if (!configurationData.supports_search) {
    this._symbolSearch = new Datafeeds.SymbolSearchComponent(this);
  }

  if (configurationData.supports_group_request) {
    //	this component will call onInitialized() by itself
    this._symbolsStorage = new Datafeeds.SymbolsStorage(this);
  } else {
    this.onInitialized();
  }

  this._fireEvent("configuration_ready");
  this._logMessage("Initialized with " + JSON.stringify(configurationData));
};

//	===============================================================================================================================
//	The functions set below is the implementation of JavaScript API.

Datafeeds.UDFCompatibleDatafeed.prototype.getMarks = function (
  symbolInfo,
  rangeStart,
  rangeEnd,
  onDataCallback,
  resolution
) {
  if (this._configuration.supports_marks) {
    this._send(this._datafeedURL + "/marks", {
      symbol: symbolInfo.ticker.toUpperCase(),
      from: rangeStart,
      to: rangeEnd,
      resolution: resolution
    })
      .done(function (response) {
        onDataCallback(JSON.parse(response));
      })
      .fail(function () {
        onDataCallback([]);
      });
  }
};

Datafeeds.UDFCompatibleDatafeed.prototype.getTimescaleMarks = function (
  symbolInfo,
  rangeStart,
  rangeEnd,
  onDataCallback,
  resolution
) {
  if (this._configuration.supports_timescale_marks) {
    this._send(this._datafeedURL + "/timescale_marks", {
      symbol: symbolInfo.ticker.toUpperCase(),
      from: rangeStart,
      to: rangeEnd,
      resolution: resolution
    })
      .done(function (response) {
        onDataCallback(JSON.parse(response));
      })
      .fail(function () {
        onDataCallback([]);
      });
  }
};

Datafeeds.UDFCompatibleDatafeed.prototype.searchSymbols = function (
  searchString,
  exchange,
  type,
  onResultReadyCallback
) {
  var MAX_SEARCH_RESULTS = 30;

  if (!this._configuration) {
    onResultReadyCallback([]);
    return;
  }

  if (this._configuration.supports_search) {
    this._send(this._datafeedURL + "/search", {
      limit: MAX_SEARCH_RESULTS,
      query: searchString.toUpperCase(),
      type: type,
      exchange: exchange
    })
      .done(function (response) {
        var data = JSON.parse(response);

        for (var i = 0; i < data.length; ++i) {
          if (!data[i].params) {
            data[i].params = [];
          }
          data[i].exchange = data[i].exchange || '';
        }

        if (data && typeof data.s == "undefined" || data.s !== "error") {
          onResultReadyCallback(data);
        } else {
          onResultReadyCallback([]);
        }
      })
      .fail(function (reason) {
        onResultReadyCallback([]);
      });
  } else {
    if (!this._symbolSearch) {
      throw new Error(
        "Datafeed error: inconsistent configuration (symbol search)"
      );
    }

    var searchArgument = {
      searchString: searchString,
      exchange: exchange,
      type: type,
      onResultReadyCallback: onResultReadyCallback
    };

    if (this._initializationFinished) {
      this._symbolSearch.searchSymbols(searchArgument, MAX_SEARCH_RESULTS);
    } else {
      var that = this;

      this.on("initialized", function () {
        that._symbolSearch.searchSymbols(searchArgument, MAX_SEARCH_RESULTS);
      });
    }
  }
};

Datafeeds.UDFCompatibleDatafeed.prototype._symbolResolveURL = "/symbols";

//	BEWARE: this function does not consider symbol's exchange
Datafeeds.UDFCompatibleDatafeed.prototype.resolveSymbol = function (
  symbolName,
  onSymbolResolvedCallback,
  onResolveErrorCallback
) {
  var that = this;

  if (!this._initializationFinished) {
    this.on("initialized", function () {
      that.resolveSymbol(
        symbolName,
        onSymbolResolvedCallback,
        onResolveErrorCallback
      );
    });

    return;
  }

  var resolveRequestStartTime = Date.now();
  that._logMessage("Resolve requested");

  function onResultReady(data) {
    var postProcessedData = data;
    if (that.postProcessSymbolInfo) {
      postProcessedData = that.postProcessSymbolInfo(postProcessedData);
    }

    that._logMessage(
      "Symbol resolved: " + (Date.now() - resolveRequestStartTime)
    );

    onSymbolResolvedCallback(postProcessedData);
  }

  if (!this._configuration.supports_group_request) {
    this._send(this._datafeedURL + this._symbolResolveURL, {
      symbol: symbolName ? symbolName.toUpperCase() : ""
    })
      .done(function (response) {
        if (response) {
          var data = JSON.parse(response);

          if (data.s && data.s !== "ok") {
            onResolveErrorCallback("unknown_symbol");
          } else {
            onResultReady(data);
          }
        } else {
          onResolveErrorCallback("unknown_symbol");
        }
      })
      .fail(function (reason) {
        that._logMessage("Error resolving symbol: " + JSON.stringify([reason]));
        onResolveErrorCallback("unknown_symbol");
      });
  } else {
    if (this._initializationFinished) {
      this._symbolsStorage.resolveSymbol(
        symbolName,
        onResultReady,
        onResolveErrorCallback
      );
    } else {
      this.on("initialized", function () {
        that._symbolsStorage.resolveSymbol(
          symbolName,
          onResultReady,
          onResolveErrorCallback
        );
      });
    }
  }
};

// Datafeeds.UDFCompatibleDatafeed.prototype.saveLastCandles = function(data,identifier,symbolName) {

// 	if( window.tvcSavedData[identifier].stopSaveLastCandles === true){
// 		return false;
// 	}

// 	if( typeof(data.t) == 'undefined' || window.tvcSavedData[identifier].l.t > data.t[data.t.length - 1] ){
// 		return false;
// 	}

// 	console.log('saveLastCandles fired!!!');

// 	window.tvcSavedData[identifier].l = {};
// 	window.tvcSavedData[identifier].p = {};
// 	//save last candle
// 	window.tvcSavedData[identifier].l.t = data.t[data.t.length - 1];
// 	window.tvcSavedData[identifier].l.c = data.c[data.c.length - 1];
// 	window.tvcSavedData[identifier].l.h = data.h[data.h.length - 1];
// 	window.tvcSavedData[identifier].l.l = data.l[data.l.length - 1];
// 	window.tvcSavedData[identifier].l.o = data.o[data.o.length - 1];
// 	window.tvcSavedData[identifier].l.v = data.v[data.v.length - 1];
// 	window.tvcSavedData[identifier].l.vo = data.vo[data.vo.length - 1];
// 	//save previous candle
// 	window.tvcSavedData[identifier].p.t = data.t[data.t.length - 2];
// 	window.tvcSavedData[identifier].p.c = data.c[data.c.length - 2];
// 	window.tvcSavedData[identifier].p.h = data.h[data.h.length - 2];
// 	window.tvcSavedData[identifier].p.l = data.l[data.l.length - 2];
// 	window.tvcSavedData[identifier].p.o = data.o[data.o.length - 2];
// 	window.tvcSavedData[identifier].p.v = data.v[data.v.length - 2];
// 	window.tvcSavedData[identifier].p.vo = data.vo[data.vo.length -  2];

// 	window.tvcSavedData[identifier].stopSaveLastCandles = true;
// 	window.tvcSavedData[identifier].hasVolume = false;

// 	if(!isNaN(data.v[data.v.length - 2]) && !isNaN(data.v[data.v.length - 1])){
// 		window.tvcSavedData[identifier].hasVolume = true;
// 	}

// 	if(typeof(window.tvcSavedData.symbolsInfo) != 'undefined'){
// 		if(typeof(window.tvcSavedData.symbolsInfo[symbolName]) != 'undefined'){
// 			window.tvcSavedData.symbolsInfo[symbolName].close = data.c[data.c.length - 1];
// 		}else{
// 			window.tvcSavedData.symbolsInfo[symbolName] = {"pair_ID":null,"identifier":identifier,"close":data.c[data.c.length - 1]}
// 		}
// 	}
// }

Datafeeds.UDFCompatibleDatafeed.prototype._historyURL = "/history";

Datafeeds.UDFCompatibleDatafeed.prototype.parseDateTimeOfTimestamp = function (timeStamp) {
  var date = new Date(timeStamp * 1000);
  var dateNow = new Date();

  var year =
    date.getFullYear() === 0 ? dateNow.getFullYear() : date.getFullYear();
  var month =
    date.getMonth() + 1 === 0 ? dateNow.getMonth() : date.getMonth() + 1;
  var day = date.getDate() === 0 ? dateNow.getDate() : date.getDate();
  var hour = date.getHours();
  var minute = date.getMinutes();

  if (month < 10) { month = "0" + month; }
  if (day < 10) { day = "0" + day; }
  if (hour < 10) { hour = "0" + hour; }
  if (minute < 10) { minute = "0" + minute; }
  // console.log(year + "/" + month + "/" + day + " " + hour + ":" + minute);
  return "" + year + "/" + month + "/" + day + " " + hour + ":" + minute;
}

Datafeeds.UDFCompatibleDatafeed.prototype.getBars = function (
  symbolInfo,
  resolution,
  rangeStartDate,
  rangeEndDate,
  onDataCallback,
  onErrorCallback,
  firstDataRequest
) {
  //	timestamp sample: 1399939200
  // console.log('=====getBars running ', currInterval);
  if (resolution === '1D') {
    resolution = 'D';
  }
  var identifier = symbolInfo.ticker + ',' + resolution;
  // console.log('identifier ->' + identifier);
  // console.log('firstDataRequest ->' + firstDataRequest);

  if (firstDataRequest == undefined) {
    // if (symbolInfo.name && this._msgQuote.symbol && this._msgQuote.symbol.toLowerCase() === symbolInfo.name.toLowerCase()) {
    // console.log('symbolInfo: ' + symbolInfo.ticker + ' ---- symbol: ' + this._msgQuote.symbol + ' ---- lastTrade: ' + this._msgQuote.lastTrade);
    // var identifier = symbolInfo.ticker + ',' + resolution;
    var timeLast = this._msgQuote.timeUpdate + '_' + this._msgQuote.lastTrade;
    // console.log('TimeUpdate -> ', this._msgTimeUpdate, timeLast);
    // var dateEnd = this.parseDateTimeOfTimestamp(rangeEndDate);
    // var dateWithoutSeconds = new Date(dateEnd);
    var dateWithoutSeconds = new Date(rangeEndDate * 1000);
    var daysInMonth = dateWithoutSeconds.getDate();
    var ts = dateWithoutSeconds.getTime() / 1000;
    var currentInterval = getCurrentInterval(resolution, daysInMonth);
    // console.log('\n\nidentifier: ' + identifier);
    var sLast = this._msgQuote.lastTrade.replace('.', '').replace(',', '.');
    var lastTrade = parseFloat(sLast);
    // console.log('dateWithoutSeconds ->', ts);
    // console.log('fixedCandleTime ->', fixedCandleTime);
    // console.log('currentInterval ->', currentInterval);
    if (currentInterval < 86400) {
      // Candle menor que diario
      isNewCandle = false;
      if (this._msgTimeUpdate !== timeLast) {
        this._msgTimeUpdate = timeLast;
        if (ts >= (fixedCandleTime / 1000) + currentInterval) {
          isNewCandle = true;
          fixedCandleTime = (fixedCandleTime + (currentInterval * 1000));
          // console.log('=====isNewCandle = ', that._symbol, isNewCandle, fixedCandleTime);
        }
      }

      if (isNewCandle) {
        candleMin[identifier] = lastTrade;
        candleMax[identifier] = lastTrade;
        candleOpen[identifier] = lastTrade;
      } else {
        getCandleMin(lastTrade, identifier);
        getCandleMax(lastTrade, identifier);
      }
    }

    var bars = [];
    var barValue = {
      time: fixedCandleTime, // rangeEndDate * 1000,
      close: lastTrade
    };

    barValue.open = candleOpen[identifier];
    barValue.high = candleMax[identifier];
    barValue.low = candleMin[identifier];

    // if (ohlPresent) {
    //   barValue.open = data.o[i];
    //   barValue.high = data.h[i];
    //   barValue.low = data.l[i];
    // } else {
    //   barValue.open = barValue.high = barValue.low = barValue.close;
    // }
    // if (volumePresent) {
    //   barValue.volume = data.v[i];
    // }

    bars.push(barValue);
    bars.push(barValue);

    if (bars[bars.length - 1].time == barValue.time) {
      bars[bars.length - 1] = barValue;
    } else {
      bars.push(barValue);
    }

    // console.log('-> ' + identifier, dateEnd, barValue);

    onDataCallback(bars, {
      noData: false,
      nextTime: undefined
    });
    // }
  } else {
    // console.log('=====getBars running > FirstTime ', firstDataRequest);

    if (rangeStartDate > 0 && (rangeStartDate + "").length > 10) {
      throw new Error([
        "Got a JS time instead of Unix one.",
        rangeStartDate,
        rangeEndDate
      ]);
    }
    // if (getCookie(LOGIN_TYPE) !== 'LoginW') {
    rangeStartDate = this.parseDateTimeOfTimestamp(rangeStartDate);
    rangeEndDate = this.parseDateTimeOfTimestamp(rangeEndDate);
    // }
    // console.log('getBars > END');
    // console.log(rangeStartDate);
    // console.log(rangeEndDate);
    // console.log('-> FILTER DATE -> ', resolution, rangeStartDate, rangeEndDate);
    this._send(this._datafeedURL + this._historyURL, {
      symbol: symbolInfo.ticker.toUpperCase(),
      resolution: resolution,
      from: rangeStartDate,
      to: rangeEndDate
    })
      .done(function (response) {
        if (response) {
          var data = JSON.parse(response);
          var identifier = symbolInfo.ticker + ',' + resolution;
          // console.log('-> RESPONSE -> ', data);

          // if(typeof(window.tvcSavedData[identifier]) !== 'object'){
          //   window.tvcSavedData[identifier] = {
          //     stopSaveLastCandles:false,
          //     curr_pid:symbolInfo.ticker,
          //     l:{},
          //     p:{}
          //   };
          // }    
          // Datafeeds.UDFCompatibleDatafeed.prototype.saveLastCandles(data,identifier,symbolInfo.name);


          var nodata = data.s === "no_data";
          if (data.s !== "ok" && !nodata) {
            if (!!onErrorCallback) {
              onErrorCallback(data.s);
            }
            return;
          }

          var bars = [];
          //	data is JSON having format {s: "status" (ok, no_data, error),
          //  v: [volumes], t: [times], o: [opens], h: [highs], l: [lows], c:[closes], nb: "optional_unixtime_if_no_data"}
          var barsCount = nodata ? 0 : data.t.length;
          var volumePresent = typeof data.v != "undefined";
          var ohlPresent = typeof data.o != "undefined";

          for (var i = 0; i < barsCount; ++i) {
            var barValue = {
              time: data.t[i] * 1000,
              close: data.c[i]
            };

            if (ohlPresent) {
              barValue.open = data.o[i];
              barValue.high = data.h[i];
              barValue.low = data.l[i];

              candleOpen[identifier] = barValue.open;
              candleMax[identifier] = barValue.high;
              candleMin[identifier] = barValue.low;
            } else {
              barValue.open = barValue.high = barValue.low = barValue.close;
              candleOpen[identifier] = barValue.close;
              candleMax[identifier] = barValue.close;
              candleMin[identifier] = barValue.close;
            }

            if (volumePresent) {
              barValue.volume = data.v[i];
            }

            fixedCandleTime = barValue.time;
            bars.push(barValue);
          }
          // console.log(bars);
          onDataCallback(bars, {
            noData: nodata,
            nextTime: data.nb || data.nextTime
          });
        }
      })
      .fail(function (arg) {
        console.warn(["getBars(): HTTP error", arg]);

        if (!!onErrorCallback) {
          onErrorCallback("network error: " + JSON.stringify(arg));
        }
      });
  }
};

Datafeeds.UDFCompatibleDatafeed.prototype.subscribeBars = function (
  symbolInfo,
  resolution,
  onRealtimeCallback,
  listenerGUID,
  onResetCacheNeededCallback
) {
  this._symbol = symbolInfo.name;
  console.log('===== subscribeBars runnning > ', this._symbol); // , symbolInfo);
  this._SendMessageWS(this._symbol, "1");
  this._barsPulseUpdater.subscribeDataListener(
    symbolInfo,
    resolution,
    onRealtimeCallback,
    listenerGUID,
    onResetCacheNeededCallback
  );
};

Datafeeds.UDFCompatibleDatafeed.prototype.unsubscribeBars = function (listenerGUID) {
  console.log('===== unsubscribeBars running ', this._symbol);
  this._SendMessageWS(this._symbol, "0");
  this._symbol = "";
  this._msgTimeUpdate = "";
  this._barsPulseUpdater.unsubscribeDataListener(listenerGUID);
};

Datafeeds.UDFCompatibleDatafeed.prototype.calculateHistoryDepth = function (
  period,
  resolutionBack,
  intervalBack
) {
  // if (period == "240") {
  // 	return {
  // 		resolutionBack: 'M',
  // 		intervalBack: 3
  // 	};
  // }
};

Datafeeds.UDFCompatibleDatafeed.prototype.getQuotes = function (
  symbols,
  onDataCallback,
  onErrorCallback
) {
  this._send(this._datafeedURL + "/quotes", { symbols: symbols })
    .done(function (response) {
      var data = JSON.parse(response);
      if (data.s === "ok") {
        //	JSON format is {s: "status", [{s: "symbol_status", n: "symbol_name", v: {"field1": "value1", "field2": "value2", ..., "fieldN": "valueN"}}]}
        if (onDataCallback) {
          onDataCallback(data.d);
        }
      } else {
        if (onErrorCallback) {
          onErrorCallback(data.errmsg);
        }
      }
    })
    .fail(function (arg) {
      if (onErrorCallback) {
        onErrorCallback("network error: " + arg);
      }
    });
};

Datafeeds.UDFCompatibleDatafeed.prototype.subscribeQuotes = function (
  symbols,
  fastSymbols,
  onRealtimeCallback,
  listenerGUID
) {
  this._quotesPulseUpdater.subscribeDataListener(
    symbols,
    fastSymbols,
    onRealtimeCallback,
    listenerGUID
  );
};

Datafeeds.UDFCompatibleDatafeed.prototype.unsubscribeQuotes = function (
  listenerGUID
) {
  this._quotesPulseUpdater.unsubscribeDataListener(listenerGUID);
};

//	==================================================================================================================================================
//	==================================================================================================================================================
//	==================================================================================================================================================

/*
	It's a symbol storage component for ExternalDatafeed. This component can
	  * interact to UDF-compatible datafeed which supports whole group info requesting
	  * do symbol resolving -- return symbol info by its name
*/
Datafeeds.SymbolsStorage = function (datafeed) {
  this._datafeed = datafeed;

  this._exchangesList = ["NYSE", "FOREX", "AMEX"];
  this._exchangesWaitingForData = {};
  this._exchangesDataCache = {};

  this._symbolsInfo = {};
  this._symbolsList = [];

  this._requestFullSymbolsList();
};

Datafeeds.SymbolsStorage.prototype._requestFullSymbolsList = function () {
  var that = this;

  for (var i = 0; i < this._exchangesList.length; ++i) {
    var exchange = this._exchangesList[i];

    if (this._exchangesDataCache.hasOwnProperty(exchange)) {
      continue;
    }

    this._exchangesDataCache[exchange] = true;

    this._exchangesWaitingForData[exchange] = "waiting_for_data";

    this._datafeed
      ._send(this._datafeed._datafeedURL + "/symbol_info", {
        group: exchange
      })
      .done(
        (function (exchange) {
          return function (response) {
            that._onExchangeDataReceived(exchange, JSON.parse(response));
            that._onAnyExchangeResponseReceived(exchange);
          };
        })(exchange)
      )
      .fail(
        (function (exchange) {
          return function (reason) {
            that._onAnyExchangeResponseReceived(exchange);
          };
        })(exchange)
      );
  }
};

Datafeeds.SymbolsStorage.prototype._onExchangeDataReceived = function (
  exchangeName,
  data
) {
  function tableField(data, name, index) {
    return data[name] instanceof Array ? data[name][index] : data[name];
  }

  try {
    for (var symbolIndex = 0; symbolIndex < data.symbol.length; ++symbolIndex) {
      var symbolName = data.symbol[symbolIndex];
      var listedExchange = tableField(data, "exchange-listed", symbolIndex);
      var tradedExchange = tableField(data, "exchange-traded", symbolIndex);
      var fullName = tradedExchange + ":" + symbolName;
      //	This feature support is not implemented yet
      //	var hasDWM = tableField(data, "has-dwm", symbolIndex);
      var hasIntraday = tableField(data, "has-intraday", symbolIndex);
      var tickerPresent = typeof data.ticker != "undefined";
      var symbolInfo = {
        name: symbolName,
        base_name: [listedExchange + ":" + symbolName],
        description: tableField(data, "description", symbolIndex),
        full_name: fullName,
        legs: [fullName],
        has_intraday: hasIntraday,
        has_no_volume: tableField(data, "has-no-volume", symbolIndex),
        listed_exchange: listedExchange,
        exchange: tradedExchange,
        minmov:
          tableField(data, "minmovement", symbolIndex) ||
          tableField(data, "minmov", symbolIndex),
        minmove2:
          tableField(data, "minmove2", symbolIndex) ||
          tableField(data, "minmov2", symbolIndex),
        fractional: tableField(data, "fractional", symbolIndex),
        pointvalue: tableField(data, "pointvalue", symbolIndex),
        pricescale: tableField(data, "pricescale", symbolIndex),
        type: tableField(data, "type", symbolIndex),
        session: tableField(data, "session-regular", symbolIndex),
        ticker: tickerPresent
          ? tableField(data, "ticker", symbolIndex)
          : symbolName,
        timezone: tableField(data, "timezone", symbolIndex),
        supported_resolutions:
          tableField(data, "supported-resolutions", symbolIndex) ||
          this._datafeed.defaultConfiguration().supported_resolutions,
        force_session_rebuild:
          tableField(data, "force-session-rebuild", symbolIndex) || false,
        has_daily: tableField(data, "has-daily", symbolIndex) || true,
        intraday_multipliers: tableField(
          data,
          "intraday-multipliers",
          symbolIndex
        ) || ["1", "5", "15", "30", "60"],
        has_fractional_volume:
          tableField(data, "has-fractional-volume", symbolIndex) || false,
        has_weekly_and_monthly:
          tableField(data, "has-weekly-and-monthly", symbolIndex) || false,
        has_empty_bars:
          tableField(data, "has-empty-bars", symbolIndex) || false,
        volume_precision: tableField(data, "volume-precision", symbolIndex) || 0
      };

      this._symbolsInfo[symbolInfo.ticker] = this._symbolsInfo[
        symbolName
      ] = this._symbolsInfo[fullName] = symbolInfo;
      this._symbolsList.push(symbolName);
      console.log(this._symbolsList);
    }
  } catch (error) {
    throw new Error(
      "API error when processing exchange `" +
      exchangeName +
      "` symbol #" +
      symbolIndex +
      ": " +
      error
    );
  }
};

Datafeeds.SymbolsStorage.prototype._onAnyExchangeResponseReceived = function (
  exchangeName
) {
  delete this._exchangesWaitingForData[exchangeName];

  var allDataReady = Object.keys(this._exchangesWaitingForData).length === 0;

  if (allDataReady) {
    this._symbolsList.sort();
    this._datafeed._logMessage("All exchanges data ready");
    this._datafeed.onInitialized();
  }
};

//	BEWARE: this function does not consider symbol's exchange
Datafeeds.SymbolsStorage.prototype.resolveSymbol = function (
  symbolName,
  onSymbolResolvedCallback,
  onResolveErrorCallback
) {
  var that = this;

  setTimeout(function () {
    if (!that._symbolsInfo.hasOwnProperty(symbolName)) {
      onResolveErrorCallback("invalid symbol");
    } else {
      onSymbolResolvedCallback(that._symbolsInfo[symbolName]);
    }
  }, 0);
};

//	==================================================================================================================================================
//	==================================================================================================================================================
//	==================================================================================================================================================

/*
	It's a symbol search component for ExternalDatafeed. This component can do symbol search only.
	This component strongly depends on SymbolsDataStorage and cannot work without it. Maybe, it would be
	better to merge it to SymbolsDataStorage.
*/

Datafeeds.SymbolSearchComponent = function (datafeed) {
  this._datafeed = datafeed;
};

//	searchArgument = { searchString, onResultReadyCallback}
Datafeeds.SymbolSearchComponent.prototype.searchSymbols = function (
  searchArgument,
  maxSearchResults
) {
  if (!this._datafeed._symbolsStorage) {
    throw new Error(
      "Cannot use local symbol search when no groups information is available"
    );
  }

  var symbolsStorage = this._datafeed._symbolsStorage;

  var results = []; // array of WeightedItem { item, weight }
  var queryIsEmpty =
    !searchArgument.searchString || searchArgument.searchString.length === 0;
  var searchStringUpperCase = searchArgument.searchString.toUpperCase();

  for (var i = 0; i < symbolsStorage._symbolsList.length; ++i) {
    var symbolName = symbolsStorage._symbolsList[i];
    var item = symbolsStorage._symbolsInfo[symbolName];

    if (
      searchArgument.type &&
      searchArgument.type.length > 0 &&
      item.type !== searchArgument.type
    ) {
      continue;
    }

    if (
      searchArgument.exchange &&
      searchArgument.exchange.length > 0 &&
      item.exchange !== searchArgument.exchange
    ) {
      continue;
    }

    var positionInName = item.name.toUpperCase().indexOf(searchStringUpperCase);
    var positionInDescription = item.description
      .toUpperCase()
      .indexOf(searchStringUpperCase);

    if (queryIsEmpty || positionInName >= 0 || positionInDescription >= 0) {
      var found = false;
      for (var resultIndex = 0; resultIndex < results.length; resultIndex++) {
        if (results[resultIndex].item === item) {
          found = true;
          break;
        }
      }

      if (!found) {
        var weight =
          positionInName >= 0 ? positionInName : 8000 + positionInDescription;
        results.push({ item: item, weight: weight });
      }
    }
  }
  searchArgument.onResultReadyCallback(
    results
      .sort(function (weightedItem1, weightedItem2) {
        return weightedItem1.weight - weightedItem2.weight;
      })
      .map(function (weightedItem) {
        var item = weightedItem.item;
        return {
          symbol: item.name,
          full_name: item.full_name,
          description: item.description,
          exchange: item.exchange,
          params: [],
          type: item.type,
          ticker: item.name
        };
      })
      .slice(0, Math.min(results.length, maxSearchResults))
  );
};

//	==================================================================================================================================================
//	==================================================================================================================================================
//	==================================================================================================================================================

/*
	This is a pulse updating components for ExternalDatafeed. They emulates realtime updates with periodic requests.
*/

Datafeeds.DataPulseUpdater = function (datafeed, updateFrequency) {
  this._datafeed = datafeed;
  this._subscribers = {};

  this._requestsPending = 0;
  var that = this;

  var update = function () {
    // verifica se o token ainda existe ou se saiu da pagina
    // caso não exista cancela a atualização.
    if (!valideCookie()) {
      clearInterval(interval);
      return;
    }

    if (that._requestsPending > 0) {
      return;
    }

    for (var listenerGUID in that._subscribers) {
      var subscriptionRecord = that._subscribers[listenerGUID];
      var resolution = subscriptionRecord.resolution;

      var datesRangeRight = parseInt(new Date().valueOf() / 1000);

      //	BEWARE: please note we really need 2 bars, not the only last one
      //	see the explanation below. `10` is the `large enough` value to work around holidays
      var datesRangeLeft =
        datesRangeRight - that.periodLengthSeconds(resolution, 10);

      that._requestsPending++;

      (function (_subscriptionRecord) {
        // eslint-disable-line
        that._datafeed.getBars(
          _subscriptionRecord.symbolInfo,
          resolution,
          datesRangeLeft,
          datesRangeRight,
          function (bars) {
            that._requestsPending--;

            //	means the subscription was cancelled while waiting for data
            if (!that._subscribers.hasOwnProperty(listenerGUID)) {
              return;
            }

            if (bars.length === 0) {
              return;
            }

            var lastBar = bars[bars.length - 1];
            if (
              !isNaN(_subscriptionRecord.lastBarTime) &&
              lastBar.time < _subscriptionRecord.lastBarTime
            ) {
              return;
            }

            var subscribers = _subscriptionRecord.listeners;

            //	BEWARE: this one isn't working when first update comes and this update makes a new bar. In this case
            //	_subscriptionRecord.lastBarTime = NaN
            var isNewBar =
              !isNaN(_subscriptionRecord.lastBarTime) &&
              lastBar.time > _subscriptionRecord.lastBarTime;

            //	Pulse updating may miss some trades data (ie, if pulse period = 10 secods and new bar is started 5 seconds later after the last update, the
            //	old bar's last 5 seconds trades will be lost). Thus, at fist we should broadcast old bar updates when it's ready.
            if (isNewBar) {
              if (bars.length < 2) {
                throw new Error(
                  "Not enough bars in history for proper pulse update. Need at least 2."
                );
              }

              var previousBar = bars[bars.length - 2];
              for (var i = 0; i < subscribers.length; ++i) {
                subscribers[i](previousBar);
              }
            }

            _subscriptionRecord.lastBarTime = lastBar.time;

            for (var i = 0; i < subscribers.length; ++i) {
              subscribers[i](lastBar);
            }
          },

          //	on error
          function () {
            that._requestsPending--;
          }
        );
      })(subscriptionRecord);
    }
  };

  if (typeof updateFrequency != "undefined" && updateFrequency > 0) {
    interval = setInterval(update, updateFrequency);
  }
};

Datafeeds.DataPulseUpdater.prototype.unsubscribeDataListener = function (
  listenerGUID
) {
  this._datafeed._logMessage("Unsubscribing " + listenerGUID);
  delete this._subscribers[listenerGUID];
};

Datafeeds.DataPulseUpdater.prototype.subscribeDataListener = function (
  symbolInfo,
  resolution,
  newDataCallback,
  listenerGUID
) {
  this._datafeed._logMessage("Subscribing " + listenerGUID);

  if (!this._subscribers.hasOwnProperty(listenerGUID)) {
    this._subscribers[listenerGUID] = {
      symbolInfo: symbolInfo,
      resolution: resolution,
      lastBarTime: NaN,
      listeners: []
    };
  }

  this._subscribers[listenerGUID].listeners.push(newDataCallback);
};

Datafeeds.DataPulseUpdater.prototype.periodLengthSeconds = function (
  resolution,
  requiredPeriodsCount
) {
  var daysCount = 0;

  if (resolution === "D") {
    daysCount = requiredPeriodsCount;
  } else if (resolution === "M") {
    daysCount = 31 * requiredPeriodsCount;
  } else if (resolution === "W") {
    daysCount = 7 * requiredPeriodsCount;
  } else {
    daysCount = requiredPeriodsCount * resolution / (24 * 60);
  }

  return daysCount * 24 * 60 * 60;
};

Datafeeds.QuotesPulseUpdater = function (datafeed) {
  this._datafeed = datafeed;
  this._subscribers = {};
  this._updateInterval = 60 * 1000;
  this._fastUpdateInterval = 10 * 1000;
  this._requestsPending = 0;

  var that = this;

  setInterval(function () {
    that._updateQuotes(function (subscriptionRecord) {
      return subscriptionRecord.symbols;
    });
  }, this._updateInterval);

  setInterval(function () {
    that._updateQuotes(function (subscriptionRecord) {
      return subscriptionRecord.fastSymbols.length > 0
        ? subscriptionRecord.fastSymbols
        : subscriptionRecord.symbols;
    });
  }, this._fastUpdateInterval);
};

Datafeeds.QuotesPulseUpdater.prototype.subscribeDataListener = function (
  symbols,
  fastSymbols,
  newDataCallback,
  listenerGUID
) {
  if (!this._subscribers.hasOwnProperty(listenerGUID)) {
    this._subscribers[listenerGUID] = {
      symbols: symbols,
      fastSymbols: fastSymbols,
      listeners: []
    };
  }

  this._subscribers[listenerGUID].listeners.push(newDataCallback);
};

Datafeeds.QuotesPulseUpdater.prototype.unsubscribeDataListener = function (
  listenerGUID
) {
  delete this._subscribers[listenerGUID];
};

Datafeeds.QuotesPulseUpdater.prototype._updateQuotes = function (symbolsGetter) {
  if (this._requestsPending > 0) {
    return;
  }

  var that = this;
  for (var listenerGUID in this._subscribers) {
    this._requestsPending++;

    var subscriptionRecord = this._subscribers[listenerGUID];
    this._datafeed.getQuotes(
      symbolsGetter(subscriptionRecord),

      // onDataCallback
      (function (subscribers, guid) {
        // eslint-disable-line
        return function (data) {
          that._requestsPending--;

          // means the subscription was cancelled while waiting for data
          if (!that._subscribers.hasOwnProperty(guid)) {
            return;
          }

          for (var i = 0; i < subscribers.length; ++i) {
            subscribers[i](data);
          }
        };
      })(subscriptionRecord.listeners, listenerGUID),
      // onErrorCallback
      function (error) {
        that._requestsPending--;
      }
    );
  }
};
