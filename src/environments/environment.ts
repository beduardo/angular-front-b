const VERSION = 'DEV.0.20.06.17A';
export const environment = {
  production: false,
  version: VERSION,
  showConsoleLog: true,
  RECONNECT_INTERVAL: 3000,
  RECONNECT_INTERVAL_TWO: 3000,
  RECONECT_ATTEMPTS: 7,
  URL_WSO2: '0.0.0.0',
  delay_quote: '600',
  delay_book: '600',
  delay_book_aggregated: '600',
  delay_book_business: '600',
  period_candle_chart: '5',
  SESSION_TIMEOUT_ALERT: 60000, // 1min before reset timeout
  SESSION_TIMEOUT: 1800000, // 30 minutos
  CONNECTION_INTERVAL: 1000,
  MIN_CARACTER_QUOTE: 4,
  MIN_REFRESH_WSO2TOKEN_INTERVAL: 5,
  VALID_USER_INTERVAL: 5000, // 5000 = 5seg
  VALID_USER_REDIRECT: '/login',
  CONFIG_FILE: '../assets/config/env.json',
  LOGIN_TYPE: 'LoginO',
  WHITELABEL: 7,
  BROKER: 'FCSTONE',
  BROKER_PATH: 'fc-stone',
  BASE_HREF: '/',
  isDev: true,
  jwtRevokeControl: false
};

// WHITELABEL:

// Ambiente: 1 - API - (CEDRO) - LoginS
// Versão: VERSION = 'PRD1.API.0.19.11.13A'
// Imagem: HB_FASTWEB_CEDRO_FACADE_3130

// Ambiente: 2 - API - (PI) - LoginO
// Versão: VERSION = 'PRD1.API.0.19.11.13A'
// Imagem: HB_FASTWEB_PI_FACADE_3130

// Ambiente: 3 - WSO2 - (RB) - LoginW
// Versão: VERSION = 'PRD1.WSO2.0.19.11.13A'
// Imagem: HB_FASTWEB_RB_WSO2_3130

// Ambiente: 4 - WSO2 - (CEDRO) - LoginW
// Versão: VERSION = 'PRD1.WSO2.0.19.11.13A'
// Imagem: HB_FASTWEB_CEDRO_WSO2_3130

// Ambiente: 5 - WSO2 (UM) - LoginW
// Versão: VERSION = 'PRD1.WSO2.0.19.11.13A'
// Imagem: HB_FASTWEB_UM_WSO2_3130

// Ambiente: 6 - WSO2 - (DAYCOVAL) - LoginS
// Versão: VERSION = 'PRD1.WSO2.0.19.11.13A'
// Imagem: HB_FASTWEB_DAYCOVAL_WSO2_3130

// Ambiente: 7 - API - (FC STONE) - LoginO
// Versão: VERSION = 'PRD1.API.0.19.11.13A'
// Imagem: HB_FASTWEB_FC_STONE_FACADE_3130

// Ambiente: 8 - WSO2 - (TRINUS) - LoginW
// Versão: VERSION = 'PRD1.WSO2.0.19.11.13A'
// Imagem: HB_FASTWEB_TRINUS_WSO2_3130

// Ambiente: 9 - API - (VITREO) - LoginS
// Versão: VERSION = 'PRD1.API.0.19.11.13A'
// Imagem: HB_FASTWEB_VITREO_FACADE_3130

// Ambiente: 10 - API - (RANKING) - LoginS
// Versão: VERSION = 'PRD1.API.0.19.11.13A'
// Imagem: HB_FASTWEB_RANKING_FACADE_3130

