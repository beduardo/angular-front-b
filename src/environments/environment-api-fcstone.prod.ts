const VERSION = 'PRD1.API.0.20.06.17A';
export const environment = {
  production: true,
  version: VERSION,
  showConsoleLog: true,
  RECONNECT_INTERVAL: 3000,
  RECONNECT_INTERVAL_TWO: 3000,
  RECONECT_ATTEMPTS: 7,
  URL_WSO2: '0.0.0.0',
  delay_quote: '600',
  delay_book: '600',
  delay_book_aggregated: '600',
  delay_book_business: '600',
  period_candle_chart: '5',
  SESSION_TIMEOUT_ALERT: 60000,           // 1min before reset timeout
  SESSION_TIMEOUT: 1800000,              // 24 horas
  CONNECTION_INTERVAL: 1000,
  MIN_CARACTER_QUOTE: 4,
  MIN_REFRESH_WSO2TOKEN_INTERVAL: 5,
  VALID_USER_INTERVAL: 5000,              // 5000 = 5seg
  VALID_USER_REDIRECT: '/login',
  CONFIG_FILE: 'assets/config/env.json',
  LOGIN_TYPE: 'LoginO',
  WHITELABEL: 7,
  BROKER: 'FCSTONE',
  BROKER_PATH: 'fc-stone',
  BASE_HREF: '/',
  isDev: false,
  jwtRevokeControl: false
};
