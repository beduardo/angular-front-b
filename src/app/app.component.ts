import { TransitionService } from './services/transition.service';
import { Component, HostListener, OnDestroy, OnInit, Renderer2 } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { MatIconRegistry, MatDialog, MatDialogRef } from '@angular/material';
import { AutoLogoutService } from '@auth/auto-logout-service.service';
import { SocketService } from '@services/webFeeder/socket.service';
import { Constants } from '@utils/constants';
import { LayoutService } from '@services/layout.service';
import { MinResolutionModalComponent } from '@component/min-resolution-modal/min-resolution-modal.component';
import { ConfigService } from '@services/webFeeder/config.service';
import { TokenService } from '@services/webFeeder/token.service';
import { AppUtils } from '@utils/app.utils';
import { AuthenticationService } from '@services/authentication.service';
import { CustomSnackbarService } from '@services/custom-snackbar-service.service';
import { Auth1Service } from '@services/strategy-pattern-login/auth1.service';
import { Auth2Service } from '@services/strategy-pattern-login/auth2.service';
import { Auth3Service } from '@services/strategy-pattern-login/auth3.service';


@Component({
  selector: 'app-fast-trade',
  templateUrl: './app.component.html',
})
export class AppComponent implements OnInit, OnDestroy {
  title = 'app';

  dialog: MatDialog;
  resizeModalShown: boolean;
  modal: MatDialogRef<MinResolutionModalComponent>;

  simpleNotificationsOptions = {
    timeOut: Constants.SIMPLE_NOTIFICATIONS_TIMEOUT,
    showProgressBar: Constants.SIMPLE_NOTIFICATIONS_PROGRESS
  };

  @HostListener('mousemove')
  onMouseMove() {
    this._autoLogoutService.resetTime();
  }

  @HostListener('click')
  onClick() {
    this._autoLogoutService.resetTime();
  }

  constructor(
    dialog: MatDialog,
    domSanitizer: DomSanitizer,
    public icons: MatIconRegistry,
    private _autoLogoutService: AutoLogoutService,
    private socketService: SocketService,
    private layoutService: LayoutService,
    private renderer: Renderer2,
    private configService: ConfigService,
    private tokenService: TokenService,
    public snackBar: CustomSnackbarService) {
    this.dialog = dialog;
    this.resizeModalShown = false;

    icons.addSvgIcon('send', domSanitizer.bypassSecurityTrustResourceUrl('assets/images/svg/paperplane-colored.svg'));
    icons.addSvgIcon('close-btn', domSanitizer.bypassSecurityTrustResourceUrl('assets/images/svg/close-btn.svg'));
    icons.addSvgIcon('notify-warning', domSanitizer.bypassSecurityTrustResourceUrl('assets/images/svg/notify-warning.svg'));
    icons.addSvgIcon('notify-error', domSanitizer.bypassSecurityTrustResourceUrl('assets/images/svg/notify-error.svg'));
    icons.addSvgIcon('notify-success', domSanitizer.bypassSecurityTrustResourceUrl('assets/images/svg/notify-success.svg'));
    icons.addSvgIcon('notify-info', domSanitizer.bypassSecurityTrustResourceUrl('assets/images/svg/notify-info.svg'));
    icons.addSvgIcon('monitor', domSanitizer.bypassSecurityTrustResourceUrl('assets/images/svg/monitor.svg'));
    icons.addSvgIcon('arrow-down', domSanitizer.bypassSecurityTrustResourceUrl('assets/images/svg/arrow-down.svg'));
    icons.addSvgIcon('marketSummary', domSanitizer.bypassSecurityTrustResourceUrl('assets/images/svg/chart.svg'));
    icons.addSvgIcon('dayTrade', domSanitizer.bypassSecurityTrustResourceUrl('assets/images/svg/people-trading.svg'));
    icons.addSvgIcon('lineChart', domSanitizer.bypassSecurityTrustResourceUrl('assets/images/svg/line-chart.svg'));
    icons.addSvgIcon('customWk', domSanitizer.bypassSecurityTrustResourceUrl('assets/images/svg/component-stack.svg'));
    icons.addSvgIcon('desktop', domSanitizer.bypassSecurityTrustResourceUrl('assets/images/svg/desktop.svg'));
    icons.addSvgIcon('close', domSanitizer.bypassSecurityTrustResourceUrl('assets/images/svg/close.svg'));
    icons.addSvgIcon('apps', domSanitizer.bypassSecurityTrustResourceUrl('assets/images/svg/apps.svg'));
    icons.addSvgIcon('lock', domSanitizer.bypassSecurityTrustResourceUrl('assets/images/svg/lock.svg'));
    icons.addSvgIcon('layers', domSanitizer.bypassSecurityTrustResourceUrl('assets/images/svg/layers.svg'));
    icons.addSvgIcon('edit-outline', domSanitizer.bypassSecurityTrustResourceUrl('assets/images/svg/edit-outline.svg'));
    icons.addSvgIcon('visibility-outline', domSanitizer.bypassSecurityTrustResourceUrl('assets/images/svg/visibility-outline.svg'));
    icons.addSvgIcon('building', domSanitizer.bypassSecurityTrustResourceUrl('assets/images/svg/building.svg'));
    icons.addSvgIcon('account', domSanitizer.bypassSecurityTrustResourceUrl('assets/images/svg/account.svg'));
    icons.addSvgIcon('arrow-right', domSanitizer.bypassSecurityTrustResourceUrl('assets/images/svg/arrow-right.svg'));
    icons.addSvgIcon('moon', domSanitizer.bypassSecurityTrustResourceUrl('assets/images/svg/moon.svg'));
    icons.addSvgIcon('power', domSanitizer.bypassSecurityTrustResourceUrl('assets/images/svg/power.svg'));
    icons.addSvgIcon('search', domSanitizer.bypassSecurityTrustResourceUrl('assets/images/svg/search.svg'));
    icons.addSvgIcon('funds', domSanitizer.bypassSecurityTrustResourceUrl('assets/images/svg/funds.svg'));
    icons.addSvgIcon('cash', domSanitizer.bypassSecurityTrustResourceUrl('assets/images/svg/cash.svg'));
    icons.addSvgIcon('delete-white', domSanitizer.bypassSecurityTrustResourceUrl('assets/images/svg/delete-white.svg'));
    icons.addSvgIcon('open-lock', domSanitizer.bypassSecurityTrustResourceUrl('assets/images/svg/open-lock.svg'));
    icons.addSvgIcon('chain', domSanitizer.bypassSecurityTrustResourceUrl('assets/images/svg/chain.svg'));
    icons.addSvgIcon('expand', domSanitizer.bypassSecurityTrustResourceUrl('assets/images/svg/expand.svg'));
    icons.addSvgIcon('filter', domSanitizer.bypassSecurityTrustResourceUrl('assets/images/svg/filter.svg'));
    icons.addSvgIcon('news', domSanitizer.bypassSecurityTrustResourceUrl('assets/images/svg/news.svg'));
    icons.addSvgIcon('goback', domSanitizer.bypassSecurityTrustResourceUrl('assets/images/svg/goback-arrow.svg'));
    icons.addSvgIcon('monitor-resolution', domSanitizer.bypassSecurityTrustResourceUrl('assets/images/svg/monitor-resolution.svg'));
    icons.addSvgIcon('grid', domSanitizer.bypassSecurityTrustResourceUrl('assets/images/svg/grid.svg'));
    icons.addSvgIcon('coin', domSanitizer.bypassSecurityTrustResourceUrl('assets/images/svg/coin.svg'));
    icons.addSvgIcon('line-chart-sidebar', domSanitizer.bypassSecurityTrustResourceUrl('assets/images/svg/line-chart-sidebar.svg'));
    icons.addSvgIcon('file', domSanitizer.bypassSecurityTrustResourceUrl('assets/images/svg/file.svg'));
    icons.addSvgIcon('edit', domSanitizer.bypassSecurityTrustResourceUrl('assets/images/svg/edit.svg'));
    icons.addSvgIcon('reply', domSanitizer.bypassSecurityTrustResourceUrl('assets/images/svg/reply.svg'));
    icons.addSvgIcon('delete', domSanitizer.bypassSecurityTrustResourceUrl('assets/images/svg/delete.svg'));
    icons.addSvgIcon('event', domSanitizer.bypassSecurityTrustResourceUrl('assets/images/svg/event.svg'));
    icons.addSvgIcon('user', domSanitizer.bypassSecurityTrustResourceUrl('assets/images/svg/user.svg'));
    icons.addSvgIcon('padlock-open', domSanitizer.bypassSecurityTrustResourceUrl('assets/images/svg/padlock-open.svg'));
    icons.addSvgIcon('padlock-locked', domSanitizer.bypassSecurityTrustResourceUrl('assets/images/svg/padlock-locked.svg'));
    icons.addSvgIcon('login-lock', domSanitizer.bypassSecurityTrustResourceUrl('assets/images/svg/login-locker.svg'));
    icons.addSvgIcon('eye', domSanitizer.bypassSecurityTrustResourceUrl('assets/images/svg/eye.svg'));
    icons.addSvgIcon('crossed-eye', domSanitizer.bypassSecurityTrustResourceUrl('assets/images/svg/crossed-eye.svg'));
    icons.addSvgIcon('date', domSanitizer.bypassSecurityTrustResourceUrl('assets/images/svg/date.svg'));
    icons.addSvgIcon('session', domSanitizer.bypassSecurityTrustResourceUrl('assets/images/svg/session.svg'));
    icons.addSvgIcon('404', domSanitizer.bypassSecurityTrustResourceUrl('assets/images/svg/404.svg'));
    icons.addSvgIcon('error', domSanitizer.bypassSecurityTrustResourceUrl('assets/images/svg/error.svg'));
    icons.addSvgIcon('permission', domSanitizer.bypassSecurityTrustResourceUrl('assets/images/svg/permission.svg'));
    icons.addSvgIcon('download', domSanitizer.bypassSecurityTrustResourceUrl('assets/images/svg/download.svg'));
    icons.addSvgIcon('power-thick', domSanitizer.bypassSecurityTrustResourceUrl('assets/images/svg/power-thick.svg'));
    icons.addSvgIcon('whatsapp-logo', domSanitizer.bypassSecurityTrustResourceUrl('assets/images/svg/whatsapp-logo.svg'));
    icons.addSvgIcon('saldo', domSanitizer.bypassSecurityTrustResourceUrl('assets/images/svg/saldo.svg'));
    icons.addSvgIcon('send', domSanitizer.bypassSecurityTrustResourceUrl('assets/images/svg/send.svg'));
    icons.addSvgIcon('password-success', domSanitizer.bypassSecurityTrustResourceUrl('assets/images/svg/password-success.svg'));
    icons.addSvgIcon('left-arrow', domSanitizer.bypassSecurityTrustResourceUrl('assets/images/svg/left-arrow.svg'));
    icons.addSvgIcon('logo-cedrotech', domSanitizer.bypassSecurityTrustResourceUrl('assets/images/svg/logo-cedrotech.svg'));
    icons.addSvgIcon('editarOrder', domSanitizer.bypassSecurityTrustResourceUrl('assets/images/svg/editarOrder.svg'));
    icons.addSvgIcon('reenviarOrder', domSanitizer.bypassSecurityTrustResourceUrl('assets/images/svg/reenviarOrder.svg'));
    icons.addSvgIcon('removerOrder', domSanitizer.bypassSecurityTrustResourceUrl('assets/images/svg/removerOrder.svg'));
    icons.addSvgIcon('more', domSanitizer.bypassSecurityTrustResourceUrl('assets/images/svg/more.svg'));
    icons.addSvgIcon('auction_hammer', domSanitizer.bypassSecurityTrustResourceUrl('assets/images/svg/auction_hammer.svg'));
    icons.addSvgIcon('term-operator', domSanitizer.bypassSecurityTrustResourceUrl('assets/images/svg/term-operator.svg'));
    icons.addSvgIcon('success-password', domSanitizer.bypassSecurityTrustResourceUrl('assets/images/svg/ico-success-password.svg'));
    icons.addSvgIcon('success-password-disable', domSanitizer.bypassSecurityTrustResourceUrl('assets/images/svg/ico-success-password-disable.svg'));
    icons.addSvgIcon('icon_firefox', domSanitizer.bypassSecurityTrustResourceUrl('assets/images/svg/icon_firefox.svg'));
    icons.addSvgIcon('icon_chrome', domSanitizer.bypassSecurityTrustResourceUrl('assets/images/svg/icon_chrome.svg'));
    icons.addSvgIcon('ranking', domSanitizer.bypassSecurityTrustResourceUrl('assets/images/svg/ranking.svg'));
    icons.addSvgIcon('material-educacional', domSanitizer.bypassSecurityTrustResourceUrl('assets/images/svg/material-educacional.svg'));
    icons.addSvgIcon('phone', domSanitizer.bypassSecurityTrustResourceUrl('assets/images/svg/phone.svg'));
    icons.addSvgIcon('clock', domSanitizer.bypassSecurityTrustResourceUrl('assets/images/svg/clock.svg'));
    icons.addSvgIcon('ico-user-manual', domSanitizer.bypassSecurityTrustResourceUrl('assets/images/svg/ico-user-manual.svg'));
    icons.addSvgIcon('ico-user-manual-two', domSanitizer.bypassSecurityTrustResourceUrl('assets/images/svg/ico-user-manual-two.svg'));
  }
  ngOnInit() {
    // this.transitionService.recover();

    if (this.configService.isAuthenticadSystem()) {
      this.tokenService.assignServices();
    }

    this.layoutService.isDark$.subscribe(isDark => {
      if (isDark) {
        this.renderer.addClass(document.body, 'darkTheme');
      } else {
        this.renderer.removeClass(document.body, 'darkTheme');
      }
    });
    if (window.innerWidth < Constants.MIN_RESOLUTION && !this.configService.isNewWindow()
      && !AppUtils.getPageByName('/liveUrl()', window.location.href) && !AppUtils.getPageByName('/chatUrl()', window.location.href)) {
      this.openResolutionModal();
    }
  }

  ngOnDestroy() {
    this.socketService.logout();
  }

  @HostListener('window:resize', ['$event'])
  onResize(event) {
    if (!this.configService.isNewWindow()) {
      if (event.target.innerWidth < Constants.MIN_RESOLUTION && !this.resizeModalShown
        && !AppUtils.getPageByName('/liveUrl()', window.location.href) && !AppUtils.getPageByName('/chatUrl()', window.location.href)) {
        this.openResolutionModal();
      } else {
        if (event.target.innerWidth >= Constants.MIN_RESOLUTION) {
          this.resizeModalShown = false;
          if (this.modal !== undefined) {
            this.modal.close();
          }
        }
      }
    }
  }

  openResolutionModal() {
    this.resizeModalShown = true;
    this.modal = this.dialog.open(MinResolutionModalComponent, {
      data: {
        title: 'Resolução minima',
        body: Constants.MSG_MIN_RESOLUTION
      },
      width: '100vw',
      height: '100vh',
      maxWidth: '100vw',
      disableClose: true,
    });
  }

}
