import * as moment from 'moment';
import { NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';
export class MomentUtils {
    public static getStartOfCurrentMonth(format?: string): string {
        const f = format ? format : 'DD/MM/YYYY';
        return moment().startOf('month').format(f);
    }
    public static getEndOfCurrentMonth(format?: string): string {
        const f = format ? format : 'DD/MM/YYYY';
        return moment().endOf('month').format(f);
    }
    public static convertToNgDate(dt: number | string | Date): NgbDateStruct {
        const d = moment(dt);
        return d.isValid() ? { year: d.year(),
                               month: d.month() + 1,
                               day: d.date() } : null;
    }
    public static setValidityDateFromCurrentDate(days: number, format?: string): string  {
        const f = format ? format : 'DD/MM/YYYY';
        return moment().add(days, 'day').format(f);
    }
    public static convertToWebfeederDate(dt: any): string {
      if (!(typeof dt === 'string')) {
        const dtJs = new Date(dt.year, dt.month - 1, dt.day);
        const d = moment(dtJs).format('DDMMYYYY');
        return d;
      } else {
        return '';
      }
    }
    public static getMinutesFromBovespaStart() {
      const m = moment(moment(), 'HH:mm');
      m.set({h: 10, m: 0});
      const now = moment(moment(), 'HH:mm');
      const minutes = now.utc().diff(m.utc(), 'minutes') + 60;
      return minutes < 60 ? -1 : minutes;
    }
    public static getHoursFromBovespaStart() {
      const m = moment(moment(), 'HH:mm');
      m.set({h: 10, m: 0});
      const now = moment(moment(), 'HH:mm');
      return now.utc().diff(m.utc(), 'hours') + 1;
    }
    public static getHourBetweenDates(date: string, date1: string) {
      const moment1 = moment(date, moment.ISO_8601);
      const moment2 = moment(date1, moment.ISO_8601);
      const duration = moment.duration(moment1.diff(moment2));
      return new Date(0, 0, 0,
        duration.asHours(),
        duration.asMinutes(),
        duration.asSeconds(),
        duration.asMilliseconds());
    }
}
