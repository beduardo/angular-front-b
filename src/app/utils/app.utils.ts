import { WorkspaceNewService } from '@services/workspace-new.service';
import * as _s from 'underscore.string';
import { Constants } from '@utils/constants';
import { WorkspaceNewDto } from '@dto/workspacenew.dto';
import * as moment from 'moment';
export class AppUtils {
  public static languageSelected = 'pt-br';
  public static languageCurrency = 'BRL';
  public static decimalSeparatorSelected: string;
  public static createNewId(moduleName: string, complement: string = '') {
    let id = '';
    const date = new Date();
    id += moduleName + complement + date.getTime().toString();
    return id;
  }
  private static getNewGUIDString(): string {
    let d = new Date().getTime();
    if (window.performance && typeof window.performance.now === 'function') {
      d += performance.now();
    }
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, c => {
      const r = ((d + Math.random() * 16) % 16) | 0;
      d = Math.floor(d / 16);
      return (c === 'x' ? r : (r & 0x3) | 0x8).toString(16);
    });
  }
  public static isDigit(ch: string): boolean {
    return ch.charCodeAt(0) >= 48 && ch.charCodeAt(0) <= 57;
  }
  public static numberToFormattedNumber(
    n: number,
    decimalPlaces: number = 2,
    decimalSeparator = ',',
    thousandSeperator = '.'
  ): string {
    if (n !== undefined) {
      return _s.numberFormat(
        n,
        decimalPlaces,
        decimalSeparator,
        thousandSeperator
      );
    } else {
      return '0';
    }
  }
  public static convertFormattedNumberToNumber(n: any, decimal?: string) {
    const mark = decimal ? decimal : ',';
    return this.convertBrCurrencyToNumber(n, mark);
  }
  public static convertBrCurrencyToNumber(n: string, decimal: string) {
    const regex = new RegExp('[^0-9-' + decimal + ']', 'g');
    let unformatted = null;
    if (n) {
      unformatted = parseFloat(
        n
          .replace(/\((?=\d+)(.*)\)/, '-$1')
          .replace(regex, '')
          .replace(decimal, '.')
      );
    }
    return unformatted;
  }
  public static convertInteger(n: string): number {
    if (typeof n === 'number') {
      return n;
    }
    if (n) {
      n = n
        .replace(/\./g, '')
        .replace('.', '')
        .replace(',', '.')
        .replace('R$', '')
        .replace('Pts.', '');
      return parseInt(n);
    } else {
      return 0;
    }
  }
  public static convertNumber(n: string): number {
    if (typeof n === 'number') {
      return n;
    }
    if (n) {
      n = n
        .replace(/\./g, '')
        .replace('.', '')
        .replace(',', '.')
        .replace('R$', '')
        .replace('Pts.', '');
      return parseFloat(n);
    } else {
      return 0;
    }
  }
  public static numberToCurrency(
    n: number,
    decimalPlaces: number = 2,
    decimalSeparator = ',',
    thousandSeperator = '.'
  ): string {
    if (n === undefined) {
      n = 0;
    }
    n = Number(n);
    if (n !== undefined) {
      return _s.numberFormat(
        n,
        decimalPlaces,
        decimalSeparator,
        thousandSeperator
      );
    } else {
      return '0';
    }
  }
  public static formatNumber(
    value: any,
    decimalPlaces: number = 2,
    decimalSeparator: string = '.'
  ) {
    if (AppUtils.isNumber(value)) {
      const newValue: number = AppUtils.convertToNumber(
        value,
        decimalPlaces,
        ','
      );
      return newValue.toLocaleString(AppUtils.languageCurrency, {
        minimumFractionDigits: decimalPlaces
      });
    } else {
      return value;
    }
  }
  public static parseFloat(value) {
    return parseFloat(value);
  }
  public static isNaN(value) {
    return isNaN(value);
  }
  public static isWebfeederNumberValid(n: string) {
    if (n !== undefined && n !== '' && n !== '-') {
      return true;
    }
    return false;
  }
  public static convertToNumber(
    value: string,
    decimalPlaces: number = 2,
    decimalSeparator: string = '.'
  ) {
    let result = -1;
    let separator = '';
    if (value === '') {
      value = '0';
    }
    for (let _i = value.length - 1; _i >= 0; _i--) {
      const item = value[_i];
      if (!AppUtils.isDigit(item) && item !== '-') {
        separator = item;
        if (separator === ',') {
          separator = '.';
        }
        break;
      }
    }
    if (separator !== '') {
      value = value.toString().replace(separator, '@');
    }
    value = value
      .toString()
      .replace(/\./g, '')
      .replace(/\,/g, '')
      .replace('@', decimalSeparator);
    result = parseFloat(value);
    if (isNaN(result)) {
      result = parseFloat(value);
    }
    if (result === NaN) {
      result = 0;
    }
    return parseFloat(result.toFixed(decimalPlaces));
  }
  public static convertNumberToCurrency(
    value: number,
    decimalPlaces: number = 2,
    prefix: string = ''
  ): string {
    if (value === undefined) {
      return '';
    }
    if (prefix !== '') {
      return (
        prefix +
        value.toFixed(decimalPlaces).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')
      );
    } else {
      let currency = 'BRL';
      if (AppUtils.languageCurrency !== '') {
        currency = AppUtils.languageCurrency;
      }
      try {
        return value
          .toLocaleString(AppUtils.languageSelected, {
            style: 'currency',
            currency: currency,
            minimumFractionDigits: decimalPlaces
          })
          .replace('R$', '');
      } catch (e) {
        return (value + '');
      }
    }
  }
  public static ticksizeToStep(ticksize: number) {
    if (ticksize === 2) {
      return 0.01;
    } else if (ticksize === 0) {
      return 1;
    }
  }
  public static convertToNumber02(
    value: string,
    decimalPlaces: number = 2,
    decimalSeparator: string = '.'
  ) {
    let result = -1;
    let separator = '';
    if (value === '') {
      value = '0';
    }
    for (let _i = value.length - 1; _i >= 0; _i--) {
      const item = value[_i];
      if (!AppUtils.isDigit(item) && item !== '-') {
        separator = item;
        if (separator === ',') {
          value = value.toString().replace(separator, '@');
          separator = '.';
        } else {
          separator = '';
        }
        break;
      }
    }
    if (separator !== '') {
      value = value.toString().replace(separator, '@');
    }
    value = value
      .toString()
      .replace(/\./g, '')
      .replace(/\,/g, '')
      .replace('@', separator);
    result = parseFloat(value);
    if (isNaN(result)) {
      result = parseFloat(value);
    }
    if (result === NaN) {
      result = 0;
    }
    return parseFloat(Number(result.toPrecision(10)).toFixed(decimalPlaces));
  }
  public static getNumberOtimized(
    value: number,
    decimalPlaces: number = 2
  ): string {
    let valueDif = 1;
    let sufix = '';
    if (typeof value === 'string') {
      value = this.convertInteger(value);
    }

    if (value >= 1000 && value < 1000000 && value < 1000000000) {
      valueDif = 1000;
      sufix = 'K';
    } else if (value >= 1000000 && value < 1000000000) {
      valueDif = 1000000;
      sufix = 'M';
    } else if (value >= 1000000000) {
      valueDif = 1000000000;
      sufix = 'B';
    }
    let result: string;
    result =
      new Intl.NumberFormat(AppUtils.languageSelected).format(
        value / valueDif
      ).split(',')[0] + sufix;
    return result;
  }
  public static convertNumberToOptimizedNumber(
    volumeFinancier: number
  ): string {
    if (!volumeFinancier) {
      return '';
    }
    let volume = '';
    if (volumeFinancier > 1000000000) {
      volume = (volumeFinancier / 1000000000).toFixed(3) + 'B';
    } else if (volumeFinancier > 1000000) {
      volume = (volumeFinancier / 1000000).toFixed(3) + 'M';
    } else if (volumeFinancier > 1000) {
      volume = (volumeFinancier / 1000) + '';
    } else {
      volume = volumeFinancier + '';
    }
    return volume;
  }
  public static convertOptimizedNumberToNumber(value: string) {
    let finalValue = 0;
    if (value.indexOf('K') !== -1) {
      const newValue = value.split('K')[0];
      finalValue = Number(newValue) * 1000;
    } else if (value.indexOf('M') !== -1) {
      const newValue = value.split('M')[0];
      finalValue = Number(newValue) * 1000000;
    } else if (value.indexOf('B') !== -1) {
      const newValue = value.split('B')[0];
      finalValue = Number(newValue) * 1000000000;
    }
    return finalValue;
  }
  public static getDateLocaleFormat(value: Date): string {
    return value.toLocaleDateString([AppUtils.languageSelected, 'en-US']);
  }
  public static getDateTimeLocaleFormat(value: Date): string {
    return (
      value.toLocaleDateString([AppUtils.languageSelected, 'en-US']) +
      ' ' +
      value.toLocaleTimeString([AppUtils.languageSelected, 'en-US'])
    );
  }
  public static getLocaleDecimalSeparator(
    language: string = Constants.LANGUAGE_PORTUGUESE
  ) {
    const value = 1.1;
    return value.toLocaleString(language).substring(1, 2);
  }
  public static decimalPlaces(num) {
    const match = ('' + num).match(/(?:\.(\d+))?(?:[eE]([+-]?\d+))?$/);
    if (!match) {
      return 0;
    }
    return Math.max(
      0,
      (match[1] ? match[1].length : 0) - (match[2] ? +match[2] : 0)
    );
  }
  public static isNumber(value: string) {
    'use strict';
    value = value
      .toString()
      .replace(/\./g, '')
      .replace(',', '.');
    return !isNaN(parseFloat(value)) && isFinite(+value);
  }
  public static identifyUserCommand(keyCode: number) {
    return keyCode === 13 || keyCode === 9 || keyCode === 9;
  }
  public static getHours(dateFormat: string): string {
    const dateActual = new Date(dateFormat);
    const minutes = dateActual.getMinutes();
    const seconds = dateActual.getSeconds();
    return (
      dateActual.getHours() +
      ':' +
      (minutes <= 9 ? '0' + minutes : '' + minutes) +
      ':' +
      (seconds <= 9 ? '0' + seconds : '' + seconds)
    );
  }
  public static getStringFormat(value: string, args: string[]) {
    if (value && (args || args.length > 0)) {
      let result: string;
      result = value;
      for (let index = 0; index < args.length; index++) {
        const element = args[index];
        result = result.replace('{' + index + '}', element);
      }
      return result;
    }
    return value;
  }
  public static getImageSymbol(symbol: string) {
    if (symbol && symbol.length >= 4) {
      symbol = symbol.toUpperCase();
      return symbol.substr(0, 4);
    }
    return '';
  }
  public static getDate(dateFormat: string): string {
    return new Date(dateFormat).toLocaleDateString();
  }
  public static GetTickSize(
    symbol: string,
    tickSizeDefault: number = 2
  ): number {
    if (symbol) {
      if (symbol.indexOf('win') === 0 || symbol.indexOf('ind') === 0) {
        return 0;
      }
      if (symbol.indexOf('wdo') === 0 || symbol.indexOf('dol') === 0) {
        return 1;
      }
    }
    return tickSizeDefault;
  }
  public static getOptionsNotifications(): any {
    return {
      position: ['bottom', 'right'],
      timeOut: 3000,
      showProgressBar: false,
      lastOnBottom: true,
      clickToClose: true,
      clickIconToClose: true,
      pauseOnHover: true
    };
  }
  public static getCookie(name: string) {
    const ca: Array<string> = document.cookie.split(';');
    const caLen: number = ca.length;
    const cookieName = `${name}=`;
    let c: string;
    for (let i = 0; i < caLen; i += 1) {
      c = ca[i].replace(/^\s+/g, '');
      if (c.indexOf(cookieName) === 0) {
        return c.substring(cookieName.length, c.length);
      }
    }
    return '';
  }

  // TODO: Cookie to Session
  // public static deleteCookie(name) {
  //   this.setCookie(name, '', -1, '/');
  // }

  public static deleteCookie(name) {
    // this.setCookie(name, '', -1, '/');
    window.sessionStorage.removeItem(name);
  }

  public static setCookie(
    name: string,
    value: string,
    expireDays: number,
    path: string = ''
  ) {
    const d: Date = new Date();
    d.setTime(d.getTime() + expireDays * 24 * 60 * 60 * 1000);
    const expires = `expires=${d.toUTCString()}`;
    const cpath: string = path ? `; path=${path}` : '';
    document.cookie = `${name}=${value}; ${expires}${cpath}`;
  }
  public static trimStart(quote: string) {
    return quote
      .trim()
      .replace(' ', '')
      .toLowerCase();
  }
  public static sortArray(array: any, direction: string, column: string): any {
    let i, j: number;
    let itemAux;
    if (direction.toUpperCase() === 'ASC') {
      for (i = 0; i <= array.length - 1; i++) {
        for (j = i + 1; j <= array.length - 1; j++) {
          if (array[i][column] > array[j][column]) {
            itemAux = array[j];
            array[j] = array[i];
            array[i] = itemAux;
          }
        }
      }
    } else {
      for (i = array.length - 1; i >= 0; i--) {
        for (j = i - 1; j >= 0; j--) {
          if (array[i][column] > array[j][column]) {
            itemAux = array[j];
            array[j] = array[i];
            array[i] = itemAux;
          }
        }
      }
    }
    return array;
  }
  public static getDateNow(): string {
    const today = new Date();
    const dd = today.getDate();
    const mm = today.getMonth() + 1;
    const yy = today.getFullYear();
    return dd + '/' + mm + '/' + yy;
  }
  public static formatPriceHistoryOrder(value, quote, revertDecimalSymbol = true): string {

    if (value) {
      value = value.replace('Pts.', '').replace('R$', '');
    }

    if (revertDecimalSymbol) {
      value = value.replace('.', '');
      value = value.replace(',', '.');
    }

    return value;
  }
  public static getDateFormat(date: Date): string {
    const month = date.getMonth() + 1;
    return this.padStart(date.getDate().toString()) + '/' +
      this.padStart(month.toString()) + '/' +
      this.padStart(date.getFullYear().toString());
  }
  private static padStart(padString: string): string {
    if (padString.length < 2) {
      padString = '0' + padString;
    }
    return padString;
  }
  public static getParameterByName(name, url) {
    if (!url) {
      url = window.location.href;
    }
    name = name.replace(/[\[\]]/g, '\\$&');
    const regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)');
    const results = regex.exec(url);
    if (!results) { return ''; }
    if (!results[2]) { return ''; }
    return decodeURIComponent(results[2].replace(/\+/g, ' '));
  }
  public static getPageByName(name, url) {
    if (!url) {
      url = window.location.href;
    }
    if (url && url.indexOf(name) > 0) {
      return true;
    }
    return false;
  }
  public static removeDuplicates(myArr, prop) {
    return myArr.filter((obj, pos, arr) => {
      return arr.map(mapObj => mapObj[prop]).indexOf(obj[prop]) === pos;
    });
  }
  public static orderObjectDesc(object: any, param?: string) {
    const args = param;
    const order = 'desc';
    if (object !== undefined) {
      object.sort((a: any, b: any) => {
        if (order === 'desc') {
          if (a[args] > b[args]) {
            return -1;
          } else if (a[args] < b[args]) {
            return 1;
          } else {
            return 0;
          }
        }
      });
    }
  }
  public static orderObjectAsc(object: any, param?: string) {
    const args = param;
    const order = 'asc';
    if (object !== undefined) {
      object.sort((a: any, b: any) => {
        if (order === 'asc') {
          if (a[args] < b[args]) {
            return -1;
          } else if (a[args] > b[args]) {
            return 1;
          } else {
            return 0;
          }
        }
      });
    }
  }
  public static orderObjectAscNumberBillet(object: any, param?: string) {
    const args = param;
    const order = 'asc';
    if (object !== undefined) {
      object.sort((a: any, b: any) => {
        if (order === 'asc') {
          if (Number(a[args]) < Number(b[args])) {
            return -1;
          } else if (Number(a[args]) > Number(b[args])) {
            return 1;
          } else {
            return 0;
          }
        }
      });
    }
  }
  public static orderObjectAscNumber(object: any) {
    // const args = param;
    const order = 'asc';
    if (object !== undefined) {
      object.sort((a: any, b: any) => {
        if (order === 'asc') {
          if (Number(a) < Number(b)) {
            return -1;
          } else if (Number(a) > Number(b)) {
            return 1;
          } else {
            return 0;
          }
        }
      });
    }
  }
  public static convertToCurrencyString(value: string) {
    return AppUtils.convertNumberToCurrency(AppUtils.convertNumber(value));
  }

  public static resetCookieItem(name) {
    // if (Cookie.get(name)) {
    if (window.sessionStorage.getItem(name)) {
      this.deleteCookie(name);
    }
  }

  public static strToDateTime(strDate): Date {
    if (strDate && (typeof strDate === 'string') && (strDate.indexOf('/') >= 0 || strDate.indexOf('-'))) {
      const dateSplitted = strDate.indexOf('/') >= 0 ? strDate.split('/') : strDate.split('-');
      if (dateSplitted.length >= 3) {
        let timeStr = '';
        let timeSplitted = [];
        if (dateSplitted[2]) {
          timeStr = dateSplitted[2].split(/(\s+)/)[2];
          timeSplitted = dateSplitted[2].split(':');
          // timeSplitted = timeStr.split(':');
        }
        const year = dateSplitted[0].length === 4 ? parseInt(dateSplitted[0], 0) : parseInt(dateSplitted[2], 0);
        const month = parseInt(dateSplitted[1], 0) - 1;
        const day = dateSplitted[0].length === 4 ? parseInt(dateSplitted[2], 0) : parseInt(dateSplitted[0], 0);
        if (timeSplitted !== []) {
          const hour = timeSplitted[0];
          const min = timeSplitted[1];
          const sec = timeSplitted[2];
          return new Date(year, month, day, hour, min, sec);
        }
        return new Date(year, month, day, 0, 0, 0);
      }
    } else if (strDate instanceof Date) {
      return strDate as Date;
    }
    return new Date(0, 0, 0, 0, 0, 0);
  }

  public static dateDiff(dtInit: Date, dtFinal: Date, returnType: string = 'days') {
    const diff = dtFinal.getTime() - dtInit.getTime();
    if (returnType === 'seconds') {
      return Math.floor(diff / 1000);
    } else if (returnType === 'minutes') {
      return Math.floor(diff / 60000);
    } else if (returnType === 'hours') {
      return Math.floor(diff / 3600000);
    } else if (returnType === 'days') {
      return Math.floor(diff / 86400000);
    } else if (returnType === 'months') {
      return Math.floor(diff / 2419200000);
    } else {
      return new Date(diff);
    }
  }

  public static isEmptyStr(str) {
    return (!str || 0 === str.trim().length);
  }

  public static strToDate(strDate): Date {
    if (strDate && (typeof strDate === 'string') && (strDate.indexOf('/') >= 0 || strDate.indexOf('-'))) {
      const dateSplitted = strDate.indexOf('/') >= 0 ? strDate.split('/') : strDate.split('-');
      if (dateSplitted.length >= 3) {
        const year = dateSplitted[0].length === 4 ? parseInt(dateSplitted[0], 0) : parseInt(dateSplitted[2], 0);
        const month = parseInt(dateSplitted[1], 0) - 1;
        const day = dateSplitted[0].length === 4 ? parseInt(dateSplitted[2], 0) : parseInt(dateSplitted[0], 0);
        return new Date(year, month, day, 0, 0, 0);
      }
    } else if (strDate instanceof Date) {
      return strDate as Date;
    }
    return new Date(0, 0, 0, 0, 0, 0);
  }

  /**
   * Checks if a quote isn't in the current workspace.
   * @param quote Quote to be checked.
   * @param workspaceService WorkspaceNewService to get the current workspace.
   * @returns True -> If it isn't in the current workspace | False -> Otherwise.
   */
  public static checksUnsubscribeQuote(quote: string, workspaceService: WorkspaceNewService) {
    const userWorkspaceSelected: WorkspaceNewDto = workspaceService.getUserWorkspaceById(workspaceService.getWksSelectedId());
    const userWorkspaces = workspaceService.getUserWorkspaces();
    let canUnsubscribe = true;

    for (let i = 0; i < userWorkspaces.length; i++) {
      if (userWorkspaces[i].Id !== undefined && userWorkspaces[i].Id === userWorkspaceSelected.Id) {
        const workspace = JSON.parse(userWorkspaces[i].JsonData);
        for (let j = 0; j < workspace.content.length; j++) {
          if (workspace.content[j].symbol.toLowerCase() === quote.toLowerCase()) {
            canUnsubscribe = false;
            break;
          }
        }
      }
    }

    return canUnsubscribe;
  }

  public static newguidgen() {
    function id() {
      return Math.floor((1 + Math.random()) * 0x10000)
        .toString(16)
        .substring(1);
    }
    return id() + id() + '-' + id() + '-' + id() + '-' +
      id() + '-' + id() + id() + id();
  }

  public static validateDateRange(start, end, rangeInDays) {
    if (!start || !end)
      return false;
    const startDate = moment(start);
    const endDate = moment(end);
    const diff = endDate.diff(startDate, 'days');
    return diff > rangeInDays ? false : true;
  }

  public static validCharSystem(e) {
    if (e.key === '&' || e.key === '?' || e.key === '=' || e.key === '#' || e.key === '%') {
      return true;
    }
  }
}
