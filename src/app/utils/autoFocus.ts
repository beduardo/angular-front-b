import { Directive, HostListener, ElementRef, EventEmitter, Output } from '@angular/core';

@Directive({
  selector: '[autoBlur]'
})
export class AutoBlurDirective {
    @Output() onOutsideClick: EventEmitter<boolean>;
    private isOpen = false;
    constructor(private elementRef: ElementRef) {
        this.onOutsideClick = new EventEmitter();
    }

  @HostListener('document:click', ['$event'])
  private documentClicked(event: MouseEvent): void {
      if (!this.elementRef.nativeElement.contains(event.target) && this.isOpen) {
          this.onOutsideClick.emit(true);
          this.isOpen = false;
      }
      this.isOpen = true;
  }
}
