import { Constants } from '@utils/constants';

// deprecated
export class WorkspacesPreDefinedUtils {

    public static WKSPACE_PERSONALIZE = 'Personalize o seu';
    public static WKSPACE_PD_01_TITLE = 'Resumo de Mercado';
    public static WKSPACE_PD_02_TITLE = 'Day Trade';
    public static WKSPACE_PD_03_TITLE = 'Chart Trading';

    private static wksAssetSummary_pdcontent =
    {
        position: {
            x: 0,
            y: 0,
            cols: 2,
            rows: 1
        },
        symbol: 'petr4',
        company: '',
        type: Constants.WINDOW_NAME_ASSET_SUMMARY,
        workspace: 'HOME'
    };

    public static wksmarket_pdcontent =
    {
        position: {
            x: 0,
            y: 0,
            cols: 2,
            rows: 1
        },
        symbol: 'vale3',
        company: 'VALE ON NM',
        type: 'market',
        workspace: 'HOME'
    };

    public static wksquotechart_pdcontent =
    {
        position: {
            x: 0,
            y: 0,
            cols: 2,
            rows: 1
        },
        symbol: 'doln18',
        company: '',
        type: 'quote-chart',
        workspace: 'HOME'
    };

    public static wksbook_pdcontent =
    {
        position: {
            x: 0,
            y: 0,
            cols: 2,
            rows: 1
        },
        symbol: 'doln18',
        company: '',
        type: 'book',
        workspace: 'HOME'
    };

    public static WKSPACE_PD_01 = {
        Id: 1,
        Name: WorkspacesPreDefinedUtils.WKSPACE_PD_01_TITLE,
        title: WorkspacesPreDefinedUtils.WKSPACE_PD_01_TITLE,
        indexTab: 1,
        content: [
            WorkspacesPreDefinedUtils.wksAssetSummary_pdcontent,
            WorkspacesPreDefinedUtils.wksquotechart_pdcontent,
            WorkspacesPreDefinedUtils.wksbook_pdcontent,
        ],
        Active: true,
        IndexTab: 1,
        SwingTrade: 1,
        IsPredefined: true
    };

    public static WKSPACE_PD_02 = {
        Id: 2,
        Name: WorkspacesPreDefinedUtils.WKSPACE_PD_02_TITLE,
        title: WorkspacesPreDefinedUtils.WKSPACE_PD_02_TITLE,
        indexTab: 1,
        content: [
            WorkspacesPreDefinedUtils.wksAssetSummary_pdcontent,
            WorkspacesPreDefinedUtils.wksquotechart_pdcontent
        ],
        Active: true,
        IndexTab: 1,
        SwingTrade: 1,
        IsPredefined: true
    };

    public static WKSPACE_PD_03 = {
        Id: 3,
        Name: WorkspacesPreDefinedUtils.WKSPACE_PD_03_TITLE,
        title: WorkspacesPreDefinedUtils.WKSPACE_PD_03_TITLE,
        indexTab: 1,
        content: [
            WorkspacesPreDefinedUtils.wksAssetSummary_pdcontent,
            WorkspacesPreDefinedUtils.wksquotechart_pdcontent
        ],
        Active: true,
        IndexTab: 1,
        SwingTrade: 1,
        IsPredefined: true
    };
}
