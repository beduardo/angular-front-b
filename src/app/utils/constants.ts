import { HttpHeaders } from '@angular/common/http';

enum WORKSPACE_TYPE {
  Web = 1,
  Mobile = 2
}

enum ALERT_TYPE {
  success = 'success',
  info = 'info',
  error = 'error',
  warn = 'warn'
}

enum LOGIN_TYPE {
  LoginO = 'LoginO',
  LoginS = 'LoginS',
  LoginW = 'LoginW'
}

enum LAYOUT_TYPE {
  Dark = 'Dark',
  Light = 'Light',
  Custom = 'Custom'
}

enum TYPE_ORDER_TEXTS {
  Limited = 'Limitada',
  MarketProtected = 'Mercado com Proteção',
  Market = 'Mercado Limite'
}

export enum Profile {
  Client,
  Assessor
}

export class Constants {

  public static WHITELABEL = [
    {
      cod: 1, whitelabel_name: 'CEDRO', login_type: 'LoginS', api: 'API', systemName: 'Fast Trade',
      phone1: '+55 34 3239-0031', phone2: '', email: 'canais@cedrotech.com', urlHbWhitelabel: '',
      isRecoverEmail: true, isRecoverSupport: false, isRecoverWhatsapp: true
    },
    {
      cod: 2, whitelabel_name: 'PI', login_type: 'LoginO', api: 'API', systemName: '',
      phone1: '', phone2: '', email: '', urlHbWhitelabel: '',
      isRecoverEmail: false, isRecoverSupport: false, isRecoverWhatsapp: false
    },
    {
      cod: 3, whitelabel_name: 'RB', login_type: 'LoginW', api: 'WSO2', systemName: '',
      phone1: '', phone2: '', email: '', urlHbWhitelabel: '',
      isRecoverEmail: false, isRecoverSupport: false, isRecoverWhatsapp: false
    },
    {
      cod: 4, whitelabel_name: 'CEDRO', login_type: 'LoginW', api: 'WSO2', systemName: '',
      phone1: '+55 34 3239-0031', phone2: '', email: 'canais@cedrotech.com', urlHbWhitelabel: '',
      isRecoverEmail: true, isRecoverSupport: false, isRecoverWhatsapp: true
    },
    {
      cod: 5, whitelabel_name: 'UM', login_type: 'LoginW', api: 'WSO2', systemName: '',
      phone1: '', phone2: '', email: '', urlHbWhitelabel: '',
      isRecoverEmail: false, isRecoverSupport: false, isRecoverWhatsapp: false
    },
    {
      cod: 6, whitelabel_name: 'DAYCOVAL', login_type: 'LoginW', api: 'WSO2', systemName: '',
      phone1: '', phone2: '', email: '', urlHbWhitelabel: '',
      isRecoverEmail: false, isRecoverSupport: false, isRecoverWhatsapp: false
    },
    {
      cod: 7, whitelabel_name: 'FC STONE', login_type: 'LoginS', api: 'API', systemName: '',
      phone1: '+11 3509 5440', phone2: '0800 942 4685', email: '', urlHbWhitelabel: 'https://dtvm.intlfcstone.com.br/#modal',
      isRecoverEmail: false, isRecoverSupport: true, isRecoverWhatsapp: false
    },
    {
      cod: 8, whitelabel_name: 'TRINUS', login_type: 'LoginW', api: 'WSO2', systemName: '',
      phone1: '', phone2: '', email: '', urlHbWhitelabel: '',
      isRecoverEmail: false, isRecoverSupport: false, isRecoverWhatsapp: false
    },
    {
      cod: 9, whitelabel_name: 'VITREO', login_type: 'LoginS', api: 'API', systemName: '',
      phone1: '', phone2: '', email: '', urlHbWhitelabel: '',
      isRecoverEmail: false, isRecoverSupport: false, isRecoverWhatsapp: false
    },
    {
      cod: 10, whitelabel_name: 'DESAFIO DE INVESTIMENTOS', login_type: 'LoginS', api: 'API', systemName: 'Fast Trade',
      phone1: '+55 34 3239-0031', phone2: '', email: 'canais@cedrotech.com', urlHbWhitelabel: '',
      isRecoverEmail: true, isRecoverSupport: false, isRecoverWhatsapp: true
    }
  ];

  public static LOGIN_MSG_TITLE = 'Seja bem vindo!';
  public static LOGIN_MSG_SUBTITLE = 'O Home Broker ágil e prático para realizar consultas e operações no mercado financeiro.';
  public static LOGIN_MSG_TITLE_RANKING = 'Desafio de Investimentos';
  // tslint:disable-next-line:max-line-length
  public static LOGIN_MSG_SUBTITLE_RANKING = 'Bem-vindo ao primeiro Desafio de Investimentos da Bolsa de Valores (B3), realizado através de uma parceria do Fast Trade, Levante Ideias de Investimentos e Guide Investimentos.';
  public static LOGIN_MSG_TITLE_AUTHENTICATION = 'Dupla Autenticação!';
  // tslint:disable-next-line:max-line-length
  public static LOGIN_MSG_SUBTITLE_AUTHENTICATION = 'O Home Broker ágil e prático para realizar consultas e operações no mercado financeiro.';

  public static DEFAULT_SOURCE_ADRESS_WSO2_CLIENT = 'Cliente';
  public static DEFAULT_SOURCE_ADRESS_WSO2_ASSESSOR = 'Assessor';
  public static DEFAULT_SOFTWARE_NAME_RANKING = 'Desafio';
  public static DEFAULT_SOFTWARE_NAME = 'FTWEB_BROKER';

  public static DATE_UPDATE_VOLUME = 'HH:mm:ss';
  public static DATE2_DDMMYYYY = 'ddMMyyyy';
  public static DATE_DDMMYYYY = 'dd-MM-yyyy';
  public static DATE_NEWS_YYYYMMDD = 'yyyy-MM-dd';
  public static DATE_NEWS_BEFORE = 'T23:59:59';
  public static DATE_NEWS_AFTER = 'T00:00:00';
  public static DAY_MINUTE_IN_MILISECONDS = (1000 * 60 * 60 * 24);

  public static SIDEBAR_HELP_TITLE = 'Precisa de ajuda?';
  public static SIDEBAR_HELP_TITLE_RANKING = 'Apoio';
  public static SIDEBAR_HELP_PHONE1 = '+55 11 3014-0200';
  public static SIDEBAR_HELP_PHONE2 = '+55 34 3239-0000';
  public static SIDEBAR_HELP_WHATSAPP = '+55 34 3239-0031';
  public static SIDEBAR_HELP_WHATSAPP_LINK = 'http://api.whatsapp.com/send?phone=';
  public static SIDEBAR_HELP_EMAIL = 'canais@cedrotech.com';

  public static ID_SIDE_MENU_ORDER = 2;

  public static LINK_DOWNLOAD_FTW = `https://promo.fastmarkets.com.br/download-plataforma-fast-trade?utm_campaign=
  ftweb&utm_medium=web&utm_source=ftweb`;

  public static CREATE_NEW_ACCOUNT_API = 'http://fast.cedrotech.com/FastCloudWebapp/';
  public static CREATE_NEW_ACCOUNT_CEDRO = 'http://cadastrodigital.cedrotech.com/cadastro';
  public static CREATE_NEW_ACCOUNT_RB = 'https://uat.rbinvest.com.br/cadastro';

  public static testAccount = '789789';
  public static TYPE_ORDER_TEXTS = TYPE_ORDER_TEXTS;
  public static LOGIN_TYPE = LOGIN_TYPE;
  public static ALERT_TYPE = ALERT_TYPE;
  public static LAYOUT_TYPE = LAYOUT_TYPE;
  public static LAYOUT_THEME_DARK = 'LAYOUT_THEME_DARK';
  public static COOKIE_USER = 'UL';
  public static COOKIE_TOKEN_WEBAPI = 'TKNAPI';
  public static COOKIE_TOKEN_WEBFEEDER = 'TKNWF';
  public static COOKIE_TOKEN_WEBFEEDER_GRAPHIC = 'TKNWF_GRAPHIC';
  public static COOKIE_TOKEN_WEBFEEDER_NEGOTIATION = 'TKNWF_NEGOT';
  public static COOKIE_HB_ACCOUNT = 'HBA';
  public static COOKIE_HB_ACCOUNT_ID = 'HBA_ID';
  public static COOKIE_HB_ASSESSOR_ID = 'HBA_ASSESSOR_ID';
  public static COOKIE_HB_USER_ID = 'HBA_USER_ID';
  public static COOKIE_HB_ACCOUNT_NAME = 'HBA_USERNAME';
  public static COOKIE_HB_ACCOUNT_EMAIL = 'HBA_EMAIL';
  public static COOKIE_HB_CURRENTUSER = 'HBA_CURRENT_USER';
  public static COOKIE_HB_ABA = 'aba0';
  public static COOKIE_MESSAGE = 'MSG';
  public static COOKIE_TOKEN_EXPIRATION = 'TKNEXP';
  public static COOKIE_GUID_USER = 'GUID_USER';
  public static COOKIE_USER_LOGIN_TYPE = 'USER_LOGIN_TYPE';

  public static COOKIE_IS_ASSESSOR = 'IS_ASSESSOR';
  public static COOKIE_SYSTEM_ACCOUNT_NAME = 'SYSTEM_USERNAME';
  public static COOKIE_ASSESSOR_LIST_CLIENT = 'ASSESSOR_LIST_CLIENT';
  public static COOKIE_ASSESSOR_CLIENT_SELECTED = 'ASSESSOR_CLIENT_SELECTED';
  public static COOKIE_CLIENT_PROFILE_NAME = 'CLIENT_PROFILE_NAME';

  public static COOKIE_TOKEN_REFRESH_WEBAPI = 'TKNAPIREFRESH'; // Cedro Accounts
  public static COOKIE_SCOPE = 'scope';                        // Cedro Accounts
  public static COOKIE_TOKEN_TP = 'token_type';                // Cedro Accounts
  public static COOKIE_TOKEN_LAST_REFRESHED = 'lastRefreshed'; // Cedro Accounts

  public static LOGIN_O = 'LoginO';
  public static LOGIN_S = 'LoginS';
  public static LOGIN_W = 'LoginW';
  public static WKS_USER_PROFILE = 'userperf01';

  public static LOCAL_TRANSITION = 'LOCAL_TRANSITION';
  public static STORAGE_AUTH = 'USER_AUTH';
  public static LOCAL_STORAGE_AUTH = 'LOCAL_AUTH';
  public static LOCAL_STORAGE_COMMON = 'LOCAL_COMMON';
  public static LOCAL_STORAGE_TRANSITION = 'LOCAL_TRANSITION';
  public static LOCAL_STORAGE_USER_PREFERENCES = 'LOCAL_USER_PREFERENCES';

  public static COOKIE_HB_LOGIN_TYPE_ENABLED = 'HB_LOGIN_TYPE_ENABLED';
  public static COOKIE_HB_BROKER_ACCOUNT_SELECTED = 'HB_BROKER_SELECT';
  public static COOKIE_HB_BROKER_ACCOUNT_LIST = 'HB_BROKER_ACCOUNT_LIST';
  public static COOKIE_HB_BROKER_ACCOUNT = 'HB_BROKER';
  public static COOKIE_HB_LOGIN_TYPE = 'HB_LOGIN_TYPE';
  public static COOKIE_HB_TYPE_ACCESSOR = '1'; // 1 = Assessor
  public static COOKIE_HB_TYPE_BROKER = '2'; // 2 = Corretora
  public static COOKIE_HB_TYPE_OFFILINE = '3'; // 3 = Não conectado
  public static COOKIE_HB_TYPE_CEDRO = '4'; // 4 = Cedro (WF)
  public static COOKIE_HB_TYPE_SIMULATOR = '5'; // 5 = Simulador (WF)
  public static BROKER_MSG002 = 'Ocorreu um erro durante a operação, verifique as informações fornecidas e tente novamente.' +
    ' Caso o problema persista, entre em contato conosco.';
  public static BROKER_MSG004 = 'As informações inseridas estão incorretas.';
  public static BROKER_MSG005 = 'Usuário autenticado com sucesso no simulador.';

  public static CLOSED_MARKET_MESSAGE = 'Olá, no momento o mercado está fechado.';
  public static EMPTY_MESSAGE = 'Não há dados do ativo.';

  public static COOKIE_CURRENT_BROKER = 'BRK';
  public static COOKIE_IP = 'IP';
  public static COOKIE_TOKEN_BROKER_WEBFEEDER = 'TKCBRK';
  public static COOKIE_TOKEN_BROKER_EXPIRATION = 'TKNBRKEXP';

  public static COOKIE_BILLET_CLOSED_MODAL = 'BILLET_LAST_CONFIG';

  public static VALUE_NUMBER_NULL = -1;
  public static RESOLUTION_MINIMUM_HEIGHT = 768;
  public static RESOLUTION_MINIMUM_WIDTH = 1024;

  public static WINDOW_NAME_MAIN_WINDOW = 'main-window';
  public static WINDOW_NAME_ASSET_SUMMARY = 'asset-summary';
  public static WINDOW_NAME_BOOK = 'book';
  public static WINDOW_NAME_WORK_SHEET = 'worksheet';
  public static WINDOW_NAME_NEWS_SEARCH = 'news-search';
  public static WINDOW_NAME_MARKET_HIGHLIGHTS = 'market';
  public static WINDOW_NAME_QUOTE_BOX = 'quoted-box';
  public static WINDOW_NAME_TIMES_TRADES = 'times-trades';
  public static WINDOW_NAME_BILLET_DAY_TRADE = 'billet-day-trade';
  public static WINDOW_NAME_POSITION_DAY = 'position-day';
  public static WINDOW_NAME_QUOTE_CHART = 'quote-chart';
  public static WINDOW_NAME_VOLUME_AT_PRICE = 'volume-at-price';

  public static WINDOW_NAME_SELECTOR_INPUT = 'selector_input';
  public static WINDOW_NAME_SELECTOR_SELECT = 'selector_select';
  public static WINDOW_NAME_EDIT_ORDER = 'edit_order';
  public static WINDOW_NAME_QUICK_SEARCH_AUTO_COMPLETE = 'quick_search_auto_complete';
  public static WINDOW_NAME_SIMULATANEOS_STOP_ORDER = 'simulataneos_stop_order';
  public static WINDOW_NAME_STOP_GAIN_BUY_ORDER = 'stop_gain_buy_order';

  public static SIGNATURE_ELETRONIC = 'SIGNATURE';

  // custom Workspace creation, Screen Card Titles
  public static TITLE_CARD_BOOK = 'LIVRO DE OFERTAS';
  public static TITLE_CARD_WORKSHEET = 'PLANILHA DE COTAÇÃO';
  public static TITLE_CARD_VOLUME_AT_PRICE = 'VOLUME POR PREÇO';
  public static TITLE_CARD_TIMES_TRADES = 'NEGÓCIOS REALIZADOS';
  public static TITLE_CARD_ASSET_SUMMARY = 'RESUMO DO ATIVO';
  public static TITLE_CARD_QUOTE_CHART = 'GRÁFICOS';
  public static TITLE_CARD_BILLET_DAY_TRADE = 'BOLETA DE COMPRA/VENDA';

  // direction: 1 = compra / 2 = venda
  public static ORDER_DIRECTION_BUY = 1;
  public static ORDER_DIRECTION_SELL = 2;
  // typeOrder: 1 = boleta / 2 daytrade
  public static OPEN_POSITION_TRADE = 'POSITION';
  public static OPEN_DAY_TRADE = 'DAYTRADE';

  // action: 1 = New, 2=Edit, 3= Reenvio
  public static ACTION_NEW = 1;
  public static ACTION_EDIT = 2;
  public static ACTION_RESEND = 3;

  public static MARKET_BOVESPA = 'XBSP';
  public static MARKET_BMF = 'XBMF';
  public static MARKET_CODE_BOVESPA = 1;
  public static MARKET_CODE_BMF = 3;
  public static ORDER_SIDE_BUY = 'BUY';
  public static ORDER_SIDE_SELL = 'SELL';

  public static MARKET_NAME_BOVESPA = 'Bovespa';
  public static MARKET_NAME_BMF = 'BM&F';

  public static ORDER_TYPE_MARKET = 'MARKET';
  public static ORDER_TYPE_LIMITED = 'LIMITED';
  public static ORDER_TYPE_START = 'START';
  public static ORDER_TYPE_STOP = 'STOP';
  public static ORDER_TYPE_ON_CLOSE = 'ONCLOSE';
  public static ORDER_TYPE_PROTECTED_MARKET = 'MARKETPROTECTED';

  public static LANGUAGE_PORTUGUESE = 'pt-br';
  public static SYMBOL_PORTUGUESE = 'BRL';

  public static INTERVAL_VOLUME_AT_PRICE = 60000;
  public static QTD_NEWS = 100;

  public static CODE_ORDER_TYPE_NORMAL = '1';
  public static CODE_ORDER_TYPE_STOP = '2';

  public static TRADINGVIEW_DEFAULT_TYPE = 1;

  public static ORDER_TYPES = [
    { cod: '1', desc: 'Normal', webfeeder: 'Limited' }
  ];
  public static TIME_IN_FORCES = [
    { cod: 1, desc: 'Hoje', webfeeder: 'DAY' },
    { cod: 3, desc: 'Até cancelar (VAC)', webfeeder: 'GTC' },
    { cod: 2, desc: 'Até a Data', webfeeder: 'GTD' },
    { cod: 4, desc: 'Executa/Cancela', webfeeder: 'IOC' },
    { cod: 5, desc: 'Tudo/Nada', webfeeder: 'FOK' },
    { cod: 6, desc: 'No Fechamento', webfeeder: 'ATC' },
    { cod: 7, desc: 'No Leilão', webfeeder: 'GFA' }
  ];

  public static ORDER_HISTORY_MARKETS = [
    { cod: 1, desc: 'Bovespa', webfeeder: 'XBSP' },
    { cod: 2, desc: 'BM&F', webfeeder: 'XBMF' },
  ];

  public static ORDER_HISTORY_DIRECTIONS = [
    { cod: 1, desc: 'Compra', webfeeder: 'BUY' },
    { cod: 2, desc: 'Venda', webfeeder: 'SELL' },
  ];

  public static ORDER_HISTORY_STATUS = [
    { cod: 1, desc: 'Todas', webfeeder: '', geric: '' },
    {
      cod: 2, desc: 'Abertas', webfeeder: 'Abertas', generic: [
        'Nova',
        'Parcialmente Executada',
        'Editada',
        'Cancelamento Pendente',
        'Pendente',
        'Edição Pendente',
        'Suspensa',
        'Aberta'
      ]
    },
    {
      cod: 3, desc: 'Editaveis', webfeeder: 'Editaveis', generic: [
        'Nova',
        'Parcialmente Executada',
        'Aberta editada',
        'Editada',
        'Cancelamento Pendente',
        'Pendente',
        'Edição Pendente',
        'Suspensa',
        'Aberta'
      ]
    },
  ];

  public static NEWS_COMPANIES_URL = [
    { cod: 0, name: 'Fast Markets', url: './assets/images/assets/logo-fastmarkets.png' },
    { cod: 1, name: 'Cedro News', url: './assets/images/assets/logo-cedro.png' },
    { cod: 2, name: 'Agencia Bovespa', url: './assets/images/assets/logo-b3.png' },
    { cod: 3, name: 'Agencia BMF', url: './assets/images/assets/logo-b3.png' }
  ];

  public static IMG_NOTEBOOK_URL = './assets/images/assets/laptop.png';

  public static BTG_TRADE = 'BTG Trade';
  public static PRICE_TYPES = [
    { cod: false, desc: 'Limitado' },
    { cod: true, desc: 'Mercado' }
  ];

  public static MODAL_RECONNECT_MSG01 = 'Ops! Não há conexão com a internet.  Verifique sua conexão.';

  public static MODAL_RECONNECT_TYPE01 = 'no-network';

  public static MODAL_SIDE_LEFT_HISTORY = 'history';
  public static MODAL_SIDE_LEFT_GRAPH = 'graph';
  public static MODAL_SIDE_LEFT_NEWS = 'news';
  public static MODAL_SIDE_LEFT_EXTRACT = 'extract';
  public static MODAL_SIDE_LEFT_BROKERAGE = 'brokerage';
  public static MODAL_SIDE_LEFT_RANKING = 'brokerage';
  public static MODAL_SIDE_LEFT_EDUCATIONAL = 'brokerage';
  public static MODAL_SIDE_LEFT_CUSTODY = 'custody';
  public static MODAL_SIDE_LEFT_BALANCE = 'balance';
  public static MODAL_SIDE_LEFT_ASSESSORDATA = 'assessorData';
  public static MODAL_SIDE_LEFT_ASSESSOR_SEARCH = 'assessorSearchClient';

  public static LABEL_ERROR_ORDER_DEFAULT = 'ORDENS NÃO ENCONTRADAS PARA OS FILTROS INFORMADOS!';
  public static COOKIE_HOUR_TOKEN_WS = '10';

  // MSG Status OMS e Difusion
  public static MSG_STATUS_OFFLINE_OMS = 'Atenção! O serviço de NEGOCIAÇÃO está OFFLINE ou INDISPONÍVEL no momento.';

  // Messages to feature Login
  public static MSG_LOGIN_MSG001 = 'Todos os campos devem ser preenchidos.';
  public static MSG_LOGIN_MSG002 = 'Usuário ou senha incorretos.';
  public static MSG_LOGIN_MSG003 = 'Ocorreu um erro durante a operação, verifique as informações fornecidas e tente novamente.\n' +
    'Caso o problema persista, entre em contato conosco.';
  public static MSG_LOGIN_MSG004 = 'Conta autenticada em outro local.';
  public static MSG_LOGIN_MSG005 = 'Este é seu primeiro login, altere sua senha para continuar.';
  public static MSG_LOGIN_MSG006 = 'Sua senha foi expirada, altere sua senha para continuar.';
  public static MSG_LOGIN_MSG007 = 'Caractere inválido.';
  public static MSG_LOGIN_MSG008 = 'Não foi possível alterar a senha, verifique as informações fornecidas e tente novamente.';
  public static MSG_RECOVER_PASSWORD = 'Sua senha foi redefinida com sucesso.';

  // messages to forgot password
  public static FORGOT_TYPE_01 = 1;
  public static FORGOT_TYPE_02 = 2;
  public static MSG01_FORGOT_TITLE = 'Redefinir senha';
  public static MSG01_FORGOT_CONTENT = 'Digite seu endereço de e-mail abaixo e nós o colocaremos de volta nos trilhos.';
  public static MSG02_FORGOT_TITLE_FALSE = 'Código de redefinição não enviado';
  public static MSG02_FORGOT_TITLE = 'Código de redefinição enviado';
  public static MSG02_FORGOT_CONTENT = `Se houver uma conta  vinculada a este endereço de e-mail, enviaremos
   instruções para redefinir sua senha.`;
  public static MSG03_FORGOT_ERROR = 'Nova senha está diferente da confirmação da senha. Tente novamente!';
  public static MSG05_FORGOT_ERROR = 'E-mail Inválido. Por favor, informe o e-mail correto.';
  public static MSG06_FORGOT_ERROR = 'A senha não atende as regras especificadas. Por favor, digite uma nova senha.';
  public static MSG07_FORGOT_ERROR = 'Token inválido!';
  public static MSG08_FORGOT_ERROR = 'Erro ao localizar Corretora. Caso o problema persista, entre em contato com o nosso time de atendimento';
  public static MSG08_FORGOT_ERROR_TITLE = 'Ocorreu um erro durante a operação.';
  public static MSG08_FORGOT_ERROR_CONTENT = `Verifique as informações fornecidas e tente novamente.
  Caso o problema persista, entre em contato com o nosso time de atendimento:`;

  // messages to signature
  public static MSG01_FORGOT_SIGNATURE_TITLE = 'Recuperar assinatura eletrônica';
  public static MSG01_FORGOT_SIGNATURE_CONTENT = 'Uma mensagem com uma nova assinatura eletrônica vai ser enviada para e-mail cadastrado..';

  public static MSG01_FORGOT_SIGNATURE_TITLE_ERROR = 'Ocorreu um erro durante a operação.';
  public static MSG01_FORGOT_SIGNATURE_CONTENT_ERROR = 'Verifique as informações fornecidas e tente novamente. Caso o problema persista, '
    + 'entre em contato com o nosso time de atendimento:';
  public static MSG01_FORGOT_SIGNATURE_TITLE_CONFIRM = 'E-mail enviado com sucesso!';
  public static MSG01_FORGOT_SIGNATURE_CONTENT_CONFIRM = 'Um e-mail com link para recuperação de nova assinatura eletrônica foi enviado '
    + 'para seu e-mail! Caso não veja em sua caixa de entrada tente localiza-la em seu lixo eletrônico';
  public static MSG01_SIGNATURE_ERROR = 'Nova Assinatura Eletrônica está diferente da confirmação da Assinatura Eletrônica.'
    + ' Tente novamente!';
  public static MSG02_SIGNATURE_ERROR = 'As assinaturas antiga e nova devem ser diferentes.';
  public static MSG03_SIGNATURE_ERROR = 'A assinatura eletrônica deve conter apenas números.';
  public static MSG04_SIGNATURE_ERROR = 'Email inválido ou incorreto.';

  // incremental-input
  public static CURRENCY_BRAZILIAN_SYMBOL = 'R$ ';
  public static CURRENCY_USA_SYMBOL = 'U$ ';
  public static CURRENCY_EUR_SYMBOL = '€ ';
  public static THOUSANDS_SEPARATOR = '.';
  public static DECIMALS_SEPARATOR = ',';

  // Messages and others informations to the feature Manage Workspace
  public static WORKSPACE_TYPE = WORKSPACE_TYPE;
  public static NUM_MAX_WORKSPACES = 4;
  public static NUM_MIN_WORKSPACES = 1;
  public static NUM_MAX_WORKSPACE_CARDS = 10; // especificado 20
  public static NUM_MIN_WORKSPACE_CARDS = 1;
  public static WORKSPACE_MSG001 = 'Deve existir pelo menos uma área de trabalho.';
  public static WORKSPACE_MSG002 = 'Já existe um workspace com esse nome, por favor escolha um nome diferente.';
  public static WORKSPACE_MSG003 = 'Tem certeza que deseja remover a área de trabalho?';
  public static WORKSPACE_MSG004 = 'Tem certeza que deseja remover o componente da área de trabalho?';
  public static WORKSPACE_MSG005 = 'É preciso selecionar um ativo.';
  public static WORKSPACE_MSG006 = 'É preciso selecionar pelo menos um ativo ou uma planilha pré-definida.';
  public static WORKSPACE_MSG007 = 'O ativo selecionado não é um ativo válido.';
  public static WORKSPACE_MSG008 = 'Tem certeza que deseja salvar todas as áreas de trabalho criadas?';
  public static WORKSPACE_MSG009 = 'Usuário desconectado por inatividade.';
  public static WORKSPACE_MSG010 = 'Tentando estabelecer conexão...';
  public static WORKSPACE_MSG011 = `Limite máximo de componentes atingido para a área de trabalho atual,
  remova algum componente ou utilize outra área de trabalho.`;
  public static WORKSPACE_MSG012 = `Limite máximo de planilhas de cotação atingido para a área de trabalho atual,
  remova alguma planilha ou utilize outra área de trabalho.`;
  public static WORKSPACE_MSG013 = 'Nome inválido'; // TODO pega frase correta
  public static WORKSPACE_MSG014 = 'Já existe esse componente com o ativo selecionado no workspace atual.'; // TODO pega frase correta
  public static WORKSPACE_MSG015 = 'O ativo selecionado já está em execução. Por favor, selecione outro ativo.';
  public static WORKSPACE_MSG016 = 'Tem certeza que deseja voltar para sua área de trabalho?';
  public static WORKSPACE_MSG017 = 'O ativo selecionado já existe na planilha.';
  public static WORKSPACE_MSG018 = 'Não há dados para serem exibidos';
  public static WORKSPACE_MSG019 = 'É preciso informar pelo menos um caractere.';
  public static WORKSPACE_MSG020 = 'Mercado do ativo não encontrado.';
  public static WORKSPACE_MSG021 = 'Conta não encontrado ou inválida.';
  public static WORKSPACE_MSG022 = 'É preciso selecionar pelo menos uma área de trabalho ou personalizar uma.';
  public static WORKSPACE_MSG023 = 'Você deve aceitar o TERMO DE CIÊNCIA antes de enviar a ordem.';
  public static WKS_TITLE_PLACEHOLDER = 'CRIE UMA ÁREA DE TRABALHO';
  public static WKS_NEW_COMPOENENT = 'Tem certeza que deseja adicionar o componente a área de trabalho?'; // Provisório
  public static WKS_WORKSPACE_WORKSHEET = `Não é possível adicionar mais colunas na planilha, remova alguma
   coluna para que seja possível adicionar outra.`;
  public static WORKSHEET_MAX_ASSETS_MSG = `Não é possível adicionar mais ativos na planilha,
   remova algum ativo para que seja possível adicionar outro.`;
  public static WKS_WORKSPACE_CONF_MODAL_INFO1 = `Investidor, vamos começar? Selecione algumas das três telas já
   definidas com ajuda de nossos traders ou crie e`;
  public static WKS_WORKSPACE_CONF_MODAL_INFO2 = ` personalize sua própria área de trabalho. Defina suas estratégias, e bons negócios!`;

  // Default value to "tickSize" if doesn't exists
  public static BILLET_TICKSIZE_DEFAULT = 2;

  // Messages and others informations to the feature Manage Billet
  public static BILLET_ORDER_MSG001 = 'O ativo selecionado não é um ativo válido.';
  public static BILLET_ORDER_MSG002 = 'A quantidade deve ser maior que zero.';
  public static BILLET_ORDER_MSG005 = 'Todos os campos obrigatórios devem ser preenchidos.';
  public static BILLET_ORDER_GAIN = 'STOP GAIN';
  public static BILLET_ORDER_LOSS = 'STOP LOSS';
  public static BILLET_ORDER_1_TRIGGER_BUY = 'Preço de disparo de loss deve ser maior ou igual que o preço do último negócio.';
  public static BILLET_ORDER_1_TRIGGER_SELL = 'Preço de disparo de loss deve ser menor ou igual que o preço do último negócio.';
  public static BILLET_ORDER_2_TRIGGER_BUY = `Preço de disparo deve gain ser menor ou igual que o preço do último negócio.`;
  public static BILLET_ORDER_2_TRIGGER_SELL = `Preço de disparo deve gain ser maior ou igual que o preço do último negócio.`;
  public static BILLET_ORDER_1_LIMIT_BUY = 'Preço limite de loss deve ser maior ou igual ao preço de disparo de loss.';
  public static BILLET_ORDER_1_LIMIT_SELL = 'Preço limite de loss deve ser menor ou igual ao preço de disparo de loss.';
  public static BILLET_ORDER_2_LIMIT_BUY = 'Preço limite de gain deve ser maior ou igual ao preço de disparo de gain.';
  public static BILLET_ORDER_2_LIMIT_SELL = 'Preço limite de gain deve ser menor ou igual ao preço de disparo de gain.';
  public static BILLET_ORDER_EMPTY_SIGNATURE = 'Por favor informe sua Assinatura Eletrônica.';
  public static BILLET_ORDER_VALID_SIGNATURE = 'Assinatura Eletrônica inválida!';

  public static ORDER_HISTORY_MSG001 = 'A quantidade deve ser maior que zero.';
  public static ORDER_HISTORY_MSG002 = 'Tem certeza que deseja cancelar a ordem?';
  public static ORDER_HISTORY_MSG003 = 'Tem certeza que deseja atualizar a ordem?';
  public static ORDER_HISTORY_MSG004 = 'Tem certeza que deseja realizar reenvio da ordem?';

  public static MARKET_OPEN_CLOSED_MSG001 = 'Olá, no momento o mercado está fechado.';

  // Please, don't remove the 'Enters' of this messages, they are needed to tell the tooltips to break the lines
  public static BILLET_ORDER_STOP_BUY_LOSS = `• Preço de disparo deve ser maior que o preço do último negócio.

• Preço de limite deve ser maior ou igual ao preço de disparo.`;
  public static BILLET_ORDER_STOP_BUY_GAIN = `• Preço de disparo deve ser menor ou igual que o preço do último negócio.

• Preço limite deve ser maior ou igual ao preço de disparo.`;
  public static BILLET_ORDER_STOP_SELL_LOSS = `• Preço de disparo deve ser menor ou igual que o preço do último negócio.

• Preço limite deve ser menor ou igual ao preço de disparo.`;
  public static BILLET_ORDER_STOP_SELL_GAIN = `• Preço de disparo deve ser maior que o preço do último negócio.

• Preço limite deve ser menor ou igual ao preço de disparo.`;
  public static BILLET_ORDER_STOP_BUY_SIM = `• Preço de disparo de loss deve ser maior que o preço do último negócio.

• Preço limite de loss deve ser maior ou igual ao preço de disparo de loss.

• Preço de disparo de gain deve ser menor que o preço do último negócio.

• Preço limite de gain deve ser maior ou igual ao preço de disparo de gain.`;
  public static BILLET_ORDER_STOP_SELL_SIM = `• Preço de disparo de loss deve ser menor que o preço do último negócio.

• Preço limite de loss deve ser menor ou igual ao preço de disparo de loss.

• Preço de disparo de gain deve ser maior que o preço do último negócio.

• Preço limite de gain deve ser menor ou igual ao preço de disparo de gain.`;

  // Messages and others informations for the Billet situations

  // Messages and other informations to the Order history
  public static DATE_MSG001 = 'A data início deve ser menor ou igual a data final.';
  public static DATE_MSG002 = 'A data final deve ser menor ou igual a data atual.';
  public static DATE_MSG003 = 'Não é possível filtrar com apenas uma das datas preenchidas.';
  public static DATE_MSG004 = 'Notícia não encontrada.';
  public static DATE_MSG005 = 'Não é possível filtrar com intervalo maior que {{range}} dias.';

  // Gridster items default positions configuration
  // Consult \src\app\..\workspace-new\gridsteroptions.txt
  public static GRID_MAX_COLS = 5;
  public static GRID_MIN_COLS = 5;
  public static GRID_MAX_ROWS = 25;
  public static GRID_MIN_ROWS = 25;
  public static GRID_DRAGGABLE = true;
  public static GRID_SET_SIZE = true;
  public static GRID_PUSH_ITEMS = true;
  public static GRID_OUTER_MARGIN = true;
  public static GRID_RESIZABLE = true;
  public static GRID_MAX_ITEM_AREA = 10;
  public static GRID_SWAP = true;
  public static GRID_PUSH_ON_DRAG = true;
  public static GRID_COL_WIDTH = 375;
  public static GRID_ROW_HEIGHT = 245;
  public static MAX_WKS_CONF = 4;
  public static GRID_WINDOW_RESIZE = true;
  public static GRID_MARGIN = 15;
  public static GRID_OUTER_MARGIN_SIZE = 25;
  public static GRID_COMPACT = 'compactUp&Left';

  public static SM_WINDOW_RES = 1366;
  public static MD_WINDOW_RES = 1755;
  public static LG_WINDOW_RES = 2145;

  // Variables to pass on PanelClass
  public static NORMAL_MODAL = 'mat-modal-normal';
  public static FULL_BOTTOMSHEET = 'full-width-bottomsheet';
  public static FULL_MODAL = 'mat-modal-fullheight';
  public static CENTER_MODAL = 'mat-modal-centered';

  public static ESC_KEY_CODE = 27;

  public static WF_SERVICE_ERROR_CODE = 100;
  public static WF_SERVICE_LOGIN_ERROR_CODE = 111;

  public static MSG_MIN_RESOLUTION = 'A resolução minima para funcionamento do Fast Trade Web Desktop é de 1280x768, para acessar '
    + 'o sistema em resoluções menores faça o download da nossa aplicação móvel na Google Play ou Apple Store.';
  public static MIN_RESOLUTION = 1280;

  // Config variables to Simple Notifications
  public static SIMPLE_NOTIFICATIONS_TIMEOUT = 3000;
  public static SIMPLE_NOTIFICATIONS_PROGRESS = true;

  public static RECONNECT_ANIMATION_URL = 'assets/images/animations/reconnect.json';
  public static RECONNECTED_ANIMATION_URL = 'assets/images/animations/reconnected.json';

  public static QUOTE_STATUS_MAP = [
    { id: 0, text: 'Negociação' },
    { id: 1, text: 'Congelado' },
    { id: 2, text: 'Suspenso' },
    { id: 3, text: 'Leilão' },
    { id: 4, text: 'Inibido' }
  ];

  public static statusClassList = {
    '0': 'circle-green-icon',
    '1': 'circle-yellow-icon',
    '2': 'circle-red-icon',
    '3': 'circle-yellow-icon',
    '4': 'circle-green-icon'
  };

  // Default Position settings to financial components when showable in gridster2
  // public static position = {
  //   x: 0,
  //   y: 0,
  //   cols: 2,
  //   rows: 1,
  //   dragEnabled: true,
  //   resizeEnabled: true,
  //   compactEnabled: true,
  //   maxItemRows: 1,
  //   minItemRows: 1,
  //   maxItemCols: 2,
  //   minItemCols: 3,
  //   minItemArea: 4,
  //   maxItemArea: 5,
  // };

  public static WORKSPACE_CONF_NAME = 0;
  public static WORKSPACE_CONF_TEXT = 1;
  public static WORKSPACE_CONF_ICON = 2;
  public static QTD_MAX_WORKSHEET = 3;
  public static QTD_MAX_ASSETS_WORKSHEET = 20;

  public static assetsummary_settings = {
    type: Constants.WINDOW_NAME_ASSET_SUMMARY,
    showInGridster: true,
    position: {
      x: 0,
      y: 0,
      cols: 1,
      rows: 1,
      dragEnabled: true,
      resizeEnabled: false,
      maxItemRows: 100,
      minItemRows: 1,
      maxItemCols: 100,
      minItemCols: 1,
      minItemArea: 1,
      maxItemArea: 1
    }
  };

  public static worksheet_settings = {
    type: Constants.WINDOW_NAME_WORK_SHEET,
    showInGridster: true,
    position: {
      x: 0,
      y: 0,
      cols: 2,
      rows: 1,
      dragEnabled: true,
      resizeEnabled: true,
      maxItemRows: 100,
      minItemRows: 1,
      maxItemCols: 100,
      minItemCols: 2,
      minItemArea: 2,
      maxItemArea: 100
    }
  };

  public static timestrades_settings = {
    type: Constants.WINDOW_NAME_TIMES_TRADES,
    showInGridster: true,
    position: {
      x: 0,
      y: 0,
      cols: 1,
      rows: 2,
      dragEnabled: true,
      resizeEnabled: false,
      maxItemRows: 100,
      minItemRows: 2,
      maxItemCols: 100,
      minItemCols: 1,
      minItemArea: 2,
      maxItemArea: 100
    }
  };

  public static volumeatprice_settings = {
    type: Constants.WINDOW_NAME_VOLUME_AT_PRICE,
    showInGridster: true,
    position: {
      x: 0,
      y: 0,
      cols: 1,
      rows: 2,
      dragEnabled: true,
      resizeEnabled: false,
      maxItemRows: 100,
      minItemRows: 2,
      maxItemCols: 100,
      minItemCols: 1,
      minItemArea: 2,
      maxItemArea: 100
    }
  };

  public static book_settings = {
    type: Constants.WINDOW_NAME_BOOK,
    showInGridster: true,
    position: {
      x: 0,
      y: 0,
      cols: 1,
      rows: 2,
      resizeEnabled: false,
      dragEnabled: true,
      maxItemRows: 100,
      minItemRows: 2,
      maxItemCols: 100,
      minItemCols: 1,
      minItemArea: 2,
      maxItemArea: 100
    }
  };

  public static quotechart_settings = {
    type: Constants.WINDOW_NAME_QUOTE_CHART,
    showInGridster: true,
    position: {
      x: 0,
      y: 0,
      cols: 2,
      rows: 1,
      dragEnabled: true,
      resizeEnabled: true,
      maxItemRows: 100,
      minItemRows: 1,
      maxItemCols: 100,
      minItemCols: 2,
      minItemArea: 2,
      maxItemArea: 100
    }
  };

  public static items_settingsArr = [Constants.assetsummary_settings,
  Constants.worksheet_settings, Constants.timestrades_settings, Constants.volumeatprice_settings, Constants.book_settings,
  Constants.quotechart_settings];

  // Options header to diferent request
  public static httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'Access-Control-Allow-Origin': '*',
      'Access-Control-Allow-Headers': 'Origin, X-Requested-With, Content-Type, Accept, Authorization',
      'Access-Control-Allow-Methods': 'POST, GET, PUT, DELETE'
    })
  };
  public static worksheetCols = [
    { field: 'description', header: 'NOME', class: 'txt-left', headerClass: 'txt-left' },
    { field: 'lastTrade', header: 'ÚLTIMO', class: 'txt-center', headerClass: 'txt-center' },
    { field: 'variation', header: 'VAR (%)', class: 'txt-center', headerClass: 'txt-center' },
    { field: 'bid', header: 'COMPRA', class: 'txt-center', headerClass: 'txt-center' },
    { field: 'ask', header: 'VENDA', class: 'txt-center', headerClass: 'txt-center' },
    { field: 'high', header: 'MÁX.', class: 'txt-center', headerClass: 'txt-center' },
    { field: 'low', header: 'MÍN.', class: 'txt-center', headerClass: 'txt-center' },
    { field: 'open', header: 'ABERTURA', class: 'txt-center', headerClass: 'txt-center' },
    { field: 'previous', header: 'FECHAMENTO', class: 'txt-center', headerClass: 'txt-center' },
    { field: 'timeUpdate', header: 'HORA', class: 'txt-center', headerClass: 'txt-center' },
    { field: 'volumeAmount', header: 'VOLUME', class: 'txt-center', headerClass: 'txt-center' },
    { field: 'volumeFinancier', header: 'VOLUME FIN.', class: 'txt-center', headerClass: 'txt-center' },
    { field: 'quantity', header: 'QTD. NEGÓCIOS', class: 'txt-center', headerClass: 'txt-center' },
    { field: 'quantityLast', header: 'QTD. ÚLT. NEG.', class: 'txt-center', headerClass: 'txt-center' },
    { field: 'adjustment', header: 'AJUSTE', class: 'txt-center', headerClass: 'txt-center' },
    { field: 'previousAdjustPrice', header: 'AJUSTE ANT.', class: 'txt-center', headerClass: 'txt-center' },
    { field: 'coinVariation', header: 'VARIAÇÃO', class: 'txt-center', headerClass: 'txt-center' },
    { field: 'varAjustp', header: 'VAR. AJUS. %', class: 'txt-center', headerClass: 'txt-center' },
    { field: 'varAjust', header: 'VAR. AJUS.', class: 'txt-center', headerClass: 'txt-center' },
    { field: 'varTeor', header: 'VAR. TEOR. %', class: 'txt-center', headerClass: 'txt-center' },
    { field: 'dateTrade', header: 'DATA', class: 'txt-center', headerClass: 'txt-center' },
    { field: 'hourLastTrade', header: 'HORA ULT.', class: 'txt-center', headerClass: 'txt-center' },
    { field: 'volAccBid', header: 'VOL. ACUM. COMP.', class: 'txt-center', headerClass: 'txt-center' }, // TODO DONT EXIST MODEL 17
    { field: 'volAccAsk', header: 'VOL. ACUM. VEND.', class: 'txt-center', headerClass: 'txt-center' }, // TODO DONT EXIST MODEL 18
    { field: 'volumeBetterBid', header: 'VOL. MELH. COMP.', class: 'txt-center', headerClass: 'txt-center' },
    { field: 'volumeBetterAsk', header: 'VOL. MELH. VEND.', class: 'txt-center', headerClass: 'txt-center' },
    { field: 'previousWeek', header: 'FECH. SEMANA', class: 'txt-center', headerClass: 'txt-center' }, // TODO DONT EXIST MODEL 36
    { field: 'previousMonth', header: 'FECH. MÊS', class: 'txt-center', headerClass: 'txt-center' }, // TODO DONT EXIST MODEL 37
    { field: 'previousYear', header: 'FECH. ANO', class: 'txt-center', headerClass: 'txt-center' }, // TODO DONT EXIST MODEL 38
    { field: 'openLastDay', header: 'AB. DIA ANT.', class: 'txt-center', headerClass: 'txt-center' }, // TODO DONT EXIST MODEL 39
    { field: 'highPriceLastDay', header: 'MAIOR DIA ANT.', class: 'txt-center', headerClass: 'txt-center' }, // TODO DONT EXIST MODEL 40
    { field: 'lowPriceLastDay', header: 'MENOR DIA ANT.', class: 'txt-center', headerClass: 'txt-center' }, // TODO DONT EXIST MODEL 41
    { field: 'average', header: 'PREÇO MÉDIO', class: 'txt-center', headerClass: 'txt-center' },
    { field: 'vhDay', header: 'VH DIA', class: 'txt-center', headerClass: 'txt-center' }, // TODO DONT EXIST MODEL 43
    { field: 'marketType', header: 'CÓD. MERCADO', class: 'txt-center', headerClass: 'txt-center' },
    { field: 'assetType', header: 'CÓD. TIPO', class: 'txt-center', headerClass: 'txt-center' },
    { field: 'loteDefault', header: 'LOTE PADRÃO', class: 'txt-center', headerClass: 'txt-center' },
    { field: 'classification', header: 'CLASSIFIC.', class: 'txt-center', headerClass: 'txt-center' },
    { field: 'quotationForm', header: 'FORMA COT.', class: 'txt-center', headerClass: 'txt-center' },
    { field: 'dateLastNegotiation', header: 'DATA ULT. NEG.', class: 'txt-center', headerClass: 'txt-center' }, // TODO DONT EXIST MODEL 54
    { field: 'directionUnansweredQty', header: 'INDIC. RESTO', class: 'txt-center', headerClass: 'txt-center' },
    { field: 'unansweredQty', header: 'QTD. NÃO ATEND.', class: 'txt-center', headerClass: 'txt-center' },
    { field: 'timeToOpen', header: 'HORA PROG. AB. PAP.', class: 'txt-center', headerClass: 'txt-center' },
    { field: 'timeReprogrammedToOpen', header: 'HORA REPROG. AB. PAP.', class: 'txt-center', headerClass: 'txt-center' },
    { field: 'expirationDate', header: 'DATA VENC.', class: 'txt-center', headerClass: 'txt-center' },
    { field: 'expiration', header: 'EXPIRADO', class: 'txt-center', headerClass: 'txt-center' },
    { field: 'quantitySymbol', header: 'TOT. PAPÉIS', class: 'txt-center', headerClass: 'txt-center' },
    { field: 'expiration', header: 'EXPIRADO', class: 'txt-center', headerClass: 'txt-center' },
    { field: 'inTheWeek', header: 'STATUS INSTR.', class: 'txt-center', headerClass: 'txt-center' },
    { field: 'typeOption', header: 'TIPO OPÇÃO', class: 'txt-center', headerClass: 'txt-center' }, // TODO DONT EXIST MODEL 72
    { field: 'theoryPrice', header: 'PREÇO TEÓR. AB.', class: 'txt-center', headerClass: 'txt-center' },
    { field: 'theoryQuantity', header: 'QTD. TEÓR.', class: 'txt-center', headerClass: 'txt-center' },
    // { field: 'coinVariation', header: 'VAR.(MOEDA)', class: 'txt-center', headerClass: 'txt-center' },
    { field: 'volAVG20', header: 'VOL. MÉD.(20 D)', class: 'txt-center', headerClass: 'txt-center' }, // TODO DONT EXIST MODEL 94
    { field: 'calculoWeeklyVariation', header: 'VAR. FECH.(7 D)', class: 'txt-center', headerClass: 'txt-center' },
    { field: 'lastTrade1Month', header: 'VALOR FECH. MÊS', class: 'txt-center', headerClass: 'txt-center' },
    { field: 'calculoMonthlyVariation', header: 'VAR. FECH. MÊS', class: 'txt-center', headerClass: 'txt-center' },
    { field: 'lastTrade1Year', header: 'VALOR FECH. ANO', class: 'txt-center', headerClass: 'txt-center' },
    { field: 'calculoYearlyVariation', header: 'VAR. FECH. ANO', class: 'txt-center', headerClass: 'txt-center' },
    { field: 'tickSize', header: 'CASAS DECIMAIS', class: 'txt-center', headerClass: 'txt-center' },
    { field: 'deltaVolumeCurrentTime', header: 'VAR. VOL. HORA(20 D)', class: 'txt-center', headerClass: 'txt-center' },
    { field: 'deltaVolumeAccumulatedHour', header: 'VAR. VOL. ATÉ HORA(20 D)', class: 'txt-center', headerClass: 'txt-center' },
    { field: 'status', header: 'STATUS', class: 'txt-center', headerClass: 'txt-center' }, // TODO BOVESPA
    { field: 'exercisePrice', header: 'PREÇO EXERCÍCIO', class: 'txt-center', headerClass: 'txt-center' }, // TODO BOVESPA
    { field: 'groupPhase', header: 'FASE DO GRUPO', class: 'txt-center', headerClass: 'txt-center' }, // TODO DONT EXIST MODEL 88 BOVESPA
    { field: 'quantityOpenedContractNumber', header: 'CONTRATOS AB.', class: 'txt-center', headerClass: 'txt-center' }, // TODO BMF
    { field: 'businesDaysToExpirationDate', header: 'DIAS ÚTEIS VENC.', class: 'txt-center', headerClass: 'txt-center' }, // TODO BMF
    { field: 'calendarDaysToExpirationDate', header: 'DIAS VENC.', class: 'txt-center', headerClass: 'txt-center' }, // TODO BMF
    // { field: 'adjustment', header: 'AJUSTE DIA', class: 'txt-center', headerClass: 'txt-center' }, // TODO BMF
    // { field: 'previousAdjustPrice', header: 'AJUSTE DIA ANT.', class: 'txt-center', headerClass: 'txt-center' }, // TODO BMF
    { field: 'securityId', header: 'SECURITY ID', class: 'txt-center', headerClass: 'txt-center' }, // TODO DONT EXIST MODEL 105 BMF
    { field: 'tickDirection', header: 'TICK DIRECTION', class: 'txt-center', headerClass: 'txt-center' }, // TODO DONT EXIST MODEL 106 BMF
    { field: 'tunnelUpperLimit', header: 'LIM. MAX. TÚNEL', class: 'txt-center', headerClass: 'txt-center' }, // TODO BMF
    { field: 'tunnelLowerLimit', header: 'LIM. MIN. TÚNEL', class: 'txt-center', headerClass: 'txt-center' }, // TODO BMF
    { field: 'minimumIncrement', header: 'INCREM. MÍN.', class: 'txt-center', headerClass: 'txt-center' }, // TODO BMF
    { field: 'exercisePriceOption', header: 'PREÇO EXERC. OPÇÃO', class: 'txt-center', headerClass: 'txt-center' }, // TODO DONT EXIST MODEL 121 BMF
    { field: 'contractMultiplier', header: 'MULT. DO CONTRATO', class: 'txt-center', headerClass: 'txt-center' }, // TODO BMF
    { field: 'adjustCurrentRate', header: 'AJUSTE ATUAL TAXA', class: 'txt-center', headerClass: 'txt-center' }, // TODO BMF
    { field: 'adjustPreviousRate', header: 'AJUSTE ANT. TAXA', class: 'txt-center', headerClass: 'txt-center' }, // TODO BMF
    { field: 'withdrawalsNumberUntilDueDate', header: 'SAQUES ATÉ VENC.', class: 'txt-center', headerClass: 'txt-center' }, // TODO BMF
  ];

  // Paths constants
  public static HOME_WKS_PATH = '/home/workspace';

  public static ERROR_404_TITLE = 'Página não encontrada!';
  public static ERROR_404_ICON = '404';
  public static ERROR_404_BUTTON = 'Início';
  public static ERROR_404_TEXT = 'O link que você clicou pode estar quebrado ou a página pode ter sido removida.';
  public static ERROR_TIMEOUT_TITLE = 'Sua sessão será encerrada em 55 segundos.';
  public static ERROR_TIMEOUT_ICON = 'session';
  public static ERROR_TIMEOUT_BUTTON = 'CONTINUAR';
  public static ERROR_TIMEOUT_TEXT = 'Para manter sua sessão ativa, clique em "continuar"';
  public static ERROR_ERROR_TITLE = 'Ocorreu um erro.';
  public static ERROR_ERROR_ICON = 'error';
  public static ERROR_ERROR_BUTTON = 'RECARREGAR PÁGINA';
  public static ERROR_ERROR_TEXT = 'Algo deu errado. Experimente recarregar a página.';
  public static ERROR_DENIED_TITLE = 'Acesso negado!';
  public static ERROR_DENIED_ICON = 'permission';
  public static ERROR_DENIED_BUTTON = 'Entre em contato';
  public static ERROR_DENIED_TEXT = 'No momento, você não tem permissão para acessar o Home Broker.';
  public static ERROR_EXPIRED_TITLE = 'Como o tempo voa. Sua sessão expirou.';
  public static ERROR_EXPIRED_ICON = 'session';
  public static ERROR_EXPIRED_BUTTON = 'LOGIN';
  public static ERROR_EXPIRED_TEXT = 'Se você quiser continuar navegando faça o login novamente';

  // Youtube API constants
  public static HB_DOMAIN = 'trade.fastmarkets.com.br'; // TODO change to production
  public static YOUTUBE_URL = 'https://www.youtube.com/';
  public static CHAT_PARAMS = '&amp;embed_domain=' + Constants.HB_DOMAIN + '&amp;wmode=opaque';
  public static CHAT_URL = Constants.YOUTUBE_URL + 'live_chat?v=';
  public static VIDEO_PARAMS = '?feature=oembed&amp;wmode=opaque';
  public static VIDEO_URL = Constants.YOUTUBE_URL + 'embed/';
  // public static SEARCH_VIDEO_INTERVAL = 60000; // 1 minuto
  public static SEARCH_VIDEO_INTERVAL = 30000; // 20s segundos
  public static PROMO_URL = 'http://promo.cedrotech.com/fast-trade-download-plataforma-3-0';

  public static YOUTUBE_CHANNEL_ID = 'UCGhbswoCt_QUHXvzdywH3TQ';                // FAST TRADE
  // public static YOUTUBE_CHANNEL_ID = 'UC7Nnkn76ijG-vM4mfhp4Cjw';             // Levante Channel Id
  // public static YOUTUBE_CHANNEL_ID = 'UCJhjE7wbdYAae1G25m0tHAA';             // Teste
  public static GOOGLE_SERVICE_KEY = 'AIzaSyA8j7g-t8RI3r8SpEs2OA9BZr3aPZpSBu0'; // Google service key
}

export const MY_DATE_FORMATS = {
  parse: {
    dateInput: 'L',
  },
  display: {
    dateInput: 'L',
    monthYearLabel: 'MMM YYYY',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'MMMM YYYY',
  },
};
