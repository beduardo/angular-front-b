import { IDictionaryCollection } from '@utils/dictionary/dictionary-collection.interface';
export class DictionaryCollection<T> implements IDictionaryCollection<T> {
  private items: { [index: string]: T };
  private countDic: number;
  constructor() {
    this.items = {};
    this.countDic = 0;
  }
  public containsKey(key: string): boolean {
    return this.items.hasOwnProperty(key);
  }
  public count(): number {
    return this.countDic;
  }
  public add(key: string, value: T) {
    if (!this.items.hasOwnProperty(key)) {
      this.countDic++;
    }
    this.items[key] = value;
  }
  get getItems() {
    return this.items;
  }
  public remove(key: string): T {
    const val = this.items[key];
    delete this.items[key];
    this.countDic--;
    return val;
  }
  public item(key: string): T {
    return this.items[key];
  }
  public keys(): string[] {
    const keySet: string[] = [];
    for (const prop in this.items) {
      if (this.items.hasOwnProperty(prop)) {
        keySet.push(prop);
      }
    }
    return keySet;
  }
  public values(): T[] {
    const values: T[] = [];
    for (const prop in this.items) {
      if (this.items.hasOwnProperty(prop)) {
        values.push(this.items[prop]);
      }
    }
    return values;
  }
  public clear() {
    this.items = {};
    this.countDic = 0;
  }
}
