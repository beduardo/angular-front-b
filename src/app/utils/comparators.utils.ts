import * as moment from 'moment';
import { AppUtils } from '@utils/app.utils';
export class Comparators {
    public static brFormattedNumberComparator(a, b) {
        const number1 = AppUtils.convertBrCurrencyToNumber(a + '', ',') || 0;
        const number2 = AppUtils.convertBrCurrencyToNumber(b + '', ',') || 0;
        return number1 - number2;
    }
    public static minMaxComparator(a, b) {
        const splitNumber1 = a.split('/')[0] || 0;
        const splitNumber2 = b.split('/')[0] || 0;
        const number1 = AppUtils.convertBrCurrencyToNumber(splitNumber1, ',') || 0;
        const number2 = AppUtils.convertBrCurrencyToNumber(splitNumber2, ',') || 0;
        return number1 - number2;
    }
    public static volumeComparator(a, b) {
        const number1 = AppUtils.convertOptimizedNumberToNumber(a) || 0;
        const number2 = AppUtils.convertOptimizedNumberToNumber(b) || 0;
        return number1 - number2;
    }
    public static dateComparator(date1, date2) {
        const date1Number = Comparators.monthToComparableNumber(date1);
        const date2Number = Comparators.monthToComparableNumber(date2);
        if (date1Number === null && date2Number === null) {
            return 0;
        }
        if (date1Number === null) {
            return -1;
        }
        if (date2Number === null) {
            return 1;
        }
        return date1Number - date2Number;
    }
    public static webfeederDateHourComparator(date1, date2) {
        if ((date1 === null && date2 === null) || (date1 === '' && date2 === '')) {
            return 0;
        }
        if (date1 === null || date1 === '') {
            return -1;
        }
        if (date2 === null || date2 === '') {
            return 1;
        }
        const m1 = moment(date1, 'DD/MM/YYYY HH:mm:ss').valueOf();
        const m2 = moment(date2, 'DD/MM/YYYY HH:mm:ss').valueOf();
        return m1 - m2;
    }
    public static monthToComparableNumber(date) {
        if (date === undefined || date === null || date.length !== 10) {
            return null;
        }
        const yearNumber = date.substring(6, 10);
        const monthNumber = date.substring(3, 5);
        const dayNumber = date.substring(0, 2);
        const result = yearNumber * 10000 + monthNumber * 100 + dayNumber;
        return result;
    }
}
