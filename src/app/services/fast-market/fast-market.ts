

export class Content {
  rendered?: string;
  protected?: boolean;
}

export class Embedded {
  author?: EmbeddedAuthor[];
  wpFeaturedmedia?: WpFeaturedmedia[];
  wpTerm?: Array<EmbeddedWpTerm[]>;
}

export class EmbeddedAuthor {
  id?: number;
  name?: string;
  url?: string;
  description?: string;
  link?: string;
  slug?: string;
  avatarUrls?: { [key: string]: string };
  links?: AuthorLinks;
}

export class AuthorLinks {
  self?: WpAttachmentElement[];
  collection?: WpAttachmentElement[];
}

export class WpAttachmentElement {
  href?: string;
}

export class WpFeaturedmedia {
  id?: number;
  date?: string;
  slug?: string;
  type?: string;
  link?: string;
  title?: GUID;
  author?: number;
  caption?: GUID;
  altText?: string;
  mediaType?: string;
  mimeType?: MIMEType;
  mediaDetails?: MediaDetails;
  sourceURL?: string;
  links?: WpFeaturedmediaLinks;
}

export class GUID {
  rendered?: string;
}

export class WpFeaturedmediaLinks {
  self?: PurpleAbout[];
  collection?: PurpleAbout[];
  about?: PurpleAbout[];
  author?: PurpleAuthor[];
  replies?: PurpleAuthor[];
}

export class PurpleAbout {
  attributes?: any[];
  href?: string;
}

export class PurpleAuthor {
  attributes?: Attributes;
  href?: string;
}

export class Attributes {
  embeddable?: boolean;
}

export class MediaDetails {
  width?: number;
  height?: number;
  file?: string;
  sizes?: { [key: string]: Size };
  imageMeta?: ImageMeta;
}

export class ImageMeta {
  aperture?: string;
  credit?: string;
  camera?: string;
  caption?: string;
  createdTimestamp?: string;
  copyright?: string;
  focalLength?: string;
  iso?: string;
  shutterSpeed?: string;
  title?: string;
  orientation?: string;
  keywords?: any[];
}

export class Size {
  file?: string;
  width?: number;
  height?: number;
  mimeType?: MIMEType;
  sourceURL?: string;
}

export enum MIMEType {
  ImagePNG = "image/png",
}

export class EmbeddedWpTerm {
  id?: number;
  link?: string;
  name?: string;
  slug?: string;
  taxonomy?: string;
  links?: WpTermLinks;
}

export class WpTermLinks {
  self?: WpAttachmentElement[];
  collection?: WpAttachmentElement[];
  about?: WpAttachmentElement[];
  wpPostType?: WpAttachmentElement[];
  curies?: Cury[];
}

export class Cury {
  name?: string;
  href?: string;
  templated?: boolean;
}

export class WelcomeLinks {
  self?: WpAttachmentElement[];
  collection?: WpAttachmentElement[];
  about?: WpAttachmentElement[];
  author?: WpFeaturedmediaElement[];
  replies?: WpFeaturedmediaElement[];
  versionHistory?: VersionHistory[];
  predecessorVersion?: PredecessorVersion[];
  wpFeaturedmedia?: WpFeaturedmediaElement[];
  wpAttachment?: WpAttachmentElement[];
  wpTerm?: LinksWpTerm[];
  curies?: Cury[];
}

export class WpFeaturedmediaElement {
  embeddable?: boolean;
  href?: string;
}

export class PredecessorVersion {
  id?: number;
  href?: string;
}

export class VersionHistory {
  count?: number;
  href?: string;
}

export class LinksWpTerm {
  taxonomy?: string;
  embeddable?: boolean;
  href?: string;
}

export class FastMarket {
  id?: number;
  date?: string;
  dateGmt?: string;
  guid?: GUID;
  modified?: string;
  modifiedGmt?: string;
  slug?: string;
  status?: string;
  type?: string;
  link?: string;
  title?: GUID;
  content?: Content;
  excerpt?: Content;
  author?: number;
  featuredMedia?: number;
  commentStatus?: string;
  pingStatus?: string;
  sticky?: boolean;
  template?: string;
  format?: string;
  meta?: any[];
  categories?: number[];
  tags?: any[];
  links?: WelcomeLinks;
  _embedded?: Embedded;
}
