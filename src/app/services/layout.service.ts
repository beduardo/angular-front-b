import { ConfigService } from '@services/webFeeder/config.service';
import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { Cookie } from 'ng2-cookies/ng2-cookies';

@Injectable({
  providedIn: 'root'
})
export class LayoutService {
  public _isDark = new BehaviorSubject(false);
  public isDark$ = this._isDark.asObservable();

  constructor(
    private configService: ConfigService
  ) {
    if (this.configService.getFavoriteThemeUser() === 'true') {
      this._isDark.next(true);
    } else {
      this._isDark.next(false);
    }
  }

  switchThemes() {
    this._isDark.next(!this._isDark.value);
  }

  getCurrentState(): boolean {
    return this._isDark.getValue();
  }

  setDarkTheme() {
    this._isDark.next(true);
  }

  setLightTheme() {
    this._isDark.next(false);
  }

}
