import { Injectable } from '@angular/core';
import { ConfigService } from '@services/webFeeder/config.service';
import { HttpService } from '@services/http-service';

@Injectable({
  providedIn: 'root'
})
export class LogsService {

  constructor(
    private httpService: HttpService,
    private configService: ConfigService
  ) { }

  public writeLog(messageLog: string) {
    const headers = this.httpService.getHeaders();
    this.configService.writeLog('LOGS > writeLog');
    this.configService.writeLog(messageLog);
    return this.httpService
      .doPost(this.configService.getApiWebFeederRest(), 'services/Logs/writeLog', JSON.stringify({ message: messageLog }))
      .map(response => {
        if (response) {
          if (response.error) {
            return [];
          }
          return response.json();
        }
        return [];
      });
  }

}
