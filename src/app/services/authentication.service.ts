import { ConfigService } from '@services/webFeeder/config.service';
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/map';
import { AuthService } from '@auth/auth.service';
import { BehaviorSubject, Observable, of } from 'rxjs';
import { Constants } from '@utils/constants';
import { Auth1Service } from '@services/strategy-pattern-login/auth1.service';
import { Auth2Service } from '@services/strategy-pattern-login/auth2.service';
import { Auth3Service } from '@services/strategy-pattern-login/auth3.service';
import { Loggable } from '@services/strategy-pattern-login/loggable';
import { CedroAccountService } from './cedro-account.service';
import { Signature } from '@model/signature.model';
import { HttpClient, HttpHeaders } from '@angular/common/http';
@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  public iLogin: Loggable;

  constructor(
    private configService: ConfigService,
    private auth1Service: Auth1Service,
    private auth2Service: Auth2Service,
    private auth3Service: Auth3Service,
    private authGuard: AuthService,
    private cedroAccountService: CedroAccountService,
    private http: HttpClient
  ) {
    if (Constants.LOGIN_TYPE.LoginO === this.configService.getLoginTypeEnabled()) {
      this.iLogin = auth1Service;
    } else if (Constants.LOGIN_TYPE.LoginS === this.configService.getLoginTypeEnabled()) {
      this.iLogin = auth2Service;
    } else if (Constants.LOGIN_TYPE.LoginW === this.configService.getLoginTypeEnabled()) {
      this.iLogin = auth3Service;
    } else {
      this.iLogin = null;
    }
  }

  private connectedAs = new BehaviorSubject<number>(
    this.checkBrokerAuthentication() ? 2 : 3
  );

  private ConnectionStatus = new BehaviorSubject<number>(
    0
  );
  public url;

  connectedAs$ = this.connectedAs.asObservable();

  ConnectionStatus$ = this.connectedAs.asObservable();

  login(username: string, password: string, profile?: any) {
    return this.iLogin.login(username, password, profile);
  }

  logout() {
    this.brokerServiceLogout();
    this.authGuard.logout();
  }

  brokerServiceLogin(username: string, password: string, broker: string) {
    return this.iLogin.brokerServiceLogin(username, password, broker);
  }

  brokerServiceLogout() {
    this.updateWorkspaceSelected(3);
    return this.iLogin.brokerServiceLogout();
  }

  brokerServiceInternalLogout() {
    this.updateWorkspaceSelected(3);
    return this.iLogin.brokerServiceInternalLogout();
  }

  getBrokers() {
    // TODO: console.log('Aguardando api para buscar corretoras');
    return true;
  }

  checkBrokerAuthentication() {
    if (this.configService.getBrokerToken()) {
      return true;
    } else {
      return false;
    }
  }

  updateWorkspaceSelected(data: number) {
    this.connectedAs.next(data);
  }

  recoverPassword(email: string, type: number, client: boolean = true) {
    return this.iLogin.recoverPassword(email, type, client);
  }

  changePassword(token: string, type: number, newPassword: string, broker: string) {
    return this.iLogin.changePassword(token, type, newPassword, broker);
  }

  difusionChangePassword(login: string, oldPassword: string, newPassword: string) {
    return this.iLogin.difusionChangePassword(login, oldPassword, newPassword);
  }

  clientRegisterSimulator(username: string, password: string) {
    return this.iLogin.clientRegisterSimulator(username, password);
  }

  validateSignatureValid(params: Object) {
    return this.iLogin.validateSignatureValid(params);
  }

  resetSignature(signature: string, newSignature: string) {
    return this.iLogin.resetSignature(signature, newSignature);
  }

  forgotSignature(email: string) {
    return this.iLogin.forgotSignature();
  }

  advisorInfo(login?: string) {
    return this.iLogin.advisorInfo(login);
  }

  advisorChangePassword(currentPassword: string, newPassword: string, guid?: string, advisorId?: number) {
    return this.iLogin.advisorChangePassword(currentPassword, newPassword, guid, advisorId);
  }

  public getIp() {
    return this.http
      .get('https://api.ipify.org/?format=json&callback=JSONP_CALLBACK')
      .map((response: any) => {
        if (response) {
          return response.ip;
        }
        return of('');
      })
      .catch(error => {
        throw error;
      });
  }

}
