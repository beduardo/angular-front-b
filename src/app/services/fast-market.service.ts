import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { ConfigService } from '@services/webFeeder/config.service';
import { Observable } from 'rxjs';
import { DatePipe } from '@angular/common';
import { Constants } from '@utils/constants';

@Injectable({
  providedIn: 'root'
})
export class FastMarketService {

  constructor(
    private http: HttpClient,
    private config: ConfigService,
    private datePipe: DatePipe
  ) { }

  getLastFastMarketNews(qtd: number, page: number, search?: string, dateStartFilter?: Date, dateEndFilter?: Date) {
    let query = 'posts?_embed';
    if (search) {
      const maxSpace = search.split(' ').length;
      for (let i = 0; i < maxSpace; i++) {
        search = search.replace(' ', '%20');
      }
      query += '&search=' + search.trim();
    }
    if (qtd) {
      query += '&per_page=' + qtd.toString();
    } else {
      query += '&per_page=10';
    }
    if (page) {
      query += '&page=' + page;
    } else {
      query += '&page=1';
    }
    let before;
    let after;
    if (dateStartFilter && dateEndFilter) {
      after = this.datePipe.transform(dateStartFilter, Constants.DATE_NEWS_YYYYMMDD);
      before = this.datePipe.transform(new Date(dateEndFilter).setDate(new Date(dateEndFilter).getDate()), Constants.DATE_NEWS_YYYYMMDD);
      query += '&before=' + before + Constants.DATE_NEWS_BEFORE + '&after=' + after + Constants.DATE_NEWS_AFTER;
    } else if (dateStartFilter && !dateEndFilter) {
      const dataFimTEmp = dateStartFilter;
      before = this.datePipe.transform(dateStartFilter, Constants.DATE_NEWS_YYYYMMDD);
      after = this.datePipe.transform(new Date(dataFimTEmp).setDate(new Date(dataFimTEmp).getDate()), Constants.DATE_NEWS_YYYYMMDD);
      query += '&before=' + before + Constants.DATE_NEWS_BEFORE + '&after=' + after + Constants.DATE_NEWS_AFTER;
    }
    return this.http.get<any>(this.config.getFastMarketApiUrl() + query)
      .map(response => {
        if (response.length > 0) {
          return response;
        }
        return [];
      });
  }
}
