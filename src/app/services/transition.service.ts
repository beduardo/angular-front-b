import { Cookie } from 'ng2-cookies/ng2-cookies';
import { ConfigService } from './webFeeder/config.service';
import { Constants } from '@utils/constants';
import { Injectable, Inject } from '@angular/core';

export class TransitionUser {
  ASSESSOR_CLIENT_SELECTED: string;
  ASSESSOR_LIST_CLIENT: string;
  CLIENT: boolean;
  HBA: number;
  HBA_CURRENT_USER: string;
  HBA_EMAIL: string;
  SYSTEM_USERNAME: string;
  HBA_ASSESSOR_ID: string;
  HBA_ID: number;
  HBA_USER_ID: number;
  HBA_USERNAME: string;
  HB_BROKER: string;
  HB_BROKER_SELECT: string;
  HB_LOGIN_TYPE: string;
  HB_LOGIN_TYPE_ENABLED: string;
  TKNAPI: string;
  TKNEXP: string;
  TKNWF: string;
  TKNWF_GRAPHIC: string;
  TKNAPIREFRESH: string;
  TKN_NEGOT: string;
  USER_LOGIN_TYPE: string;
  LAYOUT_THEME_DARK: string;
  IS_ASSESSOR: boolean;
  CLIENT_PROFILE_NAME: string;
}

@Injectable({ providedIn: 'root' })
export class TransitionService {

  public commonObj: TransitionUser = new TransitionUser();
  // private commonObj = {
  //   theme: Constants.LAYOUT_TYPE.Light
  // };

  constructor() {
    // this.commonObj = new TransitionUser();
    this.commonObj.HB_LOGIN_TYPE_ENABLED = window.sessionStorage.getItem(Constants.COOKIE_HB_LOGIN_TYPE_ENABLED);
    this.commonObj.LAYOUT_THEME_DARK = Constants.LAYOUT_TYPE.Light;
  }

  setCommonObj(commonObj) {
    this.commonObj = commonObj;
    this.save();
  }

  setKeyValue(key: string, value: any) {
    if (key) {
      this.commonObj[key] = value;
      this.save();
    }
  }

  removeKeyValue(key: string) {
    this.commonObj[key] = null;
    this.save();
  }

  get() {
    if (window.sessionStorage.getItem(Constants.STORAGE_AUTH)) {
      return this.commonObj = JSON.parse(window.sessionStorage.getItem(Constants.STORAGE_AUTH));
    }
    return;
  }

  recover() {
    if (window.sessionStorage.getItem(Constants.STORAGE_AUTH)) {
      this.commonObj = JSON.parse(window.sessionStorage.getItem(Constants.STORAGE_AUTH));
    }
  }

  clear() {
    window.sessionStorage.removeItem(Constants.STORAGE_AUTH);
  }

  private save() {
    window.sessionStorage.setItem(Constants.STORAGE_AUTH, JSON.stringify(this.commonObj));
  }

}
