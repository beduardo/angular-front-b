import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Rx';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { ConfigService } from '@services/webFeeder/config.service';
import { WebFeederFactory } from '@connections/web-feeder.factory';
import { environment } from 'environments/environment';
import { StatusConnection } from '@model/status-connection';
import { Location } from '@angular/common';
import { MatDialog } from '@angular/material';
import { Router } from '@angular/router';
import { ErrorMessage } from '@model/error-message';
import { Constants } from '@utils/constants';

@Injectable({
  providedIn: 'root'
})
export class SocketService {
  private subject: Subject<MessageEvent>;
  private url: string;
  private numberReconect = 1;
  private isConnected = false;
  private isReconnected = false;
  private isLogout = false;
  private setLoadingWks = false;
  private location: Location;
  private _statusWebSocket = new BehaviorSubject<number>(StatusConnection.INITIAL);
  statusWebSocket$ = this._statusWebSocket.asObservable();
  private _numberReconect = new BehaviorSubject<number>(0);
  numberReconect$ = this._numberReconect.asObservable();
  private _reconectSocket = new BehaviorSubject<boolean>(false);
  reconectSocket$ = this._reconectSocket.asObservable();
  private _statusWFLogin = new BehaviorSubject<boolean>(true);
  statusWFLogin$ = this._statusWFLogin.asObservable();
  private _statusWFOnError = new BehaviorSubject<number>(StatusConnection.INITIAL);
  _statusWFOnError$ = this._statusWFOnError.asObservable();
  private services = {};

  constructor(
    private configService: ConfigService,
    private modalService: MatDialog,
    private router: Router
  ) { }

  public connect() {
    if (!WebFeederFactory.existsWS(this.configService.getUrlWebSocketWebFeeder(this.numberReconect))) {
      this.url = this.configService.getUrlWebSocketWebFeeder(this.numberReconect);
      this.configService.writeLog('\nSOCKET SERVICE > -----------------------------------------------------------');
      this.configService.writeLog('SOCKET SERVICE > CONNECT - WS: [' + this.url + '][' + this.numberReconect + ']');
      this.configService.writeLog('SOCKET SERVICE > -----------------------------------------------------------\n');
      this.subject = this.create(this.url);
    }
  }

  public register(obsService: any) {
    if (!this.services[obsService.name]) {
      this.services[obsService.name] = obsService;
    }
  }

  public signService(message) {
    this.subject.next(message);
  }

  public disconnect() {
    this.configService.writeLog('\nSOCKET SERVICE > disconnect : [' + this.url);
    WebFeederFactory.closeAllConnections();
    WebFeederFactory.closeWebFeeder(this.url);
  }

  public logout() {
    this.configService.writeLog('\nSOCKET SERVICE > logout : [' + this.url);
    this.isLogout = true;
    WebFeederFactory.closeWebFeeder(this.url);
    WebFeederFactory.closeAllConnections();
  }

  private create(url): Subject<MessageEvent> {
    let observer: any;
    try {
      const ws = WebFeederFactory.getWebFeederConnection(url, this.isReconnected);
      this.configService.writeLog('\nSOCKET SERVICE > create: [' + url + ']');
      ws.onopen = evt => {
        this.isLogout = false;
        this.configService.writeLog('SOCKET SERVICE > onOpen');
        this._statusWebSocket.next(StatusConnection.SUCCESS);
        this._numberReconect.next(0);

        this.isConnected = true;
        if (this.isReconnected) {
          this.configService.writeLog('SOCKET SERVICE > SUCCESS_RECONNECT');
          this.isReconnected = false;
          this._statusWebSocket.next(StatusConnection.SUCCESS_RECONNECT);
          this.numberReconect = 0;
          setTimeout(() => {
            location.reload();
          }, 10);
        }
      };

      ws.onclose = evt => {
        this.configService.writeLog('SOCKET SERVICE > onClose');

        if (!this.isLogout) {
          this.isConnected = false;
          this._statusWebSocket.next(StatusConnection.ERROR);
          setTimeout(() => {
            this.configService.writeLog('SOCKET SERVICE > TIMEOUT > Reconnect');
            this.onReconnect();
          }, environment.RECONNECT_INTERVAL);
        }
      };

      ws.onerror = (evt, self = this) => {
        this.configService.writeLog('SOCKET SERVICE > onError');
        this.configService.writeError(evt);
        this._statusWebSocket.next(StatusConnection.ERROR);
      };

      ws.onmessage = (event) => {
        try {
          const answer = JSON.parse(event.data);
          if (answer && answer.code && answer.message) {
            this.configService.writeLog('SOCKET QUOTE > code: ' + answer.code + ' - message: ' + answer.message);
            if (answer.code === Constants.WF_SERVICE_LOGIN_ERROR_CODE) {
              this.disconnect();
              this._statusWFLogin.next(false);
              this.modalService.closeAll();
              this.router.navigate(['error'], {
                queryParams: new ErrorMessage(Constants.ERROR_EXPIRED_ICON, Constants.ERROR_EXPIRED_TITLE, Constants.ERROR_EXPIRED_TEXT,
                  Constants.ERROR_EXPIRED_BUTTON, 'login')
              });
            }
            if (answer.code === Constants.WF_SERVICE_ERROR_CODE) {
              this._statusWFOnError.next(StatusConnection.ERROR);
            }
          }
          if (answer && answer.token !== undefined && answer.success === true) {
            this.configService.setTokenWS(answer.token);
            return null;
          }
          if (this.services[answer.type]) {
            this.services[answer.type].serviceSubject.next(answer);
          } else {
            for (let key of Object.keys(this.services)) {
              if (this.services[key].fn && this.services[key].fn(answer)) {
                this.services[key].serviceSubject.next(answer);
                return;
              }
            }
          }
        } catch (err) {
          this.configService.writeLog('SOCKET SERVICE > onmessage > error converting message' + err);
        }
      };
      observer = {
        next: (data: Object) => {
          // this.configService.writeLog('SOCKET SERVICE > WS - #################### INICIO #############################################');
          // this.configService.writeLog('SOCKET SERVICE > readyState: ' + ws.readyState);
          // this.configService.writeLog('SOCKET SERVICE > readyState: [' + ws.readyState + '][' + 'CONECTADO' + ']');
          // this.configService.writeLog(data);
          // this.configService.writeLog('SOCKET SERVICE > readyState: [' + ws.readyState + '][' + 'CONECTADO' + ']');
          if (ws.readyState === WebSocket.OPEN) {
            // this.configService.writeLog(JSON.stringify(data));
            ws.send(JSON.stringify(data));
            // this.configService.writeLog('SOCKET SERVICE > ----------------------------------------------------------------\n');
          } else {
            let status = '-';
            if (ws.readyState === WebSocket.CLOSED) {
              status = 'CLOSED';
              this.configService.writeLog('SOCKET SERVICE > readyState: [' + ws.readyState + '][' + status + ']');
            } else if (ws.readyState === WebSocket.CLOSING) {
              status = 'CLOSING';
              this.configService.writeLog('SOCKET SERVICE > readyState: [' + ws.readyState + '][' + status + ']');
            } else if (ws.readyState === WebSocket.CONNECTING) {
              status = 'CONNECTING';
              this.configService.writeLog('SOCKET SERVICE > readyState: [' + ws.readyState + '][' + status + ']');
            }
            this.configService.writeLog('SOCKET SERVICE > readyState: [' + ws.readyState + '][' + status + ']');
          }
          setTimeout(() => {
            if (ws.readyState === WebSocket.OPEN && !this.setLoadingWks) {
              this.configService.writeLog('SOCKET SERVICE > readyState: [' + ws.readyState + '][' + 'CONECTADO' + ']');
              this._reconectSocket.next(true);
              this.setLoadingWks = true;
            }
          }, 3000);
        }
      };
    } catch (e) {
      this.configService.writeError('Error: criar conexão do ws');
    }
    return Subject.create(observer);
  }

  private onReconnect() {
    try {
      if (!this.isConnected) {
        this.isReconnected = true;
        this.numberReconect = this.numberReconect + 1;
        this.url = this.configService.getUrlWebSocketWebFeeder(
          this.numberReconect
        );
        this.configService.writeLog('\nSOCKET SERVICE > ---------------------------------------------------------------------------------');
        this.configService.writeLog('SOCKET SERVICE > onReconnect: [' + this.url + '][' + this.numberReconect + ']');
        this.configService.writeLog('SOCKET SERVICE > ---------------------------------------------------------------------------------\n');
        this._numberReconect.next(this.numberReconect);
        if (this.numberReconect >= environment.RECONECT_ATTEMPTS) {
          this.configService.writeLog('\nSOCKET SERVICE > -------------------------------------------------------------------------------');
          this.configService.writeLog('SOCKET SERVICE > VERIFICAR SUA CONEXÃO COM A INTERNET.');
          this.configService.writeLog('SOCKET SERVICE > -------------------------------------------------------------------------------\n');
        }
        if (this.subject) {
          this.subject.unsubscribe();
        }
        this.subject = undefined;
        this.create(this.url);
      }
    } catch (e) {
      this.configService.writeError(e);
    }
  }

  public getConnected() {
    return this.isConnected;
  }

  public dispose() {
    if (this.subject) {
      this.subject.unsubscribe();
    }
  }
}
