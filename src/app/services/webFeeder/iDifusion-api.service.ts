import { IDufusionLoggable } from '@services/strategy-pattern-login/iDifusion-loggable';
import { ConfigService } from '@services/webFeeder/config.service';
import { HttpService } from '@services/http-service';
import { Injectable } from '@angular/core';

@Injectable({ providedIn: 'root' })
export class IDifusionApiService implements IDufusionLoggable {

  constructor(
    private _configService: ConfigService,
    private _httpService: HttpService
  ) {
    this._configService.writeLog('IDIFUSION API > [LoginType][API] <<<<<');
  }

  updatePassowrd(login, oldPassword, newPassword) {
    return this._httpService
      .doPost(
        this._configService.getApiWebFeederRest(),
        `services/authfast/difusionChangePassword?login=${login}&oldpassword=${oldPassword}&newpassword=${newPassword}`
      )
      .map(response => {
        if (response) {
          return response;
        }
        return [];
      });
  }

}
