import { Injectable } from '@angular/core';
import { SocketService } from '@services/webFeeder/socket.service';
import { ConfigService } from '@services/webFeeder/config.service';
import { MessageWebFeeder } from '@model/webFeeder/message-web-feeder.model';
import { MessageParameterQuote } from '@model/webFeeder/message-parameter-quote.model';
import { environment } from 'environments/environment';
import { Subject, Observable } from 'rxjs';

export class MessageBusinessBookValues {
  public symbol: string;
  public count: number;
  public quoteTrade: any;
}

@Injectable({
  providedIn: 'root'
})
export class BusinessBookService {
  private _message: MessageBusinessBookValues[] = [];
  private serviceSubject = new Subject<MessageEvent>();
  messagesBusinessBook: Observable<any>;

  constructor(
    private configService: ConfigService,
    private socketService: SocketService
  ) { }

  public createSubject() {
    const newService = {
      name: 'BusinessBookType',
      serviceSubject: this.serviceSubject
    };
    this.socketService.register(newService);
    this.messagesBusinessBook = newService.serviceSubject.map(data => {
      data['parameter'] = data['parameter'].toLowerCase();
      let item = this._message.find(x => x.symbol === data['parameter']);
      if (item) {
        if (data['quoteTrade'] !== undefined) {
          item.quoteTrade = data['quoteTrade'];
        }
      } else {
        item = { symbol: data['parameter'], count: 1, quoteTrade: data['quoteTrade'] }
        this._message.push(item);
        this.configService.writeLog('SOCKET BUSINESSBOOK > Array: (ELSE)');
      }
      return {
        symbol: data['parameter'],
        type: data['type'],
        quoteTrade: item.quoteTrade
      };
    });
  }

  public reconnectSubscribe() {
    this._message.forEach(element => {
      this.configService.writeLog('RECONNECT BUSINESSBOOK > [' + element.symbol + '] <<<<<');
      if (element.count > 0) {
        this.sendMessageBook('1', element.symbol);
      }
    });
  }

  public subscribeBook(symbol: string) {
    if (symbol !== ''
      && symbol !== undefined) {
      symbol = symbol.trim().replace(' ', '').toLowerCase();
      const item = this._message.find(x => x.symbol === symbol);

      if (!item) {
        this._message.push({
          symbol: symbol,
          count: 1,
          quoteTrade: null
        });
      } else {
        item.count += 1;
      }

      this.configService.writeLog('SUBSCRIBE BUSINESSBOOK >> [' + symbol + '] <<<<<');
      this.sendMessageBook('1', symbol, null);
    }
  }

  public unsubscribeBook(symbol: string) {
    if (symbol !== ''
      && symbol !== undefined) {
      const item = this._message.find(x => x.symbol === symbol);
      if (item) {
        item.count -= 1;
        if (item.count <= 0) {
          this.sendMessageBook('0', symbol);
          item.count = 0;
        }
        this.configService.writeLog('UNSUBSCRIBE BUSINESSBOOK [' + item.count + '] [' + symbol + '] <<<<<');
      }
    }
  }

  private sendMessageBook(type: string, symbol: string, numberRows?: string) {
    const dataMsg: MessageWebFeeder = new MessageWebFeeder();
    dataMsg.token = this.configService.getToken();
    dataMsg.module = 'quotes';
    dataMsg.service = 'quoteTrade';
    dataMsg.parameterGet = symbol.toLowerCase();
    if (numberRows === null || numberRows === undefined) {
      numberRows = '30';
    }
    const messageParameter = new MessageParameterQuote();
    messageParameter.subsbribetype = type;
    messageParameter.quantidade = numberRows;
    messageParameter.delay = environment.delay_book_business;
    dataMsg.parameters = messageParameter;
    this.socketService.signService(dataMsg);
  }

}
