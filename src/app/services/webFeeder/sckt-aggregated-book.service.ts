import { ConfigService } from '@services/webFeeder/config.service';
import { Injectable } from '@angular/core';
import { SocketService } from '@services/webFeeder/socket.service';
import { MessageBook } from '@model/webFeeder/message-book.model';
import { MessageWebFeeder } from '@model/webFeeder/message-web-feeder.model';
import { MessageParameterQuote } from '@model/webFeeder/message-parameter-quote.model';
import { environment } from 'environments/environment';
import { Subject, Observable } from 'rxjs';

export class MessageAggregatedBookValues {
  public symbol: string;
  public count: number;
  public book: any;
}

@Injectable({
  providedIn: 'root'
})
export class AggregatedBookService {
  public messagesAggregatedBook: Observable<MessageBook>;
  private _message: MessageAggregatedBookValues[] = [];
  private serviceSubject = new Subject<MessageEvent>();

  constructor(
    private socketService: SocketService,
    private configService: ConfigService
  ) { }

  public createSubject() {
    const newService = {
      name: 'AggregatedBookType',
      serviceSubject: this.serviceSubject
    };

    this.socketService.register(newService);
    this.messagesAggregatedBook = newService.serviceSubject.map(data => {
      let item = this._message.find(x => x.symbol === data['parameter']);

      if (item) {
        if (data['book'] !== undefined) {
          item.book = data['book'];
        }
      } else {
        item = {
          symbol: data['parameter'],
          count: 1,
          book: data['book']
        }
        this._message.push(item);
      }

      return {
        symbol: data['parameter'],
        type: data['type'],
        buy: item.book.A,
        sell: item.book.B,
        errorMessage: item.book.E
      };
    });
  }

  public reconnectSubscribe() {
    this._message.forEach(element => {
      if (element.count > 0) {
        this.sendMessageAggregatedBook('1', element.symbol);
      }
    });
  }

  public subscribeAggregatedBook(symbol: string) {
    if (symbol !== ''
      && symbol !== undefined) {
      symbol = symbol.trim().replace(' ', '').toLowerCase();
      const item = this._message.find(x => x.symbol === symbol);
      let total = 0;

      if (!item) {
        this._message.push({
          symbol: symbol,
          count: 1,
          book: null
        });
      } else {
        item.count += 1;
        total = item.count;
      }
      this.sendMessageAggregatedBook('1', symbol);
      this.configService.writeLog('SUBSCRIBE AGGREGATEDBOOK > [' + total + '] [' + symbol + '] <<<<<');
    }
  }
  public unsubscribeAggregatedBook(symbol: string) {
    if (symbol !== ''
      && symbol !== undefined) {
      const item = this._message.find(x => x.symbol === symbol);
      if (item) {
        item.count -= 1;
        if (item.count <= 0) {
          this.sendMessageAggregatedBook('0', symbol);
          item.count = 0;
        }
        this.configService.writeLog('UNSUBSCRIBE AGGREGATEDBOOK > [' + item.count + '] [' + symbol + '] <<<<<');
      }
    }
  }

  private sendMessageAggregatedBook(type: string, symbol: string) {
    const dataMsg: MessageWebFeeder = new MessageWebFeeder();
    dataMsg.token = this.configService.getToken();
    dataMsg.module = 'quotes';
    dataMsg.service = 'aggregatedBook';
    dataMsg.parameterGet = symbol.toLowerCase();
    const messageParameter = new MessageParameterQuote();
    messageParameter.subsbribetype = type;
    messageParameter.delay = environment.delay_book_aggregated;
    dataMsg.parameters = messageParameter;
    this.socketService.signService(dataMsg);
  }

}
