import { Injectable } from '@angular/core';
import { IQuoteLoggable } from '@services/strategy-pattern-login/iQuote-loggable';
import { Observable } from 'rxjs';
import { DictionaryCollection } from '@utils/dictionary/dictionary-collection';
import { HttpService } from '@services/http-service';
import { ConfigService } from '@services/webFeeder/config.service';
import { Player } from '@model/webFeeder/player.model';

@Injectable({
  providedIn: 'root'
})
export class IQuotesApiService implements IQuoteLoggable {

  private _dicQuoteQuery: DictionaryCollection<any> = new DictionaryCollection<any>();
  private _player: Player[] = [];
  private timeOutPlayer: any;
  private isRequestPlayerName: boolean;

  constructor(
    private _httpService: HttpService,
    private _configService: ConfigService
  ) {
    // this.loadListPlayer();
  }

  quotesQuery(quote: string, n?: number, showOptions?: boolean, showFractionals?: boolean) {
    if (this._configService.isAuthenticadSystem()) {
      if (quote !== '' && quote !== undefined) {
        quote = quote.trim().replace(' ', '').toLowerCase();
        if (quote.length >= 5) {
          const itemList = this._dicQuoteQuery.item(quote);
          if (itemList && itemList.length > 0) {
            return Observable.of(itemList);
          }
        }
        return this._httpService.doGet(this._configService.getApiWebFeederRest(),
          'services/quotes/quotesQuery2?query=' + quote + '&qtdRegisters=5&getFractional=true&getOptions=true')
          .map(response => {
            if (response) {
              const lengthMax = 5;
              let list = response;
              if (list.length > lengthMax) {
                list = list.slice(0, lengthMax);
              }
              this._dicQuoteQuery.add(quote, list);
              return list;
            }
            return undefined;
          });
      } else {
        return Observable.of([]);
      }
    }
  }

  getQuote(quote: string) {
    if (quote) {
      quote = quote.toLowerCase();
      return this._httpService
        .doGet(this._configService.getApiWebFeederRest(), 'services/quotes/quote?query=' + quote.toLowerCase())
        .map(response => {
          if (response) {
            const json = response;
            return json;
          }
          return [];
        });
    } else {
      return Observable.of();
    }
  }

  getVolumeAtPrice(quote: string, period: number) {
    if (quote) {
      return this._httpService
        .doGet(this._configService.getApiWebFeederRest(), 'services/quotes/volumeAtPrice?query='
          + quote.toLowerCase() + '&period=' + period)
        .map(response => {
          if (response) {
            const json = response;
            return json;
          }
          return [];
        });
    } else {
      return Observable.of();
    }
  }


  loadListPlayer() {
    if (this._configService.isAuthenticadSystem() && this._player.length === 0 && !this.isRequestPlayerName) {
      this.isRequestPlayerName = true;
      this.timeOutPlayer = setTimeout(x => {
        if (this._player.length === 0) {
          this.getListPlayer()
            .subscribe(playerList => {
              if (playerList.length > 0) {
                this._player = playerList;
                clearInterval(this.timeOutPlayer);
              }
              this.isRequestPlayerName = false;
            }, error => {
              this.isRequestPlayerName = false;
            });
        }
      }, 1000);
    }
  }

  public getPlayerName(code: string) {
    if (code && this._player.length > 0) {
      code = code + '';
      const playerResult = this._player.filter(item => item.code === code);
      if (playerResult !== undefined && playerResult.length !== undefined && playerResult.length > 0) {
        return playerResult[0].name;
      }
    } else {
      this.loadListPlayer();
    }
    return code;
  }

  private getListPlayer(): Observable<Player[]> {
    return this._httpService
      .doGet(this._configService.getApiWebFeederRest(), 'services/quotes/playerNames')
      .map(response => {
        if (response) {
          const playerList = Array<Player>();
          if (response && response.length > 0) {
            response.forEach(element => {
              if (element.playerName !== undefined) {
                playerList.push({
                  code: element.playerMarketCode,
                  name: element.playerName,
                  market: element.marketCode
                });
              }
            });
          }
          return playerList;
        }
        return [];
      });
  }

}
