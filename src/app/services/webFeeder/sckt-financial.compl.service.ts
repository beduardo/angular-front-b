import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Subject';
import { SocketService } from '@services/webFeeder/socket.service';
import { ConfigService } from '@services/webFeeder/config.service';
import { MessageParameterNegotiation } from '@model/webFeeder/message-parameter-negotiation.model';
import { MessageWebFeeder } from '@model/webFeeder/message-web-feeder.model';
import { MarketEnum } from '@model/enum/market.enum';
import { Constants } from '@utils/constants';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class FinancialComplService {
  messages: Observable<any>;
  private serviceSubject = new Subject<MessageEvent>();

  constructor(
    private socketService: SocketService,
    private configService: ConfigService
  ) { }

  public createSubject() {
    const newService = {
      name: 'FinancialComplSvcByFn',
      serviceSubject: this.serviceSubject,
      fn: (data) => {
        let isFinancialCompl = false;
        if (data && data.financialInformationBean && data.financialInformationBean.length > 0) {
          data.financialInformationBean.forEach(element => {
            if (element.finalMarkToMarketAmount || element.incrementalMarkToMarketAmount) {
              isFinancialCompl = true
            }
          })
        }
        return isFinancialCompl;
      }
    };
    this.socketService.register(newService);
    this.messages = newService.serviceSubject.map(data => {
      return data;
    });
  }
  
  public unsubscribeFinancial(account: string) {
    this.configService.writeLog('UNSUBSCRIBE FINANCIAL > Compl [' + account + '] <<<<<');
    this.sendMessageFinancial(account, MarketEnum.BOVESPA, false);
    this.sendMessageFinancial(account, MarketEnum.BMF, false);
  }

  public subscribeFinancial(account: string) {
    this.configService.writeLog('SUBSCRIBE FINANCIAL > Compl [' + account + '] <<<<<');
    this.sendMessageFinancial(account, MarketEnum.BOVESPA, true);
    this.sendMessageFinancial(account, MarketEnum.BMF, true);
  }

  private sendMessageFinancial(account: string, market: MarketEnum, dispatch: boolean) {
    const dataMsg: MessageWebFeeder = new MessageWebFeeder();
    dataMsg.token = this.configService.getToken();
    dataMsg.module = 'negotiation';
    dataMsg.service = 'financialAccountInformationCompl';
    const messageParameter = new MessageParameterNegotiation();
    messageParameter.account = account;
    messageParameter.market =
      market === MarketEnum.BOVESPA ? Constants.MARKET_BOVESPA : Constants.MARKET_BMF;
    messageParameter.dispatch = dispatch;
    messageParameter.history = true;
    dataMsg.parameters = messageParameter;
    if (market !== undefined) {
      this.socketService.signService(dataMsg);
    }
  }
}
