import { Injectable } from '@angular/core';
import { Subject, Observable } from 'rxjs';
import { SocketService } from '@services/webFeeder/socket.service';
import { ConfigService } from '@services/webFeeder/config.service';
import { MessageWebFeeder } from '@model/webFeeder/message-web-feeder.model';
import { MessageParameterQuote } from '@model/webFeeder/message-parameter-quote.model';
import { environment } from 'environments/environment';

export class MessageBookValues {
  public symbol: string;
  public count: number;
  public book: any;
}

@Injectable({
  providedIn: 'root'
})
export class BookService {
  public messagesBook: Observable<any>;
  private _message: MessageBookValues[] = [];
  private serviceSubject = new Subject<MessageEvent>();

  constructor(
    private socketService: SocketService,
    private configService: ConfigService,
  ) { }

  public createSubject() {
    const newService = {
      name: 'MiniBookType',
      serviceSubject: this.serviceSubject
    };

    this.socketService.register(newService);
    this.messagesBook = newService.serviceSubject.map(data => {
      let item = this._message.find(x => x.symbol === data['parameter']);
      if (item) {
        if (data['book'] !== undefined) {
          item.book = data['book'];
        }
      } else {
        item = {
          symbol: data['parameter'],
          count: 1,
          book: data['book']
        }
        this._message.push(item);
      }
      return {
        symbol: data['parameter'],
        type: data.type,
        buy: item.book.A,
        sell: item.book.B,
        errorMessage: item.book.E
      };
    });
  }

  public reconnectSubscribe() {
    this._message.forEach(element => {
      this.configService.writeLog('RECONNECT Book [' + element.symbol + '] <<<<<');
      if (element.count > 0) {
        this.sendMessageBook('1', element.symbol);
      }
    });
  }

  public subscribeBook(symbol: string) {
    if (symbol !== ''
      && symbol !== undefined) {
      symbol = symbol.trim().replace(' ', '').toLowerCase();
      const item = this._message.find(x => x.symbol === symbol);
      let total = 0;

      if (!item) {
        this._message.push({
          symbol: symbol,
          count: 1,
          book: null
        });
      } else {
        item.count += 1;
        total = item.count;
      }
      this.sendMessageBook('1', symbol, null);
      this.configService.writeLog('Subscribe Book [' + total + '] [' + symbol + '] <<<<<');
    }
  }

  public unsubscribeBook(symbol: string) {
    if (symbol !== ''
      && symbol !== undefined) {
      const item = this._message.find(x => x.symbol === symbol);
      if (item) {
        item.count -= 1;
        if (item.count <= 0) {
          this.sendMessageBook('0', symbol);
          item.count = 0;
        }
        this.configService.writeLog('Unsubscribe Book [' + item.count + '] [' + symbol + '] <<<<<');
      }
    }
  }

  private sendMessageBook(type: string, symbol: string, numberRows?: string) {
    const dataMsg: MessageWebFeeder = new MessageWebFeeder();
    dataMsg.token = this.configService.getToken();
    dataMsg.module = 'quotes';
    dataMsg.service = 'miniBook';
    dataMsg.parameterGet = symbol.toLowerCase();
    if (numberRows === null || numberRows === undefined) {
      numberRows = '15';
    }
    const messageParameter = new MessageParameterQuote();
    messageParameter.subsbribetype = type;
    messageParameter.filter = numberRows;
    messageParameter.delay = environment.delay_book;
    dataMsg.parameters = messageParameter;
    this.socketService.signService(dataMsg);
  }

}
