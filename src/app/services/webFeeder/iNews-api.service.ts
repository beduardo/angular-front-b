import { INewsLoggable } from '../strategy-pattern-login/iNews-loggable';
import { Injectable } from '@angular/core';
import { HttpService } from '@services/http-service';
import { ConfigService } from '@services/webFeeder/config.service';
import { DatePipe } from '@angular/common';
import { Observable } from 'rxjs';

@Injectable({ providedIn: 'root' })
export class INewsApiService implements INewsLoggable {

  constructor(
    private _httpService: HttpService,
    private _configService: ConfigService,
    private datePipe: DatePipe
  ) {
  }

  public getNewsByParameters(text: string, agency: string, dateStart: Date, dateEnd: Date): Observable<any> {
    let query = '?';

    if (text && text !== '') {
      query += 'text=' + text;
    }
    if (query !== '?') {
      query += '&';
    }
    if (agency) {
      query += 'agency=' + agency;
    }
    if (dateStart && query !== '?') {
      query += '&';
    }
    if (dateStart) {
      const datStr = this.datePipe.transform(dateStart, 'ddMMyyyy000002');
      query += 'initialTime=' + datStr;
    }
    if (dateEnd && query !== '?') {
      query += '&';
    }
    if (dateEnd) {
      const datEnd = this.datePipe.transform(dateEnd, 'ddMMyyyy235959');
      query += 'finalTime=' + datEnd;
    }

    return this._httpService
      .doGet(this._configService.getApiWebFeederRest(), 'services/news/newsByParameters' + query, false)
      .map(response => {
        if (response) {
          const json = response;
          return json;
        }
        return [];
      });
  }


}
