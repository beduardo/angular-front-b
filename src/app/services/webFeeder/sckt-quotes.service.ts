import { environment } from './../../../environments/environment';
import { Injectable } from '@angular/core';
import { SocketService } from '@services/webFeeder/socket.service';
import { ConfigService } from '@services/webFeeder/config.service';
import { MessageWebFeeder } from '@model/webFeeder/message-web-feeder.model';
import { MessageParameterQuote } from '@model/webFeeder/message-parameter-quote.model';
import { QuoteSearch } from '@model/webFeeder/quote-search.model';
import { AppUtils } from '@utils/app.utils';
import { Subject, Observable } from 'rxjs';

export class MessageQuoteValues {
  public symbol: string;
  public count: number;
  public values: any;
}

@Injectable({
  providedIn: 'root'
})
export class QuoteService {
  public messagesQuote: Observable<any>;
  private _message: MessageQuoteValues[] = [];
  public quoteSearchList: QuoteSearch[] = [];
  private serviceSubject = new Subject<MessageEvent>();

  constructor(
    private socketService: SocketService,
    private configService: ConfigService
  ) {}

  public createSubject() {
    const newService = {
      name: 'QuoteType',
      serviceSubject: this.serviceSubject
    };
    this.socketService.register(newService);
    this.messagesQuote = newService.serviceSubject.map(data => {
      let item = this._message.find(x => x.symbol === data['parameter']);
      if (item) {
        if (item.values !== null) {
          [-1, 0, 1, 2, 3, 4, 5, 7, 8, 9, 10, 11, 12, 13, 14, 17, 18, 19, 20, 21, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46,
            47, 48, 49, 54, 56, 57, 58, 59, 64, 65, 66, 67, 72, 82, 83, 84, 85, 86, 88, 94, 98, 99, 100, 101, 102, 103, 104,
            105, 106, 107, 108, 110, 111, 112, 116, 121, 123, 127, 128, 130, 134, 135, 10097, 10098, 10099].forEach(el => {
              if (data['values'][el] !== undefined) {
                item.values[el] = data['values'][el];
              }
            });
        } else {
          item['values'] = data['values'];
        }
      } else {
        item = {
          symbol: data['parameter'],
          count: 1,
          values: data['values']
        }
        this._message.push(item);
      }
      const element = item;

      const result = {
        quantityTrades: item['values'][8],
        market: item['values'][44],
        ticksize: item['values'][110],
        interval: item['values'][112],
        symbol: data['parameter'],
        type: data['type'],
        errorMessage: element['values'][-1],
        timeUpdate: element['values'][0],
        dateTrade: element['values'][1],
        lastTrade: element['values'][2],
        bid: element['values'][3],
        ask: element['values'][4],
        hourLastTrade: element['values'][5],
        quantityLast: element['values'][7],
        quantity: element['values'][8],
        volumeAmount: element['values'][9],
        volumeFinancier: element['values'][10],
        high: element['values'][11],
        low: element['values'][12],
        previous: element['values'][13],
        open: element['values'][14],
        // buyTime: element['values'][15],
        // sellTime: element['values'][16],
        volAccBid: element['values'][17],
        volAccAsk: element['values'][18],
        volumeBetterBid: element['values'][19],
        volumeBetterAsk: element['values'][20],
        variation: element['values'][21],
        previousWeek: element['values'][36],
        previousMonth: element['values'][37],
        previousYear: element['values'][38],
        openLastDay: element['values'][39],
        highPriceLastDay: element['values'][40],
        lowPriceLastDay: element['values'][41],
        average: element['values'][42],
        vhDay: element['values'][43],
        marketType: element['values'] ? AppUtils.convertNumber(element['values'][44]) : null,
        assetType: element['values'][45],
        loteDefault: element['values'][46],
        description: element['values'] ? this.ChangeUndefined(element['values'][47]) : null,
        classification: element['values'] ? this.ChangeUndefined(element['values'][48]) : null,
        quotationForm: element['values'][49],
        dateLastNegotiation: element['values'][54],
        directionUnansweredQty: element['values'][56],
        unansweredQty: element['values'][57],
        timeToOpen: element['values'][58],
        timeReprogrammedToOpen: element['values'][59],
        // playerBid: element['values'][60],
        // playerAsk: element['values'][61],
        // codePlayerLastBid: element['values'][62],
        // codePlayerLastAsk: element['values'][63],
        expirationDate: element['values'][64],
        expiration: element['values'][65],
        quantitySymbol: element['values'][66],
        inTheWeek: element['values'][67],
        // inTheYear: element['values'][68],
        // inTheMonth: element['values'][69],
        typeOption: element['values'][72],
        status: element['values'][84],
        theoryPrice: element['values'][82],
        theoryQuantity: element['values'] ? element['values'][83] !== '0.0' ? element['values'][83] : '-' : null,
        exercisePrice: element['values'][85],
        coinVariation: element['values'][86],
        groupPhase: element['values'][88],
        volAVG20: element['values'][94],
        // mCap: element['values'][95],
        // lastTrade7Days: element['values'][97],
        lastTrade1Month: element['values'][98],
        lastTrade1Year: element['values'][99],
        quantityOpenedContractNumber: element['values'][100],
        businesDaysToExpirationDate: element['values'][101],
        calendarDaysToExpirationDate: element['values'][102],
        adjustment: element['values'][103],
        previousAdjustPrice: element['values'][104],
        securityId: element['values'][105],
        tickDirection: element['values'][106],
        tunnelUpperLimit: element['values'][107],
        tunnelLowerLimit: element['values'][108],
        tickSize: element['values'][110],
        minimumAmount: element['values'][111],
        minimumIncrement: element['values'][112],
        coin: element['values'][116],
        exercisePriceOption: element['values'][121],
        contractMultiplier: element['values'][123],
        adjustCurrentRate: element['values'][127],
        adjustPreviousRate: element['values'][128],
        withdrawalsNumberUntilDueDate: element['values'][130],
        deltaVolumeCurrentTime: element['values'][134],
        deltaVolumeAccumulatedHour: element['values'][135],
        // deltaVolumeDaily: element['values'][10094],
        calculoWeeklyVariation: element['values'][10097],
        calculoMonthlyVariation: element['values'][10098],
        calculoYearlyVariation: element['values'][10099]
      };

      // result.auctionVariation = this.getAuctionVariation(element.values[82], element.values[2]);
      // result.timeRemainingAuction = this.getAuctionScheduleTime(element.values[58]);
      if (result.symbol.toLowerCase().includes('cfd')) {
        result.variation = this.getVariationCfd(result);
      }
      return result;
    });
  }

  private getVariationCfd(result) {
    if (result.lastTrade && result.previous) {
      const lastTradeTemp = AppUtils.convertNumber(result.lastTrade.toString());
      const previousTemp = AppUtils.convertNumber(result.previous.toString());
      return result.variation = (1 - (previousTemp / lastTradeTemp)) * 100;
    } else {
      return result.variation = 0;
    }
  }

  public subscribeQuote(symbol: string, componentName?: string, filter?: string): any {
    if (symbol && symbol !== '' && symbol !== undefined) {
      symbol = symbol.trim().replace(' ', '').toLowerCase();
      const item = this._message.filter(x => x.symbol === symbol);
      let total = 0;
      if (item.length === 0) {
        this._message.push({ symbol: symbol, count: 1, values: null });
        total = 1;
      } else if (item.length > 0) {
        item[0].count += 1;
        total = item[0].count;
        if (item[0].values != null && item[0].values[-1] !== undefined) {
          return item[0].values[-1];
        }
      }
      if (filter === null || filter === undefined) {
        filter = '-1,0,1,2,3,4,5,7,8,9,10,11,12,13,14,17,18,19,20,21,36,' +
          '37,38,39,40,41,42,43,44,45,46,47,48,49,54,56,57,58,59,64,65,66,' +
          '67,72,82,83,84,85,86,88,94,98,99,100,101,102,103,104,105,106,107,' +
          '108,110,111,112,116,121,123,127,128,130,134,135,10097,10098,10099';
      }
      this.configService.writeLog('<<<< Subscribe Quote [' + total + '][' + symbol + '][' + componentName + '] >>>> \n');
      this.sendMessageQuote('1', symbol, filter);
    }
    return '';
  }

  public unsubscribeQuote(symbol: string, componentName?: string) {
    if (symbol && symbol !== '' && symbol !== undefined) {
      symbol = symbol.trim().replace(' ', '').toLowerCase();
      const item = this._message.filter(x => x.symbol === symbol);
      if (item.length > 0) {
        item[0].count -= 1;
        this.configService.writeLog('<<<< Unsubscribe Quote [' + item[0].count + '][' + symbol + '][' + componentName + '] >>>> \n');
        if (item[0].count <= 0) {
          this.sendMessageQuote('0', symbol);
          item[0].count = 0;
        }
      }
    }
  }

  acessListQuotes(symbol) {
    const quote = this._message.filter(x => x.symbol === symbol);
    return quote.length > 0 ? quote : undefined;
  }

  private sendMessageQuote(type: string, symbol: string, filter?: string) {
    const dataMsg: MessageWebFeeder = new MessageWebFeeder();
    dataMsg.token = this.configService.getToken();
    dataMsg.module = 'quotes';
    dataMsg.service = 'quote';
    dataMsg.parameterGet = symbol.toLowerCase();

    const messageParameter = new MessageParameterQuote();
    messageParameter.subsbribetype = type;
    messageParameter.filter = filter;
    messageParameter.delay = environment.delay_quote;
    dataMsg.parameters = messageParameter;
    this.socketService.signService(dataMsg);
  }


  private ChangeUndefined(text: any) {
    if (text) {
      return text;
    }
    return '-';
  }

}