import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Subject';
import { SocketService } from '@services/webFeeder/socket.service';
import { ConfigService } from '@services/webFeeder/config.service';
import { MessagePosition } from '@model/webFeeder/custody.model';
import { MessageParameterNegotiation } from '@model/webFeeder/message-parameter-negotiation.model';
import { MessageWebFeeder } from '@model/webFeeder/message-web-feeder.model';
import { MarketEnum } from '@model/enum/market.enum';
import { Constants } from '@utils/constants';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PositionService {
  private serviceSubject = new Subject<MessageEvent>();
  messagesPosition: Observable<any>;
  rowAllPosition: MessagePosition[] = [];
  lstPosition: MessagePosition[] = [];

  constructor(
    private socketService: SocketService,
    private configService: ConfigService
  ) { }

  public createSubject() {
    const newService = {
      name: 'PositionSvcByFn',
      serviceSubject: this.serviceSubject,
      fn: (data) => {
        let isPOsition = false;
        if (data && data.listBeans && data.listBeans.length > 0) {
          data.listBeans.forEach(element => {
            if (element.marketID) {
              isPOsition = true
            }
          })
        }
        return isPOsition;
      }
    };
    this.socketService.register(newService);
    this.messagesPosition = newService.serviceSubject.map(data => {
      const messageReturn: MessagePosition[] = [];
      data['listBeans'].forEach(element => {
          const msgPosition: MessagePosition = {
            type: 'position',
            account: element.account.value ? element.account.value : '0',
            market: element.marketID.value ? element.marketID.value : '0',
            marketCode: element.marketID.code ? element.marketID.code : '0',
            quote: element.quote.value ? element.quote.value : '0',
            posMainRptID: element.posMainRptID.value ? element.posMainRptID.value : '0',
            quoteType: element.quoteType.value ? element.quoteType.value : '0',
            currentQuantity: element.currentQuantity.value ? element.currentQuantity.value : '0',
            lastTrade: element.lastTrade.value ? element.lastTrade.value.replace('R$ ', '').replace('Pts. ', '') : '0',
            previusTrade: element.previusTrade.value ? element.previusTrade.value.replace('R$ ', '').replace('Pts. ', '') : '0',
            averagePrice: element.averagePrice.value ? element.averagePrice.value.replace('R$ ', '').replace('Pts. ', '') : '0',
            initQuantity: element.initQuantity.value ? element.initQuantity.value : '0',
            quantityD1: element.quantityD1.value ? element.quantityD1.value : '0',
            quantityD2: element.quantityD2.value ? element.quantityD2.value : '0',
            quantityD3: element.quantityD3.value ? element.quantityD3.value : '0',
            priceAverageInit: element.priceAverageInit.value
              ? element.priceAverageInit.value.replace('R$ ', '').replace('Pts. ', '') : '0',
            valueCurrent: element.valueCurrent.value ? element.valueCurrent.value.replace('R$ ', '').replace('Pts. ', '') : '0',
            valueInit: element.valueInit.value ? element.valueInit.value.replace('R$ ', '').replace('Pts. ', '') : '0',
            valueSpent: element.valueSpent.value ? element.valueSpent.value.replace('R$ ', '').replace('Pts. ', '') : '0',
            profitLoss: element.profitLoss.value ? element.profitLoss.value.replace('R$ ', '').replace('Pts. ', '') : '0',
            profitLossPercent: element.profitLossPercent.value
              ? element.profitLossPercent.value.replace('R$ ', '').replace('Pts. ', '') : '0',
            openBuy: element.openBuy.value ? element.openBuy.value : '0',
            openSell: element.openSell.value ? element.openSell.value : '0',
            executedBuy: element.executedBuy.value ? element.executedBuy.value : '0',
            executedSell: element.executedSell.value ? element.executedSell.value : '0',
            expired: element.expired.value ? element.expired.value : '0',
            quantityBlocked: element.quantityBlocked.value ? element.quantityBlocked.value : '0',
            quantityReleased: element.quantityReleased.value ? element.quantityReleased.value : '0',
            lastTradeDate: element.lastTradeDate.value ? element.lastTradeDate.value : '0',
            qtdTomadorBTC: element.qtdTomadorBTC.value ? element.qtdTomadorBTC.value : '0',
            qtdDoadaBTC: element.qtdDoadaBTC.value ? element.qtdDoadaBTC.value : '0',
            precoMedioTomadorBTC: element.precoMedioTomadorBTC.value ? element.precoMedioTomadorBTC.value : '0',
            precoMedioDoadorBTC: element.precoMedioDoadorBTC.value ? element.precoMedioDoadorBTC.value : '0',
            netDay: element.netDay.value ? element.netDay.value : '0',
            netPosition: element.netPosition.value ? element.netPosition.value : '0',
            netDaytrade: element.netDaytrade.value ? element.netDaytrade.value : '0',
            averagePricePos: element.averagePricePos.value
              ? element.averagePricePos.value.replace('R$ ', '').replace('Pts. ', '') : '0',
            openProfitLoss: element.openProfitLoss.value
              ? element.openProfitLoss.value.replace('R$ ', '').replace('Pts. ', '') : '0',
            closedProfitLoss: element.closedProfitLoss.value
              ? element.closedProfitLoss.value.replace('R$ ', '').replace('Pts. ', '') : '0',
            buyAvgPrice: element.buyAvgPrice.value ? element.buyAvgPrice.value.replace('R$ ', '').replace('Pts. ', '') : '0',
            sellAvgPrice: element.sellAvgPrice.value ? element.sellAvgPrice.value.replace('R$ ', '').replace('Pts. ', '') : '0',
            quotationForm: element.quotationForm.value ? element.quotationForm.value : '0',
            priorSetllPrice: element.priorSetllPrice.value
              ? element.priorSetllPrice.value.replace('R$ ', '').replace('Pts. ', '') : '0',
            securityType: element.SecurityType.value ? element.SecurityType.value : '0',
            totalPurchaseValue: element.totalPurchaseValue.value ? element.totalPurchaseValue.value : '0',
            totalSaleValue: element.totalSaleValue.value ? element.totalSaleValue.value : '0',
            executedDaytradePurchaseQty: element.executedDaytradePurchaseQty.value ? element.executedDaytradePurchaseQty.value : '0',
            executedDaytradeSaleQty: element.executedDaytradeSaleQty.value ? element.executedDaytradeSaleQty.value : '0',
            notExecutedPurchaseDaytradeQty: element.notExecutedPurchaseDaytradeQty.value
              ? element.notExecutedPurchaseDaytradeQty.value : '0',
            notExecutedSaleDaytradeQty: element.notExecutedSaleDaytradeQty.value ? element.notExecutedSaleDaytradeQty.value : '0',
            avgValueDaytradeToday: element.avgValueDaytradeToday.value
              ? element.avgValueDaytradeToday.value.replace('R$ ', '').replace('Pts. ', '') : '0',
            openProfitLossDaytrade: element.openProfitLossDaytrade.value
              ? element.openProfitLossDaytrade.value.replace('R$ ', '').replace('Pts. ', '') : '0',
            valueAdjustedAmount: element.valueAdjustedAmount.value
              ? element.valueAdjustedAmount.value.replace('R$ ', '').replace('Pts. ', '') : '0',
            openAvgValueDaytradeToday: element.openAvgValueDaytradeToday.value
              ? element.openAvgValueDaytradeToday.value.replace('R$ ', '').replace('Pts. ', '') : '0',
            closedProfitLossDaytrade: element.closedProfitLossDaytrade.value
              ? element.closedProfitLossDaytrade.value.replace('R$ ', '').replace('Pts. ', '') : '0',
          };
          messageReturn.push(msgPosition);
      });
      return messageReturn;
    });
  }

  public unsubscribePosition(account: string, market: number) {
    const marketCodeTemp = market === MarketEnum.BOVESPA ? Constants.MARKET_BOVESPA : Constants.MARKET_BMF;
    this.configService.writeLog('UNSUBSCRIBE POSITION > [' + account + '][' + marketCodeTemp + '] <<<<<');
    this.sendMessagePosition(account, market, false);
  }

  public subscribePosition(account: string, market: number) {
    const marketCodeTemp = market === MarketEnum.BOVESPA ? Constants.MARKET_BOVESPA : Constants.MARKET_BMF;
    this.configService.writeLog('SUBSCRIBE POSITION > [' + account + '][' + marketCodeTemp + '] <<<<<');
    this.sendMessagePosition(account, market, true);
  }

  private sendMessagePosition(account: string, market: MarketEnum, dispatch: boolean) {
    const dataMsg: MessageWebFeeder = new MessageWebFeeder();
    dataMsg.token = this.configService.getToken();
    dataMsg.module = 'negotiation';
    dataMsg.service = 'position';
    const messageParameter = new MessageParameterNegotiation();
    messageParameter.account = account;
    messageParameter.market = market === MarketEnum.BOVESPA ? Constants.MARKET_BOVESPA : Constants.MARKET_BMF;
    messageParameter.dispatch = dispatch;
    messageParameter.history = true;
    dataMsg.parameters = messageParameter;
    if (market !== undefined) {
      this.socketService.signService(dataMsg);
    }
  }

}
