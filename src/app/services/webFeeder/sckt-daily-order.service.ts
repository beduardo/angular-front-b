import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Subject';
import { SocketService } from '@services/webFeeder/socket.service';
import { ConfigService } from '@services/webFeeder/config.service';
import { MessageParameterNegotiation } from '@model/webFeeder/message-parameter-negotiation.model';
import { MessageWebFeeder } from '@model/webFeeder/message-web-feeder.model';
import { MarketEnum } from '@model/enum/market.enum';
import { MessageDailyOrder } from '@model/webFeeder/daily-message.model';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DailyOrderService {
  messagesDailyOrder: Observable<MessageDailyOrder[]>;
  rowOpenOrdersData: any[];
  private serviceSubject = new Subject<MessageEvent>();

  constructor(
    private socketService: SocketService,
    private configService: ConfigService
  ) { }

  public createSubject() {
    const newService = {
      name: 'DailyOrderSvcByFn',
      serviceSubject: this.serviceSubject,
      fn: (data) => {
        let isOrder = false;
        if (data && data.listBeans && data.listBeans.length > 0) {
          data.listBeans.forEach(element => {
            if (element.orderID) {
              isOrder = true
            }
          })
        }
        return isOrder;
      }
    };

    this.socketService.register(newService);
    this.messagesDailyOrder = newService.serviceSubject.map(data => {
      const messageReturn: MessageDailyOrder[] = [];
      data['listBeans'].forEach(element => {
        messageReturn.push({
          quote: element.quote ? element.quote.value : null,
          buyOrSell: element.buyOrSell ? element.buyOrSell.value : null,
          account: element.account ? element.account.value : null,
          stateCode: element.state ? element.state.code : null,
          state: element.state ? element.state.value : null,
          quantitySupplied: element.quantitySupplied ? element.quantitySupplied.value : null,
          quantityExecuted: element.quantityExecuted ? element.quantityExecuted.value : null,
          quantityRemaining: element.quantityRemaining ? element.quantityRemaining.value : null,
          price: element.price ? element.price.value : null,
          priceAverage: element.priceAverage ? element.priceAverage.value : null,
          typeCode: element.type ? element.type.code : null,
          type: element.type ? element.type.value : null,
          maturity: element.maturity ? element.maturity.value : null,
          mercado: element.mercado ? element.mercado.code : null,
          transactTime: element.transactTime ? element.transactTime.value : null,
          side: element.side ? element.side.value : null,
          currentCumQty: element.currentCumQty ? element.currentCumQty.value : null,
          currentAvgPx: element.currentAvgPx ? element.currentAvgPx.value : null,
          orderID: element.orderID ? element.orderID.value : null,
          orderStrategy: element.orderStrategy ? element.orderStrategy.value : null,
          timeInForceCode: element.timeInForce ? element.timeInForce.code : null,
          timeInForce: element.timeInForce ? element.timeInForce.value : null,
          clOrdID: element.clOrdID ? element.clOrdID.value : null,
          username: element.username ? element.username.value : null,
          text: element.text ? element.text.value : null,
          movingStart: element.movingStart ? element.movingStart.value : null,
          initialChange: element.initialChange ? element.initialChange.value : null,
          priceStopGain: element.priceStopGain ? element.priceStopGain.value : null,
          priceStop: element.priceStop ? element.priceStop.value : null,
          maxFloor: element.maxFloor ? element.maxFloor.value : null,
          minQty: element.minQty ? element.minQty.value : null,
          orderTag: element.orderTag ? element.orderTag.value : null,
          price2: element.price2 ? element.price2.value : null
        });
      });
      return messageReturn;
    });
  }

  public subscribeDailyOrderBovespa(account: string) {
    this.configService.writeLog('SUBSCRIBE DAILYORDER > Bovespa [' + account + '] <<<<<');
    this.sendMessageDailyOrder(account, MarketEnum.BOVESPA, true);
  }

  public subscribeDailyOrderBmf(account: string) {
    this.configService.writeLog('SUBSCRIBE DAILYORDER > Bmf [' + account + '] <<<<<');
    this.sendMessageDailyOrder(account, MarketEnum.BMF, true);
  }

  public unsubscribeDailyOrder(account: string) {
    this.sendMessageDailyOrder(account, MarketEnum.BOVESPA, false);
    this.configService.writeLog('UNSUBSCRIBE DAILYORDER > Bovespa [' + account + '] <<<<<');
    this.sendMessageDailyOrder(account, MarketEnum.BMF, false);
    this.configService.writeLog('UNSUBSCRIBE DAILYORDER > BMF [' + account + '] <<<<<');
  }

  private sendMessageDailyOrder(account: string, market: MarketEnum, dispatch: boolean) {
    const dataMsg: MessageWebFeeder = new MessageWebFeeder();
    dataMsg.token = this.configService.getToken();
    dataMsg.module = 'negotiation';
    dataMsg.service = 'dailyOrder';
    const messageParameter = new MessageParameterNegotiation();
    messageParameter.account = account;
    messageParameter.dispatch = dispatch;
    messageParameter.history = true;
    if (market === 1) {
      messageParameter.market = 'XBSP';
    } else {
      messageParameter.market = 'XBMF';
    }
    dataMsg.parameters = messageParameter;
    this.socketService.signService(dataMsg);
  }
}
