
import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Subject';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { ConfigService } from '@services/webFeeder/config.service';
import { DailyOrderService } from '@services/webFeeder/sckt-daily-order.service';
import { StatusOmsService } from '@services/webFeeder/sckt-status-oms.service';
import { StatusDifusionService } from '@services/webFeeder/sckt-status-difusion.service';
import { AggregatedBookService } from '@services/webFeeder/sckt-aggregated-book.service';
import { BookService } from '@services/webFeeder/sckt-book.service';
import { BusinessBookService } from '@services/webFeeder/sckt-business-book.service';
import { QuoteService } from '@services/webFeeder/sckt-quotes.service';
import { MessageLogin } from '@model/webFeeder/message-login.model';
import { FinancialComplService } from '@services/webFeeder/sckt-financial.compl.service';
import { FinancialService } from '@services/webFeeder/sckt-financial.service';
import { HttpService } from '@services/http-service';
import { ChartService } from './sckt-chart.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { PositionService } from './sckt-position.service';
import { SocketService } from './socket.service';

@Injectable({
  providedIn: 'root'
})
export class TokenService {
  public messagesLogin: Subject<MessageLogin>;
  public token: any;
  public passReconnect = false;
  private _reconnect = new BehaviorSubject<boolean>(false);
  reconnect$ = this._reconnect.asObservable();

  constructor(
    private dailyOrderService: DailyOrderService,
    private financialComplService: FinancialComplService,
    private financialService: FinancialService,
    private positionService: PositionService,
    private quoteService: QuoteService,
    private chartService: ChartService,
    private bookService: BookService,
    private aggregatedBookService: AggregatedBookService,
    private businessBookService: BusinessBookService,
    private configService: ConfigService,
    private httpService: HttpService,
    private statusOmsService: StatusOmsService,
    private statusDifusionService: StatusDifusionService,
    private httpClient: HttpClient,
    private socketService: SocketService
  ) {
  }

  public sendStatus(onReconnect: boolean): boolean {
    if (onReconnect) {
      this._reconnect.next(true);
    }
    return true;
  }

  assignServices() {
    this.socketService.connect();
    this.dailyOrderService.createSubject();
    this.quoteService.createSubject();
    this.chartService.createSubject();
    this.bookService.createSubject();
    this.aggregatedBookService.createSubject();
    this.businessBookService.createSubject();
    this.financialService.createSubject();
    this.financialComplService.createSubject();
    this.statusOmsService.createSubject();
    this.statusDifusionService.createSubject();
    this.positionService.createSubject();
  }

  public loginWS(guidUser: string) {
    return this.httpService
      .doGet(this.configService.getApiWebFeederRest(), 'services/AuthFast/LoginWS?guidUser=' + guidUser)
      .map(response => {
        if (response) {
          const json = response;
          return json;
        }
        return [];
      });
  }

  public revokeAPIGatewayToken() {
    const token = this.configService.getTokenApi();
    const httpHeaders = new HttpHeaders({
      'Content-Type': 'application/json',
      'Access-Control-Allow-Origin': '*',
      'Access-Control-Allow-Headers': 'Origin, X-Requested-With, Content-Type, Accept, Authorization',
      'Access-Control-Allow-Methods': 'POST, GET, PUT, DELETE',
      'Authorization': 'Bearer ' + token,
      // 'x-source-request': 'FastTradeWeb',
      // 'x-source-tracker-id': AppUtils.newguidgen()
    });
    const options = {
      headers: httpHeaders
    };
    return this.httpClient
      .post(
        this.configService.getApiWebFeederRest() +
        `/services/Authenticate/revoke`,
        {},
        options)
      .map(response => {
        if (response) {
          return response;
        }
        return [];
      })
      .catch(error => {
        throw error;
      });
  }
}
