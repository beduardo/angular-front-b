import { Injectable } from '@angular/core';
import { IQuoteLoggable } from '@services/strategy-pattern-login/iQuote-loggable';
import { HttpService } from '@services/http-service';
import { ConfigService } from '@services/webFeeder/config.service';
import { DictionaryCollection } from '@utils/dictionary/dictionary-collection';
import { Observable } from 'rxjs';
import { Player } from '@model/webFeeder/player.model';

@Injectable({ providedIn: 'root' })
export class IQuotesWso2Service implements IQuoteLoggable {

  private _dicQuoteQuery: DictionaryCollection<any> = new DictionaryCollection<any>();
  private _player: Player[] = [];
  private timeOutPlayer: any;
  private isRequestPlayerName: boolean;
  private vezes = 0;

  constructor(
    private _httpService: HttpService,
    private _configService: ConfigService
  ) {
    // this.loadListPlayer();
  }

  quotesQuery(quote: string, n?: number, showOptions?: boolean, showFractionals?: boolean) {
    if (this._configService.isAuthenticadSystem()) {
      const nResults = n ? n : 5;
      const options = showOptions !== undefined ? showOptions : true;
      const fractionals = showFractionals !== undefined ? showFractionals : true;
      quote = quote.toLowerCase();
      if (quote !== '' && quote !== undefined) {
        quote = quote.trim().replace(' ', '').toLowerCase();
        if (quote.length >= 5) {
          const itemList = this._dicQuoteQuery.item(quote);
          if (itemList && itemList.length > 0) {
            return Observable.of(itemList);
          }
        }
        return this._httpService.doGet(this._configService.getApiWebFeederRest(),
          'services/quotes/quotesQuery?query=' + quote + '&n=' + nResults + '&o=' + options + '&f=' + fractionals)
          .map(response => {
            if (response) {
              const lengthMax = nResults;
              let list = response;
              if (list.length > lengthMax) {
                list = list.slice(0, lengthMax);
              }
              this._dicQuoteQuery.add(quote, list);
              return list;
            }
            return undefined;
          });
      }
    } else {
      return Observable.of([]);
    }
  }

  getQuote(quote: string) {
    if (quote) {
      quote = quote.trim().toLowerCase();
      return this._httpService
        .doGet(this._configService.getApiWebFeederRest(), 'services/quotes/quote/' + quote)
        .map(response => {
          if (response) {
            return response;
          }
          return [];
        });
    } else {
      return Observable.of();
    }
  }

  getVolumeAtPrice(quote: string, period: number) {
    if (quote) {
      let query = '';
      if (period > 0) {
        query = quote.toLowerCase() + '/' + period;
      } else {
        query = quote.toLowerCase();
      }
      return this._httpService
        .doGet(
          this._configService.getApiWebFeederRest(),
          'services/quotes/volumeAtPrice/' + query)
        .map(response => {
          if (response) {
            const json = response;
            return json;
          }
          return [];
        });
    } else {
      return Observable.of();
    }
  }

  loadListPlayer() {
    if (this._configService.isAuthenticadSystem() && this._player.length === 0 && !this.isRequestPlayerName) {
      this.isRequestPlayerName = true;
      this.timeOutPlayer = setTimeout(x => {
        if (this._player.length === 0) {
          this.getListPlayer()
            .subscribe(playerList => {
              if (playerList.length > 0) {
                this._player = playerList;
                clearInterval(this.timeOutPlayer);
              }
              this.isRequestPlayerName = false;
            }, error => {
              this.isRequestPlayerName = false;
            });
        }
      }, 1000);
    }
  }

  getPlayerName(code: string) {
    if (code && this._player.length > 0) {
      code = code + '';
      const playerResult = this._player.filter(item => item.code === code);
      if (playerResult !== undefined && playerResult.length !== undefined && playerResult.length > 0) {
        return playerResult[0].name;
      }
    } else {
      this.loadListPlayer();
    }
    return code;
  }

  private getListPlayer(market?: number): Observable<Player[]> {
    let urlWF = '';
    if (market) {
      urlWF = 'services/quotes/playerNames?market=' + market;
    } else {
      urlWF = 'services/quotes/playerNames';
    }
    return this._httpService
      .doGet(this._configService.getApiWebFeederRest(), urlWF)
      .map(response => {
        if (response) {
          const json = response;
          const playerList = Array<Player>();
          if (json && json.length > 0) {
            json.forEach(element => {
              if (element.playerName !== undefined) {
                playerList.push({
                  code: element.playerMarketCode,
                  name: element.playerName,
                  market: element.marketCode
                });
              }
            });
          }
          return playerList;
        }
        return [];
      });
  }


}
