import { Injectable } from '@angular/core';
import { SocketService } from '@services/webFeeder/socket.service';
import { ConfigService } from '@services/webFeeder/config.service';
import { MessageWebFeeder } from '@model/webFeeder/message-web-feeder.model';
import { MessageParameterChart } from '@model/webFeeder/message-parameter-chart.model';
import { HttpService } from '@services/http-service';
import { Subject, Observable } from 'rxjs';

export class MessageCandleChartValues {
  quote: string;
  count: number;
  candles: any;
  error: string;
}

@Injectable({
  providedIn: 'root'
})
export class ChartService {
  public messagesChart: Observable<any>;
  private _message: MessageCandleChartValues[] = [];
  private serviceSubject = new Subject<MessageEvent>();


  constructor(
    private socketService: SocketService,
    private configService: ConfigService
  ) {
  }

  createSubject() {
    const newService = {
      name: 'ChartSvcByFn',
      serviceSubject: this.serviceSubject,
      fn: (data) => data && data.response_type && data.response_type.toString().toLowerCase() == 'gch'
    };

    this.socketService.register(newService);
    this.messagesChart = newService.serviceSubject.map(data => {
      let item = this._message.find(x => x.quote === data['quote']);

      if (!item) {
        item = {
          quote: data['quote'],
          count: 1,
          candles: data['candles'],
          error: data['error']
        }
        this._message.push(item);
      } else {
        if (data['error'] !== '') {
          item.candles = [];
        } else if (data['candles'] !== undefined) {
          data['candles'].forEach(element => {
            if (element !== undefined && item.candles.length > 0) {
              if (element.date > item.candles[item.candles.length - 1].date) {
                item.candles.push(element);
              }
            } else if (element !== undefined && item.candles.length === 0) {
              item.candles.push(element);
            }

          });
        }
      }
      let msgError = item.error;
      if (item.candles.length > 0) {
        msgError = '';
      }
      return {
        quote: item.quote,
        candleChart: item.candles,
        errorMessage: msgError
      };
    });
  }

  subscribeChart(symbol: string, period: string, iniDate: string) {
    if (symbol !== '' && symbol !== undefined) {
      symbol = symbol.trim().replace(' ', '').toLowerCase();
    }
    this.configService.writeLog('SUBSCRIBE CHART > [' + symbol + '] <<<<<');
    this.sendMessageChart(symbol, period, 'true', iniDate);
  }

  unsubscribeChart(symbol: string, period: string) {
    this.configService.writeLog('UNSUBSCRIBE CHART > [' + symbol + '] <<<<<');
    this.sendMessageChart(symbol, period, 'false', null);
  }

  private sendMessageChart(symbol: string, period: string, dispatch: string, iniDate: string) {
    if (!iniDate) {
      const today = new Date();
      iniDate = `${today.getFullYear()}${(today.getMonth() + 1) < 10 ? '0'
        + (today.getMonth() + 1) : (today.getMonth() + 1)}${today.getDate() < 10 ? '0' + today.getDate() : today.getDate()}0600`;
    }
    if (!period) {
      period = '5';
    }
    const dataMsg: MessageWebFeeder = new MessageWebFeeder();
    dataMsg.token = this.configService.getToken();
    dataMsg.module = 'chart';
    dataMsg.service = 'candleChart';
    const messageParameter = new MessageParameterChart();
    messageParameter.quote = symbol.toLowerCase();
    messageParameter.period = period;
    messageParameter.realTime = 'true';
    messageParameter.dispatch = dispatch;
    messageParameter.initDate = iniDate;
    dataMsg.parameters = messageParameter;
    this.socketService.signService(dataMsg);
  }

}
