import { element } from 'protractor';
import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Subject';
import { SocketService } from '@services/webFeeder/socket.service';
import { ConfigService } from '@services/webFeeder/config.service';
import { MessageParameterNegotiation } from '@model/webFeeder/message-parameter-negotiation.model';
import { MessageWebFeeder } from '@model/webFeeder/message-web-feeder.model';
import { MarketEnum } from '@model/enum/market.enum';
import { Constants } from '@utils/constants';
import { FinancialAccountInformation } from '@model/webFeeder/financial.model';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class FinancialService {
  messages: Observable<FinancialAccountInformation[]>;
  private serviceSubject = new Subject<MessageEvent>();

  constructor(
    private socketService: SocketService,
    private configService: ConfigService
  ) { }

  public createSubject() {
    const newService = {
      name: 'FinancialSvcByFn',
      serviceSubject: this.serviceSubject,
      fn: (data) => {
        let isFinancial = false;
        if (data && data.financialInformationBean && data.financialInformationBean.length > 0) {
          data.financialInformationBean.forEach(element => {
            if (element.saldoAbertura || element.saldoEmConta) {
              isFinancial = true;
              return
            }
          })
        }
        return isFinancial;
      }
    };

    this.socketService.register(newService);
    this.messages = newService.serviceSubject.map(data => {
      const messageReturn: FinancialAccountInformation[] = [];
      data['financialInformationBean'].forEach(element => {
          const msgFinancial: FinancialAccountInformation = {
            type: 'financialAccountInformation',
            marketID: element.marketID ? element.marketID.value : null,
            account: element.account ? element.account.value : null,
            financialAccountRejReason: element.financialAccountRejReason ? element.financialAccountRejReason.value : null,
            text: element.text ? element.text.value : null,
            saldoAbertura: element.saldoAbertura ? element.saldoAbertura.value : null,
            saldoEmConta: element.saldoEmConta ? element.saldoEmConta.value : null,
            contaMargemTotal: element.contaMargemTotal ? element.contaMargemTotal.value : null,
            contaMargemUtilizada: element.contaMargemUtilizada ? element.contaMargemUtilizada.value : null,
            proventos: element.proventos ? element.proventos.value : null,
            tesouroDireto: element.tesouroDireto ? element.tesouroDireto.value : null,
            clubes: element.clubes ? element.clubes.value : null,
            rendaFixa: element.rendaFixa ? element.rendaFixa.value : null,
            fundos: element.fundos ? element.fundos.value : null,
            limiteOperacional: element.limiteOperacional ? element.limiteOperacional.value : null,
            alavancagemVista: element.alavancagemVista ? element.alavancagemVista.value : null,
            alavancagemOpcoes: element.alavancagemOpcoes ? element.alavancagemOpcoes.value : null,
            alavancagemBMF: element.alavancagemBMF ? element.alavancagemBMF.value : null,
            lucroPrejuizoOpen: element.lucroPrejuizoOpen ? element.lucroPrejuizoOpen.value : null,
            lucroPrejuizoClose: element.lucroPrejuizoClose ? element.lucroPrejuizoClose.value : null,
            lucroPrejuizoTotal: element.lucroPrejuizoTotal ? element.lucroPrejuizoTotal.value : null,
            patrimonioInicial: element.patrimonioInicial ? element.patrimonioInicial.value : null,
            patrimonioOnline: element.patrimonioOnline ? element.patrimonioOnline.value : null,
            creditEntryDay0: element.creditEntryDay0 ? element.creditEntryDay0.value : null,
            creditEntryDay1: element.creditEntryDay1 ? element.creditEntryDay1.value : null,
            creditEntryDay2: element.creditEntryDay2 ? element.creditEntryDay2.value : null,
            creditEntryDay3: element.creditEntryDay3 ? element.creditEntryDay3.value : null,
            debitEntryDay0: element.debitEntryDay0 ? element.debitEntryDay0.value : null,
            debitEntryDay1: element.debitEntryDay1 ? element.debitEntryDay1.value : null,
            debitEntryDay2: element.debitEntryDay2 ? element.debitEntryDay2.value : null,
            debitEntryDay3: element.debitEntryDay3 ? element.debitEntryDay3.value : null,
            saldoProjOnline: element.saldoProjOnline ? element.saldoProjOnline.value : null,
            saldoD0: element.saldoD0 ? element.saldoD0.value : null,
            saldoD1: element.saldoD1 ? element.saldoD1.value : null,
            saldoD2: element.saldoD2 ? element.saldoD2.value : null,
            saldoD3: element.saldoD3 ? element.saldoD3.value : null,
            margemGarantias: element.margemGarantias ? element.margemGarantias.value : null,
            consideradoPl: element.consideradoPl ? element.consideradoPl.value : null,
            margemBMFLivre: element.margemBMFLivre ? element.margemBMFLivre.value : null,
            ajustePregaoBMF: element.ajustePregaoBMF ? element.ajustePregaoBMF.value : null,
            ajustePregaoBOV: element.ajustePregaoBOV ? element.ajustePregaoBOV.value : null,
            saldoProjetadoInicial: element.saldoProjetadoInicial ? element.saldoProjetadoInicial.value : null,
            taxas: element.taxas ? element.taxas.value : null,
            custosCorretagemBov: element.custosCorretagemBov ? element.custosCorretagemBov.value : null,
            custosCorretagemBMF: element.custosCorretagemBMF ? element.custosCorretagemBMF.value : null,
            emolumentos: element.emolumentos ? element.emolumentos.value : null,
            custodiaDesagiadaOpcoes: element.custodiaDesagiadaOpcoes ? element.custodiaDesagiadaOpcoes.value : null,
            custodiaDesagiadaVista: element.custodiaDesagiadaVista ? element.custodiaDesagiadaVista.value : null,
            margemGarantiaBovTotal: element.margemGarantiaBovTotal ? element.margemGarantiaBovTotal.value : null,
            margemGarantiaBovConsideradoPL: element.margemGarantiaBovConsideradoPL
              ? element.margemGarantiaBovConsideradoPL.value : null,
            openProfitLossDaytrade: element.openProfitLossDaytrade ? element.openProfitLossDaytrade.value : null,
            closeProfitLossDaytrade: element.closeProfitLossDaytrade ? element.closeProfitLossDaytrade.value : null,
            dayTradeAdjustment: element.dayTradeAdjustment ? element.dayTradeAdjustment.value : null,
            blockedValueLimit: element.blockedValueLimit ? element.blockedValueLimit.value : null,
            bmfNotExecutedPurchaseAmmount: element.bmfNotExecutedPurchaseAmmount ? element.bmfNotExecutedPurchaseAmmount.value : null,
            bmfNotExecutedSellAmmount: element.bmfNotExecutedSellAmmount ? element.bmfNotExecutedSellAmmount.value : null,
            cashOpenPurchases: element.cashOpenPurchases ? element.cashOpenPurchases.value : null,
            optionsNotExecutedPurchaseAmmount: element.optionsNotExecutedPurchaseAmmount
              ? element.optionsNotExecutedPurchaseAmmount.value : null,
            cashOpenSells: element.cashOpenSells ? element.cashOpenSells.value : null,
            optionsNotExecutedSellAmmount: element.optionsNotExecutedSellAmmount ? element.optionsNotExecutedSellAmmount.value : null,
            warrantyRequired: element.warrantyRequired ? element.warrantyRequired.value : null,
            warrantyDeposited: element.warrantyDeposited ? element.warrantyDeposited.value : null
          };
          messageReturn.push(msgFinancial);
      });
      return messageReturn;
    });

  }

  public unsubscribeFinancial(account: string) {
    this.configService.writeLog('UNSUBSCRIBE FINANCIAL > [' + account + '] <<<<<');
    this.sendMessageFinancial(account, MarketEnum.BOVESPA, false);
  }

  public subscribeFinancial(account: string) {
    this.configService.writeLog('SUBSCRIBE FINANCIAL > [' + account + '] <<<<<');
    this.sendMessageFinancial(account, MarketEnum.BOVESPA, true);
  }

  private sendMessageFinancial(account: string, market: MarketEnum, dispatch: boolean) {
    const dataMsg: MessageWebFeeder = new MessageWebFeeder();
    dataMsg.token = this.configService.getToken();
    dataMsg.module = 'negotiation';
    dataMsg.service = 'financialAccountInformation';
    const messageParameter = new MessageParameterNegotiation();
    messageParameter.account = account;
    messageParameter.market = market === MarketEnum.BOVESPA ? Constants.MARKET_BOVESPA : Constants.MARKET_BMF;
    messageParameter.dispatch = dispatch;
    messageParameter.history = true;
    dataMsg.parameters = messageParameter;
    if (market !== undefined) {
      this.socketService.signService(dataMsg);
    }
  }
}
