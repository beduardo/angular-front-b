import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Subject';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { SocketService } from '@services/webFeeder/socket.service';
import { ConfigService } from '@services/webFeeder/config.service';
import { StatusConnection } from '@model/status-connection';
import { MessageWebFeeder } from '@model/webFeeder/message-web-feeder.model';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class StatusDifusionService {

  public messagesStatusDifusion: Observable<any>;
  private serviceSubject = new Subject<MessageEvent>();

  constructor(
    private socketService: SocketService,
    private configService: ConfigService
  ) { }


  public createSubject() {
    const newService = {
      name: 'StatusDifusionSvcByFn',
      serviceSubject: this.serviceSubject,
      fn: (data) => {
        return data && data.statusDifusion
      }
    };
    this.socketService.register(newService);
    const statusWFError = this.socketService._statusWFOnError$;
    const message = newService.serviceSubject;
    this.messagesStatusDifusion = message.merge(statusWFError).map(data => {
      let status;
      if (data['statusDifusion'] === 'serving') {
        status = StatusConnection.SUCCESS;
        this.configService.writeLog('STATUS DIFUSION: Conectado');
      } else if (data['statusDifusion'] === 'stop' || data['statusDifusion'] === 'close') {
        let maxConnection = data['connections'].length - 1;
        data['connections'].forEach(element => {
          if (element.status === 'connected') {
            if (maxConnection < data['connections'].length) {
              maxConnection++;
            }
          } else {
            maxConnection--;
          }
        });
        this.configService.writeLog('STATUS DIFUSION: Servidores ON: ' + maxConnection + ' - TOTAL: ' + data['connections'].length);
        if (maxConnection <= 0) {
          status = StatusConnection.ERROR;
          this.configService.writeLog('STATUS DIFUSION: Desconectado');
        } else {
          status = StatusConnection.WARNING;
          this.configService.writeLog('STATUS DIFUSION: Alerta');
        }
      } else if (data == StatusConnection.INITIAL) {
        this.configService.writeLog('STATUS DIFUSION: Iniciando');
        status = StatusConnection.SUCCESS;
      } else if (data == StatusConnection.ERROR) {
        this.configService.writeLog('STATUS DIFUSION: Desconectado');
        status = StatusConnection.ERROR;
      } else {
        status = StatusConnection.WARNING;
        this.configService.writeLog('STATUS DIFUSION: Reconectando');
      }
      return status;
    });
  }

  public subscribeDifusion() {
    this.configService.writeLog('Subscribe Status Difusion <<<<<');
    this.sendMessageStatusDifusion(true);
  }
  public unsubscribeDifusion() {
    this.sendMessageStatusDifusion(false);
    this.configService.writeLog('Unsubscribe Status Difusion <<<<<');
  }

  private sendMessageStatusDifusion(status: boolean) {
    const dataMsg: MessageWebFeeder = new MessageWebFeeder();
    dataMsg.token = this.configService.getToken();
    dataMsg.module = 'quotes';
    dataMsg.service = 'statusDifusion';
    dataMsg.parameters = { 'dispatch': status };
    this.socketService.signService(dataMsg);
  }

}
