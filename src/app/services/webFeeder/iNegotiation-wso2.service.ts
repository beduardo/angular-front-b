import { Injectable } from '@angular/core';
import { INegotiationLoggable } from '@services/strategy-pattern-login/iNegotiation-loggable';
import { HttpService } from '@services/http-service';
import { ConfigService } from '@services/webFeeder/config.service';
import { Observable, BehaviorSubject } from 'rxjs';
import { HistoryFilterOrder } from '@model/webFeeder/history-filter-order';
import { Constants } from '@utils/constants';
import { AppUtils } from '@utils/app.utils';
import { DatePipe } from '@angular/common';
import { MarketEnum } from '@model/enum/market.enum';
import { SideEnum } from '@model/enum/side.enum';
import { TypeOrderEnum } from '@model/enum/type-order.enum';
import { HistoryOrder } from '@model/webFeeder/history-order.model';
import { HttpClient } from '@angular/common/http';

export class MessageTrade {
  public orderid?: string;
  public account: string;
  public market: string;
  public price: string;
  public quote: string;
  public qtd: number;
  public type: string;
  public side: string;
  public username: string;
  public validityorderTemp: Date;
  public validityorder: any;
  public targettrigger: string;
  public targetlimit: string;
  public stoptrigger: string;
  public stoplimit: string;
  public orderStrategy: string;
  public sourceaddress: string;
  public timeinforce?: string;
  public loteDefault: number;
  public origclordid?: string;
  public maxfloor?: string;
  public movingstart?: string;
  public initialchangetype?: string;
  public initialchange?: string;
  public pricepercentage?: string;
  public enteringtrader?: string;
  public minqty?: string;
  public ordertag?: string;
  public fractionaryQtd?: number;
  public bypasssuitability?: boolean;

  constructor() { }
}

export class MessageCancelTrade {
  public username: string;
  public orderid: string;
  public origclordid: string;
  public market: string;
  public quote: string;
  public qtd: string;
  public side: string;
  public enteringtrader: string;
  public account: string;
  public sourceaddress: string;
  constructor() { }
}

@Injectable({ providedIn: 'root' })
export class INegotiationWso2Service implements INegotiationLoggable {

  private _quantityOpenOrder = new BehaviorSubject<number>(0);
  quantityOpenOrder$ = this._quantityOpenOrder.asObservable();

  constructor(
    private _httpService: HttpService,
    private _configService: ConfigService,
    private datePipe: DatePipe,
    private httpClient: HttpClient,
  ) { }

  setQuantityOpenOrder(qtd: number) {
    this._quantityOpenOrder.next(qtd);
  }

  brokerServiceAccountList(account: string): any {
    if (!account) {
      return Observable.of([]);
    } else {
      if (this._configService.isUserAssessorLogged()) {
        return this.httpClient
          .get(this._configService.getApiWebFeederRestFacade() +
            '/services/negotiation/brokerServiceFullAccountListInformation?filter=&financial=false',
            {
              headers: {
                'Authorization': 'Bearer ' + this._configService.getToken(),
                // 'x-source-request': 'FastTradeWeb',
                // 'x-source-tracker-id': AppUtils.newguidgen()
              }
            })
          .map((response: any) => {
            if (response && response.users) {
              return response.users;
            }
            return [];
          });
      } else {
        return this._httpService
          .doGet(this._configService.getApiWebFeederRest(), 'services/negotiation/brokerServiceAccountList/' + account)
          .map(response => {
            if (response && response.accountsBean) {
              return response.accountsBean;
            } else if (response && response.length > 0) {
              return response;
            }
            return [];
          });
      }
    }
  }

  financialAccountInformationCompl(account: string, market: string) {
    if (!account || !market) {
      return Observable.of([]);
    } else {
      return this._httpService
        .doGet(this._configService.getApiWebFeederRest(), 'services/negotiation/financialAccountInformationCompl/' + account + '/' + market)
        .map(response => {
          if (response) {
            const json = response.financialInformationBean;
            return json;
          }
          return [];
        });
    }
  }

  positionBovespa(account: string, quote?: string) {
    let url = '';
    if (quote) {
      url = 'services/negotiation/positionBovespa/' + account + '/' + quote;
    } else {
      url = 'services/negotiation/positionBovespa/' + account;
    }
    return this._httpService
      .doGet(this._configService.getApiWebFeederRest(), url, false)
      .map(response => {
        if (response) {
          return response;
        }
        return [];
      });
  }

  positionBmf(account: string, quote?: string) {
    let url = '';
    if (quote) {
      url = 'services/negotiation/positionBmf/' + account + '/' + quote;
    } else {
      url = 'services/negotiation/positionBmf/' + account;
    }
    return this._httpService
      .doGet(this._configService.getApiWebFeederRest(), url, false)
      .map(response => {
        if (response) {
          return response;
        }
        return [];
      });
  }

  // WSO2
  public getHistoryOrder(
    account: string, market: string, dataInicio: string, dataFim: string, symbol: string,
    status: string, direction: string, orderID?: string, type?: string, statusGeneric?: string,
    statusGroup?: string
  ): Observable<any> {
    const params = new HistoryFilterOrder();

    params.account = account;
    params.dataInicio = dataInicio || null;
    params.dataFim = dataFim || null;
    params.orderID = orderID || null;
    params.market = '-';
    if (market === '1') {
      params.market = Constants.MARKET_BOVESPA;
    } else if (market === '2') {
      params.market = Constants.MARKET_BMF;
    }
    if (symbol && symbol !== null) {
      params.symbol = symbol.toUpperCase();
    } else {
      params.symbol = null;
    }
    if (direction && direction !== null) {
      params.direction = direction.toUpperCase();
    } else {
      params.direction = null;
    }
    if (type === '') {
      type = null;
    }
    params.type = type;
    params.statusGroup = statusGroup || null;

    if (status === 'New') {
      params.statusGroup = 'Abertas';
    } else {
      if (status && status !== null) {
        params.status = status.toUpperCase();
      } else {
        params.status = null;
      }
    }
    if (params.dataInicio !== null || params.dataFim !== null || params.orderID !== null) {
      params.queryType = '1';
    } else {
      params.queryType = '0';
    }
    if (params.dataInicio && params.dataFim) {
      params.dataInicio = this.datePipe.transform(params.dataInicio, Constants.DATE2_DDMMYYYY);
      params.dataFim = this.datePipe.transform(new Date(params.dataFim).setDate(new Date(params.dataFim).getDate() + 1),
        Constants.DATE2_DDMMYYYY);
    } else if (params.dataInicio && !params.dataFim) {
      const dataFimTEmp = params.dataInicio;
      params.dataInicio = this.datePipe.transform(params.dataInicio, Constants.DATE2_DDMMYYYY);
      params.dataFim = this.datePipe.transform(new Date(dataFimTEmp).setDate(new Date(dataFimTEmp).getDate() + 1),
        Constants.DATE2_DDMMYYYY);
    } else if (!params.dataInicio && !params.dataFim) {
      params.dataInicio = this.datePipe.transform(new Date(), Constants.DATE2_DDMMYYYY);
      params.dataFim = this.datePipe.transform(new Date().setDate(new Date().getDate() + 1), Constants.DATE2_DDMMYYYY);
    }
    return this.getParamsHistory(params);
  }

  private getParamsHistory(params: HistoryFilterOrder) {
    let urlParams = '';
    if (params && params.account && !AppUtils.isEmptyStr(params.account)) {
      urlParams += '/' + params.account;
    } else {
      urlParams += '/' + '-';
    }

    if (params && params.market && !AppUtils.isEmptyStr(params.market)) {
      urlParams += '/' + params.market;
    }

    if (params && params.dataInicio && !AppUtils.isEmptyStr(params.dataInicio) || params.dataInicio === '-') {
      urlParams += '/' + params.dataInicio;
    } else {
      urlParams += '/' + '-';
    }

    if (params && params.dataFim && !AppUtils.isEmptyStr(params.dataFim) || params.dataFim === '-') {
      urlParams += '/' + params.dataFim;
    } else {
      urlParams += '/' + '-';
    }

    if (params && params.symbol && !AppUtils.isEmptyStr(params.symbol)) {
      urlParams += '/' + params.symbol;
    } else {
      urlParams += '/' + '-';
    }

    if (params && params.status && !AppUtils.isEmptyStr(params.status)) {
      urlParams += '/' + params.status;
    } else {
      urlParams += '/' + '-';
    }

    if (params && params.direction && !AppUtils.isEmptyStr(params.direction)) {
      urlParams += '/' + params.direction;
    } else {
      urlParams += '/' + '-';
    }

    if (params && params.orderID && !AppUtils.isEmptyStr(params.orderID)) {
      urlParams += '/' + params.orderID;
    } else {
      urlParams += '/' + '-';
    }

    if (params && params.statusGroup && !AppUtils.isEmptyStr(params.statusGroup)) {
      urlParams += '/' + params.statusGroup;
    } else {
      urlParams += '/' + '-';
    }

    if (params && params.queryType && !AppUtils.isEmptyStr(params.queryType)) {
      urlParams += '/' + params.queryType;
    } else {
      urlParams += '/' + '-';
    }

    return this._httpService
      .doGet(this._configService.getApiWebFeederRest(), 'services/negotiation/historyOrder' + urlParams, false)
      .map(response => {
        if (response) {
          if (response.error) {
            return Observable.of([]);
          }
          return response;
        }
        return [];
      });
  }


  sendOrderWso2(account: string, quote: string, fractionaryQtd: number, nonFractionaryQtd: number, price: string, market: MarketEnum,
    type: TypeOrderEnum, side: SideEnum, validityOrderTemp: Date, orderStrategy: string, timeinforce?: string, orderid?: string,
    origclordid?: string, loteDefault?: number, maxfloor?: string, minqty?: string, ordertag?: string, targettrigger?: string,
    targetlimit?: string, stoptrigger?: string, stoplimit?: string, isStop?: boolean, bypasssuitability?: boolean
  ): Observable<any> {
    const params = new MessageTrade();
    // ------------------------------------------ SINGLE --------------------------------------------------------------
    if (validityOrderTemp) {
      const datStr = this.datePipe.transform(validityOrderTemp, 'ddMMyyyy');
      params.validityorder = datStr;
    } else {
      return Observable.of([]);
    }

    if (account) {
      params.username = account;
      params.account = account;
    } else {
      return Observable.of([]);
    }

    if (price) {
      params.price = price;
    }

    if (quote) {
      params.quote = quote;
    } else {
      return Observable.of([]);
    }

    if (nonFractionaryQtd) {
      params.qtd = nonFractionaryQtd;
    }

    if (nonFractionaryQtd === 0 && fractionaryQtd > 0) {
      params.qtd = fractionaryQtd;
    }

    // params.market = Number(market) === MarketEnum.BOVESPA ? Constants.MARKET_BOVESPA : Constants.MARKET_BMF;
    if (Number(market) === 1) {
      params.market = 'XBSP';
    } else {
      params.market = 'XBMF';
    }

    if (type) {
      params.type = type === TypeOrderEnum.MARKET ? Constants.ORDER_TYPE_MARKET : Constants.ORDER_TYPE_LIMITED;
    } else {
      return Observable.of([]);
    }

    if (side) {
      params.side = side === SideEnum.BUY ? Constants.ORDER_SIDE_BUY : Constants.ORDER_SIDE_SELL;
    } else {
      return Observable.of([]);
    }

    if (orderStrategy) {
      params.orderStrategy = orderStrategy;
    } else {
      return Observable.of([]);
    }

    if (timeinforce) {
      params.timeinforce = timeinforce;
    } else {
      return Observable.of([]);
    }

    params.sourceaddress = this._configService.getIP();

    if (maxfloor) {
      params.maxfloor = maxfloor;
    } else {
      params.maxfloor = '0';
    }
    if (minqty) {
      params.minqty = minqty;
    }
    if (ordertag) {
      params.ordertag = ordertag;
    }

    if (loteDefault) {
      params.loteDefault = loteDefault;
    }

    // if (params.movingstart) {
    //   params.movingstart = null;
    // }
    // if (params.initialchangetype) {
    //   params.initialchangetype = null;
    // }
    // if (params.initialchange) {
    //   params.initialchange = null;
    // }
    // if (params.pricepercentage) {
    //   params.pricepercentage = null;
    // }
    // if (params.enteringtrader) {
    //   params.enteringtrader = null;
    // }

    if (orderid) {
      params.orderid = orderid || null;
    }

    if (origclordid) {
      params.origclordid = origclordid || null;
    }
    // ------------------------------------------ STOP ------------------------------------------------------------------
    if (isStop) {
      params.type = Constants.ORDER_TYPE_STOP;
      params.targettrigger = targettrigger || '0';
      params.targetlimit = targetlimit || '0';
      params.stoptrigger = stoptrigger || '0';
      params.stoplimit = stoplimit || '0';
    } else {
      params.targettrigger = '0';
      params.targetlimit = '0';
      params.stoptrigger = '0';
      params.stoplimit = '0';
    }

    params.bypasssuitability = bypasssuitability;

    return this.getParamsWso2(params);
  }

  private getParamsWso2(params: MessageTrade) {
    let query = '?';

    if (params.account) {
      query += 'account=' + params.account;
    } else {
      return Observable.of([]);
    }
    if (params.orderid) {
      query += '&orderid=' + params.orderid;
    }

    if (params.origclordid) {
      query += '&origclordid=' + params.origclordid;
    }

    if (params.market) {
      query += '&market=' + params.market;
    } else {
      return Observable.of([]);
    }

    if (!(params.type === 'START') && !(params.type === 'STOP')) {
      query += '&price=' + params.price;
    }

    if (params.quote) {
      query += '&quote=' + params.quote;
    } else {
      return Observable.of([]);
    }

    if (params.qtd) {
      query += '&qtd=' + params.qtd.toString();
    } else {
      return Observable.of([]);
    }

    if (params.validityorder) {
      const datStr = this.datePipe.transform(params.validityorder, 'ddMMyyyy');
      query += '&validityorder=' + datStr;
    } else {
      return Observable.of([]);
    }

    if (params.targettrigger) {
      query += '&targettrigger=' + params.targettrigger.toString();
    } else {
      query += '&targettrigger=0';
    }

    if (params.targetlimit) {
      query += '&targetlimit=' + params.targetlimit.toString();
    } else {
      query += '&targetlimit=0';
    }

    if (params.stoptrigger) {
      query += '&stoptrigger=' + params.stoptrigger.toString();
    } else {
      query += '&stoptrigger=0';
    }

    if (params.stoplimit) {
      query += '&stoplimit=' + params.stoplimit.toString();
    } else {
      query += '&stoplimit=0';
    }

    if (params.type) {
      query += '&type=' + params.type.toString();
    }

    if (params.side) {
      query += '&side=' + params.side.toString();
    } else {
      return Observable.of([]);
    }

    if (params.username) {
      query += '&username=' + params.username.toString();
    } else {
      return Observable.of([]);
    }

    if (this._configService.isUserAssessorLogged()) {
      query += '&sourceaddress=' + Constants.DEFAULT_SOURCE_ADRESS_WSO2_ASSESSOR;
    } else {
      query += '&sourceaddress=' + Constants.DEFAULT_SOURCE_ADRESS_WSO2_CLIENT;
    }

    if (params.orderStrategy) {
      query += '&orderstrategy=' + params.orderStrategy;
    } else {
      return Observable.of([]);
    }

    query += '&appname=FastTradeWeb';

    if (params.timeinforce) {
      query += '&timeinforce=' + params.timeinforce;
    } else {
      return Observable.of([]);
    }

    if (params.maxfloor && params.maxfloor !== '-' && params.maxfloor !== '0') {
      query += '&maxfloor=' + params.maxfloor;
    } else {
      query += '&maxfloor=0';
    }

    if (params.minqty) {
      query += '&minqty=' + params.minqty;
    }

    if (params.ordertag) {
      query += '&ordertag=' + params.ordertag;
    }

    if (params.bypasssuitability) {
      query += '&bypasssuitability=' + params.bypasssuitability;
    }

    return this.sendWso2(params, query);
  }

  private sendWso2(params: MessageTrade, query?: any) {
    // const searchParams = Object.keys(params).map(key => {
    //   return encodeURIComponent(key) + '=' + encodeURIComponent(params[key]);
    // }).join('&');
    params.sourceaddress = this._configService.getIP();
    if (params.orderid) {
      this._configService.writeLog('SendEditOrder');
      this._configService.writeLog(query);
      return this._httpService
        .doPostWso2(this._configService.getApiWebFeederRest(), 'services/negotiation/editOrder' + query, query, null, null, true)
        .map(response => {
          if (response) {
            this._configService.writeLog('SendEditOrder > Response');
            this._configService.writeLog(response);
            return response;
          }
          return [];
        });
    } else {
      // this._configService.writeLog('sendNewOrder');
      // this._configService.writeLog(query);
      return this._httpService
        .doPostWso2(this._configService.getApiWebFeederRest(), 'services/negotiation/sendNewOrderSingle' + query, query, null, null, true)
        .map(response => {
          if (response) {
            return response;
          }
          return [];
        });
    }
  }

  cancelOrderWso2(account: string, username: string, orderid: string, origclordid: string, market: string,
    quote: string, qtd: string, side: string, enteringtrader: string) {
    let query = '';
    const params = new MessageCancelTrade();

    if (orderid) {
      params.orderid = orderid;
      query += '?orderid=' + params.orderid;
    }
    if (origclordid) {
      params.origclordid = origclordid;
      query += '&origclordid=' + params.origclordid;
    }
    if (quote) {
      params.quote = quote;
      query += '&quote=' + params.quote;
    } else {
      return Observable.of([]);
    }
    if (side) {
      params.side = side;
      query += '&side=' + params.side;
    } else {
      return Observable.of([]);
    }
    if (qtd) {
      if (qtd.indexOf('.') !== -1) {
        qtd.replace(new RegExp('.', 'g'), '');
      }
      params.qtd = qtd;
      query += '&qtd=' + params.qtd;
    } else {
      return Observable.of([]);
    }
    if (username) {
      params.username = username;
      query += '&username=' + params.username;
    } else {
      return Observable.of([]);
    }
    if (market) {
      params.market = Number(market) === MarketEnum.BOVESPA ? Constants.MARKET_BOVESPA : Constants.MARKET_BMF;
      query += '&market=' + params.market;
    } else {
      return Observable.of([]);
    }

    query += '&sourceaddress=' + this._configService.getIP();
    // TODO params.enteringtrader = enteringtrader || null;  not obligatory
    // TODO params.account = account;                        not obligatory
    return this._httpService
      .doPostWso2(this._configService.getApiWebFeederRest(), 'services/negotiation/cancelOrder', query, null, null, true)
      .map(response => {
        if (response) {
          return response;
        }
        return [];
      });
  }

  cancelAllOrderHistoryWso2(historyOrder: HistoryOrder[]) {
    const invalidHistoryOrders: HistoryOrder[] = null; // TODO use it!
    for (let i = 0; i < historyOrder.length; i++) {
      if (!historyOrder[i].quote) {
        invalidHistoryOrders.push(historyOrder[i]);
        continue;
      }

      if (!historyOrder[i].side) {
        invalidHistoryOrders.push(historyOrder[i]);
        continue;
      } else {
        if (historyOrder[i].buyOrSell === 'C') {
          historyOrder[i].side = Constants.ORDER_SIDE_BUY;
        } else {
          historyOrder[i].side = Constants.ORDER_SIDE_SELL;
        }
      }

      // be careful, if sends fractional value separated by '.' can be generated business problem
      if (historyOrder[i].quantityRemaining) {
        if (historyOrder[i].quantityRemaining.indexOf('.') !== -1) {
          historyOrder[i].quantityRemaining.replace(new RegExp('.', 'g'), '');
        }
      } else {
        invalidHistoryOrders.push(historyOrder[i]);
        continue;
      }

      if (!historyOrder[i].username) {
        invalidHistoryOrders.push(historyOrder[i]);
        continue;
      }

      if (historyOrder[i].market) {
        historyOrder[i].market =
          Number(historyOrder[i].market) === MarketEnum.BOVESPA ? Constants.MARKET_BOVESPA : Constants.MARKET_BMF;
      } else {
        invalidHistoryOrders.push(historyOrder[i]);
        continue;
      }

      return this._httpService
        .doPostWso2(this._configService.getApiWebFeederRest(), 'services/negotiation/cancelOrder', historyOrder[i], false, null, true)
        .map(response => {
          if (response) {
            return response;
          }
          return [];
        });
    }
  }

  // API
  sendOrderApi(account: string, quote: string, qtd: number, price: string, market: MarketEnum, type: any, side: SideEnum,
    validityOrder: Date, orderStrategy: string, timeinforce?: string, orderid?: string, origclordid?: string,
    loteDefault?: number, maxfloor?: string, minqty?: string, ordertag?: string): Observable<any> {
    throw new Error('Method not implemented.');
  }

  sendOrderStopApi(account: string, validityOrder: Date, orderStrategy: string, quote: string, qtd: number, targettrigger: string,
    targetlimit: string, stoptrigger: string, stoplimit: string, market: MarketEnum, side: SideEnum, timeinforce?: string,
    orderid?: string, origclordid?: string, loteDefault?: number, isStart?: boolean, maxfloor?: string, minqty?: string): Observable<any> {
    throw new Error('Method not implemented.');
  }

  cancelOrderApi(account: string, username: string, orderid: string, origclordid: string, market: string,
    quote: string, qtd: string, side: string, enteringtrader: string) {
    throw new Error('Method not implemented.');
  }

  cancelAllOrderHistoryApi(historyOrder: HistoryOrder[]) {
    throw new Error('Method not implemented.');
  }

  suitabilityCheckMatrix(account: string, marketID: string, securityType: string, securityRisk: number,
    symbol: string, suitabilityID?: string, profileName?: string, client_id?: number) {
    let perfilTemp = '';
    if (this._configService.isUserAssessorLogged()) {
      perfilTemp = profileName;
    } else {
      perfilTemp = this._configService.getProfileName();
    }
    if (perfilTemp.toLowerCase() === 'agressivo') {
      return new Observable<any>(observer => {
        const json = { is_out_of_bounds: false };
        observer.next(json);
      });
    } else {
      const payload = {
        Data: {
          code_quote: symbol,
          client_id: client_id
        },
      };
      // Verificar se cliente já assinou o desenquadramento
      return this._httpService
        .doPost2(this._configService.getApiWebFeederRestSuitabilityWSO2(), '/check_bounds/variable_income', payload, null, null)
        .map(response => {
          if (response) {
            return response;
          }
          return [];
        });
    }
  }

  postAssignBoundsWso2(account: string, symbol: string, client_id: number, user_id: number) {
    const payload = {
      Data: {
        code_quote: symbol,
        account_client: account,
        client_id: client_id,
        type_title: 3,
        user_id: user_id,
        user_id_create: user_id,
        user_type_id: this._configService.isUserAssessorLogged() ? 1 : 2,
        contract_user_type_id: this._configService.isUserAssessorLogged() ? 1 : 2,
        investment_profile_id: 4,
      },
    };
    // envia o termo para o WSO2
    return this._httpService
      .doPost2(this._configService.getApiWebFeederRestSuitabilityWSO2(), '/assign_bounds/variable_income', payload, null, null)
      .map(response => {
        if (response) {
          return response;
        }
        return [];
      });
  }

  brokeragePDF(account: string, market: string, date: Date) {
    if (!account || !market || !date) {
      return Observable.of([]);
    } else {
      const dateTemp = this.datePipe.transform(date, Constants.DATE2_DDMMYYYY);
      return this._httpService
        .doGet(this._configService.getApiWebFeederRest(), 'services/negotiation/brokeragePDF/' + account + '/' + market + '/' + dateTemp)
        .map(response => {
          if (response) {
            return response;
          }
          return [];
        });
    }
  }

  statementAccount(account: string, market: string, dateStart: Date, dateEnd: Date, ipAddress: string) {
    if (!account || !market || !dateStart || !dateEnd) {
      return Observable.of([]);
    } else {
      let module = '';
      if (market === 'XBSP') {
        module = 'statementBovespa';
      } else {
        module = 'statementBmf';
      }
      const dateStartTemp = this.datePipe.transform(dateStart, Constants.DATE2_DDMMYYYY);
      const dateEndTemp = this.datePipe.transform(dateEnd, Constants.DATE2_DDMMYYYY);
      return this._httpService
        .doGet(this._configService.getApiWebFeederRest(),
          'services/negotiation/' + module + '/' + account + '/FINAN/' + dateStartTemp + '/' + dateEndTemp)
        .map(response => {
          if (response) {
            return response;
          }
          return [];
        });
    }
  }

  getAfcSearchQuestions() {
    throw new Error('Method not implemented.');
  }

  postAfcSearchQuestions(questionResp: string, questionType: string) {
    throw new Error('Method not implemented.');
  }

}
