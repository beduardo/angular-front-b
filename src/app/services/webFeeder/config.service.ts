﻿import { Router } from '@angular/router';
import { TransitionService } from './../transition.service';
import { Injectable } from '@angular/core';
import { ConfigurationService } from '@services/configuration.service';
import { AppUtils } from '@utils/app.utils';
import { Constants, Profile } from '@utils/constants';
import { environment } from 'environments/environment';
import { BehaviorSubject } from 'rxjs';
import { ErrorMessage } from '@model/error-message';
import { Cookie } from 'ng2-cookies/ng2-cookies';
import { SimpleOrder } from '@model/simpleOrder';

@Injectable({
  providedIn: 'root'
})
export class ConfigService {

  public _restApiSocket: string;
  public _restApi: string;
  private _youtubeApiVrs = 'v3';
  private _youtubeChat: string;

  private WORKSPACE_PD_API_USER_PERFIL = Constants.WKS_USER_PROFILE;           // User Workspace profile constant
  private LOGIN_TYPE_ENABLED = environment.LOGIN_TYPE;                         // Login type environment
  private _fastMarket_api_path = '/wp-json/wp/';
  private _fastMarket_api_vrs = 'v2/';

  public _authHbLoginType = new BehaviorSubject<number>(3);
  authHbLoginType$ = this._authHbLoginType.asObservable();

  public _authHbLogged = new BehaviorSubject<boolean>(false);
  authHbLogged$ = this._authHbLogged.asObservable();

  public _loggoutSessionExpired = new BehaviorSubject<boolean>(false);
  loggoutSessionExpired$ = this._loggoutSessionExpired.asObservable();

  public _modalBuySellClosed = new BehaviorSubject<boolean>(false);
  modalBuySellClosed$ = this._modalBuySellClosed.asObservable();

  public _wksBackModal = new BehaviorSubject<boolean>(false);
  wksBackModal$ = this._wksBackModal.asObservable();

  public _wksAddNewComponentModal = new BehaviorSubject<boolean>(false);
  wksAddNewComponentModal$ = this._wksAddNewComponentModal.asObservable();

  public _clientAssessorChangeSelect = new BehaviorSubject<boolean>(false);
  clientAssessorChangeSelect$ = this._clientAssessorChangeSelect.asObservable();

  public _changeAccountBillet = new BehaviorSubject<string>('');
  changeAccountBillet$ = this._changeAccountBillet.asObservable();

  public _clickOpenSideMenu = new BehaviorSubject<number>(0);
  clickOpenSideMenu$ = this._clickOpenSideMenu.asObservable();

  constructor(
    private _configurationService: ConfigurationService,
    private _transitionService: TransitionService,
    private _router: Router
  ) {
    this.writeLog('CONFIG SERVICE > ' + this.LOGIN_TYPE_ENABLED);
    console.log(new Date().toString().slice(16, 25) + '> CONFIG SERVICE > VERSION: ' + this.getVersionSystem() + '\n ');
    if (!this.isLoginTypeWSO2()) {
      this._restApiSocket = this._configurationService.getConfiguration('URL_WEB_FEEDER_01', 'WEBFEEDER');
      this._restApi = this._configurationService.getConfiguration('URL_WEB_FEEDER_02', 'WEBFEEDER');
    } else {
      this._restApiSocket = this._configurationService.getConfiguration('URL_WEB_FEEDER_WSO2_01', 'WEBFEEDER');
      this._restApi = this._configurationService.getConfiguration('URL_WEB_FEEDER_WSO2_02', 'WEBFEEDER');
    }
  }

  isLoginTypeWSO2() {
    if (Constants.LOGIN_TYPE.LoginO === this.getLoginTypeEnabled() || Constants.LOGIN_TYPE.LoginS === this.getLoginTypeEnabled()) {
      return false;
    } else if (Constants.LOGIN_TYPE.LoginW === this.getLoginTypeEnabled()) {
      return true;
    }
  }

  setToken(value: string) {
    if (!this.isLoginTypeWSO2()) {
      window.sessionStorage.setItem(Constants.COOKIE_TOKEN_WEBFEEDER, value);
      this._transitionService.setKeyValue(Constants.COOKIE_TOKEN_WEBFEEDER, value);
    } else {
      AppUtils.resetCookieItem(Constants.COOKIE_TOKEN_WEBFEEDER);
      window.sessionStorage.setItem(Constants.COOKIE_TOKEN_WEBFEEDER, value);
      if (this.isLoginTypeWSO2()) {
        this._transitionService.setKeyValue(Constants.COOKIE_TOKEN_WEBFEEDER, value);
      }
    }
  }

  getToken() {
    // let value = Cookie.get(Constants.COOKIE_TOKEN_WEBFEEDER);
    let value = window.sessionStorage.getItem(Constants.COOKIE_TOKEN_WEBFEEDER);
    if (AppUtils.getPageByName('/worksheet', window.location.href)
      || AppUtils.getPageByName('/timestrades', window.location.href)
      || AppUtils.getPageByName('/asset-summary', window.location.href)
      || AppUtils.getPageByName('/book', window.location.href)
      || AppUtils.getPageByName('/volume-at-price', window.location.href)
      || AppUtils.getPageByName('/chart', window.location.href)) {
      const urlParams = new URLSearchParams(window.location.search);
      value = urlParams.get('token');
    }
    // this.writeLog('\nCONFIG SERVICE > getToken(): ' + value);
    return value;
  }

  setTokenNegotiation(value: string) {
    // Cookie.set(Constants.COOKIE_TOKEN_WEBFEEDER_NEGOTIATION, value, 0.5, '/');
    window.sessionStorage.setItem(Constants.COOKIE_TOKEN_WEBFEEDER_NEGOTIATION, value);
  }

  // TODO: Captura o token para negociação
  getTokenNegotiation() {
    // let value = Cookie.get(Constants.COOKIE_TOKEN_WEBFEEDER_NEGOTIATION);
    let value = window.sessionStorage.getItem(Constants.COOKIE_TOKEN_WEBFEEDER_NEGOTIATION);
    if (AppUtils.getPageByName('/worksheet', window.location.href)
      || AppUtils.getPageByName('/timestrades', window.location.href)
      || AppUtils.getPageByName('/asset-summary', window.location.href)
      || AppUtils.getPageByName('/book', window.location.href)
      || AppUtils.getPageByName('/volume-at-price', window.location.href)
      || AppUtils.getPageByName('/chart', window.location.href)) {
      const urlParams = new URLSearchParams(window.location.search);
      value = urlParams.get('token');
    }
    return value;
  }

  isNewWindow() {
    if (AppUtils.getPageByName('/worksheet', window.location.href)
      || AppUtils.getPageByName('/timestrades', window.location.href)
      || AppUtils.getPageByName('/asset-summary', window.location.href)
      || AppUtils.getPageByName('/book', window.location.href)
      || AppUtils.getPageByName('/volume-at-price', window.location.href)
      || AppUtils.getPageByName('/chart', window.location.href)) {
      return true;
    } else {
      return false;
    }
  }

  setTokenApi(value: string) {
    // Cookie.set(Constants.COOKIE_TOKEN_WEBAPI, value, 0.5, '/');
    window.sessionStorage.setItem(Constants.COOKIE_TOKEN_WEBAPI, value);
    if (this.isLoginTypeWSO2()) {
      this._transitionService.setKeyValue(Constants.COOKIE_TOKEN_WEBAPI, value);
    }
  }
  getTokenApi() {
    // const value = Cookie.get(Constants.COOKIE_TOKEN_WEBAPI);
    const value = window.sessionStorage.getItem(Constants.COOKIE_TOKEN_WEBAPI);
    // this.writeLog(new Date().toString().slice(16, 25) + ' - CONFIG SERVICE >>> getTokenApi: ' + value);
    return value;
  }
  // deprecated
  getUserWebFeeder() {
    return this._configurationService.getConfiguration('WEBFEEDER_USER', 'WEBFEEDER');
  }
  // deprecated
  getPassWebFeeder() {
    return this._configurationService.getConfiguration('WEBFEEDER_PASS', 'WEBFEEDER');
  }
  getUrlWebSocketWebFeeder(numberReconect: number, graphic?: boolean): string {
    let queryToken = '';
    let tokenWebFeeder;
    if (graphic) {
      tokenWebFeeder = this.getTokenGraphic();
    } else {
      tokenWebFeeder = this.getToken();
    }
    // this.writeLog('\nCONFIG SERVICE > getToken() - tokenWebFeeder: ' + tokenWebFeeder);
    // TODO this.writeLog(tokenWebFeeder);
    if (tokenWebFeeder !== undefined && tokenWebFeeder !== null && tokenWebFeeder !== 'null' && tokenWebFeeder !== '') {
      queryToken = '?reconnect=' + tokenWebFeeder;
    }
    if (numberReconect % 3 !== 0) {
      return this._restApiSocket + queryToken;
    }
    this.writeLog(this._restApi + queryToken);
    return this._restApi + queryToken;
  }

  getLoginTypeEnabled() {
    return this.LOGIN_TYPE_ENABLED;
  }

  getUserWorkspacesPreDefineds() {
    return this.WORKSPACE_PD_API_USER_PERFIL;
  }
  getApiWebFeederRest(): string {
    if (!this.isLoginTypeWSO2()) {
      return this._configurationService.getConfiguration('WEBFEEDER_REST_API', 'API_URLS');
    } else {
      return this._configurationService.getEndPoint('WSO2', 'WEBFEEDER');
    }
  }
  getApiWebFeederRestSuitabilityWSO2(): string {
    if (!this.isLoginTypeWSO2()) {
      return this._configurationService.getConfiguration('WEBFEEDER_REST_API', 'API_URLS');
    } else {
      return this._configurationService.getEndPoint('WSO2', 'REGISTRATION_SYSTEM_SUITABILITY');
    }
  }

  getApiWebFeederRestFacade(): string {
    if (!this.isLoginTypeWSO2()) {
      return this._configurationService.getConfiguration('WEBFEEDER_REST_API', 'API_URLS');
    } else {
      return this._configurationService.getEndPoint('WSO2', 'PORTAL_FACADE');
    }
  }

  // Api to save HB workspace
  getApiWorkspace(): string {
    if (!this.isLoginTypeWSO2()) {
      return this._configurationService.getConfiguration('WORKSPACE_REST_API', 'API_URLS');
    } else {
      return this._configurationService.getConfiguration('WORKSPACE_REST_WSO_API', 'API_URLS');
    }
  }

  // API to save TradingView
  getApiWorkspaceCandle(): string {
    return ''; // TODO environment.NODE_CHART_SAVE_LOAD;
  }

  // NODE API to return data from candle to TrandingView
  getApiNodeCandleRequest(): string {
    if (!this.isLoginTypeWSO2()) {
      return this._configurationService.getConfiguration('WEBFEEDER_REST_API', 'API_URLS');
    } else {
      // return this._configurationService.getEndPoint('WSO2', 'WEBFEEDER');
      // return this._configurationService.getConfiguration('CHART_TRADINGVIEW_WSO2_API', 'API_URLS');
      return this._configurationService.getEndPoint('WSO2', 'TRADINGVIEW');
    }
    // TODO return environment.NODE_CHART_REQUEST;
  }

  getApiChartTradingView() {
    return this._configurationService.getConfiguration('CHART_TRADINGVIEW_WSO2_API', 'API_URLS');
  }

  getFastMarketApiUrl(): string {
    return this._configurationService.getConfiguration('FAST_MARKET', 'API_URLS') + this._fastMarket_api_path + this._fastMarket_api_vrs;
  }

  getYouTubeLiveApi(): string {
    return this._configurationService.getConfiguration('YOUTUBE_LIVE', 'API_URLS') + '/' + this._youtubeApiVrs + '/';
  }

  setHbAccount(value: string) {
    // Cookie.set(Constants.COOKIE_HB_ACCOUNT, value, 0.5, '/');
    window.sessionStorage.setItem(Constants.COOKIE_HB_ACCOUNT, value);
    if (this.isLoginTypeWSO2()) {
      this._transitionService.setKeyValue(Constants.COOKIE_HB_ACCOUNT, value);
    }
  }

  getHbAccount() {
    // return Cookie.get(Constants.COOKIE_HB_ACCOUNT);
    return window.sessionStorage.getItem(Constants.COOKIE_HB_ACCOUNT);
  }

  setHbCurrentUser(user) {
    let json = '';
    if (typeof (user) === 'string') {
      json = user;
    } else {
      json = JSON.stringify(user);
    }
    window.sessionStorage.setItem(Constants.COOKIE_HB_CURRENTUSER, json);
    if (this.isLoginTypeWSO2()) {
      this._transitionService.setKeyValue(Constants.COOKIE_HB_CURRENTUSER, json);
    }
  }

  getHbCurrentUser() {
    let userLogged = window.sessionStorage.getItem(Constants.COOKIE_HB_CURRENTUSER);
    if (userLogged) {
      userLogged = JSON.parse(userLogged);
    }
    return userLogged;
  }

  deleteHbCurrentUser() {
    AppUtils.deleteCookie(Constants.COOKIE_HB_CURRENTUSER);
  }

  setBrokerAccount(value: string, type: string) {
    window.sessionStorage.setItem(Constants.COOKIE_HB_BROKER_ACCOUNT, value);
    window.sessionStorage.setItem(Constants.COOKIE_HB_LOGIN_TYPE, type);
    if (this.isLoginTypeWSO2()) {
      this._transitionService.setKeyValue(Constants.COOKIE_HB_BROKER_ACCOUNT, value);
      this._transitionService.setKeyValue(Constants.COOKIE_HB_LOGIN_TYPE, type);
    }
  }

  getBrokerAccountList() {
    let res = window.sessionStorage.getItem(Constants.COOKIE_HB_BROKER_ACCOUNT_LIST);
    if (res) {
      res = JSON.parse(res);
    }
    return res;
  }

  setBrokerAccountList(value) {
    let json = '';
    if (value) {
      json = JSON.stringify(value);
    }
    window.sessionStorage.setItem(Constants.COOKIE_HB_BROKER_ACCOUNT_LIST, json);
    if (this.isLoginTypeWSO2()) {
      this._transitionService.setKeyValue(Constants.COOKIE_HB_BROKER_ACCOUNT_LIST, json);
    }
  }

  setBrokerAccountSelected(value: string) {
    window.sessionStorage.setItem(Constants.COOKIE_HB_BROKER_ACCOUNT_SELECTED, value);
    // if (this.isLoginTypeWSO2()) {
    //   this._transitionService.setKeyValue(Constants.COOKIE_HB_BROKER_ACCOUNT_SELECTED, value);
    // }
  }

  setBrokerAccountSelectedTransition(value: string) {
    this._transitionService.setKeyValue(Constants.COOKIE_HB_BROKER_ACCOUNT_SELECTED, value);
  }

  setHbLoginTypeEnable(value: string) {
    window.sessionStorage.setItem(Constants.COOKIE_HB_LOGIN_TYPE_ENABLED, value);
  }

  getLoginTypeEnable() {
    return window.sessionStorage.getItem(Constants.COOKIE_HB_LOGIN_TYPE_ENABLED);
  }


  getHbLoginTypeEnable() {
    return this.LOGIN_TYPE_ENABLED;
  }

  getBrokerAccount() {
    return window.sessionStorage.getItem(Constants.COOKIE_HB_BROKER_ACCOUNT);
  }

  getBrokerAccountSelected() {
    return window.sessionStorage.getItem(Constants.COOKIE_HB_BROKER_ACCOUNT_SELECTED);
  }

  getBrokerAccountType() {
    // return Cookie.get(Constants.COOKIE_HB_LOGIN_TYPE);
    return window.sessionStorage.getItem(Constants.COOKIE_HB_LOGIN_TYPE);
  }

  public setUserLogged(value: string) {
    // Cookie.set(Constants.COOKIE_USER, value, 0.5, '/');
    window.sessionStorage.setItem(Constants.COOKIE_USER, value);
  }
  public getUserLogged() {
    // return Cookie.get(Constants.COOKIE_USER);
    return window.sessionStorage.getItem(Constants.COOKIE_USER);
  }

  public writeLog(msg) {
    if (environment.showConsoleLog) {
      const now = new Date().toString().slice(16, 25);
      if (typeof msg === 'string') {
        if (
          this.getHbAccount() && this.getHbAccount().toLowerCase() === 'couto1' ||
          this.getHbAccount() && this.getHbAccount().toLowerCase() === 'vcrcad' ||
          this.getHbAccount() && this.getHbAccount().toLowerCase() === '10005' ||
          this.getHbAccount() && this.getHbAccount().toLowerCase() === '703' ||
          this.getHbAccount() && this.getHbAccount().toLowerCase() === 'vitorcouto' ||
          this.getHbAccount() && this.getHbAccount().toLowerCase() === 'wellingtondma' ||
          this.getHbAccount() && this.getHbAccount().toLowerCase() === '07057694610' ||
          this.getUserSystemLogged() && this.getUserSystemLogged().toLowerCase() === 'assessor' ||
          environment.isDev
        ) {
          console.log(now + '> ' + msg + '\n\n');
        }
      } else {
        if (
          this.getHbAccount() && this.getHbAccount().toLowerCase() === 'couto1' ||
          this.getHbAccount() && this.getHbAccount().toLowerCase() === 'vcrcad' ||
          this.getHbAccount() && this.getHbAccount().toLowerCase() === '10005' ||
          this.getHbAccount() && this.getHbAccount().toLowerCase() === '703' ||
          this.getHbAccount() && this.getHbAccount().toLowerCase() === 'vitorcouto' ||
          this.getHbAccount() && this.getHbAccount().toLowerCase() === 'wellingtondma' ||
          this.getHbAccount() && this.getHbAccount().toLowerCase() === '07057694610' ||
          this.getUserSystemLogged() && this.getUserSystemLogged().toLowerCase() === 'assessor' ||
          environment.isDev
        ) {
          console.log(now);
          if (msg && msg.length > 0) {
            console.log(msg[0]);
          } else {
            console.log(msg);
          }
          console.log('\n');
        }
      }
    }
  }

  public writeError(msg) {
    if (environment.showConsoleLog) {
      const now = new Date().toString().slice(16, 24);
      console.error(now + '> ' + msg);
    }
  }

  setMessage(value: string) {
    // Cookie.set(Constants.COOKIE_MESSAGE, value);
    window.sessionStorage.setItem(Constants.COOKIE_MESSAGE, value);
  }
  getMessage() {
    // return Cookie.get(Constants.COOKIE_MESSAGE);
    return window.sessionStorage.getItem(Constants.COOKIE_MESSAGE);
  }

  deleteCookie() {
    AppUtils.deleteCookie(Constants.COOKIE_USER);
    AppUtils.deleteCookie(Constants.COOKIE_TOKEN_WEBFEEDER_NEGOTIATION);
    AppUtils.deleteCookie(Constants.COOKIE_GUID_USER);
    AppUtils.deleteCookie(Constants.LAYOUT_THEME_DARK);
    AppUtils.deleteCookie(Constants.COOKIE_TOKEN_WEBFEEDER_GRAPHIC);
    AppUtils.deleteCookie(Constants.COOKIE_TOKEN_WEBFEEDER);
    AppUtils.deleteCookie(Constants.COOKIE_TOKEN_EXPIRATION);
    AppUtils.deleteCookie(Constants.COOKIE_TOKEN_WEBAPI);
    AppUtils.deleteCookie(Constants.COOKIE_HB_ACCOUNT);
    AppUtils.deleteCookie(Constants.COOKIE_HB_BROKER_ACCOUNT);
    AppUtils.deleteCookie(Constants.COOKIE_HB_BROKER_ACCOUNT_SELECTED);
    AppUtils.deleteCookie(Constants.COOKIE_HB_CURRENTUSER);
    AppUtils.deleteCookie(Constants.COOKIE_USER_LOGIN_TYPE);
    AppUtils.deleteCookie(Constants.COOKIE_SYSTEM_ACCOUNT_NAME);
    window.sessionStorage.clear();
    if (this.isLoginTypeWSO2()) {
      this._transitionService.clear();
    }
  }

  redirectExpireSession() {
    this._router.navigate(['error'], {
      queryParams: new ErrorMessage(Constants.ERROR_EXPIRED_ICON, Constants.ERROR_EXPIRED_TITLE, Constants.ERROR_EXPIRED_TEXT,
        Constants.ERROR_EXPIRED_BUTTON, 'login')
    });
  }

  deleteBrokerCookies() {
    AppUtils.deleteCookie(Constants.COOKIE_CURRENT_BROKER);
    AppUtils.deleteCookie(Constants.COOKIE_TOKEN_BROKER_WEBFEEDER);
    AppUtils.deleteCookie(Constants.COOKIE_TOKEN_BROKER_EXPIRATION);
  }

  deleteHbBrokerUser() {
    AppUtils.deleteCookie(Constants.COOKIE_HB_LOGIN_TYPE);
    AppUtils.deleteCookie(Constants.COOKIE_HB_LOGIN_TYPE_ENABLED);
    AppUtils.deleteCookie(Constants.COOKIE_TOKEN_WEBFEEDER_NEGOTIATION);
    AppUtils.deleteCookie(Constants.COOKIE_TOKEN_REFRESH_WEBAPI);
    AppUtils.deleteCookie(Constants.COOKIE_HB_BROKER_ACCOUNT_SELECTED);
    AppUtils.deleteCookie(Constants.COOKIE_HB_BROKER_ACCOUNT);
    AppUtils.deleteCookie(Constants.COOKIE_HB_ACCOUNT_NAME);
    AppUtils.deleteCookie(Constants.COOKIE_HB_ACCOUNT_EMAIL);
    AppUtils.deleteCookie(Constants.COOKIE_HB_ACCOUNT_ID);
    if (this.isLoginTypeWSO2()) {
      this._transitionService.removeKeyValue(Constants.COOKIE_HB_LOGIN_TYPE);
      this._transitionService.removeKeyValue(Constants.COOKIE_HB_LOGIN_TYPE_ENABLED);
      this._transitionService.removeKeyValue(Constants.COOKIE_TOKEN_WEBFEEDER_NEGOTIATION);
      this._transitionService.removeKeyValue(Constants.COOKIE_TOKEN_REFRESH_WEBAPI);
      this._transitionService.removeKeyValue(Constants.COOKIE_HB_BROKER_ACCOUNT_SELECTED);
      this._transitionService.removeKeyValue(Constants.COOKIE_HB_BROKER_ACCOUNT);
      this._transitionService.removeKeyValue(Constants.COOKIE_HB_ACCOUNT_NAME);
      this._transitionService.removeKeyValue(Constants.COOKIE_HB_ACCOUNT_EMAIL);
      this._transitionService.removeKeyValue(Constants.COOKIE_HB_ACCOUNT_ID);
      this._transitionService.clear();
    }
  }

  public getTokenTime() {
    // const value = Cookie.get(Constants.COOKIE_HOUR_TOKEN_WS);
    const value = window.sessionStorage.getItem(Constants.COOKIE_HOUR_TOKEN_WS);
    return value;
  }

  getExpiresDay(): number {
    return 0.12;
  }

  public setTokenWS(value: string) {
    if (value !== null && value !== '') {
      window.sessionStorage.setItem(Constants.COOKIE_HOUR_TOKEN_WS, value);
      window.sessionStorage.setItem(Constants.COOKIE_TOKEN_WEBFEEDER, value);
      if (this.isLoginTypeWSO2()) {
        this._transitionService.setKeyValue(Constants.COOKIE_TOKEN_WEBFEEDER, value);
      }
    } else {
      AppUtils.deleteCookie(Constants.COOKIE_HOUR_TOKEN_WS);
      AppUtils.deleteCookie(Constants.COOKIE_TOKEN_WEBFEEDER);
      if (this.isLoginTypeWSO2()) {
        this._transitionService.removeKeyValue(Constants.COOKIE_TOKEN_WEBFEEDER);
      }
    }
  }

  public getTokenExpiration() {
    // return Cookie.get(Constants.COOKIE_TOKEN_EXPIRATION);
    return window.sessionStorage.getItem(Constants.COOKIE_TOKEN_EXPIRATION);
  }

  public setTokenExpiration(value: string) {
    // Cookie.set(Constants.COOKIE_TOKEN_EXPIRATION, value, 0.5, '/');
    window.sessionStorage.setItem(Constants.COOKIE_TOKEN_EXPIRATION, value);
    if (this.isLoginTypeWSO2()) {
      this._transitionService.setKeyValue(Constants.COOKIE_TOKEN_EXPIRATION, value);
    }
  }

  public getGuidUser() {
    // return Cookie.get(Constants.COOKIE_GUID_USER);
    return window.sessionStorage.getItem(Constants.COOKIE_GUID_USER);
  }

  public setGuidUser(value: string) {
    // Cookie.set(Constants.COOKIE_GUID_USER, value, 0.5, '/' );
    window.sessionStorage.setItem(Constants.COOKIE_GUID_USER, value);
  }

  public getValidUserInterval() {
    return environment.VALID_USER_INTERVAL;
  }

  public getValidUserRedirect() {
    return environment.VALID_USER_REDIRECT;
  }

  public setBrokerWebfeeder(value: string) {
    // Cookie.set(Constants.COOKIE_CURRENT_BROKER, value);
    window.sessionStorage.setItem(Constants.COOKIE_CURRENT_BROKER, value);
  }
  public getBrokerWebfeeder() {
    // return Cookie.get(Constants.COOKIE_CURRENT_BROKER);
    return window.sessionStorage.getItem(Constants.COOKIE_CURRENT_BROKER);
  }

  public setBrokerToken(value: string) {
    // Cookie.set(Constants.COOKIE_TOKEN_BROKER_WEBFEEDER, value);
    window.sessionStorage.setItem(Constants.COOKIE_TOKEN_BROKER_WEBFEEDER, value);
  }

  public getBrokerToken() {
    // return Cookie.get(Constants.COOKIE_TOKEN_BROKER_WEBFEEDER);
    return window.sessionStorage.getItem(Constants.COOKIE_TOKEN_BROKER_WEBFEEDER);
  }

  public getBrokerTokenExpiration() {
    // return Cookie.get(Constants.COOKIE_TOKEN_BROKER_EXPIRATION);
    return window.sessionStorage.getItem(Constants.COOKIE_TOKEN_BROKER_EXPIRATION);
  }

  public setBrokerTokenExpiration(value: string) {
    // Cookie.set(Constants.COOKIE_TOKEN_BROKER_EXPIRATION, value);
    window.sessionStorage.setItem(Constants.COOKIE_TOKEN_BROKER_EXPIRATION, value);
  }

  public getPageByName(name, url) {
    if (!url) {
      url = window.location.href;
    }
    if (url && url.indexOf(name) > 0) {
      return true;
    }
    return false;
  }

  public setFavoriteThemeUser(value: string) {
    Cookie.set(Constants.LAYOUT_THEME_DARK, value, null, '/');
    return window.sessionStorage.setItem(Constants.LAYOUT_THEME_DARK, value);
  }

  public getFavoriteThemeUser() {
    // return Cookie.get(Constants.LAYOUT_THEME_DARK);
    if (Cookie.get(Constants.LAYOUT_THEME_DARK)) {
      return Cookie.get(Constants.LAYOUT_THEME_DARK);
    } else if (window.sessionStorage.getItem(Constants.LAYOUT_THEME_DARK)) {
      return window.sessionStorage.getItem(Constants.LAYOUT_THEME_DARK);
    } else {
      return false;
    }
  }

  public deleteAllCookie() {
    // return Cookie.deleteAll();
    return window.sessionStorage.clear();
  }

  public deleteFavoriteThemeUser() {
    // return Cookie.delete(Constants.LAYOUT_THEME_DARK);
    return window.sessionStorage.removeItem(Constants.LAYOUT_THEME_DARK);
  }

  public isAuthenticationBroker() {
    if (this.getBrokerAccount()) {
      return true;
    } else {
      return false;
    }
  }

  public isAuthenticadSystem() {
    if (this.getLoginTypeEnabled() === Constants.LOGIN_TYPE.LoginW) {
      if (this.getTokenApi()) {
        return true;
      }
    } else {
      if (this.getTokenApi() && this.getGuidUser()) {
        return true;
      }
    }
    return false;
  }

  public closeBillet(close: boolean) {
    if (close) {
      this._modalBuySellClosed.next(true);
      this._modalBuySellClosed.next(false);
    }
  }

  setTokenGraphic(value: string) {
    // Cookie.set(Constants.COOKIE_TOKEN_WEBFEEDER_GRAPHIC, value, 0.5, '/');
    window.sessionStorage.setItem(Constants.COOKIE_TOKEN_WEBFEEDER_GRAPHIC, value);
    if (this.isLoginTypeWSO2()) {
      this._transitionService.setKeyValue(Constants.COOKIE_TOKEN_WEBFEEDER_GRAPHIC, value);
    }
  }

  getTokenGraphic() {
    // let value = Cookie.get(Constants.COOKIE_TOKEN_WEBFEEDER_GRAPHIC);
    let value = window.sessionStorage.getItem(Constants.COOKIE_TOKEN_WEBFEEDER_GRAPHIC);
    if (AppUtils.getPageByName('/worksheet', window.location.href)
      || AppUtils.getPageByName('/timestrades', window.location.href)
      || AppUtils.getPageByName('/asset-summary', window.location.href)
      || AppUtils.getPageByName('/book', window.location.href)
      || AppUtils.getPageByName('/volume-at-price', window.location.href)
      || AppUtils.getPageByName('/chart', window.location.href)) {
      const urlParams = new URLSearchParams(window.location.search);
      value = urlParams.get('token');
    }
    return value;
  }

  // WSO2
  public get USER_IDENTITY_API(): string {
    return this._configurationService.getEndPoint('WSO2', 'USER_IDENTITY');
  }

  public get WSO2(): string {
    return this._configurationService.getUrl('WSO2', 'API_URLS');
  }

  public get CADASTRO(): string {
    const url = this._configurationService.getUrl('CADASTRO', 'API_URLS') + '/cadastro';
    return url;
  }

  public get REDIRECT_LOGIN(): string {
    const url = this._configurationService.getUrl('REDIRECT_LOGIN', 'API_URLS');
    return url;
  }

  public get REDIRECT_LOGOUT(): string {
    const url = this._configurationService.getUrl('REDIRECT_LOGOUT', 'API_URLS');
    return url;
  }

  public get CEDRO_ACCOUNT_API(): string {
    return this._configurationService.getUrl('CEDRO_ACCOUNT_API', 'API_URLS');
  }

  setTokenLastRefreshed(value: string) {
    AppUtils.resetCookieItem(Constants.COOKIE_TOKEN_LAST_REFRESHED);
    // Cookie.set(Constants.COOKIE_TOKEN_LAST_REFRESHED, value);
    window.sessionStorage.setItem(Constants.COOKIE_TOKEN_LAST_REFRESHED, value);
  }

  getTokenLastRefreshed() {
    // const value = Cookie.get(Constants.COOKIE_TOKEN_LAST_REFRESHED);
    const value = window.sessionStorage.getItem(Constants.COOKIE_TOKEN_LAST_REFRESHED);
    return value;
  }

  setTokenRefreshApi(value: string) {
    AppUtils.resetCookieItem(Constants.COOKIE_TOKEN_REFRESH_WEBAPI);
    // Cookie.set(Constants.COOKIE_TOKEN_REFRESH_WEBAPI, value, 0.5, '/');
    window.sessionStorage.setItem(Constants.COOKIE_TOKEN_REFRESH_WEBAPI, value);
    if (this.isLoginTypeWSO2()) {
      this._transitionService.setKeyValue(Constants.COOKIE_TOKEN_REFRESH_WEBAPI, value);
    }
  }

  getTokenRefreshApi() {
    // return Cookie.get(Constants.COOKIE_TOKEN_REFRESH_WEBAPI);
    return window.sessionStorage.getItem(Constants.COOKIE_TOKEN_REFRESH_WEBAPI);
  }

  setScope(value: string) {
    AppUtils.resetCookieItem(Constants.COOKIE_SCOPE);
    // Cookie.set(Constants.COOKIE_SCOPE, value, 0.5, '/');
    window.sessionStorage.setItem(Constants.COOKIE_SCOPE, value);
  }

  getScope() {
    // return Cookie.get(Constants.COOKIE_SCOPE);
    return window.sessionStorage.getItem(Constants.COOKIE_SCOPE);
  }

  setTokenTp(value: string) {
    AppUtils.resetCookieItem(Constants.COOKIE_TOKEN_TP);
    // Cookie.set(Constants.COOKIE_TOKEN_TP, value, 0.5, '/');
    window.sessionStorage.setItem(Constants.COOKIE_TOKEN_TP, value);
  }

  getTokenTp() {
    // return Cookie.get(Constants.COOKIE_TOKEN_TP);
    return window.sessionStorage.getItem(Constants.COOKIE_TOKEN_TP);
  }

  setHbUserId(userId) {
    window.sessionStorage.setItem(Constants.COOKIE_HB_USER_ID, userId);
    if (this.isLoginTypeWSO2()) {
      this._transitionService.setKeyValue(Constants.COOKIE_HB_USER_ID, userId);
    }
  }

  getHbUserId() {
    return window.sessionStorage.getItem(Constants.COOKIE_HB_USER_ID);
  }

  setHbAccountId(accountId) {
    // Cookie.set(Constants.COOKIE_HB_ACCOUNT_ID, accountId, 0.5, '/');
    window.sessionStorage.setItem(Constants.COOKIE_HB_ACCOUNT_ID, accountId);
    if (this.isLoginTypeWSO2()) {
      this._transitionService.setKeyValue(Constants.COOKIE_HB_ACCOUNT_ID, accountId);
    }
  }

  getHbAccountId() {
    // return Cookie.get(Constants.COOKIE_HB_ACCOUNT_ID);
    return window.sessionStorage.getItem(Constants.COOKIE_HB_ACCOUNT_ID);
  }

  setHbAccountName(value: string) {
    // Cookie.set(Constants.COOKIE_HB_ACCOUNT_NAME, value, 0.5, '/');
    window.sessionStorage.setItem(Constants.COOKIE_HB_ACCOUNT_NAME, value);
    if (this.isLoginTypeWSO2()) {
      this._transitionService.setKeyValue(Constants.COOKIE_HB_ACCOUNT_NAME, value);
    }
  }

  getHbAccountName() {
    // return Cookie.get(Constants.COOKIE_HB_ACCOUNT_NAME);
    return window.sessionStorage.getItem(Constants.COOKIE_HB_ACCOUNT_NAME);
  }

  setHbSignature(value: string) {
    window.sessionStorage.setItem(Constants.SIGNATURE_ELETRONIC, value);
  }

  getHbSignature() {
    return window.sessionStorage.getItem(Constants.SIGNATURE_ELETRONIC);
  }

  deleteHbSignature() {
    window.sessionStorage.removeItem(Constants.SIGNATURE_ELETRONIC);
  }

  setHbEmail(value: string) {
    // Cookie.set(Constants.COOKIE_HB_ACCOUNT_EMAIL, value, 0.5, '/');
    window.sessionStorage.setItem(Constants.COOKIE_HB_ACCOUNT_EMAIL, value);
    if (this.isLoginTypeWSO2()) {
      this._transitionService.setKeyValue(Constants.COOKIE_HB_ACCOUNT_EMAIL, value);
    }
  }

  getHbEmail() {
    // return Cookie.get(Constants.COOKIE_HB_ACCOUNT_EMAIL);
    return window.sessionStorage.getItem(Constants.COOKIE_HB_ACCOUNT_EMAIL);
  }

  setHba(value: string, index: string) {
    // return Cookie.set(Constants.LAYOUT_THEME_DARK, value, null, '/');
    // window.localStorage.setItem(Constants.COOKIE_HB_ABA, value);
    Cookie.set(Constants.COOKIE_HB_ABA, value, 0.5, '/', window.location.hostname);
  }

  getHba() {
    // return window.localStorage.getItem(Constants.COOKIE_HB_ABA);
    return Cookie.get(Constants.COOKIE_HB_ABA);
  }

  deleteHba() {
    // window.localStorage.removeItem(Constants.COOKIE_HB_ABA);
    Cookie.delete(Constants.COOKIE_HB_ABA);
  }

  setUserLoginType(type) {
    let typeUser;
    if (type === Profile.Assessor) {
      typeUser = Profile.Assessor.toString();
    } else {
      typeUser = Profile.Client.toString();
    }
    window.sessionStorage.setItem(Constants.COOKIE_USER_LOGIN_TYPE, typeUser);
    if (this.isLoginTypeWSO2()) {
      this._transitionService.setKeyValue(Constants.COOKIE_USER_LOGIN_TYPE, typeUser);
    }
  }

  getUserLoginType() {
    return window.sessionStorage.getItem(Constants.COOKIE_USER_LOGIN_TYPE);
  }

  deleteUserLoginType() {
    window.sessionStorage.removeItem(Constants.COOKIE_USER_LOGIN_TYPE);
    if (this.isLoginTypeWSO2()) {
      this._transitionService.removeKeyValue(Constants.COOKIE_USER_LOGIN_TYPE);
    }
  }

  setUserSystemLogged(userlogged) {
    window.sessionStorage.setItem(Constants.COOKIE_SYSTEM_ACCOUNT_NAME, userlogged);
    if (this.isLoginTypeWSO2()) {
      this._transitionService.setKeyValue(Constants.COOKIE_SYSTEM_ACCOUNT_NAME, userlogged);
    }
  }

  getUserSystemLogged() {
    if (this.isLoginTypeWSO2() && this.isUserAssessorLogged()) {
      return window.sessionStorage.getItem(Constants.COOKIE_SYSTEM_ACCOUNT_NAME);
    } else if (this.isLoginTypeWSO2()) {
      return window.sessionStorage.getItem(Constants.COOKIE_HB_ACCOUNT_NAME);
    } else {
      return window.sessionStorage.getItem(Constants.COOKIE_SYSTEM_ACCOUNT_NAME);
    }
  }

  deleteUserSystemLogged() {
    window.sessionStorage.removeItem(Constants.COOKIE_SYSTEM_ACCOUNT_NAME);
    if (this.isLoginTypeWSO2()) {
      this._transitionService.removeKeyValue(Constants.COOKIE_SYSTEM_ACCOUNT_NAME);
    }
  }

  isUserAssessorLogged() {
    const isAssessor = this.getUserLoginType();
    if (Number(isAssessor) === Profile.Assessor) {
      return true;
    } else {
      return false;
    }
  }

  setAssessorListClients(value) {
    let listClients;
    if (value) {
      listClients = JSON.stringify(value);
    }
    window.sessionStorage.setItem(Constants.COOKIE_ASSESSOR_LIST_CLIENT, listClients);
    if (this.isLoginTypeWSO2()) {
      this._transitionService.setKeyValue(Constants.COOKIE_ASSESSOR_LIST_CLIENT, listClients);
    }
  }

  getAssessorListClients() {
    const listClients = window.sessionStorage.getItem(Constants.COOKIE_ASSESSOR_LIST_CLIENT);
    if (listClients) {
      return JSON.parse(listClients);
    }
    return null;
  }

  deleteAssessorListClients() {
    window.sessionStorage.removeItem(Constants.COOKIE_ASSESSOR_LIST_CLIENT);
    if (this.isLoginTypeWSO2()) {
      this._transitionService.removeKeyValue(Constants.COOKIE_ASSESSOR_LIST_CLIENT);
    }
  }

  setAssessorClientSelected(value) {
    let listClients;
    if (value) {
      listClients = JSON.stringify(value);
    }
    window.sessionStorage.setItem(Constants.COOKIE_ASSESSOR_CLIENT_SELECTED, listClients);
    if (this.isLoginTypeWSO2()) {
      this._transitionService.setKeyValue(Constants.COOKIE_ASSESSOR_CLIENT_SELECTED, listClients);
    }
  }

  getAssessorClientSelected() {
    const listClients = window.sessionStorage.getItem(Constants.COOKIE_ASSESSOR_CLIENT_SELECTED);
    if (listClients) {
      return JSON.parse(listClients);
    }
    return null;
  }

  deleteAssessorClientSelected() {
    window.sessionStorage.removeItem(Constants.COOKIE_ASSESSOR_CLIENT_SELECTED);
    if (this.isLoginTypeWSO2()) {
      this._transitionService.removeKeyValue(Constants.COOKIE_ASSESSOR_CLIENT_SELECTED);
    }
  }


  setIsAssessor(type: number) {
    if (type === Profile.Client) {
      this._transitionService.setKeyValue(Constants.COOKIE_IS_ASSESSOR, false);
    } else {
      this._transitionService.setKeyValue(Constants.COOKIE_IS_ASSESSOR, true);
    }
  }

  isLoginPi() {
    if (environment.WHITELABEL === 2) {
      return true;
    } else {
      return false;
    }
  }

  setProfileName(value: string) {
    if (!value) {
      value = 'Não Econtrado';
    }
    window.sessionStorage.setItem(Constants.COOKIE_CLIENT_PROFILE_NAME, value);
    if (this.isLoginTypeWSO2()) {
      this._transitionService.setKeyValue(Constants.COOKIE_CLIENT_PROFILE_NAME, value);
    }
  }

  getProfileName() {
    return window.sessionStorage.getItem(Constants.COOKIE_CLIENT_PROFILE_NAME);
  }

  setAssessorId(assessorId) {
    window.sessionStorage.setItem(Constants.COOKIE_HB_ASSESSOR_ID, assessorId);
    if (this.isLoginTypeWSO2()) {
      this._transitionService.setKeyValue(Constants.COOKIE_HB_ASSESSOR_ID, assessorId);
    }
  }

  getAssessorId() {
    return window.sessionStorage.getItem(Constants.COOKIE_HB_ASSESSOR_ID);
  }

  getWhiteLabel() {
    return environment.WHITELABEL;
  }

  setIP(value: string) {
    window.sessionStorage.setItem(Constants.COOKIE_IP, value);
  }

  getIP() {
    return window.sessionStorage.getItem(Constants.COOKIE_IP);
  }

  getConfigBilletClose() {
    if (window.sessionStorage.getItem(Constants.COOKIE_BILLET_CLOSED_MODAL)) {
      return JSON.parse(window.sessionStorage.getItem(Constants.COOKIE_BILLET_CLOSED_MODAL));
    }
  }

  setConfigBilletClose(model: SimpleOrder) {
    window.sessionStorage.setItem(Constants.COOKIE_BILLET_CLOSED_MODAL, JSON.stringify(model));
  }

  deleteConfigBilletClose() {
    window.sessionStorage.removeItem(Constants.COOKIE_BILLET_CLOSED_MODAL);
  }

  getOrderHistoryFilterRange(): number {
    return this._configurationService.getConfiguration('ORDER_HISTORY_FILTER_RANGE', 'SYSTEM_VARIABLES');
  }

  getExtractFinancialFilterRange(): number {
    return this._configurationService.getConfiguration('EXTRACT_FINANCIAL_FILTER_RANGE', 'SYSTEM_VARIABLES');
  }

  getModalNewsFilterRange(): number {
    return this._configurationService.getConfiguration('MODAL_NEWS_FILTER_RANGE', 'SYSTEM_VARIABLES');
  }

  getVersionSystem() {
    return environment.version;
  }
}
