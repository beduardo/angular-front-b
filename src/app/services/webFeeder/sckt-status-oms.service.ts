import { Injectable } from '@angular/core';
import { Subject, Observable, merge } from 'rxjs';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { SocketService } from '@services/webFeeder/socket.service';
import { ConfigService } from '@services/webFeeder/config.service';
import { StatusConnection } from '@model/status-connection';
import { MessageWebFeeder } from '@model/webFeeder/message-web-feeder.model';

@Injectable({
  providedIn: 'root'
})
export class StatusOmsService {

  public messagesStatusOms: Observable<any>;
  private _statusWebSocketOMS = new BehaviorSubject<number>(StatusConnection.INITIAL);
  statusWebSocketOMS$ = this._statusWebSocketOMS.asObservable();
  private serviceSubject = new Subject<MessageEvent>();

  constructor(
    private socketService: SocketService,
    private configService: ConfigService
  ) {

  }


  public createSubject() {
    const newService = {
      name: 'StatusOMSSvcByFn',
      serviceSubject: this.serviceSubject,
      errorHandler: true,
      fn: (data) => {
        return data && data.statusOMS
      }
    };
    this.socketService.register(newService);
    const statusWFError = this.socketService._statusWFOnError$;
    const message = newService.serviceSubject;
    this.messagesStatusOms = message.merge(statusWFError).map(data => {
      let response;
      if (data['statusOMS'] === 'connected' && data['statusDM'] === 'connected' && data['statusAuth'] === 'connected') {
        response = StatusConnection.SUCCESS;
        this.configService.writeLog('STATUS OMS: ' + data['statusOMS']);
      } else if (data['statusOMS'] === 'disconnected' || data['statusDM'] === 'disconnected' || data['statusAuth'] === 'disconnected') {
        response = StatusConnection.ERROR;
        this.configService.writeLog('STATUS OMS: ' + data['statusOMS']);
      } else if(data == StatusConnection.ERROR){
        response = StatusConnection.ERROR;
      }
      return response;
    });;

  }

  public subscribeOMS() {
    this.configService.writeLog('<<<< Subscribe Status OMS >>>> \n');
    this.sendMessageStatusOms(true);
  }

  public unsubscribeOMS() {
    this.sendMessageStatusOms(false);
    this.configService.writeLog('<<<< Unsubscribe Status OMS >>>> \n');
  }

  private sendMessageStatusOms(status: boolean) {
    const dataMsg: MessageWebFeeder = new MessageWebFeeder();
    dataMsg.token = this.configService.getToken();
    dataMsg.module = 'negotiation';
    dataMsg.service = 'statusOMS';
    dataMsg.parameters = { 'dispatch': status };
    this.socketService.signService(dataMsg);
  }

}
