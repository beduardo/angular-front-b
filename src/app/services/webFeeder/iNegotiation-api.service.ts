import { DatePipe } from '@angular/common';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { MarketEnum } from '@model/enum/market.enum';
import { SideEnum } from '@model/enum/side.enum';
import { TypeOrderEnum } from '@model/enum/type-order.enum';
import { HistoryFilterOrder } from '@model/webFeeder/history-filter-order';
import { HistoryOrder } from '@model/webFeeder/history-order.model';
import { HttpService } from '@services/http-service';
import { INegotiationLoggable } from '@services/strategy-pattern-login/iNegotiation-loggable';
import { ConfigService } from '@services/webFeeder/config.service';
import { Constants } from '@utils/constants';
import { BehaviorSubject, Observable } from 'rxjs';

export class MessageTrade {
  public orderid?: string;
  public account: string;
  public market: string;
  public price: string;
  public quote: string;
  public qtd: number;
  public type: string;
  public side: string;
  public username: string;
  public validityorderTemp: Date;
  public validityorder: any;
  public targettrigger: string;
  public targetlimit: string;
  public stoptrigger: string;
  public stoplimit: string;
  public orderStrategy: string;
  public sourceaddress: string;
  public timeinforce?: string;
  public loteDefault: number;
  public origclordid?: string;
  public maxfloor?: string;
  public movingstart?: string;
  public initialchangetype?: string;
  public initialchange?: string;
  public pricepercentage?: string;
  public enteringtrader?: string;
  public minqty?: string;
  public ordertag?: string;
  public fractionaryQtd?: number;
  public bypasssuitability?: boolean;

  constructor() { }
}

export class MessageCancelTrade {
  public username: string;
  public orderid: string;
  public origclordid: string;
  public market: string;
  public quote: string;
  public qtd: string;
  public side: string;
  public enteringtrader: string;
  public account: string;
  public sourceaddress: string;
  constructor() { }
}

@Injectable({ providedIn: 'root' })
export class INegotiationApiService implements INegotiationLoggable {

  private _quantityOpenOrder = new BehaviorSubject<number>(0);
  quantityOpenOrder$ = this._quantityOpenOrder.asObservable();

  constructor(
    private _httpService: HttpService,
    private _configService: ConfigService,
    private datePipe: DatePipe,
    private http: HttpClient
  ) { }

  setQuantityOpenOrder(qtd: number) {
    this._quantityOpenOrder.next(qtd);
  }

  brokerServiceAccountList(account: string): any {
    if (!account) {
      return Observable.of([]);
    } else {
      return this._httpService
        .doGet(this._configService.getApiWebFeederRest(), 'services/negotiation/brokerAccountList?login=' + account)
        .map(response => {
          if (response && response.accountsBean) {
            return response.accountsBean;
          } else if (response && response.length > 0) {
            return response;
          }
          return [];
        });
    }
  }

  financialAccountInformationCompl(account: string, market: string) {
    if (!account || !market) {
      return Observable.of([]);
    } else {
      return this._httpService
        .doGet(this._configService.getApiWebFeederRest(), 'services/negotiation/financialAccountInformationCompl?account='
          + account + '&market=' + market)
        .map(response => {
          if (response) {
            const json = response.financialInformationBean;
            return json;
          }
          return [];
        });
    }
  }

  positionBovespa(account: string, quote?: string) {
    let url;
    if (quote) {
      url = 'services/negotiation/positionBovespa?account=' + account + '&quote=' + quote;
    } else {
      url = 'services/negotiation/positionBovespa?account=' + account;
    }
    return this._httpService
      .doGet(this._configService.getApiWebFeederRest(), url, false)
      .map(response => {
        if (response) {
          const json = response;
          return json;
        }
        return [];
      });
  }

  positionBmf(account: string, quote?: string) {
    let url;
    if (quote) {
      url = 'services/negotiation/positionBmf?account=' + account + '&quote=' + quote;
    } else {
      url = 'services/negotiation/positionBmf?account=' + account;
    }
    return this._httpService
      .doGet(this._configService.getApiWebFeederRest(), url, false)
      .map(response => {
        if (response) {
          const json = response;
          return json;
        }
        return [];
      });
  }

  getHistoryOrder(account: string, market: string, dataInicio: string, dataFim: string,
    symbol: string, status: string, direction: string, orderID?: string, type?: string, statusGeneric?: string, statusGroup?: string
  ): Observable<any> {
    const params = new HistoryFilterOrder();

    params.account = account;
    params.dataInicio = dataInicio || null;
    params.dataFim = dataFim || null;
    params.orderID = orderID || null;
    params.market = '-';
    if (market == '1') {
      params.market = Constants.MARKET_BOVESPA;
    } else if (market == '2') {
      params.market = Constants.MARKET_BMF;
    }
    if (symbol && symbol !== null) {
      params.symbol = symbol.toUpperCase();
    } else {
      params.symbol = null;
    }
    if (direction && direction !== null) {
      params.direction = direction.toUpperCase();
    } else {
      params.direction = null;
    }
    if (type == '') {
      type = null;
    }
    params.type = type;
    params.statusGroup = statusGroup;

    if (status == 'New') {
      params.statusGroup = 'Abertas';
    } else {
      if (status && status !== null) {
        params.status = status.toUpperCase();
      } else {
        params.status = null;
      }
    }

    if (params.dataInicio !== null || params.dataFim !== null || params.orderID !== null) {
      params.queryType = '1';
    } else {
      params.queryType = '0';
    }
    return this.getApi(params);
  }

  private getApi(params: HistoryFilterOrder) {
    return this._httpService
      .doPost(this._configService.getApiWebFeederRest(), 'services/negotiation/getHistoryOrder/', JSON.stringify(params))
      .map(response => {
        if (response) {
          if (response.error) {
            return [];
          }
          try {
            return response;
          } catch (e) {
          }
        }
        return [];
      });
  }

  // WSO2
  sendOrderWso2(account: string, quote: string, fractionaryQtd: number, nonFractionaryQtd: number, price: string, market: MarketEnum,
    type: TypeOrderEnum, side: SideEnum, validityOrderTemp: Date, orderStrategy: string, timeinforce?: string, orderid?: string,
    origclordid?: string, loteDefault?: number, maxfloor?: string, minqty?: string, ordertag?: string, targettrigger?: string,
    targetlimit?: string, stoptrigger?: string, stoplimit?: string, isStop?: boolean
  ): Observable<any> {
    throw new Error('Method not implemented.');
  }

  cancelOrderWso2(account: string, username: string, orderid: string, origclordid: string, market: string,
    quote: string, qtd: string, side: string, enteringtrader: string) {
    throw new Error('Method not implemented.');
  }

  cancelAllOrderHistoryWso2(historyOrder: HistoryOrder[]) {
    throw new Error('Method not implemented.');
  }

  // API
  sendOrderApi(account: string, quote: string, qtd: number, price: string, market: MarketEnum, type: any, side: SideEnum,
    validityOrder: Date, orderStrategy: string, timeinforce?: string, orderid?: string, origclordid?: string,
    loteDefault?: number, maxfloor?: string, minqty?: string, ordertag?: string, bypasssuitability?: boolean): Observable<any> {
    const params = new MessageTrade();
    params.origclordid = null;
    params.movingstart = null;
    params.initialchangetype = null;
    params.initialchange = null;
    params.pricepercentage = null;
    params.enteringtrader = null;

    params.orderid = orderid || null;
    params.origclordid = origclordid || null;
    params.validityorder = null;
    params.targettrigger = null;
    params.targetlimit = null;
    params.stoptrigger = null;
    params.stoplimit = null;
    params.validityorder = validityOrder;
    params.username = this._configService.getBrokerAccount();
    params.account = account;
    params.price = price;
    // params.price = (price); Check issues
    params.quote = quote;
    params.qtd = qtd;
    params.loteDefault = loteDefault;

    params.market = Number(market) === MarketEnum.BOVESPA ? Constants.MARKET_BOVESPA : Constants.MARKET_BMF;
    // params.type = //TODO checar necessidade
    //   type === TypeOrderEnum.MARKET//TODO checar necessidade
    //     ? Constants.ORDER_TYPE_MARKET//TODO checar necessidade
    //     : Constants.ORDER_TYPE_LIMITED;//TODO checar necessidade
    switch (type) {
      case '1':
        params.type = 'DDD';
        break;
      case 'Limitada':
        params.type = 'Limited';
        break;
      case 'Stop':
        params.type = 'Stop';
        break;
      case 'Mercado':
        params.type = 'Market';
        break;
      default:
        params.type = type;
        break;
    }
    // params.type = type;
    params.side = side === SideEnum.BUY ? Constants.ORDER_SIDE_BUY : Constants.ORDER_SIDE_SELL; params.orderStrategy = orderStrategy;
    params.timeinforce = timeinforce;

    params.maxfloor = maxfloor;
    params.minqty = minqty;
    params.ordertag = ordertag;

    params.bypasssuitability = bypasssuitability;

    return this.sendApi(params);
  }

  sendOrderStopApi(account: string, validityOrder: Date, orderStrategy: string, quote: string, qtd: number, targettrigger: string,
    targetlimit: string, stoptrigger: string, stoplimit: string, market: MarketEnum, side: SideEnum, timeinforce?: string,
    orderid?: string, origclordid?: string, loteDefault?: number, isStart?: boolean, maxfloor?: string, minqty?: string,
    orderTag?: string, bypasssuitability?: boolean): Observable<any> {
    const params = new MessageTrade();

    params.username = this._configService.getBrokerAccount();
    params.account = account;
    params.quote = quote;
    params.qtd = qtd;

    params.orderid = orderid || null;
    params.origclordid = origclordid || null;
    params.validityorder = validityOrder;

    params.targettrigger = targettrigger;
    params.targetlimit = targetlimit;
    params.stoptrigger = stoptrigger;
    params.stoplimit = stoplimit;

    // .replace('R$', '') TODO - Adicionar

    params.timeinforce = timeinforce;
    params.orderStrategy = orderStrategy;
    params.maxfloor = maxfloor;
    params.minqty = minqty;
    params.ordertag = orderTag;

    params.loteDefault = loteDefault;

    params.market = market === MarketEnum.BOVESPA ? Constants.MARKET_BOVESPA : Constants.MARKET_BMF;
    params.type = isStart ? Constants.ORDER_TYPE_START : Constants.ORDER_TYPE_STOP;
    params.side = side === SideEnum.BUY ? Constants.ORDER_SIDE_BUY : Constants.ORDER_SIDE_SELL;

    params.bypasssuitability = bypasssuitability;

    return this.sendApi(params);
  }

  private sendApi(params: MessageTrade) {
    // const searchParams = Object.keys(params)
    //   .map(key => {
    //     return encodeURIComponent(key) + '=' + encodeURIComponent(params[key]);
    //   }).join('&');
    params.sourceaddress = this._configService.getIP();
    if (params.orderid) {
      this._configService.writeLog('SendEditOrder');
      this._configService.writeLog(params);
      return this._httpService
        .doPost(this._configService.getApiWebFeederRest(), 'services/negotiation/SendEditOrder', params, false)
        .map(response => {
          if (response) {
            return response;
          }
          return [];
        });
    } else {
      this._configService.writeLog('sendNewOrderFractional');
      this._configService.writeLog(params);
      return this._httpService
        .doPost(this._configService.getApiWebFeederRest(), 'services/negotiation/sendNewOrderFractional', params, false)
        .map(response => {
          if (response) {
            return response;
          }
          return [];
        });
    }
  }

  cancelOrderApi(account: string, username: string, orderid: string, origclordid: string, market: string,
    quote: string, qtd: string, side: string, enteringtrader: string) {
    const params = new MessageCancelTrade();
    params.username = this._configService.getBrokerAccount();
    params.orderid = orderid;
    params.origclordid = origclordid;
    params.market = market;
    params.quote = quote;
    params.qtd = qtd;
    params.side = side;
    params.enteringtrader = enteringtrader || null;
    params.account = account;
    // params.sourceaddress = Constants.DEFAULT_SOURCE_ADRESS;
    params.sourceaddress = this._configService.getIP();

    return this._httpService.doPost(this._configService.getApiWebFeederRest(), 'services/negotiation/cancelOrder', params, false)
      .map(response => {
        if (response) {
          const json = response;
          return json;
        }
        return [];
      });
  }

  cancelAllOrderHistoryApi(historyOrder: HistoryOrder[]) {
    // this._configService.writeLog('cancelAllOrderHistory');
    // this._configService.writeLog(historyOrder);
    return this._httpService
      .doPost(this._configService.getApiWebFeederRest(), 'services/negotiation/CancelAllOrderHistory', historyOrder, false)
      .map(response => {
        if (response) {
          const json = response;
          return json;
        }
        return [];
      });
  }

  suitabilityCheckMatrix(account: string, marketID: string, securityType: string, securityRisk: number,
    symbol: string, suitabilityID?: string, profileName?: string, client_id?: number) {
    const params = {
      account: account,
      marketID: marketID === '1' ? 'XBSP' : 'XBMF',
      securityType: securityType,
      securityRisk: securityRisk,
      suitabilityID: suitabilityID,
      symbol: symbol
    };
    return this._httpService
      .doPost(this._configService.getApiWebFeederRest(), 'services/Suitability/SuitabilityCheckMatrix', params, false)
      .map(response => {
        if (response) {
          const json = response;
          return json;
        }
        return [];
      });
  }

  postAssignBoundsWso2(account: string, symbol: string, client_id: number, user_id: number) {
    throw new Error('Method not implemented.');
  }

  brokeragePDF(account: string, market: string, date: Date) {
    if (!account || !market || !date) {
      return Observable.of([]);
    } else {
      date = new Date(date);
      let day = date.getDate().toString();
      let month = (date.getMonth() + 1).toString();
      const year = date.getFullYear().toString();
      if (Number(month) < 10) {
        month = 0 + month;
      }
      if (Number(day) < 10) {
        day = 0 + day;
      }
      const datetemp = day + month + year;
      const params = '?account=' + account + '&market=' + market + '&date=' + datetemp;
      return this._httpService
        .doGetPdf(this._configService.getApiWebFeederRest(), 'services/negotiation/brokeragePDF' + params)
        .map(((data) => data));
    }
  }

  statementAccount(account: string, market: string, dateStart: Date, dateEnd: Date, ipAddress: string) {
    if (!account || !market || !dateStart || !dateEnd) {
      return Observable.of([]);
    } else {
      let dayStart = dateStart.getDate().toString();
      let dayEnd = dateEnd.getDate().toString();
      let monthStart = (dateStart.getMonth() + 1).toString();
      let monthEnd = (dateEnd.getMonth() + 1).toString();
      const yearStart = dateStart.getFullYear().toString();
      const yearEnd = dateEnd.getFullYear().toString();
      if (Number(dayStart) < 10) {
        dayStart = 0 + dayStart;
      }
      if (Number(dayEnd) < 10) {
        dayEnd = 0 + dayEnd;
      }
      if (Number(monthStart) < 10) {
        monthStart = 0 + monthStart;
      }
      if (Number(monthEnd) < 10) {
        monthEnd = 0 + monthEnd;
      }
      const datetempStart = dayStart + monthStart + yearStart;
      const datetempEnd = dayEnd + monthEnd + yearEnd;
      const params = '?account=' + account + '&market=' + market + '&dateStart=' + datetempStart + '&dateEnd=' + datetempEnd;
      return this._httpService
        .doGet(this._configService.getApiWebFeederRest(), 'services/negotiation/statementAccount' + params)
        .map((res: any) => {
          if (res) {
            const json = res;
            return json;
          }
          return [];
        });
    }
  }

  getAfcSearchQuestions() {
    const ipRequest = this._configService.getIP();
    return this._httpService
      .doGet(this._configService.getApiWebFeederRest(), 'services/Negotiation/GetAfcSearchQuestions?ipRequest=' + ipRequest)
      .map(response => {
        if (response) {
          const json = response;
          return json;
        }
        return [];
      });
  }

  postAfcSearchQuestions(questionResp: string, questionType: string) {
    const params = {
      questionResp: questionResp,
      questionType: questionType
    };
    return this._httpService
      .doPost(this._configService.getApiWebFeederRest(), 'services/Negotiation/PostAfcSearchQuestions', params, false)
      .map(response => {
        if (response) {
          const json = response;
          return json;
        }
        return [];
      });
  }

}
