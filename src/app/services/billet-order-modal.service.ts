import { ConfigService } from '@services/webFeeder/config.service';
import { Injectable } from '@angular/core';
import { BuySellModalComponent } from '@component/buy-sell-modal/buy-sell-modal.component';
import { MatBottomSheet, MatDialog } from '@angular/material';
import { Constants } from '@utils/constants';
import { QuoteService } from '@services/webFeeder/sckt-quotes.service';
import { SidebarService } from '@services/sidebar.service';
import { Subscription, BehaviorSubject } from 'rxjs';
import { WorkspaceNewService } from '@services/workspace-new.service';

@Injectable({
  providedIn: 'root'
})
export class BilletOrderModalService {

  private flagOptions = false;
  private symbolAsset: string;
  public billetTypeTemp: any;
  public quoteTemp: any;
  public tabSendTemp: number;
  public clickOpenTabOrder: Subscription;
  public modalBuySell: any;

  constructor(
    private buySell: MatBottomSheet,
    private _sidebarService: SidebarService,
    private _configService: ConfigService,
    private workspaceService: WorkspaceNewService
  ) {
    this.clickOpenTabOrder = this._sidebarService.clickOpenTabAuth$
      .subscribe(tab => {
        if (tab === 5) {
          this.buySellOrder(this.billetTypeTemp, this.quoteTemp);
        }
      });
  }

  buySellOrder(billetType: string, quote?: string, price?: string, tabSend?: string): void {
    // return;
    if (this.workspaceService.getUserWorkspaces() && this.workspaceService.getUserWorkspaces()[0].Id) {
      if (billetType) {
        this.billetTypeTemp = billetType.toUpperCase();
      }
      this.quoteTemp = quote;
      if (!this._configService.isAuthenticationBroker()) {
        this._sidebarService._clickOpenTabLoginBroker.next(true);
        this._sidebarService._clickOpenTabAuthCallBack.next(5);
      } else {
        if (billetType) {
          this.modalBuySell = this.buySell.open(BuySellModalComponent, {
            disableClose: true,
            panelClass: Constants.FULL_BOTTOMSHEET,
            data: { billetType: this.billetTypeTemp, quote: this.quoteTemp, price: price }
          });
        }
      }
    } else {
      this.workspaceService.updateShowMessageToAddWks(true);
    }
  }

  flagVerify() {
    if (this.flagOptions) {
      return true;
    } else {
      return false;
    }
  }
}
