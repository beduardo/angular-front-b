import { INewsLoggable } from './strategy-pattern-login/iNews-loggable';
import { INegotiationWso2Service } from './webFeeder/iNegotiation-wso2.service';
import { INegotiationApiService } from './webFeeder/iNegotiation-api.service';
import { INegotiationLoggable } from './strategy-pattern-login/iNegotiation-loggable';
import { Injectable } from '@angular/core';
import { Constants } from '@utils/constants';
import { ConfigService } from './webFeeder/config.service';

import { IQuotesApiService } from './webFeeder/iQuotes-api.service';
import { IQuotesWso2Service } from './webFeeder/iQuotes-wso2.service';
import { IQuoteLoggable } from './strategy-pattern-login/iQuote-loggable';
import { INewsWso2Service } from './webFeeder/iNews-wso2.service';
import { INewsApiService } from './webFeeder/iNews-api.service';
import { IDufusionLoggable } from './strategy-pattern-login/iDifusion-loggable';
import { IDifusionApiService } from './webFeeder/iDifusion-api.service';
import { IDifusionWso2Service } from './webFeeder/iDifusion-wso2.service';

@Injectable({
  providedIn: 'root'
})
export class AuthServer {

  public iQuote: IQuoteLoggable;
  public iNegotiation: INegotiationLoggable;
  public iNews: INewsLoggable;
  public iDifusion: IDufusionLoggable;

  constructor(
    private _configService: ConfigService,
    private _iQuotesApiService: IQuotesApiService,
    private _iQuotesWso2Service: IQuotesWso2Service,
    private _iNegotiationApiService: INegotiationApiService,
    private _iNegotiationWso2Service: INegotiationWso2Service,
    private _iNewsWso2Service: INewsWso2Service,
    private _iNewsApiService: INewsApiService,
    private _iDifusionWso2Service: IDifusionWso2Service,
    private _iDifusionApiService: IDifusionApiService
  ) {
    const typeLogin = this._configService.getLoginTypeEnabled();
    if (this._configService.isLoginTypeWSO2()) {
      this.iQuote = this._iQuotesWso2Service;
      this.iNegotiation = this._iNegotiationWso2Service;
      this.iNews = this._iNewsWso2Service;
      this.iDifusion = this._iDifusionWso2Service;
    } else {
      this.iQuote = this._iQuotesApiService;
      this.iNegotiation = this._iNegotiationApiService;
      this.iNews = this._iNewsApiService;
      this.iDifusion = this._iDifusionApiService;
    }
  }
}
