import { Injectable, Inject, ComponentFactoryResolver } from '@angular/core';
import { BehaviorSubject, Subscription } from 'rxjs';
import { CustomSnackbarComponent } from '@component/custom-snackbar/custom-snackbar.component';

// TODO Serviço para utilização do snackbar; Incluir ele no construtor do componente em que for utilizar o Snackbar;
// TODO @Inject(ViewContainerRef) public parent: ViewContainerRef; inserir está linha no construtor do componente
// TODO para o snackbar aparecer no final do elemento. Caso deseja inserir o snackbar em algum elemento do componente
// TODO utilizar o @ViewChild para construir o viewcontainerref;

@Injectable()
export class CustomSnackbarService {
  snack: CustomSnackbarComponent;
  plate: any;
  subscription: Subscription;
  id: any;

  constructor(@Inject(ComponentFactoryResolver) factoryResolver) {
    this.snack = factoryResolver.resolveComponentFactory(CustomSnackbarComponent);
    this.plate = null;
  }

  // TODO Função utilizada para criar o snackbar; deve receber um ViewContainerRef do elemento que vai receber
  // TODO a snackbar e uma string com a mensagem que aparecerá no snackbar.
  createSnack(parent: any, message: string, type: string) {
    if (this.plate === null) {
      this.plate = parent.createComponent(this.snack);
    }

    this.subscription = this.plate.instance.close$.subscribe(item => { if (item) { this.removeSnack(); } });
    this.plate.instance.content = message;
    this.plate.instance._class.next(type);
    this.id = setTimeout(() => this.removeSnack(), 9000);
  }

  createEndlessSnack(parent: any, message: string, type: string) {
    if (this.plate === null) {
      this.plate = parent.createComponent(this.snack);
    }

    this.subscription = this.plate.instance.close$.subscribe(item => { if (item) { this.removeSnack(); } });
    this.plate.instance.content = message;
    this.plate.instance._class.next(type);
    // this.id = setTimeout(() => this.removeSnack(), 9000);
  }


  removeSnack() {
    clearInterval(this.id);
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
    if (this.plate) {
      this.plate.destroy();
      this.plate = null;
    }
  }
}
