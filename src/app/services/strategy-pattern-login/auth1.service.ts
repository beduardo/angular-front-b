import { OmsAdmin } from './../../model/recover-password';
import { Injectable } from '@angular/core';
import { Loggable } from '@services/strategy-pattern-login/loggable';
import { ConfigService } from '@services/webFeeder/config.service';
import { AuthService } from '@auth/auth.service';
import { Login } from '@model/login.model';
import { Constants } from '@utils/constants';
import { HttpClient } from '@angular/common/http';
import { Observable, forkJoin } from 'rxjs';
import { environment } from 'environments/environment';
import { map } from 'rxjs/operators';
import { WorkspaceNewService } from '@services/workspace-new.service';
import { TokenService } from '@services/webFeeder/token.service';
import { ChangePassword } from '@model/change-password';
import { HttpService } from '@services/http-service';

@Injectable({
  providedIn: 'root'
})
export class Auth1Service implements Loggable {

  constructor(
    private authGuard: AuthService,
    private httpsvc: HttpClient,
    private configService: ConfigService,
    private tokenService: TokenService,
    private workspaceService: WorkspaceNewService,
    private _httpService: HttpService,
  ) {
    // LoginO
  }

  login(username: string, password: string, profile?: any) {
    const credential: Login = {
      username: username,
      password: password,
      softwareVersion: environment.version,
      softwareName: Constants.DEFAULT_SOFTWARE_NAME
    };
    const user = { username: credential.username, firstName: 'Juvenal', lastName: 'Silva' }; // TODO retirar mock
    this.configService.setHbCurrentUser(user);
    const params = JSON.stringify(credential);
    return this.httpsvc.post(this.configService.getApiWebFeederRest() + '/services/AuthFast/LoginO', params, Constants.httpOptions);
  }

  // TODO Validate
  brokerServiceLogin(username: string, password: string, broker: string) {
    const user = { username: username, password: password, firstName: 'Teste', lastName: 'Tst' }; // TODO retirar mock
    this.configService.setHbCurrentUser(user);
    this.authGuard.setUserLogged(user.firstName);
    const credential: Login = {
      username: username, password: password, softwareVersion: environment.version, softwareName: Constants.DEFAULT_SOFTWARE_NAME
    };
    const params = JSON.stringify(credential);
    return this.httpsvc.post(this.configService.getApiWebFeederRest() + '/services/Authenticate/LoginHomeBroker',
      params, Constants.httpOptions);
  }


  brokerServiceLogout() {
    this.configService.deleteBrokerCookies();
    const username = this.configService.getHbAccount();
    const credential: any = { username: username };
    const params = JSON.stringify(credential);
    // tslint:disable-next-line: max-line-length
    return forkJoin(this.httpsvc.post(this.configService.getApiWebFeederRest() + '/services/Authenticate/LogoutHomeBroker', params, Constants.httpOptions),
      this.tokenService.revokeAPIGatewayToken(),
      this.workspaceService.revokeAPIWorkspacesToken()).pipe(
        map(([resp0,]) => {
          return resp0;
        }));
  }

  brokerServiceInternalLogout() {
    // this.configService.deleteBrokerCookies(); TODO delete part of Cookies?
    const username = this.configService.getHbAccount();
    const credential: any = { username: username };
    const params = JSON.stringify(credential);
    return this.httpsvc.post(this.configService.getApiWebFeederRest() + '/services/Authenticate/LogoutHomeBroker',
      params, Constants.httpOptions);
  }

  changePassword(token: string, type: number, newPassword: string, broker: string) {
    const credential: OmsAdmin = {
      broker: broker,
      token: token,
      newPassword: newPassword,
      type: 1
    };
    const params = JSON.stringify(credential);
    return this.httpsvc.post(this.configService.getApiWebFeederRest() + '/services/AuthFast/ChangePassword',
      params, Constants.httpOptions)
      .map(res => {
        return res;
      })
      .catch((error: any) => [Observable.throw(error)]);
  }

  recoverPassword(email: string, type: number) {
    const credential: OmsAdmin = {
      broker: environment.BROKER,
      email: email,
      type: 1
    };
    const params = JSON.stringify(credential);
    return this.httpsvc.post(this.configService.getApiWebFeederRest() + '/services/AuthFast/RecoverPassword',
      params, Constants.httpOptions)
      .map(res => {
        return res;
      })
      .catch((error: any) => [Observable.throw(error)]);
  }

  difusionChangePassword(login: string, oldPassword: string, newPassword: string) {
    let params = '';
    const credential: ChangePassword = {
      username: login,
      password: oldPassword,
      newPassword: newPassword,
      clientDocument: '',
      softwareName: '',
      softwareVersion: '',
      ipAdress: ''
    };
    params = JSON.stringify(credential);
    return this._httpService
      .doPost(this.configService.getApiWebFeederRest(), 'services/Authenticate/ChangePasswordHomeBroker', params, false)
      .map(response => {
        if (response) {
          return response;
        }
        return [];
      });
  }

  clientRegisterSimulator(username: string, password: string) {
    const credential: OmsAdmin = {
      login: username, oldPassword: password
    };
    const params = JSON.stringify(credential);
    return this.httpsvc.post(this.configService.getApiWebFeederRest() + '/services/AuthFast/ClientRegisterSimulator',
      params, Constants.httpOptions)
      .map(res => {
        return res;
      })
      .catch((error: any) => [Observable.throw(error)]);
  }

  loginToken(token: string) {
    const params = { token: token };
    return this.httpsvc.post(this.configService.getApiWebFeederRest() + '/services/AuthFast/LoginToken',
      JSON.stringify(params), Constants.httpOptions);
  }

  isTokenExpired() {
    throw new Error('Method not implemented.');
  }

  refresh() {
    throw new Error('Method not implemented.');
  }

  validateSignatureValid(params: Object) {
    throw new Error('Method not implemented.');
  }

  refreshTransition(token: string) {
    throw new Error('Method not implemented.');
  }

  resetSignature(signature: string, newSignature: string) {
    throw new Error('Method not implemented.');
  }

  forgotSignature() {
    throw new Error('Method not implemented.');
  }

  advisorInfo(login?: string) {
    throw new Error('Method not implemented.');
  }
  advisorChangePassword(currentPassword: string, newPassword: string, guid?: string, advisorId?: number) {
    throw new Error('Method not implemented.');
  }
}
