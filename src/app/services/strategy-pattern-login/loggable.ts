export interface Loggable {
    login(username: string, password: string, profile?: any);
    brokerServiceLogin(username: string, password: string, broker: string);
    brokerServiceLogout();
    brokerServiceInternalLogout();
    recoverPassword(email: string, type: number, client?: boolean);
    changePassword(token: string, type: number, newPassword: string, broker: string);
    difusionChangePassword(login: string, oldPassword: string, newPassword: string);
    clientRegisterSimulator(username: string, password: string);
    isTokenExpired();
    refresh();
    refreshTransition(token: string);
    validateSignatureValid(params: Object);
    resetSignature(signature: string, newSignature: string);
    forgotSignature();
    advisorInfo(login?: string);
    advisorChangePassword(currentPassword: string, newPassword: string, guid?: string, advisorId?: number);
    loginToken(token: string);
}
