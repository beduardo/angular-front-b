import { ConfigService } from '@services/webFeeder/config.service';
import { Injectable } from '@angular/core';
import { Loggable } from '@services/strategy-pattern-login/loggable';
import { TokenCA } from '@model/wso2/tokenca';
import { Client } from '@model/wso2/client';
import { Assessor } from '@model/wso2/assessor';
import { CedroAccountService } from '@services/cedro-account.service';
import { BehaviorSubject, Observable } from 'rxjs';
import { Profile, Constants } from '@utils/constants';
import { UserCA } from '@model/wso2/userca';
import { AppUtils } from '@utils/app.utils';
import { HttpClient } from '@angular/common/http';
import { AuthService } from '@auth/auth.service';
import { Router } from '@angular/router';
import { MatDialog } from '@angular/material';
import { ErrorMessage } from '@model/error-message';
import { Signature } from '@model/signature.model';

@Injectable({
  providedIn: 'root'
})
export class Auth3Service implements Loggable {

  private token: TokenCA = null;
  private client: Client = null;
  private assessor: Assessor = null;
  private user: UserCA;
  private connectedAs = new BehaviorSubject<number>(this.checkBrokerAuthentication() ? 2 : 3);
  private ConnectionStatus = new BehaviorSubject<number>(0);
  public url;
  connectedAs$ = this.connectedAs.asObservable();
  ConnectionStatus$ = this.connectedAs.asObservable();

  constructor(
    private configService: ConfigService,
    private cedroAccountService: CedroAccountService,
    private authSvc: AuthService,
    private httpClient: HttpClient,
    private router: Router,
    private authService: AuthService,
    private modalService: MatDialog
  ) {
    // LoginW
    this.user = new UserCA();
  }

  login(username: string, password: string, profile?: any) {
    this.user.login = username;
    this.user.password = password;
    this.user.profile = profile;
    return new Observable<any>(observer => {
      return this.cedroAccountService.auth(this.user.login, this.user.password, this.user.profile === Profile.Client)
        .subscribe((dataAuth) => {
          if (dataAuth && dataAuth._isScalar === false) {
            observer.error('Ocorreu um erro!');
          } else if (dataAuth && dataAuth._body) {
            const json = JSON.parse(dataAuth._body);
            if (json && json.error_description === 'INVALID_CREDENTIALS') {
              observer.next(json.error_description);
            }
          } else {
            this.token = dataAuth;
            this.token.lastRefreshed = new Date();

            this.configService.setTokenLastRefreshed(AppUtils.getDateTimeLocaleFormat(this.token.lastRefreshed)); // TODO
            const _user = Object.assign({}, this.user);
            _user.name = this.token.access.user_name;
            _user.loggedIn = new Date();
            _user.tokenExpires = new Date(_user.loggedIn.getTime() + (this.token.expires_in * 1000));
            if (_user.profile === Profile.Client) {
              this.client = new Client(_user, false);
              this.assessor = null;
              this.httpClient.get(this.configService.USER_IDENTITY_API + 'users/clients',
                {
                  headers: {
                    'Authorization': 'Bearer ' + this.token.access_token,
                    // 'x-source-request': 'FastTradeWeb',
                    // 'x-source-tracker-id': AppUtils.newguidgen()
                  }
                })

                .subscribe((data: any) => {
                  if (data && data.length > 0) {
                    this.client.user_id = data[0].id;
                    this.client.id = data[0].client_id;
                    this.client.email = data[0].email;
                    this.client.name = data[0].full_name;
                    this.client.cpf = this.client.login;
                    this.client.profile_name = data[0].profile_name;
                    // TODO revising
                    this.client.accounts = [{
                      account: data[0].advisor_client_accounts[0].account_id,
                      market: data[0].advisor_client_accounts[0].market
                    }];
                    this.client.selectedAccount = {
                      account: this.client.accounts[0], balance: null, totalEquity: null
                    };
                    dataAuth.user = _user;
                    dataAuth.client = this.client;
                    observer.next(dataAuth);
                  }
                },
                );
            } else {
              this.client = null;
              this.assessor = new Assessor(_user);
              dataAuth.assessor = this.assessor;
              this.httpClient.get(this.configService.USER_IDENTITY_API + 'users?login=' + username,
                {
                  headers: {
                    'Authorization': 'Bearer ' + this.token.access_token,
                    // 'x-source-request': 'FastTradeWeb',
                    // 'x-source-tracker-id': AppUtils.newguidgen()
                  }
                })
                .subscribe((data: any) => {
                  dataAuth.assessor.id = data[0].id;
                  // this.httpClient.get(this.configService.USER_IDENTITY_API + 'users/clients',
                  this.httpClient.get(this.configService.getApiWebFeederRestFacade() +
                    '/services/negotiation/brokerServiceFullAccountListInformation?filter=&financial=false',
                    {
                      headers: {
                        'Authorization': 'Bearer ' + this.token.access_token,
                        // 'x-source-request': 'FastTradeWeb',
                        // 'x-source-tracker-id': AppUtils.newguidgen()
                      }
                    })
                    .subscribe((data2: any) => {
                      dataAuth.clients = data2.users;
                      // this.configService.setAssessorListClients(dataAuth.clients);
                      observer.next(dataAuth);
                    }, error => {
                      observer.error(error);
                    });
                }, error => {
                  observer.error(error);
                });
            }
            const currentUserTemp: any = {};
            currentUserTemp.loggedIn = _user.loggedIn;
            currentUserTemp.login = _user.login;
            currentUserTemp.name = _user.name;
            currentUserTemp.profile = _user.profile;
            currentUserTemp.tokenExpires = _user.tokenExpires;
            this.configService.setHbCurrentUser(currentUserTemp);
          }
        },
          (error) => {
            observer.error(error);
          }
        );
    });
  }

  checkBrokerAuthentication() {
    if (this.configService.getBrokerToken()) {
      return true;
    } else {
      return false;
    }
  }

  brokerServiceLogin(username: string, password: string, broker: string) {
    throw new Error('Method not implemented.');
  }

  brokerServiceLogout() {
    this.updateWorkspaceSelected(3);
    // TODO return this.iLogin.brokerServiceLogout();
    // TODO return Observable.of(this.cedroAccountService.revoke(this.getToken().access_token).subscribe());
    return this.cedroAccountService.revoke(this.configService.getTokenRefreshApi());
  }

  brokerServiceInternalLogout() {
    this.updateWorkspaceSelected(3);
    return this.cedroAccountService.revoke(this.configService.getTokenRefreshApi());
  }

  recoverPassword(email: string, type: number, client: boolean) {
    return new Observable<any>(observer => {
      return this.cedroAccountService.recoverPassword(email, client)
        .subscribe(res => {
          observer.next(res);
        });
    });
  }


  changePassword(token: string, type: number, newPassword: string) {
    throw new Error('Method not implemented.');
  }

  difusionChangePassword(login: string, oldPassword: string, newPassword: string) {
    return new Observable<any>(observer => {
      return this.cedroAccountService.changePassword(oldPassword, newPassword)
        .subscribe(res => {
          observer.next(res);
        }, error => {
          observer.next(false);
        });
    });
  }

  clientRegisterSimulator(username: string, password: string) {
    throw new Error('Method not implemented.');
  }

  loginToken(token: string) {
    throw new Error('Method not implemented.');
  }

  isTokenExpired() {
    if (new Date() >= AppUtils.strToDateTime(this.configService.getTokenExpiration())) {
      return true;
    }
    return false;
  }

  refresh() {
    if (this.configService.isLoginTypeWSO2()) {
      return new Observable<any>((observable) => {
        this.cedroAccountService.refresh(this.configService.getTokenRefreshApi())
          .subscribe((data) => {
            this.token = new TokenCA();
            this.token.access_token = data.access_token;
            this.token.refresh_token = data.refresh_token;
            this.token.scope = data.scope;
            this.token.token_type = data.token_type;
            this.token.expires_in = data.expires_in;

            this.token.lastRefreshed = new Date();
            this.configService.setTokenLastRefreshed(AppUtils.getDateTimeLocaleFormat(this.token.lastRefreshed));
            // if (this.isClient) {
            //     this.client.name = this.token.access.user_name;
            //     this.client.tokenExpires = new Date((new Date()).getTime() + (this.token.expires_in * 1000));
            // } else {
            //     this.assessor.name = this.token.access.user_name;
            //     this.assessor.tokenExpires = new Date((new Date()).getTime() + (this.token.expires_in * 1000));
            // }
            this.configService.writeLog('AUTH3 > Token: ' + this.token);
            this.configService.setTokenApi(data.access_token);
            this.configService.setTokenRefreshApi(data.refresh_token);
            this.configService.setScope(data.scope);
            this.configService.setTokenTp(data.token_type);
            this.configService.setTokenExpiration(AppUtils.getDateTimeLocaleFormat(new Date((new Date()).getTime()
              + (data.expires_in * 1000))));
            observable.next(data);
          }, (error) => {
            if (error.ok === false) {
              this.authService.logoutWithoutRouter();
              this.modalService.closeAll();
              this.router.navigate(['error'], {
                queryParams: new ErrorMessage(
                  Constants.ERROR_DENIED_ICON, '', Constants.MSG_LOGIN_MSG003, 'Voltar', 'login'
                )
              });
            }
            // ErrorUtils.resolveCAError(error);
            // observable.error(error);
          }
          );
      });
    }
  }

  refreshTransition(token?: string) {
    if (this.configService.isLoginTypeWSO2()) {
      return new Observable<any>((observable) => {
        let tokenRefresh = '';
        if (token) {
          tokenRefresh = token;
        } else {
          tokenRefresh = this.configService.getTokenRefreshApi();
        }
        this.cedroAccountService.refresh(tokenRefresh)
          .subscribe((data) => {
            this.configService.writeLog('AUTH3 > refreshTransition: ');
            this.configService.writeLog(data);
            if (data && data.access_token) {
              this.token = new TokenCA();
              this.configService.setToken(data.access_token);
              this.token.access_token = data.access_token;
              this.configService.setTokenApi(data.access_token);
              this.configService.setTokenGraphic(data.access_token);
              this.token.refresh_token = data.refresh_token;
              this.configService.setTokenRefreshApi(data.refresh_token);
              this.configService.setTokenExpiration(AppUtils.getDateTimeLocaleFormat
                (new Date((new Date()).getTime() + (data.expires_in * 1000))));
              this.token.scope = data.scope;
              this.configService.setScope(data.scope);
              this.token.token_type = data.token_type;
              this.configService.setTokenTp(data.token_type);
              this.token.expires_in = data.expires_in;
              this.token.lastRefreshed = new Date();
              this.configService.setTokenLastRefreshed(AppUtils.getDateTimeLocaleFormat(this.token.lastRefreshed));
              // if (this.isClient) {
              //     this.client.name = this.token.access.user_name;
              //     this.client.tokenExpires = new Date((new Date()).getTime() + (this.token.expires_in * 1000));
              // } else {
              //     this.assessor.name = this.token.access.user_name;
              //     this.assessor.tokenExpires = new Date((new Date()).getTime() + (this.token.expires_in * 1000));
              // }
              observable.next(true);
            } else {
              observable.next(false);
            }
          }, (error) => {
            observable.next(false);
            if (error.ok === false) {
              this.authService.logoutWithoutRouter();
              this.modalService.closeAll();
              this.router.navigate(['error'], {
                queryParams: new ErrorMessage(
                  Constants.ERROR_DENIED_ICON, '', Constants.MSG_LOGIN_MSG003, 'Voltar', 'login'
                )
              });
            }
            // ErrorUtils.resolveCAError(error);
            // observable.error(error);
          });
      });
    }
  }

  updateWorkspaceSelected(data: number) {
    this.connectedAs.next(data);
  }

  validateSignatureValid(params: Object) {
    return new Observable<any>(observer => {
      return this.cedroAccountService.validateSignature(params)
        .subscribe(res => {
          observer.next(res);
        });
    });
  }

  resetSignature(signature: string, newSignature: string) {
    return new Observable<any>(observer => {
      return this.cedroAccountService.resetSignature(signature, newSignature)
        .subscribe(res => {
          observer.next(res);
        }, (error) => {
          observer.next(false);
        });
    });
  }

  forgotSignature() {
    return new Observable<any>(observer => {
      return this.cedroAccountService.forgotSignature()
        .subscribe(res => {
          observer.next(res);
        }, error => {
          observer.next(false);
        });
    });
  }

  advisorInfo(login?: string) {
    return new Observable<any>(observer => {
      return this.cedroAccountService.advisorInfo(login)
        .subscribe(res => {
          observer.next(res[0]);
        }, error => {
          observer.next(false);
        });
    });
  }

  advisorChangePassword(currentPassword: string, newPassword: string, guid?: string, advisorId?: number) {
    return new Observable<any>(observer => {
      return this.cedroAccountService.advisorChangePassword(currentPassword, newPassword, guid, advisorId)
        .subscribe(res => {
          observer.next(res);
        }, error => {
          observer.next(false);
        });
    });
  }

}
