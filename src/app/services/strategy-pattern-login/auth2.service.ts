import { Router } from '@angular/router';
import { Injectable } from '@angular/core';
import { Loggable } from '@services/strategy-pattern-login/loggable';
import { HttpClient } from '@angular/common/http';
import { ConfigService } from '@services/webFeeder/config.service';
import { Login } from '@model/login.model';
import { Constants } from '@utils/constants';
import { Observable, forkJoin } from 'rxjs';
import { OmsAdmin } from '@model/recover-password';
import { environment } from 'environments/environment';
import { map } from 'rxjs/operators';
import { TokenService } from '@services/webFeeder/token.service';
import { WorkspaceNewService } from '@services/workspace-new.service';
import { MatDialog } from '@angular/material';

@Injectable({
  providedIn: 'root'
})
export class Auth2Service implements Loggable {

  public dialog: MatDialog;

  constructor(
    private httpsvc: HttpClient,
    private configService: ConfigService,
    private tokenService: TokenService,
    private workspaceService: WorkspaceNewService,
    // private authenticationService: AuthenticationService,
    dialog: MatDialog) {
    // LoginS
    this.dialog = dialog;
  }

  login(username: string, password: string, profile?: any) {
    const credential: Login = {
      username: username,
      password: password,
      softwareVersion: environment.version,
      softwareName: Constants.DEFAULT_SOFTWARE_NAME
    };
    const user = { username: credential.username, firstName: credential.username, lastName: credential.username };
    this.configService.setHbCurrentUser(user);
    const params = JSON.stringify(credential);
    return this.httpsvc.post(this.configService.getApiWebFeederRest() + '/services/AuthFast/LoginSignIn', params, Constants.httpOptions);
  }

  brokerServiceLogin(username: string, password: string, broker: string) {
    const credential: Login = {
      username: username, password: password, softwareVersion: environment.version, softwareName: Constants.DEFAULT_SOFTWARE_NAME
    };
    const params = JSON.stringify(credential);
    return this.httpsvc
      .post(this.configService.getApiWebFeederRest() + '/services/Authenticate/LoginHomeBroker', params, Constants.httpOptions)
      .map(res => {
        return res;
      })
      .catch((error: any) => [Observable.throw(error)]);
  }

  brokerServiceLogout() {
    const username = this.configService.getBrokerAccount();
    const credential: any = { username: username };
    const params = JSON.stringify(credential);
    // tslint:disable-next-line: max-line-length
    return forkJoin(this.httpsvc.post(this.configService.getApiWebFeederRest() + '/services/Authenticate/LogoutHomeBroker', params, Constants.httpOptions),
      this.tokenService.revokeAPIGatewayToken(),
      this.workspaceService.revokeAPIWorkspacesToken()).pipe(
        map(([resp0,]) => {
          return resp0;
        }));
    //     .subscribe(x => {
    //       this.dialog.closeAll();
    //       this.configService.deleteHbBrokerUser();
    //       this.configService.deleteFavoriteThemeUser();
    //       this.configService._authHbLoginType.next(3);
    //       this.router.navigate(['/login']);
    //     }, error => {
    //       this.dialog.closeAll();
    //       this.configService.deleteHbBrokerUser();
    //       this.configService.deleteFavoriteThemeUser();
    //       this.configService._authHbLoginType.next(3);
    //       this.router.navigate(['/login']);
    //       return Observable.throw(error);
    //     });
  }

  brokerServiceInternalLogout() {
    const username = this.configService.getBrokerAccount();
    const credential: any = { username: username };
    const params = JSON.stringify(credential);
    return this.httpsvc
      .post(this.configService.getApiWebFeederRest() + '/services/Authenticate/LogoutHomeBroker', params, Constants.httpOptions)
      .map(res => res)
      .catch((error: any) => [Observable.throw(error)]);
  }

  changePassword(token: string, type: number, newPassword: string, broker: string) {
    const credential: OmsAdmin = {
      broker: broker,
      token: token,
      newPassword: newPassword,
      type: 2
    };
    const params = JSON.stringify(credential);
    return this.httpsvc.post(this.configService.getApiWebFeederRest() + '/services/AuthFast/ChangePassword',
      params, Constants.httpOptions)
      .map(res => {
        return res;
      })
      .catch((error: any) => [Observable.throw(error)]);
  }

  recoverPassword(email: string, type: number) {
    const credential: OmsAdmin = {
      broker: environment.BROKER,
      email: email,
      type: 2
    };
    const params = JSON.stringify(credential);
    return this.httpsvc.post(this.configService.getApiWebFeederRest() + '/services/AuthFast/RecoverPassword',
      params, Constants.httpOptions)
      .map(res => {
        return res;
      })
      .catch((error: any) => [Observable.throw(error)]);
  }

  difusionChangePassword(login: string, oldPassword: string, newPassword: string) {
    const credential: OmsAdmin = {
      login: login, oldPassword: oldPassword, newPassword: newPassword
    };
    const params = JSON.stringify(credential);
    return this.httpsvc.post(this.configService.getApiWebFeederRest() + '/services/AuthFast/difusionChangePassword',
      params, Constants.httpOptions)
      .map(res => {
        return res;
      })
      .catch((error: any) => [Observable.throw(error)]);
  }

  clientRegisterSimulator(username: string, password: string) {
    const credential: Login = {
      username: username, password: password, softwareVersion: environment.version, softwareName: Constants.DEFAULT_SOFTWARE_NAME
    };
    const params = JSON.stringify(credential);
    return this.httpsvc.post(this.configService.getApiWebFeederRest() + '/services/AuthFast/ClientRegisterSimulator',
      params, Constants.httpOptions)
      .map(res => {
        return res;
      })
      .catch((error: any) => [Observable.throw(error)]);
  }

  loginToken(token: string) {
    throw new Error('Method not implemented.');
  }

  isTokenExpired() {
    throw new Error('Method not implemented.');
  }

  refresh() {
    throw new Error('Method not implemented.');
  }

  validateSignatureValid(params: Object) {
    throw new Error('Method not implemented.');
  }

  refreshTransition(token: string) {
    throw new Error('Method not implemented.');
  }

  resetSignature(signature: string, newSignature: string) {
    throw new Error('Method not implemented.');
  }

  forgotSignature() {
    throw new Error('Method not implemented.');
  }

  advisorInfo(login?: string) {
    throw new Error('Method not implemented.');
  }
  advisorChangePassword(currentPassword: string, newPassword: string, guid?: string, advisorId?: number) {
    throw new Error('Method not implemented.');
  }
}
