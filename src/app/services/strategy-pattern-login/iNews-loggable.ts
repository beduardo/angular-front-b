export interface INewsLoggable {

  getNewsByParameters(text: string, agency: string, dateStart: Date, dateEnd: Date);

}
