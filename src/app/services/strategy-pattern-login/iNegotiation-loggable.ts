import { MarketEnum } from '@model/enum/market.enum';
import { SideEnum } from '@model/enum/side.enum';
import { HistoryOrder } from '@model/webFeeder/history-order.model';
import { TypeOrderEnum } from '@model/enum/type-order.enum';

export interface INegotiationLoggable {

  brokerServiceAccountList(account: string);
  setQuantityOpenOrder(qtd: number);
  financialAccountInformationCompl(account: string, market: string);
  positionBovespa(account: string, quote: string);
  positionBmf(account: string, quote: string);
  getHistoryOrder(account: string, market: string, dataInicio: string, dataFim: string,
    symbol: string, status: string, direction: string, orderID?: string, type?: string, statusGeneric?: string, statusGroup?: string
  );
  sendOrderWso2(
    account: string, quote: string, fractionaryQtd: number, nonFractionaryQtd: number, price: string, market: MarketEnum,
    type: TypeOrderEnum, side: SideEnum, validityOrderTemp: Date, orderStrategy: string, timeinforce?: string, orderid?: string,
    origclordid?: string, loteDefault?: number, maxfloor?: string, minqty?: string, ordertag?: string, targettrigger?: string,
    targetlimit?: string, stoptrigger?: string, stoplimit?: string, isStop?: boolean, bypasssuitability?: boolean
  );
  sendOrderApi(account: string, quote: string, qtd: number, price: string, market: MarketEnum, type: any, side: SideEnum,
    validityOrder: Date, orderStrategy: string, timeinforce?: string, orderid?: string, origclordid?: string,
    loteDefault?: number, maxfloor?: string, minqty?: string, ordertag?: string, bypasssuitability?: boolean);
  sendOrderStopApi(account: string, validityOrder: Date, orderStrategy: string, quote: string, qtd: number, targettrigger: string,
    targetlimit: string, stoptrigger: string, stoplimit: string, market: MarketEnum, side: SideEnum, timeinforce?: string,
    orderid?: string, origclordid?: string, loteDefault?: number, isStart?: boolean, maxfloor?: string, minqty?: string, orderTag?: string,
    bypasssuitability?: boolean);
  cancelOrderWso2(account: string, username: string, orderid: string, origclordid: string, market: string,
    quote: string, qtd: string, side: string, enteringtrader: string);
  cancelOrderApi(account: string, username: string, orderid: string, origclordid: string, market: string,
    quote: string, qtd: string, side: string, enteringtrader: string);
  cancelAllOrderHistoryWso2(historyOrder: HistoryOrder[]);
  cancelAllOrderHistoryApi(historyOrder: HistoryOrder[]);
  suitabilityCheckMatrix(account: string, marketID: string, securityType: string, securityRisk: number,
    symbol: string, suitabilityID?: string, profileName?: string, client_id?: number);
  postAssignBoundsWso2(account: string, symbol: string, client_id: number, user_id: number);
  brokeragePDF(account: string, market: string, date: Date);
  statementAccount(account: string, market: string, dateStart: Date, dateEnd: Date, ipAddress: string);
  getAfcSearchQuestions();
  postAfcSearchQuestions(questionResp: string, questionType: string);
}
