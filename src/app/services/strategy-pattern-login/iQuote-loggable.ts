export interface IQuoteLoggable {

  quotesQuery(quote: string, n?: number, showOptions?: boolean, showFractionals?: boolean);
  getQuote(quote: string);
  getVolumeAtPrice(quote: string, period: number);
  loadListPlayer();
  getPlayerName(code: string);

}
