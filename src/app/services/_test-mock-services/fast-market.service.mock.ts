import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class FastMarketServiceMock {

  constructor() { }

  getLastFastMarketNews(qtd: number, page: number, search?: string, dateStartFilter?: Date, dateEndFilter?: Date) {
    return Observable.of([]);
  }
}
