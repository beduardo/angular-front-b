import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthServiceMock {

  constructor() { }

  clearSession(): void { }
  logoutSession(): boolean {
    return false;
  }
  logout(): void { }

  logoutWithoutRouter(): void { }

  sessionExpired(): void { }
  isAuthenticated(viaLogin: boolean): boolean {
    return false;
  }

  validateTaskTime() {
    return Observable.of({});
  }

  valideToken(account: string, tokenApi: string, tokenWF: string): Observable<any> {
    return Observable.of({});
  }

  loginViaToken(tokenAuth: string): Observable<any> {
    return Observable.of({});
  }

  getUserLogged() {
    return null;
  }

  setUserLogged(userlogged: string): void {
  }

  showNavBar(ifShow: boolean): void {
  }

}
