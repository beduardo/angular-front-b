import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
import { Loggable } from '@services/strategy-pattern-login/loggable';
@Injectable({
  providedIn: 'root'
})
export class AuthenticationServiceMock {

  public iLogin: Loggable;

  constructor(
  ) { }

  brokerServiceLogout() {
    return this.iLogin.brokerServiceLogout();
  }

  logout(): void { }

}
