import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class StatusDifusionServiceMock {
  constructor() { }

  public subscribe() {
    this.sendMessageStatusDifusion(true);
  }
  public unsubscribe() {
    this.sendMessageStatusDifusion(false);
  }

  private sendMessageStatusDifusion(status: boolean) {

  }

}
