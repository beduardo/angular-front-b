import { Injectable } from '@angular/core';
import { Subject, BehaviorSubject } from 'rxjs';
import { StatusConnection } from '@model/status-connection';

@Injectable({
  providedIn: 'root'
})
export class StatusOmsServiceMock {
  public messagesStatusOms: Subject<any>;

  constructor() { }


  public subscribe(): void {
    this.sendMessageStatusOms(true);
  }
  public unsubscribe(): void {
    this.sendMessageStatusOms(false);
  }

  private sendMessageStatusOms(status: boolean): void {
    
  }

}
