import { ConfigService } from '@services/webFeeder/config.service';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Observable } from 'rxjs';
import { environment } from 'environments/environment';
import { NotificationsService } from 'angular2-notifications';
import { Constants } from '@utils/constants';
import { MatDialog } from '@angular/material';
// import { AuthenticationService } from '../services/authentication.service';
import { CustomSnackbarService } from '@services/custom-snackbar-service.service';
export enum AuthState {
  LOGGED_IN,
  LOGGED_OUT
}
@Injectable({
  providedIn: 'root'
})
export class AutoLogoutServiceMock {
  public authAlertChange$: Observable<AuthState>;
  public _resetClockDownTimer = new BehaviorSubject<boolean>(false);
  public resetClockDownTimer$ = this._resetClockDownTimer.asObservable();
  public _statusAlert = new BehaviorSubject<boolean>(false);
  public statusAlertTimeout$ = this._statusAlert.asObservable();
  constructor() {}

}
