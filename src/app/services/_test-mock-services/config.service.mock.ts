﻿import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})

export class ConfigServiceMock {
  constructor() { }

  getAssessorListClients() {
    return [];
  }

  getUrlWebSocketWebFeeder(numberReconect: number, graphic?: boolean): string {
    return null;
  }

  writeLog(msg): void { }

  writeError(msg): void { }

  getLoginTypeEnabled(): string {
    return null;
  }

  isLoginTypeWSO2(): boolean {
    return false;
  }

  public getBrokerToken(): string {
    return null;
  }

  isLoginPi(): boolean {
    return false;
  }

  getBrokerAccountSelected(): string {
    return null;
  }

  getWhiteLabel(): number {
    return 1;
  }

  getApiWebFeederRest(): string {
    return null;
  }

  getTokenApi(): string {
    return null;
  }

  getHbCurrentUser(): string {
    return null;
  }

  deleteHbBrokerUser(): void { }
  deleteFavoriteThemeUser(): void { }

  getFastMarketApiUrl(): string {
    return null;
  }

  getToken(): string {
    return null;
  }

  getHbAccount():string {
    return null;
  }

  getYouTubeLiveApi(): string {
    return null;
  }

}
