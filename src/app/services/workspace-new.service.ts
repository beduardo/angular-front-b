import { Injectable } from '@angular/core';
import { Observable, BehaviorSubject } from 'rxjs';
import { HttpService } from '@services/http-service';
import { ConfigService } from '@services/webFeeder/config.service';
import { AuthService } from '@auth/auth.service';
import { WorkspaceNewSaveDto } from '@dto/workspace-new-save-dto';
import { Constants } from '@utils/constants';
import { WorkspaceNewDto } from '@dto/workspacenew.dto';
import { PositionDto } from '@dto/position.dto';
import { WorkspaceItemContentDto } from '@dto/workspace-item-content-dto';
import { Box } from '@business/home/workspace-new/workspace-new.component';
import { CustomWorkspaceItemInterface } from '@dto/custom-workspace-item-interface.dto';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { AppUtils } from '@utils/app.utils';

@Injectable({
  providedIn: 'root'
})
export class WorkspaceNewService {
  private userWorkspaces = [];
  private wksPredefined = [];
  private wksSelectedId: number = Number(0);
  private workspaceSelected: WorkspaceNewDto;
  private _removeCard =
    new BehaviorSubject<CustomWorkspaceItemInterface>({ id: null, type: '', assetSymbol: '', company: '', title: '', removeFlag: false });
  removeCard$ = this._removeCard.asObservable();
  loggedIn = false;

  private _callScrollDown = new BehaviorSubject<boolean>(false);
  callScrollDown$ = this._callScrollDown.asObservable();

  private _windowResized = new BehaviorSubject<number>(0);
  windowResized$ = this._windowResized.asObservable();

  private dataSource = new BehaviorSubject<any>(this.userWorkspaces);
  data$ = this.dataSource.asObservable();

  private _workspaceSelected = new BehaviorSubject<number>(0);
  workspaceSelected$ = this._workspaceSelected.asObservable();

  private _showAddWorkspaces = new BehaviorSubject<boolean>(true);
  showAddWorkspaces$ = this._showAddWorkspaces.asObservable();

  private _showMessageToAddWks = new BehaviorSubject<boolean>(false);
  showMessageToAddWks$ = this._showMessageToAddWks.asObservable();

  private _workspaceLock = new BehaviorSubject<boolean>(true);
  workspaceLock$ = this._workspaceLock.asObservable();

  public _successAddComponent = new BehaviorSubject<boolean>(true);
  _successAddComponent$ = this._successAddComponent.asObservable();

  public _openTradingView = new BehaviorSubject<boolean>(true);
  _openTradingView$ = this._openTradingView.asObservable();

  private _resizeComponent = new BehaviorSubject<{ componentId: number, cols: number, rows: number }>(null);
  resizeComponent$ = this._resizeComponent.asObservable();

  constructor(private serviceHTTP: HttpService,
    private configService: ConfigService,
    private authService: AuthService,
    private httpClient: HttpClient) { }

  gridResized() {
    this._windowResized.next(1);
  }

  updatedUserWorkspaces(data: any) {
    this.dataSource.next(data);
    this.userWorkspaces = data;
  }

  updateWorkspaceSelected(data: number) {
    this._workspaceSelected.next(data);
    this.wksSelectedId = data;
    for (let i = 0; i < this.userWorkspaces.length; i++) {
      if (this.userWorkspaces[i].Id == data) {
        this.workspaceSelected = this.userWorkspaces[i] as WorkspaceNewDto;
      }
    }
  }

  callScrollDownMethod() {
    this._callScrollDown.next(true);
  }

  updateShowMessageToAddWks(data: boolean) {
    this._showMessageToAddWks.next(data);
  }

  updateShowAddWorkspace(data: boolean) {
    this._showAddWorkspaces.next(data);
  }

  updateWorkspaceLock(data: boolean) {
    this._workspaceLock.next(data);
  }

  getWksSelectedId() {
    return this.wksSelectedId;
  }

  getWorkspaceSelected(): WorkspaceNewDto {
    return this.workspaceSelected;
  }

  getUserWorkspaceById(Id: number): WorkspaceNewDto {
    for (let i = 0; i < this.userWorkspaces.length; i++) {
      if (this.userWorkspaces[i].Id == Id) {
        return this.userWorkspaces[i] as WorkspaceNewDto;
      }
    }
    return null;
  }

  getUserWorkspace(index: number) {
    return this.userWorkspaces[index];
  }

  getUserWorkspaces() {
    return this.userWorkspaces;
  }

  getPredefinedWorkspace(index: number) {
    return this.wksPredefined[index];
  }

  getPredefinedWorkspaces() {
    return this.wksPredefined;
  }

  validNumberWorkspace() {
    if (!this.userWorkspaces[0] || this.userWorkspaces[0].Id === undefined || this.userWorkspaces[0].Id === null) {
      return false;
    } else {
      return true;
    }
  }

  resizeComponent(data: { componentId: number, cols: number, rows: number }) {
    this._resizeComponent.next(data);
  }

  getResizeComponent() {
    return this.resizeComponent$;
  }

  loadWorkspaces(username: string, ispredefined: boolean): Observable<any> {
    if (username) {
      return this.serviceHTTP
        .doGet(this.configService.getApiWorkspace(), 'api/' + username + '/workspace')
        .map(response => {
          if (response) {
            const arrReturn = [];
            for (let i = 0; i <= response.length - 1; i++) {
              if (response[i].WorkspaceType === Constants.WORKSPACE_TYPE.Web) {
                const value = response[i];
                value.Name = JSON.parse(value.JsonData).title;    // Treat Workspace Api Bug!
                arrReturn.push(value);
              }
            }
            let userHb = '';
            if (this.configService.getHbAccountName()) {
              userHb = this.configService.getHbAccountName().split(' ')[0];
            }
            if (arrReturn.length >= Constants.NUM_MAX_WORKSPACES && (username === this.configService.getHbAccount() ||
              username === userHb || username === this.configService.getUserSystemLogged())) {
              this.updateShowAddWorkspace(false);
            } else {
              this.updateShowAddWorkspace(true);
            }
            // save data to share
            if (!ispredefined) {
              this.userWorkspaces = arrReturn;
            } else {
              this.wksPredefined = arrReturn;
            }
            return arrReturn;
          }
          return null;
        });
    } else {
      return Observable.of([]);
    }
  }

  saveWorkspace(workspace: WorkspaceNewSaveDto): Observable<any> {
    if (this.userWorkspaces.length >= Constants.NUM_MAX_WORKSPACES) {
      this.updateShowAddWorkspace(false);
      return null;
    } else {
      if (this.userWorkspaces.length === (Constants.NUM_MAX_WORKSPACES - 1)) {
        this.updateShowAddWorkspace(false);
      }
      return this.serviceHTTP
        .doPost(this.configService.getApiWorkspace(), 'api/workspace/' + this.authService.getUserLogged()
          + '/' + Constants.WORKSPACE_TYPE.Web, JSON.stringify(workspace), false)
        .map(response => {
          if (response) {
            const value = response;
            value.Name = JSON.parse(value.JsonData).title; // Treat Workspace Api Bug!
            return response;
          }
          return null;
        });
    }
  }

  getWorkspaceById(workspaceId: number): Observable<any> {
    if (workspaceId) {
      return this.serviceHTTP
        .doGet(this.configService.getApiWorkspace(), 'api/workspace/' + workspaceId)
        .map(response => {
          if (response) {
            response.Name = JSON.parse(response.JsonData).title; // Treat Workspace Api Bug!
            return response;
          }
          return null;
        });
    } else {
      return Observable.of({});
    }
  }

  updateWorkspace(workspaceId, content): Observable<any> {
    return this.serviceHTTP
      .doPut(this.configService.getApiWorkspace(), 'api/workspace/' + workspaceId, content, false)
      .map(response => {
        if (response) {
          const value = response;
          value.Name = JSON.parse(value.JsonData).title; // Treat Workspace Api Bug!
          return value;
        }
        return null;
      });
  }

  deleteWorkspace(workspaceId): Observable<any> {
    return this.serviceHTTP
      .doDelete(
        this.configService.getApiWorkspace(),
        'api/workspace',
        workspaceId,
        false
      )
      .map(msg => {
        if (msg && (msg.status === 200 || msg === 1)) {
          return msg;
        } else {
          console.error(msg);
          return null;
        }
      });
  }

  isNameBlank(newName) {
    if (newName.trim() === '') {
      return true;
    } else {
      return false;
    }
  }

  validateWorkspaceName(newName: string, workspaceChangedId: number): boolean {
    for (let i = 0; i < this.userWorkspaces.length; i++) {
      if (this.userWorkspaces[i].JsonData) {
        const workspace = JSON.parse(this.userWorkspaces[i].JsonData);
        if ((workspace.title.trim() == newName.trim()) && (this.userWorkspaces[i].Id !== workspaceChangedId)) {
          return false;
        }
      }
    }
    return true;
  }

  getItemDefaultSettings(type: string) {
    for (let i = 0; i < Constants.items_settingsArr.length; i++) {
      if (type == Constants.items_settingsArr[i].type) {
        return Constants.items_settingsArr[i];
      }
    }
    return null;
  }

  mountItemPositionDefaultSettings(itemSettings): PositionDto {
    const wksItemPos: PositionDto = new PositionDto(0, 0, 2, 1);
    if (itemSettings !== null) {
      if (itemSettings.position && itemSettings.position.x !== undefined) {
        wksItemPos.x = itemSettings.position.x;
      }
      if (itemSettings.position && itemSettings.position.y !== undefined) {
        wksItemPos.y = itemSettings.position.y;
      }
      if (itemSettings.position && itemSettings.position.cols !== undefined) {
        wksItemPos.cols = itemSettings.position.cols;
      }
      if (itemSettings.position && itemSettings.position.rows !== undefined) {
        wksItemPos.rows = itemSettings.position.rows;
      }
      if (itemSettings.position && itemSettings.position.dragEnabled !== undefined) {
        wksItemPos.dragEnabled = itemSettings.position.dragEnabled;
      }
      if (itemSettings.position && itemSettings.position.resizeEnabled !== undefined) {
        wksItemPos.resizeEnabled = itemSettings.position.resizeEnabled;
      }
      if (itemSettings.position && itemSettings.position.compactEnabled !== undefined) {
        wksItemPos.compactEnabled = itemSettings.position.compactEnabled;
      }
      if (itemSettings.position && itemSettings.position.maxItemRows !== undefined) {
        wksItemPos.maxItemRows = itemSettings.position.maxItemRows;
      }
      if (itemSettings.position && itemSettings.position.minItemRows !== undefined) {
        wksItemPos.minItemRows = itemSettings.position.minItemRows;
      }
      if (itemSettings.position && itemSettings.position.maxItemCols !== undefined) {
        wksItemPos.maxItemCols = itemSettings.position.maxItemCols;
      }
      if (itemSettings.position && itemSettings.position.minItemCols !== undefined) {
        wksItemPos.minItemCols = itemSettings.position.minItemCols;
      }
      if (itemSettings.position && itemSettings.position.minItemArea !== undefined) {
        wksItemPos.minItemArea = itemSettings.position.minItemArea;
      }
      if (itemSettings.position && itemSettings.position.maxItemArea !== undefined) {
        wksItemPos.maxItemArea = itemSettings.position.maxItemArea;
      }
    }
    return wksItemPos;
  }

  getNextContentItemId(contentArr: Array<WorkspaceItemContentDto>) {
    let max = -1;
    for (let i = 0; i < contentArr.length; i++) {
      if (contentArr[i].id > max) {
        max = contentArr[i].id;
      }
    }
    return max + 1;
  }

  public workspaceSaveOnDemand(box: Box) {
    const data = this.convertBoxToWorkspaceItemContent(box);
    this.userWorkspaces.forEach(element => {
      if (element.Id === box.workspaceId) {
        const wksObjToUpdate = element as WorkspaceNewDto; // as WorkspaceNewDto WorkspaceNewSaveDto
        const content: Array<any> = JSON.parse(wksObjToUpdate.JsonData);
        const itContArrUpdated: Array<WorkspaceItemContentDto> = new Array<WorkspaceItemContentDto>();
        JSON.parse(wksObjToUpdate.JsonData).content.forEach(elem => {
          if (data.id === elem.id) {
            itContArrUpdated.push(data);
          } else {
            itContArrUpdated.push(elem);
          }
        });
        const jsondata = JSON.parse(wksObjToUpdate.JsonData) as WorkspaceNewSaveDto;
        jsondata.content = itContArrUpdated;
        this.updateWorkspace(box.workspaceId, jsondata).subscribe();
        // FLOW TO SAVE WORKSPACE WHEN A COMPONENT IS EDITED
        const workspaceJson = JSON.parse(this.userWorkspaces[this.userWorkspaces.indexOf(element)].JsonData);
        workspaceJson.content = jsondata.content;
        this.userWorkspaces[this.userWorkspaces.indexOf(element)].JsonData = JSON.stringify(workspaceJson);
        this.updatedUserWorkspaces(this.userWorkspaces);
        this.updateWorkspaceSelected(this.wksSelectedId);
      }
    });
  }

  public convertBoxToWorkspaceItemContent(box: Box): WorkspaceItemContentDto {
    const posObj = new PositionDto(box.x, box.y, box.cols, box.rows, box.dragEnabled, box.resizeEnabled,
      box.compactEnabled, box.maxItemRows, box.minItemRows, box.maxItemCols, box.minItemCols, box.minItemArea, box.maxItemArea);
    return new WorkspaceItemContentDto(
      box.id,
      box.symbol,
      box.company,
      box.type,
      posObj,
      box.showInGridster,
      box.workspaceId,
      null,
      box.worksheetTitle,
      box.columns);
  }

  public setRemoveCard(value: CustomWorkspaceItemInterface) {
    this._removeCard.next(value);
  }

  setLogged() {
    this.loggedIn = true;
  }

  isLogged() {
    return this.loggedIn;
  }

  public revokeAPIWorkspacesToken() {
    const token = this.configService.getTokenApi();
    const httpHeaders =  new HttpHeaders({
      'Content-Type': 'application/json',
      'Access-Control-Allow-Origin': '*',
      'Access-Control-Allow-Headers': 'Origin, X-Requested-With, Content-Type, Accept, Authorization',
      'Access-Control-Allow-Methods': 'POST, GET, PUT, DELETE',
      'Authorization': 'Bearer ' + token,
      // 'x-source-request': 'FastTradeWeb',
      // 'x-source-tracker-id': AppUtils.newguidgen()
    });
    const options = {
      headers: httpHeaders
    };
    return this.httpClient
      .post(
        this.configService.getApiWorkspace() +
        `/api/token/revoke`,
        {},
        options
      )
      .map(response => {
        if (response) {
          return response;
        }
        return [];
      })
      .catch(error => {
        throw error;
      });
  }
  // public static WINDOW_NAME_ASSET_SUMMARY = 'asset-summary';
  // public static WINDOW_NAME_BOOK = 'book';
  // public static WINDOW_NAME_WORK_SHEET = 'worksheet';
  // public static WINDOW_NAME_NEWS_SEARCH = 'news-search';
  // public static WINDOW_NAME_MARKET_HIGHLIGHTS = 'market';
  // public static WINDOW_NAME_QUOTE_BOX = 'quoted-box';
  // public static WINDOW_NAME_TIMES_TRADES = 'times-trades';
  // public static WINDOW_NAME_BILLET_DAY_TRADE = 'billet-day-trade';
  // public static WINDOW_NAME_POSITION_DAY = 'position-day';
  // public static WINDOW_NAME_QUOTE_CHART = 'quote-chart';
  // public static WINDOW_NAME_VOLUME_AT_PRICE = 'volume-at-price';
  // for (let i = 0; i < this.userWorkspaces.length; i++) {
  //     if (this.userWorkspaces[i].JsonData) {
  //       const workspace = JSON.parse(this.userWorkspaces[i].JsonData);
  //       if ((workspace.title.trim() == newName.trim()) && (this.userWorkspaces[i].Id !== workspaceChangedId)) {
  //         return false;
  //       }
  //     }
  //   }

  // TODO Tarcisio
  // validateOnlyOneAssetAssigned(assetName: string, type: string, idWksSelected?: number): boolean {
  //   const userWorkspaces = this.getUserWorkspaces();
  //   for (let i = 0; i < userWorkspaces.length; i++) {
  //     const workspace = JSON.parse(userWorkspaces[i].JsonData);
  //     for (let j = 0; j < workspace.content.length; j++) {

  //     }
  //   }
  //   return false;
  // }
}
