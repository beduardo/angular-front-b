import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ConfigService } from '@services/webFeeder/config.service';
import { Constants } from '@utils/constants';

@Injectable({
  providedIn: 'root'
})
export class YoutubeLiveService {

  private channelId = Constants.YOUTUBE_CHANNEL_ID;
  private key = Constants.GOOGLE_SERVICE_KEY;

  constructor(private http: HttpClient,
    private config: ConfigService) { }

  getFastLive() {
    const url = 'search?';
    return this.http.get<any>(
      this.config.getYouTubeLiveApi() + url, {
        params: {
          part: 'snippet',
          channelId: this.channelId,
          eventType: 'live',
          type: 'video',
          key: this.key
        }
      }).map(response => {
        if (response) {
          return response;
        }
        return null;
      });
  }
}
