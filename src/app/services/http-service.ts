import { AppUtils } from './../utils/app.utils';
import { TransitionService } from './transition.service';
import { CedroAccountService } from '@services/cedro-account.service';
import { AuthenticationService } from './authentication.service';
import { Router } from '@angular/router';
import { Injectable } from '@angular/core';
import { Headers, Http, RequestOptions, Response } from '@angular/http';
import { Observable } from 'rxjs';
import { ConfigService } from '@services/webFeeder/config.service';
import { WebFeederFactory } from '@connections/web-feeder.factory';
import { HttpErrorResponse, HttpHeaders, HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class HttpService {

  constructor(
    private http: Http,
    private configService: ConfigService,
    private httpPdf: HttpClient
  ) { }

  // newguidgen() {
  //   function id() {
  //     return Math.floor((1 + Math.random()) * 0x10000)
  //       .toString(16)
  //       .substring(1);
  //   }
  //   return id() + id() + '-' + id() + '-' + id() + '-' +
  //     id() + '-' + id() + id() + id();
  // }

  getHeaders(url?: string) {
    // console.log('\n\nHTTP SERVER >>> getHeaders - URL: ' + url);
    if (!this.configService.isLoginTypeWSO2()) {
      const token = this.configService.getTokenApi();
      if (WebFeederFactory.appComponent && WebFeederFactory.appComponent.setValueToken) {
        WebFeederFactory.appComponent.setValueToken(token);
      }
      const headers = new Headers();
      headers.append('Content-Type', 'application/json');
      headers.append('Access-Control-Allow-Origin', '*');
      headers.append('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept, Authorization');
      headers.append('Access-Control-Allow-Methods', 'POST, GET, PUT, DELETE');
      // headers.append('x-source-request', 'FastTradeWeb');
      headers.append('x-source-tracker-id', AppUtils.newguidgen());
      if (token !== '' && token !== null) {
        headers.append('Authorization', 'Bearer ' + token);
      }
      // console.log(token);
      return headers;
    } else {
      const token = this.configService.getTokenApi();
      // console.log('HTTP SERVER >>> getHeaders - TOKEN: ' + token + '\n\n');
      const headers = new Headers();
      headers.append('Content-Type', 'application/json');
      // headers.append('x-source-request', 'FastTradeWeb');
      // headers.append('x-source-tracker-id', AppUtils.newguidgen());
      if (token !== '' && token !== null && (url.indexOf(this.configService.getApiWebFeederRest()) !== -1
        || url.indexOf(this.configService.USER_IDENTITY_API) !== -1)) {
        headers.append('Authorization', 'Bearer ' + token);
      }
      return headers;
    }
  }

  getHeadersWso2(noAuthentication?, isOrder?: boolean) {
    const token = this.configService.getTokenApi();
    if (WebFeederFactory.appComponent && WebFeederFactory.appComponent.setValueToken) {
      WebFeederFactory.appComponent.setValueToken(token);
    }
    const headers = new Headers();
    headers.append('Content-Type', 'application/x-www-form-urlencoded');
    headers.append('Access-Control-Allow-Origin', '*');
    // headers.append('x-source-request', 'FastTradeWeb');
    // headers.append('x-source-tracker-id', apputils.newguidgen());
    if (token !== '' && token !== null) {
      headers.append('Authorization', 'Bearer ' + token);
    }
    if (isOrder) {
      headers.append('signature',
        btoa(JSON.stringify({ user_client_id: + this.configService.getHbAccountId(), signature: this.configService.getHbSignature() })));
    }
    return headers;
  }

  handleResponse(
    observable: Observable<Response>,
    url?: string,
    loader?: boolean
  ) {
    let result = null;
    try {
      result = observable
        .map(res => {
          // console.log('\n\n\n-------------------------------------------------------------------------------------');
          // console.log('url: ' + url);
          // console.log(res);
          // console.log('-------------------------------------------------------------------------------------\n\n\n');
          return this.extractData(res);
        })
        .catch((error: any) => this.onCatch(error))
        .catch((error: any) => [Observable.throw(error)])
        .finally(() => { });
    } catch (error) {
      Observable.throw(error);
    }
    return result;
  }

  private extractData(res: Response) {
    const text = res.text();
    let body;
    if (text.indexOf('Not Permit lastTime') < 0) {
      body = res.json();
    }
    if (body) {
      return body.Data || body;
    } else {
      return {};
    }
  }

  public doGet(api: string, path?: string, loader?: boolean) {
    let url = api + '/';
    path !== undefined ? (url += path) : (url += '');
    const requestOptions = new RequestOptions({ headers: this.getHeaders(url) });
    // this.configService.writeLog('doGet >>>>: ' + url);
    return this.handleResponse(this.http.get(url, requestOptions));
  }

  public doGetPdf(api: string, path?: string) {
    let url = api + '/';
    path !== undefined ? (url += path) : (url += '');
    if (!this.configService.isLoginTypeWSO2()) {
      const token = this.configService.getTokenApi();
      if (WebFeederFactory.appComponent && WebFeederFactory.appComponent.setValueToken) {
        WebFeederFactory.appComponent.setValueToken(token);
      }
      if (token !== '' && token !== null) {
        const HTTPOptions = {
          headers: new HttpHeaders({
            'Accept': 'application/pdf',
            'Content-Type': 'application/json',
            'Access-Control-Allow-Origin': '*',
            'Access-Control-Allow-Headers': 'Origin, X-Requested-With, Content-Type, Accept, Authorization',
            'Access-Control-Allow-Methods': 'POST, GET, PUT, DELETE',
            'Authorization': 'Bearer ' + token
          }),
          'responseType': 'blob' as 'json'
        };
        return this.httpPdf.get<any[]>(url, HTTPOptions).map(((data) => data));
      } else {
        const HTTPOptions = {
          headers: new HttpHeaders({
            'Accept': 'application/pdf',
            'Content-Type': 'application/json',
            'Access-Control-Allow-Origin': '*',
            'Access-Control-Allow-Headers': 'Origin, X-Requested-With, Content-Type, Accept, Authorization',
            'Access-Control-Allow-Methods': 'POST, GET, PUT, DELETE'
          }),
          'responseType': 'blob' as 'json'
        };
        return this.httpPdf.get<any[]>(url, HTTPOptions).map(((data) => data));
      }
    }
  }

  public doGetWso2(api: string, path?: string, loader?: boolean) {
    let url = api;
    path !== undefined ? (url += path) : (url += '');
    const requestOptions = new RequestOptions({ headers: this.getHeaders(url) });
    // this.configService.writeLog('doGet >>>>: ' + url);
    return this.handleResponse(this.http.get(url, requestOptions));
  }

  public doPost(api: string, path: string, params?: any, headerAuthentication?: boolean, loader?: boolean) {
    let url = '';
    if (!path || path === '' || path === null) {
      url = api + '/' + path;
    } else {
      url = api + '/' + path;
    }
    const requestOptions = new RequestOptions({
      headers: this.getHeaders(url),
      method: 'POST'
    });
    // this.configService.writeLog('doPost >>>>: ' + url + '\nparans: ');
    // this.configService.writeLog(params);
    return this.handleResponse(this.http.post(url, params, requestOptions), url, loader);
  }

  public doPostWso2(api: string, path: string, params?: any, headerAuthentication?: boolean, loader?: boolean, isOrder?: boolean) {
    let url = '';
    if (!path || path === '' || path === null) {
      url = api;
    } else {
      url = api + '/' + path;
    }
    const requestOptions = new RequestOptions({
      headers: this.getHeadersWso2(url, isOrder),
      method: 'POST'
    });
    // this.configService.writeLog('doPostWso2 >>>>: ' + url + '\nparans: ');
    // this.configService.writeLog(params);
    return this.handleResponse(
      this.http.post(url, params, requestOptions),
      url,
      loader
    );
  }

  public doPost2(api: string, path: string, params?: any, noAuthentication?: boolean, loader?: boolean) {
    const url = api + path;
    const headers = this.getHeadersPostWso2(noAuthentication);
    const requestOptions = new RequestOptions({
      headers: headers,
      method: 'POST'
    });

    // this.configService.writeLog('doPost2 >>>>: ' + url + '\nparans: ');
    // this.configService.writeLog(params);
    return this.handleResponse(this.http.post(url, params, requestOptions), url, loader);
  }

  getHeadersPostWso2(noAuthentication?) {
    const token = this.configService.getTokenApi();
    if (
      WebFeederFactory.appComponent &&
      WebFeederFactory.appComponent.setValueToken
    ) {
      WebFeederFactory.appComponent.setValueToken(token);
    }
    const headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('Access-Control-Allow-Origin', '*');
    // headers.append('x-source-request', 'FastTradeWeb');
    // headers.append('x-source-tracker-id', apputils.newguidgen());
    if (token && !noAuthentication) {
      headers.append('Authorization', 'Bearer ' + token);
    }
    return headers;
  }

  public doPut(api: string, path: string, params?: any, loader?: boolean) {
    const url = api + '/' + path;
    const requestOptions = new RequestOptions({ headers: this.getHeaders(url) });
    // this.configService.writeLog('doPut >>>>: ' + url + '\nparans: ');
    // this.configService.writeLog(params);
    return this.handleResponse(this.http.put(url, params, requestOptions), url, loader);
  }

  public doDelete(api: string, path: string, params: any, loader?: boolean) {
    const url = api + '/' + path;
    const urlParam = url + '/' + params;
    const requestOptions = new RequestOptions({ headers: this.getHeaders(url) });
    // this.configService.writeLog('doDelete >>>>: ' + urlParam + '\nparans: ');
    // this.configService.writeLog(params);
    return this.handleResponse(this.http.delete(urlParam, requestOptions), urlParam, loader);
  }

  private onCatch(error: HttpErrorResponse) {
    this.errorsStatus(error);
    if (this.configService.isLoginTypeWSO2() && (error.status === 400 || error.status === 422)) {
      return Observable.of(error);
    }
    return Observable.throw(error);
  }

  private errorsStatus(error) {
    this.configService.writeLog('HTTP SERVICE > errorsStatus:');
    this.configService.writeLog(error);
    if (error.status === 401) {
      this.configService.writeLog('HTTP SERVICE > errorCode: ' + error.status + ' - URL: ' + error.url);
    } else if (error.status === 110) {
      this.configService.deleteBrokerCookies();
      this.configService.deleteHbBrokerUser();
      this.configService.deleteCookie();
      this.configService.deleteFavoriteThemeUser();
      this.configService._authHbLoginType.next(3);
      this.configService.writeLog('HTTP SERVICE > URL: ' + error.url + ' - errorsStatus: ' + error.status + ' - body: ' + error.body);
      this.configService.redirectExpireSession();
    }
  }

}
