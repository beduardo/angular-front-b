import { Injectable, Injector } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map, filter, scan } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ConfigurationService {
  config: any;
  constructor(private injector: Injector) { }

  load(url: string) {
    const injectHttp = this.injector.get(HttpClient);
    return new Promise((resolve) => {
      injectHttp.get(url).pipe(
        map(res => res)
      ).subscribe(config => {
        this.config = config;
        resolve();
      });
    });
  }

  getConfiguration(element: string, dataList?: string, includeBar: boolean = false) {
    let urlWithElement;
    if (!dataList) {
      urlWithElement = this.config[element];
    } else {
      urlWithElement = this.config[dataList][element];
    }
    return urlWithElement;
  }

  getEndPoint(elementUrl: string, elementPath: string) {
    // const url = this.verifyUrl(this.config['API_URLS'][elementUrl]);
    const url = this.config['API_URLS'][elementUrl];
    // const path = this.verifyUrl(this.config['PATHS'][elementPath]);
    const path = this.config['PATHS'][elementPath];

    return url + '/' + path;
  }

  getUrl(element: string, dataList?: string) {
    if (!dataList) {
      const urlWithElement = this.config[element];
      // return this.verifyUrl(urlWithElement);
      return urlWithElement;
    } else {
      const urlWithDataList = this.config[dataList][element];
      // return this.verifyUrl(urlWithDataList);
      return urlWithDataList;
    }
  }

  verifyUrl(typeModel: any): string {
    if (typeModel) {
      if (typeModel.includes('/', typeModel.length - 1)) {
        const typeRelease = typeModel;
        return typeRelease;
      } else {
        const newType = typeModel + '/';
        return newType;
      }
    }
    return '/';
  }
}
