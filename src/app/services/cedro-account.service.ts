import { Signature } from '@model/signature.model';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs-compat';
import { ConfigService } from '@services/webFeeder/config.service';
import { HttpService } from '@services/http-service';
import { Http } from '@angular/http';
import { HttpClient } from 'selenium-webdriver/http';

@Injectable({
  providedIn: 'root'
})
export class CedroAccountService {

  private httpService: HttpService;
  constructor(
    private http: Http,
    private configService: ConfigService
  ) {
    this.httpService = new HttpService(this.http, configService, null); // "http://18.221.29.24:8010/");
    // this.httpService.setErrorSubscribe(() => { });
  }

  auth(login: string, password: string, client: boolean = true, withRefresh: boolean = true) {
    const obj = { user_name: login, password: password };
    let uri = '';
    if (client) {
      uri = 'auth/clients/login';
    } else {
      uri = 'auth/users/login';
    }
    return new Observable<any>(observer => {
      this.httpService.doPost(this.configService.CEDRO_ACCOUNT_API, uri, obj)
        .subscribe((dataAuth) => {
          if (dataAuth && 'access_token' in dataAuth) {
            if (withRefresh) {
              this.refreshOnLogin(login, password, client, dataAuth.refresh_token).subscribe(
                (dataRefresh) => {
                  dataAuth.access_token = dataRefresh.access_token;
                  dataAuth.refresh_token = dataRefresh.refresh_token;
                  dataAuth.scope = dataRefresh.scope;
                  dataAuth.token_type = dataRefresh.token_type;
                  dataAuth.expires_in = dataRefresh.expires_in;
                  // this.configService.writeLog(dataAuth);
                  observer.next(dataAuth);
                },
                (error) => { observer.error(error); }
              );
            } else {
              observer.next(dataAuth);
            }
          } else {
            observer.next(dataAuth);
          }
        },
          (error) => { observer.error(error); }
        );
    });
  }

  recoverPassword(usuario: string, client: boolean = true) {
    const url = (client ? 'clients/' : 'users/') + usuario + '/forgot_password';
    return this.httpService.doPost(this.configService.USER_IDENTITY_API + url, '', { load: 'true' });
  }

  private refreshOnLogin(login: string, password: string, client: boolean, refreshToken: string) {
    return new Observable<any>(observer => {
      this.refresh(refreshToken).subscribe(
        (dataRefresh) => {
          observer.next(dataRefresh);
        },
        (error) => {
          // REVOKE WHEN AUTHORIZATION PROBLEM
          if (error.error.error_description === 'Provided Authorization Grant is invalid') {
            this.revoke(refreshToken).subscribe(
              (revokeData) => {
                // TRY AUTH AGAIN
                this.auth(login, password, client, false).subscribe(
                  (dataAuth) => { observer.next(dataAuth); },
                  (errorAuth) => { observer.error(errorAuth); }
                );
              },
              (revokeError) => { observer.error(error); }
            );
          } else {
            observer.error(error);
          }
        }
      );
    });
  }

  refresh(refreshToken: string) {
    const obj = { token: refreshToken };
    const uri = 'auth/refresh_token';
    return this.httpService.doPost(this.configService.CEDRO_ACCOUNT_API, uri, obj);
  }

  revoke(refreshToken: string) {
    const obj = { token: refreshToken };
    const uri = 'auth/revoke_token';
    return this.httpService.doPost(this.configService.CEDRO_ACCOUNT_API, uri, obj);
  }

  resetSignature(signature: string, newSignature: string) {
    const obj = {
      user_client_id: this.configService.getHbAccountId(),
      signature: signature,
      new_signature: newSignature,
    };
    const url = `clients/reset_signature`;
    return this.httpService.doPost2(this.configService.USER_IDENTITY_API, url, JSON.stringify(obj), false)
      .map(res => {
        if (res) {
          if (res.status === 400 && res._body) {
            res.message = res._body;
            return res;
          }
          return res;
        }
        return [];
      });
  }

  validateSignature(params: Object) {
    const url = 'clients/validate_signature';
    return this.httpService.doPost2(this.configService.USER_IDENTITY_API, url, JSON.stringify(params), false)
      .map(res => {
        if (res) {
          return res;
        }
        return [];
      });
  }

  forgotSignature() {
    const clientId = this.configService.getHbAccountId();
    if (!clientId) {
      return [];
    }
    const url = 'clients/' + clientId + '/forgot_signature';
    return this.httpService.doPost2(this.configService.USER_IDENTITY_API, url, {}, false)
      .map(res => {
        if (res) {
          if (res.status === 400 && res._body) {
            res.message = res._body;
            return res;
          }
          return res;
        }
        return [];
      });
  }

  changePassword(currentPassword: string, newPassword: string, guid?: string) {
    const obj = { password: currentPassword, new_password: newPassword };
    const url = 'clients/' + (guid ? (guid + '/') : '') + 'reset_password';
    return this.httpService.doPost2(this.configService.USER_IDENTITY_API, url, obj, false)
      .map(res => {
        if (res) {
          if (res.status === 400 && res._body) {
            res.message = res._body;
            return res;
          }
          return res;
        }
        return [];
      });
  }

  advisorInfo(login?: string) {
    let url = 'users?';
    if (login) {
      url += `login=${login}`;
    }
    return this.httpService.doGetWso2(this.configService.USER_IDENTITY_API, url);
  }

  advisorChangePassword(currentPassword: string, newPassword: string, guid?: string, advisorId?: number) {
    const obj = { id: advisorId, password: currentPassword, new_password: newPassword };
    // tslint:disable-next-line:prefer-template
    const url = 'users/' + (guid ? (guid + '/reset_password') : 'change_password');
    return this.httpService.doPost2(this.configService.USER_IDENTITY_API, url, obj, false)
      .map(res => {
        if (res) {
          if (res.status === 422 && res._body) {
            res.message = JSON.parse(res._body);
            return res;
          } else if ((res.status === 400) && res._body) {
            res.message = res._body;
            return res;
          }
          return res;
        }
        return [];
      });
  }

}
