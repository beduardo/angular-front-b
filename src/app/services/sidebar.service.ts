import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SidebarService {
  private _status = new BehaviorSubject(true);
  public status$ = this._status.asObservable();

  public _clickOpenTabAuth = new BehaviorSubject<number>(0);
  clickOpenTabAuth$ = this._clickOpenTabAuth.asObservable();

  public _clickOpenTabLoginBroker = new BehaviorSubject<boolean>(false);
  clickOpenTabLoginBroker$ = this._clickOpenTabLoginBroker.asObservable();

  public _clickOpenTabAuthCallBack = new BehaviorSubject<number>(0);
  clickOpenTabAuthCallBack$ = this._clickOpenTabAuthCallBack.asObservable();

  private detachedWindows: any[] = [];

  openClose() {
    this._status.next(!this._status.value);
  }

  saveDetachedWindow(window) {
    this.detachedWindows.push(window);
  }

  closeDetachedWindows() {
    this.detachedWindows.forEach( window => {
      window.close();
    });
  }
}
