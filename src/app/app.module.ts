import { NgModule, LOCALE_ID, APP_INITIALIZER } from '@angular/core';
import { AppComponent } from './app.component';
import { LoginComponent } from '@business/login/login.component';
import { HttpClient } from '@angular/common/http';
import { SharedModule } from '@shared/shared.module';
import { NotificationsService, SimpleNotificationsModule } from 'angular2-notifications';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { CurrencyMaskModule } from 'ng2-currency-mask';
export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http);
}
import { registerLocaleData, DatePipe, APP_BASE_HREF } from '@angular/common';
import localePt from '@angular/common/locales/pt';
import { StyleguideComponent } from '@styleguide/styleguide.component';
import { CustomSnackbarComponent } from '@component/custom-snackbar/custom-snackbar.component';
import { CustomSnackbarService } from '@services/custom-snackbar-service.service';
import { AgGridModule } from 'ag-grid-angular';
registerLocaleData(localePt);
import { ConfigurationService } from '@services/configuration.service';
import { environment } from 'environments/environment';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CustomWorkspaceModalComponent } from '@component/custom-workspace-modal/custom-workspace-modal.component';
import { AppRoutingModule } from './app.routing.module';
import { RecoverPasswordComponent } from '@business/recover-password/recover-password.component';

export function ConfigLoader(configurationService: ConfigurationService) {
  return () => configurationService.load(environment.CONFIG_FILE);
}
registerLocaleData(localePt);
@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    StyleguideComponent,
    CustomSnackbarComponent,
    RecoverPasswordComponent,
  ],
  imports: [
    AppRoutingModule,
    BrowserAnimationsModule,
    SharedModule,
    SimpleNotificationsModule.forRoot(),
    NgbModule.forRoot(),
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    }),
    AgGridModule.withComponents([])
  ],
  entryComponents: [
    LoginComponent,
    CustomSnackbarComponent,
    RecoverPasswordComponent
  ],
  exports: [
    CustomWorkspaceModalComponent
  ],
  providers: [
    { provide: APP_BASE_HREF, useValue: environment.BASE_HREF},
    ConfigurationService,
    {
      provide: APP_INITIALIZER,
      useFactory: ConfigLoader,
      deps: [ConfigurationService],
      multi: true
    },
    NotificationsService,
    CustomSnackbarService,
    { provide: LOCALE_ID, useValue: 'pt-BR' },
    DatePipe
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
