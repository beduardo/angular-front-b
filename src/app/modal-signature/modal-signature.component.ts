import { ConfigService } from '@services/webFeeder/config.service';
import { Component, OnInit, Inject } from '@angular/core';
import { Constants } from '@utils/constants';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { NotificationsService } from 'angular2-notifications';
import { AuthenticationService } from '@services/authentication.service';
import { Signature } from '@model/signature.model';

@Component({
  selector: 'app-modal-signature',
  templateUrl: './modal-signature.component.html'
})
export class ModalSignatureComponent implements OnInit {

  public model: Signature = new Signature();

  public loadingforgot = false;
  public showConfirm = false;
  public showError = false;
  public helpPhone1: string;
  public helpEmail: string;

  public titleError = Constants.MSG01_FORGOT_SIGNATURE_TITLE_ERROR;
  public contentError = Constants.MSG01_FORGOT_SIGNATURE_CONTENT_ERROR;
  public titleConfirm = Constants.MSG01_FORGOT_SIGNATURE_TITLE_CONFIRM;
  public contentConfirm = Constants.MSG01_FORGOT_SIGNATURE_CONTENT_CONFIRM;
  public titleRecover = Constants.MSG01_FORGOT_SIGNATURE_TITLE;
  public contentRecover = Constants.MSG01_FORGOT_SIGNATURE_CONTENT;

  constructor(
    public dialogRef: MatDialogRef<ModalSignatureComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private _configService: ConfigService,
    public _notificationService: NotificationsService,
    private _authenticationService: AuthenticationService,
  ) {
    this.helpPhone1 = Constants.SIDEBAR_HELP_WHATSAPP;
    this.helpEmail = Constants.SIDEBAR_HELP_EMAIL;
    this.data.email = this._configService.getHbEmail();
  }

  ngOnInit() {
  }

  goBack() {
    this.showError = false;
    this.showConfirm = false;
  }

  closeClick(): void {
    this.dialogRef.close(false);
  }

  openWhatsappWindow() {
    window.open(Constants.SIDEBAR_HELP_WHATSAPP_LINK);
  }

  send() {
    this.loadingforgot = true;
    if (!this.data.email) {
      this._notificationService.warn('', Constants.MSG04_SIGNATURE_ERROR);
      this.loadingforgot = false;
      return;
    }
    this._authenticationService.forgotSignature(this.data.email)
      .subscribe(res => {
        if (res && (res.success === 'true' || res.success === true || res.data.status === 202)) {
          this.model = new Signature();
          this.showConfirm = true;
        } else {
          if (res && res.message) {
            this._notificationService.error('', res.message);
          } else {
            this._notificationService.error('', Constants.MSG_LOGIN_MSG003);
          }
          this.showError = true;
        }
        this.loadingforgot = false;
      }, error => {
        this._notificationService.error('', Constants.MSG_LOGIN_MSG003);
        this.loadingforgot = false;
        this.showError = true;
      });
  }

}
