import { ManualOnlineComponent } from './../../business/manual-online/manual-online.component';
import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';

import { UpdatePasswordComponent } from '@business/update-password/update-password.component';
import { MatDialog, MatSlideToggle } from '@angular/material';
import { AuthenticationService } from '@services/authentication.service';
import { LayoutService } from '@services/layout.service';
import { ConfigService } from '@services/webFeeder/config.service';
import { Constants } from '@utils/constants';
import { UpdateSignatureComponent } from '@business/update-signature/update-signature.component';
import { environment } from 'environments/environment';
import { NotificationsService } from 'angular2-notifications';


@Component({
  selector: 'app-user-settings',
  templateUrl: './user-settings.component.html',
})

export class UserSettingsComponent implements OnInit {

  public accountLogged: string;
  public darkThemeSelected = false;
  public toggleInfo = 'desativado';
  public dialog: MatDialog;
  public downloadUrl = Constants.LINK_DOWNLOAD_FTW;
  public imgUrl = Constants.IMG_NOTEBOOK_URL;
  public isWSO2: boolean;
  public userName: string;
  public isAssessor: boolean;
  public showHelp: boolean;
  public whitelabaleActive: number;
  @ViewChild('darkThemeToggle') toggler: MatSlideToggle;

  constructor(
    dialog: MatDialog,
    private authenticationService: AuthenticationService,
    private router: Router,
    private layoutService: LayoutService,
    private configService: ConfigService,
    private notificationService: NotificationsService
  ) {
    this.dialog = dialog;
    this.accountLogged = this.configService.getBrokerAccount();
    this.isWSO2 = this.configService.isLoginTypeWSO2();
    this.isAssessor = this.configService.isUserAssessorLogged();
    if (environment.WHITELABEL !== 1 && environment.WHITELABEL !== 4) {
      this.showHelp = true;
    }
    if (this.configService.isLoginTypeWSO2()) {
      this.userName = this.configService.getUserSystemLogged() ? this.configService.getUserSystemLogged().split(' ')[0].toUpperCase() : '';
    }
    this.whitelabaleActive = Number(this.configService.getWhiteLabel());
  }

  ngOnInit() {
    if (this.layoutService.getCurrentState() === true) {
      this.toggler.checked = true;
    }
    this.layoutService.isDark$.subscribe(isDark => {
      this.darkThemeSelected = isDark;
      if (this.darkThemeSelected) {
        this.toggleInfo = 'ativado';
      }
    });
  }

  changeTheme() {
    this.layoutService.switchThemes();
    if (this.darkThemeSelected) {
      this.toggleInfo = 'ativado';
      this.configService.setFavoriteThemeUser('true');
    } else {
      this.toggleInfo = 'desativado';
      this.configService.setFavoriteThemeUser('false');
    }
  }

  openDownloadWindow() {
    window.open(this.downloadUrl);
  }

  openUpdatePasswordModal() {
    this.dialog.closeAll();
    this.dialog.open(UpdatePasswordComponent, {
      panelClass: 'mat-modal-fullheight',
      position: { left: '0' },
      width: '555px'
    });
  }

  openManualOnline() {
    this.dialog.closeAll();
    this.dialog.open(ManualOnlineComponent, {
      position: {
        left: '186px',
        top: '94.3px'
      },
      height: 'calc(100% - 118.5px)',
      width: 'calc(100% - 209px)',
      maxWidth: 'unset',
      panelClass: 'panel-class-modal-news-component',
      disableClose: true
    });
  }

  openUpdateSignature() {
    this.dialog.closeAll();
    this.dialog.open(UpdateSignatureComponent, {
      panelClass: 'mat-modal-fullheight',
      position: { left: '0' },
      width: '555px'
    });
  }

  goTo(value) {
    window.location.href = value;
  }

  logout() {
    const hbLoginTypeEnable = this.configService.getHbLoginTypeEnable();
    if (hbLoginTypeEnable === Constants.LOGIN_S && !this.configService.getBrokerAccount()) {
      this.dialog.closeAll();
      this.authenticationService.logout();
      this.configService.deleteHbBrokerUser();
      this.configService.deleteFavoriteThemeUser();
      this.configService.deleteAllCookie();
      if (this.configService.isLoginTypeWSO2() && this.configService.isUserAssessorLogged()) {
        window.location.href = this.configService.REDIRECT_LOGOUT + '/assessor/login';
      } else if (this.configService.isLoginTypeWSO2() && !this.configService.isUserAssessorLogged()) {
        window.location.href = this.configService.REDIRECT_LOGOUT + '/client/login';
      } else {
        this.router.navigate(['/login']);
      }
    } else {
      this.authenticationService.brokerServiceLogout()
        .subscribe((data: any) => {
          if (this.configService.isLoginTypeWSO2()) {
            if (data) {
              this.dialog.closeAll();
              this.configService.deleteHbBrokerUser();
              this.configService.deleteFavoriteThemeUser();
              this.authenticationService.logout();
              this.configService._authHbLoginType.next(3);
              if (this.whitelabaleActive === 6) {
                this.router.navigate(['/login']);
              } else if (this.configService.isLoginTypeWSO2() && this.configService.isUserAssessorLogged()) {
                window.location.href = this.configService.REDIRECT_LOGOUT + '/assessor/login';
              } else if (this.configService.isLoginTypeWSO2() && !this.configService.isUserAssessorLogged()) {
                window.location.href = this.configService.REDIRECT_LOGOUT + '/client/login';
              } else {
                this.router.navigate(['/login']);
              }
            } else {
              this.configService._authHbLogged.next(false);
              this.notificationService.error('', Constants.BROKER_MSG002);
            }
          } else {
            if (data && data.isAuthenticated === 'N') {
              if (hbLoginTypeEnable === Constants.LOGIN_O ||
                (hbLoginTypeEnable === Constants.LOGIN_S && this.configService.getBrokerAccount())) {
                this.dialog.closeAll();
                this.authenticationService.logout();
                this.configService.deleteHbBrokerUser();
                this.configService.deleteFavoriteThemeUser();
                if (this.configService.isLoginTypeWSO2() && this.configService.isUserAssessorLogged()) {
                  window.location.href = this.configService.REDIRECT_LOGOUT + '/assessor/login';
                } else if (this.configService.isLoginTypeWSO2() && !this.configService.isUserAssessorLogged()) {
                  window.location.href = this.configService.REDIRECT_LOGOUT + '/client/login';
                } else {
                  this.router.navigate(['/login']);
                }
              }
              this.configService._authHbLoginType.next(3);
            } else {
              this.configService._authHbLogged.next(false);
              this.notificationService.error('', Constants.BROKER_MSG002);
              this.dialog.closeAll();
              this.authenticationService.logout();
              this.configService.deleteHbBrokerUser();
              this.configService.deleteFavoriteThemeUser();
              if (this.configService.isLoginTypeWSO2() && this.configService.isUserAssessorLogged()) {
                window.location.href = this.configService.REDIRECT_LOGOUT + '/assessor/login';
              } else if (this.configService.isLoginTypeWSO2() && !this.configService.isUserAssessorLogged()) {
                window.location.href = this.configService.REDIRECT_LOGOUT + '/client/login';
              } else {
                this.router.navigate(['/login']);
              }
            }
          }
        },
          error => {
            this.configService._authHbLogged.next(false);
            this.notificationService.error('', Constants.BROKER_MSG002);
            this.authenticationService.logout();
            this.configService.deleteHbBrokerUser();
            this.configService.deleteFavoriteThemeUser();
            if (this.configService.isLoginTypeWSO2() && this.configService.isUserAssessorLogged()) {
              window.location.href = this.configService.REDIRECT_LOGOUT + '/assessor/login';
            } else if (this.configService.isLoginTypeWSO2() && !this.configService.isUserAssessorLogged()) {
              window.location.href = this.configService.REDIRECT_LOGOUT + '/client/login';
            } else {
              this.router.navigate(['/login']);
            }
          });
    }
  }

}
