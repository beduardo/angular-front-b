import { Component, EventEmitter, forwardRef, Input, OnInit, Output } from '@angular/core';
import { ControlValueAccessor, FormControl, NG_VALUE_ACCESSOR, Validators } from '@angular/forms';
import { AppUtils } from '@utils/app.utils';
import { isNumber } from 'util';


const noop = () => {
};

@Component({
  selector: 'app-incremental-input',
  templateUrl: './incremental-input.component.html',
  providers:
    [
      {
        provide: NG_VALUE_ACCESSOR,
        useExisting: forwardRef(() => IncrementalInputComponent),
        multi: true
      }
    ]
})

export class IncrementalInputComponent implements OnInit, ControlValueAccessor {

  @Input() invalid = true;

  @Input() tickSize = 1;
  @Input() maxlength = 11;
  @Input() decimal = 0;
  @Input() step = 0;
  @Input() isQuantity: boolean;
  @Input() price: boolean;
  @Input() themeColor: string;
  @Input() labelText: string;
  @Input() editable = true;
  @Input() isDisabled: boolean;
  @Input() inputWidth: string;
  @Input() roundLot: any;
  @Input() minValue;
  @Input() maxValue;
  @Input() placeholder;
  @Input() formControl;
  @Input() options = { prefix: '', thousands: '.', decimal: ',', allowNegative: true, precision: 0, align: 'right' };
  @Output() valueChanged: EventEmitter<number>;
  public modelValue: any;
  private innerValue: any = '';
  public isFocused: boolean;
  private numberPattern = /^\d+$/;
  private onTouchedCallback: () => void = noop;
  private onChangeCallback: (_: any) => void = noop;
  private flag = 0;

  formValidator = new FormControl('', [Validators.required]);
  constructor() {
    this.modelValue = 0.00;
    this.valueChanged = new EventEmitter();
    this.roundLot = 1;
    this.isQuantity = false;
    this.isDisabled = false;
  }

  ngOnInit() {
    this.isFocused = false;
    this.labelText = this.placeholder;
  }

  onScroll(event: WheelEvent) {
    if (this.isFocused) {
      event.preventDefault();
      if (event.deltaY < 0) {
        this.increment();
      } else if (event.deltaY > 0) {
        this.decrement();
      }
    }
  }

  keyPressed(event: any) {
    const inputChar = String.fromCharCode(event.charCode);

    this.flag++;
    if (this.flag > 8) {
      event.preventDefault();
      this.value = 999999;
    }
    if (!this.numberPattern.test(inputChar)) {
      // invalid character, prevent input
      event.preventDefault();
    }
  }

  keyUpped() {
    this.flag = 0;
  }

  onPaste(event) {
    this.keyPressed(event);
    const content = event.clipboardData.getData('text/plain');

    if (!this.numberPattern.test(content)) {
      event.preventDefault();
    }

  }

  onFocus() {
    this.isFocused = true;
  }

  onBlur() {
    this.isFocused = false;
  }

  setLabelActive() {
    this.isFocused = true;
  }

  setLabelInactive() {
    this.isFocused = false;
  }

  increment() {
    this.value = parseFloat(this.value);
    const precision = this.options.precision;
    if (this.isQuantity && (this.value < this.maxValue)) {
      if (this.price) {
        this.rulesValueChange(true);
      } else {
        this.value = (Number(this.value) + Number(this.roundLot)).toFixed(precision);
      }
    } else if (!this.isQuantity && (this.value < this.maxValue) && !this.price) {
      this.value = (Number(this.value) + Number(this.step)).toFixed(precision);
    }

    if (!this.value && this.value !== '0' && this.value !== 0 && !this.price) {
      this.value = '0';
    }
    this.valueChanged.emit(this.value);
  }

  decrement() {
    this.value = parseFloat(this.value);
    const precision = this.options.precision;
    if (this.minValue) {
      if (this.isQuantity) {
        if (this.value - this.roundLot >= this.minValue) {
          if (this.price) {
            this.rulesValueChange(false);
          } else {
            this.value = Number(this.value - this.roundLot).toFixed(precision);
          }
        } else {
          this.value = 0;
        }
      } else {
        const step = AppUtils.ticksizeToStep(this.options.precision);
        if (this.value - this.step >= this.minValue) {
          this.value = Number(this.value - this.step).toFixed(precision);
        }
      }
      this.valueChanged.emit(this.value);
    } else {
      if (this.isQuantity) {
        if (this.price) {
          this.rulesValueChange(false);
        } else {
          this.value = Number(this.value - this.roundLot).toFixed(precision);
        }
      } else {
        const step = AppUtils.ticksizeToStep(this.options.precision);
        this.value = Number(this.value - this.step).toFixed(precision);
      }
      this.valueChanged.emit(this.value);
    }
  }

  rulesValueChange(increment) {
    const qtdDecimals = this.roundLot.split('.').length > 1 ? this.roundLot.split('.')[1].length : 0;
    const lot = +this.roundLot;
    this.value = Number(this.value.toFixed(qtdDecimals));
    let value = this.value;
    if (lot == 0) return;
    const division = Number((value / lot).toFixed(qtdDecimals));
    const divisionRound = increment ? Math.round(division + .5) : Math.round(division - .5);
    if (division < divisionRound) {
      value = increment ? divisionRound * lot : (divisionRound - 1) * lot;
    } else if (division > divisionRound) {
      value = increment ? (divisionRound + 1) * lot : divisionRound * lot;
    } else {
      value = increment ? (division + 1) * lot : (division - 1) * lot;
    }
    if (this.value !== value) {
      this.value = value;
    } else {
      this.value = increment ? (value + lot) : (value - lot);
    }
  }


  get value(): any {
    return this.innerValue;
  }

  set value(v: any) {
    if (v !== this.innerValue) {
      this.innerValue = v;
      this.onChangeCallback(v);
    }
  }

  writeValue(value: any) {
    if (value == null && this.minValue != null || value == '' || value < this.minValue) {
      this.innerValue = this.minValue;
    } else {
      if (value !== this.innerValue) {
        this.innerValue = value;
      }
    }
  }

  propagateChange = (_: any) => { };
  registerOnChange(fn: any) {
    this.onChangeCallback = fn;
  }
  registerOnTouched(fn: any) {
    this.onTouchedCallback = fn;
  }
}
