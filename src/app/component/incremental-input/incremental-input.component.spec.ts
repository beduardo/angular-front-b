import { AppModule } from '../../app.module';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IncrementalInputComponent } from '@component/incremental-input/incremental-input.component';
import { APP_BASE_HREF } from '@angular/common';

describe('IncrementalInputComponent', () => {
  let component: IncrementalInputComponent;
  let fixture: ComponentFixture<IncrementalInputComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [],
      imports: [AppModule],
      providers: [{ provide: APP_BASE_HREF, useValue: '/' }]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IncrementalInputComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
