import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FullheightModalComponent } from '@component/fullheight-modal/fullheight-modal.component';

describe('FullscreenModalComponent', () => {
  let component: FullheightModalComponent;
  let fixture: ComponentFixture<FullheightModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FullheightModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FullheightModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
