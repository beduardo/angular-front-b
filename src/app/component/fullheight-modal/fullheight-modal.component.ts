import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material';

@Component({
  selector: 'app-fullheight-modal',
  templateUrl: './fullheight-modal.component.html',
})
export class FullheightModalComponent implements OnInit {

  constructor(public dialogRef: MatDialogRef<FullheightModalComponent>) { }

  closeClick(): void {
    this.dialogRef.close();
  }

  ngOnInit() {
  }
}
