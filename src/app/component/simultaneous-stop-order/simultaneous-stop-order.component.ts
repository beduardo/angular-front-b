import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';

import { MessageQuote } from '@model/webFeeder/message-quote.model';
import { SimultaneousStopOrder } from '@model/simultaneous-stop-order.model';
import { QuoteService } from '@services/webFeeder/sckt-quotes.service';
import { Constants } from '@utils/constants';

@Component({
  selector: 'app-simultaneous-stop-order',
  templateUrl: './simultaneous-stop-order.component.html'
})
export class SimultaneousStopOrderComponent implements OnInit, OnDestroy {

  @Input() symbol: any;
  @Input() type: any;

  private subscriptionQuote: Subscription;
  public flag = true;
  public currentSymbol;
  public _messageQuote = new MessageQuote();
  public standardQuantity: number;
  public model = new SimultaneousStopOrder();
  public durationOptions = Constants.TIME_IN_FORCES;

  public priceLimitValuesFormatOptions = {
    prefix: 'R$ ',
    thousands: '.',
    decimal: '.',
    allowNegative: false,
    precision: 2
  };

  constructor(private _quoteService: QuoteService) { }

  ngOnInit() {
    this.model.type = this.type;
    if (this.symbol) {
      this.selectAsset(this.symbol);
    }
  }

  ngOnDestroy() {
    this._quoteService.unsubscribeQuote(this.symbol, Constants.WINDOW_NAME_SIMULATANEOS_STOP_ORDER);
    if (this.subscriptionQuote) {
      this.subscriptionQuote.unsubscribe();
    }
  }

  parseFloat(value) {
    return parseFloat(value);
  }

  selectAsset(value: any) {

    // TODO this.model.quantitySet = false; If you want to reset the default value every time you switch assets
    if (value) {
      this._quoteService.unsubscribeQuote(this.symbol, Constants.WINDOW_NAME_SIMULATANEOS_STOP_ORDER);
      if (this.subscriptionQuote) {
        this.subscriptionQuote.unsubscribe();
      }
      if (value.symbol) {
        value = value.symbol;
      }
      if (value.symbol !== '') {
      }
      this._quoteService.subscribeQuote(value.toLocaleLowerCase(), Constants.WINDOW_NAME_SIMULATANEOS_STOP_ORDER);
      this.subscriptionQuote = this._quoteService.messagesQuote.subscribe(
        data => {
          if (
            data !== undefined &&
            data.symbol === value.toLocaleLowerCase()
          ) {
            this._messageQuote = data;
            this.model.symbol = this._messageQuote.symbol;
            if (!this.model.quantitySet) {
              this.model.quantity = this._messageQuote.loteDefault;
              this.model.quantitySet = true;
            }
            this.model.lastTrade = this._messageQuote.lastTrade;
            this.model.tunnelUpperLimit = this._messageQuote.tunnelUpperLimit;
            this.model.tunnelLowerLimit = this._messageQuote.tunnelLowerLimit;
            this.currentSymbol = value;
          }
        }
      );
    }
  }
  toggleBtn() {
    this.flag = !this.flag;
  }
}
