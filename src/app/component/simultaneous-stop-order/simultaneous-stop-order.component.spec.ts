import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SimultaneousStopOrderComponent } from '@component/simultaneous-stop-order/simultaneous-stop-order.component';

describe('SimultaneousStopOrderComponent', () => {
  let component: SimultaneousStopOrderComponent;
  let fixture: ComponentFixture<SimultaneousStopOrderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SimultaneousStopOrderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SimultaneousStopOrderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
