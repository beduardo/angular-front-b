import { AuthServer } from '@services/auth-server.service';
import { ChangeDetectorRef, Component, EventEmitter, Input, OnInit, Output, OnDestroy } from '@angular/core';
import { MatDialog } from '@angular/material';
import { Router } from '@angular/router';
import { AuthService } from '@auth/auth.service';
import { NormalModalComponent } from '@component/normal-modal/normal-modal.component';
import { EditOrderModel } from '@model/editOrder.model';
import { TypeOrderEnum } from '@model/enum/type-order.enum';
import { Quote } from '@model/webFeeder/quote.model';
import { ConfigService } from '@services/webFeeder/config.service';
import { QuoteService } from '@services/webFeeder/sckt-quotes.service';
import { Constants } from '@utils/constants';
import { NotificationsService } from 'angular2-notifications';
import { MessageQuote } from '@model/webFeeder/message-quote.model';
import { Subscription } from 'rxjs';
import { AbstractItem } from '@business/base/abstract-item';
import { WorkspaceNewService } from '@services/workspace-new.service';
import { AbstractItemService } from '@business/base/abstract-item.service';
import { WorkspaceNewDto } from '@dto/workspacenew.dto';

@Component({
  selector: 'app-edit-order',
  templateUrl: './edit-order.component.html',

})
export class EditOrderComponent implements OnInit, OnDestroy {

  @Input() dataOrder: any;
  @Output() close = new EventEmitter<any>();

  private idEdit: number;
  @Output()
  editOpenChange = new EventEmitter<any>();

  @Input()
  get editOpen() {
    return this.idEdit;
  }
  set editOpen(value) {
    this.idEdit = value;
    this.editOpenChange.emit(this.idEdit);
  }

  public stopTriggerLoss = true;
  public stopLimitLoss = true;
  public stopTriggerGain = true;
  public stopLimitGain = true;

  private _subscription: Subscription;
  public data;
  public errors = '';
  public loading = false;
  public loadingEditOrder = false;
  public indiceFuture = false;
  public model: Quote;
  public dialog: MatDialog;
  public minDate: Date = new Date();
  public minDate2: Date = new Date((new Date()).getFullYear(), (new Date()).getMonth(), (new Date()).getDate());
  public order: EditOrderModel = new EditOrderModel();
  public timeInForcesOptions = Constants.TIME_IN_FORCES;
  public hideEdits;
  public showResult = -1;
  public stopType: string;
  public lastTrade;
  public minimumIncrement = '0.01';
  public tickSize = 2;
  public typesOptions = [
    { id: TypeOrderEnum.LIMITED, text: Constants.TYPE_ORDER_TEXTS.Limited },
    { id: TypeOrderEnum.MARKETPROTECTED, text: Constants.TYPE_ORDER_TEXTS.MarketProtected },
    { id: TypeOrderEnum.MARKET, text: Constants.TYPE_ORDER_TEXTS.Market },
  ];

  public priceLimitValuesFormatOptions = {
    prefix: Constants.CURRENCY_BRAZILIAN_SYMBOL,
    thousands: Constants.THOUSANDS_SEPARATOR,
    decimal: Constants.DECIMALS_SEPARATOR,
    allowNegative: false,
    precision: Constants.BILLET_TICKSIZE_DEFAULT,
    align: 'right',
  };

  public priceLimitValuesFormatOptions2 = {
    prefix: '',
    thousands: Constants.THOUSANDS_SEPARATOR,
    decimal: Constants.DECIMALS_SEPARATOR,
    allowNegative: false,
    precision: 0,
    align: 'right'
  };

  public priceLimitValuesFormatOptions3 = {
    prefix: '',
    thousands: Constants.THOUSANDS_SEPARATOR,
    decimal: Constants.DECIMALS_SEPARATOR,
    allowNegative: false,
    precision: 0,
    align: 'right'
  };

  public priceLimitValuesFormatOptions4 = {
    prefix: '',
    thousands: Constants.THOUSANDS_SEPARATOR,
    decimal: Constants.DECIMALS_SEPARATOR,
    allowNegative: false,
    precision: 0,
    align: 'right'
  };

  public confirmModalOpened = false;

  constructor(
    private cdRef: ChangeDetectorRef,
    private _authService: AuthService,
    private _configService: ConfigService,
    private _quoteService: QuoteService,
    private _router: Router,
    private workspaceService: WorkspaceNewService,
    private _notificationService: NotificationsService,
    dialog: MatDialog,
    private _authServer: AuthServer,
  ) {
    this.dialog = dialog;
  }

  ngOnInit() {
    this.loading = true;
    this.data = this.dataOrder;
    this.order = this.transformOrder(this.data);
    if (this.order.typeCode === '4') {
      this.stopType = this.getStopType();
    }

    for (let i = 0; i < this.typesOptions.length; i++) {
      if (this.order.type.toString() === this.typesOptions[i].text.toString()) {
        this.order.type = this.typesOptions[i].id;
      }
      if (this.order.timeInForce.toString() === this.timeInForcesOptions[i].desc.toString()) {
        this.order.timeInForce = this.timeInForcesOptions[i].webfeeder;
      }
    }

    if (this.data.quote.toLowerCase().indexOf('win') === 0 || this.data.quote.toLowerCase().indexOf('wdo') === 0 ||
      this.data.quote.toLowerCase().indexOf('dol') === 0 || this.data.quote.toLowerCase().indexOf('ind') === 0) {
      this.indiceFuture = true;
    } else {
      this.indiceFuture = false;
    }

    this._authServer.iQuote.getQuote(this.data.quote)
      .subscribe((x: Quote) => {
        this.order.loteDefault = x.loteDefault;
        this.lastTrade = x.lastTrade;
        this.model = x;

        if (x.tickSize !== undefined && x.tickSize !== null) {
          this.tickSize = x.tickSize;
        }

        if (x.minIntervalIncrPrice !== undefined && x.minIntervalIncrPrice !== null) {
          this.minimumIncrement = `${x.minIntervalIncrPrice}`;
        }

        if (this.indiceFuture) {
          this.priceLimitValuesFormatOptions4.precision = this.tickSize;
        } else {
          this.priceLimitValuesFormatOptions.precision = this.tickSize;
        }
        this.loading = false;
      });

    this._quoteService.subscribeQuote(this.data.quote, Constants.WINDOW_NAME_EDIT_ORDER);
    this._subscription = this._quoteService.messagesQuote
      .subscribe((msg) => {
        if (msg && msg.errorMessage && msg.symbol.toLowerCase() === this.data.quote.toLowerCase()) {
          this._quoteService.unsubscribeQuote(msg.symbol.toLowerCase(), Constants.WINDOW_NAME_EDIT_ORDER);
        } else if (msg !== undefined && msg.symbol != null && msg.symbol.toLowerCase() === this.data.quote.toLowerCase()) {
          let last = msg.lastTrade + '';
          last = last.replace('.', '').replace(',', '.');
          this.lastTrade = last;
          if (msg.minimumIncrement === undefined || !msg.minimumIncrement) {
            this.minimumIncrement = '0.01';
          } else {
            this.minimumIncrement = msg.minimumIncrement.toString().replace(',', '.');
          }
        }
      });
  }

  verifyPrice() {
    if (!this.order.type.includes(this.typesOptions[0].id)) {
      return false;
    } else {
      return true;
    }
  }

  verifyType() {
    if (!this.order.type.includes(TypeOrderEnum.LIMITED)) {
      this.order.price = this.lastTrade;
      return false;
    }
  }

  public verifyQuantity() {
    // this.order.total = this.order.price * this.order.qtd; TODO remove this function, i'm using it for now.
    this.stopOrderValidator();
  }

  public resetDate() {
    // this.order.validityOrder = new Date();
  }

  public callClose() {
    this.editOpen = -1;
    this.close.emit(true);
  }

  stopOrderValidator() {
    const stopGain = (this.stopType === 'stopGain' || this.stopType === 'stopGainStopLoss');
    const stopLoss = (this.stopType === 'stopLoss' || this.stopType === 'stopGainStopLoss');

    // BUY
    if (this.order.side === Constants.ORDER_SIDE_BUY) {
      if (stopGain) {
        if (Number(this.order.priceStopGain) > this.lastTrade) {
          this.stopTriggerGain = false;
        } else {
          this.stopTriggerGain = true;
        }
        if (Number(this.order.price2) < Number(this.order.priceStopGain)) {
          this.stopLimitGain = false;
        } else {
          this.stopLimitGain = true;
        }
      }

      if (stopLoss) {
        if (Number(this.order.priceStop) < this.lastTrade) {
          this.stopTriggerLoss = false;
        } else {
          this.stopTriggerLoss = true;
        }

        if (Number(this.order.price) < Number(this.order.priceStop)) {
          this.stopLimitLoss = false;
        } else {
          this.stopLimitLoss = true;
        }
      }
      return;
      // SELL
    } else if (this.order.side === Constants.ORDER_SIDE_SELL) {
      if (stopGain) {
        if (Number(this.order.priceStopGain) < this.lastTrade) {
          this.stopTriggerGain = false;
        } else {
          this.stopTriggerGain = true;
        }
        if (Number(this.order.price2) > Number(this.order.priceStopGain)) {
          this.stopLimitGain = false;
        } else {
          this.stopLimitGain = true;
        }
      }

      if (stopLoss) {
        if (Number(this.order.priceStop) > this.lastTrade) {
          this.stopTriggerLoss = false;
        } else {
          this.stopTriggerLoss = true;
        }

        if (Number(this.order.price) > Number(this.order.priceStop)) {
          this.stopLimitLoss = false;
        } else {
          this.stopLimitLoss = true;
        }
      }
      return;
    }
  }


  public transformOrder(data: any): any {
    try {
      const messageReturn: EditOrderModel = new EditOrderModel;
      messageReturn.account = data.account;
      messageReturn.quote = data.quote;
      messageReturn.qtd = data.quantitySupplied;
      messageReturn.market = Constants.ORDER_HISTORY_MARKETS.find(x => x.desc === data.mercado).cod;
      messageReturn.type = data.type;
      messageReturn.typeCode = data.typeCode;
      messageReturn.side = Constants.ORDER_HISTORY_DIRECTIONS.find(x => x.desc === data.side).webfeeder;
      const day = data.transactTime.toString().substring(0, 2);
      const month = data.transactTime.toString().substring(3, 5);
      const year = data.transactTime.toString().substring(6, 10);
      if (data.maturity) {
        const auxDate = data.maturity.substring(0, 10).split('/');
        messageReturn.validityOrder = new Date(`${auxDate[2]}/${auxDate[1]}/${auxDate[0]}`);
      } else {
        messageReturn.validityOrder = new Date(year, month - 1, day);
      }
      messageReturn.orderStrategy = data.orderStrategy;
      messageReturn.timeInForce = data.timeInForce;
      messageReturn.orderID = data.orderID;
      messageReturn.origclordid = data.origclordid || '';
      messageReturn.clOrdID = data.clOrdID;
      if (data.priceStopGain === '') {
        messageReturn.priceStopGain = data.priceStopGain;
      } else {
        messageReturn.priceStopGain = parseFloat(data.priceStopGain.replace('R$ ', ''));
      }
      if (!data.maxFloor || data.maxFloor === '') {
        messageReturn.maxFloor = 0;
      } else {
        messageReturn.maxFloor = parseFloat(data.maxFloor.replace('.', ''));
      }
      if (data.price === '') {
        messageReturn.price = data.price;
      } else {
        messageReturn.price = parseFloat(data.price.replace(',', '.'));
      }
      if (data.price2 === '') {
        messageReturn.price2 = data.price2;
      } else {
        messageReturn.price2 = parseFloat(data.price2.replace('R$ ', ''));
      }
      if (data.priceStop === '') {
        messageReturn.priceStop = data.priceStop;
      } else {
        messageReturn.priceStop = parseFloat(data.priceStop.replace('R$ ', ''));
      }
      return messageReturn;
    } catch (error) {
    }
  }

  public alterResult(value) {
    // this.showResult = value;
    // this.hideEdits = true;
    // window.setTimeout(function() {this.callClose()}.bind(this), 2000); TODO: UX discussion, will implement on future release
    this.callClose();
  }

  confirmationEditModal() {
    this.confirmModalOpened = true;
    if (this.order.qtd === 0) {
      this._notificationService.warn('', Constants.ORDER_HISTORY_MSG001);
    } else if (this.order.loteDefault && this.order.qtd % this.order.loteDefault !== 0) {
      this._notificationService.warn('', `Valor deve ser múltiplo do lote default`); // TODO waiting for documentation
    } else {
      let isLoginClientWSO2 = false;
      if (this._configService.isLoginTypeWSO2() && !this._configService.isUserAssessorLogged()) {
        isLoginClientWSO2 = true;
      }
      const modalOrders = this.dialog.open(NormalModalComponent, {
        data: {
          title: '',
          body: Constants.ORDER_HISTORY_MSG003,
          isHistoryOrderWSO2: isLoginClientWSO2
        },
        panelClass: 'mat-modal-normal',
      });
      modalOrders.afterClosed()
        .subscribe(result => {
          if (result) {
            this.loadingEditOrder = true;
            if (this.order.quote === null) {
              this._notificationService.warn('OPS', Constants.WORKSPACE_MSG005);
              this.loadingEditOrder = false;
            } else if (this.order.typeCode !== '4') {
              if (this._configService.isLoginTypeWSO2()) {
                this._authServer.iNegotiation.sendOrderWso2(this.order.account, this.order.quote, 0, this.order.qtd,
                  this.order.price.toString(), this.order.market, this.order.type, this.order.side,
                  this.order.validityOrder, this.order.orderStrategy, this.order.timeInForce,
                  this.order.orderID, this.order.clOrdID,
                  this.order.loteDefault, this.order.maxFloor, null, null, null, null, null, null, false)
                  .subscribe(res => {
                    if (res.code === 1) {
                      this._notificationService.alert('', 'Ordem recebida com sucesso!');
                      this.alterResult(0);
                      this.loadingEditOrder = false;
                    } else {
                      this._notificationService.error('OPS', 'Algum erro ocorreu!');
                      this.alterResult(1);
                      this.loadingEditOrder = false;
                    }
                  },
                    error => {
                      this.errors = error;
                    });
              } else {
                if (!this.validateOrderStop()) {
                  this.loadingEditOrder = false;
                  return;
                }
                this._authServer.iNegotiation.sendOrderApi(this.order.account, this.order.quote, this.order.qtd,
                  this.order.price.toString(), this.order.market, this.order.type, this.order.side,
                  this.order.validityOrder, this.order.orderStrategy, this.order.timeInForce,
                  this.order.orderID, this.order.clOrdID,
                  this.order.loteDefault, this.order.maxFloor && this.order.maxFloor !== '-' ? this.order.maxFloor : null)
                  .subscribe(res => {
                    if (res.code === 1) {
                      this._notificationService.alert('', 'Ordem recebida com sucesso!');
                      this.alterResult(0);
                      this.loadingEditOrder = false;
                    } else {
                      this._notificationService.error('OPS', 'Algum erro ocorreu!');
                      this.alterResult(1);
                      this.loadingEditOrder = false;
                    }
                  },
                    error => {
                      this.errors = error;
                    });
              }
            } else {
              if (this._configService.isLoginTypeWSO2()) {
                this._authServer.iNegotiation.sendOrderWso2(this.order.account, this.order.quote, 0, this.order.qtd,
                  null, this.order.market, this.order.type, this.order.side, this.order.validityOrder, this.order.orderStrategy,
                  this.order.timeInForce, this.order.orderID, this.order.origclordid, this.order.loteDefault, null, null, null,
                  this.order.priceStopGain, this.order.price2, this.order.priceStop, this.order.price, true)
                  .subscribe(res => {
                    if (res.code === 1) {
                      this._notificationService.alert('', 'Ordem recebida com sucesso!');
                      this.alterResult(0);
                    } else {
                      this._notificationService.error('OPS', 'Algum erro ocorreu!');
                      this.alterResult(1);
                    }
                  },
                    error => {
                      this.errors = error;
                    });
              } else {
                if (!this.validateOrderStop()) {
                  this.loadingEditOrder = false;
                  return;
                }
                this._authServer.iNegotiation.sendOrderStopApi(this.order.account, this.order.validityOrder, this.order.orderStrategy,
                  this.order.quote, this.order.qtd, this.order.priceStopGain, this.order.price2,
                  this.order.priceStop, this.order.price, this.order.market,
                  this.order.side, this.order.timeInForce, this.order.orderID, this.order.clOrdID,
                  this.order.loteDefault, this.order.type.toUpperCase() !== 'STOP',
                  this.order.maxFloor && this.order.maxFloor !== '-' ? this.order.maxFloor : null)
                  .subscribe(res => {
                    if (res.code === 1) {
                      this._notificationService.alert('', 'Ordem recebida com sucesso!');
                      this.alterResult(0);
                      this.loadingEditOrder = false;
                    } else {
                      this._notificationService.error('OPS', 'Algum erro ocorreu!');
                      this.alterResult(1);
                      this.loadingEditOrder = false;
                    }
                  },
                    error => {
                      this.errors = error;
                    });
              }
            }
          } else {

          }

        });
    }
  }

  getStopType() {
    if (this.order.price === '' && this.order.priceStop === '') {
      return 'stopGain';
    }

    if (this.order.price2 === '' && this.order.priceStopGain === '') {
      return 'stopLoss';
    }

    if (this.order.price !== '' && this.order.priceStop !== '' && this.order.price2 !== '' && this.order.priceStopGain !== '') {
      return 'stopGainStopLoss';
    }

    return '';
  }

  validateOrderStop() {
    const stopGain = (this.stopType === 'stopGain' || this.stopType === 'stopGainStopLoss');
    const stopLoss = (this.stopType === 'stopLoss' || this.stopType === 'stopGainStopLoss');

    // BUY
    if (this.order.side === Constants.ORDER_SIDE_BUY) {
      if (stopGain) {
        if (Number(this.order.priceStopGain) > this.lastTrade) {
          this._notificationService.warn('', Constants.BILLET_ORDER_2_TRIGGER_BUY);
          return false;
        }
        if (Number(this.order.price2) < Number(this.order.priceStopGain)) {
          this._notificationService.warn('', Constants.BILLET_ORDER_2_LIMIT_BUY);
          return false;
        }
      }

      if (stopLoss) {
        if (Number(this.order.priceStop) < this.lastTrade) {
          this._notificationService.warn('', Constants.BILLET_ORDER_1_TRIGGER_BUY);
          return false;
        }

        if (Number(this.order.price) < Number(this.order.priceStop)) {
          this._notificationService.warn('', Constants.BILLET_ORDER_1_LIMIT_BUY);
          return false;
        }
      }
      return true;
      // SELL
    } else if (this.order.side === Constants.ORDER_SIDE_SELL) {
      if (stopGain) {
        if (Number(this.order.priceStopGain) < this.lastTrade) {
          this._notificationService.warn('', Constants.BILLET_ORDER_2_TRIGGER_SELL);
          return false;
        }
        if (Number(this.order.price2) > Number(this.order.priceStopGain)) {
          this._notificationService.warn('', Constants.BILLET_ORDER_2_LIMIT_SELL);
          return false;
        }
      }

      if (stopLoss) {
        if (Number(this.order.priceStop) > this.lastTrade) {
          this._notificationService.warn('', Constants.BILLET_ORDER_1_TRIGGER_SELL);
          return false;
        }

        if (Number(this.order.price) > Number(this.order.priceStop)) {
          this._notificationService.warn('', Constants.BILLET_ORDER_1_LIMIT_SELL);
          return false;
        }
      }
      return true;
    }
  }

  ngOnDestroy() {
    if (this._subscription) {
      const userWorkspaceSelected: WorkspaceNewDto = this.workspaceService.getUserWorkspaceById(this.workspaceService.getWksSelectedId());
      const userWorkspaces = this.workspaceService.getUserWorkspaces();
      let isExist = false;

      for (let i = 0; i < userWorkspaces.length; i++) {
        if (userWorkspaces[i].Id !== undefined && userWorkspaces[i].Id === userWorkspaceSelected.Id) {
          const workspace = JSON.parse(userWorkspaces[i].JsonData);
          for (let j = 0; j < workspace.content.length; j++) {
            if (workspace.content[j].symbol.toLowerCase() === this.order.quote.toLowerCase()) {
              isExist = true;
              break;
            }
          }
        }
      }

      this._subscription.unsubscribe();
      if (isExist) {
        return;
      } else {
        this._quoteService.unsubscribeQuote(this.order.quote, Constants.WINDOW_NAME_EDIT_ORDER);
      }
    }

    this.editOpen = -1;
  }
}






