import { Constants, Profile } from '@utils/constants';
import { Component, OnInit, Inject, ViewChild, Renderer2 } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Router } from '@angular/router';
import { NgForm } from '@angular/forms';
import { AuthenticationService } from '@services/authentication.service';
import { NotificationsService } from 'angular2-notifications';
import { LayoutService } from '@services/layout.service';
import { ConfigService } from '@services/webFeeder/config.service';
import { environment } from 'environments/environment';

@Component({
  selector: 'app-modal-password',
  templateUrl: './modal-password.component.html'
})
export class ModalPasswordComponent implements OnInit {

  public loading: boolean;
  public title = 'Redefinir senha';
  public user: string;
  public showConfirm: boolean;
  public showError: boolean;
  public isUserLogged: boolean;
  public isLoginWSO2: boolean;
  public loginNoAsterisks = '';
  public placeHolder = '';
  public optinsRecovever: any = {};

  @ViewChild('loginForm') loginForm: NgForm;

  constructor(
    public dialogRef: MatDialogRef<ModalPasswordComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private _authenticationService: AuthenticationService,
    public _NotificationService: NotificationsService,
    private renderer: Renderer2,
    private _configService: ConfigService
  ) {
    this.showConfirm = false;
    this.isLoginWSO2 = this._configService.isLoginTypeWSO2();
    if (this.isLoginWSO2) {
      this.placeHolder = 'Usuário (CPF)';
    } else {
      this.placeHolder = 'email';
    }

    Constants.WHITELABEL.forEach(element => {
      if (element.cod === environment.WHITELABEL) {
        if (element.phone1 && element.phone1 !== '') {
          if (element.isRecoverWhatsapp) {
            this.optinsRecovever.isRecoverWhatsapp = element.isRecoverWhatsapp;
            this.optinsRecovever.helpPhone1 = element.phone1;
          }
          if (element.isRecoverSupport) {
            this.optinsRecovever.isRecoverSupport = element.isRecoverSupport;
            if (element.phone1) {
              this.optinsRecovever.helpPhone1 = element.phone1;
            }
            if (element.phone2) {
              this.optinsRecovever.helpPhone2 = element.phone2;
            }
          }
          if (element.isRecoverEmail) {
            this.optinsRecovever.isRecoverEmail = element.isRecoverEmail;
            this.optinsRecovever.helpEmail = element.email;
          }
        }
      }
    });
  }

  closeClick(): void {
    this.dialogRef.close(false);
  }

  ngOnInit() {
    this.renderer.removeClass(document.body, 'darkTheme');
  }

  public resetValidity(value) {
    if (value.key !== 'Enter') {
      const valueBefore = this.loginForm.controls['username'].value;
      this.loginForm.controls['username'].setValue(valueBefore);
      const snackDiv = document.getElementById('snack-bar-msg');
      snackDiv.classList.remove('active');
      snackDiv.style.display = 'hidden';
    }
  }

  public send(user: string) {
    this.loading = true;
    if (!user) {
      this._NotificationService.error('', Constants.MSG05_FORGOT_ERROR);
      return;
    }
    if (this.isLoginWSO2) {
      user = this.loginNoAsterisks;
    }
    this._authenticationService.recoverPassword(user, Constants.FORGOT_TYPE_02, true)
      .subscribe(res => {
        if (res && res.success === 'true') {
          this.data.title = Constants.MSG02_FORGOT_TITLE;
          this.data.content = Constants.MSG02_FORGOT_CONTENT;
          this.loading = false;
          this.showConfirm = true;
        } else if (res && res.success === 'false' && res.message) {
          this.data.title = res.message;
          this.data.content = null;
          this.loading = false;
          this.showError = true;
        } else if (this._configService.isLoginTypeWSO2() && res._isScalar !== false) {
          this.data.title = Constants.MSG02_FORGOT_TITLE;
          this.data.content = Constants.MSG02_FORGOT_CONTENT;
          this.loading = false;
          this.showConfirm = true;
        } else {
          this.data.title = Constants.MSG08_FORGOT_ERROR_TITLE;
          this.data.content = Constants.MSG08_FORGOT_ERROR_CONTENT;
          this.loading = false;
          this.showError = true;
          // if (res && res.message) {
          //   this._NotificationService.error('', res.message);
          // } else {
          //   this._NotificationService.error('', Constants.MSG_LOGIN_MSG003);
          // }
          this.loading = false;
        }
      }, error => {
        this.loading = false;
        this._NotificationService.error('', Constants.MSG_LOGIN_MSG003);
      });
  }

  goBack() {
    this.showError = false;
    this.showConfirm = false;
    this.data.title = Constants.MSG01_FORGOT_TITLE;
    this.data.content = Constants.MSG01_FORGOT_CONTENT;
  }

  openWhatsappWindow() {
    window.open(Constants.SIDEBAR_HELP_WHATSAPP_LINK);
  }

  maskLogin(event, field) {
    let totalLettersDeleted = 0;
    if (event.type === 'paste') { // MASCARA QUANDO COLAR
      let clipboardData, pastedData;
      event.stopPropagation();
      event.preventDefault();
      clipboardData = event.clipboardData;
      pastedData = clipboardData.getData('Text');
      this.loginNoAsterisks = this.restrictCharacters(pastedData);
    } else { // MASCAR QUANDO DIGITA
      if ((event.keyCode === 32) || (event.keyCode === 46) || (event.code === 'KeyV' && event.ctrlKey && this.user !== '')) {
        event.stopPropagation();
        event.preventDefault();
        return false;
      }
      let retorno = true;
      if (this.loginNoAsterisks.length >= 11 && event.keyCode !== 8) {
        event.stopPropagation();
        event.preventDefault();
        this.user = this.transformToAsterisk(this.loginNoAsterisks);
        return false;
      }
      if (event.keyCode === 8) { // EVENTO DE APAGAR CARACTERES
        retorno = false;
        const arrNumbers = Array.from(this.loginNoAsterisks);
        totalLettersDeleted = this.loginNoAsterisks.length - this.user.length;
        for (let i = 0; i < totalLettersDeleted; i++) {
          arrNumbers.pop();
        }
        this.loginNoAsterisks = arrNumbers.join('');
        // this.loginNoAsterisks = this.loginNoAsterisks.slice(0, -1);
        this.user = this.transformToAsterisk(this.loginNoAsterisks);
        event.preventDefault();
        return retorno;
      }
      if (event instanceof KeyboardEvent) { // Captura de dados informados
        if (event.code === 'KeyV' && event.ctrlKey) {
          this.loginNoAsterisks = this.user;
        } else {
          this.loginNoAsterisks += event.key;
        }
        const reg = this.restrictCharacters(this.loginNoAsterisks);
        this.loginNoAsterisks = reg;
        this.user = this.transformToAsterisk(reg);
        return retorno;
      } else {
        this.loginNoAsterisks += event.key;
      }
      return retorno;
    }
    this.user = this.transformToAsterisk(this.loginNoAsterisks);
  }

  transformToAsterisk(text) {
    return text.substring(0, 7).replace(/\S/g, '*') + text.substring(7, 11);
  }

  restrictCharacters(text) {
    return text.replace(/[\*\D]/g, '');
  }

}
