import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MinResolutionModalComponent } from '@component/min-resolution-modal/min-resolution-modal.component';

describe('MinResolutionModalComponent', () => {
  let component: MinResolutionModalComponent;
  let fixture: ComponentFixture<MinResolutionModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MinResolutionModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MinResolutionModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
