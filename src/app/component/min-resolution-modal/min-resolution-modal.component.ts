import { ConfigService } from './../../services/webFeeder/config.service';
import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Router } from '@angular/router';

@Component({
  selector: 'app-min-resolution-modal',
  templateUrl: './min-resolution-modal.component.html'
})
export class MinResolutionModalComponent implements OnInit {

  public whiteLabel: number;

  constructor(
    public dialogRef: MatDialogRef<MinResolutionModalComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private router: Router,
    private configService: ConfigService
  ) {
    this.whiteLabel = Number(this.configService.getWhiteLabel());
  }

  closeClick(): void {
    this.dialogRef.close();
  }

  ngOnInit() {
  }

  redirect() {
    if (this.data.redirectUrl && this.data.queryParams) {
      this.router.navigate([this.data.redirectUrl], {
        queryParams: this.data.queryParams
      });
      this.dialogRef.close();
    }
  }

}
