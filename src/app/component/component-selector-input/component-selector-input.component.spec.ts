import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ComponentSelectorInputComponent } from '@component/component-selector-input/component-selector-input.component';

describe('ComponentSelectorInputComponent', () => {
  let component: ComponentSelectorInputComponent;
  let fixture: ComponentFixture<ComponentSelectorInputComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ComponentSelectorInputComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ComponentSelectorInputComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
