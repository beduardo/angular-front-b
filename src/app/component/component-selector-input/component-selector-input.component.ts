import {
  Component, OnInit, Input, Output, EventEmitter, Renderer2, ElementRef, Inject,
  ViewContainerRef, ViewChild, OnDestroy
} from '@angular/core';
import { MessageQuoteQuery } from '@model/webFeeder/message-quote-query.model';
import { QuoteService } from '@services/webFeeder/sckt-quotes.service';
import { CustomWorkspaceItemDto } from '@dto/custom-workspace-item.dto';
import { Constants } from '@utils/constants';
import { NotificationsService } from 'angular2-notifications';
import { Subscription } from 'rxjs';
import { WorkspaceNewService } from '@services/workspace-new.service';
import { MessageQuote } from '@model/webFeeder/message-quote.model';
import { isUndefined } from 'util';
import { AuthServer } from '@services/auth-server.service';

@Component({
  selector: 'app-component-selector-input',
  templateUrl: './component-selector-input.component.html',
})
export class ComponentSelectorInputComponent implements OnInit, OnDestroy {

  @ViewChild('csi') currentComponent: ElementRef;
  @Input() cardTitle: any;
  @Output() addSelectInput = new EventEmitter();
  private removeCardTemp: Subscription;
  private successAdded: Subscription;

  public button_selected = false;
  public loading = false;
  public company: string;
  public assetSymbol: string;
  public validInput: boolean;
  public hasSuccessAddingComponent = true;
  private subscriptionQuote: Subscription;
  private currentQuote;
  private resetText;
  private selectedFlag = false;

  public model: CustomWorkspaceItemDto = new CustomWorkspaceItemDto(null, this.cardTitle, '', '', '', false);
  isCompanySet: boolean;

  @Input() symbolAutoComplete: string;
  @Input() hiddenError: boolean;
  @Output() onsymbolChanged = new EventEmitter<MessageQuoteQuery>();
  public hiddenInvalid: boolean;
  public filteredAssets: any;

  public hiddenInvalidActive = false;

  public digitedValue;


  constructor(
    private renderer: Renderer2,
    private _authServer: AuthServer,
    private _notificationService?: NotificationsService,
    private quoteService?: QuoteService,
    private workspaceService?: WorkspaceNewService,
  ) {
  }

  ngOnInit() {

    this.successAdded = this.workspaceService._successAddComponent$.subscribe(y => {
      this.hasSuccessAddingComponent = y;
    });

    this.removeCardTemp = this.workspaceService.removeCard$.subscribe(x => {
      if (this.model.type) {
        if (x.type === this.model.type) {
          this.model.assetSymbol = '';
          this.model.company = '';
          this.company = '';
          this.assetSymbol = '';
          this.isCompanySet = false;
        }
      }
    });
    this.model.company = '';
    this.company = '';
  }

  ngOnDestroy() {
    if (this.subscriptionQuote) {
      this.subscriptionQuote.unsubscribe();
    }
    if (this.successAdded) {
      this.successAdded.unsubscribe();
    }
  }

  changeClass() {
    this.button_selected = !this.button_selected;
  }

  addComponent() {
    this.loading = true;
    if (this.assetSymbol === '' || this.assetSymbol === undefined) {
      this.validInput = false;
      this.hiddenInvalidActive = true;
      this.model.company = this.company = '';
      this._notificationService.warn('Ops,', Constants.WORKSPACE_MSG005);
      this.loading = false;
    } else if (this.model.assetSymbol === '' || this.assetSymbol.length < 4) {
      this.hiddenInvalidActive = true;
      this.model.company = this.company = '';
      this.validInput = false;
      this.loading = false;
    } else {
      this.changeClass();
      this.cardEffect(this.currentComponent);
      this.model.company = '';
      this.company = '';
      setTimeout(() => {
        this.changeClass();
        this.addSelectInput.emit(this.model);
        if (this.hasSuccessAddingComponent === true) {
          this.company = '';
        }
        this.loading = false;
      }, 500);
    }
  }

  public onAutoCompletAsset(textInput: any, source?: string) {
    if (textInput.key.includes('Enter')) {
      this.onInputEnter(textInput, source);
    } else {
      if (textInput.key !== 'ArrowDown' && textInput.key !== 'ArrowUp') {
        this.model.assetSymbol = '';
        this.model.company = '';
      }
      this.hiddenInvalid = true;
      if (textInput.key !== 'ArrowDown' && textInput.key !== 'ArrowUp') {
        let symbolAutoComplete = '';
        if (textInput.target === undefined) {
          symbolAutoComplete = textInput.toLowerCase();
        } else {
          symbolAutoComplete = textInput.target.value.toLowerCase();
        }
        if (symbolAutoComplete.length >= 1) {
          this._authServer.iQuote.quotesQuery(symbolAutoComplete)
            .subscribe(datas => {
              if (datas && datas.length > 0) {
                this.filteredAssets = datas;
              } else {
                this.filteredAssets = [];
              }
            });
        }
      }
    }
  }

  onInputEnter(event, source?: string) {
    if (event.target.value === '' && event.key.includes('Enter')) {
      this._notificationService.warn('Ops,', Constants.WORKSPACE_MSG005);
    } else if (event.key.includes('Enter')) {
      // if (this.filteredAssets.length > 0) {
      this.setQuote(event.target.value, source);
      // } else {
      //   this.setQuote(event.target.value, source);
      // }
    } else {
      return false;
    }
  }

  cardEffect(cardRef: ElementRef): void {
    const elementLeft = cardRef.nativeElement.offsetLeft;
    const windowWidth = window.innerWidth;
    const elementTop = cardRef.nativeElement.offsetTop;
    const verticalFinalPosition = windowWidth - elementLeft - 315;
    const horizontalFinalPosition = window.innerHeight / 2 - 300;

    this.renderer.setStyle(cardRef.nativeElement, 'right', '-' + verticalFinalPosition + 'px');

    if (elementTop > window.innerHeight / 2) {
      this.renderer.setStyle(cardRef.nativeElement, 'top', '-' + horizontalFinalPosition + 'px');
    } else {
      this.renderer.setStyle(cardRef.nativeElement, 'top', horizontalFinalPosition + 'px');
    }

    setTimeout(() => {
      this.renderer.setStyle(cardRef.nativeElement, 'top', '0px');
      this.renderer.setStyle(cardRef.nativeElement, 'right', '0px');
    }, 500);
  }


  public onBlur(quoteSymbol?: string, source?, symbolAutoComplete?: string, itemSelected?: string) {
    this.setQuote(quoteSymbol, source);
  }

  onchangeSymbol(evento, source?, symbolAutoComplete?: string, itemSelected?: string) {
    this.assetSymbol = itemSelected;
    this.setQuote(itemSelected, source);

  }

  public onQuoteChange(quoteSymbol?: string, source?, symbolAutoComplete?: string, itemSelected?: string) {
    let searchSymbol;
    if (quoteSymbol) {
      // closePanel
      if (quoteSymbol.length >= 2) {
        // closePanel
        if (this.filteredAssets && this.filteredAssets.length > 0) {
          // closePanel
          const symbolTemp = this.filteredAssets.filter(x => x.symbol.toUpperCase() === quoteSymbol.toUpperCase());
          if (symbolTemp.length === 0) {
            searchSymbol = quoteSymbol;
          } else {
            searchSymbol = symbolTemp[0].symbol;
            this.filteredAssets = [];
            this.hiddenInvalidActive = false;
            this.isCompanySet = true;
            // this.model.company = symbolTemp[0].company;
            this.model.assetSymbol = symbolTemp[0].symbol;
            this.filteredAssets = [];
            this.validInput = true;
            // return;
          }
        } else {
          searchSymbol = quoteSymbol;
        }
        // closePanel
        this.model.type = source;
        // this.model.company = searchSymbol[0].company;
        // this.isCompanySet = true;
        // this.model.assetSymbol = searchSymbol[0];
        this.setQuote(searchSymbol, source);
      } else {
        // closePanel
        return false;
      }
    }
  }

  setQuote(value: any, source: any) {
    // closePanel
    if (value) {
      if (this.subscriptionQuote) {
        this.subscriptionQuote.unsubscribe();
      }
      this.quoteService.subscribeQuote(value.toLowerCase(), Constants.WINDOW_NAME_SELECTOR_INPUT);
      let countValid = 0;
      this.subscriptionQuote = this.quoteService.messagesQuote
        .subscribe((data: MessageQuote) => {
          if (data && data.errorMessage && data.symbol.toLowerCase() === value.toLowerCase()
            && this.cardTitle.id.toLowerCase() === source) {
            if (countValid === 0) {
              this.hiddenInvalidActive = true;
              this.model.company = this.company = '';
              this.quoteService.unsubscribeQuote(value.toLowerCase(), Constants.WINDOW_NAME_SELECTOR_INPUT);
              this.filteredAssets = [];
              this.validInput = false;
              countValid++;
              return false;
            }
          } else if (data !== undefined && data.symbol != null && data.symbol.toLowerCase() === value.toLowerCase()
            && this.cardTitle.id.toLowerCase() === source) {
            if (countValid === 0) {
              this.hiddenInvalidActive = false;
              this.isCompanySet = true;
              this.model.company = this.company = data.description.toLocaleUpperCase();
              this.model.assetSymbol = data.symbol.toLocaleUpperCase();
              this.filteredAssets = [];
              this.validInput = true;
              this.quoteService.unsubscribeQuote(value.toLowerCase(), Constants.WINDOW_NAME_SELECTOR_INPUT);
              countValid++;
              return true;
            }
          }
        });
    } else {
      this.hiddenInvalidActive = true;
      this.model.company = this.company = '';
      this.quoteService.unsubscribeQuote(value.toLowerCase(), Constants.WINDOW_NAME_SELECTOR_INPUT);
      this.filteredAssets = [];
      this.validInput = false;
    }
  }

  optionSelected(event) {
    this.selectedFlag = true;
    const symbol = event.option.value.toLowerCase();
    const messageQuoteQuery = new MessageQuoteQuery();
    this.symbolAutoComplete = symbol;
    this.filteredAssets = [];
    messageQuoteQuery.symbol = this.symbolAutoComplete.toUpperCase();
    messageQuoteQuery.company = this.company;
    this.onsymbolChanged.emit(messageQuoteQuery);
    this.hiddenInvalid = true;
  }


}
