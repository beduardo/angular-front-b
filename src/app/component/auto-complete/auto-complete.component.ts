import { MessageQuote } from '@model/webFeeder/message-quote.model';
import { Component, OnInit, Output, EventEmitter, Input, AfterViewInit, ViewChild, ViewContainerRef, Renderer2 } from '@angular/core';
import { MessageQuoteQuery } from '@model/webFeeder/message-quote-query.model';
import { QuoteService } from '@services/webFeeder/sckt-quotes.service';
import { ICellEditorAngularComp } from 'ag-grid-angular';
import { Constants } from '@utils/constants';
import { MatAutocomplete } from '@angular/material';
import { Subject } from 'rxjs';
import { NotificationsService } from 'angular2-notifications';
import { AuthServer } from '@services/auth-server.service';
import { FormControl } from '@angular/forms';
import { startWith, map } from 'rxjs/operators';


@Component({
  selector: 'app-auto-complete',
  templateUrl: './auto-complete.component.html'
})
export class AutoCompleteComponent implements OnInit, AfterViewInit, ICellEditorAngularComp {
  @Input() symbolAutoComplete: string;
  @Input() symbolCompanyAutoComplete: string;
  @Output() onsymbolChanged = new EventEmitter<MessageQuoteQuery>();
  @Output() onsymbolChangedInvalid = new EventEmitter<boolean>();
  @Output() rawInformation = new EventEmitter<String>();
  @Output() hiddenInvalidOutput = new EventEmitter<Boolean>();
  @Input() hiddenError: boolean;
  @Input() autoFocus: boolean;
  @Input() hideCompany: boolean;
  @Input() placeHolderSetter: string;
  @Input() reset: Subject<boolean>;
  @Input() autoSizeActive = false;
  @Output() setActiveValid = new EventEmitter<boolean>();
  @ViewChild('autoInput', { read: ViewContainerRef }) public input;
  @ViewChild('issuingAsset') autoComplete: MatAutocomplete;

  public assetValid = false;
  public filteredAssets: MessageQuoteQuery[];
  public company: string;
  public hiddenInvalid = true;
  private params: any;
  private selectedFlag = false;
  isWorksheet = false;
  msgInvalidAsset = Constants.WORKSPACE_MSG007;
  private inputQuotePattern = /^[a-z0-9]+$/i;
  symbolCtrl = new FormControl();
  oldValue: any;

  constructor(
    private quoteService: QuoteService,
    private renderer: Renderer2,
    private _authServer: AuthServer,
    private _notificationService?: NotificationsService,
  ) {
    this.symbolAutoComplete = '';
    this.hiddenError = true;
    if (!this.placeHolderSetter) {
      this.placeHolderSetter = 'ATIVO';
    }
    const ctrlQuestion = this.symbolCtrl.valueChanges
      .pipe(startWith(null),
        map(filterString => this.onAutoCompletAsset(filterString)));
    ctrlQuestion.subscribe();
  }

  ngOnInit() {
    if (this.symbolAutoComplete) {

      this._authServer.iQuote.quotesQuery(this.symbolAutoComplete)
        .subscribe(datas => {
          if (datas) {
            if (this.symbolAutoComplete) {
              this.symbolAutoComplete = this.symbolAutoComplete.toUpperCase();
            }
            if (datas.length > 0) {
              this.filteredAssets = datas;
              this.SetCompany(this.symbolAutoComplete);
            }
          }
        });
    } else {
      this.symbolAutoComplete = '';
    }

    if (this.reset) {
      setTimeout(() => {
        this.reset.subscribe(v => {
          this.hiddenInvalid = true;
        });
      }, 0);
    }
  }

  callBackWorksheet(event) {
    setTimeout(() => {
      if (this.isWorksheet) {
        if (this.symbolAutoComplete) {
          const aux = this.filteredAssets.find(data => {
            return data.symbol === this.symbolAutoComplete.toLocaleUpperCase();
          });
          this.symbolAutoComplete = aux ? this.symbolAutoComplete.toLocaleUpperCase() : '';
        }
        this.params.api.stopEditing();

      }
    }, 100);
  }

  ngAfterViewInit() {
    if (this.autoFocus) {
      setTimeout(() => {
        this.focusInput();
      });
    }
  }

  public clearInput() {
    this.symbolAutoComplete = '';
    this.company = '';
  }

  focusInput() {
    this.input.element.nativeElement.focus();
  }

  public onAutoCompletAsset(textInput: any) {
    if (this.oldValue && textInput && this.oldValue.toUpperCase() === textInput.toUpperCase() || textInput === '') {
      this.filteredAssets = [];
      return [];
    }
    this.oldValue = textInput;
    this.hiddenInvalid = true;
    this.hiddenInvalidOutput.emit(this.hiddenInvalid);
    if (textInput && textInput.length > 1) {
      this.rawInformation.emit(textInput);
      this._authServer.iQuote.quotesQuery(textInput)
        .subscribe(datas => {
          if (datas) {
            if (datas.length > 0 && textInput !== '') {
              this.filteredAssets = datas;
            } else {
              this.filteredAssets = [];
            }
          } else {
            this.filteredAssets = undefined;
            return;
          }
        });
    }
    if (textInput && textInput.length >= 5) {
      if (this.filteredAssets && this.filteredAssets.length === 0) {
      } else {
        this.hiddenInvalid = true;
      }
    } else {
      // TODO this.company = '';
    }
    if (this.filteredAssets === undefined || this.filteredAssets.length === 0) {
      // TODO this.company = '';
    }
    return this.filteredAssets;
  }

  onchangeSymbol(evento, symbolAutoComplete?: string, itemSelected?: string) {
    this.symbolAutoComplete = '';
    const messageQuoteQuery = new MessageQuoteQuery();
    if (evento.source !== undefined) {
      if (this.filteredAssets.length > 0) {
        let searchSymbol;
        if (itemSelected) {
          searchSymbol = this.filteredAssets.filter(x => x.symbol.toUpperCase() === itemSelected.toUpperCase());
        } else if (symbolAutoComplete) {
          searchSymbol = this.filteredAssets.filter(x => x.symbol.toUpperCase() === symbolAutoComplete.toUpperCase());
        }
        if (searchSymbol) {
          this.symbolAutoComplete = searchSymbol[0].symbol.toUpperCase();
        }
      } else {
        return false;
      }
      this.symbolAutoComplete = evento.source.value.toUpperCase();
      this.SetCompany(this.symbolAutoComplete.toUpperCase());
      this.filteredAssets = [];
      messageQuoteQuery.symbol = this.symbolAutoComplete.toUpperCase();
      // this.onsymbolChanged.emit(messageQuoteQuery);
      this.onInputEnter(evento);
    } else if (evento.target !== undefined) {
      this.symbolAutoComplete = evento.target.value.toUpperCase();
      if (this.filteredAssets.findIndex(s => s.symbol.toUpperCase() === this.symbolAutoComplete) === -1) {
        this.symbolAutoComplete = '';
      }
      this.SetCompany(this.symbolAutoComplete.toUpperCase());
      this.filteredAssets = [];
      messageQuoteQuery.symbol = this.symbolAutoComplete.toUpperCase();
      this.onsymbolChanged.emit(messageQuoteQuery);
    } else if (evento.value !== undefined && evento.value !== '') {
      this.symbolAutoComplete = evento.value.toUpperCase();
      if (this.filteredAssets.findIndex(s => s.symbol.toUpperCase() === this.symbolAutoComplete) !== -1) {
        this.SetCompany(this.symbolAutoComplete.toUpperCase());
        this.filteredAssets = [];
        messageQuoteQuery.symbol = this.symbolAutoComplete.toUpperCase();
        this.onsymbolChanged.emit(messageQuoteQuery);
      }
    }
    if (this.params) {
      this.params.api.stopEditing();
    }
  }

  private SetCompany(symbolAutoComplete: string) {
    if (!this.filteredAssets && this.filteredAssets.length === 0) {
      return;
    }
    const filtered = this.filteredAssets.find(s => {
      if (s.symbol) {
        return s.symbol.toUpperCase() === symbolAutoComplete;
      }
    });
    if (filtered) {
      this.company = filtered.company;
      this.symbolCompanyAutoComplete = filtered.company;
      setTimeout(() => {
        this.symbolAutoComplete = filtered.symbol;
      }, 300);
    }
  }

  onInputEnter(event) {
    setTimeout(() => {
      if (!this.selectedFlag) {
        const messageQuoteQuery = new MessageQuoteQuery();
        let symbol = '';
        if (event.target && event.target.value) {
          symbol = event.target.value.toLowerCase();
        } else if (event.source) {
          symbol = event.source.value.toLowerCase();
        } else {
          symbol = event;
        }
        if (event.target && event.target.value === '' && event.key.includes('Enter')) {
          this._notificationService.warn('Ops,', Constants.WORKSPACE_MSG005);
          return;
        }
        this.symbolAutoComplete = symbol;
        if (symbol.length > 0 && symbol.length < 3) {
          this.filteredAssets = [];
          this.company = '';
          this.hiddenInvalid = false;
          this.hiddenInvalidOutput.emit(this.hiddenInvalid);
          messageQuoteQuery.symbol = symbol;
          this.onsymbolChanged.emit(messageQuoteQuery);
          if (this.params) {
            this.params.api.stopEditing();
          }
          return;
        }
        this._authServer.iQuote.quotesQuery(symbol)
          .subscribe(datas => {
            if (datas) {
              if (datas.length > 0) {
                const data = datas.filter(item => item.symbol.toUpperCase() === symbol.toUpperCase());
                if (data.length === 1) {
                  this.symbolAutoComplete = data[0].symbol;
                  this.SetCompany(this.symbolAutoComplete.toUpperCase());
                  this.filteredAssets = [];
                  messageQuoteQuery.symbol = this.symbolAutoComplete.toUpperCase();
                  messageQuoteQuery.company = this.company;
                  this.onsymbolChanged.emit(messageQuoteQuery);
                  this.hiddenInvalid = true;
                  this.hiddenInvalidOutput.emit(this.hiddenInvalid);
                  if (this.params) {
                    this.params.api.stopEditing();
                  }
                } else {
                  setTimeout(() => {
                    if (!this.params) {
                      this.filteredAssets = [];
                    }
                    this.company = '';
                    this.hiddenInvalid = false;

                    this.hiddenInvalidOutput.emit(this.hiddenInvalid);
                    if (this.params) {
                      messageQuoteQuery.symbol = '';
                    } else {
                      messageQuoteQuery.symbol = symbol;
                    }
                    this.onsymbolChanged.emit(messageQuoteQuery);
                    if (this.params) {
                      this.params.api.stopEditing();
                    }
                  }, 100);
                }
              } else {
                this.company = '';
                this.hiddenInvalid = false;

                this.hiddenInvalidOutput.emit(this.hiddenInvalid);
                if (this.params) {
                  messageQuoteQuery.symbol = '';
                } else {
                  messageQuoteQuery.symbol = symbol;
                }
                this.onsymbolChanged.emit(messageQuoteQuery);
                setTimeout(() => {
                  if (this.params) {
                    this.params.api.stopEditing();
                  }
                }, 0);
                // messageQuoteQuery.symbol = 'invalid';
                // messageQuoteQuery.company = 'invalid';
                // this.onsymbolChanged.emit(messageQuoteQuery);
              }
            } else {
              this.company = '';
            }
          });
      } else {
        this.selectedFlag = false;
      }
    }, 0);
  }

  agInit(params: any): void {
    this.params = params;
    this.symbolAutoComplete = this.params.value;
    this.isWorksheet = true;
    setTimeout(() => {
      this.input.element.nativeElement.focus();
    });
  }

  getValue(): any {
    return this.symbolAutoComplete;
  }

  optionSelected(event) {
    this.selectedFlag = true;
    const symbol = event.option.value.toLowerCase();
    const messageQuoteQuery = new MessageQuoteQuery();
    this.symbolAutoComplete = symbol;
    this.SetCompany(this.symbolAutoComplete.toUpperCase());
    this.filteredAssets = [];
    messageQuoteQuery.symbol = this.symbolAutoComplete.toUpperCase();
    messageQuoteQuery.company = this.company;
    this.onsymbolChanged.emit(messageQuoteQuery);
    this.hiddenInvalid = true;
    this.hiddenInvalidOutput.emit(this.hiddenInvalid);
    if (this.params) {
      this.params.api.stopEditing();
    }
  }

  keyPressed(event: any) {
    const inputChar = String.fromCharCode(event.charCode);
    if (event.charCode !== 45) {
      if (!this.inputQuotePattern.test(inputChar)) {
        // invalid character, prevent input
        event.preventDefault();
      }
    }
  }

  onPaste(event) {
    this.keyPressed(event);
    const content = event.clipboardData.getData('text/plain');

    if (!this.inputQuotePattern.test(content)) {
      event.preventDefault();
    }

  }

}
