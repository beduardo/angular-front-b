import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StopGainBuyOrderComponent } from '@component/stop-gain-buy-order/stop-gain-buy-order.component';

describe('StopGainBuyOrderComponent', () => {
  let component: StopGainBuyOrderComponent;
  let fixture: ComponentFixture<StopGainBuyOrderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StopGainBuyOrderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StopGainBuyOrderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
