import { Constants } from '@utils/constants';
import { StopGainOrder } from '@model/stop-gain-order.model';
import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { QuoteService } from '@services/webFeeder/sckt-quotes.service';
import { Subscription } from 'rxjs';
import { MessageQuote } from '@model/webFeeder/message-quote.model';
import { ErrorStateMatcher } from '@angular/material';
import { Validators, FormControl } from '@angular/forms';


@Component({
  selector: 'app-stop-gain-buy-order',
  templateUrl: './stop-gain-buy-order.component.html'
})
export class StopGainBuyOrderComponent implements OnInit, OnDestroy {
  @Input() symbol: any;
  @Input() type: any;
  private subscriptionQuote: Subscription;

  public currentSymbol;
  public _messageQuote = new MessageQuote();
  public standardQuantity: number;
  public model = new StopGainOrder;
  public durationOptions = Constants.TIME_IN_FORCES;

  quantidade = new FormControl('', [Validators.required, Validators.min(1)]);

  public priceLimitValuesFormatOptions = {
    prefix: Constants.CURRENCY_BRAZILIAN_SYMBOL,
    thousands: Constants.THOUSANDS_SEPARATOR,
    decimal: Constants.DECIMALS_SEPARATOR,
    allowNegative: false,
    precision: 2,
    align: 'left'
  };

  constructor(
    private _quoteService: QuoteService,
  ) { }

  ngOnInit() {
    this.model.type = this.type;
    if (this.symbol) {
      this.selectAsset(this.symbol);
    }

  }

  ngOnDestroy(): void {
    this._quoteService.unsubscribeQuote(this.symbol, Constants.WINDOW_NAME_STOP_GAIN_BUY_ORDER);
    if (this.subscriptionQuote) {
      this.subscriptionQuote.unsubscribe();
    }
  }

  selectAsset(value: any) {
    if (value) {
      this._quoteService.unsubscribeQuote(this.symbol, Constants.WINDOW_NAME_STOP_GAIN_BUY_ORDER);
      if (this.subscriptionQuote) {
        this.subscriptionQuote.unsubscribe();
      }
      if (value.symbol) {
        value = value.symbol;
      }
      if (value.symbol !== '') {
      }
      this._quoteService.subscribeQuote(value.toLocaleLowerCase(), Constants.WINDOW_NAME_STOP_GAIN_BUY_ORDER);
      this.subscriptionQuote = this._quoteService.messagesQuote.subscribe(
        data => {
          if (
            data !== undefined &&
            data.symbol === value.toLocaleLowerCase()
          ) {
            this._messageQuote = data;
            this.model.symbol = this._messageQuote.symbol;
            if (!this.model.quantitySet) {
              this.model.quantity = this._messageQuote.loteDefault;
              this.model.quantitySet = true;
            }
            this.model.lastTrade = this._messageQuote.lastTrade;
            this.model.tunnelUpperLimit = this._messageQuote.tunnelUpperLimit;
            this.model.tunnelLowerLimit = this._messageQuote.tunnelLowerLimit;
            this.currentSymbol = value;
          }
        }
      );
    }
  }

  parseFloat(value) {
    return parseFloat(value);
  }

}
