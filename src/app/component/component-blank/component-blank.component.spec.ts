import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ComponentBlankComponent } from '@component/component-blank/component-blank.component';

describe('ComponentBlankComponent', () => {
  let component: ComponentBlankComponent;
  let fixture: ComponentFixture<ComponentBlankComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ComponentBlankComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ComponentBlankComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
