import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-component-blank',
  templateUrl: './component-blank.component.html',
})
export class ComponentBlankComponent implements OnInit {

  public button_selected = false;

  @Input() public cardTitle: any;
  @Output() addFunc = new EventEmitter();

  constructor() { }

  ngOnInit() {
  }

  changeClass() {
    this.button_selected = !this.button_selected;
  }

  addComponent() {
    this.changeClass();
    if (!this.button_selected) {
      this.addFunc.emit('ALÔ');
    } else {
      this.addFunc.emit('ALÔ2');
    }
  }

}
