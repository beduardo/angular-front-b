import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Router } from '@angular/router';
import { ConfigService } from '@services/webFeeder/config.service';
import { AuthenticationService } from '@services/authentication.service';
import { NotificationsService } from 'angular2-notifications';

@Component({
  selector: 'app-normal-modal',
  templateUrl: './normal-modal.component.html',
})
export class NormalModalComponent implements OnInit {

  public signature: string;
  public loadingConfirmation: boolean;
  public validSignature: boolean;

  constructor(
    public dialogRef: MatDialogRef<NormalModalComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private router: Router,
    private _configService: ConfigService,
    private _authenticationService: AuthenticationService,
    private _notificationService: NotificationsService,
  ) {
    // console.log(data);
  }

  closeClick(): void {
    this.dialogRef.close(false);
  }

  ngOnInit() {
  }

  redirect() {
    if (this.data.redirectUrl && this.data.queryParams) {
      this.router.navigate([this.data.redirectUrl], {
        queryParams: this.data.queryParams
      });
    }
    this.dialogRef.close(true);
  }

  validateSignatureValid(signature: string) {
    if (signature) {
      this.loadingConfirmation = true;
      const clientId = Number(this._configService.getHbAccountId());
      const params = { user_client_id: clientId, signature: this.signature };
      this._authenticationService.validateSignatureValid(params)
        .subscribe(rest => {
          if (rest && rest._isScalar !== false) {
            this.validSignature = rest.is_valid;
            this.loadingConfirmation = false;
            if (!this.validSignature) {
              this.signature = null;
              this._notificationService.error('', 'Assinatura Eletrônica inválida!');
            }
          } else {
            this.validSignature = false;
            this.loadingConfirmation = false;
            this.signature = null;
            this._notificationService.error('', 'Ocorreu um erro ao validar a Assinatura Eletrônica!');
          }
        }, error => {
          this._notificationService.error('', 'Ocorreu um erro ao validar a Assinatura Eletrônica!');
          this.signature = null;
          this.loadingConfirmation = false;
        });
    }
  }

}
