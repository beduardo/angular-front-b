import { Component, Input } from '@angular/core';

import { ICellRendererAngularComp } from 'ag-grid-angular';

@Component({
  selector: 'currency-cell',
  templateUrl: './currency.template.html'
})
export class CurrencyComponent implements ICellRendererAngularComp {
  public params: any;
  public value: any;
  public integerValue: any;
  public centsValue: any;
  private prefixMoney = '';
  private classActual = '';
  private changeColor = false;

  @Input()
  set price(value: any) {
    this.value = value;
  }
  get price(): any {
    return this.value;
  }

  @Input()
  set showPrefix(value: boolean) {
    if (value) {
      this.prefixMoney = 'R$';
    } else {
      this.prefixMoney = '';
    }
  }
  get showPrefix(): boolean {
    return this.prefixMoney !== '';
  }

  @Input()
  set changeBackground(value: boolean) {
    this.changeColor = value;
  }
  get changeBackground(): boolean {
    return this.changeColor;
  }

  agInit(params: any): void {
    this.params = params;
    this.value = this.params.value;
  }

  refresh(params: any): boolean {
    const refreshField = this.value !== params.value;
    if (refreshField) {
      this.params = params;
      this.value = this.params.value;
    }
    return refreshField;
  }

   private separatorValue(param) {
    if (param === undefined) {
      param = '';
    }
    const value = param + '';
    const prefix = '';
    this.integerValue = value;
    this.centsValue = '';
    if (value) {
      const result = value.split(',');
      if (result.length > 1) {
        this.integerValue = this.prefixMoney + result[0] + ',';
        this.centsValue = result[1];
      } else {
        this.integerValue = this.prefixMoney + result[0] + ',';
        this.centsValue = 0;
      }
    }
  }

  public getClass() {
    return this.classActual;
  }
}
