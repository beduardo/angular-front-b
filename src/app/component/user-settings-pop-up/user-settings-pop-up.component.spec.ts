import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserSettingsPopUpComponent } from '@component/user-settings-pop-up/user-settings-pop-up.component';

describe('UserSettingsPopUpComponent', () => {
  let component: UserSettingsPopUpComponent;
  let fixture: ComponentFixture<UserSettingsPopUpComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserSettingsPopUpComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserSettingsPopUpComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
