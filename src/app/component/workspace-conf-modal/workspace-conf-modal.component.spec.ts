import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WorkspaceConfModalComponent } from '@component/workspace-conf-modal/workspace-conf-modal.component';

describe('WorkspaceConfModalComponent', () => {
  let component: WorkspaceConfModalComponent;
  let fixture: ComponentFixture<WorkspaceConfModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WorkspaceConfModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WorkspaceConfModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
