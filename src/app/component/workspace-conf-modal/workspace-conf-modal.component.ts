import { Subscription } from 'rxjs/Rx';
import { Component, OnInit, Inject, HostListener, ViewChild, Input, ElementRef } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA, MatDialog } from '@angular/material/dialog';
import { ModalMessageConfirmComponent } from '@component/modal-message-confirm/modal-message-confirm.component';
import { Router } from '@angular/router';
import { WorkspaceNewService } from '@services/workspace-new.service';
import { WorkspaceConfModalDto } from '@dto/workspace-conf-modal-dto';
import { Constants } from '@utils/constants';
import { WorkspaceNewSaveDto } from '@dto/workspace-new-save-dto';
import { AuthService } from '@auth/auth.service';
import { WorkspaceItemContentDto } from '@dto/workspace-item-content-dto';
import { PositionDto } from '@dto/position.dto';
import { NotificationsService } from 'angular2-notifications';
import { ConfigService } from '../../services/webFeeder/config.service';
import { CustomWorkspaceModalComponent } from '../custom-workspace-modal/custom-workspace-modal.component';
import { AutoLogoutService } from '@auth/auto-logout-service.service';

@Component({
  selector: 'app-workspace-conf-modal',
  templateUrl: './workspace-conf-modal.component.html'
})
export class WorkspaceConfModalComponent implements OnInit {
  dialog: MatDialog;
  public wksBackModalSubscription: Subscription;
  public wkNewComponentModalSubscription: Subscription;
  public wksOptions = [];
  public customNameWorkspace = '';
  public wksOptSelected = 0;
  public loading = false;
  public wksOptSel: WorkspaceConfModalDto;
  public messageInfo1 = Constants.WKS_WORKSPACE_CONF_MODAL_INFO1;
  public messageInfo2 = Constants.WKS_WORKSPACE_CONF_MODAL_INFO2;
  private nameComp = 'app-workspace-conf-modal';
  @ViewChild('inputCustomName') inputCustomName: ElementRef;

  @HostListener('mousemove')
  onMouseMove() {
    this._autoLogoutService.resetTime();
  }

  @HostListener('click')
  onClick() {
    this._autoLogoutService.resetTime();
  }



  @HostListener('body:keydown', ['$event'])
  onkeydown(event) {
    if (event.key === 'Escape') {
      if (this.wksOptSelected === 6 || this.wksOptSelected === 0) {
        if (!this.workspaceService.validNumberWorkspace()) {
          this._NotificationService.info('', Constants.WORKSPACE_MSG001);
          return false;
        } else {
          this.dialogRef.close();
        }
      }
    }
  }


  constructor(private router: Router,
    dialog: MatDialog,
    public dialogRef: MatDialogRef<ModalMessageConfirmComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private workspaceService: WorkspaceNewService,
    private authService: AuthService,
    private _service: NotificationsService,
    private configService: ConfigService,
    private _autoLogoutService: AutoLogoutService,
    private _NotificationService: NotificationsService
  ) {
    if (!this.authService.isAuthenticated) {
      this.configService.deleteBrokerCookies();
      this.configService.deleteCookie();
      if (this.configService.isLoginTypeWSO2() && this.configService.isUserAssessorLogged()) {
        window.location.href = this.configService.REDIRECT_LOGOUT + '/assessor/login';
      } else if (this.configService.isLoginTypeWSO2() && !this.configService.isUserAssessorLogged()) {
        window.location.href = this.configService.REDIRECT_LOGOUT + '/client/login';
      } else {
        this.router.navigate(['/login']);
      }
    } else {
      this.dialog = dialog;
      this.wksOptSel = new WorkspaceConfModalDto(0, '', '', '', false);
    }

    if (this.data && this.data.workspaceName !== undefined && this.data.workspaceName !== '') {
      this.customNameWorkspace = this.data.workspaceName;
    }

    this.wksBackModalSubscription = this.configService.wksBackModal$.subscribe(res => {
      if (res) {
        this.wksOptSelected = 0;
      }
    });

    this.wkNewComponentModalSubscription = this.configService.wksAddNewComponentModal$.subscribe(res => {
      if (res) {
        this.dialogRef.close();
        this.configService._wksAddNewComponentModal.next(false);
      }
    });
  }

  ngOnInit() {
    if (this.workspaceService.getPredefinedWorkspaces().length === 0) {
      this.workspaceService.loadWorkspaces(this.configService.getUserWorkspacesPreDefineds(), true).subscribe(resp => {
        if (resp) {
          this.mountAndPush(resp);
        }
      });
    } else {
      this.mountAndPush(this.workspaceService.getPredefinedWorkspaces());
    }
    setTimeout(() => {
      this.selectValue(6);
    }, 100);
    setTimeout(() => {
      this.inputCustomName.nativeElement.focus();
    }, 500);
  }

  validateForm(): boolean {
    if (this.customNameWorkspace.length === 0 || this.wksOptSelected <= 0) {
      return false;
    } else {
      return true;
    }
  }

  private mountAndPush(workspacesArr: Array<any>) {
    for (let i = 0; i < workspacesArr.length; i++) {
      let isPersonalized = false;
      const JsonDataArr = JSON.parse(workspacesArr[i].JsonData);
      if (JsonDataArr.content.length === 0) {
        isPersonalized = true;
      } else {
        isPersonalized = false;
      }
      const wksConfDto = new WorkspaceConfModalDto(workspacesArr[i].Id,
        workspacesArr[i].Name.split('-')[Constants.WORKSPACE_CONF_NAME],
        workspacesArr[i].Name.split('-')[Constants.WORKSPACE_CONF_TEXT],
        JsonDataArr, isPersonalized,
        workspacesArr[i].Name.split('-')[Constants.WORKSPACE_CONF_ICON].trim());
      this.wksOptions.push(wksConfDto);
    }
  }

  iniciar() {
    this.loading = true;
    if (!this.wksOptSel.jsondata) {
      this._NotificationService.warn('', Constants.WORKSPACE_MSG022);
      this.loading = false;
      return;
    } else {
      if (this.workspaceService.validateWorkspaceName(this.customNameWorkspace, 9)) {
        if (this.workspaceService.isNameBlank(this.customNameWorkspace)) {
          this._service.warn('', Constants.WORKSPACE_MSG013);
          this.loading = false;
        } else {
          if (this.wksOptSel.isPersonalize) {
            const dialogConfig = this.dialog.open(CustomWorkspaceModalComponent, {
              position: { top: '0' },
              maxWidth: '100vw',
              width: '100vw',
              height: '100vh',
              data: {
                wksOptSelected: this.wksOptSelected,
                customNameWorkspace: this.customNameWorkspace
              },
              disableClose: true
            });
            this.loading = false;
            // dialogConfig.keydownEvents().subscribe(key => {
            //   if (this.wksOptSelected === 6 || this.wksOptSelected === null) {
            //     if (key.keyCode === Constants.ESC_KEY_CODE) {
            //       this.dialogRef.close();
            //     }
            //   }
            // });

            // dialogConfig.beforeClose().subscribe(isGoBack => {
            //   if (isGoBack) {
            //     this.dialogRef.close();
            //   }
            // });
          } else {
            this.dialogRef.close();

            this.workspaceService.loadWorkspaces(this.authService.getUserLogged(), false).subscribe(resp => {
              let indexTabNew = 0;
              for (let i = 0; i < resp.length; i++) {
                if (resp[i].IndexTab > indexTabNew) {
                  indexTabNew = resp[i].IndexTab;
                }
              }
              indexTabNew = indexTabNew + 1;
              const content: Array<WorkspaceItemContentDto> = this.convertDtoContenToSave(this.wksOptSel.jsondata);

              const wksDtoSave: WorkspaceNewSaveDto = new WorkspaceNewSaveDto(this.customNameWorkspace, content, indexTabNew);
              this.loading = false;
              this.workspaceService.saveWorkspace(wksDtoSave).subscribe(data => {
                if (data) {
                  this.router.navigate([Constants.HOME_WKS_PATH], {
                    queryParams: {
                      workspaceId: data.Id
                    }
                  });
                }
              });
            });
          }
        }
      } else {
        this._NotificationService.warn('', Constants.WORKSPACE_MSG002);
        this.loading = false;
      }
    }
  }

  private convertDtoContenToSave(jsondata: any): Array<WorkspaceItemContentDto> {
    const contentFrom = jsondata.content;
    const contentTo: Array<WorkspaceItemContentDto> = [];
    for (let k = 0; k < contentFrom.length; k++) {
      const objItem: WorkspaceItemContentDto = new WorkspaceItemContentDto(k, contentFrom[k].symbol,
        contentFrom[k].company, contentFrom[k].type, contentFrom[k].position as PositionDto,
        contentFrom[k].showInGridster || false, contentFrom[k].workspace);
      contentTo.push(objItem);
    }

    return contentTo;
  }

  closeModal(): void {
    if (!this.workspaceService.validNumberWorkspace()) {
      this._NotificationService.info('', Constants.WORKSPACE_MSG001);
    } else {
      this.dialogRef.close();
    }
  }

  selectValue(value) {
    setTimeout(() => {
      this.wksOptSelected = value;
      for (let k = 0; k < this.wksOptions.length; k++) {
        if (this.wksOptions[k].id === value) {
          this.wksOptSel = this.wksOptions[k];

          for (let i = 0; i < Constants.MAX_WKS_CONF; i++) {
            if (this.wksOptions[i].id !== value) {
              const elem = document.getElementById(this.wksOptions[i].id);
              elem.className = 'select-pre-built';
            } else {
              const elem = document.getElementById(this.wksOptions[i].id);
              elem.className = 'select-pre-built selected';
            }
          }
          break;
        }
      }
    }, 100);
  }
}
