import { ChangeDetectorRef, Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { AutoLogoutService } from '@auth/auto-logout-service.service';
import { ModalDismissReasons, NgbCalendar, NgbDate, NgbDatepickerConfig, NgbModal, NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';
import { Subject } from 'rxjs';
import * as moment from 'moment';
import { bind } from 'moment';
@Component({
  selector: 'ngbd-datepicker-range',
  templateUrl: './datepicker-range.html',
})
export class NgbdDatepickerRange implements OnInit {

  public monthNames = ['jan', 'fev', 'mar', 'abr', 'mai', 'jun',
    'jul', 'ag', 'set', 'out', 'nov', 'dez'
  ];

  public dayNames = ['dom', 'seg', 'ter', 'qua', 'qui', 'sex', 'sab'
  ];



  public years;
  hoveredDate: NgbDate;
  fromDate: NgbDate;
  toDate: NgbDate;
  dateIniti;
  dateNow = new Date();

  public description;
  public stringContent1;
  public stringContent2;
  public valueI;
  public valueF;
  public selected;
  public selectedMonth;
  public closeResult: string;
  public arrows: NgbDatepickerConfig;
  public model: NgbDateStruct;
  public maxDate = null;
  public minDate = null;
  public oldDateIni = null;
  public oldDateEnd = null;

  @Input() minOneYear = false;
  @Input() maxDateToday = false;
  @Input() startEmpty = false;
  @Input() reset: Subject<boolean>;
  @Output() dateIni = new EventEmitter<any>();
  @Output() dateFin = new EventEmitter<any>();
  before: string;
  before2: string;

  onMouseMove() {
    this._autoLogoutService.resetTime();
  }

  onMouseClick(event: any) {
    if (event.target.offsetParent == null && (document.getElementsByTagName('ngb-modal-window').length > 0)
      && (event.target.localName !== 'svg') && (event.target.localName !== 'path')) {
        // TODO SetTimeOut and effects removed because delayed calendar close
        // document.getElementsByTagName('ngb-modal-window')[0].classList.add('fadeOut');
      // setTimeout(() => {
        this.modalService.dismissAll();
      // }, 0);
    }
  }

  isEscClose(event?: any) {
    if (event.key === 'Escape' && (document.getElementsByTagName('ngb-modal-window').length > 0)) {
      // TODO SetTimeOut and effects removed because delayed calendar close
      // document.getElementsByTagName('ngb-modal-window')[0].classList.add('fadeOut');
      // setTimeout(() => {
        this.modalService.dismissAll();
      // }, 0);
    }
  }

  closeModalDatepicker() {
    if ((document.getElementsByTagName('ngb-modal-window').length > 0)) {
      document.getElementsByTagName('ngb-modal-window')[0].classList.add('fadeOut');
      setTimeout(() => {
        this.modalService.dismissAll();
      }, 0);
    }
  }

  constructor(
    public _cdRef: ChangeDetectorRef,
    public calendar: NgbCalendar,
    private modalService: NgbModal,
    private _autoLogoutService: AutoLogoutService,
  ) {
  }

  ngOnInit() {
    if (this.maxDateToday) {
      const date = new Date();
      let thirtyDaysAgo = moment().subtract(30, 'days');
      // if (this.minOneYear) {
        thirtyDaysAgo = moment().subtract(4, 'year');
      // }
      this.minDate = { year: thirtyDaysAgo.year(), month: thirtyDaysAgo.month() + 1, day: thirtyDaysAgo.date() };
      this.maxDate = { year: date.getFullYear(), month: date.getMonth() + 1, day: date.getDate() };
    }
    this.years = [
      this.dateNow.getFullYear() - 3,
      this.dateNow.getFullYear() - 2,
      this.dateNow.getFullYear() - 1,
      this.dateNow.getFullYear(),
    ];
    if (!this.startEmpty) {
      this.fromDate = this.calendar.getPrev(this.calendar.getToday(), 'd', 3);
      this.toDate = this.calendar.getToday();
      this.valueI = new Date(this.fromDate.year, this.fromDate.month - 1, this.fromDate.day);
      this.valueF = new Date(this.toDate.year, this.toDate.month - 1, this.toDate.day);
      this.oldDateIni = this.fromDate;
      this.oldDateEnd = this.toDate;
      const valueI = new Date(this.fromDate.year, this.fromDate.month - 1, this.fromDate.day);
      const valueF = new Date(this.toDate.year, this.toDate.month - 1, this.toDate.day);
      this.stringContent1 = this.dayNames[valueI.getDay()] + ',' + valueI.getDate() + ' de ' + this.monthNames[valueI.getMonth()];
      this.stringContent2 = this.dayNames[valueF.getDay()] + ',' + valueF.getDate() + ' de ' + this.monthNames[valueF.getMonth()];
      this.dateIni.emit(this.valueI);
      this.dateFin.emit(this.valueF);
    }

    if (this.reset) {
      setTimeout(() => {
        this.reset.subscribe(v => {
          if (this.startEmpty) {
            this.fromDate = null;
            this.toDate = null;
          } else {
            this.fromDate = this.calendar.getPrev(this.calendar.getToday(), 'd', 3);
            this.toDate = this.calendar.getToday();
            this.valueI = new Date(this.fromDate.year, this.fromDate.month - 1, this.fromDate.day);
            this.valueF = new Date(this.toDate.year, this.toDate.month - 1, this.toDate.day);
            this.oldDateIni = this.valueI;
            this.oldDateEnd = this.valueF;
            this.stringContent1 = this.dayNames[this.valueI.getDay()] + ',' + this.valueI.getDate()
              + ' de ' + this.monthNames[this.valueI.getMonth()];
            this.stringContent2 = this.dayNames[this.valueF.getDay()] + ',' + this.valueF.getDate()
              + ' de ' + this.monthNames[this.valueF.getMonth()];
          }
        });
      }, 0);
    }
  }

  nextDayFrom() {
    const currentDate = new Date();
    const currentNgbDate = new NgbDate(currentDate.getFullYear(), currentDate.getMonth() + 1, currentDate.getDate());

    if (this.fromDate) {
      if (this.fromDate.equals(currentNgbDate) || this.fromDate.after(currentNgbDate)) {
        return;
      }
      this.fromDate = this.calendar.getNext(this.fromDate, 'd', 1);
      if (this.fromDate.after(this.toDate)) {
        this.toDate = this.fromDate;
      }
    }
  }

  previousDayFrom() {
    const previousDay = this.calendar.getPrev(this.fromDate, 'd', 1);
    if (this.fromDate) {
      if (previousDay.before(this.minDate)) {
        this.fromDate = this.minDate;
      } else {
        this.fromDate = previousDay;
      }
    }
  }

  nextDayTo() {
    const currentDate = new Date();
    const currentNgbDate = new NgbDate(currentDate.getFullYear(), currentDate.getMonth() + 1, currentDate.getDate());

    if (this.fromDate) {
      if (this.toDate.equals(currentNgbDate) || this.toDate.after(currentNgbDate)) {
        return;
      }
      if (this.toDate) {
        this.toDate = this.calendar.getNext(this.toDate, 'd', 1);
      } else {
        this.toDate = this.calendar.getNext(this.fromDate, 'd', 1);
      }
    }
  }

  previousDayTo() {
    if (this.fromDate) {
      if (this.toDate && !this.fromDate.equals(this.toDate)) {
        this.toDate = this.calendar.getPrev(this.toDate, 'd', 1);
      } else {
        this.toDate = this.fromDate = this.calendar.getPrev(this.fromDate, 'd', 1);
      }
    }
  }

  adjustViewWeekdays() {
    setTimeout(() => {
      const x = document.getElementsByClassName('ngb-dp-weekday');
      for (let i = 0; i < x.length; i++) {
        x[i].innerHTML = x[i].innerHTML.substr(0, 2).toLocaleUpperCase();
      }
    }, 0);
  }

  onDateSelection(date: NgbDate) {
    if (!this.fromDate && !this.toDate) {
      this.fromDate = date;
    } else if (this.fromDate && !this.toDate && date.after(this.fromDate)) {
      this.toDate = date;
      this.valueF = new Date(this.toDate.year, this.toDate.month - 1, this.toDate.day);
    } else {
      this.toDate = null;
      this.fromDate = date;
      this.valueI = new Date(this.fromDate.year, this.fromDate.month - 1, this.fromDate.day);
    }
  }

  adjustDateHeader(answer: string, isYear?) {
    setTimeout(() => {
      if (this.fromDate && this.toDate) {
        let aux = new NgbDate(Number(answer), this.fromDate.month, this.fromDate.day);
        this.fromDate = aux;
        if (isYear) {
          this.dateIniti = aux;
        }
        aux = null;
        aux = new NgbDate(Number(answer), this.toDate.month, this.toDate.day);
        this.toDate = aux;
        this.valueI = new Date(this.fromDate.year, this.fromDate.month - 1, this.fromDate.day);
        this.valueF = new Date(this.toDate.year, this.toDate.month - 1, this.toDate.day);
        this.stringContent1 = this.valueI;
        this.stringContent2 = this.valueF;
      }
    }, 0);
  }

  isHovered(date: NgbDate) {
    return this.fromDate && !this.toDate &&
      this.hoveredDate &&
      date.after(this.fromDate) &&
      date.before(this.calendar.getNext(this.hoveredDate, 'd', 1));
  }

  isInside(date: NgbDate) {
    return date.after(this.fromDate) && date.before(this.toDate);
  }

  isRange(date: NgbDate) {
    return date.equals(this.fromDate) || date.equals(this.toDate) || this.isInside(date) || this.isHovered(date);
  }

  redefine() {
    if (this.startEmpty) {
      this.fromDate = null;
      this.toDate = null;
      this.valueI = null;
      this.valueF = null;
      this.dateIni.emit(this.valueI);
      this.dateFin.emit(this.valueF);
    } else {
      this.fromDate = this.calendar.getPrev(this.calendar.getToday(), 'd', 3);
      this.toDate = this.calendar.getToday();
      this.valueI = new Date(this.fromDate.year, this.fromDate.month - 1, this.fromDate.day);
      this.valueF = new Date(this.toDate.year, this.toDate.month - 1, this.toDate.day);
      this.stringContent1 = this.valueI;
      this.stringContent2 = this.valueF;
      this.selected = this.calendar.getToday().year;
    }
  }

  cancelAndClose() {
    this.fromDate = this.oldDateIni;
    this.toDate = this.oldDateEnd;
    this.valueI = new Date(this.fromDate.year, this.fromDate.month - 1, this.fromDate.day);
    this.valueF = new Date(this.toDate.year, this.toDate.month - 1, this.toDate.day);
    this.dateIni.emit(this.valueI);
    this.dateFin.emit(this.valueF);
    this.modalService.dismissAll();
  }

  confirmAndCLose() {
    this.oldDateIni = this.fromDate;
    this.oldDateEnd = this.toDate;
    this.dateIniti = this.fromDate;
    this.dateIni.emit(this.valueI);
    this.dateFin.emit(this.valueF);
    this.modalService.dismissAll();
  }

  open(content) {
    this.modalService.open(content, {
      ariaLabelledBy: 'modal-basic-title', windowClass: 'datepicker-modal fadeIn animated',
      container: 'ngbd-datepicker-range', keyboard: false
    }).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
      if (this.fromDate) {
        this.valueI = new Date(this.fromDate.year, this.fromDate.month - 1, this.fromDate.day);
        if (this.toDate) {
          this.valueF = new Date(this.toDate.year, this.toDate.month - 1, this.toDate.day);
        } else {
          this.toDate = this.fromDate;
          this.valueF = new Date(this.fromDate.year, this.fromDate.month - 1, this.fromDate.day);
        }
      }
      this.dateIni.emit(this.valueI);
      this.dateFin.emit(this.valueF);
      if (this.fromDate) {
        this.stringContent1 = this.dayNames[this.valueI.getDay()] + ',' + this.valueI.getDate()
          + ' de ' + this.monthNames[this.valueI.getMonth()];
        this.stringContent2 = this.dayNames[this.valueF.getDay()] + ',' + this.valueF.getDate()
          + ' de ' + this.monthNames[this.valueF.getMonth()]
          ;
      } else {
        this.stringContent1 = '--/--/--';
        this.stringContent2 = '--/--/--';
      }
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

  public changeStatus(value) {
    setTimeout(x => {
      this.selected = value.next.year;
      // TODO date repeat correction
      // this.adjustDateHeader(value.next.year);
      this.selectedMonth = value.next.month;
    }, 0);
  }

  public checkMax(value: NgbDate) {
    if (this.maxDateToday) {
      if (value.after(this.maxDate) || value.before(this.minDate)) {
        return true;
      } else {
        return false;
      }
    }
    return false;
  }


  public toNumber(value) {
    return parseInt(value, 10);
  }

  newDateFrom(event) {
    const date = event.target.value.split('/');
    if (date.length === 3) {
      const dateConverted = (new Date(Number(date[2]), Number(date[1]) - 1, Number(date[0])));
      const dateNgbDate = (new NgbDate(Number(date[2]), Number(date[1]), Number(date[0])));

      if (dateConverted.getDate() === Number(date[0]) && dateConverted.getMonth() === Number(date[1]) - 1
        && (dateNgbDate.before(this.maxDate) || dateNgbDate.equals(this.maxDate))
        && (dateNgbDate.after(this.minDate) || dateNgbDate.equals(this.minDate))) {
        this.fromDate = dateNgbDate;
        this.valueI = new Date(this.fromDate.year, this.fromDate.month - 1, this.fromDate.day);
        this.valueF = new Date(this.toDate.year, this.toDate.month - 1, this.toDate.day);
      } else {
        event.target.value = (this.minDate.day < 10 ? '0' : '') + this.minDate.day + '/' +
          (this.minDate.month < 10 ? '0' : '') + this.minDate.month + '/' + this.minDate.year;
      }
    } else {
      event.target.value = (this.fromDate.day < 10 ? '0' : '') + this.fromDate.day + '/' +
        (this.fromDate.month < 10 ? '0' : '') + this.fromDate.month + '/' + this.fromDate.year;
    }
  }

  newDateTo(event) {
    const date = event.target.value.split('/');
    if (date.length === 3) {
      const dateConverted = (new Date(Number(date[2]), Number(date[1]) - 1, Number(date[0])));
      const dateNgbDate = (new NgbDate(Number(date[2]), Number(date[1]), Number(date[0])));

      if (dateConverted.getDate() === Number(date[0]) && dateConverted.getMonth() === Number(date[1]) - 1
        && (dateNgbDate.before(this.maxDate) || dateNgbDate.equals(this.maxDate))
        && (dateNgbDate.after(this.fromDate) || dateNgbDate.equals(this.fromDate))) {
        this.toDate = dateNgbDate;
        this.valueI = new Date(this.fromDate.year, this.fromDate.month - 1, this.fromDate.day);
        this.valueF = new Date(this.toDate.year, this.toDate.month - 1, this.toDate.day);
      } else {
        event.target.value = (this.toDate.day < 10 ? '0' : '') + this.toDate.day + '/' +
          (this.toDate.month < 10 ? '0' : '') + this.toDate.month + '/' + this.toDate.year;
      }
    } else {
      event.target.value = (this.toDate.day < 10 ? '0' : '') + this.toDate.day + '/' +
        (this.toDate.month < 10 ? '0' : '') + this.toDate.month + '/' + this.toDate.year;
    }
  }
}



