import { Component, OnInit, Input, ElementRef } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Component({
  selector: 'app-custom-snackbar',
  templateUrl: './custom-snackbar.component.html',
})
export class CustomSnackbarComponent implements OnInit {

  @Input()
  content: String;
  close: boolean;
  private _close = new BehaviorSubject<boolean>(false);
  close$ = this._close.asObservable();
  private _class = new BehaviorSubject<string>('success');
  class$ = this._class.asObservable();

  constructor(private element: ElementRef) {
  }

  ngOnInit() {
    this._class.subscribe(el => {
      this.element.nativeElement.children[0].classList.add(el);
    });
  }

  closeSnack() {
    this._close.next(true);
  }
}
