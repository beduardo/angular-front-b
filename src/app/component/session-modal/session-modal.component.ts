import { Component, OnInit, Inject, OnDestroy } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA, MatDialog } from '@angular/material';
import { environment } from 'environments/environment';
import { AutoLogoutService } from '@auth/auto-logout-service.service';

@Component({
  selector: 'app-session-modal',
  templateUrl: './session-modal.component.html'
})
export class SessionModalComponent implements OnInit, OnDestroy {
  intervalTimeout: any;
  timeout: number;
  public dialog: MatDialog;

  constructor(
    public dialogRef: MatDialogRef<SessionModalComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private autoLogoutService: AutoLogoutService,
    dialog: MatDialog
  ) {
    this.timeout = environment.SESSION_TIMEOUT_ALERT / 1000;
  }

  ngOnInit() {
    this.intervalTimeout = setInterval(() => {
      if (this.timeout > 0) {
        this.timeout = this.timeout - 1;
      } else {
        if (this.intervalTimeout) {
          clearInterval(this.intervalTimeout);
        }
        if (this.dialog) {
          this.dialog.closeAll();
        }
      }
    }, 1000);
  }

  ngOnDestroy() {
    if (this.intervalTimeout) {
      clearInterval(this.intervalTimeout);
    }
  }

  onClick() {
    if (this.intervalTimeout) {
      clearInterval(this.intervalTimeout);
    }
    this.autoLogoutService.resetTime();
    this.autoLogoutService._statusAlert.next(false);
    this.dialogRef.close();
  }

}
