import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material';

@Component({
  selector: 'app-youtube-live-modal',
  templateUrl: './youtube-live-modal.component.html'
})
export class YoutubeLiveModalComponent implements OnInit {

  constructor(public dialogRef: MatDialogRef<YoutubeLiveModalComponent>) { }

  ngOnInit() {
  }

  close() {
    this.dialogRef.close();
  }
}
