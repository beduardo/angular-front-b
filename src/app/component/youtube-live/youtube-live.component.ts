import { Component, OnInit, Input, Output, EventEmitter, OnDestroy } from '@angular/core';
import { YoutubeLiveService } from '@services/youtube-live.service';
import { Constants } from '@utils/constants';

@Component({
  selector: 'app-youtube-live',
  templateUrl: './youtube-live.component.html'
})
export class YoutubeLiveComponent implements OnInit, OnDestroy {

  @Input() fromModal = false;
  @Output() emitClose = new EventEmitter();
  public urlVideo = '';
  public urlChat = '';
  public title = 'AO VIVO';
  public livePlaying = false;
  public intervalFastLive: any;

  constructor(private youtubeService: YoutubeLiveService) { }

  ngOnInit() {
    this.youtubeService.getFastLive().subscribe(resp => {
      this.getLive(resp);
    });
    this.intervalFastLive = setInterval(() => {
      this.youtubeService.getFastLive().subscribe(resp => {
        if (!this.livePlaying) {
          this.getLive(resp);
        }
      });
    }, Constants.SEARCH_VIDEO_INTERVAL);
  }

  ngOnDestroy(){
    if (this.intervalFastLive) {
      clearInterval(this.intervalFastLive);
    }
  }

  getLive(resp: any) {
    if (resp) {
      const arr: Array<any> = Array.from(resp.items);
      if (arr && arr.length > 0) {
        this.urlVideo = Constants.VIDEO_URL + arr[0].id.videoId + Constants.VIDEO_PARAMS;
        this.urlChat = Constants.CHAT_URL + arr[0].id.videoId + Constants.CHAT_PARAMS;
        this.livePlaying = true;
      }
    }
  }

  openWindow() {
    this.closeModal();
    window.open(
      'live',
      '_blank',
      'width=1300px, height=768px'
    );
  }

  closeModal() {
    this.emitClose.emit();
  }
}
