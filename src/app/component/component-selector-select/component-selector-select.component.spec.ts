import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ComponentSelectorSelectComponent } from '@component/component-selector-select/component-selector-select.component';

describe('ComponentSelectorSelectComponent', () => {
  let component: ComponentSelectorSelectComponent;
  let fixture: ComponentFixture<ComponentSelectorSelectComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ComponentSelectorSelectComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ComponentSelectorSelectComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
