import { Component, OnInit, Input, Output, EventEmitter, ElementRef, ViewChild, Renderer2 } from '@angular/core';
import { MessageQuoteQuery } from '@model/webFeeder/message-quote-query.model';
import { QuoteService } from '@services/webFeeder/sckt-quotes.service';
import { Constants } from '@utils/constants';
import { Subscription } from 'rxjs/Rx';
import { WorkspaceNewService } from '@services/workspace-new.service';
import { CustomWorkspaceItemMultipleDto } from '@dto/custom-workspace-item-multiple.dto';
import { NotificationsService } from 'angular2-notifications';
import { AuthServer } from '@services/auth-server.service';
import { MessageQuote } from '@model/webFeeder/message-quote.model';
import { AppUtils } from '@utils/app.utils';

@Component({
  selector: 'app-component-selector-select',
  templateUrl: './component-selector-select.component.html',
})
export class ComponentSelectorSelectComponent implements OnInit {

  @ViewChild('css') currentComponent: ElementRef;
  public button_selected = false;
  public loading = false;
  public assetsWorksheet: any;
  public company: any;
  public lastEventList = [];

  @Input() cardTitle: any;
  @Output() addMultiple = new EventEmitter();
  private removeCardTemp: Subscription;
  private subscriptionQuote: Subscription;
  public model;

  @Input() symbolAutoComplete: string;
  @Input() hiddenError: boolean;
  @Output() onsymbolChanged = new EventEmitter<MessageQuoteQuery>();
  public hiddenInvalid: boolean;
  public filteredAssets: any;
  public auxFilteredAssets = [];
  public auxFilteredTooltipAssets = '';

  constructor(
    private renderer: Renderer2,
    private quoteService: QuoteService,
    private _workspaceService: WorkspaceNewService,
    private _notificationService: NotificationsService,
    private _authServer: AuthServer,
  ) { }

  ngOnInit() {
    this.model = new CustomWorkspaceItemMultipleDto(null, this.cardTitle.id, [], [], '', false);
    this.removeCardTemp = this._workspaceService.removeCard$.subscribe(x => {
      if (this.model.type) {
        if (x.type === this.model.type) {
          this.model.assetSymbol = [];
          this.model.company = [];
          this.assetsWorksheet = [];
          this.auxFilteredAssets = [];
        }
      }
    });
  }

  changeClass() {
    this.button_selected = !this.button_selected;
  }

  addComponent() {
    this.loading = true;
    this.assetsWorksheet = this.auxFilteredAssets;
    if (this.assetsWorksheet.length < 0) {
      this._notificationService.warn('Ops,', Constants.WORKSPACE_MSG006);
      this.loading = false;
    } else {
      this.changeClass();
      this.cardEffect(this.currentComponent);
      setTimeout(() => {
        this.changeClass();
        this.assetsWorksheet.type = this.cardTitle.id;
        this.addMultiple.emit(this.assetsWorksheet);
        this.loading = false;
      }, 500);
    }
    this.auxFilteredAssets = [];
    this.filteredAssets = [];
    this.auxFilteredTooltipAssets = '';
  }

  readjustHeight() {
    if (this.auxFilteredAssets.length > 0) {
      if (document.getElementsByClassName('mat-select-panel').length > 0) {
        const elem = document.getElementsByClassName('mat-select-panel').item(0).classList.add('mat-select-selector-select');
      }
    }
  }

  public onAutoCompletAsset(textInput: any) {
    this.hiddenInvalid = true;
    if (textInput.key !== 'ArrowDown' && textInput.key !== 'ArrowUp') {
      let symbolAutoComplete = '';
      if (textInput.target === undefined) {
        symbolAutoComplete = textInput.toLowerCase();
      } else {
        symbolAutoComplete = textInput.target.value.toLowerCase();
      }
      if (symbolAutoComplete) {
        this._authServer.iQuote.quotesQuery(symbolAutoComplete)
          .subscribe(datas => {
            if (datas && datas.length > 0) {
              this.filteredAssets = datas;
            }
          });
      }
      if (this.filteredAssets) {
        if (symbolAutoComplete.length >= 5) {
          if (this.filteredAssets.length === 0) {
            this.hiddenInvalid = false;
          } else {
            this.hiddenInvalid = true;
          }
        }
      }

      if (this.filteredAssets === undefined || this.filteredAssets.length === 0) {
        this.company = '';
      }

      return this.filteredAssets;
    }
  }

  onInputEnter(event) {
    let symbol = '';
    if (event.target === undefined) {
      symbol = event;
    } else {
      symbol = event.target.value;
    }

    if (this.subscriptionQuote) {
      this.subscriptionQuote.unsubscribe();
    }
    this.quoteService.subscribeQuote(symbol.toLowerCase(), Constants.WINDOW_NAME_SELECTOR_SELECT);
    this.subscriptionQuote = this.quoteService.messagesQuote.subscribe(
      res => {
        if (res && !res.errorMessage && res.symbol.toLowerCase() === symbol.toLowerCase()) {
          const msgQuote: MessageQuoteQuery = new MessageQuoteQuery();
          msgQuote.symbol = res.symbol.toLocaleUpperCase();
          msgQuote.company = res.description.toLocaleUpperCase();
          if (msgQuote.symbol) {
            if (this.auxFilteredAssets.filter(item =>
              item.symbol === msgQuote.symbol).length === 0) {
              this.auxFilteredAssets.push(msgQuote);
              this.auxFilteredTooltipAssets = '';
              for (let i = 0; i < this.auxFilteredAssets.length; i++) {
                if (i === 0) {
                  this.auxFilteredTooltipAssets += this.auxFilteredAssets[i].symbol;
                } else {
                  this.auxFilteredTooltipAssets += ', ' + this.auxFilteredAssets[i].symbol;
                }
              }
            }
          } else {
            this.auxFilteredAssets = this.auxFilteredAssets.filter(item =>
              item.symbol !== symbol);
            this.auxFilteredTooltipAssets = '';
            for (let i = 0; i < this.auxFilteredAssets.length; i++) {
              if (i === 0) {
                this.auxFilteredTooltipAssets += this.auxFilteredAssets[i].symbol;
              } else {
                this.auxFilteredTooltipAssets += ', ' + this.auxFilteredAssets[i].symbol;
              }
            }
          }
        }
        this.subscriptionQuote.unsubscribe();
      });
  }

  onchangeSymbol() {
    let symbol = '';
    let quote;
    const auxFilteredAssets = this.auxFilteredAssets.filter(x => {
      if (this.filteredAssets.filter(asset => asset.symbol === x.symbol).length > 0) {
        return true;
      } else {
        return false;
      }
    });
    const isSelecting = this.itemAddedSelect(auxFilteredAssets);
    if (isSelecting) {
      if (!auxFilteredAssets && auxFilteredAssets.length > 0) {
        quote = this.assetsWorksheet[0];
      } else {
        quote = this.assetsWorksheet.filter(x => {
          if (auxFilteredAssets.filter(asset => asset.symbol === x.symbol).length > 0) {
            return false;
          } else {
            return true;
          }
        })[0];
      }
    } else {
      if (!this.assetsWorksheet) {
        quote = auxFilteredAssets[0];
      } else {
        quote = auxFilteredAssets.filter(x => {
          if (this.assetsWorksheet.filter(asset => asset.symbol === x.symbol).length > 0) {
            return false;
          } else {
            return true;
          }
        })[0];
      }
    }
    symbol = quote.symbol;

    if (isSelecting) {
      // Checks the max number of quotes.
      if (this.auxFilteredAssets && this.auxFilteredAssets.length >= Constants.QTD_MAX_ASSETS_WORKSHEET) {
        setTimeout(() => {

          this.assetsWorksheet = this.auxFilteredAssets;
        }, 100);
        this._notificationService.warn('', Constants.WORKSHEET_MAX_ASSETS_MSG);
        return false;
      }

      // Checks if the quote is valid.
      this.quoteService.subscribeQuote(symbol, Constants.WINDOW_NAME_SELECTOR_SELECT);
      const quoteSubscribe = this.quoteService.messagesQuote.subscribe((data: MessageQuote) => {
        if (data && data.errorMessage && data.symbol.toLowerCase() === symbol.toLowerCase()) {
          this.quoteService.unsubscribeQuote(symbol.toLowerCase(), Constants.WINDOW_NAME_SELECTOR_SELECT);
          this.assetsWorksheet = this.auxFilteredAssets;
          this._notificationService.warn('', Constants.WORKSPACE_MSG007);
          quoteSubscribe.unsubscribe();
          return false;

        } else if (data !== undefined && data.symbol != null && data.symbol.toLowerCase() === symbol.toLowerCase()) {
          if (AppUtils.checksUnsubscribeQuote(symbol, this._workspaceService)) {
            this.quoteService.unsubscribeQuote(symbol.toLowerCase(), Constants.WINDOW_NAME_SELECTOR_SELECT);
          }

          if (this.auxFilteredAssets.filter(item =>
            item.symbol === symbol).length === 0) {
            this.auxFilteredAssets.push(quote);
            this.auxFilteredTooltipAssets = '';
            for (let i = 0; i < this.auxFilteredAssets.length; i++) {
              if (i === 0) {
                this.auxFilteredTooltipAssets += this.auxFilteredAssets[i].symbol;
              } else {
                this.auxFilteredTooltipAssets += ', ' + this.auxFilteredAssets[i].symbol;
              }
            }
          }

          setTimeout(() => {
            this.assetsWorksheet = this.auxFilteredAssets;
          }, 100);
          quoteSubscribe.unsubscribe();
          return true;
        }
      });
    } else {
      this.auxFilteredAssets = this.auxFilteredAssets.filter(item =>
        item.symbol !== symbol);
      this.auxFilteredTooltipAssets = '';
      for (let i = 0; i < this.auxFilteredAssets.length; i++) {
        if (i === 0) {
          this.auxFilteredTooltipAssets += this.auxFilteredAssets[i].symbol;
        } else {
          this.auxFilteredTooltipAssets += ', ' + this.auxFilteredAssets[i].symbol;
        }
      }

      setTimeout(() => {
        this.assetsWorksheet = this.auxFilteredAssets;
      }, 100);
    }
  }

  itemAddedSelect(auxFilteredAssets) {
    if (auxFilteredAssets && !this.assetsWorksheet) {
      return false;
    }

    if (!auxFilteredAssets && this.assetsWorksheet) {
      return true;
    }

    if (auxFilteredAssets && this.assetsWorksheet) {
      if (auxFilteredAssets.length > this.assetsWorksheet.length) {
        return false;
      }

      if (auxFilteredAssets.length < this.assetsWorksheet.length) {
        return true;
      }
    }
  }

  clickSelect() {
    setTimeout(() => {
      document.getElementById('inputSelect').focus();
    }, 200);

  }
  compareFn(item1: any, item2: any) {
    return item1 && item2 ? item1.symbol === item2.symbol : false;
  }

  cardEffect(cardRef: ElementRef): void {
    const elementLeft = cardRef.nativeElement.offsetLeft;
    const windowWidth = window.innerWidth;
    const elementTop = cardRef.nativeElement.offsetTop;
    const verticalFinalPosition = windowWidth - elementLeft - 315;
    const horizontalFinalPosition = window.innerHeight / 2 - 300;

    this.renderer.setStyle(cardRef.nativeElement, 'right', '-' + verticalFinalPosition + 'px');

    if (elementTop > window.innerHeight / 2) {
      this.renderer.setStyle(cardRef.nativeElement, 'top', '-' + horizontalFinalPosition + 'px');
    } else {
      this.renderer.setStyle(cardRef.nativeElement, 'top', horizontalFinalPosition + 'px');
    }

    setTimeout(() => {
      this.renderer.setStyle(cardRef.nativeElement, 'top', '0px');
      this.renderer.setStyle(cardRef.nativeElement, 'right', '0px');
    }, 500);
  }
}
