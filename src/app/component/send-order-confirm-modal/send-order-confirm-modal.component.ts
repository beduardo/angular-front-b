import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';

@Component({
  selector: 'app-send-order-confirm-modal',
  templateUrl: './send-order-confirm-modal.component.html'
})
export class SendOrderConfirmModalComponent implements OnInit {

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    public dialogRef: MatDialogRef<SendOrderConfirmModalComponent>,
  ) { }

  ngOnInit() {
  }

}
