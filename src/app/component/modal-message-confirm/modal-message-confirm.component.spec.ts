import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ModalMessageConfirmComponent } from '@component/modal-message-confirm/modal-message-confirm.component';
import { NgModule } from '@angular/core';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { HttpClient } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatDialogModule, MAT_DIALOG_DATA } from '@angular/material';


@NgModule({
  declarations: [ModalMessageConfirmComponent],
  entryComponents: [ModalMessageConfirmComponent],
  exports: [ModalMessageConfirmComponent],
  imports: [
    // BrowserAnimationsModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: null,
        deps: [HttpClient]
      }
    }),
    MatDialogModule
  ]
})
class TestModule { }


describe('ModalMessageConfirmComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [TestModule],
      providers: [
        { provide: MAT_DIALOG_DATA, useValue: {}}
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
  });

  it('should be created', () => {
  });
});
