import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';

@Component({
  selector: 'app-modal-message-confirm',
  templateUrl: './modal-message-confirm.component.html'
})
export class ModalMessageConfirmComponent implements OnInit {
  constructor(
    public dialogRef: MatDialogRef<ModalMessageConfirmComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
    data.okey = true;
    data.nao = false;
  }

  ngOnInit() { }

  keyDownPressed(event) {
    if (event.key === 'Escape') {
      this.dialogRef.close();
    }
  }
}
