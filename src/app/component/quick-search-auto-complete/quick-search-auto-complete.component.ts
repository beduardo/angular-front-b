import { AfterViewInit, Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { MessageQuoteQuery } from '@model/webFeeder/message-quote-query.model';
import { QuoteService } from '@services/webFeeder/sckt-quotes.service';
import { NotificationsService } from 'angular2-notifications';
import { Constants } from '@utils/constants';
import { MessageQuote } from '@model/webFeeder/message-quote.model';
import { Subscription } from 'rxjs';
import { AuthServer } from '@services/auth-server.service';

@Component({
  selector: 'app-quick-search-auto-complete',
  templateUrl: './quick-search-auto-complete.component.html',
})
export class QuickSearchAutoCompleteComponent implements OnInit, AfterViewInit {
  @Input() symbolAutoComplete: string;
  @Output() onsymbolChanged = new EventEmitter<MessageQuoteQuery>();
  @Input() hiddenError: boolean;
  @Input() autoFocus: boolean;
  @Output() setActiveValid = new EventEmitter<boolean>();
  @Output() focusOut: EventEmitter<boolean>;

  public assetValid = false;
  public filteredAssets: MessageQuoteQuery[];
  public company: string;
  public hiddenInvalid = true;
  public focus: boolean;
  public subscriptionQuote: Subscription;
  constructor(
    private quoteService: QuoteService,
    private _authServer: AuthServer,
    private _notificationService?: NotificationsService,
  ) {
    this.symbolAutoComplete = '';
    this.hiddenError = true;
    this.focus = false;
    this.focusOut = new EventEmitter<boolean>(false);
  }

  ngOnInit() {
    if (this.symbolAutoComplete) {
      this._authServer.iQuote.quotesQuery(this.symbolAutoComplete)
        .subscribe(datas => {
          if (datas) {
            this.symbolAutoComplete = this.symbolAutoComplete.toUpperCase();
            if (datas.length > 0) {
              this.filteredAssets = datas;
              this.SetCompany(this.symbolAutoComplete);
            }
          }
        });
    }
  }

  ngAfterViewInit() {
    if (this.autoFocus && document.getElementById('input-graph')) {
      document.getElementById('input-graph').getElementsByTagName('input')[0].focus();
    }
  }
  public clearInput() {
    this.symbolAutoComplete = '';
    this.company = '';
  }

  public onAutoCompletAsset(textInput: any) {
    this.hiddenInvalid = true;
    if (textInput.key !== 'ArrowDown' && textInput.key !== 'ArrowUp') {
      if (textInput.target.value.length < 2) {
        this.filteredAssets = [];
        return [];
      }
      let symbolAutoComplete = '';
      if (textInput.target === undefined) {
        symbolAutoComplete = textInput.toLowerCase();
      } else {
        symbolAutoComplete = textInput.target.value.toLowerCase();
      }
      if (symbolAutoComplete) {
        this._authServer.iQuote.quotesQuery(symbolAutoComplete)
          .subscribe(datas => {
            if (datas && datas.length > 0) {
              this.filteredAssets = datas;
            }
          });
      }
      if (symbolAutoComplete.length >= 5) {
        if (this.filteredAssets !== undefined && this.filteredAssets.length === 0) {
          this.hiddenInvalid = false;
        } else {
          this.hiddenInvalid = true;
        }
      }
      if (this.filteredAssets === undefined || this.filteredAssets.length === 0) {
        this.company = '';
      }
      return this.filteredAssets;
    }
  }

  onchangeSymbol(evento) {
    this.symbolAutoComplete = '';
    const messageQuoteQuery = new MessageQuoteQuery();
    if (evento.source !== undefined) {
      this.symbolAutoComplete = evento.source.value.toUpperCase();
      this.SetCompany(this.symbolAutoComplete.toUpperCase());
      this.filteredAssets = [];
      messageQuoteQuery.symbol = this.symbolAutoComplete.toUpperCase();
      this.onsymbolChanged.emit(messageQuoteQuery);
    } else if (evento.target !== undefined) {
      this.symbolAutoComplete = evento.target.value.toUpperCase();
      if (this.filteredAssets.findIndex(s => s.symbol.toUpperCase() === this.symbolAutoComplete) === -1) {
        this.symbolAutoComplete = '';
      }
      this.SetCompany(this.symbolAutoComplete.toUpperCase());
      this.filteredAssets = [];
      messageQuoteQuery.symbol = this.symbolAutoComplete.toUpperCase();
      this.onsymbolChanged.emit(messageQuoteQuery);
    } else if (evento.value !== undefined && evento.value !== '') {
      this.symbolAutoComplete = evento.value.toUpperCase();
      if (this.filteredAssets.findIndex(s => s.symbol.toUpperCase() === this.symbolAutoComplete) !== -1) {
        this.SetCompany(this.symbolAutoComplete.toUpperCase());
        this.filteredAssets = [];
        messageQuoteQuery.symbol = this.symbolAutoComplete.toUpperCase();
        this.onsymbolChanged.emit(messageQuoteQuery);
      }
    }
  }

  private SetCompany(symbolAutoComplete: string) {
    if (!this.filteredAssets && this.filteredAssets.length === 0) {
      return;
    }
    const filtered = this.filteredAssets.find(s => s.symbol.toUpperCase() === symbolAutoComplete);
    if (filtered) {
      this.company = filtered.company;
    }
  }

  private EmitOnSymbolChanged(symbolAutoComplete: string) {
    const messageQuoteQuery = new MessageQuoteQuery();
    messageQuoteQuery.company = this.company;
    this.onsymbolChanged.emit(messageQuoteQuery);
  }

  onInputEnter(event) {
    const symbol = event.target.value.toLowerCase();
    if (event.target.value === '' && event.key.includes('Enter')) {
      this._notificationService.warn('Ops,', Constants.WORKSPACE_MSG005);
      return;
    }
    if (this.subscriptionQuote) {
      this.subscriptionQuote.unsubscribe();
    }
    this.quoteService.subscribeQuote(symbol.toLowerCase(), Constants.WINDOW_NAME_QUICK_SEARCH_AUTO_COMPLETE);
    const messageQuoteQuery = new MessageQuoteQuery();
    this.subscriptionQuote = this.quoteService.messagesQuote
      .subscribe((data: MessageQuote) => {
        if (data && !data.errorMessage && data.symbol.toLowerCase() === symbol.toLowerCase()) {
          this.quoteService.unsubscribeQuote(symbol.toLowerCase(), Constants.WINDOW_NAME_QUICK_SEARCH_AUTO_COMPLETE);
          this.filteredAssets = [];
          messageQuoteQuery.symbol = data.symbol;
          messageQuoteQuery.company = data.description;
          this.onsymbolChanged.emit(messageQuoteQuery);
          this.subscriptionQuote.unsubscribe();
        }
      });
  }

  switchFocus() {
    if (this.focus) {
      this.focus = false;
    } else {
      this.focus = true;
    }
  }

  inputShrink() {
    this.focusOut.emit(true);
  }
}
