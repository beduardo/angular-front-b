import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { QuickSearchAutoCompleteComponent } from '@component/quick-search-auto-complete/quick-search-auto-complete.component';

describe('QuickSearchAutoCompleteComponent', () => {
  let component: QuickSearchAutoCompleteComponent;
  let fixture: ComponentFixture<QuickSearchAutoCompleteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ QuickSearchAutoCompleteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(QuickSearchAutoCompleteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
