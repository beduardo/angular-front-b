import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-centered-modal',
  templateUrl: './centered-modal.component.html',
})
export class CenteredModalComponent implements OnInit {

  constructor(public dialogRef: MatDialogRef<CenteredModalComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any) { }

  closeClick(): void {
    this.dialogRef.close();
  }

  ngOnInit() {
  }
}
