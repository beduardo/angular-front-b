import { ConfigService } from './../../services/webFeeder/config.service';
import { Component, OnInit, Input, ViewChild, ViewContainerRef, AfterViewInit, Output, EventEmitter } from '@angular/core';
import { ICellEditorAngularComp } from 'ag-grid-angular';
import { Subject } from 'rxjs';
import { NotificationsService } from 'angular2-notifications';
import { Constants } from '@utils/constants';

@Component({
  selector: 'app-auto-complete-assessor',
  templateUrl: './auto-complete-assessor.component.html'
})
export class AutoCompleteAssessorComponent implements OnInit, AfterViewInit, ICellEditorAngularComp {


  @Input() autoSizeActive = false;
  @Input() nameAutoComplete: string;
  @Input() autoFocus: boolean;
  @Input() reset: Subject<boolean>;

  @ViewChild('autoInput', { read: ViewContainerRef }) public input;

  @Output() hiddenInvalidOutput = new EventEmitter<Boolean>();
  @Output() rawInformation = new EventEmitter<String>();
  @Output() onsymbolChanged = new EventEmitter<String>();

  public filteredAssets: any[];
  public hiddenInvalid = true;
  public isWorksheet = false;
  public listAssessorClient: any;
  private inputQuotePattern = /^[a-z0-9]+$/i;
  private params: any;
  private selectedFlag = false;

  constructor(
    private _configService: ConfigService,
    private _notificationService?: NotificationsService,
  ) {
    this.listAssessorClient = this._configService.getAssessorListClients();
    this.nameAutoComplete = '';
  }

  ngOnInit() {
    this.nameAutoComplete = '';
    if (this.reset) {
      setTimeout(() => {
        this.reset.subscribe(v => {
          this.hiddenInvalid = true;
        });
      }, 0);
    }
  }

  ngAfterViewInit() {
    if (this.autoFocus) {
      setTimeout(() => {
        this.focusInput();
      });
    }
  }

  getValue(): any {
    return this.nameAutoComplete;
  }

  focusInput() {
    this.input.element.nativeElement.focus();
  }

  onInputEnter(event) {
    setTimeout(() => {
      if (!this.selectedFlag) {
        let nameSearch = '';
        let symbol = '';
        if (event.target.value) {
          symbol = event.target.value.toLowerCase();
        } else {
          symbol = event;
        }
        if (event.target.value === '' && event.key.includes('Enter')) {
          this._notificationService.warn('', Constants.WORKSPACE_MSG019);
          return;
        }
        if (symbol.length > 0 && symbol.length < 2) {
          this.filteredAssets = [];
          this.hiddenInvalid = false;
          this.hiddenInvalidOutput.emit(this.hiddenInvalid);
          nameSearch = symbol;
          this.onsymbolChanged.emit(nameSearch);
          if (this.params) {
            this.params.api.stopEditing();
          }
          return;
        }
      } else {
        this.selectedFlag = false;
      }
    }, 200);
  }

  onAutoCompletAsset(textInput: any) {
    this.hiddenInvalid = true;
    this.hiddenInvalidOutput.emit(this.hiddenInvalid);
    if (textInput.key !== 'ArrowDown' && textInput.key !== 'ArrowUp' && textInput.key !== 'Enter') {
      if (textInput.target === undefined) {
        this.nameAutoComplete = textInput.toLowerCase();
      } else {
        this.nameAutoComplete = textInput.target.value.toLowerCase();
      }
      this.rawInformation.emit(this.nameAutoComplete);
      this.filteredAssets = this.listAssessorClient.filter(state => state.full_name.toLowerCase().indexOf(this.nameAutoComplete) === 0);
      return this.filteredAssets;
    }
  }

  keyPressed(event: any) {
    const inputChar = String.fromCharCode(event.charCode);
    if (event.charCode !== 45) {
      if (!this.inputQuotePattern.test(inputChar)) {
        event.preventDefault();
      }
    }
  }

  optionSelected(event) {
    this.selectedFlag = true;
    const name = event.option.value.toLowerCase();
    this.nameAutoComplete = name;
    this.filteredAssets = [];
    this.onsymbolChanged.emit(this.nameAutoComplete);
    this.hiddenInvalid = true;
    this.hiddenInvalidOutput.emit(this.hiddenInvalid);
    if (this.params) {
      this.params.api.stopEditing();
    }
  }

  onPaste(event) {
    this.keyPressed(event);
    const content = event.clipboardData.getData('text/plain');
    if (!this.inputQuotePattern.test(content)) {
      event.preventDefault();
    }
  }

  callBackWorksheet() {
    if (this.isWorksheet) {
      this.params.api.stopEditing();
    }
  }

  agInit(params: any): void {
    this.params = params;
    this.nameAutoComplete = this.params.value;
    this.isWorksheet = true;
    setTimeout(() => {
      this.input.element.nativeElement.focus();
    });
  }

}
