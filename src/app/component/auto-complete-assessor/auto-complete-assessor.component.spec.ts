import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AutoCompleteAssessorComponent } from './auto-complete-assessor.component';

describe('AutoCompleteAssessorComponent', () => {
  let component: AutoCompleteAssessorComponent;
  let fixture: ComponentFixture<AutoCompleteAssessorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AutoCompleteAssessorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AutoCompleteAssessorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
