import { ModalSignatureComponent } from './../../modal-signature/modal-signature.component';
import { AuthenticationService } from '@services/authentication.service';
import { AuthServer } from './../../services/auth-server.service';
import {
  ChangeDetectorRef, Component, EventEmitter, HostListener,
  Inject, Input, OnDestroy, OnInit, Optional, Output, ViewChild, AfterViewInit, ElementRef
} from '@angular/core';
import { MatBottomSheetRef, MatDialog, MAT_BOTTOM_SHEET_DATA, MatTooltip } from '@angular/material';
import { Router } from '@angular/router';
import { AuthService } from '@auth/auth.service';
import { AutoLogoutService } from '@auth/auto-logout-service.service';
import { SendOrderConfirmModalComponent } from '@component/send-order-confirm-modal/send-order-confirm-modal.component';
import { SideEnum } from '@model/enum/side.enum';
import { TypeOrderEnum } from '@model/enum/type-order.enum';
import { SimpleOrder } from '@model/simpleOrder';
import { MessageQuoteQuery } from '@model/webFeeder/message-quote-query.model';
import { MessageQuote } from '@model/webFeeder/message-quote.model';
import { ConfigService } from '@services/webFeeder/config.service';
import { QuoteService } from '@services/webFeeder/sckt-quotes.service';
import { AppUtils } from '@utils/app.utils';
import { Constants } from '@utils/constants';
import { NotificationsService } from 'angular2-notifications';
import { Subscription } from 'rxjs';
import { FinancialService } from '@services/webFeeder/sckt-financial.service';
import { BrokerAccount } from '@model/broker-account.model';
import { environment } from 'environments/environment';
import { StatusOmsService } from '@services/webFeeder/sckt-status-oms.service';
import { StatusConnection } from '@model/status-connection';

@Component({
  selector: 'app-buy-sell-modal',
  templateUrl: './buy-sell-modal.component.html'
})
export class BuySellModalComponent implements OnInit, OnDestroy {

  public symbol = null;
  public type = Constants.ORDER_SIDE_BUY;
  public flagOptions = false;
  public stopLoss = true;
  public stopGain = true;
  public isStop = false;
  public fullFlag = true;
  public fractFlag = true;
  public messageInvalidAsset = Constants.BILLET_ORDER_MSG001;
  public messageStopLoss: string;
  public messageStopGain: string;
  public fullFlagLoss = true;
  public fractFlagLoss = true;
  public fullFlagGain = true;
  public fractFlagGain = true;
  public fractionaryQtd: number;
  public fractionary: number;
  public nonFractionary: number;
  public nonFractionaryQtd: number;
  public fractionaryQtdLoss: number;
  public fractionaryLoss: number;
  public nonFractionaryLoss: number;
  public nonFractionaryQtdLoss: number;
  public fractionaryQtdGain: number;
  public fractionaryGain: number;
  public nonFractionaryGain: number;
  public nonFractionaryQtdGain: number;
  public totalDisplay = 0;
  public totalDisplayLoss = 0;
  public totalDisplayGain = 0;
  public indiceFuture: boolean;
  public accountOptions = [];
  public typesOptions = [
    { id: TypeOrderEnum.LIMITED, text: Constants.TYPE_ORDER_TEXTS.Limited },
    { id: TypeOrderEnum.MARKETPROTECTED, text: Constants.TYPE_ORDER_TEXTS.MarketProtected },
    { id: TypeOrderEnum.MARKET, text: Constants.TYPE_ORDER_TEXTS.Market },
  ];
  public strategyOptions = [
    { id: Constants.OPEN_DAY_TRADE, text: 'Daytrade' },
    { id: Constants.OPEN_POSITION_TRADE, text: 'Position' }
  ];
  public timeInForcesOptions = Constants.TIME_IN_FORCES;
  public coin = Constants.CURRENCY_BRAZILIAN_SYMBOL;
  public loteSizeStep?: string;
  public company = '';
  public minimumAmount: number;
  public lastTrade: number;
  public variation: string;
  public variationNumber: number;
  public ask: number;
  public bid: number;
  public quoteState: number;
  public quoteStateText: string;
  public priceTheory: number;
  public quotationForm: number;
  public contractMultiplier: number;
  public minimumIncrement: string;
  public tunnelUpperLimit: number;
  public tunnelLowerLimit: number;
  public inTheWeek: number;
  public tickSize = Constants.BILLET_TICKSIZE_DEFAULT;
  public selectedTab = 1;
  public minDate: Date = new Date();
  public minDate2: Date = new Date((new Date()).getFullYear(), (new Date()).getMonth(), (new Date()).getDate());
  public isBuy: boolean;
  public qtdValid = true;
  private inputQuotePattern = /^[a-z0-9]+$/i;
  public lossTrigger = true;
  public lossLimit = true;
  public gainTrigger = true;
  public gainLimit = true;
  public loadingConfirmation: boolean;
  public preventDoubleClick = false;
  dialog: MatDialog;
  @Output() onsymbolChanged = new EventEmitter<MessageQuoteQuery>();
  @Output() animationDone: EventEmitter<void>;
  @ViewChild('inputQuote') inputQuoteEl: ElementRef;
  @ViewChild('inputQuoteStop') inputQuoteStopEl: ElementRef;
  public hiddenInvalid = true;
  public hiddenInvalidActive = false;
  public filteredAssets: any;
  public loading = true;
  public loadingInit = true;
  public loadingEmitOrder = false;
  public options = '';
  public initialPrice = false;
  public item: any;
  public confirmModalOpened = false;
  public priceLimitValuesFormatOptions = {
    prefix: '',
    thousands: Constants.THOUSANDS_SEPARATOR,
    decimal: Constants.DECIMALS_SEPARATOR,
    allowNegative: true,
    precision: this.tickSize,
    align: 'right'
  };
  public priceLimitValuesFormatOptionsFutureIndex = {
    prefix: '',
    thousands: Constants.THOUSANDS_SEPARATOR,
    decimal: Constants.DECIMALS_SEPARATOR,
    allowNegative: false,
    precision: 0,
    align: 'right'
  };
  public _symbolName: string;
  public _symbolNameLast: string;
  private _subscribed: boolean;
  private _subscription: Subscription;
  private _closedModalBuySell: Subscription;
  public messageQuote: MessageQuote = new MessageQuote();
  public model: SimpleOrder = new SimpleOrder();
  public configBilletClosed: SimpleOrder = new SimpleOrder();
  public clickConfirmation: boolean;
  public subscriptionMyBalance: Subscription;
  public limiteOperacional = '';
  public saveSignature: boolean;
  public validSignature: boolean;
  public isLoginW = this._configService.isLoginTypeWSO2();
  public isAssessor: boolean;
  public isRequest: boolean;
  public userName = '';
  public termOperator: boolean;
  public checkTermOperator: boolean;
  public accountSelectBroker: BrokerAccount = new BrokerAccount();
  public accountSelectBrokerAssessor: any;
  public whiteLabel = environment.WHITELABEL;
  public accounBrokerList: any;
  public setIntervalAccount: any;
  public acccountBalanceSubscribe: string;
  titleMessageorder = 'Ordem recebida com sucesso';
  messageorder = 'Para acompanhar sua(s) ordem(ns) clique aqui';
  disableOptCenter = true;
  private subscriptionStatusOms: Subscription;
  public statusOmsOnline: boolean;
  public msgStatusOmsOffile = Constants.MSG_STATUS_OFFLINE_OMS;
  @HostListener('mousemove')
  onMouseMove() {
    this._autoLogoutService.resetTime();
  }
  @HostListener('click')
  onClick() {
    this._autoLogoutService.resetTime();
  }

  constructor(
    private cdRef: ChangeDetectorRef,
    private buySellRef: MatBottomSheetRef,
    private _authService: AuthService,
    private _configService: ConfigService,
    private _router: Router,
    @Optional() @Inject(MAT_BOTTOM_SHEET_DATA) public data,
    private _quoteService: QuoteService,
    private _notificationService: NotificationsService,
    private _autoLogoutService: AutoLogoutService,
    private _financialService: FinancialService,
    dialog: MatDialog,
    private _authServer: AuthServer,
    private _authenticationService: AuthenticationService,
    private _statusOmsService: StatusOmsService
  ) {
    this.indiceFuture = false;
    this.dialog = dialog;
    this.hiddenInvalid = true;
    if (this._configService.getUserSystemLogged()) {
      this.userName = this._configService.getUserSystemLogged().toUpperCase();
    }
    if (Number(this._configService.getBrokerAccountType()) === 1) {
      this.isAssessor = true;
    } else {
      this.isAssessor = false;
    }
    if (this._configService.getHbSignature()) {
      this.model.signature = this._configService.getHbSignature();
      this.saveSignature = true;
    }

    this.subscriptionStatusOms = this._statusOmsService.messagesStatusOms
      .subscribe(statusOms => {
        if (statusOms === StatusConnection.ERROR) {
          this.loading = false;
          this.statusOmsOnline = false;
        } else {
          this.statusOmsOnline = true;
        }
      });
  }

  ngOnInit() {
    this.buySellRef.backdropClick().subscribe(() => { this.buySellRef.dismiss(); });
    if (!this.isLoginW) {
      this.validSignature = true;
    }
    if (!this._authService.isAuthenticated) {
      this.buySellRef.dismiss();
      this._configService.deleteCookie();
      this._configService.deleteHbSignature();
      if (this._configService.isLoginTypeWSO2() && this._configService.isUserAssessorLogged()) {
        window.location.href = this._configService.REDIRECT_LOGOUT + '/assessor/login';
      } else if (this._configService.isLoginTypeWSO2() && !this._configService.isUserAssessorLogged()) {
        window.location.href = this._configService.REDIRECT_LOGOUT + '/client/login';
      } else {
        this._router.navigate(['/login']);
      }
    }

    let accountSelected = this._configService.getBrokerAccountSelected();
    if (!accountSelected) {
      this.setIntervalAccount = setInterval(() => {
        accountSelected = this._configService.getBrokerAccountSelected();
        if (accountSelected) {
          this.model.account = accountSelected;
          this.initBillet(this.model.account);
          clearInterval(this.setIntervalAccount);
        }
      }, 1000);
    } else {
      this.model.account = accountSelected;
      this.initBillet(this.model.account);
    }

  }

  initBillet(accountSelected) {
    if (this.model.account) {
      let getBalancetrue = false;
      this.accounBrokerList = this._configService.getBrokerAccountList();
      if (this.accounBrokerList) {
        // Assessor
        if (this.accounBrokerList.accountsBean) {
          const optionsTemp: any = [];
          this.accounBrokerList.accountsBean.forEach((item) => {
            const duplicated = optionsTemp.findIndex(redItem => {
              return item.account === redItem.account;
            }) > -1;
            if (!duplicated) {
              optionsTemp.push(item);
            }
          });
          this.accounBrokerList.accountsBean = optionsTemp;
          AppUtils.orderObjectAsc(this.accounBrokerList.accountsBean, 'account');
        } else if (this.accounBrokerList && this.accounBrokerList.length > 1) {
          const optionsTemp: any = [];
          this.accounBrokerList.forEach((item) => {
            const duplicated = optionsTemp.findIndex(redItem => {
              return item === redItem;
            }) > -1;
            if (!duplicated) {
              optionsTemp.push(item);
            }
          });
          this.accounBrokerList = [];
          this.accounBrokerList = optionsTemp;
          AppUtils.orderObjectAscNumberBillet(this.accounBrokerList, 'account');
          this.accountOptions.push(this.accounBrokerList);
          if (Number(accountSelected)) {
            for (let x = 0; x < this.accounBrokerList.length; x++) {
              if (Number(this.accounBrokerList[x].account) === Number(accountSelected)) {
                if (!getBalancetrue) {
                  this.getBalanceValue(accountSelected);
                  this.acccountBalanceSubscribe = accountSelected;
                  getBalancetrue = true;
                }
                this.accountSelectBroker = this.accounBrokerList[x];
                break;
              }
            }
          }
        }
        if (this._configService.isUserAssessorLogged()) {
          for (let i = 0; i < this.accounBrokerList.accountsBean.length; i++) {
            this.accountOptions.push(this.accounBrokerList.accountsBean[i].account);
            if (!this.accountOptions.includes(this.accounBrokerList.accountsBean)) {
              for (let x = 0; x < this.accounBrokerList.accountsBean.length; x++) {
                if (this.accounBrokerList.user.profile_name
                  && Number(this.accounBrokerList.accountsBean[x].account) === Number(accountSelected)) {
                  if (!getBalancetrue) {
                    this.getBalanceValue(accountSelected);
                    this.acccountBalanceSubscribe = accountSelected;
                    getBalancetrue = true;
                  }
                  this.accountSelectBroker = this.accounBrokerList;
                  this.accountSelectBrokerAssessor = this.accounBrokerList;
                  break;
                }
              }
            }
          }
        } else {
          if (this.accounBrokerList.account) {
            if (!this.accountOptions.includes(this.accounBrokerList.account)) {
              this.accountOptions.push(this.accounBrokerList.account);
              this.getBalanceValue(this.accounBrokerList.account);
              this.accountSelectBroker = this.accounBrokerList;
            }
          } else {
            this.accountOptions = [];
            this.accounBrokerList.forEach(element => {
              this.accountOptions.push(element.account);
            });
          }
          if (this.accountOptions.length <= 0) {
            this._notificationService.error('', 'Conta não encontrada');
            this.model.account = '';
          }
        }
      }
      this.setInitialValues();

      if (this.data.billetType) {
        this.type = this.data.billetType;
      }

      if (this.data.quote) {
        this.model.quote = this.data.quote;
        this.model.price = this.data.price;
        this.symbol = this.data.quote;
        if (this.data.type) {
          this.type = this.data.type;
        } else {
          this.type = this.data.billetType;
        }
      } else if (this._configService.getConfigBilletClose()) {
        this.configBilletClosed = this._configService.getConfigBilletClose();
        this.setInitialValuesClosedModal(this.configBilletClosed);
      }

      this._closedModalBuySell = this._configService.modalBuySellClosed$
        .subscribe(closed => {
          if (closed && this.buySellRef) {
            this.buySellRef.dismiss();
          }
        });

      //TODO Remover quando refatorar o FT para parametrização dos tipos de mercado.      
      if (environment.WHITELABEL === 2) {
        this.model.orderStrategy = 'DAYTRADE';
      }
      this.loadingInit = false;
      if (this.cdRef) {
        this.cdRef.detectChanges();
      }
    }
  }

  getBalanceValue(account: string) {
    this._financialService.subscribeFinancial(account);
    this.subscriptionMyBalance = this._financialService.messages
      .subscribe(data => {
        if (data) {
          data.filter(element => AppUtils.convertToCurrencyString(element.limiteOperacional) !== this.limiteOperacional)
            .forEach(elem => {
              elem.limiteOperacional = AppUtils.convertNumberToCurrency(Number(elem.limiteOperacional));
              this.limiteOperacional = elem.limiteOperacional;
            });
          this.loading = false;
        }
      });
  }

  ngOnDestroy() {
    if (this._symbolNameLast !== '') {
      this._quoteService.unsubscribeQuote(this._symbolNameLast, Constants.WINDOW_NAME_BILLET_DAY_TRADE);
    }
    if (this._subscribed) {
      this._subscription.unsubscribe();
    }
    if (this._financialService && this.model.account) {
      this._financialService.unsubscribeFinancial(this.model.account);
      if (this.subscriptionMyBalance) {
        this.subscriptionMyBalance.unsubscribe();
      }
    }
    if (this.subscriptionStatusOms) {
      this.subscriptionStatusOms.unsubscribe();
    }
    if (this._closedModalBuySell) {
      this._closedModalBuySell.unsubscribe();
      this._configService._modalBuySellClosed.next(false);
    }
    if (this.model.quote) {
      this._configService.setConfigBilletClose(this.model);
    } else {
      this._configService.deleteConfigBilletClose();
    }
    if (this.setIntervalAccount) {
      clearInterval(this.setIntervalAccount);
    }
  }

  setSelectYPosition(event) {
    if (event) {
      const element = document.getElementsByClassName('cdk-overlay-pane');
      if (element && element.length > 0) {
        for (var i = 0; i < element.length; i++) {
          if (element[i].children[0] && element[i].children[0].classList.contains('buy-sell-options')) {
            let tempStyle = element[i]['style'].cssText;
            if (tempStyle && tempStyle.indexOf('transform:translateY(0)') == -1) {
              tempStyle = tempStyle.replace(/translateY\(.*\)/, 'translateY(0)');
              element[i].setAttribute("style", tempStyle);
            }
          }
        }
      }
    }
  }

  public resetDate() {
    this.model.validityOrder = new Date();
  }

  public tabChanged() {
    this.isStop = !this.isStop;
    if (!this.isStop) {
      this.lossTrigger = true;
      this.lossLimit = true;
      this.gainTrigger = true;
      this.gainLimit = true;
      setTimeout(() => {
        if (!this.symbol) {
          this.inputQuoteEl.nativeElement.focus();
        }
      }, 300);
    } else {
      setTimeout(() => {
        if (!this.symbol) {
          this.inputQuoteStopEl.nativeElement.focus();
        }
      }, 300);
    }
  }

  public verifyPrice() {
    if (this.model && this.model.quote && this.model.quote.length >= 5 &&
      this.model.type.includes(this.typesOptions[0].id) && this.symbol) {
      return true;
    }
  }

  public quantityDisabled() {
    if (this.symbol == null) {
      return false;
    } else {
      return true;
    }
  }

  public validPrice(value: number) {
    if (value < 0) {
      return this.model.price = this.model.price * -1;
    }
    if (this.model.price > 999999) {
      return this.model.price = 999999;
    } else if (this.model.targetlimit > 999999) {
      return this.model.targetlimit = 999999;
    } else if (this.model.targettrigger > 999999) {
      return this.model.targettrigger = 999999;
    } else if (this.model.stoplimit > 999999) {
      return this.model.stoplimit = 999999;
    } else if (this.model.stoptrigger > 999999) {
      return this.model.stoptrigger = 999999;
    } else if (this.model.maxfloor > 9999999) {
      return this.model.maxfloor = 9999999;
    } else if (this.model.minqtd > 999999) {
      return this.model.minqtd = 999999;
    } else if (this.model.qtd > 999999) {
      return this.model.qtd = 999999;
    }
  }

  public openModalConfirm() {
    if (!this.statusOmsOnline) {
      this._notificationService.alert('', this.msgStatusOmsOffile);
      return;
    }

    if (!this.validateOpenConfirm()) {
      return;
    }
    this.confirmModalOpened = true;
    if (this.model.qtd === 0) {
      this._notificationService.alert('', Constants.BILLET_ORDER_MSG002);
    } else {
      const element = document.getElementById('confirmation');
      element.style.display = 'unset';
      element.style.bottom = '0px';
      element.parentElement.style.display = 'block';
      this.clickConfirmation = true;
      if (this.model.signature) {
        this.validateSignatureValid(this.model.signature);
      }
      if (this.model.qtd % Number(this.model.loteDefault) === 0) {
        this.fractionaryQtd = 0;
        this.nonFractionaryQtd = this.model.qtd;
        this.nonFractionary = Number(this.model.price) * this.model.qtd;
        this.totalDisplay = this.totalDisplay + this.nonFractionary;

        this.fractionaryQtdGain = 0;
        this.nonFractionaryQtdGain = this.model.qtd;
        this.nonFractionaryGain = Number(this.model.targettrigger) * this.model.qtd;
        this.totalDisplayGain = this.totalDisplayGain + this.nonFractionaryGain;

        this.fractionaryQtdLoss = 0;
        this.nonFractionaryQtdLoss = this.model.qtd;
        this.nonFractionaryLoss = Number(this.model.stoptrigger) * this.model.qtd;
        this.totalDisplayLoss = this.totalDisplayLoss + this.nonFractionaryLoss;

      } else {
        this.fractionaryQtd = this.model.qtd - (this.model.qtd % Number(this.model.loteDefault));
        this.fractionaryQtd = this.model.qtd - this.fractionaryQtd;
        this.nonFractionaryQtd = this.model.qtd - this.fractionaryQtd;
        this.nonFractionary = this.nonFractionaryQtd * this.model.price;
        this.totalDisplay = this.totalDisplay + this.nonFractionary;
        this.fractionary = this.fractionaryQtd * Number(this.model.price.toString());
        this.totalDisplay = this.totalDisplay + this.fractionary;

        this.fractionaryQtdGain = this.model.qtd - (this.model.qtd % Number(this.model.loteDefault));
        this.fractionaryQtdGain = this.model.qtd - this.fractionaryQtdGain;
        this.nonFractionaryQtdGain = this.model.qtd - this.fractionaryQtdGain;
        this.nonFractionaryGain = this.nonFractionaryQtdGain * this.model.targettrigger;
        this.totalDisplayGain = this.totalDisplayGain + this.nonFractionaryGain;
        this.fractionaryGain = this.fractionaryQtdGain * this.model.targettrigger;
        this.totalDisplayGain = this.totalDisplayGain + this.fractionaryGain;

        this.fractionaryQtdLoss = this.model.qtd - (this.model.qtd % Number(this.model.loteDefault));
        this.fractionaryQtdLoss = this.model.qtd - this.fractionaryQtdLoss;
        this.nonFractionaryQtdLoss = this.model.qtd - this.fractionaryQtdLoss;
        this.nonFractionaryLoss = this.nonFractionaryQtdLoss * this.model.stoptrigger;
        this.totalDisplayLoss = this.totalDisplayLoss + this.nonFractionaryLoss;
        this.fractionaryLoss = this.fractionaryQtdLoss * this.model.stoptrigger;
        this.totalDisplayLoss = this.totalDisplayLoss + this.fractionaryLoss;
      }
    }
  }

  validateOpenConfirm() {
    if (this.model.qtd == null || this.model.qtd === 0) {
      this._notificationService.warn('', 'Quantidade inválida.');
      return false;
    }
    if (((this.model.price == null || this.model.price) && Number(this.model.price) <= 0) && !this.isStop) {
      this._notificationService.warn('', 'Preço zerado ou inválido.');
      return false;
    }
    if ((this.model.stoptrigger == null || this.model.stoptrigger) && Number(this.model.stoptrigger) <= 0) {
      this._notificationService.warn('', 'Preço zerado ou inválido.');
      return false;
    }
    if ((this.model.stoplimit == null || this.model.stoplimit) && Number(this.model.stoplimit) <= 0) {
      this._notificationService.warn('', 'Preço zerado ou inválido.');
      return false;
    }
    if ((this.model.targettrigger == null || this.model.targettrigger) && Number(this.model.targettrigger) <= 0) {
      this._notificationService.warn('', 'Preço zerado ou inválido.');
      return false;
    }
    if ((this.model.targetlimit == null || this.model.targetlimit) && Number(this.model.targetlimit) <= 0) {
      this._notificationService.warn('', 'Preço zerado ou inválido.');
      return false;
    }
    if (!this.model.quote || this.model.quote === '') {
      return false;
    }
    if (!this.lossTrigger) {
      if (this.model.side === Constants.ORDER_SIDE_BUY) {
        this._notificationService.warn('', Constants.BILLET_ORDER_1_TRIGGER_BUY);
      } else if (this.model.side === Constants.ORDER_SIDE_SELL) {
        this._notificationService.warn('', Constants.BILLET_ORDER_1_TRIGGER_SELL);
      }
      return false;
    }

    if (!this.lossLimit) {
      if (this.model.side === Constants.ORDER_SIDE_BUY) {
        this._notificationService.warn('', Constants.BILLET_ORDER_1_LIMIT_BUY);
      } else if (this.model.side === Constants.ORDER_SIDE_SELL) {
        this._notificationService.warn('', Constants.BILLET_ORDER_1_LIMIT_SELL);
      }

      return false;
    }

    if (!this.gainTrigger) {
      if (this.model.side === Constants.ORDER_SIDE_BUY) {
        this._notificationService.warn('', Constants.BILLET_ORDER_2_TRIGGER_BUY);
      } else if (this.model.side === Constants.ORDER_SIDE_SELL) {
        this._notificationService.warn('', Constants.BILLET_ORDER_2_TRIGGER_SELL);
      }

      return false;
    }

    if (!this.gainLimit) {
      if (this.model.side === Constants.ORDER_SIDE_BUY) {
        this._notificationService.warn('', Constants.BILLET_ORDER_2_LIMIT_BUY);
      } else if (this.model.side === Constants.ORDER_SIDE_SELL) {
        this._notificationService.warn('', Constants.BILLET_ORDER_2_LIMIT_SELL);
      }

      return false;
    }

    return true;
  }

  public changeType(value: string) {
    if (value === Constants.ORDER_SIDE_BUY) {
      this.type = Constants.ORDER_SIDE_BUY;
      this.data.billetType = Constants.ORDER_SIDE_BUY;
      this.model.side = SideEnum.BUY;
      this.isBuy = true;
      this.setMessagesStop();
      if (this.isStop) {
        this.model.stoptrigger = AppUtils.convertToNumber(this.ask.toString(), this.messageQuote.tickSize);
        this.model.stoplimit = AppUtils.convertToNumber(this.ask.toString(), this.messageQuote.tickSize);
        this.model.targettrigger = AppUtils.convertToNumber(this.ask.toString(), this.messageQuote.tickSize);
        this.model.targetlimit = AppUtils.convertToNumber(this.ask.toString(), this.messageQuote.tickSize);
      } else {
        if (this.ask && this.messageQuote && this.messageQuote.tickSize) {
          this.model.price = AppUtils.convertToNumber(this.ask.toString(), this.messageQuote.tickSize);
        }
      }
    } else {
      this.type = Constants.ORDER_SIDE_SELL;
      this.data.billetType = Constants.ORDER_SIDE_SELL;
      this.model.side = SideEnum.SELL;
      this.isBuy = false;
      this.setMessagesStop();
      if (this.isStop) {
        this.model.stoptrigger = AppUtils.convertToNumber(this.bid.toString(), this.messageQuote.tickSize);
        this.model.stoplimit = AppUtils.convertToNumber(this.bid.toString(), this.messageQuote.tickSize);
        this.model.targettrigger = AppUtils.convertToNumber(this.bid.toString(), this.messageQuote.tickSize);
        this.model.targetlimit = AppUtils.convertToNumber(this.bid.toString(), this.messageQuote.tickSize);
      } else {
        if (this.bid && this.messageQuote && this.messageQuote.tickSize) {
          this.model.price = AppUtils.convertToNumber(this.bid.toString(), this.messageQuote.tickSize);
        }
      }
    }

    if (!this.isStop) {
      setTimeout(() => {
        if (!this.symbol) {
          this.inputQuoteEl.nativeElement.focus();
        }
      }, 100);
    } else {
      setTimeout(() => {
        if (!this.symbol) {
          this.inputQuoteStopEl.nativeElement.focus();
        }
      }, 100);
    }
  }

  public setInitialValuesClosedModal(value: SimpleOrder) {
    if (this.data.billetType === Constants.ORDER_SIDE_BUY) {
      this.model.side = SideEnum.BUY;
      this.isBuy = true;
      this.messageStopLoss = Constants.BILLET_ORDER_STOP_BUY_LOSS;
      this.messageStopGain = Constants.BILLET_ORDER_STOP_BUY_GAIN;
    } else {
      this.model.side = SideEnum.SELL;
      this.isBuy = false;
      this.messageStopLoss = Constants.BILLET_ORDER_STOP_SELL_LOSS;
      this.messageStopGain = Constants.BILLET_ORDER_STOP_SELL_GAIN;
    }
    if (value.quote && value.quote !== '') {
      this.getMessageQuote(value.quote);
    } else {
      this.symbol = null;
    }
    this.model.type = value.type;
    this.model.validityOrder = value.validityOrder;
    this.model.quote = value.quote;
    this.model.stoptrigger = value.stoptrigger;
    this.model.stoplimit = value.stoplimit;
    this.model.targettrigger = value.targettrigger;
    this.model.targetlimit = value.targetlimit;
    this.model.maxfloor = value.maxfloor;
    this.model.minqtd = value.minqtd;
    this.model.price = value.price;
    this.model.qtd = value.qtd;
    this.model.totalGain = value.totalGain;
    this.model.totalLoss = value.totalLoss;
    this.minimumIncrement = '0.01';
    this.fractionaryGain = 0;
    this.fractionaryLoss = 0;
    this.model.orderStrategy = value.orderStrategy;
    this.model.timeInForces = value.timeInForces;
    this.qtdValid = true;
    this.minimumAmount = 0;
    this.verifyQuantity();
  }


  public setInitialValues() {
    if (this.data.billetType === Constants.ORDER_SIDE_BUY) {
      this.model.side = SideEnum.BUY;
      this.isBuy = true;
      this.messageStopLoss = Constants.BILLET_ORDER_STOP_BUY_LOSS;
      this.messageStopGain = Constants.BILLET_ORDER_STOP_BUY_GAIN;
    } else {
      this.model.side = SideEnum.SELL;
      this.isBuy = false;
      this.messageStopLoss = Constants.BILLET_ORDER_STOP_SELL_LOSS;
      this.messageStopGain = Constants.BILLET_ORDER_STOP_SELL_GAIN;
    }
    if (this.data.quote && this.data.quote !== '') {
      this.getMessageQuote(this.data.quote);
    } else {
      this.symbol = null;
    }
    this.model.type = this.typesOptions[0].id;
    this.model.validityOrder = new Date();
    this.model.quote = '';
    this.model.stoptrigger = 0;
    this.model.stoplimit = 0;
    this.model.targettrigger = 0;
    this.model.targetlimit = 0;
    this.model.maxfloor = 0;
    this.model.minqtd = 0;
    this.model.price = 0;
    this.model.qtd = 0;
    this.model.totalGain = 0;
    this.model.totalLoss = 0;
    this.minimumIncrement = '0.01';
    this.fractionaryGain = 0;
    this.fractionaryLoss = 0;
    this.model.orderStrategy = this.strategyOptions[1].id;
    this.model.timeInForces = this.timeInForcesOptions[0].webfeeder;
    this.qtdValid = true;
    this.minimumAmount = 0;
    this.verifyQuantity();
  }

  public resetDefaultValues() {
    if (this.data.billetType === Constants.ORDER_SIDE_BUY) {
      this.model.side = SideEnum.BUY;
      this.isBuy = true;
      this.messageStopLoss = Constants.BILLET_ORDER_STOP_BUY_LOSS;
      this.messageStopGain = Constants.BILLET_ORDER_STOP_BUY_GAIN;
    } else {
      this.model.side = SideEnum.SELL;
      this.isBuy = false;
      this.messageStopLoss = Constants.BILLET_ORDER_STOP_SELL_LOSS;
      this.messageStopGain = Constants.BILLET_ORDER_STOP_SELL_GAIN;
    }
    if (this.data.quote && this.data.quote !== '') {
      this.getMessageQuote(this.data.quote);
    }

    this.model.type = this.typesOptions[0].id;
    this.model.validityOrder = new Date();
    this.model.stoptrigger = 0;
    this.model.stoplimit = 0;
    this.model.targettrigger = 0;
    this.model.targetlimit = 0;
    this.model.maxfloor = 0;
    this.model.minqtd = 0;
    this.model.price = 0;
    this.model.qtd = 0;
    this.model.totalGain = 0;
    this.model.totalLoss = 0;
    this.minimumIncrement = '0.01';
    this.fractionaryGain = 0;
    this.fractionaryLoss = 0;
    this.model.orderStrategy = this.strategyOptions[1].id;
    this.model.timeInForces = this.timeInForcesOptions[0].webfeeder;
    this.qtdValid = true;
    this.minimumAmount = 0;
    this.verifyQuantity();
  }

  @HostListener('document:keydown', ['$event'])
  onKeyDownHandler(event: KeyboardEvent) {
    this._autoLogoutService.resetTime();
    if (event.key === 'Escape') {
      this.confirmModalOpened ? this.closeConfirmation() : this.buySellRef.dismiss();
    }
  }

  public closeModal(event?: MouseEvent): void {
    this.buySellRef.dismiss();
    if (event) {
      event.preventDefault();
    }
  }

  public verifyQuantity() {
    if (this.isStop) {
      this.tooltipValidator();
    }
    if (this.model.price < 0) {
      this.model.price = this.model.price * -1;
    }
    if (this.model.qtd < 0) {
      this.model.qtd = this.model.qtd * -1;
    }
    if (this.model.qtd < this.minimumAmount) {
      this.qtdValid = false;
    } else {
      this.qtdValid = true;
    }
    this.model.total = ((this.model.price * this.model.qtd) / this.messageQuote.quotationForm) * this.messageQuote.contractMultiplier;
    this.model.totalLoss = this.model.stoptrigger * this.model.qtd;
    this.model.totalGain = this.model.targettrigger * this.model.qtd;
  }

  public flagChange() {
    this.flagOptions = !this.flagOptions;
    if (this.flagOptions) {
      const elem3 = document.getElementById('optionsBody');
      elem3.style.display = 'flex';
      const elem2 = document.getElementById('options');
      elem2.className = 'options-arrow-up';
    } else {
      const elem3 = document.getElementById('optionsBody');
      elem3.style.display = 'none';
      const elem2 = document.getElementById('options');
      elem2.className = 'options-arrow-down';
    }
  }

  public getMessageQuote(symbol: string) {
    this.symbol = symbol;
    this.hiddenInvalid = true;
    if ((this._symbolName !== undefined || this._symbolName !== '') && this._symbolName !== symbol) {
      this._symbolName = symbol.toLowerCase();
      this.model.quote = symbol;
      this.initialPrice = true;
      if (this._symbolName !== undefined) {
        if (this._symbolNameLast !== undefined) {
          this._quoteService.unsubscribeQuote(this._symbolNameLast, Constants.WINDOW_NAME_BILLET_DAY_TRADE);
        }
        this._symbolNameLast = symbol.toLowerCase();
        this._quoteService.subscribeQuote(this._symbolName, Constants.WINDOW_NAME_BILLET_DAY_TRADE);
        if (symbol !== this.configBilletClosed.quote) {
          this.configBilletClosed = new SimpleOrder();
        }
        this._subscribed = true;
        if (this._subscription) {
          this._subscription.unsubscribe();
        }
        this._subscription = this._quoteService.messagesQuote
          .subscribe((msg: MessageQuote) => {
            if (msg && msg.symbol === this._symbolNameLast && (msg.errorMessage || msg.expiration === '1')) {
              this.hiddenInvalidActive = false;
              this.filteredAssets = [];
              this._quoteService.unsubscribeQuote(msg.symbol.toLowerCase(), Constants.WINDOW_NAME_BILLET_DAY_TRADE);
              this.resetDefaultValues();
            } else if (msg !== undefined && msg.symbol != null && msg.symbol === this._symbolNameLast) {
              this.hiddenInvalidActive = true;
              this.filteredAssets = [];
              this.messageQuote = msg;
              this.minimumAmount = 1;
              if (this.messageQuote.symbol.indexOf('win') === 0 || this.messageQuote.symbol.indexOf('dol') === 0 ||
                msg.symbol.toLowerCase().indexOf('ind') === 0) {
                this.indiceFuture = true;
              } else {
                this.indiceFuture = false;
              }
              if (this.messageQuote.lastTrade && this.messageQuote.lastTrade.toString() !== '-') {
                const lastTradeTemp = msg.lastTrade.toString().replace('.', '');
                this.messageQuote.lastTrade = AppUtils.convertNumber(lastTradeTemp);
                this.messageQuote.lastTrade = AppUtils.convertToNumber(this.messageQuote.lastTrade.toString(), this.messageQuote.tickSize);
                this.lastTrade = this.messageQuote.lastTrade;
              } else {
                this.lastTrade = 0;
              }
              if (this.messageQuote.loteDefault === undefined || this.messageQuote.loteDefault <= 0) {
                this.model.loteDefault = 100;
                this.loteSizeStep = '100';
              } else {
                this.model.loteDefault = this.messageQuote.loteDefault;
                this.loteSizeStep = (this.messageQuote.loteDefault).toString();
              }
              if (this.messageQuote.symbol.indexOf('win') === 0 ||
                this.messageQuote.symbol.indexOf('dol') === 0 || msg.symbol.toLowerCase().indexOf('ind') === 0) {
                if (this.messageQuote.tickSize !== null && this.messageQuote.tickSize !== undefined) {
                  this.tickSize = this.messageQuote.tickSize;
                } else {
                  this.tickSize = 2;
                }
                this.priceLimitValuesFormatOptionsFutureIndex.precision = this.tickSize;
              } else {
                if (this.messageQuote.tickSize !== null && this.messageQuote.tickSize !== undefined) {
                  this.tickSize = this.messageQuote.tickSize;
                } else {
                  this.tickSize = 2;
                }
                this.priceLimitValuesFormatOptions.precision = this.tickSize;
              }
              let priceTemp: any;
              if (this.initialPrice) {
                if (!msg.ask.toString().includes('-')) {
                  if (this.data.price) {
                    priceTemp = this.data.price;
                    this.data.price = null;
                  } else if (this.configBilletClosed.price) {
                    priceTemp = this.configBilletClosed.price;
                  } else if (this.type.match(new RegExp('buy', 'i'))) {
                    priceTemp = msg.ask.toString().replace('.', '');
                  } else if (this.type.match(new RegExp('sell', 'i'))) {
                    priceTemp = msg.bid.toString().replace('.', '');
                  }
                  this.model.price = AppUtils.convertNumber(priceTemp);
                  this.model.price = AppUtils.convertToNumber(this.model.price.toString(), this.messageQuote.tickSize);
                  this.model.stoptrigger = this.model.price;
                  this.model.stoplimit = this.model.price;
                  this.model.targettrigger = this.model.price;
                  this.model.targetlimit = this.model.price;
                  this.initialPrice = false;
                  if (this.model.loteDefault > 0) {
                    this.model.qtd = this.model.loteDefault;
                  } else {
                    this.model.qtd = 0;
                  }
                } else {
                  this.initialPrice = false;
                  this.model.price = 0;
                }
              }
              if (this.messageQuote.minimumIncrement === undefined || !this.messageQuote.minimumIncrement) {
                this.minimumIncrement = '0.01';
              } else {
                this.minimumIncrement = this.messageQuote.minimumIncrement.toString().replace(',', '.');
              }
              this.variationNumber = (this.messageQuote.variation);
              if (this.messageQuote.variation) {
                this.variation = (this.messageQuote.variation.toString()).replace('.', ',').concat('%');
              } else {
                this.variation = '0';
              }
              if (msg.ask.toString() !== '-') {
                priceTemp = msg.ask.toString().replace('.', '');
                this.ask = AppUtils.convertNumber(priceTemp);
                this.ask = AppUtils.convertToNumber(this.ask.toString(), this.messageQuote.tickSize);
              } else {
                this.ask = 0;
              }
              if (msg.bid.toString() !== '-') {
                priceTemp = msg.bid.toString().replace('.', '');
                this.bid = AppUtils.convertNumber(priceTemp);
                this.bid = AppUtils.convertToNumber(this.bid.toString(), this.messageQuote.tickSize);
              } else {
                this.bid = 0;
              }
              this.quoteState = this.messageQuote.status;
              if (msg.theoryPrice && msg.theoryPrice.toString() !== '-') {
                priceTemp = AppUtils.convertNumber(msg.theoryPrice.toString());
                this.priceTheory = AppUtils.convertNumber(priceTemp);
                this.priceTheory = AppUtils.convertToNumber(this.priceTheory.toString(), this.messageQuote.tickSize);
              }
              if (this.quoteState) {
                this.quoteStateText = Constants.QUOTE_STATUS_MAP.find(x => x.id === Number(this.quoteState)).text;
              }
              if (!this.messageQuote.quotationForm) {
                this.quotationForm = 1;
              } else {
                this.quotationForm = this.messageQuote.quotationForm;
              }
              if (!this.messageQuote.contractMultiplier) {
                this.contractMultiplier = 1;
              } else {
                this.contractMultiplier = this.messageQuote.contractMultiplier;
              }
              this.model.market = this.messageQuote.marketType;
              this.model.total = ((this.model.price * this.model.qtd) / this.quotationForm) * this.contractMultiplier;
              this.tunnelUpperLimit = this.messageQuote.tunnelUpperLimit;
              this.tunnelLowerLimit = this.messageQuote.tunnelLowerLimit;
              if (this.messageQuote.tickSize > 0 && this.messageQuote.tickSize.toString() !== '-') {
                this.inTheWeek = AppUtils.convertToNumber(this.messageQuote.inTheWeek.toString(), this.messageQuote.tickSize);
              } else {
                this.inTheWeek = 0;
              }
              this.symbol = this.messageQuote.symbol.toLocaleUpperCase();
              if (this.messageQuote.marketType === Constants.MARKET_CODE_BOVESPA) {
                this.company = this.messageQuote.description + ' ' + this.messageQuote.classification;
              } else {
                this.company = this.messageQuote.description;
              }
              if (this.cdRef) {
                this.cdRef.detectChanges();
              }
            }
            this.verifyQuantity();
          });
      }
    }
  }

  public onQuoteChange(quoteSymbol?: string) {
    if (quoteSymbol) {
      if (quoteSymbol.length >= 2) {
        this.getMessageQuote(quoteSymbol);
      }
    } else {
      this._quoteService.unsubscribeQuote(this.symbol, Constants.WINDOW_NAME_BILLET_DAY_TRADE);
      this.hiddenInvalidActive = false;
    }
  }

  public onAutoCompleteAsset(textInput: any) {
    let symbolAutoComplete = '';
    if (textInput.key !== 'ArrowDown'
      && textInput.key !== 'ArrowUp'
      && textInput.key !== 'Enter') {
      if (textInput.target === undefined) {
        symbolAutoComplete = textInput.toLowerCase();
      } else {
        symbolAutoComplete = textInput.target.value.toLowerCase();
      }
      if (symbolAutoComplete) {
        this._authServer.iQuote.quotesQuery(symbolAutoComplete)
          .subscribe(datas => {
            if (datas) {
              if (datas.length > 0 && symbolAutoComplete !== '') {
                this.filteredAssets = [...datas];
                const asset = this.filteredAssets.findIndex(index =>
                  index.symbol.toLocaleUpperCase() === symbolAutoComplete.toLocaleUpperCase());
                if (asset === -1) {
                  this.hiddenInvalid = false;
                } else {
                  this.hiddenInvalid = true;
                }
              } else {
                this.filteredAssets = [];
                this.hiddenInvalid = false;
              }
            } else {
              this.filteredAssets = undefined;
              return;
            }
          });
      }
      if (!(symbolAutoComplete.length >= 5)) {
        this.company = '';
      }
      if (this.filteredAssets === undefined || this.filteredAssets.length === 0) {
        this.company = '';
      }
      return this.filteredAssets;
    }
  }

  onchangeSymbol(evento, symbolAutoComplete?: string, itemSelected?: string) {
    this.model.quote = '';
    const messageQuoteQuery = new MessageQuoteQuery();
    if (evento.source !== undefined) {
      if (this.filteredAssets.length > 0) {
        let searchSymbol;
        if (itemSelected) {
          searchSymbol = this.filteredAssets.filter(x => x.symbol.toUpperCase() === itemSelected.toUpperCase());
        } else if (symbolAutoComplete) {
          searchSymbol = this.filteredAssets.filter(x => x.symbol.toUpperCase() === symbolAutoComplete.toUpperCase());
        }
        if (searchSymbol) {
          this.model.quote = searchSymbol[0].symbol.toUpperCase();
        }
      } else {
        return false;
      }
      this.SetCompany(this.model.quote.toUpperCase());
      this.filteredAssets = [];
      messageQuoteQuery.symbol = this.model.quote.toUpperCase();
      this.onsymbolChanged.emit(messageQuoteQuery);
    } else if (evento.target !== undefined) {
      this.model.quote = evento.target.value.toUpperCase();
      if (this.filteredAssets.findIndex(s => s.symbol.toUpperCase() === this.model.quote) === -1) {
        this.model.quote = '';
      }
      this.SetCompany(this.model.quote.toUpperCase());
      this.filteredAssets = [];
      messageQuoteQuery.symbol = this.model.quote.toUpperCase();
      this.onsymbolChanged.emit(messageQuoteQuery);
    } else if (evento.value !== undefined && evento.value !== '') {
      this.model.quote = evento.value.toUpperCase();
      if (this.filteredAssets.findIndex(s => s.symbol.toUpperCase() === this.model.quote) !== -1) {
        this.SetCompany(this.model.quote.toUpperCase());
        this.filteredAssets = [];
        messageQuoteQuery.symbol = this.model.quote.toUpperCase();
        this.onsymbolChanged.emit(messageQuoteQuery);
      }
    }
  }

  setStopLoss() {
    if (this.model.stoptrigger == null) {
      this.model.stoptrigger = 0;
    }
    if (this.model.stoplimit == null) {
      this.model.stoplimit = 0;
    }
    this.stopLoss = !this.stopLoss;
    const fieldGain = document.getElementById('stop-gain-content');
    const fieldLoss = document.getElementById('stop-loss-content');

    if (!this.stopGain && !this.stopLoss) {
      this.stopGain = true;
      const elem1 = document.getElementById('gain-toggle');
      elem1.className = 'mat-slide-toggle toggle-on';
      fieldGain.style.opacity = '1';
      this.stopLoss = false;
      const elem2 = document.getElementById('loss-toggle');
      elem2.className = 'mat-slide-toggle toggle-off';
      fieldLoss.style.opacity = '0.5';
      this.lossTrigger = true;
      this.lossLimit = true;
    } else {
      if (this.stopLoss) {
        const elem = document.getElementById('loss-toggle');
        elem.className = 'mat-slide-toggle toggle-on';
        fieldLoss.style.opacity = '1';
        this.tooltipValidator();
      } else {
        const elem = document.getElementById('loss-toggle');
        elem.className = 'mat-slide-toggle toggle-off';
        fieldLoss.style.opacity = '0.5';
        this.lossTrigger = true;
        this.lossLimit = true;
      }
    }
    this.setMessagesStop();
  }

  setStopGain() {
    if (this.model.targettrigger == null) {
      this.model.targettrigger = 0;
    }
    if (this.model.targetlimit == null) {
      this.model.targetlimit = 0;
    }
    this.stopGain = !this.stopGain;
    const fieldGain = document.getElementById('stop-gain-content');
    const fieldLoss = document.getElementById('stop-loss-content');

    if (!this.stopGain && !this.stopLoss) {
      fieldLoss.style.opacity = '1';
      this.stopLoss = true;
      const elem1 = document.getElementById('loss-toggle');
      elem1.className = 'mat-slide-toggle toggle-on';
      this.stopGain = false;
      const elem2 = document.getElementById('gain-toggle');
      elem2.className = 'mat-slide-toggle toggle-off';
      fieldGain.style.opacity = '0.5';
      this.gainTrigger = true;
      this.gainLimit = true;
    } else {
      if (this.stopGain) {
        const elem = document.getElementById('gain-toggle');
        elem.className = 'mat-slide-toggle toggle-on';
        fieldGain.style.opacity = '1';
        this.tooltipValidator();
      } else {
        const elem = document.getElementById('gain-toggle');
        elem.className = 'mat-slide-toggle toggle-off';
        fieldGain.style.opacity = '0.5';
        this.gainTrigger = true;
        this.gainLimit = true;
      }
    }
    this.setMessagesStop();
  }

  setMessagesStop() {
    if (this.isBuy) {
      this.messageStopLoss = Constants.BILLET_ORDER_STOP_BUY_LOSS;
      this.messageStopGain = Constants.BILLET_ORDER_STOP_BUY_GAIN;
    } else {
      this.messageStopLoss = Constants.BILLET_ORDER_STOP_SELL_LOSS;
      this.messageStopGain = Constants.BILLET_ORDER_STOP_SELL_GAIN;
    }
  }

  public fullAssetFlag() {
    if (!this.fullFlag) {
      const elem = document.getElementById('order1');
      if (document.body.classList.contains('darkTheme')) {
        elem.style.color = 'white';
        elem.style.opacity = '0.6';
      } else {
        elem.style.color = 'black';
        elem.style.opacity = '0.6';
      }
      this.totalDisplay = Number((this.totalDisplay - this.nonFractionary).toFixed(2));
    } else {
      const elem = document.getElementById('order1');
      if (document.body.classList.contains('darkTheme')) {
        elem.style.color = '#2175ca';
        elem.style.opacity = '1';
      } else {
        elem.style.color = 'white';
        elem.style.opacity = '1';
      }
      this.totalDisplay = this.totalDisplay + this.nonFractionary;
    }
  }

  public fractAssetFlag() {
    if (!this.fractFlag) {
      const elem = document.getElementById('order2');
      if (document.body.classList.contains('darkTheme')) {
        elem.style.color = 'white';
        elem.style.opacity = '0.6';
      } else {
        elem.style.color = 'black';
        elem.style.opacity = '0.6';
      }
      this.totalDisplay = Number((this.totalDisplay - this.fractionary).toFixed(2));
    } else {
      const elem = document.getElementById('order2');
      if (document.body.classList.contains('darkTheme')) {
        elem.style.color = '#2175ca';
        elem.style.opacity = '1';
      } else {
        elem.style.color = 'white';
        elem.style.opacity = '1';
      }
      this.totalDisplay = this.totalDisplay + this.fractionary;
    }
  }

  public fullAssetFlagLoss() {
    if (!this.fullFlagLoss) {
      const elem = document.getElementById('order2');
      if (document.body.classList.contains('darkTheme')) {
        elem.style.color = 'white';
        elem.style.opacity = '0.6';
      } else {
        elem.style.color = 'black';
        elem.style.opacity = '0.6';
      }
      this.totalDisplayLoss = Number((this.totalDisplayLoss - this.nonFractionaryLoss).toFixed(2));
    } else {
      const elem = document.getElementById('order2');
      if (document.body.classList.contains('darkTheme')) {
        elem.style.color = '#2175ca';
        elem.style.opacity = '1';
      } else {
        elem.style.color = 'white';
        elem.style.opacity = '1';
      }
      this.totalDisplayLoss = this.totalDisplayLoss + this.nonFractionaryLoss;
    }
  }

  public fractAssetFlagLoss() {
    if (!this.fractFlagLoss) {
      const elem = document.getElementById('order1');
      if (document.body.classList.contains('darkTheme')) {
        elem.style.color = 'white';
        elem.style.opacity = '0.6';
      } else {
        elem.style.color = 'black';
        elem.style.opacity = '0.6';
      }
      this.totalDisplayLoss = Number((this.totalDisplayLoss - this.fractionaryLoss).toFixed(2));
    } else {
      const elem = document.getElementById('order1');
      if (document.body.classList.contains('darkTheme')) {
        elem.style.color = '#2175ca';
        elem.style.opacity = '1';
      } else {
        elem.style.color = 'white';
        elem.style.opacity = '1';
      }
      this.totalDisplayLoss = this.totalDisplayLoss + this.fractionaryLoss;
    }
  }

  public fullAssetFlagGain() {
    if (!this.fullFlagGain) {
      const elem = document.getElementById('order3');
      if (document.body.classList.contains('darkTheme')) {
        elem.style.color = 'white';
        elem.style.opacity = '0.6';
      } else {
        elem.style.color = 'black';
        elem.style.opacity = '0.6';
      }
      this.totalDisplayGain = Number((this.totalDisplayGain - this.nonFractionaryGain).toFixed(2));
    } else {
      const elem = document.getElementById('order3');
      if (document.body.classList.contains('darkTheme')) {
        elem.style.color = '#2175ca';
        elem.style.opacity = '1';
      } else {
        elem.style.color = 'white';
        elem.style.opacity = '1';
      }
      this.totalDisplayGain = this.totalDisplayGain + this.nonFractionaryGain;
    }
  }

  public fractAssetFlagGain() {
    if (!this.fractFlagGain) {
      const elem = document.getElementById('order4');
      if (document.body.classList.contains('darkTheme')) {
        elem.style.color = 'white';
        elem.style.opacity = '0.6';
      } else {
        elem.style.color = 'black';
        elem.style.opacity = '0.6';
      }
      if (!this.fractionaryGain) {
        this.fractionaryGain = 0;
      }
      this.totalDisplayGain = Number((this.totalDisplayGain - this.fractionaryGain).toFixed(2));
    } else {
      const elem = document.getElementById('order4');
      if (document.body.classList.contains('darkTheme')) {
        elem.style.color = '#2175ca';
        elem.style.opacity = '1';
      } else {
        elem.style.color = 'white';
        elem.style.opacity = '1';
      }
      if (!this.fractionaryGain) {
        this.fractionaryGain = 0;
      }
      this.totalDisplayGain = this.totalDisplayGain + this.fractionaryGain;
    }
  }

  public fullSignatureFlag(value, valid?: boolean) {
    setTimeout(() => {
      this.saveSignature = valid;
      if (!this.saveSignature) {
        this._configService.deleteHbSignature();
        const elem2 = document.getElementById('saveSignature');
        if (elem2) {
          if (document.body.classList.contains('darkTheme')) {
            elem2.style.color = 'white';
            elem2.style.opacity = '0.6';
          } else {
            elem2.style.color = 'black';
            elem2.style.opacity = '0.6';
          }
          const elem = document.getElementById('saveCheck');
          if (document.body.classList.contains('darkTheme')) {
            elem.style.color = 'white';
            elem.style.opacity = '0.6';
          } else {
            elem.style.color = 'black';
            elem.style.opacity = '0.6';
          }
        }
      } else {
        this._configService.setHbSignature(value);
        const elem2 = document.getElementById('saveSignature');
        if (elem2) {
          if (document.body.classList.contains('darkTheme')) {
            elem2.style.color = '#2175ca';
            elem2.style.opacity = '1';
          } else {
            elem2.style.color = 'white';
            elem2.style.opacity = '1';
          }
          const elem = document.getElementById('saveCheck');
          if (document.body.classList.contains('darkTheme')) {
            elem.style.color = '#2175ca';
            elem.style.opacity = '1';
          } else {
            elem.style.color = 'white';
            elem.style.opacity = '1';
          }
        }
      }
    }, 1);
  }

  public closeConfirmation() {
    const elem = document.getElementById('confirmation');
    if (elem) {
      elem.style.bottom = '-186px';
      elem.parentElement.style.display = 'none';
    }
    this.clickConfirmation = false;
    this.disableCheckBoxConfirm();
    this.confirmModalOpened = false;
    if (!this.saveSignature) {
      this.model.signature = null;
    }
  }

  public disableCheckBoxConfirm() {
    this.fullFlag = true;
    this.fullFlagGain = true;
    this.fullFlagLoss = true;
    this.fractFlag = true;
    this.fractFlagGain = true;
    this.fractFlagLoss = true;
    this.totalDisplay = 0;
    this.totalDisplayGain = 0;
    this.totalDisplayLoss = 0;
    this.priceTheory = undefined;
    for (let i = 1; i <= 4; i++) {
      const id = 'order' + i;
      const elem = document.getElementById(id);
      if (elem) {
        if (document.body.classList.contains('darkTheme')) {
          elem.style.color = '#2175ca';
          elem.style.opacity = '1';
        } else {
          elem.style.color = 'white';
          elem.style.opacity = '1';
        }
      }
    }
  }

  public stopOrderValidator() {
    if (this.model.side === Constants.ORDER_SIDE_BUY) {
      if (this.stopGain) {
        if (Number(this.model.targettrigger) > this.lastTrade) {
          this._notificationService.warn('', Constants.BILLET_ORDER_2_TRIGGER_BUY);
          return false;
        }
        if (Number(this.model.targetlimit) < Number(this.model.targettrigger)) {
          this._notificationService.warn('', Constants.BILLET_ORDER_2_LIMIT_BUY);
          return false;
        }
      }
      if (this.stopLoss) {
        if (Number(this.model.stoptrigger) < this.lastTrade) {
          this._notificationService.warn('', Constants.BILLET_ORDER_1_TRIGGER_BUY);
          return false;
        }

        if (Number(this.model.stoplimit) < Number(this.model.stoptrigger)) {
          this._notificationService.warn('', Constants.BILLET_ORDER_1_LIMIT_BUY);
          return false;
        }
      }
      return true;
    } else if (this.model.side === Constants.ORDER_SIDE_SELL) {
      if (this.stopGain) {
        if (Number(this.model.targettrigger) < this.lastTrade) {
          this._notificationService.warn('', Constants.BILLET_ORDER_2_TRIGGER_SELL);
          return false;
        }
        if (Number(this.model.targetlimit) > Number(this.model.targettrigger)) {
          this._notificationService.warn('', Constants.BILLET_ORDER_2_LIMIT_SELL);
          return false;
        }
      }
      if (this.stopLoss) {
        if (Number(this.model.stoptrigger) > this.lastTrade) {
          this._notificationService.warn('', Constants.BILLET_ORDER_1_TRIGGER_SELL);
          return false;
        }

        if (Number(this.model.stoplimit) > Number(this.model.stoptrigger)) {
          this._notificationService.warn('', Constants.BILLET_ORDER_1_LIMIT_SELL);
          return false;
        }
      }
      return true;
    }
  }

  public validSuitability() {
    this.isRequest = true;
    this.loadingEmitOrder = false;
    if (this.accounBrokerList && this.accounBrokerList.market === 'XBSP' && this.model && this.model.market === 1) {
      this.accountSelectBroker = this.accounBrokerList;
    } else if (this.accounBrokerList && this.accounBrokerList.market === 'XBMF' && this.model.market === 3) {
      this.accountSelectBroker = this.accounBrokerList;
    }
    if ((this.accountSelectBroker.validateSuitability && this.accountSelectBroker.suitabilityID) || this._configService.isLoginTypeWSO2()) {
      let profileName = '';
      let client_id = 0;
      if (this._configService.isLoginTypeWSO2() && this._configService.isUserAssessorLogged()) {
        profileName = this.accountSelectBrokerAssessor.user.profile_name;
        client_id = this.accountSelectBrokerAssessor.user.client_id;
      } else {
        client_id = Number(this._configService.getHbAccountId());
      }
      this._authServer.iNegotiation
        .suitabilityCheckMatrix(this.model.account, this.model.market.toString(), null, 0, this.model.quote,
          this.accountSelectBroker.suitabilityID, profileName, client_id)
        .subscribe(res => {
          if (res && this._configService.isLoginTypeWSO2() && !res.is_out_of_bounds && res._isScalar !== false) {
            this.emitOrder();
          } else if (res.code && res.code.toString() === '200' && res.message) {
            this.emitOrder();
          } else if (res && ((res.is_out_of_bounds && res._isScalar !== false)) || (res.code.toString() === '200' && !res.message)) {
            this.closeConfirmation();
            this.confirmModalOpened = false;
            this.termOperator = true;
          } else {
            this.isRequest = false;
            this.loadingEmitOrder = false;
            this.termOperator = false;
            this._notificationService.error('', 'Algum erro ocorreu!');
            return false;
          }
        }, error => {
          this.isRequest = false;
          this.loadingEmitOrder = false;
          this.termOperator = false;
          this._notificationService.error('', 'Algum erro ocorreu!');
          return false;
        });
    } else if (this.accountSelectBroker.validateSuitability && !this.accountSelectBroker.suitabilityID) {
      this._notificationService.warn('', 'Você deve preencher seu PERFIL de SUITABILITY para enviar ordem.');
      return false;
    } else {
      this.emitOrder();
    }
  }

  public emitOrder() {
    this.preventDoubleClick = true;
    if (this.termOperator && !this.checkTermOperator) {
      this._notificationService.warn('', 'O termo de ciência deve ser aceito.');
      return false;
    }
    this.model.bypasssuitability = this.checkTermOperator;
    if (!this._configService.isLoginTypeWSO2()) {
      this.loadingEmitOrder = true;
    }
    if (!this.model.market) {
      this._notificationService.warn('', Constants.WORKSPACE_MSG020);
      return false;
    }
    if (!this.model.account) {
      this._notificationService.warn('', Constants.WORKSPACE_MSG021);
      return false;
    }
    if (this.model.quote === null) {
      this._notificationService.warn('', Constants.WORKSPACE_MSG005);
      return false;
    } else {
      let GainTrigger: string;
      let GainExecution: string;
      let LossTrigger: string;
      let LossExecution: string;
      if (this.stopLoss && this.stopGain) {
        if (this.model.side === SideEnum.BUY) {
          LossTrigger = this.model.stoptrigger.toString();
          LossExecution = this.model.stoplimit.toString();
          GainTrigger = this.model.targettrigger.toString();
          GainExecution = this.model.targetlimit.toString();
        } else if (this.model.side === SideEnum.SELL) {
          GainTrigger = this.model.targettrigger.toString();
          GainExecution = this.model.targetlimit.toString();
          LossTrigger = this.model.stoptrigger.toString();
          LossExecution = this.model.stoplimit.toString();
        }
      } else if (this.stopGain) {
        // Only for up
        if (this.model.side === SideEnum.BUY) {
          LossTrigger = null;
          LossExecution = null;
          GainTrigger = this.model.targettrigger.toString();
          GainExecution = this.model.targetlimit.toString();
        } else if (this.model.side === SideEnum.SELL) {
          GainTrigger = this.model.targettrigger.toString();
          GainExecution = this.model.targetlimit.toString();
          LossTrigger = '0';
          LossExecution = '0';
        }
      } else {
        // Only for down
        if (this.model.side === SideEnum.BUY) {
          LossTrigger = this.model.stoptrigger.toString();
          LossExecution = this.model.stoplimit.toString();
          GainTrigger = '0';
          GainExecution = '0';
        } else if (this.model.side === SideEnum.SELL) {
          GainTrigger = null;
          GainExecution = null;
          LossTrigger = this.model.stoptrigger.toString();
          LossExecution = this.model.stoplimit.toString();
        }
      }
      if (this._configService.isLoginTypeWSO2()) {
        let client_id = 0;
        let user_id = 0;
        if (this._configService.isLoginTypeWSO2() && this._configService.isUserAssessorLogged()) {
          client_id = this.accountSelectBrokerAssessor.user.client_id;
          user_id = Number(this._configService.getAssessorId());
        } else {
          client_id = Number(this._configService.getHbAccountId());
          user_id = Number(this._configService.getHbUserId());
        }
        if (!this.validSignature && !this.model.signature && !this.isAssessor) {
          this.isRequest = false;
          this._notificationService.warn('', Constants.BILLET_ORDER_EMPTY_SIGNATURE);
          return false;
        }
        if (!this.validSignature && !this.isAssessor) {
          this.isRequest = false;
          this._notificationService.error('', Constants.BILLET_ORDER_VALID_SIGNATURE);
          return false;
        }
        if (this.fractFlag && !this.fullFlag) {
          this.model.qtd = this.fractionaryQtd;
          this.model.quote = this.model.quote.toLocaleUpperCase() + 'F';
        } else if (this.fullFlag && !this.fractFlag) {
          this.model.qtd = this.nonFractionaryQtd;
        }
        if (!this.isStop) {
          this._authServer.iNegotiation.sendOrderWso2(this.model.account, this.model.quote, this.fractionaryQtd, this.nonFractionaryQtd,
            this.model.price.toString(), this.model.market, this.model.type, this.model.side, this.model.validityOrder,
            this.model.orderStrategy, this.model.timeInForces, this.model.orderid, this.model.origclordid, this.model.loteDefault,
            this.model.maxfloor > 0 ? `${this.model.maxfloor}` : null, this.model.minqtd > 0 ? `${this.model.minqtd}` : null,
            this.model.orderTag, null, null, null, null, false, this.model.bypasssuitability)
            .subscribe(res => {
              if (res.code === 1) {
                if (this.fractionaryQtd > 0) {
                  this._authServer.iNegotiation.sendOrderWso2(this.model.account, this.model.quote + 'F', this.fractionaryQtd, 0,
                    this.model.price.toString(), this.model.market, this.model.type, this.model.side, this.model.validityOrder,
                    this.model.orderStrategy, this.model.timeInForces, this.model.orderid,
                    this.model.origclordid, this.model.loteDefault, null, null, null, null, null, null, null, false,
                    this.model.bypasssuitability)
                    .subscribe(res2 => {
                      if (res2.code === 1) {
                        this.callAlert(this.titleMessageorder, this.messageorder);
                      }
                      if (this.checkTermOperator && this._configService.isLoginTypeWSO2()) {
                        this._authServer.iNegotiation.postAssignBoundsWso2(this.model.account, this.model.quote, client_id, user_id)
                          .subscribe(res3 => {
                            if (res3) {
                              this._notificationService.success('', 'Termo de aceite enviado com sucesso!');
                              this.checkTermOperator = false;
                            }
                          }, error => {
                            this._notificationService.error('', 'Ocorreu um erro ao enviar o termo de aceite!');
                          });
                      }
                      this.isRequest = false;
                      this.termOperator = false;
                    }, error => {
                      this.isRequest = false;
                      this.termOperator = false;
                    });
                } else {
                  this.callAlert(this.titleMessageorder, this.messageorder);
                  if (this.checkTermOperator && this._configService.isLoginTypeWSO2()) {
                    this._authServer.iNegotiation.postAssignBoundsWso2(this.model.account, this.model.quote, client_id, user_id)
                      .subscribe(res3 => {
                        if (res3) {
                          this._notificationService.success('', 'Termo de aceite enviado com sucesso!');
                          this.checkTermOperator = false;
                        }
                      }, error => {
                        this._notificationService.error('', 'Ocorreu um erro ao enviar o termo de aceite!');
                      });
                  }
                }
                this.symbol = null;
                this.closeConfirmation();
                this.buySellRef.dismiss()
                this.isRequest = false;
                this.termOperator = false;
              } else {
                this._notificationService.error('', 'Algum erro ocorreu!');
                this.isRequest = false;
                this.termOperator = false;
              }
            }, error => {
              this.isRequest = false;
              this.termOperator = false;
            });
        } else if (this.stopOrderValidator()) {
          this._authServer.iNegotiation.sendOrderWso2(this.model.account, this.model.quote, this.fractionaryQtd, this.nonFractionaryQtd,
            null, this.model.market, this.model.type, this.model.side, this.model.validityOrder, this.model.orderStrategy,
            this.model.timeInForces, this.model.orderid, this.model.origclordid, this.model.loteDefault,
            this.model.maxfloor > 0 ? `${this.model.maxfloor}` : null, this.model.minqtd > 0 ? `${this.model.minqtd}` : null,
            this.model.orderTag, GainTrigger, GainExecution, LossTrigger, LossExecution, true, this.model.bypasssuitability)
            .subscribe(res => {
              if (res.code === 1) {
                if (this.fractionaryQtd > 0) {
                  this._authServer.iNegotiation.sendOrderWso2(this.model.account, this.model.quote + 'F', this.fractionaryQtd, 0, null,
                    this.model.market, this.model.type, this.model.side, this.model.validityOrder, this.model.orderStrategy,
                    this.model.timeInForces, this.model.orderid, this.model.origclordid, this.model.loteDefault, null, null, null,
                    GainTrigger, GainExecution, LossTrigger, LossExecution, true, this.model.bypasssuitability)
                    .subscribe(res2 => {
                      if (res2.code === 1) {
                        this.callAlert(this.titleMessageorder, this.messageorder);
                      }
                      if (this.checkTermOperator && this._configService.isLoginTypeWSO2()) {
                        this._authServer.iNegotiation.postAssignBoundsWso2(this.model.account, this.model.quote, client_id, user_id)
                          .subscribe(res3 => {
                            if (res3) {
                              this._notificationService.success('', 'Termo de aceite enviado com sucesso!');
                              this.checkTermOperator = false;
                            }
                          }, error => {
                            this._notificationService.error('', 'Ocorreu um erro ao enviar o termo de aceite!');
                          });
                      }
                      this.isRequest = false;
                      this.termOperator = false;
                    }, error => {
                      this.isRequest = false;
                      this.termOperator = false;
                    });
                } else {
                  this.callAlert(this.titleMessageorder, this.messageorder);
                  if (this.checkTermOperator && this._configService.isLoginTypeWSO2()) {
                    this._authServer.iNegotiation.postAssignBoundsWso2(this.model.account, this.model.quote, client_id, user_id)
                      .subscribe(res3 => {
                        if (res3) {
                          this._notificationService.success('', 'Termo de aceite enviado com sucesso!');
                          this.checkTermOperator = false;
                        }
                      }, error => {
                        this._notificationService.error('', 'Ocorreu um erro ao enviar o termo de aceite!');
                      });
                  }
                }
                this.lastTrade = 0;
                this.symbol = null;
                this.closeConfirmation();
                this.buySellRef.dismiss()
              } else {
                this._notificationService.error('', 'Algum erro ocorreu!');
              }
              this.isRequest = false;
              this.termOperator = false;
            },
              error => {
                this.isRequest = false;
                this.termOperator = false;
              });
        }
      } else {
        // API
        if (this.fractFlag && !this.fullFlag) {
          this.model.qtd = this.fractionaryQtd;
          this.model.quote = this.model.quote.toLocaleUpperCase() + 'F';
        } else if (this.fullFlag && !this.fractFlag) {
          this.model.qtd = this.nonFractionaryQtd;
        }
        if (!this.isStop) {
          this._authServer.iNegotiation.sendOrderApi(this.model.account, this.model.quote, this.model.qtd,
            this.model.price.toString(), this.model.market, this.model.type, this.model.side,
            this.model.validityOrder, this.model.orderStrategy, this.model.timeInForces,
            this.model.orderid, this.model.origclordid, this.model.loteDefault,
            this.model.maxfloor > 0 ? `${this.model.maxfloor}` : null,
            this.model.minqtd > 0 ? `${this.model.minqtd}` : null, this.model.orderTag, this.model.bypasssuitability)
            .subscribe(res => {
              if (res.code === 1) {
                if (res.message && res.message.split(';').length > 1) {
                  const msg01 = JSON.parse(res.message.split(';')[0]);
                  const msg02 = JSON.parse(res.message.split(';')[1]);
                  if (msg01 && msg01.code === 1 && msg02 && msg02.code === 1) {
                    this.callAlert('Ordens dos ativos ' + this.model.quote.toUpperCase() +
                      ' e ' + this.model.quote.toUpperCase() + 'F enviadas.', this.messageorder);
                  }
                } else {
                  try {
                    if (JSON.parse(res.message)) {
                      this.callAlert(this.titleMessageorder, this.messageorder);
                    }
                  } catch {
                    this.callAlert(this.titleMessageorder, this.messageorder);
                  }
                }
                this.symbol = null;
                this.closeConfirmation();
                this.buySellRef.dismiss()
              } else if (res.code === 2) {
                if (res.message && res.message.split(';').length > 0) {
                  const msg01 = JSON.parse(res.message.split(';')[0]);
                  const msg02 = JSON.parse(res.message.split(';')[1]);
                  if (msg01 && msg01.code === 1) {
                    this._notificationService.success('', msg01.message);
                  } else {
                    this._notificationService.warn('', msg01.message);
                  }
                  if (msg02 && msg02.code === 1) {
                    this._notificationService.success('', msg02.message);
                  } else {
                    this._notificationService.warn('', msg02.message);
                  }
                }
                this.closeConfirmation();
                this.buySellRef.dismiss()
              } else if (res.code === 3 && res.message) {
                this.callAlert('', 'Ordens dos ativos ' + this.model.quote.toUpperCase() +
                  ' e ' + this.model.quote.toUpperCase() + 'F NÃO recebidas. ' + this.messageorder);
              } else {
                this._notificationService.error('', 'Ocorreu um erro no envio da ordem.');
              }
              this.loadingEmitOrder = false;
              this.isRequest = false;
              this.termOperator = false;
            },
              error => {
                this.isRequest = false;
                this.loadingEmitOrder = false;
                this.termOperator = false;
              });
        } else if (this.stopOrderValidator()) {
          this._authServer.iNegotiation.sendOrderStopApi(this.model.account, this.model.validityOrder, this.model.orderStrategy,
            this.model.quote, this.model.qtd, GainTrigger, GainExecution, LossTrigger, LossExecution, this.model.market,
            this.model.side, this.model.timeInForces, this.model.orderid, this.model.origclordid, this.model.loteDefault, null,
            this.model.maxfloor > 0 ? `${this.model.maxfloor}` : null,
            this.model.minqtd > 0 ? `${this.model.minqtd}` : null, this.model.orderTag, this.model.bypasssuitability)
            .subscribe(res => {
              if (res.code === 1) {
                if (res.message && res.message.split(';').length > 1) {
                  const msg01 = JSON.parse(res.message.split(';')[0]);
                  const msg02 = JSON.parse(res.message.split(';')[1]);
                  if (msg01 && msg01.code === 1 && msg02 && msg02.code === 1) {
                    this.callAlert('Ordens dos ativos ' + this.model.quote.toUpperCase() +
                      ' e ' + this.model.quote.toUpperCase() + 'F recebidas.', this.messageorder);
                  }
                } else {
                  try {
                    if (JSON.parse(res.message)) {
                      this.callAlert(this.titleMessageorder, this.messageorder);
                    }
                  } catch {
                    this.callAlert(this.titleMessageorder, this.messageorder);
                  }
                }
                this.symbol = null;
                this.lastTrade = 0;
                this.symbol = null;
                this.closeConfirmation();
                this.buySellRef.dismiss();
              } else if (res.code === 2) {
                if (res.message && res.message.split(';').length > 0) {
                  const msg01 = JSON.parse(res.message.split(';')[0]);
                  const msg02 = JSON.parse(res.message.split(';')[1]);
                  if (msg01 && msg01.code === 1) {
                    this.callAlert(msg01.message, this.messageorder);
                  } else {
                    this._notificationService.warn('', msg01.message);
                  }
                  if (msg02 && msg02.code === 1) {
                    this.callAlert(msg02.message, this.messageorder);
                  } else {
                    this._notificationService.warn('', msg02.message);
                  }
                }
                this.symbol = null;
                this.lastTrade = 0;
                this.symbol = null;
                this.closeConfirmation();
                this.buySellRef.dismiss()
              } else if (res.code === 3 && res.message) {
                this.callAlert('', 'Ordens dos ativos ' + this.model.quote.toUpperCase() +
                  ' e ' + this.model.quote.toUpperCase() + 'F NÃO recebidas. ' + this.messageorder);
              } else {
                this._notificationService.error('', 'Ocorreu um erro no envio da ordem.');
              }
              this.isRequest = false;
              this.termOperator = false;
              this.loadingEmitOrder = false;
            },
              error => {
                this.loadingEmitOrder = false;
                this.isRequest = false;
                this.termOperator = false;
              });
        } else {
          this.loadingEmitOrder = false;
          this.preventDoubleClick = false;
          this.isRequest = false;
          this.termOperator = false;
        }
      }
    }
  }

  callAlert(title, message) {
    const alertMsg = this._notificationService.alert(title, message);
    alertMsg.click.subscribe((data) => {
      this._configService._clickOpenSideMenu.next(2);
      this.closeModal();
    });
  }

  keyPressed(event: any) {
    const inputChar = String.fromCharCode(event.charCode);
    if (inputChar !== '-' && !this.inputQuotePattern.test(inputChar)) {
      event.preventDefault();
    }
  }

  onPaste(event) {
    this.keyPressed(event);
    const content = event.clipboardData.getData('text/plain');
    if (!this.inputQuotePattern.test(content)) {
      event.preventDefault();
    }
  }

  tooltipValidator() {
    if (this.isBuy && this.isStop) {
      if (this.stopLoss) {
        if ((this.model.stoptrigger < this.lastTrade) || (this.model.stoplimit < this.model.stoptrigger)) {
          if ((this.model.stoptrigger < this.lastTrade)) {
            this.lossTrigger = false;
          } else {
            this.lossTrigger = true;
          }
          if (this.model.stoplimit < this.model.stoptrigger) {
            this.lossLimit = false;
          } else {
            this.lossLimit = true;
          }
        } else {
          this.lossTrigger = true;
          this.lossLimit = true;
        }
      }
      if (this.stopGain) {
        if ((this.model.targettrigger > this.lastTrade) || (this.model.targetlimit < this.model.targettrigger)) {
          if (this.model.targettrigger > this.lastTrade) {
            this.gainTrigger = false;
          } else {
            this.gainTrigger = true;
          }
          if (this.model.targetlimit < this.model.targettrigger) {
            this.gainLimit = false;
          } else {
            this.gainLimit = true;
          }
        } else {
          this.gainTrigger = true;
          this.gainLimit = true;
        }
      }
    } else
      if (!this.isBuy && this.isStop) {
        if (this.stopLoss) {
          if ((this.model.stoptrigger > this.lastTrade) || (this.model.stoplimit > this.model.stoptrigger)) {
            if ((this.model.stoptrigger > this.lastTrade)) {
              this.lossTrigger = false;
            } else {
              this.lossTrigger = true;
            }
            if (this.model.stoplimit > this.model.stoptrigger) {
              this.lossLimit = false;
            } else {
              this.lossLimit = true;
            }
          } else {
            this.lossTrigger = true;
            this.lossLimit = true;
          }
        }
        if (this.stopGain) {
          if ((this.model.targettrigger < this.lastTrade) || (this.model.targetlimit > this.model.targettrigger)) {
            if (this.model.targettrigger < this.lastTrade) {
              this.gainTrigger = false;
            } else {
              this.gainTrigger = true;
            }
            if (this.model.targetlimit > this.model.targettrigger) {
              this.gainLimit = false;
            } else {
              this.gainLimit = true;
            }
          } else {
            this.gainTrigger = true;
            this.gainLimit = true;
          }
        }
      }
  }

  optionSelected(event) {
    const symbol = event.option.value.toLowerCase();
    const messageQuoteQuery = new MessageQuoteQuery();
    this.model.quote = symbol;
    this.SetCompany(this.model.quote.toUpperCase());
    this.filteredAssets = [];
    messageQuoteQuery.symbol = this.model.quote.toUpperCase();
    messageQuoteQuery.company = this.company;
    this.onsymbolChanged.emit(messageQuoteQuery);
    this.hiddenInvalid = true;
  }

  private SetCompany(symbolAutoComplete: string) {
    if (!this.filteredAssets && this.filteredAssets.length === 0) {
      return;
    }
    const filtered = this.filteredAssets.find(s => {
      if (s.symbol) {
        return s.symbol.toUpperCase() === symbolAutoComplete;
      }
    });
    if (filtered) {
      this.company = filtered.company;
    }
  }

  disableConfirmButton() {
    if (!this.isStop) {
      if ((!this.fullFlag && !this.fractFlag) ||
        (!this.nonFractionary && !this.fractFlag) ||
        (!this.fullFlag && this.fractionaryQtd === 0)) {
        return true;
      }
    } else {
      if (this.stopGain && this.stopLoss) {
        if (this.nonFractionaryLoss && this.nonFractionaryGain && this.fractionaryQtdLoss !== 0 && this.fractionaryQtdGain !== 0) {
          if (!this.fullFlagLoss && !this.fullFlagGain && !this.fractFlagLoss && !this.fractFlagGain) {
            return true;
          }
        } else {
          if (this.nonFractionaryLoss && this.nonFractionaryGain) {
            if (!this.fullFlagLoss && !this.fullFlagGain) {
              return true;
            }
          } else if (this.fractionaryQtdLoss !== 0 && this.fractionaryQtdGain !== 0) {
            if (!this.fractFlagLoss && !this.fractFlagGain) {
              return true;
            }
          }
        }
      } else {
        if (this.stopGain) {
          if (this.nonFractionaryGain && this.fractionaryQtdGain !== 0) {
            if (!this.fullFlagGain && !this.fractFlagGain) {
              return true;
            }
          } else {
            if (this.nonFractionaryGain) {
              if (!this.fullFlagGain) {
                return true;
              }
            } else if (this.fractionaryQtdGain !== 0) {
              if (!this.fractFlagGain) {
                return true;
              }
            }
          }
        } else if (this.stopLoss) {
          if (this.nonFractionaryLoss && this.fractionaryQtdLoss !== 0) {
            if (!this.fullFlagLoss && !this.fractFlagLoss) {
              return true;
            }
          } else {
            if (this.nonFractionaryLoss) {
              if (!this.fullFlagLoss) {
                return true;
              }
            } else if (this.fractionaryQtdLoss !== 0) {
              if (!this.fractFlagLoss) {
                return true;
              }
            }
          }
        }
      }
    }
    return false;
  }

  validateSignatureValid(signature: string) {
    if (signature) {
      this.loadingConfirmation = true;
      const clientId = Number(this._configService.getHbAccountId());
      const params = { user_client_id: clientId, signature: signature };
      this._authenticationService.validateSignatureValid(params)
        .subscribe(rest => {
          if (rest && rest._isScalar !== false) {
            this.validSignature = rest.is_valid;
            this.loadingConfirmation = false;
            if (this.validSignature) {
              this.fullSignatureFlag(this.model.signature, this.validSignature);
            } else {
              this.fullSignatureFlag(this.model.signature, false);
              this._configService.deleteHbSignature();
              this.model.signature = null;
              this._notificationService.error('', 'Assinatura Eletrônica inválida!');
            }
          } else {
            this.fullSignatureFlag(this.model.signature, false);
            this._configService.deleteHbSignature();
            this.validSignature = false;
            this.loadingConfirmation = false;
            this.model.signature = null;
            this._notificationService.error('', 'Ocorreu um erro ao validar a Assinatura Eletrônica!');
          }
        }, error => {
          this.fullSignatureFlag(this.model.signature, false);
          this._configService.deleteHbSignature();
          this._notificationService.error('', 'Ocorreu um erro ao validar a Assinatura Eletrônica!');
          this.model.signature = null;
          this.loadingConfirmation = false;
        });
    }
  }

  stopPropagation(event) {
    event.stopPropagation();
  }

  forgotSignature() {
    const dialogConfig = this.dialog.open(ModalSignatureComponent, {
      data: {
        title: Constants.MSG01_FORGOT_SIGNATURE_TITLE,
        content: Constants.MSG01_FORGOT_SIGNATURE_CONTENT,
      },
      panelClass: Constants.NORMAL_MODAL,
      width: '590px',
      height: '315px',
      disableClose: true
    });
  }

  closeSuitability() {
    this.closeConfirmation();
    this.isRequest = false;
    this.termOperator = false;
  }

  changeAccount(account) {
    if (account && account !== this.acccountBalanceSubscribe) {
      this._financialService.unsubscribeFinancial(this.acccountBalanceSubscribe);
      this.limiteOperacional = '-';
      this._configService.setBrokerAccountSelected(account);
      this._configService.setBrokerAccountSelectedTransition(account);
      this.getBalanceValue(account);
      this.acccountBalanceSubscribe = account;
      this._configService._changeAccountBillet.next(account);
      this._configService.writeLog('BOLETA > changeAccount: ' + account);
    }
  }

}
