import {
  Component,
  EventEmitter,
  HostListener,
  Inject,
  Input,
  OnInit,
  Output
} from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Router } from '@angular/router';
import { AutoLogoutService } from '@auth/auto-logout-service.service';
import { CustomWorkspaceItemDto } from '@dto/custom-workspace-item.dto';
import { PositionDto } from '@dto/position.dto';
import { WorkspaceItemContentDto } from '@dto/workspace-item-content-dto';
import { WorkspaceNewSaveDto } from '@dto/workspace-new-save-dto';
import { MessageQuoteQuery } from '@model/webFeeder/message-quote-query.model';
import { QuoteService } from '@services/webFeeder/sckt-quotes.service';
import { WorkspaceNewService } from '@services/workspace-new.service';
import { Constants } from '@utils/constants';
import { NotificationsService } from '../../../../node_modules/angular2-notifications';
import { AuthService } from '../../auth/auth.service';
import { CustomWorkspaceItemInterface } from '../../dto/custom-workspace-item-interface.dto';
import { CustomWorkspaceItemMultipleDto } from '../../dto/custom-workspace-item-multiple.dto';
import { WorkspaceNewDto } from '../../dto/workspacenew.dto';
import { ConfigService } from '../../services/webFeeder/config.service';
import { AbstractItemService } from '@business/base/abstract-item.service';
import { AbstractItem } from '@business/base/abstract-item';
import { NormalModalComponent } from '@component/normal-modal/normal-modal.component';
import { AuthServer } from '@services/auth-server.service';

@Component({
  selector: 'app-custom-workspace-modal',
  templateUrl: './custom-workspace-modal.component.html'
})
export class CustomWorkspaceModalComponent extends AbstractItem implements OnInit {
  @Input()
  symbolAutoComplete: string;
  @Input()
  hiddenError: boolean;
  @Output()
  onsymbolChanged = new EventEmitter<MessageQuoteQuery>();
  @Input()
  wksSelectedId: number;
  workspaceName: string;

  dialog: MatDialog;
  public isRequesting = false;
  public filteredAssets: any;
  public company: string;
  public hiddenInvalid: boolean;
  public wksOptSelected: number;
  public loading = false;
  public componentsListBeforeUpdate: Array<WorkspaceItemContentDto>;
  public componentsListAfterUpdate: Array<WorkspaceItemContentDto>;
  public componentsListDifference: Array<WorkspaceItemContentDto>;

  public cardTitles = [
    { id: Constants.WINDOW_NAME_BOOK, title: Constants.TITLE_CARD_BOOK },
    {
      id: Constants.WINDOW_NAME_WORK_SHEET,
      title: Constants.TITLE_CARD_WORKSHEET
    },
    {
      id: Constants.WINDOW_NAME_VOLUME_AT_PRICE,
      title: Constants.TITLE_CARD_VOLUME_AT_PRICE
    },
    {
      id: Constants.WINDOW_NAME_TIMES_TRADES,
      title: Constants.TITLE_CARD_TIMES_TRADES
    },
    {
      id: Constants.WINDOW_NAME_ASSET_SUMMARY,
      title: Constants.TITLE_CARD_ASSET_SUMMARY
    },
    {
      id: Constants.WINDOW_NAME_QUOTE_CHART,
      title: Constants.TITLE_CARD_QUOTE_CHART
    }
  ];

  public listCustomWorkspaceDto: Array<CustomWorkspaceItemInterface> = [];
  itemId = 0;

  @HostListener('mousemove')
  onMouseMove() {
    this._autoLogoutService.resetTime();
  }

  @HostListener('click')
  onClick() {
    this._autoLogoutService.resetTime();
  }

  constructor(
    dialog: MatDialog,
    public dialogRef: MatDialogRef<CustomWorkspaceModalComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private quoteService: QuoteService,
    workspaceService: WorkspaceNewService,
    private router: Router,
    private authService: AuthService,
    private notificationService: NotificationsService,
    private configService: ConfigService,
    private _autoLogoutService: AutoLogoutService,
    itemService: AbstractItemService,
    public _NotificationService: NotificationsService,
    private _authServer: AuthServer,
  ) {
    super(workspaceService, dialog, itemService);
    if (!this.authService.isAuthenticated) {
      this.configService.deleteBrokerCookies();
      this.configService.deleteCookie();
      if (this.configService.isLoginTypeWSO2() && this.configService.isUserAssessorLogged()) {
        window.location.href = this.configService.REDIRECT_LOGOUT + '/assessor/login';
      } else if (this.configService.isLoginTypeWSO2() && !this.configService.isUserAssessorLogged()) {
        window.location.href = this.configService.REDIRECT_LOGOUT + '/client/login';
      } else {
        this.router.navigate(['/login']);
      }
    } else {
      this.symbolAutoComplete = '';
      this.hiddenError = true;
      this.dialog = dialog;
    }
    this.workspaceName = data ? data.customNameWorkspace : '';
  }

  ngOnInit() { }

  @HostListener('body:keydown', ['$event'])
  onKeyDownHandler(event: KeyboardEvent) {
    if (event.key === 'Escape') {
      if (!this.workspaceService.validNumberWorkspace() && this.listCustomWorkspaceDto.length <= 0 && this.data.wksOptSelected === 3) {
        this._NotificationService.info('', Constants.WORKSPACE_MSG001);
        return false;
      } else {
        // this.dialogRef.close();
        this.showConfirmClose();
      }
    }
  }

  public goBack() {
    this.configService._wksBackModal.next(true);
    this.dialogRef.close(false);
  }

  public closeModal(): void {
    this.dialogRef.close(true);
    this.configService._wksBackModal.next(true);
  }

  showConfirmClose() {
    if (this.listCustomWorkspaceDto.length === 0) {
      this.closeModal();
    } else {
      const dialogConfig = this.dialog.open(NormalModalComponent, {
        data: {
          title: '',
          body: Constants.WORKSPACE_MSG016,
          redirectUrl: Constants.HOME_WKS_PATH,
          queryParams: { workspaceId: this.workspaceService.getWksSelectedId(), closeModal: true }
        },
        panelClass: Constants.NORMAL_MODAL,
      }).afterClosed().subscribe(res => {
        if (res) {
          this.dialogRef.close(true);
        }
      });
    }
  }

  public onAutoCompletAsset(textInput: any) {
    this.hiddenInvalid = true;
    if (textInput.key !== 'ArrowDown' && textInput.key !== 'ArrowUp') {
      this.loading = true;
      let symbolAutoComplete = '';
      if (textInput.target === undefined) {
        symbolAutoComplete = textInput.toLowerCase();
      } else {
        symbolAutoComplete = textInput.target.value.toLowerCase();
      }
      if (symbolAutoComplete) {
        this._authServer.iQuote.quotesQuery(symbolAutoComplete)
          .subscribe(datas => {
            if (datas && datas.length > 0) {
              this.filteredAssets = datas;
            }
          });
      }
      if (symbolAutoComplete.length >= 5) {
        if (this.filteredAssets.length === 0) {
          this.hiddenInvalid = false;
        } else {
          this.hiddenInvalid = true;
        }
      }
      if (
        this.filteredAssets === undefined ||
        this.filteredAssets.length === 0
      ) {
        this.company = '';
      }
      this.loading = false;
      return this.filteredAssets;
    }
  }

  onInputEnter(event) {
    const symbol = event.target.value.toLowerCase();
    this._authServer.iQuote.quotesQuery(symbol).subscribe(datas => {
      if (datas && datas.length > 0) {
        const data = datas.filter(item => item.symbol === symbol.toUpperCase());
        if (data.length === 1) {
          this.onsymbolChanged.emit(datas[0]);
        }
      }
    });
  }

  saveMultiOptionWorkSheet(
    value: any,
    currentWorksheet: CustomWorkspaceItemMultipleDto
  ) {
    for (let i = 0; i < value.length; i++) {
      currentWorksheet.assetSymbol.push(value[i].symbol);
      currentWorksheet.company.push(value[i].company);
    }
  }

  isNewOrUpdate() {
    this.isRequesting = true;
    if (!this.data || !this.data.customNameWorkspace) {
      this.addItemWorkspace();
    } else {
      this.saveWorkspace();
    }
  }

  callToScrollDown() {
    this.workspaceService.callScrollDownMethod();
  }

  addItemWorkspace() {
    const newWorkspace: WorkspaceNewDto = this.workspaceService.getWorkspaceSelected();
    const contentToUpdate: Array<WorkspaceItemContentDto> = JSON.parse(newWorkspace.JsonData).content;
    this.componentsListBeforeUpdate = contentToUpdate.map(x => Object.assign({}, x));
    if (
      contentToUpdate.length > Constants.NUM_MAX_WORKSPACE_CARDS ||
      contentToUpdate.length + this.listCustomWorkspaceDto.length >
      Constants.NUM_MAX_WORKSPACE_CARDS
    ) {
      this.notificationService.warn('', Constants.WORKSPACE_MSG011);
      this.isRequesting = false;
    } else {
      const workspaceUpdated: WorkspaceNewSaveDto = new WorkspaceNewSaveDto(
        newWorkspace.Name,
        [],
        newWorkspace.IndexTab
      );
      if (contentToUpdate) {
        for (let j = 0; j < contentToUpdate.length; j++) {
          workspaceUpdated.content.push(contentToUpdate[j]);
        }
        for (let i = 0; i < this.listCustomWorkspaceDto.length; i++) {
          const itemSettings = this.workspaceService.getItemDefaultSettings(
            this.listCustomWorkspaceDto[i].type
          );
          const wksItemPos: PositionDto = this.workspaceService.mountItemPositionDefaultSettings(
            itemSettings
          );
          let company: string;
          if (!this.listCustomWorkspaceDto[i].company) {
            company = '';
          } else {
            company = this.listCustomWorkspaceDto[i].company.toString();
          }
          const wksItemObj: WorkspaceItemContentDto = new WorkspaceItemContentDto(
            this.workspaceService.getNextContentItemId(
              workspaceUpdated.content
            ),
            this.listCustomWorkspaceDto[i].assetSymbol.toString(),
            company,
            this.listCustomWorkspaceDto[i].type,
            wksItemPos,
            (itemSettings && itemSettings.showInGridster) || false,
            newWorkspace.Id || 0
          );
          workspaceUpdated.content.push(wksItemObj);
          this.componentsListAfterUpdate = workspaceUpdated.content.map(x => Object.assign({}, x));
        }
        if (
          workspaceUpdated.content.filter(
            x => x.type === Constants.WINDOW_NAME_WORK_SHEET
          ).length > Constants.QTD_MAX_WORKSHEET
        ) {
          this.notificationService.warn('', Constants.WORKSPACE_MSG012);
          this.isRequesting = false;
        } else {
          this.router.navigate([Constants.HOME_WKS_PATH], {
            queryParams: {
              workspaceId: newWorkspace.Id,
              addCmp: true,
              workspaceUpdated: JSON.stringify(workspaceUpdated)
            }
          });
          this.callToScrollDown();
          this.isRequesting = false;
          this.closeModal();
          this.configService._wksAddNewComponentModal.next(true);
        }
      }
    }
  }

  saveWorkspace() {
    let nameWorkspace: string;
    this.workspaceService
      .loadWorkspaces(this.authService.getUserLogged(), false)
      .subscribe(resp => {
        this.loading = true;
        let indexTabNew = 0;
        for (let i = 0; i < resp.length; i++) {
          if (resp[i].IndexTab > indexTabNew) {
            indexTabNew = resp[i].IndexTab;
          }
        }
        indexTabNew = indexTabNew + 1;

        nameWorkspace = this.data.customNameWorkspace;
        const wksNewObj: WorkspaceNewSaveDto = new WorkspaceNewSaveDto(nameWorkspace, [], indexTabNew);
        if (this.listCustomWorkspaceDto.filter(x => x.type === Constants.WINDOW_NAME_WORK_SHEET).length > Constants.QTD_MAX_WORKSHEET) {
          this.notificationService.warn('', Constants.WORKSPACE_MSG012);
          this.isRequesting = false;
        } else {
          for (let i = 0; i < this.listCustomWorkspaceDto.length; i++) {
            const itemSettings = this.workspaceService.getItemDefaultSettings(
              this.listCustomWorkspaceDto[i].type
            );
            const wksItemPos: PositionDto = this.workspaceService.mountItemPositionDefaultSettings(
              itemSettings
            );
            // The index i is the card workspace id
            let company: string;
            if (!this.listCustomWorkspaceDto[i].company) {
              company = '';
            } else {
              company = this.listCustomWorkspaceDto[i].company.toString();
            }
            const wksItemObj: WorkspaceItemContentDto = new WorkspaceItemContentDto(
              i,
              this.listCustomWorkspaceDto[i].assetSymbol.toString(),
              company,
              this.listCustomWorkspaceDto[i].type,
              wksItemPos,
              (itemSettings && itemSettings.showInGridster) || false,
              0 // not saved yet
            );
            wksNewObj.content.push(wksItemObj);
          }
          this.workspaceService.saveWorkspace(wksNewObj).subscribe(data => {
            if (data) {
              this.closeModal();
              this.isRequesting = false;
              this.router.navigate([Constants.HOME_WKS_PATH], {
                queryParams: {
                  workspaceId: data.Id
                }
              });
            }
          });
          this.configService._wksAddNewComponentModal.next(true);
        }
      });
  }

  hasEqualComponentOnList(symbol: string, type: string, list: any) {
    const node = this.listCustomWorkspaceDto.findIndex(elem =>
      elem.assetSymbol === symbol &&
      elem.type === type);
    if (node === -1) {
      return false;
    } else {
      return true;
    }
  }

  addComponentSelectorInput(value: CustomWorkspaceItemInterface, clearCard?: number) {
    if (clearCard === 1) {
      this.listCustomWorkspaceDto.splice(
        this.listCustomWorkspaceDto.findIndex(elem => elem.id === value.id),
        1
      );
      this.workspaceService.setRemoveCard(value);
    } else {

      this.cardTitles.every(card => {
        if (value.type === Constants.WINDOW_NAME_WORK_SHEET) {
          let workspaceId;
          if (!this.data || !this.data.customNameWorkspace) {
            workspaceId = this.workspaceService.getWorkspaceSelected();
          } else {
            workspaceId = this.data.wksOptSelected;
          }

          if (this.validateMaxComponents(this.listCustomWorkspaceDto.length, workspaceId.Id)) {
            this.notificationService.warn('', Constants.WORKSPACE_MSG011);
            return;
          }

          if (!this.validateMaxWorksheets()) {
            this.notificationService.warn('', Constants.WORKSPACE_MSG012);
            return;
          }

          const workSheet = new CustomWorkspaceItemMultipleDto(
            null,
            Constants.WINDOW_NAME_WORK_SHEET,
            [],
            [],
            '',
            false
          );
          workSheet.id = this.itemId;
          workSheet.title = Constants.TITLE_CARD_WORKSHEET;
          this.saveMultiOptionWorkSheet(value, workSheet);
          this.listCustomWorkspaceDto.push(workSheet);
          this.workspaceService.setRemoveCard(value);
          this.itemId++;
          return false;
        } else {
          let workspaceId;
          if (!this.data || !this.data.customNameWorkspace) {
            workspaceId = this.workspaceService.getWorkspaceSelected();
          } else {
            workspaceId = this.data.wksOptSelected;
          }

          if (this.validateOnlyOneAssetAssigned(value.assetSymbol.toString(), value.type, workspaceId.Id) === true
            || this.hasEqualComponentOnList(value.assetSymbol.toString(), value.type, this.listCustomWorkspaceDto)) {
            this.workspaceService._successAddComponent.next(false);
            this._NotificationService.error('Erro', Constants.WORKSPACE_MSG014);
            return;
          } else {
            this.workspaceService._successAddComponent.next(true);
          }
          if (card.id === value.type) {
            const newItem = new CustomWorkspaceItemDto(
              this.itemId,
              value.type,
              <string>value.assetSymbol,
              <string>value.company,
              card.title,
              false
            );

            if (this.validateMaxComponents(this.listCustomWorkspaceDto.length, workspaceId.Id)) {
              this.notificationService.warn('', Constants.WORKSPACE_MSG011);
              return;
            }
            this.listCustomWorkspaceDto.push(newItem);
            this.workspaceService.setRemoveCard(value);
            this.itemId++;
            return false;
          }
        }
        return true;
      });
    }
  }

  getItemsWorksheet(worksheet: CustomWorkspaceItemMultipleDto): string {
    let list = '';
    worksheet.assetSymbol.forEach((asset, id) => {
      if (id === worksheet.assetSymbol.length - 1) {
        list = list + asset;
      } else {
        list = list + asset + ',';
      }
    });
    return list;
  }

  validateMaxWorksheets() {
    let workspaceId;
    if (!this.data || !this.data.customNameWorkspace) {
      workspaceId = this.workspaceService.getWorkspaceSelected();
    } else {
      workspaceId = this.data.wksOptSelected;
    }

    const userWorkspace = this.workspaceService.getUserWorkspaceById(workspaceId.Id);
    if (userWorkspace && userWorkspace.JsonData) {
      const workspace = JSON.parse(userWorkspace.JsonData);
      if (workspace.content.filter(x => x.type === Constants.WINDOW_NAME_WORK_SHEET).length +
        this.listCustomWorkspaceDto.filter(x => x.type === Constants.WINDOW_NAME_WORK_SHEET).length
        >= Constants.QTD_MAX_WORKSHEET) {
        return false;
      }
    }
    return true;
  }
}
