import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CustomWorkspaceModalComponent } from '@component/custom-workspace-modal/custom-workspace-modal.component';

describe('CustomWorkspaceModalComponent', () => {
  let component: CustomWorkspaceModalComponent;
  let fixture: ComponentFixture<CustomWorkspaceModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CustomWorkspaceModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CustomWorkspaceModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
