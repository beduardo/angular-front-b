import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-spinner-loading',
  templateUrl: './spinner.component.html'
})
export class SpinnerComponent implements OnInit {
  @Input() diamet: number;
  constructor() {
    this.diamet = 50;
  }

  ngOnInit() {
  }

}
