import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ErrorMessage } from '@model/error-message';
import { ConfigService } from '@services/webFeeder/config.service';
import { Constants } from '@utils/constants';

@Component({
  selector: 'app-error-screen',
  templateUrl: './error-screen.component.html',
})
export class ErrorScreenComponent implements OnInit {

  public errorMsg: ErrorMessage;

  constructor(
    private activeRoute: ActivatedRoute,
    private router: Router,
    private _configService: ConfigService
  ) {
    this.errorMsg = new ErrorMessage(Constants.ERROR_ERROR_ICON, Constants.ERROR_ERROR_TITLE,
      Constants.ERROR_ERROR_TEXT, Constants.ERROR_ERROR_BUTTON, 'login');
  }

  ngOnInit() {
    this._configService.closeBillet(true);
    this.activeRoute.queryParams.subscribe(errorMsg => {
      if (errorMsg.title === undefined || errorMsg === null || errorMsg === undefined) {
        this.errorMsg = new ErrorMessage(
          Constants.ERROR_404_ICON, Constants.ERROR_404_TITLE,
          Constants.ERROR_404_TEXT, Constants.ERROR_404_BUTTON, 'login');
      } else {
        this.errorMsg = new ErrorMessage(
          errorMsg.iconName, errorMsg.title,
          errorMsg.text, errorMsg.buttonText, errorMsg.route);
      }
    });
  }

  onClick() {
    this.router.navigate([this.errorMsg.route]);
  }
}
