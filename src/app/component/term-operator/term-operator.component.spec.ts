import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TermOperatorComponent } from './term-operator.component';

describe('TermOperatorComponent', () => {
  let component: TermOperatorComponent;
  let fixture: ComponentFixture<TermOperatorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TermOperatorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TermOperatorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
