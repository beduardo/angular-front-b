import { Constants } from '@utils/constants';

import { Injectable, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';
import { HttpService } from '@services/http-service';
import { ConfigService } from '@services/webFeeder/config.service';
import { AutoLogoutService } from '@auth/auto-logout-service.service';
import { Observable } from 'rxjs';
import { SocketService } from '@services/webFeeder/socket.service';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  public showNavBarEmitter: EventEmitter<boolean> = new EventEmitter<boolean>();
  private authenticated = false;
  private userLogged: string;

  constructor(
    private router: Router,
    private svcHttp: HttpService,
    private configService: ConfigService,
    private autoLogoutService: AutoLogoutService,
    private socketService: SocketService
  ) {
    const userLoginObserver = this.socketService.statusWFLogin$.subscribe(isLoged => {
      if (!isLoged) {
        this.logoutWithoutRouter();
      }
    })
  }

  private clearSession() {
    this.authenticated = false;
    this.configService.deleteCookie();
    this.showNavBar(false);
    this.configService.deleteHbCurrentUser();
    this.autoLogoutService.logout();
  }
  public logoutSession(): boolean {
    if (this.authenticated) {
      this.clearSession();
      window.location.href = this.configService.getValidUserRedirect();
      return true;
    }
    return false;
  }
  public logout() {
    this.clearSession();
    this.socketService.logout();
    this.router.navigate(['/logout']);
  }

  public logoutWithoutRouter() {
    this.clearSession();
    this.socketService.logout();
  }

  public sessionExpired() {
    this.clearSession();
    this.router.navigate([this.configService.getValidUserRedirect()]);

  }
  public isAuthenticated(viaLogin: boolean) {
    return this.authenticated;
  }

  public validateTaskTime() {
    return this.svcHttp
      .doPost(
        this.configService.getApiWebFeederRest(),
        'services/Authenticate/ValidateTaskTime', {}
      );
  }

  public valideToken(account: string, tokenApi: string, tokenWF: string): Observable<any> {
    return this.svcHttp
      .doPost(
        this.configService.getApiWebFeederRest(),
        'services/Authenticate/ValideToken', "{'account':'" + account + "', 'tokenA':'" + tokenApi + "', 'tokenW':'" + tokenWF + "'}"
      )
      .map(response => {
        if (response) {
          const json = response;
          if (json.accountNumber && json.accountNumber !== null && json.accountNumber !== '') {
            this.authenticated = true;
            this.userLogged = json.accountNumber;
            this.configService.setUserLogged(json.accountNumber);
            this.autoLogoutService.login();
            return {
              success: true,
              token: json.token,
              guidUser: json.guidUser,
              tokenWS: json.tokenWS,
              account: json.accountNumber
            };
          } else {
            this.authenticated = false;
            this.configService.setMessage('');
          }
          if (!json.message && !json.codeMessage) {
            return { noNetworkError: true };
          }
          return [];
        }
      });
  }

  public loginViaToken(tokenAuth: string): Observable<any> {
    return this.svcHttp
      .doPost(this.configService.getApiWebFeederRest(), 'services/Authenticate/PostLogin', "{'token':'" + tokenAuth + "'}")
      .map(response => {
        if (response) {
          const json = response;
          if (json.code) {
            this.authenticated = false;
            this.configService.writeLog('Auth > RESPONSE_LOG');
            // this.configService.writeLog(json.message);
            this.configService.setMessage(json.message);
          } else if (json.accountNumber) {
            this.authenticated = true;
            this.userLogged = json.accountNumber;
            if (tokenAuth.length <= 5) {
              this.userLogged = tokenAuth;
            }
            this.configService.setUserLogged(json.accountNumber);
            this.autoLogoutService.login();
            return {
              success: true,
              token: json.token,
              tokenWS: json.tokenWS,
              guidUser: json.guidUser,
              account: json.accountNumber
            };

          } else {
            this.authenticated = false;
            this.configService.setMessage('');
          }
          return [];
        }
        return [];
      });
  }

  public getUserLogged() {
    if (!this.userLogged) {
      const userLogged: any = this.configService.getHbCurrentUser();
      if (userLogged && userLogged.username) {
        this.userLogged = userLogged.username;
      } else if (userLogged && userLogged.name) {
        this.userLogged = userLogged.name.split(' ')[0];
      } else {
        const userJson = JSON.parse(userLogged);
        this.userLogged = userJson.name.split(' ')[0];
        this.configService.writeLog('AUTH > GETUSERLOGGED: usuario não encontrado.');
      }
    }
    return this.userLogged;
  }

  public setUserLogged(userlogged: string) {
    this.authenticated = true;
    this.userLogged = userlogged;
    this.autoLogoutService.login();
  }

  private showNavBar(ifShow: boolean) {
    this.showNavBarEmitter.emit(ifShow);
  }

}
