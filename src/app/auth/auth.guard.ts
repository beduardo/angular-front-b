import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { ConfigService } from '@services/webFeeder/config.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {

  constructor(
    private router: Router,
    private config: ConfigService
  ) { }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    this.config.writeLog('AUTH GUARD > isAuthenticadSystem: ' + this.config.isAuthenticadSystem());
    if (this.config.isAuthenticadSystem()) {
      return true;
    }
    this.config.writeLog('AUTH GUARD > redireciona para a tela de login - state.url ' + state.url);
    if (this.config.isLoginTypeWSO2() && this.config.isUserAssessorLogged()) {
      window.location.href = this.config.REDIRECT_LOGOUT + '/assessor/login';
    } else if (this.config.isLoginTypeWSO2() && !this.config.isUserAssessorLogged()) {
      window.location.href = this.config.REDIRECT_LOGOUT + '/client/login';
    } else {
      this.router.navigate(['/login'], { queryParams: { returnUrl: state.url } });
    }
    return false;
  }
}
