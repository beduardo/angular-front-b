import { ConfigService } from '@services/webFeeder/config.service';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Observable } from 'rxjs';
import { environment } from 'environments/environment';
import { NotificationsService } from 'angular2-notifications';
import { Constants } from '@utils/constants';
import { MatDialog } from '@angular/material';
// import { AuthenticationService } from '../services/authentication.service';
import { CustomSnackbarService } from '@services/custom-snackbar-service.service';
export enum AuthState {
  LOGGED_IN,
  LOGGED_OUT
}
@Injectable({
  providedIn: 'root'
})
export class AutoLogoutService {
  private authState: AuthState;

  private authManager: BehaviorSubject<AuthState>;
  public authChange$: Observable<AuthState>;
  private authAlertManager: BehaviorSubject<AuthState>;
  public authAlertChange$: Observable<AuthState>;
  public dialog: MatDialog;

  public _statusAlert = new BehaviorSubject<boolean>(false);
  public statusAlertTimeout$ = this._statusAlert.asObservable();

  public _resetClockDownTimer = new BehaviorSubject<boolean>(false);
  public resetClockDownTimer$ = this._resetClockDownTimer.asObservable();

  constructor(
    public _NotificationService: NotificationsService,
    public _configService: ConfigService,
    public _snackBar: CustomSnackbarService,
    dialog: MatDialog,
  ) {
    this.dialog = dialog;
    this.authManager = new BehaviorSubject(AuthState.LOGGED_OUT);
    this.authChange$ = this.authManager.asObservable();
    this.authChange$
      .filter((authState: AuthState) => authState === AuthState.LOGGED_IN)
      .map((authState: AuthState) => Observable.timer(environment.SESSION_TIMEOUT))
      .switch()
      .subscribe(() => {
        this._statusAlert.next(false);
        // this._NotificationService.alert('', Constants.WORKSPACE_MSG009);
        this._configService._loggoutSessionExpired.next(true);
      });
    const timeOutAlert = (environment.SESSION_TIMEOUT - environment.SESSION_TIMEOUT_ALERT);
    this.authAlertManager = new BehaviorSubject(AuthState.LOGGED_OUT);
    this.authAlertChange$ = this.authAlertManager.asObservable();
    this.authAlertChange$
      .filter((authState: AuthState) => authState === AuthState.LOGGED_IN)
      .map((authState: AuthState) => Observable.timer(timeOutAlert))
      .switch()
      .subscribe(() => {
        this._statusAlert.next(true);
      });
  }

  private setAuthState(newAuthState: AuthState): void {
    if (newAuthState !== this.authState) {
      this.authState = newAuthState;
      this.emitAuthState();
    }
  }

  emitAuthState(): void {
    this.authManager.next(this.authState);
    this.authAlertManager.next(this.authState);
    this._statusAlert.next(false);
  }

  public login() {
    this.setAuthState(AuthState.LOGGED_IN);
  }

  public resetTime() {
    this._resetClockDownTimer.next(true);
    this.authManager.next(this.authState);
    this.authAlertManager.next(this.authState);
  }

  public logout() {
    this.setAuthState(AuthState.LOGGED_OUT);
  }

}
