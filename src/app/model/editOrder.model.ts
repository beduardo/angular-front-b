import { TypeOrderEnum } from '@model/enum/type-order.enum';
import { SideEnum } from '@model/enum/side.enum';
import { MarketEnum } from '@model/enum/market.enum';

export class EditOrderModel {
  constructor() {}
  public account;
  public quote;
  public qtd;
  public market;
  public type;
  public typeCode;
  public side;
  public validityOrder;
  public orderStrategy;
  public timeInForce;
  public orderID;
  public origclordid;
  public loteDefault;
  public maxFloor;
  public clOrdID;

  /** disparo loss */
  public priceStop;
  /** limite loss  or price if not stop*/
  public price;
  /** disparo gain*/
  public priceStopGain;
  /** limite gain */
  public price2;

  get total(): number {
    return this.qtd * this.price;
}

get totalLoss(): number{
  return this.qtd * this.price;
}

get totalGain(): number {
   return this.qtd * this.price2;
}

}
