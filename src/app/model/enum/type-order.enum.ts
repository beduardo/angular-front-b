export enum TypeOrderEnum {
  MARKET = 'Market',
  LIMITED = 'Limited',
  STOP = 'Stop',
  START = 'Start',
  MARKETPROTECTED = 'MarketProtected'
  // ONCLOSE = 5 ?
}
