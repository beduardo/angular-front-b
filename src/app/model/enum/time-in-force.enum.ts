export enum TimeInForceEnum {
  DAY = 0,
  GTC = 1,
  OPG = 2,
  IOC = 3,
  FOK = 4,
  GTD = 6,
  ATC = 7
}
