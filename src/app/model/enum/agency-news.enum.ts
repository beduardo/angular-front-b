export enum AgencyNewsEnum {
  CEDRO = 'AGENCIA CEDRO',
  BOVESPA = 'AGENCIA BOVESPA',
  BMF = 'AGENCIA BMF',
  FR = 'FR'
}
