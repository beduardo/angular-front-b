import { TypeOrderEnum } from '@model/enum/type-order.enum';
import { SideEnum } from '@model/enum/side.enum';
import { MarketEnum } from '@model/enum/market.enum';

export class SimpleOrder {

  constructor() { }
  public client: string;
  public account: string;
  public quote: string;
  public qtd: number;
  public price: any;
  public market: MarketEnum;
  public type: TypeOrderEnum;
  public side: SideEnum;
  public validityOrder: Date;
  public orderStrategy: string;
  public timeInForces?: string;
  public orderid?: string;
  public origclordid?: string;
  public loteDefault?: number;
  public total?: number;
  public totalLoss?: number;
  public totalGain?: number;
  public maxfloor: number;
  public minqtd: number;
  public orderTag: string;

  public timeInForce: string;
  public quantitySupplied: number;

  public targettrigger: number;
  public targetlimit: number;
  public stoptrigger: number;
  public stoplimit: number;

  public signature?: string;
  public bypasssuitability: boolean;

}
