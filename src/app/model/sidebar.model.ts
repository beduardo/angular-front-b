import { ToolItem } from '@model/tool-item.model';
export class Sidebar {
  constructor(private sidebarOrientation: string, private sidebarContent?: Array<ToolItem>) {}
  set setSidebarOrientation(sidebarOrientation: string) {
    this.sidebarOrientation = sidebarOrientation;
  }
  set setSidebarContent(sidebarContent: Array<ToolItem>) {
    this.sidebarContent = sidebarContent;
  }
  get getSidebarOrientation(): string {
    return this.sidebarOrientation;
  }
  get getSidebarContent(): Array<ToolItem>  {
    return this.sidebarContent;
  }
}
