export class AssetSummary {
  public symbol: string;
  public type: string;
  public buy: string[];
  public sell: string[];
}
