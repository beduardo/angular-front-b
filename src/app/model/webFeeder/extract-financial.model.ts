export class ExtractFinancial {
  dtTransaction: string;
  dtLiquidation: string;
  description: string;
  amount: string;
  balance: string;
  initialBalance: string;
}
