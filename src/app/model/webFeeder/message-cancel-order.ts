export class CancelOrder {
    public orderid: string;
    public origclordid: string;
    public market: string;
    public quote: string;
    public qtd: string;
    public side: string;
    public username: string;
    public enteringtrader: string;
    public sourceaddress;
    public clOrdID;
}
