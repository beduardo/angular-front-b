export class MessageWebFeeder {
  token: string;
  module: string;
  service: string;
  parameterGet?: string;
  parameters: any;
}
