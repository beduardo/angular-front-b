export class Market {
  public code: number;
  public name: string;

  constructor(code: number, name: string) {
    this.code = code;
    this.name = name;
  }

}
