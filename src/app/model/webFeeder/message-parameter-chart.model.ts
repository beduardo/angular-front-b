export class MessageParameterChart {
  public quote: string;
  public period: string;
  public tick_size: string;
  public quantity: string;
  public realTime: string;
  public dispatch: string;
  public initDate: string;
  public fimDate: string;
  public filter: string;
}
