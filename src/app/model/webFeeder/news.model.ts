export class News {
  public newsCode: number;
  public agencyCode: number;
  public agencyDescription: string;
  public title: string;
  public description: string;
  public categoryCode: number;
  public newsDate: Date;
  public fullDescription: string;
  public img?: any;
}
