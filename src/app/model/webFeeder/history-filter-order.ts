export class HistoryFilterOrder {

  public account: string;
  public market: string;
  public dataInicio: string;
  public dataFim: string;
  public symbol: string;
  public status: string;
  public direction: string;
  public type: string;
  public orderID?: string;
  public typeDateFilter?: string;
  public statusGroup?: string;
  statusGeneric?: string;
  public queryType?: string;

  constructor() { }

}
