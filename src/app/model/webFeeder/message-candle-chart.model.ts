export class MessageCandleChart {
  public quote: string;
  public period: string;
  public tick_size: string;
  public response_type: string;
  public error: string;
  public subscribe_id: string;
  public candles: Candle[];
}


export class Candle {

    public date: number;
    public price: number;
    public open: number;
    public high: number;
    public low: number;
    public fVol: string;
    public aVol: string;
    public isClose: boolean;

}

