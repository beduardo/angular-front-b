export class BmfContracts {
  public code: number;
  public symbol: string;
  public name: string;

  constructor(code: number, name: string) {
    this.code = code;
    this.name = name;
    this.symbol = '';
  }
}
