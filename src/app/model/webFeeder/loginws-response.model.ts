export class LoginWsResponse {
    public code: string;
    public codeMessage: boolean;
    public tokenWS: string;
    public message?: string;
    public accountNumber?: string;
    public token?: string;
    public expiration?: string;
}
