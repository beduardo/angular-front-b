﻿export class SwingTrade {

  public assetSearch: string;

  public typeOrder: string;

  public orderstrategy: string;

  public quantity?: number;

  public variation?: number;

  public typePrice: string;

  public price?: number;

  public typeValidity: string;

  public validityOrder: Date;

  public lastTrade?: number; // TODO Preço da última negociação

  public bid?: number; // TODO Melhor oferta de compra

  public ask?: number; // TODO Melhor oferta de venda

  public enableUp?: boolean;

  public priceGainTrigger?: number;

  public priceGainExecution?: number;

  public totalUp?: number;

  public priceLossTrigger?: number;

  public priceLossExecution?: number;

  public enableDown?: boolean;

  public totalDown?: number;

  public loteSize?: number;

  public loteSizeStep?: number;

  public ticksize?: number;

  public interval?: number;

  public positionDefault?: number;

  public profit?: number;

  public side: number;

  public timeinforce?: string;

  public loteDefault?: number;

  public currentQuantity?: number;

  public marketType?: number;

  public company?: string;

  constructor() {
    this.assetSearch = '';
    this.positionDefault = 0;
    this.variation = 0;
    this.company = '';
    this.loteSize = 0;
    this.loteSizeStep = 0;
    this.profit = 0;
    this.ticksize = 2;
    this.currentQuantity = 0;
    this.price = 0;
    this.priceGainTrigger = 0;
    this.priceGainExecution = 0;
    this.priceLossExecution = 0;
    this.priceLossTrigger = 0;
  }

}
