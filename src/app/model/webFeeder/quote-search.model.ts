export class QuoteSearch {
  public description: string;
  public speak: string;
  public expirationDate: string;
  public marketName: string;
  public typeQuotesDescription: string;
  public empresa: string;
  public ativoObjeto: string;
  public isinPaper: string;
  public precoExercicio: string;
  public segmentName: string;
}
