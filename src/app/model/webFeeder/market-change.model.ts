﻿export class MarketChange {
  public quote: string;
  public company: string;
  public price: number;
  public priceFormat?: string;
  public value: number;
  public change: number;
  public volumeFinancier: number;
}
