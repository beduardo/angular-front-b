export class Warranty {
  public warrantyID: string;
  public descWarranty: string;
  public quantity: string;
  public valueWarranty: string;
  public operation: string;
  public account: string;
  constructor() {
    this.account = '';
    this.warrantyID = '';
    this.descWarranty = '';
    this.quantity = '';
    this.valueWarranty = '';
    this.operation = '';
  }
}
