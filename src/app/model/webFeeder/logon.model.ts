export class Logon {
  module: string;
  service: string;
  parameters: ParameterLogon = new ParameterLogon();
}

class ParameterLogon {
  login: string;
  password: string;
  account: string;
}
