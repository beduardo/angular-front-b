import { News } from '@model/webFeeder/news.model';

export class Pagination {
  public page: number;
  public size: number;
  public lastNewsCode: number;
  public lastPage: number;
  public requestId: number;
  public sizeTotal: number;
}
