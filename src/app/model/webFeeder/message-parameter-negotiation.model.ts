export class MessageParameterNegotiation {
  account: string;
  market: string;
  dispatch: boolean;
  history: boolean;
  update?: boolean;
}
