export class Company {
  public codeLocal: number;
  public marketCode: number;
  public name: string;
  public marketName: string;
  public segmentCode: number;

  constructor(code: number, name: string) {
    this.marketCode = code;
    this.name = name;
  }
}
