export class MessageBook {
  public symbol: string;
  public type: string;
  public buy: any;
  public sell: any;
  public errorMessage: string;
}
