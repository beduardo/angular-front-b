export interface MessageLogin {
  success: string;
  token: string;
  error?: string;
}
