import { AppUtils } from '@utils/app.utils';
import { MainQuoteInfos } from '@model/webFeeder/main-quote-infos.model';

export class MessageQuote {

  public errorMessage: string;
  public symbol: string;
  public type: string;
  public timeUpdate: string;
  public dateTrade: string;
  public lastTrade: number;
  public lastTradeTemp?: boolean;
  public bid: number; // TODO Melhor oferta de compra
  public ask: number; // TODO Melhor oferta de venda
  public quantity: number;
  public quantityLast: number;
  public volumeFinancier: string;
  public volumeBetterBid: string;
  public volumeBetterAsk: string;
  // public soldQuantity: number;
  public high: number;      // TODO Maior preço do dia
  public low: number;       // TODO Menor preço do dia
  public previous: number;  // TODO Preço de fechamento do dia anterior
  public open: number;      // TODO Preço de abertura
  public variation: number; // TODO Variação do preço do ativo relativo ao último negócio
  public average: number;
  public marketType: number;
  // public assetType: number;
  // public assetTypeName: string;
  public loteDefault: number;
  public description: string;
  public status: number;
  public tickSize: number; // TODO valor para incrementar
  public classification: string;
  // public mCap: number;
  public inTheWeek: number;
  // public inTheMonth: number;
  // public inTheYear: number;
  // public lastTrade7Days: number;
  public lastTrade1Month: number;
  public lastTrade1Year: number;
  public theoryPrice: number;
  public expirationDate: string;
  public calendarDaysToExpirationDate: number;
  public businesDaysToExpirationDate: number;
  public tunnelUpperLimit: number;
  public tunnelLowerLimit: number;
  public previousAdjustPrice: number;
  public volumeAmount: string;
  public theoryQuantity: string;
  public quantitySymbol: number;
  public exercisePrice: number;
  // public theoricChange: number;
  public directionUnansweredQty: number;
  public unansweredQty: number;
  public contractMultiplier: number;
  public quotationForm: number;
  public minimumIncrement: number;
  // public volumeProjected: string;
  // public buyTime: string;
  // public sellTime: string;
  public calculoWeeklyVariation: number;
  public calculoMonthlyVariation: number;
  public calculoYearlyVariation: number;
  // public auctionVariation: number;
  // public timeRemainingAuction: Date;
  public deltaVolumeCurrentTime: number;
  public deltaVolumeAccumulatedHour: number;
  // public deltaVolumeDaily: number;
  public hourLastTrade: Date;
  public timeToOpen: string;
  public timeReprogrammedToOpen: string;
  public coin: string; // TODO 116 Moeda utilizada no preço String Exemplos: BRL, EUR, USD.
  public expiration: string;
  public quantityOpenedContractNumber: any; // TODO 110 contratos em aberto.
  public adjustCurrentRate: number; // TODO 127 Ajuste atual em taxa.
  public adjustPreviousRate: number; // TODO 128 Ajuste anterior em taxa.
  public withdrawalsNumberUntilDueDate: number; // TODO 130 Número de saques até a data de vencimento.
  // public playerBid: number; // TODO 60 código da corretora que fez a melhor oferta de compra.
  // public playerAsk: number; // TODO 61 código da corretora que fez a melhor oferta de compra.
  // public codePlayerLastBid: number; // TODO 62 código da corretora que realizou a última compra.
  // public codePlayerLastAsk: number; // TODO 63 código da corretora que realizou a última venda.
  public adjustment: number; // TODO 103 preço de ajuste do dia.
  public interval: number;
  public coinVariation?: number;
  public volAccBid?: string;
  public volAccAsk?: string;
  public previousWeek?: string;
  public previousMonth?: string;
  public previousYear?: string;
  public openLastDay?: string;
  public highPriceLastDay?: string;
  public lowPriceLastDay?: string;
  public vhDay?: string;
  public dateLastNegotiation?: string;
  public typeOption?: string;
  public volAVG20?: string;
  public groupPhase?: string;
  public securityId?: string;
  public tickDirection?: string;
  public exercisePriceOption?: string;
  public company?: string;

  constructor(json?: any) {
    if (json) {
      this.symbol = json.symbol || '';
      this.tickSize = json.tickSize !== undefined ? Number(json.tickSize) : 0;
      this.type = json.type || '';
      this.errorMessage = '';
      this.dateTrade = json.dateTrade || '';
      this.lastTrade = json.lastTrade ? AppUtils.convertNumber(json.lastTrade) : 0;
      this.lastTradeTemp = false;
      this.bid = json.bid ? AppUtils.convertNumber(json.bid) : 0;
      this.ask = json.ask ? AppUtils.convertNumber(json.ask) : 0;
      this.quantity = json.quantity ? AppUtils.convertNumber(json.quantity) : 0;
      this.quantityLast = json.quantityLast ? AppUtils.convertNumber(json.quantityLast) : 0;
      this.volumeFinancier = json.volumeFinancier || '0';
      this.volumeBetterBid = json.volumeBetterBid || '0';
      this.volumeBetterAsk = json.volumeBetterAsk || '0';
      // this.soldQuantity = 0;
      this.high = json.high ? AppUtils.convertNumber(json.high) : 0;
      this.low = json.low ? AppUtils.convertNumber(json.low) : 0;
      this.previous = json.previous ? AppUtils.convertNumber(json.previous) : 0;
      this.open = json.open ? AppUtils.convertNumber(json.open) : 0;
      // this.average = json.average ? AppUtils.convertNumber(json.average) : 0;
      this.variation = json.variation !== undefined ? Number(json.variation) : 0;
      this.loteDefault = json.loteDefault !== undefined ? Number(json.loteDefault) : 0;
      this.description = json.description || '';
      this.status = json.status !== undefined ? Number(json.status) : 0;
      // this.assetType = json.assetType !== undefined ? Number(json.assetType) : 0;
      this.classification = '-';
      // this.mCap = 0;
      this.inTheWeek = 0;
      // this.inTheMonth = 0;
      // this.inTheYear = 0;
      // this.lastTrade7Days = 0;
      this.lastTrade1Month = 0;
      this.lastTrade1Year = 0;
      this.theoryPrice = 0;
      this.expirationDate = '';
      this.tunnelUpperLimit = 0;
      this.marketType = json.marketType ? AppUtils.convertNumber(json.marketType) : 0;
      this.tunnelLowerLimit = 0;
      this.previousAdjustPrice = 0;
      this.theoryQuantity = '0';
      this.volumeAmount = json.volumeAmount || '';
      this.contractMultiplier = json.contractMultiplier !== undefined ? Number(json.contractMultiplier) : 0;
      this.quotationForm = json.quotationForm !== undefined ? Number(json.quotationForm) : 0;
      this.quantitySymbol = 0;
      this.exercisePrice = 0;
      // this.theoricChange = 0;
      this.unansweredQty = 0;
      this.minimumIncrement = json.minimumIncrement ? AppUtils.convertNumber(json.minimumIncrement) : 0;
      // this.volumeProjected = '-';
      // this.auctionVariation = 0;
      // this.timeRemainingAuction = null;
      this.hourLastTrade = null;
      this.quantityOpenedContractNumber = json.quantityOpenedContractNumber !== undefined ? json.quantityOpenedContractNumber : '0';
      this.adjustCurrentRate = json.adjustCurrentRate !== undefined ? Number(json.adjustCurrentRate) : 0;
      this.adjustPreviousRate = json.adjustCurrentRate !== undefined ? Number(json.adjustCurrentRate) : 0;
      this.withdrawalsNumberUntilDueDate =
        json.withdrawalsNumberUntilDueDate !== undefined ? Number(json.withdrawalsNumberUntilDueDate) : 0;
      // this.playerBid = json.playerBid !== undefined ? Number(json.playerBid) : 0;
      // this.playerAsk = json.playerAsk !== undefined ? Number(json.playerAsk) : 0;
      // this.codePlayerLastBid = json.codePlayerLastBid !== undefined ? Number(json.codePlayerLastBid) : 0;
      // this.codePlayerLastAsk = json.codePlayerLastAsk ? AppUtils.convertNumber(json.codePlayerLastAsk) : 0;
      this.adjustment = json.adjustment ? AppUtils.convertNumber(json.adjustment) : 0;
      this.coinVariation = json.coinVariation ? AppUtils.convertNumber(json.coinVariation) : 0;

    } else {
      Object.assign(this, {});
    }
  }

  // toMainQuoteInfos() {
  //   const mainQuoteInfos = new MainQuoteInfos(this.lastTrade, this.variation, this.volumeFinancier, this.bid, this.ask);
  //   return mainQuoteInfos;
  // }

}
