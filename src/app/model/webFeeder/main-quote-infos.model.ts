export class MainQuoteInfos {
    private lastTrade: number;
    private variation: number;
    private volume: string;
    private bid: number;
    private ask: number;

    constructor( lastTrade?: number,
         variation?: number,
         volume?: string,
         bid?: number,
         ask?: number) {
        this.lastTrade = lastTrade !== undefined ? lastTrade : 0;
        this.variation = variation !== undefined ? variation : 0;
        this.volume = volume !== undefined ? volume : '-';
        this.bid = bid !== undefined ? bid : 0;
        this.ask = ask !== undefined ? ask : 0;
    }

}
