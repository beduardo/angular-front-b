
// TODO Remove all comments below!

// export interface Quote {
//   name: string;
//   value: string;
// }

// export interface CurrentQuantity {
//   name: string;
//   value: string;
// }

// export interface InitQuantity {
//   name: string;
//   value: string;
// }

// export interface QuantityBlocked {
//   name: string;
//   value: string;
// }

// export interface QuantityD1 {
//   name: string;
//   value: string;
// }

// export interface QuantityD2 {
//   name: string;
//   value: string;
// }

// export interface QuantityD3 {
//   name: string;
//   value: string;
// }

// export interface LastTrade {
//   name: string;
//   value: string;
// }

// export interface AveragePrice {
//   name: string;
//   value: string;
// }

// export interface AveragePricePos {
//   name: string;
//   value: string;
// }
// export interface ProfitLoss {
//   name: string;
//   value: string;
// }

// export interface PrecoAjuste {
//   name: string;
//   value: string;
// }

// export interface CurrentPosition {
//   name: string;
//   value: string;
// }

// export interface ExecutedBuy {
//   name: string;
//   value: string;
// }

// export interface ExecutedSell {
//   name: string;
//   value: string;
// }

// export interface OpenProfitLoss {
//   name: string;
//   value: string;
// }
// export interface ClosedProfitLoss {
//   name: string;
//   value: string;
// }
// export interface CurrentyQuantity {
//   name: string;
//   value: string;
// }
// export interface PreviousTrade {
//   name: string;
//   value: string;
// }
// export interface PriceAverageInit {
//   name: string;
//   value: string;
// }
// export interface ValueCurrent {
//   name: string;
//   value: string;
// }
// export interface ValueInit {
//   name: string;
//   value: string;
// }
// export interface ValueSpent {
//   name: string;
//   value: string;
// }
// export interface ProfitLossPercent {
//   name: string;
//   value: string;
// }
// export interface OpenBuy {
//   name: string;
//   value: string;
// }
// export interface OpenSell {
//   name: string;
//   value: string;
// }
// export interface Expired {
//   name: string;
//   value: string;
// }
// export interface QuantityReleased {
//   name: string;
//   value: string;
// }
// export interface LastTradeDate {
//   name: string;
//   value: string;
// }
// export interface NetDay {
//   name: string;
//   value: string;
// }
// export interface NetPosition {
//   name: string;
//   value: string;
// }
// export interface NetDayTrade {
//   name: string;
//   value: string;
// }
// export interface BuyAvgPrice {
//   name: string;
//   value: string;
// }
// export interface SellAvgPrice {
//   name: string;
//   value: string;
// }

// export class CustodyBovespa {
//   quote: Quote;
//   currentQuantity: CurrentQuantity;
//   initQuantity: InitQuantity;
//   quantityBlocked: QuantityBlocked;
//   quantityD1: QuantityD1;
//   quantityD2: QuantityD2;
//   quantityD3: QuantityD3;
//   lastTrade: LastTrade;
//   total: number;
//   totalValue: string;
//   openProfitLoss: OpenProfitLoss;
//   closedProfitLoss: ClosedProfitLoss;
//   averagePricePos: AveragePricePos;
//   executedBuy: ExecutedBuy;
//   executedSell: ExecutedSell;
//   averagePrice: AveragePrice;
//   profitLoss: ProfitLoss;
// }

// export class CustodyBmf {
//   quote: Quote;
//   initQuantity: InitQuantity;
//   lastTrade: LastTrade;
//   dayNet: number;
//   total: string;
//   profitLoss: ProfitLoss;
//   position: CurrentPosition;
//   executedBuy: ExecutedBuy;
//   executedSell: ExecutedSell;
//   averagePricePos: AveragePricePos;
// }

// export class CustodyDayTrade {
//   quote: Quote;
//   executedBuy: ExecutedBuy;
//   averagePrice: AveragePrice;
//   executedSell: ExecutedSell;
//   averagePricePos: AveragePricePos;
//   dayNet: number;
//   lastTrade: LastTrade;
//   openProfitLoss: OpenProfitLoss;
//   closedProfitLoss: ClosedProfitLoss;
//   profitLoss: string;
//   total: number;
//   isBmf: boolean;
// }

// export class Custody {
//   custodyBovespa: CustodyBovespa[];
//   custodyBmf: CustodyBmf[];
//   custodyDayTrade: CustodyDayTrade[] = [];
//   financialInformationBean: CustodyDayTrade[] = [];
//   code: number;
//   constructor() {}
// }


export class CustodyPosition {
  market: string;
  quote: string;
  currentQuantity: string;
  lastTrade: string;
  previusTrade: string;
  averagePrice: string;
  initQuantity: string;
  quantityD1: string;
  quantityD2: string;
  quantityD3: string;
  priceAverageInit: string;
  valueCurrent: string;
  valueInit: string;
  valueSpent: string;
  profitLoss: string;
  profitLossPercent: string;
  openBuy: string;
  openSell: string;
  executedBuy: string;
  executedSell: string;
  expired: string;
  quantityBlocked: string;
  quantityReleased: string;
  lastTradeDate: string;
  netDay: string;
  netPosition: string;
  netDaytrade: string;
  averagePricePos: string;
  openProfitLoss: string;
  closedProfitLoss: string;
  buyAvgPrice: string;
  sellAvgPrice: string;
  totalValue: string;
  total: string;
}

export class MessagePosition {
  type: string;
  account: string;
  market: string;
  marketCode: string;
  quote: string;
  // symbol: string;

  currentQuantity: any;
  initQuantity: any;
  quantityBlocked: string;
  quantityD1: any;
  quantityD2: any;
  quantityD3: any;
  profitLoss: string;
  openProfitLoss: string;
  closedProfitLoss: string;
  averagePricePos: string;
  executedBuy: any;
  executedSell: any;
  averagePrice: string;
  lastTrade: string;
  // total: number;
  // total: number;

  // clearedQuantity: number;
  // dayNet: number;
  // totalValue: string;
  // position: string;

  buyAvgPrice: string;
  sellAvgPrice: string;
  quotationForm: string;
  priorSetllPrice: string;
  securityType: string;
  totalPurchaseValue: string;
  totalSaleValue: string;
  executedDaytradePurchaseQty: string;
  executedDaytradeSaleQty: string;
  notExecutedPurchaseDaytradeQty: string;
  notExecutedSaleDaytradeQty: string;
  avgValueDaytradeToday: string;
  openProfitLossDaytrade: string;
  valueAdjustedAmount: string;
  openAvgValueDaytradeToday: string;
  closedProfitLossDaytrade: string;
  posMainRptID: string;
  quoteType: string;
  previusTrade: string;
  priceAverageInit: string;
  valueCurrent: string;
  valueInit: string;
  valueSpent: string;
  profitLossPercent: string;
  openBuy: string;
  openSell: string;
  expired: string;
  quantityReleased: string;
  lastTradeDate: string;
  qtdTomadorBTC: string;
  qtdDoadaBTC: string;
  precoMedioTomadorBTC: string;
  precoMedioDoadorBTC: string;
  netDay: number;
  netPosition: number;
  netDaytrade: number;
}
