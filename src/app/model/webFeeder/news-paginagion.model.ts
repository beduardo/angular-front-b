import { News } from '@model/webFeeder/news.model';
import { Pagination } from '@model/webFeeder/pagination.model';

export class NewsPagination {
  public pagination: Pagination;
  public list: News[];
}
