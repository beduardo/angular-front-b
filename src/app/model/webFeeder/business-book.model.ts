import { QuoteTrade } from '@model/webFeeder/quote-trade.model';

export class BusinessBook {
  public quoteTrade: QuoteTrade;
  public type: string;
  public symbol: string;
}
