export class ChangePassword {
  username: string;
  password: string;
  newPassword: string;
  clientDocument?: string;
  softwareName?: string;
  softwareVersion?: string;
  ipAdress?: string;
}
