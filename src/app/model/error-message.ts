export class ErrorMessage {
    iconName: string;
    title: string;
    text: string;
    buttonText: string;
    route: string;

    constructor (iconName: string, title: string, text: string, buttonText: string, route: string) {
        this.iconName = iconName;
        this.title = title;
        this.text = text;
        this.buttonText = buttonText;
        this.route = route;
    }
}
