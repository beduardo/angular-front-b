export class Signature {

  user_client_id: string;
  signature: string;
  newSignature: string;
  confirmSignature: string;

}
