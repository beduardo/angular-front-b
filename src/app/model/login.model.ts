export class Login {
  username: string;
  password: string;
  newPassword?: string;
  clientDocument?: string;
  token?: string;
  softwareVersion: string;
  softwareName: string;
}
