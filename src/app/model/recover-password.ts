export class OmsAdmin {
  newPassword?: string;
  passwordConfirm?: string;
  token?: string;
  email?: string;
  type?: number;
  login?: string;
  oldPassword?: string;
  passwordTemp?: string;
  broker?: string;
}
