export class SimultaneousStopOrder {
  public symbol: string;
  public company: string;
  public type: string;
  public quantity: number;
  public quantitySet: boolean;
  public lastTrade: any;
  get valorTotal() { // TODO english var name please
    return this.calcValorTotal();
  }
  public tunnelUpperLimit: any;
  public tunnelLowerLimit: any;
  public duration: string;
  public expiration: Date;
  public gainPrice: number;
  public gainTriggerPrice: number;
  public lossPrice: number;
  public lossTriggerPrice: number;
  private calcValorTotal() {
    return (
      this.quantity * parseFloat(this.lastTrade.replace(',', '.'))
    ).toFixed(2);
  }
}
