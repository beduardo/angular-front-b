export class StatusConnection {
  public static SUCCESS = 0;
  public static WARNING = 1;
  public static ERROR = 2;
  public static INITIAL = 3;
  public static SUCCESS_RECONNECT = 4;
  public static statusList = {
      '0': 'circle',
      '1': 'circle', // -o
      '2': 'circle', // -o-notch
      '3': 'circle',
      '4': 'circle'
    };
    public static statusClassList = {
      '0': 'circle-green-icon',
      '1': 'circle-yellow-icon',
      '2': 'circle-red-icon',
      '3': 'circle-yellow-icon',
      '4': 'circle-green-icon'
    };
  constructor() {}
}
