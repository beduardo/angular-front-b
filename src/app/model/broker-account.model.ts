export class BrokerAccount {
  account: string;
  market: string;
  accountDesc: string;
  advisorID: string;
  portID: string;
  suitabilityID: string;
  suitabilityIdName: string;
  validateSuitability: boolean;
}
