import { Profile } from '@utils/constants';

export class UserCA {
    public id: number;
    public login: string;
    public password: string;
    public name: string;
    public profile: Profile;
    public image: string;
    public email: string;

    public loggedIn: Date;
    public pwdExpires: Date;
    public tokenExpires: Date;

    public constructor(init?: Partial<UserCA>) {
        if (init) {
            Object.assign(this, init);
        } else {
            this.loggedIn = new Date();
        }
    }

    public firstName() {
        if (this.name && this.name.split(/\s+/).length > 0) {
            return this.name.split(/\s+/)[0];
        } else {
            return this.name;
        }
    }
}
