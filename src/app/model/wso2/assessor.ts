import { UserCA } from './userca';

export class Assessor extends UserCA {

  public clients: any[];

  public constructor(init?: Partial<Assessor>) {
    super(init);
    if (init) {
      Object.assign(this, init);
    }
  }
}
