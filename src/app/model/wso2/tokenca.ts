export class TokenCA {
    access_token: string;
    refresh_token: string;
    scope: string;
    token_type: string;
    expires_in: number;
    access: AccessToken;
    lastRefreshed: Date;
    constructor() { }
}

export class AccessToken {
    user_name: string;
    access: any[];
    constructor() { }
}
