import { Observable } from 'rxjs';
import { UserCA } from './userca';

export class Client extends UserCA {

  public perfil: string;
  public cpf: string;
  public profile_name: string;
  public user_id: number;

  public selectedAccount: { account: { account: string, market: string }, balance: number, totalEquity: number };
  public accounts: { account: string, market: string }[];

  public constructor(init?: Partial<Client>, setMock?: boolean) {
    super(init);
    if (init) { Object.assign(this, init); }

    if (setMock) {
      this.perfil = 'Conservador';
      this.cpf = this.login;
      // this.id = this.getIdPerCpf(this.cpf); TODO verify value
      // this.accounts = [{ account: this.getAccountPerCpf(this.cpf), market: 'XBSP' }]; TODO verify value
      this.selectedAccount = { account: this.accounts[0], balance: null, totalEquity: null };

      if (this.accounts.filter(x => x.account === '60').length === 0) {
        this.accounts.push({ account: '60', market: 'XBSP' });
      }
    } else {
      if (this.login) {
        this.cpf = this.login;
      } else if (this.cpf) {
        this.login = this.cpf;
      }
    }
  }

  public observSelectedAccount() {
    return new Observable<{ account: { account: string, market: string }, balance: number, totalEquity: number }>((subscriber) => {
      const interval = setInterval(() => subscriber.next(this.selectedAccount), 100);
      return () => clearInterval(interval);
    });
  }
}
