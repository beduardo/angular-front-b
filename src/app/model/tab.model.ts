export class Tab {
  private active: boolean = false;
  private title: string = '';
  public indexTab: number = -1;
  public listQuote: Array<string>;
  public content = [];
  constructor(active?: boolean, title?: string, index?: number) {
    this.active = active;
    this.title = title;
    this.indexTab = index;
  }
  setTitle(title: string): void {
    this.title = title;
  }
  setActive(active: boolean): void {
    this.active = active;
  }
  setIndex(index): void {
    this.indexTab = index;
  }
  getTitle(): string {
    return this.title;
  }
  getActive(): boolean {
    return this.active;
  }
  getIndex(): number {
    if (this.indexTab === undefined) {
      return -1;
    } else {
      return this.indexTab;
    }
  }
}
