import { TypeOrderEnum } from '@model/enum/type-order.enum';
import { SideEnum } from '@model/enum/side.enum';
import { MarketEnum } from '@model/enum/market.enum';

export class EditOrder {

  constructor() { }
  public client: string;
  public account: string;
  public quote: string;
  public qtd: number;
  public price: any;
  public market: MarketEnum;
  public type: TypeOrderEnum;
  public side: SideEnum;
  public validityOrder: Date;
  public orderStrategy: string;
  public timeInForces?: string;
  public timeInForce?: string;
  public orderid?: string;
  public orderID?: string;
  public origclordid?: string;
  public loteDefault?: number;
  public total?: number;
  public maxfloor: number;
  public minqtd: number;
  public orderTag: string;
  public quantitySupplied?: number;

  public targettrigger: string;
  public targetlimit: string;
  public stoptrigger: string;
  public stoplimit: string;
}
