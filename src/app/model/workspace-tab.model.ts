import { Tab } from '@model/tab.model';
export class WorkspaceTab extends Tab {
    public content: Array<any>;
    constructor(active: boolean, title: string, index: number, content: Array<any>) {
        super();
        this.content = content;
    }
    setContent(content: Array<any>) {
        this.content = content;
    }
    getContent(): Array<any> {
        return this.content;
    }
    getContentAt(index: number): any {
        if (index < 0 || index >= this.content.length) {
            return null;
        }
        return this.content[index];
    }
    removeContentAt(index: number): boolean {
        if (index < 0 || index >= this.content.length) {
            return false;
        }
        this.content.splice(index);
        return true;
    }
    insertContentAt(index: number, item: any) {
        if (index < 0 || index > this.content.length) {
            return false;
        }
        this.content.splice(index, 0, item);
        return true;
    }
}
