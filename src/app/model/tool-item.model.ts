export class ToolItem {
  constructor(private id?: string, private name?: string, private icon?: string, private active?: boolean) {}
  getName(): string {
    return this.name;
  }
  getIcon(): string {
    return this.icon;
  }
  getId(): string {
    return this.id;
  }
  isActive(): boolean {
    return this.active;
  }
  setActive(active: boolean): void {
    this.active = active;
  }
  setName(name: string): void {
    this.name = name;
  }
  setIcon(icon: string): void {
    this.icon = icon;
  }
  setId(id: string): void {
    this.id = id;
  }
  switchActive(): boolean {
    this.active = !this.active;
    return this.active;
  }
}
