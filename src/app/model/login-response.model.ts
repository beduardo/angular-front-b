export class LoginResponse {
  code: string;
  codeMessage: string;
  message: string;
  accountNumber: string;
  tokenWS: string;
  guidUser: string;
  token: string;
  expiration: string;
  tokenWsGraphic: string;
}
