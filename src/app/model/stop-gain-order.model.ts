export class StopGainOrder {
  public symbol: string;
  public company: string;
  public type: string;
  public quantity: number;
  public quantitySet: boolean;
  public lastTrade: any;
  get valorTotal() {
    return this.calcValorTotal();
  }
  public tunnelUpperLimit: any;
  public tunnelLowerLimit: any;
  public duration: string;
  public expiration: Date;
  public price: number;
  public triggerPrice: number;
  private calcValorTotal() {
    return (
      this.quantity * parseFloat(this.lastTrade.replace(',', '.'))
    ).toFixed(2);
  }
}
