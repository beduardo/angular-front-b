import { ShortcutsEnum } from "@model/enum/shortcuts.enum";
import { DictionaryCollection } from "@utils/dictionary/dictionary-collection";

export interface ICustomComponent {
  customShortcuts: DictionaryCollection<ShortcutsEnum[]>;
}