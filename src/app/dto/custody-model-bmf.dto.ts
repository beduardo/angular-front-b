export class BmfModel {
    public initQuantity: string;
    public quote: string;
    public position: string;
    public executedBuy: string;
    public executedSell: string;
    public lastTradeDate: Date;
    public valueCurrent:string;
    public netDay: string;
    public averagePricePos: string;
    public openProfitLoss: string;
    public closedProfitLoss: string;
    public currentPrice: string;
    public previousAdjustment: string;
    public positionAdjustment: string;
    public precoAjuste: string;
    public valueAdjustedAmount: string;

    constructor(initQuantity?: string, quote?: string, position?: string, executedBuy?: string,
        executedSell?: string, lastTradeDate?: Date) {
            this.initQuantity = initQuantity;
            this.quote = quote;
            this.position = position;
            this.executedBuy = executedBuy;
            this.executedSell = executedSell,
            this.lastTradeDate = lastTradeDate;
    }
}
