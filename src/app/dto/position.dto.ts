import { GridsterItem } from 'angular-gridster2';

export class PositionDto implements GridsterItem {
  public x: number;
  public y: number;
  public cols: number;
  public rows: number;
  public dragEnabled?: boolean;
  public resizeEnabled?: boolean;
  public compactEnabled?: boolean;
  public maxItemRows?: number;
  public minItemRows?: number;
  public maxItemCols?: number;
  public minItemCols?: number;
  public minItemArea?: number;
  public maxItemArea?: number;

  constructor(x: number, y: number, cols: number, rows: number, dragEnabled?: boolean, resizeEnabled?: boolean,
    compactEnabled?: boolean, maxItemRows?: number, minItemRows?: number, maxItemCols?: number, minItemCols?: number,
    minItemArea?: number, maxItemArea?: number) {
    this.x = x;
    this.y = y;
    this.cols = cols;
    this.rows = rows;
    this.dragEnabled = dragEnabled;
    this.resizeEnabled = resizeEnabled;
    this.compactEnabled = compactEnabled;
    this.maxItemRows = maxItemRows;
    this.minItemRows = minItemRows;
    this.maxItemCols = maxItemCols;
    this.minItemCols = minItemCols;
    this.minItemArea = minItemArea;
    this.maxItemArea = maxItemArea;
  }
}
