import { PositionDto } from '@dto/position.dto';

export class WorkspaceItemContentDto {
    public id: number;                  // Used to set Box id
    public symbol: string;
    public company: string;
    public type: string;                // Constants.WINDOW_NAME...
    public position: PositionDto;
    public showInGridster: boolean;
    public workspaceId: number;
    public title: string;
    public worksheetTitle: string;
    public columns?: any;

    constructor(id: number, symbol: string, company: string, type: string, position: PositionDto,
        showInGridster: boolean, workspaceId: number, title?: string, worksheetTitle?: string, columns?: any
    ) {
        this.id = id;
        this.symbol = symbol;
        this.company = company;
        this.type = type;
        this.workspaceId = workspaceId;
        this.position = position;
        this.showInGridster = showInGridster;
        if (title) {
            this.title = title;
        }
        if (worksheetTitle) {
          this.worksheetTitle = worksheetTitle;
        }
        if (columns) {
            this.columns = columns;
        }
      }
}
