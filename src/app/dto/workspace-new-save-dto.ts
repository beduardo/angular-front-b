import { WorkspaceItemContentDto } from '@dto/workspace-item-content-dto';

export class WorkspaceNewSaveDto {
    public title: string;
    public indexTab?: number;
    public content: Array<WorkspaceItemContentDto>;

    constructor(title: string, content: Array<WorkspaceItemContentDto>, indexTab?: number ) {
        this.title = title;
        this.indexTab = indexTab;
        this.content = content;
    }
}
