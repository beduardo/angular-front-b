
export class WorkspaceNewDto {
            public Id: number;
            public UserName: string;
            public Name: string;
            public Active: boolean;
            public IndexTab: number;
            public IsPrivate: boolean;
            public WorkspaceType: number;
            public JsonData: string;
            public IsPreDefined?: boolean;

    constructor(Id: number,
        UserName: string,
        Name: string,
        Active: boolean,
        IndexTab: number,
        IsPrivate: boolean,
        WorkspaceType: number,
        JsonData: string,
        IsPreDefined?: boolean
        ) {
        this.Id = Id;
        this.UserName = UserName;
        this.IsPrivate = IsPrivate;
        this.Name = Name;
        this.JsonData = JsonData;
        this.Active = Active;
        this.IndexTab = IndexTab;
        this.WorkspaceType = WorkspaceType;
        this.IsPreDefined = IsPreDefined;
    }
}
