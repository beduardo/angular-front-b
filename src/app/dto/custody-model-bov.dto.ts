import { AppUtils } from '@utils/app.utils';

export class BovModel {
    public initQuantity: any;
    public currentQuantity: any;
    public positionDay: any;
    public quantityD1: any;
    public quantityD2: any;
    public quantityD3: any;
    public openBuy: any;
    public openSell: any;
    public executedBuy: any;
    public executedSell: any;
    public quantityBlocked: any;
    public quote: any;
    public quantityReleased: any;
    public averagePrice: any;
    public valueInit: any;
    public valueCurrent: any;
    public profitLoss: any;
    public profitLossPercent: any;
    public lastTradeDate: Date;
    public lastTrade: any;
    public netDay: any;
    public avgValueDaytradeToday: any;
    public openProfitLoss: any;
    public openProfitLossColor: number;
    public closedProfitLoss: any;
    public closedProfitLossColor: number;
    public quoteType: any;
    public precoMedioDoadorBTC: any;
    public precoMedioTomadorBTC: any;

    constructor(initQuantity?: any, currentQuantity?: any, position?: any, quantityD1?: any, quantityD2?: any,
        quantityD3?: any, openBuy?: any, openSell?: any, executedBuy?: any, executedSell?: any, quantityBlocked?: any,
        quote?: any, quantityReleased?: any, averagePrice?: any, valueInit?: any, valueCurrent?: any, profitLoss?: any,
        profitLossPercent?: any, lastTradeDate?: Date, lastTrade?: any, netDay?: any, avgValueDaytradeToday?: any,
        openProfitLoss?: any, closedProfitLoss?: any, quoteType?: any, precoMedioDoadorBTC?: any, precoMedioTomadorBTC?: any) {
        this.initQuantity = initQuantity;
        this.currentQuantity = currentQuantity;
        this.positionDay = position;
        this.executedBuy = executedBuy;
        this.executedSell = executedSell;
        this.quantityD1 = quantityD1;
        this.quantityD2 = quantityD2;
        this.quantityD3 = quantityD3;
        this.openBuy = openBuy;
        this.openSell = openSell;
        this.executedBuy = executedBuy;
        this.executedSell = executedSell;
        this.quantityBlocked = quantityBlocked;
        this.quote = quote;
        this.quantityReleased = quantityReleased;
        this.averagePrice = averagePrice;
        this.valueInit = valueInit;
        this.valueCurrent = valueCurrent;
        this.profitLoss = profitLoss;
        this.profitLossPercent = profitLossPercent;
        this.lastTradeDate = lastTradeDate;
        this.lastTrade = lastTrade;
        this.netDay = netDay;
        this.avgValueDaytradeToday = avgValueDaytradeToday;
        this.openProfitLoss = openProfitLoss;
        this.closedProfitLoss = closedProfitLoss;
        this.quoteType = quoteType;
        this.precoMedioDoadorBTC = precoMedioDoadorBTC;
        this.precoMedioTomadorBTC = precoMedioTomadorBTC;
    }
}
