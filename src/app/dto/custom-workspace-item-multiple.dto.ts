import { CustomWorkspaceItemInterface } from '@dto/custom-workspace-item-interface.dto';

export class CustomWorkspaceItemMultipleDto implements CustomWorkspaceItemInterface {
    public id: number;
    public type: string;
    public assetSymbol: string[];
    public company: string[];
    public title: string;
    public removeFlag: boolean;

    constructor(
        id: number,
        type: string,
        assetSymbol: Array<string>,
        company: Array<string>,
        title: string,
        removeFlag: boolean
    ) {
        this.id = id;
        this.type = type;
        this.assetSymbol = assetSymbol;
        this.company = company;
        this.title = title;
        this.removeFlag = removeFlag;
    }
}
