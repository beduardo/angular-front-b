export interface CustomWorkspaceItemInterface {
    id: number;
    type: string;
    assetSymbol: string[] | string;
    company: string[] | string;
    title: string;
    removeFlag: boolean;
}
