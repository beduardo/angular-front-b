
export class WorkspaceConfModalDto {
  public id: number;
  public title: string;
  public text: string;
  public jsondata: string;
  public isPersonalize: boolean;
  public iconName?: string;

  constructor(id: number, title: string, text: string, jsondata: string, isPersonalize: boolean, iconName?: string) {
    this.id = id;
    this.title = title;
    this.text = text;
    this.jsondata = jsondata;
    this.isPersonalize = isPersonalize;
    this.iconName = iconName ? iconName : '';
  }
}
