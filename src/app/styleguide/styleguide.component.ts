import { Component, OnInit, ViewContainerRef, ViewChild } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material';
import { CenteredModalComponent } from '@component/centered-modal/centered-modal.component';
import { NormalModalComponent } from '@component/normal-modal/normal-modal.component';
import { FullheightModalComponent } from '@component/fullheight-modal/fullheight-modal.component';
import { NotificationsService } from 'angular2-notifications';
import { CustomSnackbarService } from '@services/custom-snackbar-service.service';
import { Constants } from '@utils/constants';

@Component({
  selector: 'app-styleguide',
  templateUrl: './styleguide.component.html',
})
export class StyleguideComponent implements OnInit {
  @ViewChild('styleguide', { read: ViewContainerRef }) parent: ViewContainerRef;

  gridOptions: any;
  columnDefs = [
    { headerName: 'Make', field: 'make' },
    { headerName: 'Model', field: 'model' },
    { headerName: 'Price', field: 'price' }
  ];

  rowData = [
    { make: 'Toyota', model: 'Celica', price: 35000 },
    { make: 'Ford', model: 'Mondeo', price: 32000 },
    { make: 'Porsche', model: 'Boxter', price: 72000 }
  ];
  // FORM
  email = new FormControl('', [Validators.required, Validators.email]);

  constructor(public dialog: MatDialog, private _service: NotificationsService, public snackBar: CustomSnackbarService) {
  }

  ngOnInit() {
  }

  // MODAL
  openModalFull() {
    this.dialog.open(FullheightModalComponent, {
      panelClass: 'mat-modal-fullheight',
      position: { left: '0' },
      width: '555px'
    });
  }

  openModalCentered() {
    this.dialog.open(CenteredModalComponent, {
      data: {
        title: 'Código de redefinição enviado',
        body: 'Se houver uma conta  vinculada a este endereço de e-mail, enviaremos instruções para redefinir sua senha.'
      },
      panelClass: 'mat-modal-centered'
    });
  }

  openModalNormal() {
    this.dialog.open(NormalModalComponent, {
      data: {
        title: 'Código de redefinição enviado',
        body: 'Se houver uma conta  vinculada a este endereço de e-mail, enviaremos instruções para redefinir sua senha.'
      },
      panelClass: 'mat-modal-normal',
    });
  }

  // NOTIFICATIONS
  notifySuccess() {
    this._service.success('tsete', 'aaahhhh');
  }

  notifyInfo() {
    this._service.info('tsete', 'aaahhhh');
  }

  notifyError() {
    this._service.error('tsete', 'aaahhhh');
  }

  notifyWarn() {
    this._service.warn('tsete', 'aaahhhh');
  }

  notifyNormal() {
    this._service.alert('tsete', 'aaahhhh');
  }

  // ALERTS
  alertSuccess() {
    this.snackBar.createSnack(this.parent, 'aaahhhh', Constants.ALERT_TYPE.success);
  }

  alertInfo() {
    this.snackBar.createSnack(this.parent, 'aaahhhh', Constants.ALERT_TYPE.info);
  }

  alertError() {
    this.snackBar.createSnack(this.parent, 'aaahhhh', Constants.ALERT_TYPE.error);
  }

  alertWarn() {
    this.snackBar.createSnack(this.parent, 'aaahhhh', Constants.ALERT_TYPE.warn);
  }
}
