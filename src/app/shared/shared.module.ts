import { DatepickerMaskDirective } from './directives/datepicker-mask.directive';
import { NumberPipe } from './pipes/number.pipe';
import { AutoSizeInputDirective } from './directives/autosize-input.directive';
import { EditOrderComponent } from '@component/edit-order/edit-order.component';
import { IncrementalInputComponent } from '@component/incremental-input/incremental-input.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SpinnerComponent } from '@component/spinner/spinner.component';
import { CurrencyComponent } from '@component/currency/currency.component';
import { ModalMessageConfirmComponent } from '@component/modal-message-confirm/modal-message-confirm.component';
import { NgbTooltipModule, NgbDatepickerModule } from '@ng-bootstrap/ng-bootstrap';
import { NgxMatDrpModule } from 'ngx-mat-daterange-picker';
import {
  MatButtonModule,
  MatDialogModule,
  MatSelectModule,
  MatInputModule,
  MatCheckboxModule,
  MatAutocompleteModule,
  MatCardModule,
  MatGridListModule,
  MatTabsModule,
  MatMenuModule,
  MatDatepickerModule,
  MatNativeDateModule,
  MatExpansionModule,
  MatRadioModule,
  MatListModule,
  MatButtonToggleModule,
  MatSlideToggleModule,
  MatIconModule,
  MatSnackBarModule,
  MatBottomSheetModule,
  MatProgressSpinnerModule,
  MatTooltipModule,
  MatFormFieldModule,
  DateAdapter,
  MAT_DATE_LOCALE,
  MAT_DATE_FORMATS
} from '@angular/material';
import { BookControllerComponent } from '@business/book/book.controller';
import { GridsterModule } from 'angular-gridster2';
import { RouterModule } from '@angular/router';
import { QuoteChartComponent } from '@business/quote-chart/quote-chart.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AutoCompleteComponent } from '@component/auto-complete/auto-complete.component';
import { QuickSearchAutoCompleteComponent } from '@component/quick-search-auto-complete/quick-search-auto-complete.component';
import { HttpClientModule } from '@angular/common/http';
import { HttpModule, JsonpModule } from '@angular/http';
import { AgGridModule } from 'ag-grid-angular';
import { TradingViewComponent } from '@business/trading-view/trading-view.component';
import { CenteredModalComponent } from '@component/centered-modal/centered-modal.component';
import { NormalModalComponent } from '@component/normal-modal/normal-modal.component';
import { FullheightModalComponent } from '@component/fullheight-modal/fullheight-modal.component';
import { WorkspaceConfModalComponent } from '@component/workspace-conf-modal/workspace-conf-modal.component';
import { AssetSummaryComponent } from '@business/asset-summary/asset-summary.component';
import { AssetSummaryPopUpComponent } from '@business/asset-summary-pop-up/asset-summary-pop-up.component';
import { QuickSearchComponent } from '@business/quick-search/quick-search.component';
import { UserSettingsPopUpComponent } from '@component/user-settings-pop-up/user-settings-pop-up.component';
import { UserSettingsComponent } from '@component/user-settings/user-settings.component';
import { TimesTradesComponent } from '@business/times-trades/times-trades.component';
import { WorksheetComponent } from '@business/worksheet/worksheet.component';
import { FilterPipe } from '@shared/pipes/filter.pipe';
import { NgbToDatePipe } from '@shared/pipes/ngbToDate.pipe';
import { BuySellModalComponent } from '@component/buy-sell-modal/buy-sell-modal.component';
import { VolumeAtPriceComponent } from '@business/volume-at-price/volume-at-price.component';
import { UpdatePasswordComponent } from '@business/update-password/update-password.component';
import { BrokerAuthenticationComponent } from '@business/broker-authentication/broker-authentication.component';
import { SimultaneousStopOrderComponent } from '@component/simultaneous-stop-order/simultaneous-stop-order.component';
import { CurrencyMaskModule } from 'ng2-currency-mask';
import { OrdersHistoryComponent } from '@business/orders-history/orders-history.component';
import { OrdersHistoryDetailComponent } from '@business/orders-history/orders-history-detail/orders-history-detail.component';
import { OrdersHistoryPaginatorComponent } from '@business/orders-history/orders-history-paginator/orders-history-paginator.component';
import { ChartModule } from 'angular-highcharts';
import { WorksheetConfigModalComponent } from '@business/worksheet/worksheet-config-modal/worksheet-config-modal.component';
import { ReconnectComponent } from '@business/reconnect/reconnect.component';
import { ModalTradingViewComponent } from '@business/modal-trading-view/modal-trading-view.component';
import { ModalNewsComponent } from '@business/modal-news/modal-news.component';
import { NewsCardComponent } from '@business/modal-news/news-card/news-card.component';
import { LottieAnimationViewModule } from 'ng-lottie';
import { CustodyConsultComponent } from '@business/custody-consult/custody-consult.component';
import { NewsGridComponent } from '@business/modal-news/news-grid/news-grid.component';
import { NewsDetailComponent } from '@business/modal-news/news-detail/news-detail.component';
import { NgxMasonryModule } from 'ngx-masonry';
import { BalanceConsultComponent } from '@business/balance-consult/balance-consult.component';
import { CurrencyPipe } from '@shared/pipes/currency.pipe';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NgbdDatepickerRange } from '@component/datepicker-range/datepicker-range';
import { SendOrderConfirmModalComponent } from '@component/send-order-confirm-modal/send-order-confirm-modal.component';
import { ComponentSelectorInputComponent } from '@component/component-selector-input/component-selector-input.component';
import { ComponentBlankComponent } from '@component/component-blank/component-blank.component';
import { ComponentSelectorSelectComponent } from '@component/component-selector-select/component-selector-select.component';
import { StopGainBuyOrderComponent } from '@component/stop-gain-buy-order/stop-gain-buy-order.component';
import { CustomWorkspaceModalComponent } from '@component/custom-workspace-modal/custom-workspace-modal.component';
import { MinResolutionModalComponent } from '@component/min-resolution-modal/min-resolution-modal.component';
import { ErrorScreenComponent } from '../component/error-screen/error-screen.component';
import { YoutubeLiveComponent } from '@component/youtube-live/youtube-live.component';
import { SafeUrlPipe } from './pipes/safe-url.pipe';
import { SessionModalComponent } from '../component/session-modal/session-modal.component';
import { YoutubeLiveModalComponent } from '../component/youtube-live/youtube-live-modal/youtube-live-modal.component';
import { AutoBlurDirective } from '@utils/autoFocus';
import { ModalPasswordComponent } from '../component/modal-password/modal-password.component';
import { AuthBrokerComponent } from '../business/auth-broker/auth-broker.component';
import { ScrollDispatchModule } from '@angular/cdk/scrolling';
import { Overlay, CloseScrollStrategy } from '@angular/cdk/overlay';
import { MAT_AUTOCOMPLETE_SCROLL_STRATEGY } from '@angular/material';
import { MomentDateAdapter } from '@angular/material-moment-adapter';
import { MY_DATE_FORMATS } from '@utils/constants';
import { UpdateSignatureComponent } from '../business/update-signature/update-signature.component';
import { ModalSignatureComponent } from '../modal-signature/modal-signature.component';
import { AssessorDataComponent } from '../business/assessor-data/assessor-data.component';
import { AssessorSearchClientComponent } from '../business/assessor-search-client/assessor-search-client.component';
import { AutoCompleteAssessorComponent } from '../component/auto-complete-assessor/auto-complete-assessor.component';
import { TermOperatorComponent } from '../component/term-operator/term-operator.component';
import { ExtractFinancialComponent } from '../business/extract-financial/extract-financial.component';
import { BrokerageNoteComponent } from '../business/brokerage-note/brokerage-note.component';
import { RankingComponent } from '../business/ranking/ranking.component';
import { EducationalComponent } from '../business/educational/educational.component';
import { ManualOnlineComponent } from '../business/manual-online/manual-online.component';

export function scrollFactory(overlay: Overlay): () => CloseScrollStrategy {
  return () => overlay.scrollStrategies.close();
}

const materialModules = [
  MatButtonModule,
  MatDialogModule,
  MatIconModule,
  MatSelectModule,
  NgbDatepickerModule,
  NgbTooltipModule,
  MatAutocompleteModule,
  MatCardModule,
  MatGridListModule,
  MatTabsModule,
  MatButtonToggleModule,
  MatInputModule,
  MatMenuModule,
  MatDatepickerModule,
  MatNativeDateModule,
  MatCheckboxModule,
  MatExpansionModule,
  MatRadioModule,
  MatSlideToggleModule,
  MatListModule,
  MatSnackBarModule,
  MatProgressSpinnerModule,
  MatBottomSheetModule,
  MatTooltipModule,
  MatFormFieldModule
];
@NgModule({
  imports: [
    ChartModule,
    CurrencyMaskModule,
    HttpModule,
    HttpClientModule,
    CommonModule,
    NgbModule,
    GridsterModule,
    NgxMatDrpModule,
    RouterModule,
    FormsModule,
    JsonpModule,
    ReactiveFormsModule,
    AgGridModule.withComponents([
      CurrencyComponent
    ]),
    materialModules,
    LottieAnimationViewModule.forRoot(),
    NgxMasonryModule,
    CommonModule,
    CurrencyMaskModule,
    ScrollDispatchModule
  ],
  declarations: [
    IncrementalInputComponent,
    AssetSummaryComponent,
    AssetSummaryPopUpComponent,
    UserSettingsComponent,
    UserSettingsPopUpComponent,
    QuickSearchComponent,
    SpinnerComponent,
    CurrencyComponent,
    ModalMessageConfirmComponent,
    ReconnectComponent,
    BookControllerComponent,
    AutoCompleteComponent,
    QuickSearchAutoCompleteComponent,
    QuoteChartComponent,
    TradingViewComponent,
    TimesTradesComponent,
    WorksheetComponent,
    WorkspaceConfModalComponent,
    FullheightModalComponent,
    NormalModalComponent,
    CenteredModalComponent,
    TimesTradesComponent,
    FilterPipe,
    NgbToDatePipe,
    VolumeAtPriceComponent,
    UpdatePasswordComponent,
    BrokerAuthenticationComponent,
    SimultaneousStopOrderComponent,
    OrdersHistoryComponent,
    EditOrderComponent,
    OrdersHistoryDetailComponent,
    OrdersHistoryPaginatorComponent,
    WorksheetConfigModalComponent,
    ModalTradingViewComponent,
    ModalNewsComponent,
    NewsCardComponent,
    CustodyConsultComponent,
    NewsGridComponent,
    NewsDetailComponent,
    BalanceConsultComponent,
    CurrencyPipe,
    NumberPipe,
    NgbdDatepickerRange,
    BuySellModalComponent,
    SendOrderConfirmModalComponent,
    CustomWorkspaceModalComponent,
    ComponentSelectorInputComponent,
    ComponentBlankComponent,
    ComponentSelectorSelectComponent,
    StopGainBuyOrderComponent,
    MinResolutionModalComponent,
    ErrorScreenComponent,
    YoutubeLiveComponent,
    SafeUrlPipe,
    SessionModalComponent,
    YoutubeLiveModalComponent,
    AutoBlurDirective,
    ModalPasswordComponent,
    AuthBrokerComponent,
    AutoSizeInputDirective,
    DatepickerMaskDirective,
    UpdateSignatureComponent,
    ModalSignatureComponent,
    AssessorDataComponent,
    AssessorSearchClientComponent,
    AutoCompleteAssessorComponent,
    TermOperatorComponent,
    ExtractFinancialComponent,
    BrokerageNoteComponent,
    RankingComponent,
    EducationalComponent,
    ManualOnlineComponent
  ],
  entryComponents: [
    BuySellModalComponent,
    CustomWorkspaceModalComponent,
    AssetSummaryComponent,
    AssetSummaryPopUpComponent,
    UserSettingsComponent,
    UserSettingsPopUpComponent,
    TradingViewComponent,
    WorkspaceConfModalComponent,
    FullheightModalComponent,
    NormalModalComponent,
    ModalPasswordComponent,
    CenteredModalComponent,
    ModalMessageConfirmComponent,
    AutoCompleteComponent,
    EditOrderComponent,
    UpdatePasswordComponent,
    UpdateSignatureComponent,
    WorksheetConfigModalComponent,
    ReconnectComponent,
    BrokerAuthenticationComponent,
    ModalTradingViewComponent,
    ModalNewsComponent,
    CustodyConsultComponent,
    BalanceConsultComponent,
    MinResolutionModalComponent,
    YoutubeLiveModalComponent,
    SessionModalComponent,
    AuthBrokerComponent,
    ModalSignatureComponent,
    AssessorDataComponent,
    AssessorSearchClientComponent,
    AutoCompleteAssessorComponent,
    TermOperatorComponent,
    ExtractFinancialComponent,
    BrokerageNoteComponent,
    RankingComponent,
    EducationalComponent,
    ManualOnlineComponent
  ],
  exports: [
    IncrementalInputComponent,
    AssetSummaryComponent,
    AssetSummaryPopUpComponent,
    UserSettingsComponent,
    UserSettingsPopUpComponent,
    QuickSearchComponent,
    SpinnerComponent,
    CurrencyComponent,
    ModalMessageConfirmComponent,
    ReconnectComponent,
    BookControllerComponent,
    AutoCompleteComponent,
    QuickSearchAutoCompleteComponent,
    materialModules,
    RouterModule,
    FormsModule,
    JsonpModule,
    ReactiveFormsModule,
    QuoteChartComponent,
    GridsterModule,
    ScrollDispatchModule,
    TradingViewComponent,
    TimesTradesComponent,
    WorksheetComponent,
    WorkspaceConfModalComponent,
    FilterPipe,
    NgbToDatePipe,
    VolumeAtPriceComponent,
    UpdatePasswordComponent,
    UpdateSignatureComponent,
    SimultaneousStopOrderComponent,
    OrdersHistoryComponent,
    OrdersHistoryDetailComponent,
    EditOrderComponent,
    OrdersHistoryPaginatorComponent,
    WorksheetConfigModalComponent,
    ModalTradingViewComponent,
    ModalNewsComponent,
    NewsCardComponent,
    CustodyConsultComponent,
    NewsGridComponent,
    NewsDetailComponent,
    BalanceConsultComponent,
    BuySellModalComponent,
    SendOrderConfirmModalComponent,
    CustomWorkspaceModalComponent,
    ComponentSelectorInputComponent,
    ComponentBlankComponent,
    ComponentSelectorSelectComponent,
    StopGainBuyOrderComponent,
    CommonModule,
    MinResolutionModalComponent,
    CurrencyMaskModule,
    ErrorScreenComponent,
    YoutubeLiveComponent,
    AutoSizeInputDirective,
    DatepickerMaskDirective,
    AssessorDataComponent,
    AssessorSearchClientComponent,
    AutoCompleteAssessorComponent,
    TermOperatorComponent,
    ExtractFinancialComponent,
    BrokerageNoteComponent,
    RankingComponent,
    EducationalComponent,
    ManualOnlineComponent
  ],
  providers: [
    { provide: MAT_AUTOCOMPLETE_SCROLL_STRATEGY, useFactory: scrollFactory, deps: [Overlay] },
    { provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE] },
    { provide: MAT_DATE_FORMATS, useValue: MY_DATE_FORMATS },
  ]
})
export class SharedModule { }
