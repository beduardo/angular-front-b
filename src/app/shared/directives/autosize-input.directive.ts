import { Directive, Input, OnChanges, HostListener, ElementRef, Renderer2, AfterViewInit, AfterContentChecked } from '@angular/core';

@Directive({
  selector: '[appAutosizeInput]'
})
export class AutoSizeInputDirective implements AfterContentChecked {

  @Input() minWidth = 80;
  @Input() maxWidth = 180;
  @Input() autoSizeActive = false;

  private value;

  constructor(
    public element: ElementRef,
    public renderer: Renderer2) { }

  // @HostListener('input', ['$event.target'])
  // public onInput(target): void {
  //   this.adjustInput();
  //   console.log(target.value)
  // }

  ngAfterContentChecked() {
    if ((this.autoSizeActive) && (this.element.nativeElement.value !== this.value)) {
      this.value = this.element.nativeElement.value;
      this.adjustInput();
    }
  }

  adjustInput() {
    const style = window.getComputedStyle(this.element.nativeElement);
    const font = style.fontVariant + ' ' + style.fontStyle + ' ' + style.fontWeight + ' ' + style.fontSize + ' ' + style.fontFamily;
    const width = this.calcWidth(this.element.nativeElement.value.toUpperCase(), font);

    if (width <= this.minWidth) {
      this.renderer.setStyle(this.element.nativeElement.parentNode, 'width', `${this.minWidth}px`);
    } else if (width >= this.maxWidth) {
      this.renderer.setStyle(this.element.nativeElement.parentNode, 'width', `${this.maxWidth}px`);
    } else {

      this.renderer.setStyle(this.element.nativeElement.parentNode, 'width', `${width}px`);
    }

  }

  calcWidth(text, font) {
    const canvas = this.calcWidth['canvas'] || (this.calcWidth['canvas'] = document.createElement('canvas'));
    const context = canvas.getContext('2d');
    context.font = font;
    const metrics = context.measureText(text);

    return metrics.width;
  }
}
