import { Directive, HostListener, ElementRef } from '@angular/core';

@Directive({
  selector: '[appDatepickerMask]'
})
export class DatepickerMaskDirective {
  constructor(
    public element: ElementRef) { }

  @HostListener('keypress', ['$event, $event.target'])
  public onInput(event, target): void {
    if (/[0-9]/.test(event.key)) {
      if (target.value.length === 2 || target.value.length === 5) {
        target.value += '/';
      }
    } else {
      event.preventDefault();
    }
  }
}
