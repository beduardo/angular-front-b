import { Pipe, PipeTransform } from '@angular/core';
import * as _s from 'underscore.string';

@Pipe({
  name: 'number'
})
export class NumberPipe implements PipeTransform {

  transform(item: any, decimalPlaces = 2): any {
    if (!item) {
      return '';
    } else {

      return _s.numberFormat(
        Number(item),
        decimalPlaces,
        ',',
        '.'
      );
    }
  }

}
