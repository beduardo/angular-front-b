import { Pipe, PipeTransform } from '@angular/core';
import { AppUtils } from '@utils/app.utils';

@Pipe({
  name: 'ngbToDate'
})
export class NgbToDatePipe implements PipeTransform {

  transform(item: any, args?: any): any {
    if (!item) {
      return null;
    } else {
      return new Date(item.year, item.month - 1, item.day);
    }
  }

}
