import { Pipe, PipeTransform } from '@angular/core';
import { AppUtils } from '@utils/app.utils';

@Pipe({
  name: 'currency'
})
export class CurrencyPipe implements PipeTransform {

  transform(item: any, args?: any): any {
    if (!item) {
      return '';
    } else {
      if (item.toString().includes('-')) {
        return 'R$' + item;
      }
      return 'R$' + AppUtils.convertToCurrencyString(item);
    }
  }

}
