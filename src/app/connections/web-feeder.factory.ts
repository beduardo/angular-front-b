import { DictionaryCollection } from '@utils/dictionary/dictionary-collection';

export class WebFeederFactory {

  private static dicConnections: DictionaryCollection<WebSocket> = new DictionaryCollection<WebSocket>();

  public static appComponent: any;

  public static getWebFeederConnection(url: string = '', isReconnected: boolean = false): WebSocket {
    if (isReconnected) {
      this.dicConnections.remove(url);
    }
    if (this.dicConnections !== null && !this.dicConnections.containsKey(url)) {
      const webSocket = new WebSocket(url);
      this.dicConnections.add(url, webSocket);
    }

    return this.dicConnections.getItems[url];

    // if (this.dicConnections && this.dicConnections.count() === 0) {
    //   const webSocket = new WebSocket(url);
    //   this.dicConnections.add(url, webSocket);
    // }
    // return this.dicConnections.getItems[url];
  }

  // public static closeWebFeeder(url: string) {
  //   if (this.dicConnections !== null && this.dicConnections.containsKey(url)) {
  //     const webSocket = this.dicConnections.getItems[url];
  //     this.dicConnections.remove(url);
  //     webSocket.close();
  //   }
  // }

  public static closeAllConnections() {
    this.dicConnections.values().forEach(element => {
      element.close();
    });

    this.dicConnections.clear();
  }

  public static closeWebFeeder(url: string) {
    if (this.dicConnections !== null && !this.dicConnections.containsKey(url)) {
      const webSocket = this.dicConnections.getItems[url];
      this.dicConnections.clear();
      if (webSocket) {
        webSocket.close();
      }
    }
  }

  // public static closeAllConnections() {
  //   this.dicConnections.values().forEach(element => {
  //     element.close();
  //   });

  //   this.dicConnections.clear();
  // }

  public static existsWS(url: string) {
    return this.dicConnections.containsKey(url);
  }
}