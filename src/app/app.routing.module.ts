import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from '@auth/auth.guard';
import { AssetSummaryComponent } from '@business/asset-summary/asset-summary.component';
import { BookControllerComponent } from '@business/book/book.controller';
import { LoginComponent } from '@business/login/login.component';
import { OrdersHistoryComponent } from '@business/orders-history/orders-history.component';
import { TimesTradesComponent } from '@business/times-trades/times-trades.component';
import { VolumeAtPriceComponent } from '@business/volume-at-price/volume-at-price.component';
import { WorksheetComponent } from '@business/worksheet/worksheet.component';
import { ErrorScreenComponent } from '@component/error-screen/error-screen.component';
import { StyleguideComponent } from '@styleguide/styleguide.component';
import { TradingViewComponent } from '@business/trading-view/trading-view.component';
import { YoutubeLiveComponent } from '@component/youtube-live/youtube-live.component';
import { RecoverPasswordComponent } from '@business/recover-password/recover-password.component';

const APP_ROUTES: Routes = [
  { path: '', redirectTo: '/login', pathMatch: 'full', canActivate: [AuthGuard] },
  { path: 'home', loadChildren: './business/home/home.module#HomeModule' },
  { path: 'chart', component: TradingViewComponent, canActivate: [AuthGuard] },
  { path: 'timestrades', component: TimesTradesComponent, canActivate: [AuthGuard] },
  { path: 'login', component: LoginComponent },
  { path: 'recoverPassword', component: RecoverPasswordComponent },
  { path: 'logout', component: LoginComponent },
  { path: 'asset-summary', component: AssetSummaryComponent, canActivate: [AuthGuard] },
  // { path: 'styleguide', component: StyleguideComponent },
  { path: 'book', component: BookControllerComponent, canActivate: [AuthGuard] },
  { path: 'worksheet', component: WorksheetComponent, canActivate: [AuthGuard] },
  { path: 'volume-at-price', component: VolumeAtPriceComponent, canActivate: [AuthGuard] },
  { path: 'history', component: OrdersHistoryComponent, canActivate: [AuthGuard] },
  { path: 'error', component: ErrorScreenComponent },
  { path: 'live', component: YoutubeLiveComponent, canActivate: [AuthGuard] },
  { path: '**', component: ErrorScreenComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(APP_ROUTES, { useHash: false })],
  exports: [RouterModule]
})

export class AppRoutingModule { }
