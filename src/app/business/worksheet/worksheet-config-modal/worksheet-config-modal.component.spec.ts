import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WorksheetConfigModalComponent } from '@business/worksheet/worksheet-config-modal/worksheet-config-modal.component';

describe('WorksheetConfigModalComponent', () => {
  let component: WorksheetConfigModalComponent;
  let fixture: ComponentFixture<WorksheetConfigModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WorksheetConfigModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WorksheetConfigModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
