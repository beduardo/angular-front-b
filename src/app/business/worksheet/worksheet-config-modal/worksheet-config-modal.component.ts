import { Component, OnInit, Inject, ViewContainerRef, ViewChild } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Constants } from '@utils/constants';
import { CustomSnackbarService } from '@services/custom-snackbar-service.service';
import { NotificationsService } from 'angular2-notifications';

@Component({
  selector: 'app-worksheet-config-modal',
  templateUrl: './worksheet-config-modal.component.html'
})
export class WorksheetConfigModalComponent implements OnInit {

  public listSelected;
  public listColumns = Constants.worksheetCols;
  public modelList = [];
  public qtdItens = 0;
  public bottomArrow = false;

  @ViewChild('worksheetConfig', { read: ViewContainerRef }) parent: ViewContainerRef;

  constructor(
    public dialogRef: MatDialogRef<WorksheetConfigModalComponent>,
    public _NotificationService: NotificationsService,
    @Inject(MAT_DIALOG_DATA) public data: any,
    public snackBar: CustomSnackbarService
  ) {
    this.listSelected = data.columns;
    this.bottomArrow = data.bottomArrow;
    this.dialogRef.backdropClick()
      .subscribe(result => {
        this.close();
      });
  }

  ngOnInit() {
    this.setModel();
  }

  setModel() {
    this.listColumns.forEach((element, index) => {
      if (this.listSelected.some(e => e.field === element.field)) {
        this.modelList[index] = true;
        this.qtdItens++;
      } else {
        this.modelList[index] = false;
      }
    });
  }

  defaultColumns() {
    this.listColumns.forEach((element, index) => {
      if (this.listSelected.some(e => e.field === element.field)) {
        this.modelList[index] = false;
      }
    });
    this.qtdItens = 0;
    this.listSelected = [];
    this.listSelected = Constants.worksheetCols.slice(0, 17);
    this.listColumns.forEach((element, index) => {
      if (this.listSelected.some(e => e.field === element.field)) {
        this.modelList[index] = true;
        this.qtdItens++;
      }
    });
  }

  clearColumns() {
    this.listColumns.forEach((element, index) => {
      if (this.listSelected.some(e => e.field === element.field)) {
        this.modelList[index] = false;
      }
    });
    this.qtdItens = 0;
  }

  close() {
    const list = [];
    this.listColumns.forEach((element, index) => {
      if (this.modelList[index]) {
        list.push(element);
      }
    });
    this.dialogRef.close({ data: list });
  }

  onChange(event, index) {
    if (event.checked) {
      if (this.qtdItens > 19) {
        setTimeout(() => {
          this.modelList[index] = false;
        }, 300);
        this._NotificationService.warn('', Constants.WKS_WORKSPACE_WORKSHEET);
        return;
      }
      this.qtdItens++;
    } else {
      this.qtdItens--;
    }
  }

  onResize() {
    this.close();
  }
}
