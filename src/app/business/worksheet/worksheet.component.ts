import { AuthServer } from './../../services/auth-server.service';
import {
  AfterViewInit, ChangeDetectorRef, Component, ElementRef, Input, OnDestroy, OnInit,
  ViewChild, ViewContainerRef, HostListener
} from '@angular/core';
import { MatDialog } from '@angular/material';
import { ActivatedRoute } from '@angular/router';
import { AbstractItem } from '@business/base/abstract-item';
import { AbstractItemService } from '@business/base/abstract-item.service';
import { Box } from '@business/home/workspace-new/workspace-new.component';
import { WorksheetConfigModalComponent } from '@business/worksheet/worksheet-config-modal/worksheet-config-modal.component';
import { AutoCompleteComponent } from '@component/auto-complete/auto-complete.component';
import { MessageQuote } from '@model/webFeeder/message-quote.model';
import { BilletOrderModalService } from '@services/billet-order-modal.service';
import { QuoteService } from '@services/webFeeder/sckt-quotes.service';
import { WorkspaceNewService } from '@services/workspace-new.service';
import { AppUtils } from '@utils/app.utils';
import { Comparators } from '@utils/comparators.utils';
import { Constants } from '@utils/constants';
import { GridApi, GridOptions } from 'ag-grid';
import { NotificationsService } from 'angular2-notifications';
import { Subscription } from 'rxjs';
import { DatePipe } from '@angular/common';


@Component({
  selector: Constants.WINDOW_NAME_WORK_SHEET,
  templateUrl: './worksheet.component.html'
})
export class WorksheetComponent extends AbstractItem
  implements OnInit, OnDestroy, AfterViewInit {
  @Input() data: Box;
  public assetList: MessageQuote[] = [];
  public listSymbol = [];
  public updatedListSymbol = [];
  private subscriptionQuote: Subscription;
  public newSymbol = '';
  public newWindow = false;
  public newAsset = false;
  public loading = true;
  public windowActive: any;
  public editRow: number;
  public removeRowIndex: number;
  public title = 'PLANILHA DE COTAÇÃO';
  public titleInput = this.title;
  public editTitleFlag = false;
  public flagExistsQuote;
  public removePosition = '-30px';
  public isOnRemove = false;
  public rowSelection;
  public checkedItems = false;
  public lastCheckedItems = false;
  public checkIndeterminate = false;
  public lengthChecked = 0;
  public gridOptions: GridOptions;
  public filterOn = false;

  public listColumns = Constants.worksheetCols.slice(0, 17);
  // ag-grid
  private gridApi: GridApi;
  private gridColumnApi;
  public lastTradeList = [];
  public columnDefs: any[] = [];
  public rowData: any[] = [];
  public defaultColDef;
  dialog: MatDialog;
  protected workspaceService: WorkspaceNewService;
  public rowClassRules = {
    'row-even': function (params) {
      return params.node.rowIndex % 2 === 0;
    }
  };

  public rowDataOldValue: string;
  public rowDataValue: any;
  public rowDataIndex: any;
  public rowDataUnsubscribe = 0;

  public isStart: boolean;
  public dataWs: any;
  public lastTradeTemp = 0;

  @ViewChild('inputTitle') inputTitle: ElementRef;
  @ViewChild('inputFilter') inputFilter: ElementRef;
  @ViewChild('worksheet', { read: ViewContainerRef }) parent: ViewContainerRef;

  @HostListener('document:keydown', ['$event'])
  onKeyDownHandler(event: KeyboardEvent) {
    if (event.key === 'Escape') {
      this.editingStopped();
    }
  }

  constructor(
    private quoteService: QuoteService,
    private route: ActivatedRoute,
    private cd: ChangeDetectorRef,
    private billetOrderModalService: BilletOrderModalService,
    dialog: MatDialog,
    workspaceService: WorkspaceNewService,
    itemService: AbstractItemService,
    private el: ElementRef,
    private notificationService: NotificationsService,
    private _authServer: AuthServer,
    private datePipe: DatePipe
  ) {
    super(workspaceService, dialog, itemService);
    const self = this;
    this.gridOptions = <GridOptions>{
      enableColResize: true,
      processRowPostCreate: (params) => {
        this.generateRowEvents(params, self);
      },
      getRowNodeId: (data) => {
        return data.id;
      },
      localeText: { noRowsToShow: 'Não há dados para serem exibidos' },
      context: { componentParent: this, scope: this },
    };
    // this.setColums();
    this.dialog = dialog;
    this.workspaceService = workspaceService;
    this.initWks();
  }

  ngOnInit() {
    this.lastTradeTemp = 0;
    this.initWks();
  }

  initWks() {
    setTimeout(() => {
      const context = this;
      this.setListSymbol();
      if (this.data) {
        this.setColums();
        if ((this.data && this.data.symbol !== '')) {
          this.listSymbol = this.data.symbol.split(',');
        }
        if (this.data.worksheetTitle) {
          this.title = this.data.worksheetTitle;
          this.titleInput = this.data.worksheetTitle;
        }
      } else if (this.listColumns) {
        this.setColums();
      }
      window.addEventListener('beforeunload', function (e) {
        if (context.newWindow) {
        }
      });
    }, 0);
  }

  setListSymbol() {
    let listUserColumnsTemp;
    const listColumns = Constants.worksheetCols;
    this.route.queryParams.subscribe(params => {
      if (params['symbol']) {
        this.newWindow = true;
        if (typeof params['symbol'] === 'string') {
          this.listSymbol = [params['symbol']];
        } else {
          this.listSymbol = params['symbol'];
        }
      }
      if (params['columns']) {
        this.listColumns = [];
        listUserColumnsTemp = params['columns'];
        for (let i = 0; i < listUserColumnsTemp.length; i++) {
          for (let x = 0; x < listColumns.length; x++) {
            if (listUserColumnsTemp[i] === listColumns[x].field) {
              this.listColumns.push(listColumns[x]);
              break;
            }
          }
        }
      }
      this.loading = false;
    });
  }

  ngAfterViewInit() {
    this.cd.detectChanges();
  }

  ngOnDestroy() {
    this.closeSheet();
    if (this.subscriptionQuote) {
      this.subscriptionQuote.unsubscribe();
    }
    if (this.listSymbol.length > 0) {
      this.listSymbol.forEach((element, index) => {
        this.quoteService.unsubscribeQuote(element, Constants.WINDOW_NAME_WORK_SHEET);
      });
    }
  }

  saveChanges() {
    const aglist = [];
    this.gridApi.forEachNode(function (node) {
      if (node.data.ativo !== '') {
        aglist.push(node.data.ativo);
      }
    });
    const agListAux = aglist.toString();
    const aux = this.data.symbol;
    this.data.symbol = agListAux;
    super.updateOnDemand(aux, agListAux);
  }

  setQuote(symbol: string, index: number) {
    symbol = AppUtils.trimStart(symbol);
    symbol = symbol.toLowerCase();
    if (!symbol && symbol !== '') {
      return;
    }
    if (!this.newWindow && !this.isStart) {
      this.saveChanges();
    }
    this.assetList[index] = new MessageQuote();
    if (this.validSymbol(symbol)) {
      this.quoteService.subscribeQuote(symbol, Constants.WINDOW_NAME_WORK_SHEET);
      this.subscriptionQuote = this.quoteService.messagesQuote
        .subscribe((msg: MessageQuote) => {
          if (msg && msg.errorMessage && msg.symbol.toLowerCase() === symbol.toLowerCase()) {
            if (this.rowDataUnsubscribe === 0) {
              if (this.quoteService.subscribeQuote(msg.symbol, Constants.WINDOW_NAME_WORK_SHEET)) {
                this.quoteService.unsubscribeQuote(msg.symbol, Constants.WINDOW_NAME_WORK_SHEET);
              }
              this.treatmenInvalidQuote(msg);
            }
          } else if (msg !== undefined && msg.symbol != null && msg.symbol.toLowerCase() === symbol.toLowerCase()) {
            this.rowDataUnsubscribe = 0;
            if (this.editRow !== index) {
              this.assetList[index] = msg;
              const list = [];
              this.gridApi.forEachNode(function (node) {
                list.push(node);
              });
              list.forEach(data => {
                if (data.data.ativo === msg.symbol.toUpperCase()) {
                  this.setData(msg, data.rowIndex);
                }
              });
            }
          }
        }
        );
    }
  }
  private treatmenInvalidQuote(msg) {
    this.notificationService.warn('', Constants.WORKSPACE_MSG007);
    let oldValue = 'novo';
    let oldAssetValue = '';
    if (this.rowDataOldValue && this.rowDataOldValue !== '') {
      oldValue = this.rowDataOldValue.toUpperCase();
      oldAssetValue = this.rowDataOldValue.toUpperCase();
    }
    const dataEmpty = this.emptyRow();
    dataEmpty.id = oldValue;
    dataEmpty.ativo = oldAssetValue;
    setTimeout(() => {
      if (oldAssetValue === '') {
        this.gridApi.updateRowData({
          remove: [{ id: msg.symbol.toUpperCase() }],
          add: [dataEmpty],
          addIndex: this.rowDataIndex
        });
        this.gridApi.setFocusedCell(this.rowDataIndex, 'ativo');
        this.gridApi.startEditingCell({
          rowIndex: this.rowDataIndex,
          colKey: 'ativo'
        });
        this.saveChanges();
      } else {
        this.gridApi.updateRowData({
          remove: [{ id: msg.symbol.toUpperCase() }],
          add: [dataEmpty],
          addIndex: this.rowDataIndex
        });
        msg = new MessageQuote();
        this.rowDataUnsubscribe = 1;
        this.quoteService.subscribeQuote(this.rowDataOldValue, Constants.WINDOW_NAME_WORK_SHEET);
        if (!this.newWindow) {
          this.saveChanges();
        }
      }
    }, 0);
  }

  private validSymbol(symbol): boolean {
    const quote = this.quoteService.acessListQuotes(symbol);
    if (quote && quote[0] && quote[0].values) {
      if (quote[0].values.hasOwnProperty('-1')) {
        this.treatmenInvalidQuote(quote[0]);
        return false;
      }
      return true;
    } else {
      return true;
    }
  }

  toggleInput() {
    this.newAsset = !this.newAsset;
  }
  selectAsset(event) {
    this.listSymbol.push(event.symbol);
    this.setQuote(event.symbol, this.listSymbol.length - 1);
    this.toggleInput();
  }

  openNewWindow() {
    if (!this.data.symbol) {
      this.notificationService.warn('', 'Necessário ter pelo menos um ativo na planilha de cotação.');
      return;
    }
    let parameters = '';
    let first = true;
    this.gridApi.forEachNode(function (node) {
      if (node.data.ativo && node.data.ativo !== '') {
        if (first) {
          parameters = parameters + '?symbol=' + node.data.ativo;
          first = false;
        } else {
          parameters = parameters + '&symbol=' + node.data.ativo;
        }
      }
    });
    if (this.data && this.data.columns) {
      this.data.columns.forEach(element => {
        parameters = parameters + '&columns=' + element.field;
      });
    }
    const options = 'width=' + this.el.nativeElement.offsetParent.offsetWidth
      + 'px, height=' + this.el.nativeElement.offsetParent.offsetHeight + 'px';
    if (this.windowActive == null || this.windowActive.closed) {
      super.getNewWindow(Constants.WINDOW_NAME_WORK_SHEET, parameters, options, this.data.id)
        .subscribe(window => {
          this.windowActive = window;
        });
    } else {
      this.windowActive.focus();
    }
  }

  closeSheet() {
    if (this.windowActive != null) {
      this.windowActive.close();
    }
  }

  openNewTab() {
    let parameters = '';
    this.listSymbol.forEach((element, index) => {
      if (index === 0) {
        parameters = parameters + '?symbol=' + element;
      } else {
        parameters = parameters + '&symbol=' + element;
      }
    });
    window.open('worksheet' + parameters);
  }
  editAsset(event, i) {
    this.editRow = i;
  }

  selectEdit(event, i) {
    this.editRow = null;
    if (event.symbol !== '') {
      this.quoteService.unsubscribeQuote(event.symbol, Constants.WINDOW_NAME_WORK_SHEET);
      this.listSymbol[i] = event.symbol;
      this.subscriptionQuote.unsubscribe();
      this.setQuote(event.symbol, i);
    }
  }
  selectRow(i) {
    this.removeRowIndex = i;
  }

  removeAsset(i) {
    this.removeRowIndex = null;
    this.quoteService.unsubscribeQuote(this.listSymbol[i], Constants.WINDOW_NAME_WORK_SHEET);
    this.subscriptionQuote.unsubscribe();
    this.listSymbol.splice(i, 1);
    this.assetList.splice(i, 1);
  }

  toggleEditTitle() {
    this.editTitleFlag = !this.editTitleFlag;
    if (this.editTitleFlag) {
      this.titleInput = this.title;
      setTimeout(() => {
        this.inputTitle.nativeElement.focus();
      }, 100);
    }
  }

  checkTitle() {
    if (this.titleInput !== '' && this.isAlphanumeric(this.titleInput)) {
      this.title = this.titleInput.toUpperCase();
      if (this.titleInput !== this.data.worksheetTitle) {
        this.data.worksheetTitle = this.titleInput.toUpperCase();
        super.updateContentOnDemand();
      }
    } else {
      this.notificationService.warn('',
        'O nome da planilha deve conter pelo menos 1 caracter alfanumérico.');
    }
    this.toggleEditTitle();
  }

  checkColumns(listColumns: any) {
    if (listColumns && this.data) {
      this.data.columns = listColumns;
      super.updateContentOnDemand();
    } else {
      this.listColumns = listColumns;
    }
  }

  isAlphanumeric(txt) {
    let code = null;
    for (let i = 0; i < txt.length; i++) {
      code = txt.charCodeAt(i);
      if ((code > 47 && code < 58) ||
        (code > 64 && code < 91) ||
        (code > 96 && code < 123)) {
        return true;
      }
    }
    return false;
  }

  setColums() {
    this.columnDefs = [];
    this.columnDefs.push({
      colId: 'id',
      headerName: 'id',
      field: 'id',
      hide: true
    });
    this.columnDefs.push({
      headerName: 'ATIVO',
      width: 150,
      maxWidth: 300,
      field: 'ativo',
      editable: true,
      cellEditorFramework: AutoCompleteComponent,
      comparator: this.stringComparator,
      cellClass: 'txt-left',
      headerClass: 'txt-left'
    });
    if (this.data && this.data.columns && this.data.columns.length > 0) {
      this.listColumns = [];
      this.data.columns.forEach(element => {
        const columns = {
          class: element.class,
          field: element.field,
          header: element.header,
          headerClass: element.headerClass
        };
        this.listColumns.push(columns);
      });
    }
    this.listColumns.forEach(element => {
      let widthTemp;
      if (element.header.length < 10) {
        widthTemp = 80;
      } else if (element.header.length <= 14) {
        widthTemp = 120;
      } else if (element.header.length <= 16) {
        widthTemp = 130;
      } else {
        widthTemp = 165;
      }
      if (element.field === 'variation') {
        this.columnDefs.push({
          headerName: element.header,
          field: element.field,
          cellRenderer: this.renderVariation,
          width: 100,
          maxWidth: 300,
          suppressCellFlash: true,
          cellClass: element.class,
          headerClass: element.headerClass
        });
      } else if (element.field === 'description') {
        this.columnDefs.push({
          headerName: element.header,
          field: element.field,
          comparator: this.stringComparator,
          cellRenderer: this.renderTooltip,
          cellClass: element.class,
          headerClass: element.headerClass
        });
      } else if (element.field === 'lastTrade') {
        this.columnDefs.push({
          headerName: element.header,
          field: element.field,
          comparator: this.valueComparator,
          cellRenderer: this.renderLastTrade,
          suppressCellFlash: true,
          width: 100,
          maxWidth: 300,
          cellClass: element.class,
          headerClass: element.headerClass
        });
      } else if (element.field === 'bid') {
        this.columnDefs.push({
          headerName: element.header,
          field: element.field,
          cellRenderer: this.renderBuy,
          valueFormatter: this.priceFormatter,
          suppressCellFlash: true,
          width: 100,
          maxWidth: 300,
          cellClass: element.class,
          headerClass: element.headerClass
        });
      } else if (element.field === 'ask') {
        this.columnDefs.push({
          headerName: element.header,
          field: element.field,
          cellRenderer: this.renderSell,
          valueFormatter: this.priceFormatter,
          suppressCellFlash: true,
          width: 100,
          maxWidth: 300,
          cellClass: element.class,
          headerClass: element.headerClass
        });
      } else if (element.field === 'timeUpdate') {
        this.columnDefs.push({
          headerName: element.header,
          field: element.field,
          valueFormatter: this.timeFormatter,
          width: 100,
          maxWidth: 300,
          cellClass: element.class,
          headerClass: element.headerClass
        });
      } else if (element.field === 'dateTrade') {
        this.columnDefs.push({
          headerName: element.header,
          field: element.field,
          valueFormatter: this.timeFormatter,
          width: 100,
          maxWidth: 300,
          cellClass: element.class,
          headerClass: element.headerClass
        });
      } else if (element.field === 'open') {
        this.columnDefs.push({
          headerName: element.header,
          field: element.field,
          valueFormatter: this.timeFormatter,
          width: 100,
          maxWidth: 300,
          cellClass: element.class,
          headerClass: element.headerClass
        });
      } else {
        this.columnDefs.push({
          headerName: element.header,
          field: element.field,
          width: widthTemp,
          maxWidth: 300,
          cellClass: element.class,
          headerClass: element.headerClass
        });
      }
    });
    this.defaultColDef = {
      width: 150,
      maxWidth: 300,
      headerCheckboxSelection: this.isFirstColumn,
      headerCheckboxSelectionFilteredOnly: true,
      checkboxSelection: this.isFirstColumn,
      suppressKeyboardEvent: function (event) {
        if (event.editing) {
          return true;
        }
      }
    };
    this.rowSelection = 'multiple';
  }
  isFirstColumn(params) {
    const displayedColumns = params.columnApi.getAllDisplayedColumns();
    const thisIsFirstColumn = displayedColumns[0] === params.column;
    return thisIsFirstColumn;
  }
  timeFormatter(params) {
    if (params.value && params.value.length === 12) {
      return params.value.substring(0, params.value.length - 4);
    } else {
      return params.value;
    }
  }
  priceFormatter(params) {
    if (params.value === undefined) {
      return '0,00';
    } else {
      return params.value;
    }
  }
  valueComparator(value1, value2) {
    return Comparators.brFormattedNumberComparator(value1, value2);
  }
  stringComparator(value1, value2, nodea, nodeb, inverted) {

    if (value1 === '' && inverted) {
      return 1;
    } if (value1 === '' && !inverted) {
      return -1;
    } if (value1 > value2) {
      return 1;
    } if (value1 < value2) {
      return -1;
    }
    return 0;
  }

  renderBuy(param) {
    if (param.value === '' || param.value === undefined) {
      param.value = '0,00';
    }
    if (param.value === '-') {
      return '<div class="buy-renderer disabled"><span class="buy-span">' + param.value + '</span></div>';
    } else {
      return '<div class="buy-renderer"><span class="buy-span">' + param.value + '</span></div>';
    }
  }

  renderSell(param) {
    if (param.value === '' || param.value === undefined) {
      param.value = '0,00';
    }
    if (param.value === '-') {
      return '<div class="sell-renderer disabled"><span class="sell-span">' + param.value + '</span></div>';
    } else {
      return '<div class="sell-renderer"><span class="sell-span">' + param.value + '</span></div>';
    }
  }
  renderLastTrade(param) {
    if (param.value) {
      let last = 0;
      const valueNumber = parseFloat(param.value.replace(',', '.'));
      if (param.data.lastTrade && param.data.lastTrade.toString() === '-') {
        param.data.lastTrade = param.data.previous;
      }
      if (param.data.lastTradeTemp) {
        last = parseFloat(param.data.lastTradeTemp.replace(',', '.'));
      }
      if (valueNumber < last) {
        return '<div class="negative-renderer"><span class="low-color weight-500">' + param.data.lastTrade + '</span></div>';
      } else {
        return '<div class="positive-renderer"><span class="high-color weight-500">' + param.data.lastTrade + '</span></div>';
      }
    }
    return '';
  }
  renderVariation(param) {
    if (param.value) {
      const valueNumber = parseFloat(param.value);
      const valueDate = param.value.toString().split('/');
      if (valueDate.length > 1) {
        return '<span>' + param.value + '</span>';
      } else if (valueNumber > 0) {
        return '<span class="high-color">+' + AppUtils.numberToFormattedNumber(valueNumber) + '</span>';
      } else if (valueNumber < 0) {
        return '<span class="low-color">' + AppUtils.numberToFormattedNumber(valueNumber) + '</span>';
      }
    }
    return '0,00';
  }
  renderTooltip(param) {
    if (param.value) {
      return '<span title="' + param.value + '">' + param.value + '</span>';
    } else {
      return '';
    }
  }
  openBilletBuy() {

  }
  setData(msg: MessageQuote, i: any) {
    if (this.gridApi) {
      const rowNode = this.gridApi.getDisplayedRowAtIndex(i);
      if (rowNode) {
        let varAjustp = 0;
        let varAjust = 0;
        let varTeor = 0;
        let lastTradeConst = 0;
        let previousAdjustConst = 0;
        let theoryPriceConst = 0;
        let previousConst = 0;
        if (msg.lastTrade && msg.lastTrade.toString() !== '-') {
          lastTradeConst = AppUtils.convertBrCurrencyToNumber(msg.lastTrade.toString(), ',');
        }
        if (msg.previousAdjustPrice && msg.previousAdjustPrice.toString() !== '-') {
          previousAdjustConst = AppUtils.convertBrCurrencyToNumber(msg.previousAdjustPrice.toString(), ',');
        }
        if (msg.theoryPrice && msg.theoryPrice.toString() !== '-') {
          theoryPriceConst = AppUtils.convertBrCurrencyToNumber(msg.theoryPrice.toString(), ',');
        }
        if (msg.previous && msg.previous.toString() !== '-') {
          previousConst = AppUtils.convertBrCurrencyToNumber(msg.previous.toString(), ',');
        }
        if (lastTradeConst > 0 && previousAdjustConst > 0) {
          varAjustp = (lastTradeConst / previousAdjustConst - 1) * 100;
          if (varAjustp !== 0) {
            varAjustp = Number(Number(varAjustp).toFixed(2));
          }
          varAjust = lastTradeConst - previousAdjustConst;
        }
        if (theoryPriceConst > 0 && previousConst > 0) {
          varTeor = theoryPriceConst - previousConst;
          if (varTeor !== 0) {
            varTeor = Number(Number(varTeor).toFixed(2));
          }
        }
        const data = {
          id: msg.symbol.toUpperCase(),
          ativo: msg.symbol.toUpperCase(),
          varAjustp: AppUtils.convertNumberToCurrency(varAjustp, null, '') + '%',
          varAjust: AppUtils.convertNumberToCurrency(varAjust, null, '') + '%',
          varTeor: AppUtils.convertNumberToCurrency(varTeor, null, '') + '%',
          lastTrade: this.lastTradeList[i]
        };
        this.listColumns.forEach(element => {
          if (element.field !== 'varAjustp' && element.field !== 'varAjust' && element.field !== 'varTeor') {
            data[element.field] = msg[element.field];
          }
          if (msg && element.field === 'variation') {
            const newDate = this.datePipe.transform(new Date(), 'dd/MM/yyyy');
            if ((msg[element.field] &&
              (msg[element.field].toString() === '-' || msg[element.field].toString() === '0.0' || msg[element.field] === 0))
              && newDate !== msg.dateLastNegotiation) {
              data[element.field] = msg.dateLastNegotiation;
            }
          }
        });
        data['lastTradeTemp'] = this.lastTradeList[i];
        this.gridApi.updateRowData({ update: [data] });
      }
      this.lastTradeList[i] = msg.lastTrade;
    }
  }
  onGridReady(param) {
    this.gridApi = param.api;
    this.gridColumnApi = param.columnApi;
    this.isStart = true;
    this.listSymbol.forEach((element, index) => {
      this.gridApi.updateRowData({
        add: [
          {
            id: element.toUpperCase(),
            ativo: element.toUpperCase()
          }
        ]
      });
      if (this.newWindow) {
        setTimeout(() => {
          this.setQuote(element, index);
        }, 2000);
      } else {
        this.setQuote(element, index);
      }
    });
    this.isStart = false;
  }

  existQuote(symbol) {

    if (symbol && symbol !== '') {
      this._authServer.iQuote.quotesQuery(symbol.toLowerCase()).subscribe(datas => {
        if (datas && datas.length > 0) {
          const data = datas.filter(
            item => item.symbol === symbol.toUpperCase()
          );
          if (data.length === 1) {
            this.flagExistsQuote = true;
          } else {
            this.flagExistsQuote = false;
          }
        }

      });
    }
  }
  onCellValueChanged(event) {
    this.rowDataOldValue = event.oldValue;
    this.rowDataIndex = event.rowIndex;
    if (event.data.data) {
      this.rowDataValue = event.data.data;
    } else {
      this.rowDataValue = event.data;
    }
    let data;
    if (event.data.data) {
      data = event.data.data;
    } else {
      data = event.data;
    }
    const rowNode = this.gridApi.getRowNode(event.newValue.toUpperCase());
    if (event.newValue.toUpperCase() !== '' && event.newValue.toUpperCase() !== event.oldValue.toUpperCase() && rowNode === undefined) {
      this.listSymbol[data.id] = event.newValue.toUpperCase();
      this.gridApi.updateRowData({
        remove: [{ id: data.id }],
        add: [{
          id: event.newValue.toUpperCase(),
          ativo: event.newValue.toUpperCase()
        }],
        addIndex: event.rowIndex
      });
      this.setQuote(event.newValue, data.id);
    } else {
      const oldData = data;
      if (event.oldValue) {
        oldData.ativo = event.oldValue;
      } else {
        oldData.ativo = '';
      }
      if (event.newValue !== event.oldValue) {
        if (rowNode === undefined || rowNode === null) {
          this.notificationService.warn('', Constants.WORKSPACE_MSG007);
        } else {
          this.notificationService.warn('', Constants.WORKSPACE_MSG017);
          this.gridApi.setFocusedCell(this.rowDataIndex, 'ativo');
          this.gridApi.startEditingCell({
            rowIndex: this.rowDataIndex,
            colKey: 'ativo'
          });
        }
      }
      this.gridApi.updateRowData({
        update: [
          oldData
        ],
      });
      return;
    }
    this.gridApi.forEachNode(function (rowNodet) {
      rowNodet.setDataValue('setRandom', 5);
    });
  }

  onRemoveSelected() {
    if (this.gridApi !== undefined) {
      const selectedData = this.gridApi.getSelectedRows();
      this.gridApi.updateRowData({ remove: selectedData });
      selectedData.forEach(element => {
        this.quoteService.unsubscribeQuote(element.ativo.toUpperCase(), Constants.WINDOW_NAME_WORK_SHEET);
      });
      this.subscriptionQuote.unsubscribe();

      if (this.gridApi.getDisplayedRowCount() <= 0) {
        this.workspaceService.resizeComponent({ componentId: this.data.id, rows: 1, cols: null });
      }
      this.saveChanges();
    }
  }

  removeGrid() {
    super.removeItemFromWorkspace();
  }
  emptyRow() {
    const data = { id: '', ativo: '' };
    this.listColumns.forEach(element => {
      if (element.field !== 'lastTrade') {
        data[element.field] = '';
      } else {
        data[element.field] = '0';
      }
    }
    );
    return data;
  }
  addNew() {
    let add = true;
    const length = this.gridApi.getDisplayedRowCount();
    if (length >= Constants.QTD_MAX_ASSETS_WORKSHEET) {
      add = false;
      this.notificationService.warn('', Constants.WORKSHEET_MAX_ASSETS_MSG);
      return;
    }
    this.gridApi.forEachNode(function (node) {
      if (node.data.ativo === '' || node.data.ativo === undefined) {
        add = false;
        return;
      }
    });
    if (add) {
      this.listSymbol.push('');
      const data = this.emptyRow();
      data.id = 'novo';
      this.gridApi.updateRowData({ add: [data], addIndex: 0 });
      setTimeout(() => {
        this.gridApi.setFocusedCell(0, 'ativo');
        this.gridApi.startEditingCell({ rowIndex: 0, colKey: 'ativo' });
      });

    }
  }
  onCellHover(event) {
    const qtd = 80 + 28 * event.rowIndex;
    this.removePosition = qtd + 'px';
  }
  onCellOut(event) {
    if (!this.isOnRemove) {
      this.removePosition = '-30px';
    }
  }
  btnRemoveEnter(event) {
    this.isOnRemove = true;
  }
  btnRemoveLeave(event) {
    this.isOnRemove = false;
  }
  openFilter() {
    this.toggleFilter();
  }

  destroyFilter() {
    this.gridApi.destroyFilter('ativo');
  }

  filterAsset(valueFilter) {
    const filterInput = {
      ativo: {
        type: 'contains',
        filter: valueFilter
      }
    };
    this.gridApi.setFilterModel(filterInput);
    this.gridApi.onFilterChanged();
  }
  keyup(event) {
    this.filterAsset(event.target.value);
  }
  symbolChanged(event) {
    this.filterAsset(event.symbol);
  }
  closeFilter() {
    this.destroyFilter();
    this.filterOn = false;
  }
  toggleFilter() {

    if (!this.filterOn) {
      setTimeout(() => {
        document.getElementById('input-filter').getElementsByTagName('input')[0].focus();
      }, 400);
    }
    document.getElementById('input-filter').getElementsByTagName('input')[0].value = '';
    this.destroyFilter();
    this.filterOn = !this.filterOn;
  }
  onCellClicked(event) {
    if (this.newWindow) {
      return;
    }
    if ((event.event.target.className === 'buy-renderer' || event.event.target.className === 'buy-span') && (event.data.ask !== '-' && event.data.bid !== '-')) {
      this.billetOrderModalService.buySellOrder('sell', event.data.ativo, event.data.bid);
    } else if ((event.event.target.className === 'sell-renderer' || event.event.target.className === 'sell-span') && (event.data.ask !== '-' && event.data.bid !== '-')) {
      this.billetOrderModalService.buySellOrder('buy', event.data.ativo, event.data.ask);
    }
  }

  public generateRowEvents(params, parent) {
    params.eRow.draggable = true;
    params.eRow.ondragstart = (event: DragEvent) => {
      parent.newRowIndex = params.node.rowIndex;
      parent.currentRowIndex = params.node.rowIndex;
    };
    params.eRow.ondragenter = (event: DragEvent) => {
      parent.newRowIndex = params.node.rowIndex;
    };
    params.eRow.ondragleave = (event: DragEvent) => {
      parent.newRowIndex = params.node.rowIndex;
    };
    params.eRow.ondragover = (event: DragEvent) => {
      parent.newRowIndex = params.node.rowIndex;
    };
    params.eRow.ondragend = (event: DragEvent) => {
      const sortmodel = parent.gridOptions.api.getSortModel();
      if (sortmodel.length === 0 && parent.newRowIndex > -1 && parent.newRowIndex !== params.node.rowIndex) {
        const record = params.node.data;
        const rowData = [];
        this.gridApi.forEachNode(function (node) {
          rowData.push(node.data);
        });
        parent.gridOptions.api.setRowData(rowData);
        rowData.splice(parent.newRowIndex, 0, rowData.splice(parent.currentRowIndex, 1)[0]);
        parent.gridOptions.api.removeItems([params.node], false);
        parent.gridOptions.api.insertItemsAtIndex(parent.newRowIndex, [record], false);
        // TODO parent.gridApi.redrawRows();
        this.gridApi.forEachNode(function (rowNode) {
          rowNode.setDataValue('setRandom', 5);
        });

      } else {
        parent.newRowIndex = -1;
      }
    };
  }

  editingStarted(event) {
    this.editRow = event.rowIndex;
  }

  editingStopped(event?) {
    const firstRow = this.gridApi ? this.gridApi.getDisplayedRowAtIndex(0) : null;
    if (firstRow && firstRow.id === 'novo') {
      this.gridApi.selectIndex(0, false, false);
      this.onRemoveSelected();
      return;
    }
    this.editRow = null;
  }

  onSelectionChanged(event) {
    const length = this.gridApi.getDisplayedRowCount();
    this.lengthChecked = event.api.getSelectedNodes().length;
    if (this.lengthChecked > 0) {
      if (this.lengthChecked === length) {
        this.checkedItems = true;
        setTimeout(() => {
          this.checkIndeterminate = false;
        }, 100);
      } else {
        this.checkIndeterminate = true;
        this.checkedItems = false;
      }
    } else {
      this.checkedItems = false;
      this.checkIndeterminate = false;
    }
  }
  changeCheck(event) {
    if (event.checked) {
      this.gridApi.selectAllFiltered();
    } else {
      this.gridApi.deselectAllFiltered();
    }
  }
  openConfig(event) {
    let topPosition = event.y + 5;
    const leftPosition = event.x - 710;
    let bottomArrow = false;
    if (topPosition + 325 >= window.innerHeight && event.y - 10 - 325 > 0) {
      topPosition = event.y - 19 - 325;
      bottomArrow = true;
    }

    const dialogRef = this.dialog.open(WorksheetConfigModalComponent, {
      width: '818.3px',
      height: '340.8px',
      position: {
        left: leftPosition + 'px',
        top: topPosition + 'px'
      },
      panelClass: 'worksheet-config-modal-container',
      data: {
        columns: this.listColumns,
        bottomArrow: bottomArrow
      },
      disableClose: true,
      backdropClass: 'worksheet-config-modal-backdrop'
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result.data.length > 0) {
        this.listColumns = result.data;
      } else {
        this.listColumns = Constants.worksheetCols.slice(0, 17);
      }
      this.checkColumns(this.listColumns);
      this.setColums();
    });
  }
}
