import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material';

@Component({
  selector: 'app-ranking',
  templateUrl: './ranking.component.html'
})
export class RankingComponent implements OnInit {

  public title = 'RANKING';
  public loading = true;
  public urlIframe = 'https://data.fastmarkets.com.br/ranking/desafiodeinvestimentos';

  constructor(
    public dialogRef: MatDialogRef<RankingComponent>,
  ) {
    setTimeout(() => {
      this.loading = false;
    }, 1000);
  }

  ngOnInit() {
  }

  close() {
    this.dialogRef.close();
  }

}
