import { StatusOmsService } from '@services/webFeeder/sckt-status-oms.service';
import { animate, style, transition, trigger } from '@angular/animations';
import { Component, OnDestroy, OnInit, ViewChild, HostListener } from '@angular/core';
import { MatDialogRef } from '@angular/material';
import { FinancialAccountInformation } from '@model/webFeeder/financial.model';
import { NgbTooltip } from '@ng-bootstrap/ng-bootstrap';
import { ConfigService } from '@services/webFeeder/config.service';
import { AppUtils } from '@utils/app.utils';
import { Constants } from '@utils/constants';
import { Subscription } from 'rxjs';
import { AutoLogoutService } from '@auth/auto-logout-service.service';
import { environment } from 'environments/environment';
import { FinancialComplService } from '@services/webFeeder/sckt-financial.compl.service';
import { StatusConnection } from '@model/status-connection';

@Component({
  selector: 'app-balance-consult',
  templateUrl: './balance-consult.component.html',
  animations: [
    trigger('valueAnimation', [
      transition('void => *', [
        style({ transform: 'translateX(0) scale(0)' }),
        animate('0.2s ease-in')
      ])
    ]),
    trigger('valueListAnimation', [
      transition('* => void', [
        animate('0.3s ease-in-out', style({ transform: 'translateX(-100%)' }))
      ]),
      transition('void => *', [
        style({
          transform: 'translateX(-100%)'
        }),
        animate('0.3s 300ms ease-in-out')
      ])
    ]),
    trigger('valueLDetailAnimation', [
      transition('* => void', [
        animate('0.3s ease-in-out', style({ transform: 'translateX(100%)' }))
      ]),
      transition('void => *', [
        style({
          transform: 'translateX(100%)'
        }),
        animate('0.3s 300ms ease-in-out')
      ])
    ])
  ]
})
export class BalanceConsultComponent implements OnInit, OnDestroy {

  public title = 'FINANCEIRO';
  private subscriptionBalance: Subscription;
  private subscriptionStatusOms: Subscription;
  public statusOmsOnline: boolean;
  public msgStatusOmsOffile = Constants.MSG_STATUS_OFFLINE_OMS;
  public availableBalanceColor = 0;
  public availableBalance = '';
  public availableBalanceTool = '';
  public saldoProjOnline = '';
  public saldoProjOnlineTool = '';
  public saldoProjOnlineColor = 0;
  public lucroPrejuizoTotal = '';
  public lucroPrejuizoTotalTool = '';
  public lucroPrejuizoTotalColor = 0;
  public variableIncome = '';
  public variableIncomeTool = '';
  public limiteOperacional = '';
  public limiteOperacionalColor = 0;
  public alavancagemVista = '';
  public alavancagemVistaColor = 0;
  public alavancagemOpcoes = '';
  public alavancagemOpcoesColor = 0;
  public alavancagemBMF = '';
  public alavancagemBMFColor = 0;
  public lucroPrejuizoOpen = '';
  public lucroPrejuizoOpenTool = '';
  public lucroPrejuizoOpenColor = 0;
  public lucroPrejuizoClose = '';
  public lucroPrejuizoCloseTool = '';
  public lucroPrejuizoCloseColor = 0;
  public ajustePregao = '0';
  public ajustePregaoColor = 0;
  public patrimonioOnline = '0';
  public patrimonioOnlineColor = 0;
  public patrimonioInicial = '0';
  public patrimonioInicialColor = 0;
  public rendaFixa = '0';
  public rendaFixaColor = 0;
  public fundos = '0';
  public fundosColor = 0;
  public clubes = '0';
  public clubesColor = 0;
  public tesouroDireto = '0';
  public tesouroDiretoColor = 0;
  public custodiaOnline = '0';
  public custodiaOnlineColor = 0;
  public proventos = '0';
  public proventosColor = 0;
  public ajustePosicao = '0';
  public ajustePosicaoColor = 0;
  public garantia = '0';
  public garantiaColor = 0;
  public totalOnlineStockPortfolioValue = '';
  public totalOnlineOptionsPortfolioValue = '';
  public totalOnlineBmfPortfolioValue = '';
  public account = '';
  public market = '';
  public financialList: FinancialAccountInformation[] = [];
  public filteredHistory = [];
  public loading: boolean;
  public counterIdFilter = 0;
  public numberOfLines = 15;
  public showpassword = false;
  public D0 = '';
  public D0Tool = '';
  public D0Color = 0;
  public D1 = '';
  public D1Tool = '';
  public D1Color = 0;
  public D2 = '';
  public D2Tool = '';
  public D2Color = 0;
  public garantiaLivreColor = 0;
  public garantiaLivre = '';
  public whiteLabel = environment.WHITELABEL;
  public intervalAccount: any;
  icon = 'visibility_off';
  @ViewChild('tooltipLeft') tooltipLeft: NgbTooltip;
  @ViewChild('tooltipRight') tooltipRight: NgbTooltip;

  @HostListener('mousemove')
  onMouseMove() {
    this._autoLogoutService.resetTime();
  }

  @HostListener('click')
  onClick() {
    this._autoLogoutService.resetTime();
  }


  constructor(
    public dialogRef: MatDialogRef<BalanceConsultComponent>,
    private configService: ConfigService,
    private _autoLogoutService: AutoLogoutService,
    private _financialComplService: FinancialComplService,
    private _statusOmsService: StatusOmsService
  ) {
    this.variableIncome = '0';
    this.lucroPrejuizoClose = '0';
    this.limiteOperacional = '0';
    this.alavancagemVista = '0';
    this.alavancagemOpcoes = '0';
    this.alavancagemBMF = '0';
    this.lucroPrejuizoOpenTool = '0';
    this.lucroPrejuizoCloseTool = '0';
    this.lucroPrejuizoTotal = '0';
    this.lucroPrejuizoTotalTool = '0';
    this.saldoProjOnline = '0';
    this.saldoProjOnlineTool = '0';
    this.availableBalance = '0';
    this.lucroPrejuizoOpen = '0';
    this.D0 = '0';
    this.D1 = '0';
    this.D2 = '0';
    this.loading = true;
  }

  ngOnInit() {
    this.subscriptionStatusOms = this._statusOmsService.messagesStatusOms
      .subscribe(statusOms => {
        if (statusOms === StatusConnection.ERROR) {
          this.loading = false;
          this.statusOmsOnline = false;
        } else {
          this.intervalAccount = setInterval(() => {
            this.statusOmsOnline = true;
            this.account = this.configService.getBrokerAccountSelected();
            this.market = Constants.MARKET_BOVESPA;     // TODO Desmock please, without market info might return value, verify API
            if (this.account && this.market) {
              clearInterval(this.intervalAccount);
              this.getFinancialAccountInformationCompl();
            }
          }, 1000);
        }
      });
  }

  ngOnDestroy() {
    if (this._financialComplService && this.account) {
      this._financialComplService.unsubscribeFinancial(this.account);
      if (this.subscriptionBalance) {
        this.subscriptionBalance.unsubscribe();
      }
    }
    if (this.subscriptionStatusOms) {
      this.subscriptionStatusOms.unsubscribe();
    }
    if (this.intervalAccount) {
      clearInterval(this.intervalAccount);
    }
  }

  public getFinancialAccountInformationCompl() {
    this._financialComplService.subscribeFinancial(this.account);
    this.subscribeFinancialService();
  }

  public subscribeFinancialService() {
    this.subscriptionBalance = this._financialComplService.messages
      .subscribe(data => {
        if (data) {
          data.financialInformationBean.filter(element =>
            (element.uniqueLimit
              && AppUtils.convertToCurrencyString(Number(element.uniqueLimit.value).toString()) !== this.limiteOperacional) ||
            (element.cashLeveragedLimit
              && AppUtils.convertToCurrencyString(Number(element.cashLeveragedLimit.value).toString()) !== this.alavancagemVista) ||
            (element.optionsLeveragedLimit
              && AppUtils.convertToCurrencyString(Number(element.optionsLeveragedLimit.value).toString()) !== this.alavancagemOpcoes) ||
            (element.openProfitLoss
              && AppUtils.convertToCurrencyString(Number(element.openProfitLoss.value).toString()) !== this.lucroPrejuizoOpen) ||
            (element.closedProfitLoss
              && AppUtils.convertToCurrencyString(Number(element.closedProfitLoss.value).toString()) !== this.lucroPrejuizoClose) ||
            (element.profitTotalLoss
              && AppUtils.convertToCurrencyString(Number(element.profitTotalLoss.value).toString()) !== this.lucroPrejuizoTotal) ||
            (element.balanceProjOnline
              && AppUtils.convertToCurrencyString(Number(element.balanceProjOnline.value).toString()) !== this.saldoProjOnline) ||
            (element.bmfLeveragedLimit
              && AppUtils.convertToCurrencyString(Number(element.bmfLeveragedLimit.value).toString()) !== this.alavancagemBMF))
            .forEach(elem => {
              this.limiteOperacional = AppUtils.convertNumberToCurrency(Number(elem.uniqueLimit.value));
              this.limiteOperacionalColor = Number(elem.uniqueLimit.value);
              this.alavancagemVista = AppUtils.convertNumberToCurrency(Number(elem.cashLeveragedLimit.value));
              this.alavancagemVistaColor = Number(elem.cashLeveragedLimit.value);
              this.alavancagemOpcoes = AppUtils.convertNumberToCurrency(Number(elem.optionsLeveragedLimit.value));
              this.alavancagemOpcoesColor = Number(elem.optionsLeveragedLimit.value);
              this.lucroPrejuizoOpen = AppUtils.convertNumberToCurrency(Number(elem.openProfitLoss.value));
              this.lucroPrejuizoOpenColor = Number(elem.openProfitLoss.value);
              this.lucroPrejuizoOpenTool = 'R$' + this.lucroPrejuizoOpen;
              this.lucroPrejuizoClose = AppUtils.convertNumberToCurrency(Number(elem.closedProfitLoss.value));
              this.lucroPrejuizoCloseColor = Number(elem.closedProfitLoss.value);
              this.lucroPrejuizoCloseTool = 'R$' + this.lucroPrejuizoClose;
              this.lucroPrejuizoTotal = AppUtils.convertNumberToCurrency(Number(elem.profitTotalLoss.value));
              this.lucroPrejuizoTotalColor = Number(elem.profitTotalLoss.value);
              this.lucroPrejuizoTotalTool = 'R$' + this.lucroPrejuizoTotal;
              this.saldoProjOnline = AppUtils.convertNumberToCurrency(Number(elem.balanceProjOnline.value));
              this.saldoProjOnlineColor = Number(elem.balanceProjOnline.value);
              this.saldoProjOnlineTool = 'R$' + this.saldoProjOnline;
              this.availableBalance = AppUtils.convertNumberToCurrency(Number(elem.availableBalance.value));
              this.availableBalanceColor = Number(elem.availableBalance.value);
              this.availableBalanceTool = 'R$' + this.availableBalance;
              this.rendaFixa = AppUtils.convertNumberToCurrency(Number(elem.fixedIncome.value));
              this.rendaFixaColor = Number(elem.fixedIncome.value);
              this.fundos = AppUtils.convertNumberToCurrency(Number(elem.funds.value));
              this.fundosColor = Number(elem.funds.value);
              this.clubes = AppUtils.convertNumberToCurrency(Number(elem.clubs.value));
              this.clubesColor = Number(elem.clubs.value);
              this.custodiaOnline = AppUtils.convertNumberToCurrency(Number(elem.totalOnlineBmfPortfolioValue.value)
                + Number(elem.totalOnlineOptionsPortfolioValue.value) + Number(elem.totalOnlineStockPortfolioValue.value));
              this.custodiaOnlineColor = Number(elem.totalOnlineBmfPortfolioValue.value)
                + Number(elem.totalOnlineOptionsPortfolioValue.value) + Number(elem.totalOnlineStockPortfolioValue.value);
              this.tesouroDireto = AppUtils.convertNumberToCurrency(Number(elem.treasuryDirect.value));
              this.tesouroDiretoColor = Number(elem.treasuryDirect.value);
              this.garantia = AppUtils.convertNumberToCurrency(Number(elem.xbspWarrantyTotal.value) + Number(elem.xbmfWarrantyTotal.value));
              this.garantiaColor = Number(elem.xbspWarrantyTotal.value) + Number(elem.xbmfWarrantyTotal.value);
              this.proventos = AppUtils.convertNumberToCurrency(Number(elem.dividends.value));
              this.proventosColor = Number(elem.dividends.value);
              this.alavancagemBMF = AppUtils.convertNumberToCurrency(Number(elem.bmfLeveragedLimit.value));
              this.alavancagemBMFColor = Number(elem.bmfLeveragedLimit.value);
              this.patrimonioInicial = AppUtils.convertNumberToCurrency(Number(elem.initialEquity.value));
              this.patrimonioInicialColor = Number(elem.initialEquity.value);
              this.patrimonioOnline = AppUtils.convertNumberToCurrency(Number(elem.totalEquity.value));
              this.patrimonioOnlineColor = Number(elem.totalEquity.value);
              this.ajustePosicao = AppUtils.convertNumberToCurrency(Number(elem.bmfPositionAdjust.value));
              this.ajustePosicaoColor = Number(elem.bmfPositionAdjust.value);
              this.ajustePregao = AppUtils.convertNumberToCurrency(Number(elem.bmfTradingAdjust.value));
              this.ajustePregaoColor = Number(elem.bmfTradingAdjust.value);
              this.D0 = AppUtils.convertNumberToCurrency(Number(elem.balanceD0.value));
              this.D0Color = Number(elem.balanceD0.value);
              this.D0Tool = 'R$' + this.D0;
              this.D1 = AppUtils.convertNumberToCurrency(Number(elem.balanceD1.value));
              this.D1Color = Number(elem.balanceD1.value);
              this.D1Tool = 'R$' + this.D1;
              this.D2 = AppUtils.convertNumberToCurrency(Number(elem.balanceD2.value));
              this.D2Color = Number(elem.balanceD2.value);
              this.D2Tool = 'R$' + this.D2;
              this.garantiaLivreColor = Number(elem.xbmfWarrantyTotal.value);
              this.garantiaLivre = AppUtils.convertNumberToCurrency(Number(elem.xbmfWarrantyTotal.value));
              this.financialList.push(elem);
            });
          this.loading = false;
          this.counterIdFilter++;
        }
      });
  }


  close() {
    this.dialogRef.close();
    const elem = document.getElementById('sidebar-left');
    elem.style.zIndex = '1';
  }

  togglePassword() {
    this.showpassword = !this.showpassword;
    this.icon = this.showpassword ? 'visibility_off' : 'visibility';
  }

  // public togglePassword() {
  //   this.showpassword = !this.showpassword;
  //   this.icon = this.showpassword ? 'visibility' : 'visibility_off';
  // }
}
