import { RouterTestingModule } from '@angular/router/testing';
import { MatIconModule, MatProgressSpinnerModule, MatCardModule, MatDialogRef, MatDialogModule } from '@angular/material';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { BalanceConsultComponent } from '@business/balance-consult/balance-consult.component';
import { SpinnerComponent } from '@component/spinner/spinner.component';
import { ConfigService } from '@services/webFeeder/config.service';
import { ConfigServiceMock, CustomSnackbarServiceMock } from '@services/_test-mock-services/_mock-services';
import { NotificationsService, SimpleNotificationsModule } from 'angular2-notifications';
import { CustomSnackbarService } from '@services/custom-snackbar-service.service';
import { HttpModule } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';
import { DatePipe } from '@angular/common';

describe('BalanceConsultComponent', () => {
  let component: BalanceConsultComponent;
  let fixture: ComponentFixture<BalanceConsultComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ 
        BalanceConsultComponent,
        SpinnerComponent
       ],
      imports:[
        MatIconModule,
        MatProgressSpinnerModule,
        MatCardModule,
        RouterTestingModule,
        SimpleNotificationsModule.forRoot(),
        MatDialogModule,
        HttpModule,
        HttpClientModule
      ],
      providers:[
        DatePipe,
        NotificationsService,
        { provide: MatDialogRef, useValue: {} },
        { provide: ConfigService, useClass: ConfigServiceMock },
        { provide: CustomSnackbarService, useClass: CustomSnackbarServiceMock },

      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BalanceConsultComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
