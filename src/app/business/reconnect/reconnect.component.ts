import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA, MatDialog } from '@angular/material';
import { Constants } from '@utils/constants';
import { TokenService } from '@services/webFeeder/token.service';

@Component({
  selector: 'app-reconnect',
  templateUrl: './reconnect.component.html',
})
export class ReconnectComponent implements OnInit {

  public animation: any;
  public lottieConfig: any;
  public lottieConfigSuccess: any;
  public isSuccess: boolean;
  offlineText = 'Ops! Algo deu errado, os serviços de cotação e negociação pararam.';
  bothDown = false;

  constructor(
    public tokenService: TokenService,
    public dialogRef: MatDialogRef<ReconnectComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private modalService: MatDialog
  ) {
    this.lottieConfig = {
      path: Constants.RECONNECT_ANIMATION_URL,
      renderer: 'canvas',
      autoplay: true,
      loop: true
    };
    this.lottieConfigSuccess = {
      path: Constants.RECONNECTED_ANIMATION_URL,
      renderer: 'canvas',
      autoplay: true,
      loop: true
    };
    this.isSuccess = false;
  }

  ngOnInit() {
    if (this.data && this.data !== undefined) {
      if (this.data === 'allDown') {
        this.offlineText = 'Ops! Algo deu errado, os serviços de <b>cotação</b> e <b>negociação</b> pararam.';
        this.bothDown = true;
      } else if (this.data === 'omsDown') {
        this.offlineText = 'Ops! Algo deu errado, o serviço de <b>negociação</b> parou. ';
      } else if (this.data === 'difusionDown') {
        this.offlineText = 'Ops! Algo deu errado, o serviço de <b>cotação</b> parou.';
      }
    }
  }

  change(value) {
    if (value && value !== undefined) {
      if (value === 'allDown') {
        setTimeout(() => {
          this.offlineText = 'Ops! Algo deu errado, os serviços de <b>cotação</b> e <b>negociação</b> pararam.';
          this.bothDown = true;
        }, 0);
      } else if (value === 'omsDown') {
        setTimeout(() => {
          this.offlineText = 'Ops! Algo deu errado, o serviço de <b>negociação</b> parou. ';
          this.bothDown = false;
        }, 0);
      } else if (value === 'difusionDown') {
        setTimeout(() => {
          this.offlineText = 'Ops! Algo deu errado, o serviço de <b>cotação</b> parou.';
          this.bothDown = false;
        }, 0);
      }
    }
  }

  handleAnimation(anim: any) {
    this.animation = anim;
  }

  closeModal(isSuccess: boolean) {
    this.isSuccess = isSuccess;
    if (isSuccess) {
      setTimeout(() => {
        this.dialogRef.close();
        // this.modalService.closeAll();
      }, 3000);
    } else {
      this.dialogRef.close();
      // this.modalService.closeAll();
    }
  }
}


