import { AuthServer } from '@services/auth-server.service';
import { ConfigService } from '@services/webFeeder/config.service';
import { Component, OnInit, ViewChild, ViewContainerRef } from '@angular/core';
import { MatDialogRef } from '@angular/material';
import { CustomSnackbarService } from '@services/custom-snackbar-service.service';
import { NotificationsService } from 'angular2-notifications';
import { Constants } from '@utils/constants';
import { AuthenticationService } from '@services/authentication.service';
import { AppUtils } from '@utils/app.utils';

@Component({
  selector: 'app-update-password',
  templateUrl: './update-password.component.html'
})
export class UpdatePasswordComponent implements OnInit {
  @ViewChild('updatePassword', { read: ViewContainerRef }) parent: ViewContainerRef;

  icon = 'visibility_off';
  iconNewPassword = 'visibility_off';
  iconConfirmPassword = 'visibility_off';
  showpassword = false;
  showNewPassword = false;
  showConfirmPassword = false;
  model: any = {};
  cotationUser = 'mirae10013';
  changeSuccess = false;
  confirmPasswordValid = true;
  errorMsg01: string = Constants.MSG03_FORGOT_ERROR;
  validPass = true;
  loading: boolean;
  showConfirm: boolean;

  passwordComplexyLength: boolean;
  passwordComplexySpecial: boolean;
  passwordComplexyNumber: boolean;
  passwordComplexyLetterUp: boolean;
  passwordComplexyLetterDown: boolean;
  passwordWeak: boolean;
  passwordMedium: boolean;
  passwordGood: boolean;
  passwordStrong: boolean;
  isPasswordStrong: boolean;

  constructor(
    public dialogRef: MatDialogRef<UpdatePasswordComponent>,
    private _snackBar: CustomSnackbarService,
    private _configService: ConfigService,
    private _service: NotificationsService,
    private _authenticationService: AuthenticationService,
    public _NotificationService: NotificationsService,
    private _authServer: AuthServer
  ) {
    this.model.password = '';
    this.model.newPassword = '';
    this.model.confirmPassword = '';
    this.model.login = this._configService.getHbAccount();
  }

  closeClick(): void {
    this.dialogRef.close();
  }

  ngOnInit() {
    this.changeSuccess = false;
    this.confirmPasswordValid = true;
  }

  public togglePassword() {
    this.showpassword = !this.showpassword;
    this.icon = this.showpassword ? 'visibility' : 'visibility_off';
  }

  public toggleNewPassword() {
    this.showNewPassword = !this.showNewPassword;
    this.iconNewPassword = this.showNewPassword ? 'visibility' : 'visibility_off';
  }

  public toggleConfirmPassword() {
    this.showConfirmPassword = !this.showConfirmPassword;
    this.iconConfirmPassword = this.showConfirmPassword ? 'visibility' : 'visibility_off';
  }

  public onUpdatePassword() {
    if (!this.Validate()) {
      return;
    }
    this._authServer.iDifusion.updatePassowrd(this.cotationUser, this.model.password, this.model.newPassword)
      .subscribe(response => {
        this.changeSuccess = false;
        if (response) {
          this.changeSuccess = true;
        } else {
          this.alertWarn('Senha atual incorreta');
        }
      });
  }

  focusOutConfirmPassword() {
    this.confirmPasswordValid = true;
    if (this.model && this.model.newPassword && this.model.confirmPassword) {
      if (this.model.newPassword !== this.model.confirmPassword) {
        this.confirmPasswordValid = false;
      }
    }
  }

  private Validate() {
    if (this.model.newPassword !== this.model.confirmPassword) {
      this.alertWarn('Nova senha está diferente da confirmação da senha. Tente novamente!');
      return false;
    }
    return true;
  }

  alertWarn(text) {
    this._service.warn('Alerta', text);
  }

  validatorPassword(value) {
    if (this.model.newPassword !== this.model.confirmPassword) {
      this.validPass = false;
    } else {
      this.validPass = true;
    }
  }

  validatorPassword2(value) {
    if (this.model.confirmPassword) {
      if (this.model.newPassword !== this.model.confirmPassword) {
        this.validPass = false;
      } else {
        this.validPass = true;
      }
    }
    if (value) {
      this.complexyPassoword(value);
    } else {
      this.passwordWeak = false;
      this.passwordMedium = false;
      this.passwordGood = false;
      this.passwordStrong = false;
      this.passwordComplexyLength = false;
      this.passwordComplexyNumber = false;
      this.passwordComplexySpecial = false;
      this.passwordComplexyLetterUp = false;
      this.passwordComplexyLetterDown = false;
    }
  }

  difusionChangePassword() {
    // TODO: valida se a senha é forte
    // const validPassword = this.passwordValidation(this.model.confirmPassword);
    // if (!validPassword) {
    //   this._NotificationService.warn('', Constants.MSG_LOGIN_MSG003);
    //   this.loading = true;
    //   return false;
    // }
    if (!this._configService.isUserAssessorLogged()) {
      this._authenticationService.difusionChangePassword(this.model.login, this.model.password, this.model.confirmPassword)
        .subscribe(res => {
          if (res && (res.success === 'true' || res.status === true || Number(res.userStatus) === 5)) {
            this.showConfirm = true;
            this.loading = false;
            this.model = {};
          } else {
            this.loading = false;
            if (res && res.message && res.messageDetails) {
              this._NotificationService.error(res.message, res.messageDetails);
            } else if (res && res.message) {
              this._NotificationService.error('', res.message);
            } else if (res && res.userStatusText) {
              this._NotificationService.error('', res.userStatusText);
            } else if (res && (!res.success || res.success === 'false')) {
              this._NotificationService.error('', Constants.MSG_LOGIN_MSG008);
            } else {
              this._NotificationService.error('', Constants.MSG_LOGIN_MSG003);
            }
            this.loading = false;
          }
        }, error => {
          this._NotificationService.error('', Constants.MSG_LOGIN_MSG003);
          this.loading = false;
        });
    } else {
      this._authenticationService.advisorInfo(this.model.login)
        .subscribe(res => {
          if (res) {
            this._authenticationService.advisorChangePassword(this.model.password, this.model.confirmPassword, null, res.id)
              .subscribe(res2 => {
                if (res2 && res2.status === 422 && res2.message && res2.message.Detail) {
                  this._NotificationService.error('', res2.message.Detail);
                } else {
                  this.showConfirm = true;
                  this.loading = false;
                  this.model = {};
                }
                this.loading = false;
              }, error => {
                this._NotificationService.error('', Constants.MSG_LOGIN_MSG003);
                this.loading = false;
              });
          }
        }, error => {
          this._NotificationService.error('', Constants.MSG_LOGIN_MSG003);
          this.loading = false;
        });
    }
  }

  validChar(e) {
    if (AppUtils.validCharSystem(e)) {
      this._NotificationService.warn('', 'Caracter (' + e.key + ') inválido.');
      return false;
    }
  }

  passwordValidation(value) {
    const character2 = new RegExp('^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[#$^+=!*()@%&]).{8,10}$');
    return character2.test(this.model.newPassword);
  }

  complexyPassoword(value) {
    if (value && value !== '') {
      const passwordComplexyLengthTemp = new RegExp('^(?=.{8,})').test(value);
      const passwordComplexySpecialTemp = new RegExp('^(?=.*[#$^+=!*()@%&])').test(value);
      const passwordComplexyNumberTemp = new RegExp('^(?=.*[0-9])').test(value);
      const passwordComplexyLetterUpTemp = new RegExp('^(?=.*[A-Z])').test(value);
      const passwordComplexyLetterDownTemp = new RegExp('^(?=.*[a-z])').test(value);
      if (passwordComplexyLengthTemp) {
        this.passwordComplexyLength = true;
      } else {
        this.passwordComplexyLength = false;
      }
      if (passwordComplexySpecialTemp) {
        this.passwordComplexySpecial = true;
      } else {
        this.passwordComplexySpecial = false;
      }
      if (passwordComplexyNumberTemp) {
        this.passwordComplexyNumber = true;
      } else {
        this.passwordComplexyNumber = false;
      }
      if (passwordComplexyLetterUpTemp) {
        this.passwordComplexyLetterUp = true;
      } else {
        this.passwordComplexyLetterUp = false;
      }
      if (passwordComplexyLetterDownTemp) {
        this.passwordComplexyLetterDown = true;
      } else {
        this.passwordComplexyLetterDown = false;
      }

      if (value.length >= 8 && passwordComplexyLengthTemp && passwordComplexyNumberTemp && passwordComplexyLetterUpTemp
        && passwordComplexyLetterDownTemp && passwordComplexySpecialTemp) {
        this.passwordWeak = true;
        this.passwordMedium = true;
        this.passwordGood = true;
        this.passwordStrong = true;
        // this.isPasswordStrong = true;
      } else if (value.length >= 8 && passwordComplexyLengthTemp && passwordComplexyNumberTemp
        && (passwordComplexyLetterUpTemp || passwordComplexySpecialTemp)) {
        this.passwordWeak = true;
        this.passwordMedium = true;
        this.passwordGood = true;
        this.passwordStrong = false;
      } else if (value.length >= 8 && passwordComplexyLengthTemp && passwordComplexyNumberTemp
        && (passwordComplexyLetterDownTemp || passwordComplexyLetterUpTemp)) {
        this.passwordWeak = true;
        this.passwordMedium = true;
        this.passwordGood = false;
        this.passwordStrong = false;
      } else {
        this.passwordWeak = true;
        this.passwordMedium = false;
        this.passwordGood = false;
        this.passwordStrong = false;
      }
    }
  }

}

