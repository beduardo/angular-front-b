import { Component, Inject, OnDestroy, OnInit, ViewContainerRef, HostListener, ViewChild } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { AuthenticationService } from '@services/authentication.service';
import { CustomSnackbarService } from '@services/custom-snackbar-service.service';
import { SidebarService } from '@services/sidebar.service';
import { ConfigService } from '@services/webFeeder/config.service';
import { Constants } from '@utils/constants';
import { NotificationsService } from 'angular2-notifications';
import { Subscription } from 'rxjs';
import { AutoLogoutService } from '@auth/auto-logout-service.service';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-auth-broker',
  templateUrl: './auth-broker.component.html'
})
export class AuthBrokerComponent implements OnInit, OnDestroy {

  public brokers: any[];
  public model: any = {};
  public loading: boolean;
  public icon = 'visibility_off';
  public showpassword = false;
  public tabClickCallBack: number;
  public clickOpenTabOrder: Subscription;
  public isUserLogged = false;
  @ViewChild('error', { read: ViewContainerRef }) errorDiv: ViewContainerRef;
  @ViewChild('brokerForm') brokerForm: NgForm;

  @HostListener('mousemove')
  onMouseMove() {
    this._autoLogoutService.resetTime();
  }

  @HostListener('click')
  onClick() {
    this._autoLogoutService.resetTime();
  }

  @HostListener('paste', ['$event']) blockPaste(e: KeyboardEvent) {
    e.preventDefault();
  }

  constructor(
    private authenticationService: AuthenticationService,
    private configService: ConfigService,
    public dialogRef: MatDialogRef<AuthBrokerComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    public snackBar: CustomSnackbarService,
    // @Inject(ViewContainerRef) public msgPlaceholder: ViewContainerRef,
    private sidebarService: SidebarService,
    private _autoLogoutService: AutoLogoutService,
    private _notificationService: NotificationsService,
  ) {
    // this.brokers = ['Cedro', 'Easynvest', 'Guide', 'XP Investimentos']; // TODO Desmock please!
    this.brokers = [
      { icon: 'logo-cedrotech', company: 'Cedro Technologies' }
    ];

  }

  ngOnInit() {
    this.clickOpenTabOrder = this.sidebarService.clickOpenTabAuthCallBack$
      .subscribe(x => {
        if (x) {
          this.tabClickCallBack = x;
        }
      });
  }

  ngOnDestroy() {
    this.tabClickCallBack = 0;
  }

  closeClick(): void {
    this.dialogRef.close();
  }

  brokerServiceLogin() {
    this.loading = true;
    if (!(this.model.username || this.model.password || this.model.broker)) {
      this._notificationService.warn('', Constants.MSG_LOGIN_MSG001);
      this.loading = false;
    }

    try {
      if (this.model.broker) {
        this.authenticationService.brokerServiceLogin(this.model.username, this.model.password, this.model.broker)
          .subscribe((data: any) => {
            if (data && data.isAuthenticated === 'N') {
              this._notificationService.error('', Constants.MSG_LOGIN_MSG002);
              // this.brokerForm.controls['password'].setErrors({ 'incorrect': true });
              // this.brokerForm.controls['username'].setErrors({ 'incorrect': true });
              // this.brokerForm.controls['searchText'].setErrors({ 'incorrect': true });
              this.loading = false;
            } else if (data.isAuthenticated === 'Y') {
              const typeLoginHB = Constants.COOKIE_HB_TYPE_CEDRO;
              if (data.authenticationReqId.split(',').length > 1) {
                data.authenticationReqId = data.authenticationReqId.split(',')[0];
              }
              this.configService.setTokenNegotiation(data.tokenNegotiation);
              this.configService.setBrokerAccount(data.authenticationReqId, typeLoginHB);
              this.configService._authHbLoginType.next(Number(typeLoginHB));
              this._notificationService.success('', 'Usuário autenticado com sucesso da corretora.');
              this.closeClick();
              this.sidebarService._clickOpenTabAuth.next(this.tabClickCallBack);
              this.configService._authHbLogged.next(true);
              this.configService.setHbLoginTypeEnable(Constants.LOGIN_S);
              this.tabClickCallBack = 0;
            } else {
              this.snackBar.createSnack(this.errorDiv, Constants.BROKER_MSG002, Constants.ALERT_TYPE.error);
              this.brokerForm.controls['password'].setErrors({ 'incorrect': true });
              this.brokerForm.controls['username'].setErrors({ 'incorrect': true });
              this.brokerForm.controls['broker'].setErrors({ 'incorrect': true });
              this.configService._authHbLogged.next(false);
            }
            this.loading = false;
          },
            error => {
              this.loading = false;
              this.snackBar.createSnack(this.errorDiv, Constants.BROKER_MSG002, Constants.ALERT_TYPE.error);
              if (error) {
                this.brokerForm.controls['password'].setErrors({ 'incorrect': true });
                this.brokerForm.controls['username'].setErrors({ 'incorrect': true });
                this.brokerForm.controls['broker'].setErrors({ 'incorrect': true });
                this.configService._authHbLogged.next(false);
              }
            });
      } else {
        this.loading = false;
        return false;
      }
    } catch (e) {
      this.loading = false;
    }
  }

  public togglePassword() {
    this.showpassword = !this.showpassword;
    this.icon = this.showpassword ? 'visibility' : 'visibility_off';
  }

  public onKeyPressLetter(e) {
    return false;
  }

}
