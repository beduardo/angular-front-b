import { Component, HostListener, OnDestroy, OnInit, ElementRef, ViewChild } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { AuthService } from '@auth/auth.service';
import { WorkspaceNewDto } from '@dto/workspacenew.dto';
import { SidebarService } from '@services/sidebar.service';
import { WorkspaceNewService } from '@services/workspace-new.service';
import { Constants } from '@utils/constants';
import { DisplayGrid, GridsterConfig, GridsterItem, GridType, GridsterComponent } from 'angular-gridster2';
import { Subscription } from 'rxjs';
import { QuoteService } from '@services/webFeeder/sckt-quotes.service';

@Component({
  selector: 'app-workspace-new',
  templateUrl: './workspace-new.component.html',
})
export class WorkspaceNewComponent implements OnInit, OnDestroy {
  public subscription: Subscription;
  public boxes: Array<Box> = [];
  private userWorkspacesCurrent;
  private wksSelectedId: number;
  private wksSelectedDto: WorkspaceNewDto;
  public sidebarStatus: boolean;
  private workService: WorkspaceNewService;
  loading: boolean;

  @ViewChild('gridster') gridsterDiv: GridsterComponent;

  // begin gridster functions
  public options: GridsterConfig;
  static destroyItemHome(item) { }
  static initCallback(item) { }
  static destroyCallback(item, itemComponent) { }
  public changedOptions() {
    this.options.api.optionsChanged();
  }

  // removeItem($event, item) {
  //   $event.preventDefault();
  //   $event.stopPropagation();
  //   this.options.splice(this.options.indexOf(item), 1);
  // }

  // end gridster functions
  constructor(
    private workspaceService: WorkspaceNewService,
    private acroute: ActivatedRoute,
    private authService: AuthService,
    private sidebarService: SidebarService,
    private router: Router,
  ) {
    this.userWorkspacesCurrent = [];
    this.wksSelectedId = 0;
    this.sidebarStatus = true;
    this.workService = workspaceService;
    this.loading = true;
  }

  ngOnInit() {
    this.workspaceService.workspaceLock$.subscribe(data => {
      if (data !== undefined && this.options) {
        this.options.draggable = { enabled: data };
        this.changedOptions();
      }
    });

    this.workspaceService.callScrollDown$.subscribe(x => {
      if (x === true) {
        this.scrollDown();
      }
    });

    // Gridster component configuration
    this.options = {
      itemChangeCallback: this.itemChange.bind(this),
      initCallback: WorkspaceNewComponent.initCallback,
      destroyCallback: WorkspaceNewComponent.destroyItemHome,
      setGridSize: Constants.GRID_SET_SIZE,
      pushItems: Constants.GRID_PUSH_ITEMS,
      draggable: {
        enabled: Constants.GRID_DRAGGABLE,
        ignoreContentClass: 'gridster-item-content',
        ignoreContent: false,
        dragHandleClass: 'drag-handler',
      },
      compactType: 'compactUp&Left',
      outerMargin: Constants.GRID_OUTER_MARGIN,
      outerMarginTop: Constants.GRID_OUTER_MARGIN_SIZE,
      outerMarginBottom: Constants.GRID_OUTER_MARGIN_SIZE,
      outerMarginLeft: Constants.GRID_OUTER_MARGIN_SIZE,
      outerMarginRight: Constants.GRID_OUTER_MARGIN_SIZE,
      maxCols: Constants.GRID_MAX_COLS,
      minCols: Constants.GRID_MIN_COLS,
      maxRows: Constants.GRID_MAX_ROWS,
      minRows: Constants.GRID_MIN_ROWS,
      fixedColWidth: Constants.GRID_COL_WIDTH,
      fixedRowHeight: Constants.GRID_ROW_HEIGHT,
      swap: Constants.GRID_SWAP,
      gridType: GridType.VerticalFixed,
      disableWindowResize: Constants.GRID_WINDOW_RESIZE,
      displayGrid: DisplayGrid.None,
      disablePushOnDrag: Constants.GRID_PUSH_ON_DRAG,
      resizable: {
        enabled: Constants.GRID_RESIZABLE,
      },
      maxItemArea: Constants.GRID_MAX_ITEM_AREA,
      margin: Constants.GRID_MARGIN,
    };

    // Other way to get url parameters
    this.subscription = this.acroute.queryParams
      .subscribe((param: Params) => {
        this.loading = true;
        // clean workspace variables to recharge it
        this.clearUserWorkspaces();
        this.clearBoxes();
        this.wksSelectedId = param['workspaceId'];
        // flow to remove component from Workspace
        if (this.wksSelectedId && param['deleteCmp'] === 'true' || param['addCmp'] === 'true' ||
          param['workspaceUpdated'] && param['workspaceUpdated'] != '') {
          this.workspaceService.updateWorkspace(this.wksSelectedId, param['workspaceUpdated']).subscribe(respUpdate => {
            if (respUpdate) {
              this.mainFlow(undefined);
              this.loading = false;
            }
          });
        } else {
          this.mainFlow(param['delete']);
          this.loading = false;
        }
      }
      );

    this.sidebarService.status$.subscribe(elem => {
      this.sidebarStatus = elem;
      this.setGridsterColumns(window.innerWidth);
    });

    this.setGridsterColumns(window.innerWidth);
    this.setResizeComponentObservable();
  }

  itemChange(item, itemComponent) {
    const boxUpdated: Box = {
      workspaceId: item.workspaceId,
      id: item.id,
      symbol: item.symbol,
      company: item.company,
      type: item.type,
      showInGridster: item.showInGridster,
      x: itemComponent.$item.x,
      y: itemComponent.$item.y,
      rows: itemComponent.$item.rows,
      cols: itemComponent.$item.cols,
      dragEnabled: item.dragEnabled,
      resizeEnabled: item.resizeEnabled,
      compactEnabled: item.compactEnabled,
      maxItemRows: item.maxItemRows,
      minItemRows: item.minItemRows,
      maxItemCols: item.maxItemCols,
      minItemCols: item.minItemCols,
      minItemArea: item.minItemArea,
      maxItemArea: item.maxItemArea,
      worksheetTitle: item.worksheetTitle
    };
    this.workService.workspaceSaveOnDemand(boxUpdated);
  }

  private mainFlow(paramDelete) {
    this.workspaceService.loadWorkspaces(this.authService.getUserLogged(), false)
      .subscribe(result => {
        if (result) {
          let wksExist = false;
          for (let y = 0; y < result.length; y++) {
            if (this.wksSelectedId && result[y].Id.toString() === this.wksSelectedId.toString()) {
              wksExist = true;
              break;
            }
          }
          if (!wksExist && result.length > 0) {
            this.wksSelectedId = result[0].Id;
          }
          for (let z = 0; z < result.length; z++) {
            if (this.wksSelectedId && result[z].Id.toString() === this.wksSelectedId.toString()) {
              for (let i = 0; i < result.length; i++) {
                const wksObj = this.mountAndPushWks(this.workspaceService.getUserWorkspace(i).Id,
                  this.workspaceService.getUserWorkspace(i).UserName,
                  this.workspaceService.getUserWorkspace(i).Name,
                  this.workspaceService.getUserWorkspace(i).Title,
                  this.workspaceService.getUserWorkspace(i).Active,
                  this.workspaceService.getUserWorkspace(i).IndexTab,
                  this.workspaceService.getUserWorkspace(i).IsPrivate,
                  this.workspaceService.getUserWorkspace(i).WorksapaceType,
                  this.workspaceService.getUserWorkspace(i).JsonData);
                // Please, don't put restrict operator here or any place at this source without to contact with team leader!
                if (wksObj.Id == this.wksSelectedId) {
                  this.wksSelectedDto = wksObj;
                }
              }
              // flow to present the new workspace in the view
              if (result[z] && !this.checkUserWorkspaceById(this.wksSelectedId)) {
                const wksObj = this.mountAndPushWks(
                  result[z].Id,
                  result[z].UserName,
                  result[z].Name,
                  result[z].Title,
                  result[z].Active,
                  result[z].IndexTab,
                  result[z].IsPrivate,
                  result[z].WorkspaceType,
                  result[z].JsonData
                );
                this.wksSelectedDto = wksObj;
              }
              // flow to remove the workspace
              if (result[z] && paramDelete === 'true') {
                this.workspaceService.deleteWorkspace(this.wksSelectedId).subscribe(msg => {
                  if (msg !== null) {
                    this.popWksAndUpdWksSelectedId(this.wksSelectedId);
                    if (this.userWorkspacesCurrent.length < Constants.NUM_MAX_WORKSPACES) {
                      this.workspaceService.updateShowAddWorkspace(true);
                    }
                    this.persistUpdate(result[z], paramDelete);
                  }
                });
              } else {
                this.persistUpdate(result[z], paramDelete);
              }
            }
          }
        }
      });
  }

  // private existsUserWorkspace(): Promise<boolean> {
  //   return this.workService.loadWorkspaces(this.authService.getUserLogged(), false)
  //     .toPromise()
  //     .then(dados => {
  //       if (dados) {
  //         return dados.length > 0;
  //       } else {
  //         return false;
  //       }
  //     },
  //       error => {
  //         return false;
  //       }
  //     );
  // }

  // private mainFlow(paramDelete) {
  //   this.workspaceService.loadWorkspaces(this.authService.getUserLogged(), false).subscribe(result => {
  //     if (result) {
  //       this.workspaceService.getWorkspaceById(this.wksSelectedId)
  //         .subscribe(resp => {
  //           for (let i = 0; i < result.length; i++) {
  //             const wksObj = this.mountAndPushWks(this.workspaceService.getUserWorkspace(i).Id,
  //               this.workspaceService.getUserWorkspace(i).UserName,
  //               this.workspaceService.getUserWorkspace(i).Name,
  //               this.workspaceService.getUserWorkspace(i).Title,
  //               this.workspaceService.getUserWorkspace(i).Active,
  //               this.workspaceService.getUserWorkspace(i).IndexTab,
  //               this.workspaceService.getUserWorkspace(i).IsPrivate,
  //               this.workspaceService.getUserWorkspace(i).WorksapaceType,
  //               this.workspaceService.getUserWorkspace(i).JsonData);
  //             // Please, don't put restrict operator here or any place at this source without to contact with team leader!
  //             if (wksObj.Id == this.wksSelectedId) {
  //               this.wksSelectedDto = wksObj;
  //             }
  //           }
  //           // flow to present the new workspace in the view
  //           if (resp && !this.checkUserWorkspaceById(this.wksSelectedId)) {
  //             const wksObj = this.mountAndPushWks(resp.Id,
  //               resp.UserName,
  //               resp.Name,
  //               resp.Title,
  //               resp.Active,
  //               resp.IndexTab,
  //               resp.IsPrivate,
  //               resp.WorkspaceType,
  //               resp.JsonData);
  //             this.wksSelectedDto = wksObj;
  //           }
  //           // flow to remove the workspace
  //           if (resp && paramDelete === 'true') {
  //             this.workspaceService.deleteWorkspace(this.wksSelectedId).subscribe(msg => {
  //               if (msg !== null) {
  //                 this.popWksAndUpdWksSelectedId(this.wksSelectedId);
  //                 if (this.userWorkspacesCurrent.length < Constants.NUM_MAX_WORKSPACES) {
  //                   this.workspaceService.updateShowAddWorkspace(true);
  //                 }
  //                 this.persistUpdate(resp, paramDelete);
  //               }
  //             });
  //           } else {
  //             this.persistUpdate(resp, paramDelete);
  //           }
  //         });
  //     }
  //   });
  // }

  private persistUpdate(resp, paramDelete) {
    this.mountBoxes();
    this.workspaceService.updatedUserWorkspaces(this.userWorkspacesCurrent);
    this.workspaceService.updateWorkspaceSelected(this.wksSelectedId);
    // reload workspace of previous position
    // if (resp && paramDelete === 'true') {
    if (resp && this.wksSelectedId) {
      this.router.navigate([Constants.HOME_WKS_PATH], {
        queryParams: {
          workspaceId: this.wksSelectedId
        }
      });
    }
  }

  public scrollDown() {
    setTimeout(() => {
      const gridsterElem = document.getElementById('gridster');
      gridsterElem.scrollTo(0, 2000);
    }, 1000);
  }

  private mountBoxes() {
    if (this.wksSelectedDto && this.wksSelectedDto.JsonData) {
      let result: any = JSON.parse(this.wksSelectedDto.JsonData);
      if (!result.content) {
        result = JSON.parse(result.JsonData);
      }
      if ((result.content && result.content.length > 0) || result.length > 0) {
        for (let j = 0; j < result.content.length; j++) {
          if (result.content[j].showInGridster && result.content[j].showInGridster === true) {
            this.boxes.push({
              workspaceId: this.wksSelectedDto.Id, // TODO wks save
              symbol: result.content[j].symbol,
              company: result.content[j].company,
              type: result.content[j].type,
              id: result.content[j].id,
              data: result.content[j],
              x: result.content[j].position.x,
              y: result.content[j].position.y,
              rows: result.content[j].position.rows,
              cols: (result.content[j].position.cols <= this.options.maxCols ? result.content[j].position.cols : this.options.maxCols),
              minItemRows: result.content[j].position.minItemRows,
              minItemCols: result.content[j].position.minItemCols,
              showInGridster: result.content[j].showInGridster,
              resizeEnabled: result.content[j].position.resizeEnabled,
              worksheetTitle: result.content[j].worksheetTitle,
              columns: result.content[j].columns
            });
          }
        }
      }
    }
  }

  private checkUserWorkspaceById(workspaceId: number): boolean {
    for (let j = 0; j < this.userWorkspacesCurrent.length; j++) {
      if (this.userWorkspacesCurrent[j].Id == workspaceId) {
        return true;
      }
    }
    return false;
  }

  private clearUserWorkspaces() {
    this.userWorkspacesCurrent = [];
  }

  private clearBoxes() {
    this.boxes = [];
  }

  private popWksAndUpdWksSelectedId(Id: number) {
    for (let j = 0; j < this.userWorkspacesCurrent.length; j++) {
      if (this.userWorkspacesCurrent[j].Id == Id) {
        this.userWorkspacesCurrent.splice(j, 1);
        if (j > 0) {
          this.wksSelectedId = this.userWorkspacesCurrent[j - 1].Id;
        } else if (j === 0) {
          this.wksSelectedId = this.userWorkspacesCurrent[j].Id;
        } else {
          this.wksSelectedId = 0; // not exists workspaces
        }
      }
    }
  }

  private mountAndPushWks(Id, UserName, Name, Title, Active, IndexTab, IsPrivate, WorkspaceType, JsonData, IsPredefined?): WorkspaceNewDto {
    const wksObj = new WorkspaceNewDto(Id,
      UserName,
      Name || Title,
      Active,
      IndexTab,
      IsPrivate,
      WorkspaceType,
      JsonData,
      IsPredefined);
    this.userWorkspacesCurrent.push(wksObj);
    return wksObj;
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  @HostListener('window:resize', ['$event'])
  onResize(event) {
    this.setGridsterColumns(event.target.innerWidth);
  }

  setGridsterColumns(windowWidth: Number) {
    const colsBeforeChange = this.options.minCols;

    if (windowWidth < Constants.SM_WINDOW_RES) {
      if (this.sidebarStatus) {
        this.options.minCols = 2;
        this.options.maxCols = 2;
      } else {
        this.options.minCols = 3;
        this.options.maxCols = 3;
      }
    } else if (windowWidth < Constants.MD_WINDOW_RES) {
      this.options.minCols = 3;
      this.options.maxCols = 3;
    } else if (windowWidth < Constants.LG_WINDOW_RES) {
      this.options.minCols = 4;
      this.options.maxCols = 4;
    } else {
      this.options.minCols = 5;
      this.options.maxCols = 5;
    }

    if (this.options.api && this.options.api.optionsChanged) {
      this.options.api.optionsChanged();

      if (colsBeforeChange < this.options.minCols) {
        let i = 0;
        while (i <= this.options.minCols) {
          this.boxes.forEach(box => {
            if (box.y > 0) {
              this.options.api.getNextPossiblePosition(box);
              this.options.api.optionsChanged();
            }
          });

          i++;
        }
      } else {
        this.boxes.forEach(box => {
          if (box.cols > this.options.minCols) {
            box.cols = this.options.minCols;
            this.options.api.optionsChanged();
          }
          if (box.x + box.cols - 1 >= this.options.minCols) {
            this.options.api.getNextPossiblePosition(box);
            this.options.api.optionsChanged();
          }
        });
      }
    }
  }

  public askResize(box) {
    if (box.type == 'quote-chart') {
      this.workspaceService.gridResized();
    }
  }

  setResizeComponentObservable() {
    this.workspaceService.getResizeComponent().subscribe(
      (data) => {
        if (data) {
          this.boxes.forEach(
            (box) => {
              if (box.id === data.componentId) {
                if (data.rows) {
                  box.rows = data.rows;
                }
                if (data.cols) {
                  box.cols = data.cols;
                }
                this.options.api.optionsChanged();
              }
            }
          );
        }
      }
    );
  }
}

export interface Box extends GridsterItem {
  workspaceId: number;
  id: number;
  symbol: string; // May receive list of asset symbol names splitted by ,
  company: string;
  type: string;
  showInGridster?: boolean;
  worksheetTitle?: string;
  columns?: any;
}
