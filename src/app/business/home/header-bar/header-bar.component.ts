import { TokenService } from '@services/webFeeder/token.service';
import { Component, ElementRef, OnDestroy, OnInit, Renderer2, ViewChild, ViewContainerRef } from '@angular/core';
import { MatBottomSheet, MatDialog, MatDialogConfig, MatDialogRef } from '@angular/material';
import { Router } from '@angular/router';
import { AuthService } from '@auth/auth.service';
import { AutoLogoutService } from '@auth/auto-logout-service.service';
import { AssetSummaryPopUpComponent } from '@business/asset-summary-pop-up/asset-summary-pop-up.component';
import { ReconnectComponent } from '@business/reconnect/reconnect.component';
import { UpdatePasswordComponent } from '@business/update-password/update-password.component';
import { BuySellModalComponent } from '@component/buy-sell-modal/buy-sell-modal.component';
import { CustomWorkspaceModalComponent } from '@component/custom-workspace-modal/custom-workspace-modal.component';
import { UserSettingsPopUpComponent } from '@component/user-settings-pop-up/user-settings-pop-up.component';
import { WorkspaceConfModalComponent } from '@component/workspace-conf-modal/workspace-conf-modal.component';
import { WorkspaceNewSaveDto } from '@dto/workspace-new-save-dto';
import { WorkspaceNewDto } from '@dto/workspacenew.dto';
import { ErrorMessage } from '@model/error-message';
import { StatusConnection } from '@model/status-connection';
import { AuthenticationService } from '@services/authentication.service';
import { CustomSnackbarService } from '@services/custom-snackbar-service.service';
import { FastMarketService } from '@services/fast-market.service';
import { SidebarService } from '@services/sidebar.service';
import { ConfigService } from '@services/webFeeder/config.service';
import { StatusDifusionService } from '@services/webFeeder/sckt-status-difusion.service';
import { StatusOmsService } from '@services/webFeeder/sckt-status-oms.service';
import { WorkspaceNewService } from '@services/workspace-new.service';
import { Constants } from '@utils/constants';
import { NotificationsService } from 'angular2-notifications';
import { environment } from 'environments/environment';
import { fromEvent, Observable } from 'rxjs';
import { Subscription } from 'rxjs/Subscription';
import { AppUtils } from './../../../utils/app.utils';
import { FastMarket } from '@services/fast-market/fast-market';
import { YoutubeLiveService } from '@services/youtube-live.service';
import { News } from '@model/webFeeder/news.model';
import { ModalNewsComponent } from '@business/modal-news/modal-news.component';
import { SessionModalComponent } from '@component/session-modal/session-modal.component';
import { YoutubeLiveModalComponent } from '@component/youtube-live/youtube-live-modal/youtube-live-modal.component';
import { NormalModalComponent } from '@component/normal-modal/normal-modal.component';
import { WorkspaceItemContentDto } from '@dto/workspace-item-content-dto';
import { AuthServer } from '@services/auth-server.service';
import { Title } from '@angular/platform-browser';
import { SocketService } from '@services/webFeeder/socket.service';

@Component({
  selector: 'app-header-bar',
  templateUrl: './header-bar.component.html'
})
export class HeaderBarComponent implements OnInit, OnDestroy {

  name: string;
  idInterval: any;
  version = environment.version;
  private subscriptionStatusAlertTimeout: Subscription;
  private subscriptionAuthChange: Subscription;
  public timeSecond = 0;
  public intervalTiemout: any;
  @ViewChild('parent', { read: ViewContainerRef }) parent: ViewContainerRef;
  public userWorkspaces = [];
  public workspaceSelId: number;
  public showAddWorkspace: boolean;
  dialog: MatDialog;
  userName = '';
  headerBarOpen = true;
  doubleClickedTab = 6;
  @ViewChild('quick') quicksearch: ElementRef;
  public lockWorkspace: boolean;
  public screenClicked = false;
  public modalOpened = false;
  public connectionStatus = true;
  public tries = environment.RECONECT_ATTEMPTS;
  public workspaceChangedId: number;

  private reconnectModal: MatDialogRef<ReconnectComponent, any>;
  private subscriptionStatusOms: Subscription;
  private subscriptionStatusOmsChange: Subscription;
  private subscriptionStatusDifusion: Subscription;
  public statusOMS = false;
  public statusClassOMS = Constants.statusClassList[StatusConnection.INITIAL];
  public previousOMS = true;
  public statusDifusion = true;
  public isStatusDifusionRepeat: boolean;
  public isFirstInitStatusModalReconect: boolean;
  public isFirstInitModalReconectStatusDifusion: boolean;
  public isFirstInitStatusModalReconectStatusOMS: boolean;
  public statusClassDifusion = Constants.statusClassList[StatusConnection.INITIAL];
  public statutsNetworkconnected = true;
  public networkErrorMessage = Constants.MODAL_RECONNECT_MSG01;
  public previousDifusion = true;
  public previousOption;
  lastNew: News;
  public lastNewFestMarket: FastMarket = new FastMarket();
  private subscriptionshowMessageToAddWks: Subscription;
  private subscriptionshowClockDownTimer: Subscription;
  liveRunning: boolean;
  liveTitle: string;

  public titleNewsFastMarket = '';
  public currentOms = true;
  public currentDifusion = true;
  public isWso2: boolean;
  public isAssessor: boolean;
  public urlGoPath: string;
  public isLoginPI: boolean;
  public isLoginUm: boolean;
  public componentsListBeforeUpdate: Array<WorkspaceItemContentDto>;
  // public contador = 0;
  // public contador2 = 0;

  public timerSessionInterval: any;
  public clockDownTimer: string;
  public milliSecondsSession = environment.SESSION_TIMEOUT;
  public milliSecondsSessionReset: any;

  public intervalValidateTaskTime: any;
  public intervalLastFastMarketNews: any;
  public internvalFastLive: any;

  public activeWhiteLabel: number;

  constructor(
    private authenticationService: AuthenticationService,
    private router: Router,
    private configService: ConfigService,
    private authService: AuthService,
    private autoLogoutService: AutoLogoutService,
    public snackBar: CustomSnackbarService,
    private workService: WorkspaceNewService,
    dialog: MatDialog,
    private modalBuySell: MatBottomSheet,
    public sidebarService: SidebarService,
    private statusOmsService: StatusOmsService,
    private statusDifusionService: StatusDifusionService,
    public _NotificationService: NotificationsService,
    private renderer: Renderer2,
    private fastMarketService: FastMarketService,
    private youtubeService: YoutubeLiveService,
    private modalService: MatDialog,
    public _authServer: AuthServer,
    private _titleService: Title,
    private socketService: SocketService,
    private tokenService: TokenService
  ) {
    Constants.WHITELABEL.forEach(element => {
      if (element.cod === environment.WHITELABEL) {
        this._titleService.setTitle(element.whitelabel_name);
        this.activeWhiteLabel = element.cod;
      }
    });
    this.clockDownSession();
    this.subscriptionshowClockDownTimer = this.autoLogoutService.resetClockDownTimer$.subscribe(x => {
      if (x) {
        this.milliSecondsSessionReset = x;
      }
    });
    this.dialog = dialog;
    this.reconnectModal = null;
    this.liveRunning = false;
    this.lastNew = new News();
    this.isWso2 = this.configService.isLoginTypeWSO2();
    this.isLoginPI = this.configService.isLoginPi();
    if (environment.WHITELABEL === 5) {
      this.isLoginUm = true;
    }
    if (this.configService.isLoginTypeWSO2() && this.configService.getUserSystemLogged()) {
      this.userName = this.configService.getUserSystemLogged() ? this.configService.getUserSystemLogged().split(' ')[0].toUpperCase() : '';
      this.isAssessor = this.configService.isUserAssessorLogged();
      if (this.isAssessor) {
        this.urlGoPath = '/portal/assessor';
      } else {
        this.urlGoPath = '/portal/client';
      }
    }


  }

  clockDownSession() {
    let minutosTemp = this.milliSecondsSession / 1000; // ssegundos
    this.timerSessionInterval = setInterval(() => {
      if (this.milliSecondsSessionReset) {
        this.milliSecondsSession = environment.SESSION_TIMEOUT;
        minutosTemp = (this.milliSecondsSession / 1000) - 1;
        this.milliSecondsSessionReset = false;
      }
      const horasFormat = Number((minutosTemp / 3600).toString().substring(0, 2));
      const horas = Number((minutosTemp / 3600).toString().split('.')[0]);
      const minutos = Number(((minutosTemp - (horas * 3600)) / 60).toString().split('.')[0]);
      const segundos = minutosTemp - (horas * 3600) - (minutos * 60);
      this.clockDownTimer =
        (horas < 10 ? '0' + horas : horas)
        + ':' + (minutos < 10 ? '0' + minutos : minutos)
        + ':' + (segundos < 10 ? '0' + segundos : segundos);
      if (horas <= 0 && minutos <= 0 && segundos <= 0) {
        clearInterval(this.timerSessionInterval);
        this.configService._authHbLogged.next(false);
        this.dialog.closeAll();
        this.authenticationService.logout();
        this.configService.deleteHbBrokerUser();
        this.configService.deleteFavoriteThemeUser();
        if (this.configService.isLoginTypeWSO2() && this.configService.isUserAssessorLogged()) {
          window.location.href = this.configService.REDIRECT_LOGOUT + '/assessor/login';
        } else if (this.configService.isLoginTypeWSO2() && !this.configService.isUserAssessorLogged()) {
          window.location.href = this.configService.REDIRECT_LOGOUT + '/client/login';
        } else {
          this.router.navigate(['error'], {
            queryParams: new ErrorMessage(Constants.ERROR_EXPIRED_ICON, Constants.ERROR_EXPIRED_TITLE, Constants.ERROR_EXPIRED_TEXT,
              Constants.ERROR_EXPIRED_BUTTON, 'login')
          });
        }
      }
      minutosTemp = minutosTemp - 1;
    }, 1000);
  }

  alertInfo(msg: string) {
    this.snackBar.createSnack(this.parent, msg, Constants.ALERT_TYPE.info);
  }

  clickSidebar(): void {
    this.sidebarService.openClose();
  }

  ngOnInit() {
    this.userWorkspaces = [];
    this.showAddWorkspace = true;
    this.lockWorkspace = true;
    const firstnameTemp: any = this.configService.getHbCurrentUser();
    if (firstnameTemp) {
      this.name = firstnameTemp.firstName;
    } else {
      this.dialog.closeAll();
      this.authenticationService.logout();
      this.configService.deleteHbBrokerUser();
      this.configService.deleteFavoriteThemeUser();
      if (this.configService.isLoginTypeWSO2() && this.configService.isUserAssessorLogged()) {
        window.location.href = this.configService.REDIRECT_LOGOUT + '/assessor/login';
      } else if (this.configService.isLoginTypeWSO2() && !this.configService.isUserAssessorLogged()) {
        window.location.href = this.configService.REDIRECT_LOGOUT + '/client/login';
      } else {
        this.router.navigate(['/login']);
      }
    }
    this.subscriptionStatusAlertTimeout = this.autoLogoutService.statusAlertTimeout$ && this.autoLogoutService.statusAlertTimeout$
      .subscribe(x => {
        this.showModalSession(x);
      });

    if (this.configService.getLoginTypeEnabled() !== Constants.LOGIN_TYPE.LoginW) {
      if (environment.jwtRevokeControl) {
        this.intervalValidateTaskTime = setInterval(() => {
          this.authService.validateTaskTime() && this.authService.validateTaskTime()
            .subscribe(
              resp => {
                if (!resp.valid) { } // TODO if not valid, token will be blacklisted
              },
              error => {
                this.configService.writeLog('Error validating task.');
              });
        }, 20000); // TODO if on line test it's okay, put this value in env. parameter
      }

      this.idInterval = setInterval(() => {
        this.validateUser();
      }, environment.VALID_USER_INTERVAL);
    }

    // TODO: WF
    // this.newsService.getLastNews(1).subscribe(lastNew => {
    //   if (lastNew && lastNew.length > 0 && !this.liveRunning) {
    //     this.lastNew = lastNew[0];
    //   }
    // });

    // TODO: WF
    // setInterval(() => {
    //   this.newsService.getLastNews(1).subscribe(lastNew => {
    //     if (lastNew && lastNew.length > 0 && !this.liveRunning) {
    //       // this.lastNew = lastNew[0];
    //       this.lastNew = lastNew[0];
    //     }
    //   });
    // }, 60000);

    // TODO: Fast MarketS
    this.fastMarketService.getLastFastMarketNews(1, 1) && this.fastMarketService.getLastFastMarketNews(1, 1)
      .subscribe(lastNewFast => {
        if (lastNewFast && lastNewFast[0] && lastNewFast[0].title.rendered) {
          this.lastNewFestMarket = lastNewFast;
        }
      });

    // TODO: Fast Markets
    this.intervalLastFastMarketNews = setInterval(() => {
      this.fastMarketService.getLastFastMarketNews(1, 1) && this.fastMarketService.getLastFastMarketNews(1, 1)
        .subscribe(lastNewFast => {
          if (lastNewFast && lastNewFast[0] && lastNewFast[0].title.rendered) {
            this.lastNewFestMarket = lastNewFast;
          }
        });
    }, 60000);


    this.subscriptionStatusDifusion = this.statusDifusionService.messagesStatusDifusion
      .subscribe(status => {
        if (status != undefined) {
          this.setStatusDifusion(status);
        }
      });


    this.subscriptionStatusOms = this.statusOmsService.messagesStatusOms
      .subscribe(status => {
        if (status != undefined) {
          this.setStatusOms(status);
        }
      });


    fromEvent(window, 'offline').subscribe(() => {
      this.statutsNetworkconnected = false;
      this.statusClassDifusion = Constants.statusClassList[StatusConnection.ERROR];
      this.statusDifusion = false;
      this.statusClassOMS = Constants.statusClassList[StatusConnection.ERROR];
      this.statusOMS = false;
      this.setStatusNetwork(StatusConnection.ERROR, StatusConnection.ERROR);
    });

    fromEvent(window, 'online').subscribe(() => {
      this.statutsNetworkconnected = true;
      this.statusClassDifusion = Constants.statusClassList[StatusConnection.SUCCESS];
      this.statusDifusion = true;
      this.statusClassOMS = Constants.statusClassList[StatusConnection.SUCCESS];
      this.statusOMS = true;
      this.statusOmsService.unsubscribeOMS();
      if (this.configService.isAuthenticadSystem()) {
        this.tokenService.assignServices();
      } else {
        this.configService.deleteBrokerCookies();
        this.configService.deleteHbBrokerUser();
        this.configService.deleteCookie();
        this.configService.deleteFavoriteThemeUser();
        this.configService._authHbLoginType.next(3);
        this.configService.writeLog('HEADER BAR > Usuário desconectado ou tokens invalido.');
        this.configService.redirectExpireSession();
      }
      this.setStatusNetwork(StatusConnection.SUCCESS, StatusConnection.SUCCESS);
    });

    this.subscriptionshowMessageToAddWks = this.workService.showMessageToAddWks$ && this.workService.showMessageToAddWks$.subscribe(data => {
      if (data) {
        // this.snackBar.createSnack(this.parent, Constants.WORKSPACE_MSG001, Constants.ALERT_TYPE.info);
        this._NotificationService.info('', Constants.WORKSPACE_MSG001);
      }
    });

    this.workService.data$.subscribe(data => {
      if (data && data.length > 0) {
        this.userWorkspaces = AppUtils.removeDuplicates(data, 'Id');
      }
    });

    this.workService.workspaceSelected$.subscribe(data => {
      if (data > 0) {
        this.workspaceSelId = data;
      }
    });

    this.workService.showAddWorkspaces$.subscribe(data => {
      this.showAddWorkspace = data;
    });

    this.subscriptionAuthChange = this.autoLogoutService.authChange$ && this.autoLogoutService.authChange$.subscribe(data => {
      if (data === 0) {
        this.screenClicked = true;
      }
    });

    // if (this.configService.getWhiteLabel() === 1 || this.configService.getWhiteLabel() === 4) {
    //   this.youtubeService.getFastLive().subscribe(resp => {
    //     if (resp) {
    //       this.liveVideoActivate(resp);
    //     }
    //   });
    //   this.internvalFastLive = setInterval(() => {
    //     this.youtubeService.getFastLive().subscribe(resp => {
    //       if (resp) {
    //         this.liveVideoActivate(resp);
    //       }
    //     });
    //   }, Constants.SEARCH_VIDEO_INTERVAL);
    //   this.load();
    // }

  }

  // liveVideoActivate(video) {
  //   const arr: Array<any> = Array.from(video.items);
  //   if (arr && arr.length > 0) {
  //     this.liveRunning = true;
  //     this.liveTitle = arr[0].snippet.title;
  //   } else {
  //     this.liveRunning = false;
  //     // FAST MARKETS
  //     this.fastMarketService.getLastFastMarketNews(1, 1)
  //       .subscribe(lastNewFast => {
  //         if (lastNewFast && lastNewFast[0] && lastNewFast[0].title.rendered) {
  //           this.lastNewFestMarket = lastNewFast;
  //         }
  //       });
  //   }
  // }

  ngOnDestroy() {
    this.subscriptionStatusDifusion.unsubscribe();
    this.subscriptionStatusOms.unsubscribe();
    this.statusOmsService.unsubscribeOMS();
    this.statusDifusionService.unsubscribeDifusion();
    if (this.idInterval) {
      clearInterval(this.idInterval);
    }
    if (this.intervalValidateTaskTime) {
      clearInterval(this.intervalValidateTaskTime);
    }
    if (this.timerSessionInterval) {
      clearInterval(this.timerSessionInterval);
    }
    if (this.subscriptionStatusAlertTimeout) {
      this.subscriptionStatusAlertTimeout.unsubscribe();
    }
    if (this.intervalTiemout) {
      clearInterval(this.intervalTiemout);
    }
    if (this.intervalLastFastMarketNews) {
      clearInterval(this.intervalLastFastMarketNews);
    }
    if (this.internvalFastLive) {
      clearInterval(this.internvalFastLive);
    }
    if (this.autoLogoutService.authChange$) {
      this.subscriptionAuthChange.unsubscribe();
    }
    if (this.dialog.getDialogById('loading-modal')) {
      this.dialog.getDialogById('loading-modal').close();
    }
    if (this.subscriptionshowMessageToAddWks) {
      this.subscriptionshowMessageToAddWks.unsubscribe();
    }
  }

  public getRefresh() {
    const userWorkspaces = this.workService.getUserWorkspaces();
    this.router.navigateByUrl('/home/workspace?workspaceId=' + userWorkspaces[0].Id.toString());
  }

  public setStatusOms(value: number) {
    this.statusClassOMS = Constants.statusClassList[value];
    if (value === StatusConnection.SUCCESS || value === StatusConnection.SUCCESS_RECONNECT) {
      this.statusOMS = true;
    } else {
      this.statusOMS = false;
    }
    this.setStatusNetwork(null, value);
  }

  public setStatusDifusion(value: number) {
    this.statusClassDifusion = Constants.statusClassList[value];
    if (value === StatusConnection.SUCCESS || value === StatusConnection.SUCCESS_RECONNECT) {
      this.statusDifusion = true;
    } else {
      this.statusDifusion = false;
    }
    this.setStatusNetwork(value, null);
  }

  public setStatusNetwork(statusDifusionTemp: number, statusOmsTemp: number) {
    if (statusDifusionTemp || statusDifusionTemp === 0) {
      this.statusClassDifusion = Constants.statusClassList[statusDifusionTemp];
    }
    if (statusOmsTemp || statusOmsTemp === 0) {
      this.statusClassOMS = Constants.statusClassList[statusOmsTemp];
    }
    this.configService.writeLog('HEADER BAR > -----------------------------------------------------------');
    this.configService.writeLog('HEADER BAR > statusDifusion: ' + this.statusDifusion);
    this.configService.writeLog('HEADER BAR > statusClassDifusion: ' + this.statusClassDifusion);
    this.configService.writeLog('HEADER BAR > statusOMS: ' + this.statusOMS);
    this.configService.writeLog('HEADER BAR > statusClassOMS: ' + this.statusClassOMS);
    this.configService.writeLog('HEADER BAR > statutsNetworkconnected: ' + this.statutsNetworkconnected);
    this.configService.writeLog('HEADER BAR > -----------------------------------------------------------');
    if (statusDifusionTemp !== StatusConnection.ERROR || statusOmsTemp !== StatusConnection.ERROR) {
      this.showCloseNetworkErrorMessage();
    }
    this.checkReconnectModal();
  }

  public logout() {
    this.authenticationService.logout();
    if (this.configService.isLoginTypeWSO2() && this.configService.isUserAssessorLogged()) {
      window.location.href = this.configService.REDIRECT_LOGOUT + '/assessor/login';
    } else if (this.configService.isLoginTypeWSO2() && !this.configService.isUserAssessorLogged()) {
      window.location.href = this.configService.REDIRECT_LOGOUT + '/client/login';
    } else {
      this.router.navigate(['/login']);
    }
  }

  validateUser() {
    const account = this.configService.getHbAccount();
    const token = this.configService.getTokenApi();
    const tokenWF = this.configService.getToken();
    if (account && account !== null && account !== ''
      && tokenWF && tokenWF !== null && tokenWF !== ''
      && window.sessionStorage.length > 0) {

      this.authService.setUserLogged(account);
      this.authService.valideToken(account, token, tokenWF)
        .subscribe(r => {
          const tokenAba = this.configService.getHba();
          if (r && r.account && tokenAba === r.guidUser) {
          } else {
            this.authService.logoutWithoutRouter();
            this.modalService.closeAll();
            this.configService._authHbLoginType.next(3);
            if (r.length === 0) {
              if (this.configService.isLoginTypeWSO2()) {
                if (this.configService.isUserAssessorLogged()) {
                  this.router.navigate(['error'], {
                    queryParams: new ErrorMessage(Constants.ERROR_DENIED_ICON, Constants.MSG_LOGIN_MSG004, '', 'LOGIN', '/portal/assessor/login')
                  });
                } else {
                  this.router.navigate(['error'], {
                    queryParams: new ErrorMessage(Constants.ERROR_DENIED_ICON, Constants.MSG_LOGIN_MSG004, '', 'LOGIN', '/portal/client/login')
                  });
                }
              } else {
                this.router.navigate(['error'], {
                  queryParams: new ErrorMessage(Constants.ERROR_DENIED_ICON, Constants.MSG_LOGIN_MSG004, '', 'LOGIN', 'login')
                });
              }

            } else if (r && !r.noNetworkError) {
              this.router.navigate(['error'], {
                queryParams: new ErrorMessage(Constants.ERROR_EXPIRED_ICON, Constants.ERROR_EXPIRED_TITLE, Constants.ERROR_EXPIRED_TEXT,
                  Constants.ERROR_EXPIRED_BUTTON, 'login')
              });
            }
          }
        });
    }
  }

  showModalSession(show) {
    this.screenClicked = false;
    if (show && this.authService.isAuthenticated(false)) {
      this.dialog.open(SessionModalComponent, {
        width: '100vw',
        height: '100vh',
        panelClass: 'fullscreen',
        data: { secondsRemaining: this.timeSecond }
      });
    }
  }

  private load() {
    if (!this.workService.isLogged()) {
      return;
    }
    const userLogged = this.authService.getUserLogged();
    this.workService.loadWorkspaces(userLogged, false)
      .subscribe(dados => {
        if (dados) {
          this.authService.setUserLogged(userLogged);
          this.userWorkspaces = [];
          if (dados.length >= Constants.NUM_MAX_WORKSPACES) {
            this.showAddWorkspace = false;
          }
          if (dados.length > 0) {
            for (let i = 0; i < dados.length; i++) {
              const wksObj = new WorkspaceNewDto(dados[i].Id,
                dados[i].UserName,
                dados[i].Name,
                dados[i].Active,
                dados[i].IndexTab,
                dados[i].IsPrivate,
                dados[i].WorkspaceType,
                dados[i].JsonData);
              this.userWorkspaces.push(wksObj);
            }
          }

          if (this.userWorkspaces && this.userWorkspaces.length > 0) {
            this.workspaceSelId = this.userWorkspaces[0].Id;
            this.router.navigate([Constants.HOME_WKS_PATH], {
              queryParams: {
                workspaceId: this.userWorkspaces[0].Id    // First user workspace Id
              }
            });
          }
        }
      },
        () => {
        }
      );
  }

  clickTab(workspaceID: number): void {
    this.workspaceSelId = workspaceID;
    this.router.navigate([Constants.HOME_WKS_PATH], {
      queryParams: {
        workspaceId: this.workspaceSelId
      }
    });
  }

  addWorkspace() {
    this.dialog.open(WorkspaceConfModalComponent, {
      panelClass: 'workspace-conf-modal',
      disableClose: true
    });
  }

  addComponent() {
    const newWorkspace: WorkspaceNewDto = this.workService.getWorkspaceSelected();
    const contentToUpdate: Array<WorkspaceItemContentDto> = JSON.parse(newWorkspace.JsonData).content;
    this.componentsListBeforeUpdate = contentToUpdate.map(x => Object.assign({}, x));
    if (contentToUpdate.length >= Constants.NUM_MAX_WORKSPACE_CARDS) {
      this._NotificationService.warn('', Constants.WORKSPACE_MSG011);
    } else {
      if (this.userWorkspaces && this.userWorkspaces[0].Id) {
        this.dialog.open(CustomWorkspaceModalComponent, {
          panelClass: ['custom-wk-modal-outter'],
          position: { top: '0' },
          width: '100vw',
          maxWidth: '100vw',
          height: '100vh',
          disableClose: true
        });
      } else {
        // this.snackBar.createSnack(this.parent, Constants.WORKSPACE_MSG001, Constants.ALERT_TYPE.info);
        this._NotificationService.info('', Constants.WORKSPACE_MSG001);
      }
    }
  }

  buySellOrder(): void {
    this.modalBuySell.open(BuySellModalComponent, {
      panelClass: 'full-width-bottomsheet',
    });
  }

  showConfirmClose() {
    if (this.userWorkspaces.length > Constants.NUM_MIN_WORKSPACES) {
      const dialogConfig = this.dialog.open(NormalModalComponent, {
        data: {
          title: '',
          body: Constants.WORKSPACE_MSG003,
          redirectUrl: Constants.HOME_WKS_PATH,
          queryParams: { workspaceId: this.workspaceSelId, delete: true }
        },
        panelClass: Constants.NORMAL_MODAL,
      });
    } else {
      // this.snackBar.createSnack(this.parent, Constants.WORKSPACE_MSG001, Constants.ALERT_TYPE.info);
      this._NotificationService.info('', Constants.WORKSPACE_MSG001);
    }
  }

  openUpdatePasswordModal() {
    this.dialog.open(UpdatePasswordComponent, {
      panelClass: 'mat-modal-fullheight',
      position: { left: '0' },
      width: '555px'
    });
  }

  minimaxiHeader() {
    const headerBar = document.getElementById('extendable-header');
    const gridster = document.getElementById('contentGridster');
    const minmaxButton = document.getElementById('minmax-button-header');
    const triangle = document.getElementById('triangle-header-bar');
    if (this.statutsNetworkconnected) {
      if (this.headerBarOpen) {
        this.headerBarOpen = false;
        headerBar.style.top = '12px';
        minmaxButton.style.top = '70px';
        gridster.style.height = 'calc(100% - 60px)';
        gridster.style.top = '60px';
        triangle.className = 'triangle-bottom';
      } else {
        this.headerBarOpen = true;
        headerBar.style.top = '70px';
        minmaxButton.style.top = '128px';
        gridster.style.height = 'calc(100% - 128px)';
        gridster.style.top = '128px';
        triangle.className = 'triangle-top';
      }
    } else {
      if (this.headerBarOpen) {
        this.headerBarOpen = false;
        headerBar.style.top = '12px';
        minmaxButton.style.top = '108px';
        gridster.style.height = 'calc(100% - 98px)';
        gridster.style.top = '98px';
        triangle.className = 'triangle-bottom';
      } else {
        this.headerBarOpen = true;
        headerBar.style.top = '108px';
        minmaxButton.style.top = '166px';
        gridster.style.height = 'calc(100% - 166px)';
        gridster.style.top = '166px';
        triangle.className = 'triangle-top';
      }
    }
  }

  showCloseNetworkErrorMessage() {
    const headerBar = document.getElementById('extendable-header');
    const gridster = document.getElementById('contentGridster');
    const minmaxButton = document.getElementById('minmax-button-header');
    const triangle = document.getElementById('triangle-header-bar');
    const sidebarleft = document.getElementById('sidebar-left');

    if (this.statutsNetworkconnected) {
      sidebarleft.style.top = '0';
      if (this.headerBarOpen) {
        headerBar.style.top = '70px';
        minmaxButton.style.top = '128px';
        gridster.style.height = 'calc(100% - 128px)';
        gridster.style.top = '128px';
        triangle.className = 'triangle-top';
      } else {
        headerBar.style.top = '12px';
        minmaxButton.style.top = '70px';
        gridster.style.height = 'calc(100% - 60px)';
        gridster.style.top = '60px';
        triangle.className = 'triangle-bottom';
      }
    } else {
      sidebarleft.style.top = '25px';
      if (this.headerBarOpen) {
        headerBar.style.top = '108px';
        minmaxButton.style.top = '166px';
        gridster.style.height = 'calc(100% - 166px)';
        gridster.style.top = '166px';
        triangle.className = 'triangle-top';
      } else {
        headerBar.style.top = '12px';
        minmaxButton.style.top = '108px';
        gridster.style.height = 'calc(100% - 98px)';
        gridster.style.top = '98px';
        triangle.className = 'triangle-bottom';
      }
    }
  }

  editing(id) {
    if (id === this.doubleClickedTab) {
      return true;
    } else {
      return false;
    }
  }

  doEditRename(id) {
    this.workService.getUserWorkspaces().forEach(wk => {
      if (wk.Id === id) {
        this.doubleClickedTab = id;
      }
    });
  }

  enterRename(event, id) {
    this.doubleClickedTab = 6;
    const wkName = event.target.value;
    this.renameWorkspace(wkName, id);
  }

  openUserSettings() {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.minHeight = 386.5;
    dialogConfig.position = { 'top': '55px', 'right': '20px' };
    dialogConfig.panelClass = 'user-settings-dialog';
    this.dialog.open(UserSettingsPopUpComponent, dialogConfig);
  }

  private renameWorkspace(newName: string, workspaceChangedId: number) {
    if (newName.trim() === '') {
      this._NotificationService.warn('', Constants.WORKSPACE_MSG013);
    } else {
      if (this.workService.validateWorkspaceName(newName, workspaceChangedId)) {
        const userWorkspaceSelected: WorkspaceNewDto = this.workService.getUserWorkspaceById(this.workspaceSelId);
        const workspaceNameUpdated: WorkspaceNewSaveDto =
          new WorkspaceNewSaveDto(newName, JSON.parse(userWorkspaceSelected.JsonData).content,
            userWorkspaceSelected.IndexTab);
        this.router.navigate([Constants.HOME_WKS_PATH], {
          queryParams: { workspaceId: this.workspaceSelId, workspaceUpdated: JSON.stringify(workspaceNameUpdated) }
        });
      } else {
        this._NotificationService.warn('', Constants.WORKSPACE_MSG002);
      }
    }
  }

  inputGrow() {
    this.renderer.setStyle(this.quicksearch.nativeElement, 'right', '0');
  }

  inputShrink(focusOut: boolean) {
    if (focusOut) {
      this.renderer.setStyle(this.quicksearch.nativeElement, 'right', '-50px');
    }
  }

  workspaceLockUnlock() {
    this.lockWorkspace = !this.lockWorkspace;
    this.workService.updateWorkspaceLock(this.lockWorkspace);
  }

  selectAsset(value): void {
    if (this.userWorkspaces && this.userWorkspaces[0].Id) {
      if (value.symbol !== '') {
        const element = document.getElementById('searchBar');
        const position = element.getBoundingClientRect();
        const x = position.left;
        const dialogConfig = new MatDialogConfig();
        dialogConfig.data = { injectedSymbol: value };
        dialogConfig.minHeight = 300;
        dialogConfig.position = { 'top': '62px', left: (x - 72) + 'px' };
        dialogConfig.panelClass = 'custom-dialog-container';
        dialogConfig.autoFocus = false;
        this.dialog.open(AssetSummaryPopUpComponent, dialogConfig);
      } else {
        this._NotificationService.error('Erro', Constants.WORKSPACE_MSG007);
      }
    } else {
      this._NotificationService.info('', Constants.WORKSPACE_MSG001);
    }
  }

  checkReconnectModal() {
    setTimeout(() => {
      let options;
      this.configService.writeLog('HEADER BAR > ---------------- MODAL RECONECT ---------------------------');
      this.configService.writeLog('HEADER BAR > statusDifusion: ' + this.statusDifusion);
      this.configService.writeLog('HEADER BAR > statusOMS: ' + this.statusOMS);
      this.configService.writeLog('HEADER BAR > statutsNetworkconnected: ' + this.statutsNetworkconnected);
      if ((this.statusDifusion && this.statusOMS) && this.reconnectModal || !this.statutsNetworkconnected) {
        options = 'clear';
        this.closeReconnectModal();
        this.isFirstInitStatusModalReconect = false;
        this.isFirstInitModalReconectStatusDifusion = false;
        this.isFirstInitStatusModalReconectStatusOMS = false;
        this.configService.writeLog('HEADER BAR > Fecha modal');
        this.configService.writeLog('HEADER BAR > -----------------------------------------------------------');
      } else {
        if (!this.statusDifusion && !this.statusOMS) {
          this.configService.writeLog('HEADER BAR > DIFUSION e OMS OFFLINE');
          this.configService.writeLog('HEADER BAR > ----------------------------------------------------------');
          options = 'allDown';
          this.previousOption = options;
          if (this.reconnectModal && this.reconnectModal.componentInstance) {
            this.reconnectModal.componentInstance.change(options);
          } else {
            if (!this.isFirstInitStatusModalReconect) {
              this.isFirstInitStatusModalReconect = true;
              this.reconnectModal = this.dialog.open(ReconnectComponent, {
                id: 'reconnect-modal',
                disableClose: true,
                panelClass: 'reconnect-modal',
                position: { 'top': '61px', 'left': '190px' },
                data: options
              });
            }
          }
        } else if (!this.statusDifusion) {
          if (!this.isFirstInitModalReconectStatusDifusion) {
            this.isFirstInitModalReconectStatusDifusion = true;
            this.configService.writeLog('HEADER BAR > DIFUSION OFFLINE');
            this.configService.writeLog('HEADER BAR > -----------------------------------------------------------');
            options = 'difusionDown';
            this.previousOption = options;
            if (this.reconnectModal && this.reconnectModal.componentInstance) {
              this.reconnectModal.componentInstance.change(options);
            } else {
              this.reconnectModal = this.dialog.open(ReconnectComponent, {
                id: 'reconnect-modal',
                disableClose: true,
                panelClass: 'reconnect-modal',
                position: { 'top': '61px', 'left': '190px' },
                data: options
              });
              // if (this.reconnectModal) {
              //   setTimeout(() => {
              //     if (this.reconnectModal) {
              //       this.reconnectModal.close();
              //     }
              //   }, 5000);
              // }
            }
          }
        } else if (!this.statusOMS) {
          if (!this.isFirstInitStatusModalReconectStatusOMS) {
            this.isFirstInitStatusModalReconectStatusOMS = true;
            this.configService.writeLog('HEADER BAR > OMS OFFLINE');
            this.configService.writeLog('HEADER BAR > -----------------------------------------------------------');
            options = 'omsDown';
            this.previousOption = options;
            if (this.reconnectModal && this.reconnectModal.componentInstance) {
              this.reconnectModal.componentInstance.change(options);
            } else {
              this.reconnectModal = this.dialog.open(ReconnectComponent, {
                id: 'reconnect-modal',
                disableClose: true,
                panelClass: 'reconnect-modal',
                position: { 'top': '61px', 'left': '190px' },
                data: options
              });
              // if (this.reconnectModal) {
              //   setTimeout(() => {
              //     if (this.reconnectModal) {
              //       this.reconnectModal.close();
              //     }
              //   }, 5000);
              // }
            }
          }
        }
      }
    }, 10);
  }

  closeReconnectModal() {
    if (this.reconnectModal && this.reconnectModal.componentInstance) {
      this.reconnectModal.componentInstance.closeModal(true);
      this.reconnectModal = null;
    }
  }

  openNew() {
    if (this.liveRunning) {
      this.dialog.open(YoutubeLiveModalComponent, {
        width: '80vw',
        height: '80vh'
      });
    } else {
      this.dialog.open(ModalNewsComponent, {
        data: {
          new: this.lastNewFestMarket,
          close: false,
          origin: 'workspace'
        }
      });
    }
  }


}
