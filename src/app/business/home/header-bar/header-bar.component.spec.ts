import { ReconnectComponent } from '@business/reconnect/reconnect.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { StatusDifusionServiceMock } from './../../../services/_test-mock-services/sckt-status-difusion.service.mock';
import { NotificationsService, SimpleNotificationsModule } from 'angular2-notifications';
import { QuickSearchAutoCompleteComponent } from './../../../component/quick-search-auto-complete/quick-search-auto-complete.component';
import { MatIconModule, MatInputModule, MatAutocompleteModule, MatDialogModule, MatBottomSheetModule, MatProgressSpinnerModule, MatCardModule } from '@angular/material';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { HeaderBarComponent } from '@business/home/header-bar/header-bar.component';
import { ConfigService } from '@services/webFeeder/config.service';
import { ConfigServiceMock, CustomSnackbarServiceMock, AuthenticationServiceMock, AuthServiceMock, FastMarketServiceMock, StatusOmsServiceMock, AutoLogoutServiceMock } from '@services/_test-mock-services/_mock-services';
import { HttpModule } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';
import { CustomSnackbarService } from '@services/custom-snackbar-service.service';
import { DatePipe } from '@angular/common';
import { AuthenticationService } from '@services/authentication.service';
import { AuthService } from '@auth/auth.service';
import { FastMarketService } from '@services/fast-market.service';
import { StatusOmsService } from '@services/webFeeder/sckt-status-oms.service';
import { StatusDifusionService } from '@services/webFeeder/sckt-status-difusion.service';
import { AutoLogoutService } from '@auth/auto-logout-service.service';
import { RouterModule } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { LoginComponent } from '@business/login/login.component';
import { SpinnerComponent } from '@component/spinner/spinner.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { AutoCompleteComponent } from '@component/auto-complete/auto-complete.component';
import { LottieAnimationViewModule } from 'ng-lottie';

describe('HeaderBarComponent', () => {
  let component: HeaderBarComponent;
  let fixture: ComponentFixture<HeaderBarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        HeaderBarComponent,
        QuickSearchAutoCompleteComponent,
        LoginComponent,
        SpinnerComponent,
        ReconnectComponent,
      ],
      imports: [
        RouterTestingModule.withRoutes(
          [{ path: 'login', component: LoginComponent }]),
        MatIconModule,
        MatInputModule,
        MatAutocompleteModule,
        HttpModule,
        HttpClientModule,
        SimpleNotificationsModule.forRoot(),
        MatDialogModule,
        MatBottomSheetModule,
        BrowserAnimationsModule,
        MatProgressSpinnerModule,
        FormsModule,
        MatCardModule,
        LottieAnimationViewModule
      ],
      providers: [
        DatePipe,
        NotificationsService,
        { provide: AuthService, useClass: AuthServiceMock },
        { provide: AuthenticationService, useClass: AuthenticationServiceMock },
        { provide: ConfigService, useClass: ConfigServiceMock },
        { provide: CustomSnackbarService, useClass: CustomSnackbarServiceMock },
        { provide: FastMarketService, useClass: FastMarketServiceMock },
        { provide: StatusOmsService, useClass: StatusOmsServiceMock },
        { provide: StatusDifusionService, useClass: StatusDifusionServiceMock },
        { provide: AutoLogoutService, useClass: AutoLogoutServiceMock },
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HeaderBarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
