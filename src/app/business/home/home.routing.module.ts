import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { HomeComponent } from '@business/home/home.component';
import { AuthGuard } from '@auth/auth.guard';
import { WorkspaceNewComponent } from '@business/home/workspace-new/workspace-new.component';

const homeRoutes: Routes = [
    {
        path: '', component: HomeComponent, canActivate: [AuthGuard],
        children: [
            { path: '', pathMatch: 'full', component: WorkspaceNewComponent, canActivate: [AuthGuard] },
            { path: 'workspace', pathMatch: 'full', component: WorkspaceNewComponent, canActivate: [AuthGuard] },
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(homeRoutes)],
    exports: [RouterModule]
})
export class HomeRoutingModule { }
