import { RankingComponent } from './../../ranking/ranking.component';
import { ExtractFinancialComponent } from './../../extract-financial/extract-financial.component';
import { AppUtils } from '@utils/app.utils';
import { AssessorSearchClientComponent } from './../../assessor-search-client/assessor-search-client.component';
import { AssessorDataComponent } from './../../assessor-data/assessor-data.component';
import { Component, Inject, OnDestroy, OnInit, ViewContainerRef, HostListener } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material';
import { Router } from '@angular/router';
import { AuthService } from '@auth/auth.service';
import { BalanceConsultComponent } from '@business/balance-consult/balance-consult.component';
import { BrokerAuthenticationComponent } from '@business/broker-authentication/broker-authentication.component';
import { CustodyConsultComponent } from '@business/custody-consult/custody-consult.component';
import { ModalNewsComponent } from '@business/modal-news/modal-news.component';
import { ModalTradingViewComponent } from '@business/modal-trading-view/modal-trading-view.component';
import { OrdersHistoryComponent } from '@business/orders-history/orders-history.component';
import { OrdersHistoryPresenter } from '@business/orders-history/orders-history.presenter';
import { AuthenticationService } from '@services/authentication.service';
import { BilletOrderModalService } from '@services/billet-order-modal.service';
import { CustomSnackbarService } from '@services/custom-snackbar-service.service';
import { SidebarService } from '@services/sidebar.service';
import { ConfigService } from '@services/webFeeder/config.service';
import { DailyOrderService } from '@services/webFeeder/sckt-daily-order.service';
import { WorkspaceNewService } from '@services/workspace-new.service';
import { Constants } from '@utils/constants';
import { NotificationsService } from 'angular2-notifications';
import { Subscription } from 'rxjs';
import { ErrorMessage } from '@model/error-message';
import { AutoLogoutService } from '@auth/auto-logout-service.service';
import { AuthServer } from '@services/auth-server.service';
import { environment } from 'environments/environment';
import { BrokerageNoteComponent } from '@business/brokerage-note/brokerage-note.component';
import { StatusOmsService } from '@services/webFeeder/sckt-status-oms.service';
import { StatusConnection } from '@model/status-connection';

@Component({
  selector: 'app-side-bar-left',
  templateUrl: './side-bar-left.component.html'
})

export class SideBarLeftComponent implements OnInit, OnDestroy {
  public versionQA: string;

  public connectedAs: number; // 1 = Assessor, 2 = Corretora, 3 = Não conectado
  public subscriptionHbLoginType: Subscription;
  public accountHbLogged: string;

  public clickOpenTabOrder: Subscription;
  public clickOpenLoginTab: Subscription;
  public clickOpenTabCustody: Subscription;
  public clickOpenTabFinance: Subscription;
  public clickOpenTabHistory: Subscription;
  public hbLoginTypeEnable: string;
  public isLoginTypeWSO2: boolean;
  public showHelp: boolean;
  public isAssessor: boolean;
  public modalAuth: any;

  workspaceSelId = 0;
  clientName;
  bmfOrders = 0;
  bovOrders = 0;
  custodyOrders = this.bmfOrders + this.bovOrders;
  balance;
  image;
  perfilKind;
  sideBarOpen = true;
  modalExtract: any;
  modalBrokerage: any;
  modalRanking: any;
  modalEducational: any;
  modalOrders: any;
  modalTradingView: MatDialogRef<ModalTradingViewComponent>;
  modalNews: MatDialogRef<ModalNewsComponent>;
  modalCustody: any;
  modalBalance: any;
  modalAssessorData: any;
  modalAssessorSearch: any;
  idSelected: string;
  modalOpen = false;
  public subscriptionDailyOrder: Subscription;
  private subscriptionSessionExpired: Subscription;
  public subscriptionClientAssessorChangeSelect: Subscription;
  public subscriptionSideMenu: Subscription;

  public subscriptionMyBalance: Subscription;
  public subscriptionCustodyBmf: Subscription;
  public subscriptionCustodyBov: Subscription;
  public authHbLoggedBroker: Subscription;
  public quantityOrder: number;
  public account: string;
  public rowAllOrdersData: any[];
  public currentDailyList = [];
  private historyOrderPresenter: OrdersHistoryPresenter;
  selectedAccount: string;
  public sessionExpired: boolean;
  public accountOptions = [];

  public titleHelp = Constants.SIDEBAR_HELP_TITLE;
  public helpPhone1: string;
  public helpPhone2: string;
  public helpEmail: string;
  public sessionAlert: boolean;
  private subscriptionStatusAlertTimeout: Subscription;
  private subscriptionAccountList: Subscription;
  public menuSelected = 0;
  public loadingAssessor = false;
  public loadingAssessorCedro = false;
  public assessorClientSelected: any;
  public perfilProfile: string;
  public whithelabel = this.configService.getWhiteLabel();

  private subscriptionStatusOms: Subscription;
  public statusOmsOnline: boolean;
  public msgStatusOmsOffile = Constants.MSG_STATUS_OFFLINE_OMS;

  constructor(
    private dialog: MatDialog,
    private authenticationService: AuthenticationService,
    private workService: WorkspaceNewService,
    private autoLogoutService: AutoLogoutService,
    private sidebarService: SidebarService,
    private authService: AuthService,
    private configService: ConfigService,
    private dailyOrderService: DailyOrderService,
    private notificationService: NotificationsService,
    public snackBar: CustomSnackbarService,
    private _configService: ConfigService,
    @Inject(ViewContainerRef) public msgPlaceholder: ViewContainerRef,
    private router: Router,
    private billetOrderModalService: BilletOrderModalService,
    private _authServer: AuthServer,
    private _statusOmsService: StatusOmsService
  ) {
    Constants.WHITELABEL.forEach(element => {
      if (element.cod === environment.WHITELABEL) {
        if (Number(element.cod) === 10) {
          this.showHelp = false;
          this.titleHelp = Constants.SIDEBAR_HELP_TITLE_RANKING;
        } else if (element.cod !== 1 && element.cod !== 4 && element.cod !== 7) {
          this.showHelp = true;
        } else {
          if (element.cod === environment.WHITELABEL) {
            if (element.phone1 && element.phone1 !== '') {
              this.helpPhone1 = element.phone1;
            }
            if (element.phone2 && element.phone2 !== '') {
              this.helpPhone2 = element.phone2;
            }
            if (element.email && element.email !== '') {
              this.helpEmail = element.email;
            }
          }
        }
      }
    });
    this.hbLoginTypeEnable = configService.getLoginTypeEnabled();
    this.isLoginTypeWSO2 = this.configService.isLoginTypeWSO2();

    this.subscriptionStatusOms = this._statusOmsService.messagesStatusOms
      .subscribe(statusOms => {
        if (statusOms === StatusConnection.ERROR) {
          this.statusOmsOnline = false;
          if (this.subscriptionAccountList) {
            this.subscriptionAccountList.unsubscribe();
            this.subscriptionAccountList = null;
          }
        } else {
          this.statusOmsOnline = true;
          this.subscriptionHbLoginType = this.configService.authHbLoginType$.subscribe(x => {
            this.connectedAs = x;
            if (this.configService.getBrokerAccountType()) {
              this.connectedAs = Number(this.configService.getBrokerAccountType());
            }
            this.configService.writeLog('SIDEBAR > perfil: ' + this.connectedAs);
            this.loadProfileInit();
          });
        }
      });

    this._configService.changeAccountBillet$.subscribe(res => {
      if (res) {
        this.loadingAssessor = true;
        this.selectedAccount = res;
        setTimeout(() => {
          this.loadingAssessor = false;
          this.notificationService.success('', 'Conta do perfil alterada com sucesso.');
        }, 500);
      }
    });

    // TODO: Assina os serviços de socket
    // TODO: 1 = Order | 2 = History | 3 = Custody | 4 = Meu Saldo | 5 - Billet (Boleta) | 6 - Billet (Planilha) | 7 = Grafico | 8 = News
    // this.clickOpenTabOrder = this.sidebarService.clickOpenTabAuth$
    //   .subscribe(tab => {
    //     if (tab === 1) {
    //       this.loadOrders();
    //     } else if (tab === 2) {
    //       this.openHistory();
    //     } else if (tab === 3) {
    //       this.openCustody();
    //     } else if (tab === 4) {
    //       this.openMyBalance();
    //     }
    //   });

    this.clickOpenLoginTab = this.sidebarService.clickOpenTabLoginBroker$.subscribe(x => {
      if (x) {
        this.openBrokerAuthenticationModal();
      }
    });

    this.subscriptionSessionExpired = this.configService.loggoutSessionExpired$.subscribe(x => {
      this.sessionExpired = x;
      if (this.sessionExpired) {
        this.changeAccount();
      }
    });

    this.subscriptionClientAssessorChangeSelect = this.configService.clientAssessorChangeSelect$.subscribe(x => {
      if (x) {
        this.loadingAssessor = true;
        this.loadProfileInit();
      }
    });

    this.subscriptionSideMenu = this.configService.clickOpenSideMenu$.subscribe(x => {
      if (x && x === Constants.ID_SIDE_MENU_ORDER) {
        this.openHistory();
      }
    });

    this.dialog = dialog;
    this.account = this.configService.getBrokerAccount();
    this.historyOrderPresenter = new OrdersHistoryPresenter();
  }

  loadProfileInit() {
    // this.accountOptions = [];
    this.configService.writeLog('SIDEBAR > connectedAs: ' + this.connectedAs);
    if (this.connectedAs === 1) {
      // Assessor
      this.loadingAssessor = true;
      this.assessorClientSelected = this.configService.getAssessorClientSelected();
      // TODO: receber as contas do assessor de acordo com o novo metodo
      // const accountSelected = this.assessorClientSelected.user.advisor_client_accounts[0].account_id;
      let accountSelected = '';
      if (this.configService.getBrokerAccountSelected()) {
        accountSelected = this.configService.getBrokerAccountSelected();
      } else {
        accountSelected = this.assessorClientSelected.user.advisor_client_accounts[0].account_id;
      }
      if (accountSelected) {
        this.configService.writeLog('SIDEBAR > accountSelected: ' + accountSelected);
        this.configService.writeLog('SIDEBAR > chama - brokerServiceAccountList');
        // this.subscriptionAccountList = this._authServer.iNegotiation.brokerServiceAccountList(accountSelected)
        this._authServer.iNegotiation.brokerServiceAccountList(accountSelected)
          .subscribe(res => {
            this.configService.writeLog('SIDEBAR > resposta - brokerServiceAccountList');
            this.configService.writeLog(res);
            if (res.length > 0) {
              for (let i = 0; i < res.length; i++) {
                if (!this.accountOptions.includes(res[i].accountsBean)) {
                  for (let x = 0; x < res[i].accountsBean.length; x++) {
                    if (res[i].user.profile_name && Number(res[i].accountsBean[x].account) === Number(accountSelected)) {
                      if (!this.accountOptions.includes(res[i].accountsBean[x].account)) {
                        this.accountOptions.push(res[i].accountsBean);
                        this.configService.setBrokerAccountList(res[i]);
                        this.accountHbLogged = this.accountOptions[0];
                        this.perfilProfile = res[i].user.profile_name.toUpperCase();
                        this.selectedAccount = res[i].accountsBean[x].account;
                        break;
                      }
                    }
                  }
                }
              }
              const optionsTemp: any = [];
              this.accountOptions[0].forEach((item) => {
                const duplicated = optionsTemp.findIndex(redItem => {
                  return item.account === redItem.account;
                }) > -1;
                if (!duplicated) {
                  optionsTemp.push(item);
                }
              });
              this.accountOptions = [];
              this.accountHbLogged = optionsTemp;
              AppUtils.orderObjectAsc(this.accountHbLogged, 'account');
              // this.startSubscribe(this.selectedAccount);
              this.configService.setBrokerAccountSelected(this.selectedAccount);
              this.configService._authHbLogged.next(true);
              this.loadPerfil();
              this.loadingAssessor = false;
              this.configService.writeLog('SIDEBAR > selectedAccount: ' + this.selectedAccount);
            }
          }, error => {
            this.configService.writeLog('SIDEBAR > resposta - brokerServiceAccountList');
            this.configService.writeLog(error);
            this.configService.writeLog('SIDEBAR > Conta não encontrada!');
            this.loadingAssessor = false;
          });
      }

    } else if (this.connectedAs === 2 || this.connectedAs === 4) {
      this.configService.writeLog('SIDEBAR > connectedAs: ' + this.connectedAs);
      // Cedro
      if (!this.subscriptionAccountList) {
        this.loadingAssessorCedro = true;
        this.configService.writeLog('SIDEBAR > Account: ' + this._configService.getBrokerAccount());
        this.configService.writeLog('SIDEBAR > chama - brokerServiceAccountList');
        this.subscriptionAccountList = this._authServer.iNegotiation.brokerServiceAccountList(
          this._configService.getBrokerAccount())
          .subscribe(res => {
            this.configService.writeLog('SIDEBAR > resposta - brokerServiceAccountList');
            this.configService.writeLog(res);
            if (res) {
              for (let i = 0; i < res.length; i++) {
                this.configService.setBrokerAccountList(res);
                if (!this.accountOptions.includes(res[i].account)) {
                  this.accountOptions.push(res[i].account);
                  if (this.configService.isLoginTypeWSO2()) {
                    this.perfilProfile = this.configService.getProfileName().toUpperCase();
                  } else {
                    if (res[i].suitabilityIdName) {
                      this.perfilProfile = res[i].suitabilityIdName.toUpperCase();
                    }
                  }
                }
              }
              const optionsTemp: any = [];
              this.accountOptions.forEach((item) => {
                const duplicated = optionsTemp.findIndex(redItem => {
                  return item === redItem;
                }) > -1;
                if (!duplicated) {
                  optionsTemp.push(item);
                }
              });
              this.accountOptions = [];
              this.accountOptions = optionsTemp;
              AppUtils.orderObjectAscNumber(this.accountOptions);
              if (this.configService.getBrokerAccountSelected()) {
                this.accountHbLogged = this.configService.getBrokerAccountSelected();
              } else {
                this.accountHbLogged = this.accountOptions[0];
              }
              this.selectedAccount = this.accountHbLogged;
              this.configService.setBrokerAccountSelected(this.selectedAccount);
              this.configService._authHbLogged.next(true);
              this.loadPerfil();
              this.loadingAssessorCedro = false;
            }
          }, error => {
            this.configService.writeLog('SIDEBAR > resposta - brokerServiceAccountList');
            this.configService.writeLog(error);
            this.configService.writeLog('SIDEBAR > Conta não encontrada!');
            this.loadingAssessorCedro = false;
          });
      }
    } else if (this.connectedAs === 3) {
      this.configService.writeLog('SIDEBAR > connectedAs: ' + this.connectedAs);
      this.configService.writeLog('SIDEBAR > OFFILINE sem brokerServiceAccountList');
      // Offline
      this.configService._authHbLogged.next(false);
      this.loadPerfil();
    } else if (this.connectedAs === 5) {
      this.configService.writeLog('SIDEBAR > connectedAs: ' + this.connectedAs);
      // Simulador
      this.accountHbLogged = this.configService.getBrokerAccount();
      if (!this.subscriptionAccountList) {
        this.configService.writeLog('SIDEBAR > Account: ' + this._configService.getBrokerAccount());
        this.configService.writeLog('SIDEBAR > chama - brokerServiceAccountList');
        this.subscriptionAccountList = this._authServer.iNegotiation.brokerServiceAccountList(this._configService.getBrokerAccount())
          .subscribe(res => {
            this.configService.writeLog('SIDEBAR > resposta - brokerServiceAccountList');
            this.configService.writeLog(res);
            if (res) {
              for (let i = 0; i < res.length; i++) {
                if (!this.accountOptions.includes(res[i].account)) {
                  this.accountOptions.push(res[i].account);
                  this.configService.setBrokerAccountList(res[i]);
                  if (res[i].suitabilityIdName) {
                    this.perfilProfile = res[i].suitabilityIdName.toUpperCase();
                  }
                }
              }
              this.accountHbLogged = this.accountOptions[0];
              this.selectedAccount = this.accountHbLogged;
              // this.startSubscribe(this.selectedAccount);
              this.configService.setBrokerAccountSelected(this.selectedAccount);
              this.configService._authHbLogged.next(true);
              this.loadPerfil();
            }
          }, error => {
            this.configService.writeLog('SIDEBAR > resposta - brokerServiceAccountList');
            this.configService.writeLog(error);
            this.configService.writeLog('SIDEBAR > Conta não encontrada!');
          });
      }
    } else {
      this.configService._authHbLogged.next(false);
      this.loadPerfil();
    }
  }

  ngOnInit() {
    this.subscriptionStatusAlertTimeout = this.autoLogoutService.statusAlertTimeout$
      .subscribe(x => {
        this.sessionAlert = x;
        if (this.sessionAlert) {
          const sidebar = document.getElementById('sidebar-left');
          if (sidebar) {
            sidebar.classList.add('modelInvisible');
          }
        } else {
          const sidebar = document.getElementById('sidebar-left');
          if (sidebar) {
            sidebar.classList.remove('modelInvisible');
          }
        }
      });

    this.authenticationService.connectedAs$.subscribe(data => {
      this.loadPerfil();
    });
    this.workService.workspaceSelected$.subscribe(data => {
      if (data > 0) {
        this.workspaceSelId = data;
      }
    });
    this.loadPerfil();
    // this.fillOrders();
    this.sidebarService.status$.subscribe((status: boolean) => {
      this.sideBarOpen = !status;
      this.minimaxi();
    });
  }

  @HostListener('window:beforeunload', ['$event'])
  beforeUnloadHander(event) {
    this.sidebarService.closeDetachedWindows();
  }

  ngOnDestroy() {
    if (this.dialog.getDialogById('order-history-modal')) {
      this.dialog.getDialogById('order-history-modal').close();
    }
    if (this.subscriptionStatusOms) {
      this.subscriptionStatusOms.unsubscribe();
    }
    if (this.subscriptionDailyOrder) {
      this.subscriptionDailyOrder.unsubscribe();
      this.subscriptionMyBalance.unsubscribe();
      this.subscriptionCustodyBmf.unsubscribe();
      this.subscriptionCustodyBov.unsubscribe();
    }

    if (this.subscriptionDailyOrder) {
      this.subscriptionDailyOrder.unsubscribe();
    }

    if (this.subscriptionAccountList) {
      this.subscriptionAccountList.unsubscribe();
    }

    if (this.subscriptionHbLoginType) {
      this.subscriptionHbLoginType.unsubscribe();
    }

    if (this.clickOpenLoginTab) {
      this.clickOpenLoginTab.unsubscribe();
    }

    this.workService._openTradingView.next(false);
  }

  loadPerfil() {
    if (this.connectedAs === 1) { // TODO remove mock
      this.image = './assets/images/assets/avatar.svg';
      this.clientName = this.assessorClientSelected.user.full_name.split(' ')[0];
      this.perfilKind = this.assessorClientSelected.user.profile_name;
      // this.balance = 'R$ 1.250.030,15';
      // TODO } else if (this.authenticationService.checkBrokerAuthentication()) {
    } else if (this.connectedAs === 2) {
      this.image = './assets/images/assets/cedro.png';
      this.clientName = 'Cedro';
      // this.balance = 'R$ 32.189.050,00';
    } else if (this.connectedAs === 3) {
      this.image = './assets/images/svg/ico.svg';
      // this.balance = 'R$ 0,00';
    } else if (this.connectedAs === 4) {
      this.connectedAs = 4;
      this.clientName = 'Cedro';
      this.image = './assets/images/assets/cedro.png';
      // this.balance = 'R$ 0,00';
    } else if (this.connectedAs === 5) {
      this.clientName = 'SIMULADOR';
    }
  }

  minimaxi() {
    const sideBar = document.getElementById('sidebar-left');
    const button = document.getElementById('minmax-button');
    const gridster = document.getElementById('contentGridster');
    const headerBar = document.getElementById('extendable-header');
    const triangle = document.getElementById('triangle-left-bar');
    const minmaxButton = document.getElementById('minmax-button-header');
    if (this.sideBarOpen) {
      if (this.idSelected === Constants.MODAL_SIDE_LEFT_HISTORY && this.modalOrders) {
        this.modalOrders.updatePosition({
          left: '150px'
        });
      } else if (this.idSelected === Constants.MODAL_SIDE_LEFT_GRAPH && this.modalTradingView) {
        this.modalTradingView.updatePosition({
          left: '150px'
        });
      } else if (this.idSelected === Constants.MODAL_SIDE_LEFT_NEWS && this.modalNews) {
        this.modalNews.updatePosition({
          left: '150px'
        });
      } else if (this.idSelected === Constants.MODAL_SIDE_LEFT_CUSTODY && this.modalCustody) {
        this.modalCustody.updatePosition({
          left: '150px'
        });
      } else if (this.idSelected === Constants.MODAL_SIDE_LEFT_ASSESSORDATA && this.modalAssessorData) {
        this.modalAssessorData.updatePosition({
          left: '150px'
        });
      } else if (this.idSelected === Constants.MODAL_SIDE_LEFT_ASSESSOR_SEARCH && this.modalAssessorSearch) {
        this.modalAssessorSearch.updatePosition({
          left: '150px'
        });
      }
      this.sideBarOpen = false;
      button.style.zIndex = '1';
      sideBar.style.left = '-160px';
      gridster.style.width = '100%';
      gridster.style.marginLeft = '0';
      headerBar.style.width = '100%';
      headerBar.style.left = '0';
      triangle.className = 'triangle-right';
      minmaxButton.style.left = 'calc(50% - 72px)';
    } else {
      if (this.idSelected === Constants.MODAL_SIDE_LEFT_HISTORY && this.modalOrders) {
        this.modalOrders.updatePosition({
          left: '200px'
        });
      } else if (this.idSelected === Constants.MODAL_SIDE_LEFT_GRAPH && this.modalTradingView) {
        this.modalTradingView.updatePosition({
          left: '200px'
        });
      } else if (this.idSelected === Constants.MODAL_SIDE_LEFT_NEWS && this.modalNews) {
        this.modalNews.updatePosition({
          left: '200px'
        });
      } else if (this.idSelected === Constants.MODAL_SIDE_LEFT_CUSTODY && this.modalCustody) {
        this.modalCustody.updatePosition({
          left: '200px'
        });
      } else if (this.idSelected === Constants.MODAL_SIDE_LEFT_BALANCE && this.modalBalance) {
        this.modalBalance.updatePosition({
          left: '200px'
        });
      } else if (this.idSelected === Constants.MODAL_SIDE_LEFT_ASSESSORDATA && this.modalAssessorData) {
        this.modalAssessorData.updatePosition({
          left: '200px'
        });
      } else if (this.idSelected === Constants.MODAL_SIDE_LEFT_ASSESSOR_SEARCH && this.modalAssessorSearch) {
        this.modalAssessorSearch.updatePosition({
          left: '200px'
        });
      }
      this.sideBarOpen = true;
      button.style.zIndex = '0';
      sideBar.style.left = '0px';
      gridster.style.width = 'calc(100% - 160px)';
      gridster.style.marginLeft = '160px';
      headerBar.style.width = 'calc(100% - 160px)';
      headerBar.style.left = '160px';
      triangle.className = 'triangle-left';
      minmaxButton.style.left = 'calc(100% - 50%)';
    }
  }

  openBrokerAuthenticationModal() {
    this.dialog.closeAll();
    if (!this.dialog.getDialogById('broker-authentication-component')) {
      this.sidebarService._clickOpenTabAuthCallBack.next(0);
      this.modalAuth = this.dialog.open(BrokerAuthenticationComponent, {
        panelClass: Constants.FULL_MODAL,
        position: { left: '0' },
        width: '555px',
        autoFocus: false,
        id: 'broker-authentication-component',
        data: {
          title: '',
          body: Constants.WORKSPACE_MSG003,
          redirectUrl: Constants.HOME_WKS_PATH,
          queryParams: { workspaceId: this.workspaceSelId, delete: true }
        }
      });
    }
  }

  selectOption(value) {
    this.idSelected = value;
    if (this.idSelected !== Constants.MODAL_SIDE_LEFT_HISTORY && this.modalOrders) {
      this.modalOrders.close();
    }
    if (this.idSelected !== Constants.MODAL_SIDE_LEFT_GRAPH && this.modalTradingView) {
      this.modalTradingView.close();
    }
    if (this.idSelected !== Constants.MODAL_SIDE_LEFT_NEWS && this.modalNews) {
      this.modalNews.close();
    }
    if (this.idSelected !== Constants.MODAL_SIDE_LEFT_CUSTODY && this.modalCustody) {
      this.modalCustody.close();
    }
    if (this.idSelected !== Constants.MODAL_SIDE_LEFT_BALANCE && this.modalBalance) {
      this.modalBalance.close();
    }
    if (this.idSelected !== Constants.MODAL_SIDE_LEFT_ASSESSORDATA && this.modalAssessorData) {
      this.modalAssessorData.close();
    }
    if (this.idSelected !== Constants.MODAL_SIDE_LEFT_ASSESSORDATA && this.modalAssessorSearch) {
      this.modalAssessorSearch.close();
    }
    for (let i = 1; i <= 5; i++) {
      if (i !== value) {
        const elem = document.getElementById(i.toString());
        if (elem) {
          elem.className = 'list-item pointer';
        }
      } else {
        const elem = document.getElementById(i.toString());
        if (elem) {
          elem.className = 'list-item selected pointer';
        }
      }
    }
    const sidebar = document.getElementById('sidebar-left');
    setTimeout(() => {
      sidebar.classList.add('modalOrder');
    });
  }

  openHistory() {
    if (!this.dialog.getDialogById('order-history-modal')) {
      this.dialog.closeAll();
      const isAuthenticationBroker = this.configService.isAuthenticationBroker();
      if (!isAuthenticationBroker) {
        this.openBrokerAuthenticationModal();
        this.sidebarService._clickOpenTabAuthCallBack.next(2);
        return false;
      } else {
        this.menuSelected = 2;
        if (this.idSelected !== Constants.MODAL_SIDE_LEFT_HISTORY || !this.modalOpen) {
          this.modalOrders = this.dialog.open(OrdersHistoryComponent, {
            position: {
              left: '186px',
              top: '94.3px'
            },
            id: 'order-history-modal',
            height: 'calc(100% - 118.5px)',
            width: 'calc(100% - 209px)',
            maxWidth: 'unset',
            panelClass: 'panel-class-order-component',
            disableClose: true
          });
          this.modalOpen = true;
        }
        this.modalOrders.afterClosed().subscribe(data => {
          const elem = document.getElementById(Constants.MODAL_SIDE_LEFT_HISTORY);
          if (elem) {
            elem.className = 'list-item pointer';
          }
          this.modalOpen = false;
          this.menuSelected = 0;
        });
        this.modalOrders.beforeClose().subscribe(data => {
          const sidebar = document.getElementById('sidebar-left');
          if (sidebar) {
            sidebar.classList.remove('modalOrder');
          }
        });
        this.selectOption(Constants.MODAL_SIDE_LEFT_HISTORY);
        this.modalOrders.afterOpen().subscribe(item => {
          this.modalOrders.componentInstance.initializeOrderHistory();
        });
      }
    }
  }

  openCloseSidebar() {
    this.sidebarService.openClose();
  }

  openGraph() {
    this.dialog.closeAll();
    if (this.idSelected !== Constants.MODAL_SIDE_LEFT_GRAPH || !this.modalOpen) {
      this.menuSelected = 7;
      this.modalTradingView = this.dialog.open(ModalTradingViewComponent, {
        position: {
          left: '186px',
          top: '94.3px'
        },
        height: 'calc(100% - 118.5px)',
        width: 'calc(100% - 209px)',
        maxWidth: 'unset',
        panelClass: 'panel-class-order-component'
      });
      this.modalOpen = true;
    }
    this.modalTradingView.afterClosed().subscribe(data => {
      const elem = document.getElementById(Constants.MODAL_SIDE_LEFT_GRAPH);
      if (elem) {
        elem.className = 'list-item pointer';
        this.modalOpen = false;
        this.menuSelected = 0;
      }
    });
    this.modalTradingView.beforeClose().subscribe(data => {
      const sidebar = document.getElementById('sidebar-left');
      sidebar.classList.remove('modalOrder');
    });
    this.modalTradingView.afterOpen().subscribe(data => {
      this.modalTradingView.componentInstance.createChart();
    });
    this.selectOption(Constants.MODAL_SIDE_LEFT_GRAPH);
  }

  openNews() {
    this.dialog.closeAll();
    if (this.idSelected !== Constants.MODAL_SIDE_LEFT_NEWS || !this.modalOpen) {
      this.menuSelected = 8;
      this.modalNews = this.dialog.open(ModalNewsComponent, {
        position: {
          left: '186px',
          top: '94.3px'
        },
        height: 'calc(100% - 118.5px)',
        width: 'calc(100% - 209px)',
        maxWidth: 'unset',
        panelClass: 'panel-class-modal-news-component',
        disableClose: true
      });
      this.modalOpen = true;
    }
    this.modalNews.afterClosed().subscribe(data => {
      const elem = document.getElementById(Constants.MODAL_SIDE_LEFT_NEWS);
      if (elem) {
        elem.className = 'list-item pointer';
      }
      this.modalOpen = false;
      this.menuSelected = 0;
    });
    this.modalNews.beforeClose().subscribe(data => {
      const sidebar = document.getElementById('sidebar-left');
      if (sidebar) {
        sidebar.classList.remove('modalOrder');
      }
    });
    this.modalNews.afterOpen().subscribe(data => {
      this.modalNews.componentInstance.getNewsAll(false);
    });
    this.selectOption(Constants.MODAL_SIDE_LEFT_NEWS);
  }

  openCustody() {
    this.dialog.closeAll();
    const isAuthenticationBroker = this.configService.isAuthenticationBroker();
    if (!isAuthenticationBroker) {
      this.openBrokerAuthenticationModal();
      this.sidebarService._clickOpenTabAuthCallBack.next(3);
    } else {
      if (this.idSelected !== Constants.MODAL_SIDE_LEFT_CUSTODY || !this.modalOpen) {
        this.menuSelected = 3;
        this.modalCustody = this.dialog.open(CustodyConsultComponent, {
          position: {
            left: '182px',
            top: '94.3px'
          },
          height: 'calc(100% - 118.5px)',
          width: 'calc(100% - 209px)',
          maxWidth: 'unset',
          panelClass: 'panel-class-order-component'
        });
        this.modalCustody.afterClosed().subscribe(data => {
          const elem = document.getElementById(Constants.MODAL_SIDE_LEFT_CUSTODY);
          if (elem) {
            elem.className = 'list-item pointer';
          }
          this.modalOpen = false;
          this.menuSelected = 0;
        });
        this.modalCustody.beforeClose().subscribe(data => {
          const sidebar = document.getElementById('sidebar-left');
          if (sidebar) {
            sidebar.classList.remove('modalOrder');
          }
        });
        this.selectOption(Constants.MODAL_SIDE_LEFT_CUSTODY);
      }
    }
  }

  logoutBrokerHB() {
    const userBrokerLogged = this.configService.getBrokerAccount();
    if (userBrokerLogged) {
      this.authenticationService.brokerServiceInternalLogout()
        .subscribe((data: any) => {
          if (this.configService.isLoginTypeWSO2()) {
            if (data) {
              this.dialog.closeAll();
              this.configService.deleteHbBrokerUser();
              this.configService.deleteFavoriteThemeUser();
              this.authenticationService.logout();
              this.configService._authHbLoginType.next(3);
              if (this._configService.isLoginTypeWSO2() && this._configService.isUserAssessorLogged()) {
                window.location.href = this._configService.REDIRECT_LOGOUT + '/assessor/login';
              } else if (this._configService.isLoginTypeWSO2() && !this._configService.isUserAssessorLogged()) {
                window.location.href = this._configService.REDIRECT_LOGOUT + '/client/login';
              } else {
                this.router.navigate(['/login']);
              }
            } else {
              this.configService._authHbLogged.next(false);
              this.snackBar.createSnack('', Constants.BROKER_MSG002, Constants.ALERT_TYPE.error);
            }
          } else {
            if (data && data.isAuthenticated === 'N' && data.text === 'Logout has been successfull.') {
              this.configService.deleteHbBrokerUser();
              this.configService._authHbLoginType.next(Number(Constants.COOKIE_HB_TYPE_OFFILINE));
              this.dailyOrderService.unsubscribeDailyOrder(this.accountHbLogged);
              this.configService._authHbLogged.next(false);
              this.selectedAccount = '';
              if (!this.sessionExpired) {
                this.notificationService.warn('', 'Usuário desconectado da corretora.');
              }
              if (this.hbLoginTypeEnable === Constants.LOGIN_O || this.sessionExpired) {
                this.dialog.closeAll();
                this.authenticationService.logout();
                this.configService.deleteHbBrokerUser();
                this.configService.deleteFavoriteThemeUser();
                this.closeAllModal();
                this.router.navigate(['error'], {
                  queryParams: new ErrorMessage(Constants.ERROR_EXPIRED_ICON, Constants.ERROR_EXPIRED_TITLE, Constants.ERROR_EXPIRED_TEXT,
                    Constants.ERROR_EXPIRED_BUTTON, 'login')
                });
              }
            } else {
              this.configService._authHbLogged.next(false);
              this.snackBar.createSnack(this.msgPlaceholder, Constants.BROKER_MSG002, Constants.ALERT_TYPE.error);
            }
          }
          this.sessionExpired = false;
          this.configService._loggoutSessionExpired.next(false);
        },
          error => {
            this.configService._authHbLogged.next(false);
            this.snackBar.createSnack(this.msgPlaceholder, Constants.BROKER_MSG002, Constants.ALERT_TYPE.error);
            this.sessionExpired = false;
            this.configService._loggoutSessionExpired.next(false);
          });
    } else {
      this.dialog.closeAll();
      this.authenticationService.logout();
      this.configService.deleteHbBrokerUser();
      this.configService.deleteFavoriteThemeUser();
      this.sessionExpired = false;
      this.configService._loggoutSessionExpired.next(false);
      if (this.configService.isLoginTypeWSO2()) {
        if (this.configService.isUserAssessorLogged()) {
          this.router.navigate(['error'], {
            queryParams: new ErrorMessage(Constants.ERROR_EXPIRED_ICON, Constants.ERROR_EXPIRED_TITLE, Constants.ERROR_EXPIRED_TEXT,
              Constants.ERROR_EXPIRED_BUTTON, 'portal/assessor/login')
          });
        } else {
          this.router.navigate(['error'], {
            queryParams: new ErrorMessage(Constants.ERROR_EXPIRED_ICON, Constants.ERROR_EXPIRED_TITLE, Constants.ERROR_EXPIRED_TEXT,
              Constants.ERROR_EXPIRED_BUTTON, 'portal/client/login')
          });
        }
      } else {
        this.router.navigate(['error'], {
          queryParams: new ErrorMessage(Constants.ERROR_EXPIRED_ICON, Constants.ERROR_EXPIRED_TITLE, Constants.ERROR_EXPIRED_TEXT,
            Constants.ERROR_EXPIRED_BUTTON, 'login')
        });
      }
    }
  }

  closeAllModal() {
    if (this.modalOrders) {
      this.modalOrders.close();
    }
    if (this.modalTradingView) {
      this.modalTradingView.close();
    }
    if (this.modalNews) {
      this.modalNews.close();
    }
    if (this.modalCustody) {
      this.modalCustody.close();
    }
    if (this.modalBalance) {
      this.modalBalance.close();
      const sidebar = document.getElementById('sidebar-left');
      sidebar.classList.remove('modalBalance');
    }
    if (this.modalAssessorData) {
      this.modalAssessorData.close();
    }
    if (this.modalAssessorSearch) {
      this.modalAssessorData.close();
    }
    this.configService._modalBuySellClosed.next(true);
  }

  openMyBalance() {
    this.dialog.closeAll();
    const isAuthenticationBroker = this.configService.isAuthenticationBroker();
    if (!isAuthenticationBroker) {
      this.openBrokerAuthenticationModal();
      this.sidebarService._clickOpenTabAuthCallBack.next(4);
    } else {
      if (this.idSelected !== 'balance' || !this.modalOpen) {
        this.menuSelected = 4;
        this.modalBalance = this.dialog.open(BalanceConsultComponent, {
          position: {
            left: '182px',
            top: '94.3px'
          },
          height: 'calc(100% - 118.5px)',
          width: 'calc(100% - 209px)',
          maxWidth: 'unset',
          panelClass: 'panel-class-order-component'
        });
        this.modalOpen = true;
      }
      this.modalBalance.afterClosed().subscribe(data => {
        const elem = document.getElementById('balance');
        if (elem) {
          elem.className = 'list-item pointer';
        }
        this.modalOpen = false;
        this.menuSelected = 0;
      });
      this.modalBalance.beforeClose().subscribe(data => {
        const sidebar = document.getElementById('sidebar-left');
        if (sidebar) {
          sidebar.classList.remove('modalOrder');
        }
      });
      this.selectOption('balance');
    }
  }

  changeAccount() {
    if (this.selectedAccount === 'sair' || this.sessionExpired) {
      if (this.hbLoginTypeEnable === Constants.LOGIN_O || this.configService.isLoginTypeWSO2()) {
        this.logoutBrokerHB();
        this.accountOptions = [];
      } else {
        this.logoutBrokerHB();
        this.configService.deleteHbBrokerUser();
        this.accountOptions = [];
      }
      if (this.subscriptionAccountList) {
        this.subscriptionAccountList.unsubscribe();
        this.subscriptionAccountList = null;
      }
    } else {
      this.configService.setBrokerAccountSelected(this.selectedAccount);
      this.configService.setBrokerAccountSelectedTransition(this.selectedAccount);
      this.configService.writeLog('SIDEBAR > changeAccount: ' + this.selectedAccount);
    }
  }

  openWhatsappWindow() {
    if (this.whithelabel === 1 || this.whithelabel === 4) {
      window.open(Constants.SIDEBAR_HELP_WHATSAPP_LINK + this.helpPhone1);
    }
  }

  openAssessorSearch() {
    this.dialog.closeAll();
    const isAuthenticationBroker = this.configService.isAuthenticationBroker();
    if (!isAuthenticationBroker) {
      this.openBrokerAuthenticationModal();
      this.sidebarService._clickOpenTabAuthCallBack.next(4);
    } else {
      if (!this.modalOpen) {
        this.menuSelected = 1;
        this.modalAssessorSearch = this.dialog.open(AssessorSearchClientComponent, {
          position: {
            left: '182px',
            top: '94.3px'
          },
          height: 'calc(100% - 118.5px)',
          width: 'calc(100% - 209px)',
          maxWidth: 'unset',
          panelClass: 'panel-class-order-component',
          disableClose: true
        });
        this.modalOpen = true;
      }
      this.modalAssessorSearch.afterClosed().subscribe(data => {
        const elem = document.getElementById('assessorData');
        if (elem) {
          elem.className = 'list-item pointer';
        }
        this.modalOpen = false;
        this.menuSelected = 0;
      });
      this.modalAssessorSearch.beforeClose().subscribe(data => {
        const sidebar = document.getElementById('sidebar-left');
        if (sidebar) {
          sidebar.classList.remove('modalOrder');
        }
      });
      this.selectOption('assessorData');
    }
  }

  openBrokerage() {
    this.dialog.closeAll();
    const isAuthenticationBroker = this.configService.isAuthenticationBroker();
    if (!isAuthenticationBroker) {
      this.openBrokerAuthenticationModal();
      this.sidebarService._clickOpenTabAuthCallBack.next(9);
    } else {
      if (this.idSelected !== Constants.MODAL_SIDE_LEFT_BROKERAGE || !this.modalOpen) {
        this.menuSelected = 9;
        this.modalBrokerage = this.dialog.open(BrokerageNoteComponent, {
          position: {
            left: '182px',
            top: '94.3px'
          },
          height: 'calc(100% - 118.5px)',
          width: 'calc(100% - 209px)',
          maxWidth: 'unset',
          panelClass: 'panel-class-order-component'
        });
        this.modalBrokerage.afterClosed().subscribe(data => {
          const elem = document.getElementById(Constants.MODAL_SIDE_LEFT_BROKERAGE);
          if (elem) {
            elem.className = 'list-item pointer';
          }
          this.modalOpen = false;
          this.menuSelected = 0;
        });
        this.modalBrokerage.beforeClose().subscribe(data => {
          const sidebar = document.getElementById('sidebar-left');
          if (sidebar) {
            sidebar.classList.remove('modalOrder');
          }
        });
        this.selectOption(Constants.MODAL_SIDE_LEFT_BROKERAGE);
      }
    }
  }

  openExtract() {
    this.dialog.closeAll();
    const isAuthenticationBroker = this.configService.isAuthenticationBroker();
    if (!isAuthenticationBroker) {
      this.openBrokerAuthenticationModal();
      this.sidebarService._clickOpenTabAuthCallBack.next(9);
    } else {
      if (this.idSelected !== Constants.MODAL_SIDE_LEFT_EXTRACT || !this.modalOpen) {
        this.menuSelected = 10;
        this.modalExtract = this.dialog.open(ExtractFinancialComponent, {
          position: {
            left: '182px',
            top: '94.3px'
          },
          height: 'calc(100% - 118.5px)',
          width: 'calc(100% - 209px)',
          maxWidth: 'unset',
          panelClass: 'panel-class-order-component'
        });
        this.modalExtract.afterClosed().subscribe(data => {
          const elem = document.getElementById(Constants.MODAL_SIDE_LEFT_EXTRACT);
          if (elem) {
            elem.className = 'list-item pointer';
          }
          this.modalOpen = false;
          this.menuSelected = 0;
        });
        this.modalExtract.beforeClose().subscribe(data => {
          const sidebar = document.getElementById('sidebar-left');
          if (sidebar) {
            sidebar.classList.remove('modalOrder');
          }
        });
        this.selectOption(Constants.MODAL_SIDE_LEFT_EXTRACT);
      }
    }
  }

  openRanking() {
    window.open('https://data.fastmarkets.com.br/ranking/desafiodeinvestimentos');
    // this.dialog.closeAll();
    // const isAuthenticationBroker = this.configService.isAuthenticationBroker();
    // if (!isAuthenticationBroker) {
    //   this.openBrokerAuthenticationModal();
    //   this.sidebarService._clickOpenTabAuthCallBack.next(9);
    // } else {
    //   if (this.idSelected !== Constants.MODAL_SIDE_LEFT_RANKING || !this.modalOpen) {
    //     this.menuSelected = 11;
    //     this.modalRanking = this.dialog.open(RankingComponent, {
    //       position: {
    //         left: '182px',
    //         top: '94.3px'
    //       },
    //       height: 'calc(100% - 118.5px)',
    //       width: 'calc(100% - 209px)',
    //       maxWidth: 'unset',
    //       panelClass: 'panel-class-order-component'
    //     });
    //     this.modalRanking.afterClosed().subscribe(data => {
    //       const elem = document.getElementById(Constants.MODAL_SIDE_LEFT_RANKING);
    //       if (elem) {
    //         elem.className = 'list-item pointer';
    //       }
    //       this.modalOpen = false;
    //       this.menuSelected = 0;
    //     });
    //     this.modalRanking.beforeClose().subscribe(data => {
    //       const sidebar = document.getElementById('sidebar-left');
    //       if (sidebar) {
    //         sidebar.classList.remove('modalOrder');
    //       }
    //     });
    //     this.selectOption(Constants.MODAL_SIDE_LEFT_RANKING);
    //   }
    //   }
  }

  openEducational() {
    window.open('http://promo.fastmarkets.com.br/desafio-de-investimentos-tutoriais');
  }

  // TODO: Modal Dados Cadastrais
  // openAssessorData() {
  //   this.dialog.closeAll();
  //   const isAuthenticationBroker = this.configService.isAuthenticationBroker();
  //   if (!isAuthenticationBroker) {
  //     this.openBrokerAuthenticationModal();
  //     this.sidebarService._clickOpenTabAuthCallBack.next(4);
  //   } else {
  //     if (this.idSelected !== 'assessorData' || !this.modalOpen) {
  //       this.menuSelected = 9;
  //       this.modalAssessorData = this.dialog.open(AssessorDataComponent, {
  //         position: {
  //           left: '182px',
  //           top: '94.3px'
  //         },
  //         height: 'calc(100% - 118.5px)',
  //         width: 'calc(100% - 209px)',
  //         maxWidth: 'unset',
  //         panelClass: 'panel-class-order-component'
  //       });
  //       this.modalOpen = true;
  //     }
  //     this.modalAssessorData.afterClosed().subscribe(data => {
  //       const elem = document.getElementById('assessorData');
  //       if (elem) {
  //         elem.className = 'list-item pointer';
  //       }
  //       this.modalOpen = false;
  //       this.menuSelected = 0;
  //     });
  //     this.modalAssessorData.beforeClose().subscribe(data => {
  //       const sidebar = document.getElementById('sidebar-left');
  //       if (sidebar) {
  //         sidebar.classList.remove('modalOrder');
  //       }
  //     });
  //     this.selectOption('assessorData');
  //   }
  // }

  // TODO: Assina os metodos
  // startSubscribe(selectedAccount) {
  //   this.authHbLoggedBroker = this.configService.authHbLogged$
  //     .subscribe(x => {
  //       if (x) {
  //         if (this.selectedAccount) {
  //           // TODO: busca os outros metodos da sidebar
  //           this.getHistoryDailyOrder();
  //           this.getBalanceValue();
  //           this.getBmfOrders();
  //           this.getBovespaOrders();
  //         }
  //       } else {
  //         if (this.subscriptionDailyOrder) {
  //           this.subscriptionDailyOrder.unsubscribe();
  //           this.dailyOrderService.unsubscribeDailyOrder(this.accountHbLogged);
  //           // TODO: limpar as outras variaveis da sidebar
  //           this.quantityOrder = 0;
  //           this.balance = 'R$ 0,00';
  //         }
  //       }
  //     });
  // }

  // fillOrders() {
  //   // TODO: remove:  this.loadOrders();
  //   // TODO: remove: this.dailyOrderService.subscribeDailyOrderBovespa(this.account);
  //   // TODO: remove: this.dailyOrderService.subscribeDailyOrderBmf(this.account);
  // }

  // loadOrders() {
  //   const isAuthenticationBroker = this.configService.isAuthenticationBroker();
  //   if (!isAuthenticationBroker) {
  //     this.openBrokerAuthenticationModal();
  //     this.sidebarService._clickOpenTabAuthCallBack.next(1);

  //     return false;
  //   } else {
  //     this.subscriptionDailyOrder = this.dailyOrderService.messagesDailyOrder
  //       .subscribe((msg: MessageDailyOrder[]) => {
  //         if (msg !== undefined && msg.length > 0) {
  //           if (this.rowAllOrdersData === undefined) {
  //             this.rowAllOrdersData = [];
  //           }
  //           for (let i = 0; i < msg.length; i++) {
  //             this.rowAllOrdersData = this.rowAllOrdersData.filter(
  //               x => x !== msg[i].orderID
  //             );
  //             this.rowAllOrdersData.push(msg[i].orderID);
  //           }
  //           this._authServer.iNegotiation.setQuantityOpenOrder(this.rowAllOrdersData.length);
  //         }
  //       });
  //   }
  // }

  // getBalanceValue() {
  //   this.subscriptionMyBalance =
  //     this._authServer.iNegotiation.financialAccountInformationCompl(this.accountHbLogged, Constants.MARKET_BOVESPA)
  //       .subscribe(resp => setTimeout(() => {
  //         if (resp && resp.length > 0) {
  //           this.balance = AppUtils.convertToCurrencyString(resp[0].availableBalance.value);
  //           this.balance = 'R$' + this.balance;
  //         }
  //       }));
  // }

  // getBmfOrders() {
  //   this.subscriptionCustodyBmf = this._authServer.iNegotiation.positionBmf(this.accountHbLogged, null)
  //     .subscribe(msgBmf => {
  //       if (msgBmf !== null && msgBmf.custodyBmf) {
  //         this.bmfOrders = msgBmf.custodyBmf.length;
  //         this.sumCustodyOrders();
  //       }
  //     });
  // }

  // getBovespaOrders() {
  //   this.subscriptionCustodyBov = this._authServer.iNegotiation.positionBovespa(this.accountHbLogged, null)
  //     .subscribe(msgBov => {
  //       if (msgBov !== null && msgBov.custodyBovespa) {
  //         this.bovOrders = msgBov.custodyBovespa.length;
  //         this.sumCustodyOrders();
  //       }
  //     });
  // }

  // sumCustodyOrders() {
  //   this.custodyOrders = this.bmfOrders + this.bovOrders;
  // }

  // getHistoryDailyOrder() {
  //   this.dailyOrderService.subscribeDailyOrderBovespa(this.accountHbLogged);
  //   this.dailyOrderService.subscribeDailyOrderBmf(this.accountHbLogged);

  //   this.subscriptionDailyOrder = this.dailyOrderService.messagesDailyOrder
  //     .subscribe(x => {
  //       if (x != undefined && x.length > 0) {
  //         x = x.filter(element => Constants.ORDER_HISTORY_STATUS[1].generic.includes(element.state));
  //         x = this.historyOrderPresenter.transformDailyOrder(x);
  //         for (let i = 0; i < x.length; i++) {
  //           this.currentDailyList = this.currentDailyList.filter(
  //             element => element.orderID != x[i].orderID
  //           );
  //           this.currentDailyList.push(x[i]);
  //           this.quantityOrder = this.currentDailyList.length;
  //         }
  //       }
  //     });
  // }

}
