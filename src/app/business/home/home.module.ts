import { NgModule } from '@angular/core';
import { HomeComponent } from '@business/home/home.component';
import { SideBarLeftComponent } from '@business/home/side-bar-left/side-bar-left.component';
import { HeaderBarComponent } from '@business/home/header-bar/header-bar.component';
import { SharedModule } from '@shared/shared.module';
import { WorkspaceNewComponent } from '@business/home/workspace-new/workspace-new.component';
import { FooterComponent } from '@business/home/footer/footer.component';
import { HomeRoutingModule } from '@business/home/home.routing.module';

@NgModule({
  imports: [
    HomeRoutingModule,
    SharedModule,
  ],
  declarations: [
    HomeComponent,
    SideBarLeftComponent,
    HeaderBarComponent,
    WorkspaceNewComponent,
    FooterComponent
  ],
  exports: [
    HomeComponent
  ]
})
export class HomeModule { }
