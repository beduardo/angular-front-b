import { SidebarService } from '@services/sidebar.service';
import { ConfigService } from '@services/webFeeder/config.service';
import { Component, OnInit } from '@angular/core';
import { BilletOrderModalService } from '@services/billet-order-modal.service';
import { MatDialog } from '@angular/material';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
})
export class FooterComponent implements OnInit {

  dialog: MatDialog;
  clickOpenTabOrder: Subscription;
  billetTypeTemp: any;
  quoteTemp: any;

  constructor(
    private _billetOrderService: BilletOrderModalService,
  ) { }

  ngOnInit() {
  }

  chamaBoleta(billetType?: string, quote?: string) {
    this._billetOrderService.buySellOrder(billetType, quote);
  }

  flagVerify() {
    return (this._billetOrderService.flagVerify());
  }

}
