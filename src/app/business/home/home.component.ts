import { SocketService } from '@services/webFeeder/socket.service';
import { Constants } from './../../utils/constants';
import { Component, HostListener, OnDestroy, OnInit, ViewEncapsulation } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { MinResolutionModalComponent } from '@component/min-resolution-modal/min-resolution-modal.component';
import { WorkspaceConfModalComponent } from '@component/workspace-conf-modal/workspace-conf-modal.component';
import { Subscription } from 'rxjs';
import { AutoLogoutService } from '@auth/auto-logout-service.service';
import { AppUtils } from '@utils/app.utils';
import { ConfigService } from '@services/webFeeder/config.service';
import { environment } from 'environments/environment';
import { AuthenticationService } from '@services/authentication.service';
import { WorkspaceNewService } from '@services/workspace-new.service';
import { WorkspaceItemContentDto } from '@dto/workspace-item-content-dto';
import { WorkspaceNewSaveDto } from '@dto/workspace-new-save-dto';
import { PositionDto } from '@dto/position.dto';
import { StatusOmsService } from '@services/webFeeder/sckt-status-oms.service';
import { StatusDifusionService } from '@services/webFeeder/sckt-status-difusion.service';
import { StatusConnection } from '@model/status-connection';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  encapsulation: ViewEncapsulation.None,
  // tslint:disable-next-line:use-host-property-decorator
  host: {
    '(document:keydown)': 'onActivedWindow($event)',
    '(document:click)': 'onActivedWindow($event)'
  }
})
export class HomeComponent implements OnInit, OnDestroy {
  public subscription: Subscription;
  dialog: MatDialog;
  resizeModalShown: boolean;
  modal: MatDialogRef<MinResolutionModalComponent>;
  private subscriptionQuote: Subscription;
  public loadingWorkspace = true;
  @HostListener('mousemove')
  onMouseMove() {
    this.autoLogoutService.resetTime();
  }

  @HostListener('click')
  onClick() {
    this.autoLogoutService.resetTime();
  }

  constructor(
    dialog: MatDialog,
    private acroute: ActivatedRoute,
    private autoLogoutService: AutoLogoutService,
    private configService: ConfigService,
    private authSvc: AuthenticationService,
    private socketService: SocketService,
    private workspaceService: WorkspaceNewService,
    private router: Router,
    private statusOmsService: StatusOmsService,
    private statusDifusionService : StatusDifusionService
  ) {
    this.dialog = dialog;
    this.resizeModalShown = false;
    const observeStatusWS = this.socketService.statusWebSocket$.subscribe(
      status => {
        if(status === StatusConnection.SUCCESS || status === StatusConnection.SUCCESS_RECONNECT){
          this.statusOmsService.subscribeOMS();
          this.statusDifusionService.subscribeDifusion();
        }
      }
    )
  }

  ngOnInit() {
    this.socketService.reconectSocket$.subscribe(x => {
      if (x) {
        this.subscription = this.acroute.queryParams
          .filter(param => param['existsUserWorkspace'])
          .subscribe((param: any) => {
            if (param['existsUserWorkspace'] !== 'undefined' && param['existsUserWorkspace'] === 'false') {
              this.addWorkspace();
            }
          });
        setTimeout(() => {
          this.loadingWorkspace = false;
        }, 1000);
      }
    });
  }

  addWorkspace() {
    if (Number(this.configService.getWhiteLabel()) === 10) {
      this.addWorkspaceRanking();
    } else {
      Promise.resolve().then(() => {
        const dialogRef = this.dialog.open(WorkspaceConfModalComponent, {
          panelClass: 'workspace-conf-modal',
          disableClose: true
        });
      });
    }
  }

  addWorkspaceRanking() {
    this.workspaceService.loadWorkspaces(this.configService.getUserWorkspacesPreDefineds(), true)
      .subscribe(response => {
        if (response) {
          const indexTabNew = 0;
          const wksOptSel = response[1];
          const content: Array<WorkspaceItemContentDto> = this.convertDtoContenToSave(wksOptSel.JsonData);
          const wksDtoSave: WorkspaceNewSaveDto = new WorkspaceNewSaveDto('DayTrade', content, indexTabNew);
          this.workspaceService.saveWorkspace(wksDtoSave).subscribe(data => {
            if (data) {
              this.router.navigate([Constants.HOME_WKS_PATH], { queryParams: { workspaceId: data.Id } });
            }
          });
        }
      });
  }

  private convertDtoContenToSave(jsondata: any): Array<WorkspaceItemContentDto> {
    const contentFrom = JSON.parse(jsondata).content;
    const contentTo: Array<WorkspaceItemContentDto> = [];
    for (let k = 0; k < contentFrom.length; k++) {
      const objItem: WorkspaceItemContentDto = new WorkspaceItemContentDto(k, contentFrom[k].symbol,
        contentFrom[k].company, contentFrom[k].type, contentFrom[k].position as PositionDto,
        contentFrom[k].showInGridster || false, contentFrom[k].workspace);
      contentTo.push(objItem);
    }
    return contentTo;
  }

  ngOnDestroy(): void {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
    if (this.subscriptionQuote) {
      this.subscriptionQuote.unsubscribe();
    }
  }

  onActivedWindow() {
    if (this.configService.getLoginTypeEnabled() === Constants.LOGIN_TYPE.LoginW) {
      if (!this.authSvc.iLogin.isTokenExpired()) {
        const diff = AppUtils.dateDiff(AppUtils.strToDateTime(this.configService.getTokenLastRefreshed()), new Date(), 'minutes') as number;
        const firstnameTemp: any = this.configService.getHbCurrentUser();
        if (diff >= environment.MIN_REFRESH_WSO2TOKEN_INTERVAL && firstnameTemp) {
          this.authSvc.iLogin.refresh().subscribe(data => {
            this.configService.setTokenApi(data.access_token);
            this.configService.setTokenRefreshApi(data.refresh_token);
            this.configService.setScope(data.scope);
            this.configService.setTokenTp(data.token_type);
            this.configService.setTokenExpiration(AppUtils.getDateTimeLocaleFormat(new Date((new Date()).getTime()
              + (data.expires_in * 1000))));
          },
            () => {
              // this.snackBar.createSnack(this.parent, ErrorUtils.resolveCAError(error), Constants.ALERT_TYPE.error);
            });
        }
      } else {
        this.authSvc.logout();
      }
    }
  }

}
