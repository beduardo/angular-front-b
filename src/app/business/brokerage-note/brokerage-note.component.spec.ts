import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BrokerageNoteComponent } from './brokerage-note.component';
import { MatIconModule, MatDatepickerModule, MatFormFieldModule, MatSelectModule, MatOptionModule, MatProgressSpinnerModule, MatCardModule, MatDialogRef, MatNativeDateModule, MatInputModule } from '@angular/material';
import { SpinnerComponent } from '@component/spinner/spinner.component';
import { FormsModule } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { ConfigService } from '@services/webFeeder/config.service';
import { ConfigServiceMock, CustomSnackbarServiceMock } from '@services/_test-mock-services/_mock-services';
import { HttpModule } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';
import { DatePipe } from '@angular/common';
import { NotificationsService, SimpleNotificationsModule } from 'angular2-notifications';
import { CustomSnackbarService } from '@services/custom-snackbar-service.service';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

describe('BrokerageNoteComponent', () => {
  let component: BrokerageNoteComponent;
  let fixture: ComponentFixture<BrokerageNoteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        BrokerageNoteComponent,
        SpinnerComponent
      ],
      imports: [
        MatIconModule,
        MatDatepickerModule,
        MatFormFieldModule,
        MatInputModule,
        MatSelectModule,
        MatOptionModule,
        MatProgressSpinnerModule,
        FormsModule,
        MatCardModule,
        RouterTestingModule,
        HttpModule,
        HttpClientModule,
        SimpleNotificationsModule.forRoot(),
        MatDatepickerModule,
        MatNativeDateModule,
        BrowserAnimationsModule
      ],
      providers: [
        DatePipe,
        NotificationsService,
        { provide: MatDialogRef, useValue: {} },
        { provide: ConfigService, useClass: ConfigServiceMock },
        { provide: CustomSnackbarService, useClass: CustomSnackbarServiceMock },
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BrokerageNoteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
