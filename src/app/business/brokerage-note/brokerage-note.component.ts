import { ConfigService } from '@services/webFeeder/config.service';
import { Component, OnInit, OnDestroy, HostListener } from '@angular/core';
import { MatDialogRef } from '@angular/material';
import { Subject, Subscription } from 'rxjs';
import { NgxMasonryOptions } from 'ngx-masonry';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';
import { AuthServer } from '@services/auth-server.service';
import { NotificationsService } from 'angular2-notifications';
import { Constants } from '@utils/constants';
import { AutoLogoutService } from '@auth/auto-logout-service.service';
import { StatusOmsService } from '@services/webFeeder/sckt-status-oms.service';
import { StatusConnection } from '@model/status-connection';

@Component({
  selector: 'app-brokerage-note',
  templateUrl: './brokerage-note.component.html'
})
export class BrokerageNoteComponent implements OnInit, OnDestroy {

  public title = 'NOTAS DE CORRETAGEM';
  public loading = false;
  public dateStartFilter: Date;
  public dateEndFilter: Date;
  public changingValue: Subject<boolean> = new Subject();
  public myOptions: NgxMasonryOptions = {
    originLeft: true,
    transitionDuration: '1.17s',
  };
  public urlPdf: SafeResourceUrl;
  public account: string;
  public market: string;
  public dataFilter: any;
  public listMarket = [
    { value: 'XBSP', desc: 'Bovespa' },
    { value: 'XBMF', desc: 'BMF' }
  ];
  private subscriptionStatusOms: Subscription;
  public statusOmsOnline: boolean;
  public msgStatusOmsOffile = Constants.MSG_STATUS_OFFLINE_OMS;

  @HostListener('mousemove')
  onMouseMove() {
    this._autoLogoutService.resetTime();
  }

  @HostListener('click')
  onClick() {
    this._autoLogoutService.resetTime();
  }

  // @HostListener('document:keydown', ['$event'])
  // onkeydown(event) {
  //   if (event.key == 'Escape') {
  //     if ((document.getElementsByTagName('ngb-modal-window').length == 0)) {
  //       this.close();
  //     }
  //   }
  // }

  constructor(
    public dialogRef: MatDialogRef<BrokerageNoteComponent>,
    private sanitizer: DomSanitizer,
    private _configService: ConfigService,
    private _authServer: AuthServer,
    private _notificationService: NotificationsService,
    private _autoLogoutService: AutoLogoutService,
    private _statusOmsService: StatusOmsService
  ) {
    this.account = this._configService.getBrokerAccountSelected();
    this.dataFilter = new Date();
    this.market = this.listMarket[0].value;
    this.subscriptionStatusOms = this._statusOmsService.messagesStatusOms
      .subscribe(statusOms => {
        if (statusOms === StatusConnection.ERROR) {
          this.loading = false;
          this.statusOmsOnline = false;
        } else {
          this.statusOmsOnline = true;
        }
      });
  }

  ngOnInit() {
  }

  ngOnDestroy() {
    this.changingValue.unsubscribe();
    if (this.subscriptionStatusOms) {
      this.subscriptionStatusOms.unsubscribe();
    }
  }

  close() {
    this.dialogRef.close();
  }

  changeDateIni(value) {
    if (value) {
      this.dateStartFilter = new Date(value);
      this.dataFilter = new Date(value);
    }
  }

  changeDateFim(value) {
    if (value) {
      this.dateEndFilter = new Date(value);
    }
  }

  clearFilter() {
    this.changingValue.next(true);
    this.dateStartFilter = null;
    this.dateEndFilter = null;
  }

  searchFilter() {
    if (!this.account || !this.market || !this.dataFilter) {
      this._notificationService.error('', 'Paramêtros incorretos.');
      return;
    }
    this.loading = true;
    this._authServer.iNegotiation.brokeragePDF(this.account, this.market, this.dataFilter)
      .subscribe(res => {
        if (res && res.size > 0) {
          this.urlPdf = this.sanitizer.bypassSecurityTrustResourceUrl(URL.createObjectURL(res));
        } else {
          this._notificationService.error('', 'Nota de corretagem não encontrada.');
        }
        this.loading = false;
      }, error => {
        this._notificationService.error('', Constants.BROKER_MSG002);
        this.loading = false;
      });
  }

}
