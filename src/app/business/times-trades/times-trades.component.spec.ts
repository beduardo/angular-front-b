import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TimesTradesComponent } from '@business/times-trades/times-trades.component';

describe('TimesTradesComponent', () => {
  let component: TimesTradesComponent;
  let fixture: ComponentFixture<TimesTradesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TimesTradesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TimesTradesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
