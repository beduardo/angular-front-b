import { DatePipe } from '@angular/common';
import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import { ActivatedRoute } from '@angular/router';
import { AbstractItem } from '@business/base/abstract-item';
import { AbstractItemService } from '@business/base/abstract-item.service';
import { MessageQuote } from '@model/webFeeder/message-quote.model';
import { BusinessBookService } from '@services/webFeeder/sckt-business-book.service';
import { ConfigService } from '@services/webFeeder/config.service';
import { QuoteService } from '@services/webFeeder/sckt-quotes.service';
import { TokenService } from '@services/webFeeder/token.service';
import { WorkspaceNewService } from '@services/workspace-new.service';
import { AppUtils } from '@utils/app.utils';
import { Constants } from '@utils/constants';
import { NotificationsService } from 'angular2-notifications';
import { Subscription } from 'rxjs';
import { AuthServer } from '@services/auth-server.service';

@Component({
  selector: Constants.WINDOW_NAME_TIMES_TRADES,
  templateUrl: './times-trades.component.html'
})
export class TimesTradesComponent extends AbstractItem implements OnInit, OnDestroy { // DoCheck, OnChanges

  public symbol = '';
  public tickSize = 2;
  public rowOffersData: any[];
  public rowPriceData: any[];
  public imgName: string;
  public newWindow = false;
  private subscriptionBusinessBook: Subscription;
  private subscriptionQuote: Subscription;
  public messageQuote = new MessageQuote();
  // public classPressure: string;
  public classRowCenter: string;
  public windowActive: any;
  dialog: MatDialog;
  protected workspaceService: WorkspaceNewService;
  public style = 0;
  public last;
  public loading: boolean;
  pressureBarPercentageLeft: string;
  pressureBarPercentageRight: string;
  highOptimized = '';
  lowOptimized = '';
  quantityOptimized = '';
  public indiceFuture: boolean;
  public closedMarketmsg = Constants.CLOSED_MARKET_MESSAGE;
  public msgEmpty = Constants.EMPTY_MESSAGE;
  public msgNoData = Constants.WORKSPACE_MSG018;
  public marketClosed = false;

  public hiddenInvalidActive: boolean;
  public msgInvalidAsset = Constants.WORKSPACE_MSG007;
  public company: string;

  @Input() data: any;
  public tooltiphover: boolean;
  public isSubscribeBusinessBook: boolean;
  public showEmpty: boolean;
  public auctionHammer: boolean;
  public showVariation: boolean;
  public isUpdateOnDemand: boolean;

  constructor(
    private businessBookService: BusinessBookService,
    private quoteService: QuoteService,
    private route: ActivatedRoute,
    workspaceService: WorkspaceNewService,
    itemService: AbstractItemService,
    dialog: MatDialog,
    public _NotificationService: NotificationsService,
    private _authServer: AuthServer,
    private datePipe: DatePipe
  ) {
    super(workspaceService, dialog, itemService);
    this.workspaceService = workspaceService;
    // this.classPressure = 'linear-gradient(to right, #ffb300 40%, #78bd36 60%)';
    this.dialog = dialog;
    this.tooltiphover = false;
    this.indiceFuture = false;
  }

  ngOnInit() {
    if (this.data && this.data.symbol) {
      this.symbol = this.data.symbol;
      this.setQuote(this.data.symbol);
    } else {
      this.newWindow = true;
      this.route.queryParams.subscribe(params => {
        if (params['symbol']) {
          this.symbol = params['symbol'];
          setTimeout(() => {
            this.setQuote(this.symbol);
          }, 2000);
        }
      });
    }
  }

  ngOnDestroy() {
    this.closeSheet();
    if (this.symbol !== '') {
      if (this.businessBookService) {
        this.businessBookService.unsubscribeBook(this.symbol);
      }
    }
    if (this.symbol !== '') {
      if (this.quoteService) {
        this.quoteService.unsubscribeQuote(this.symbol, Constants.WINDOW_NAME_TIMES_TRADES);
      }
    }
    if (this.subscriptionBusinessBook) {
      this.subscriptionBusinessBook.unsubscribe();
    }
    if (this.subscriptionQuote) {
      this.subscriptionQuote.unsubscribe();
    }
  }

  setQuote(symbol: string) {
    symbol = AppUtils.trimStart(symbol);
    symbol = symbol.toLowerCase();
    const oldSymbolTemp = this.symbol;

    if (symbol.toLowerCase() === 'invalid') {
      // this.imgName = '';
      this.symbol = '';
      this.hiddenInvalidActive = true;
      return;
    }
    if (this.symbol && symbol && this.subscriptionQuote !== undefined
      && this.symbol.toLowerCase() === symbol.toLowerCase()) {
      return;
    }
    if (this.symbol && symbol && this.symbol !== '' && this.symbol.toLowerCase() !== symbol) {
      if (this.data && super.validateOnlyOneAssetAssigned(symbol, this.data.type, this.data.workspaceId) === true) {
        this._NotificationService.error('Erro', Constants.WORKSPACE_MSG015);
        this.symbol = '';
        super.updateOnDemand(oldSymbolTemp, this.symbol, true);
        setTimeout(() => {
          this.symbol = oldSymbolTemp;
        }, 0);
        return;
      }

      this.quoteService.unsubscribeQuote(this.symbol, Constants.WINDOW_NAME_TIMES_TRADES);
      if (this.subscriptionQuote) {
        this.subscriptionQuote.unsubscribe();
      }
    }
    this.messageQuote = new MessageQuote();
    this.rowOffersData = [];
    this.last = 0;
    this.loading = true;
    const oldSymbol = this.symbol;
    this.symbol = symbol;
    // if (this.data) {
    //   this.data.symbol = this.symbol;
    //   super.updateOnDemand(oldSymbol, this.symbol);
    // }

    // if (symbol.length < 5) {
    //   return;
    // }
    this.quoteService.subscribeQuote(symbol, Constants.WINDOW_NAME_TIMES_TRADES);
    this.isSubscribeBusinessBook = false;
    this.isUpdateOnDemand = false;
    this.subscriptionQuote = this.quoteService.messagesQuote
      .subscribe((msg: MessageQuote) => {
        if (msg && msg.errorMessage && msg.symbol === this.symbol) {
          this.hiddenInvalidActive = true;
          this.symbol = null;
          this.company = null;
          this.quoteService.unsubscribeQuote(msg.symbol.toLowerCase(), Constants.WINDOW_NAME_TIMES_TRADES);
        } else if (msg !== undefined && msg.symbol != null && msg.symbol === this.symbol) {
          if (!this.isUpdateOnDemand) {
            this.isUpdateOnDemand = true;
            if (this.data) {
              this.data.symbol = this.symbol;
              super.updateOnDemand(oldSymbol, this.symbol);
            }
          }
          if (!this.isSubscribeBusinessBook) {
            this.isSubscribeBusinessBook = true;
            this.subscrobeBusinessBook(msg.symbol, msg.marketType);
          }
          this.hiddenInvalidActive = false;
          this.last = this.messageQuote.lastTrade;
          this.messageQuote = msg;
          if (this.messageQuote.lastTrade && this.messageQuote.lastTrade.toString() === '-') {
            this.messageQuote.lastTrade = this.messageQuote.previous;
          }
          this.quantityOptimized = AppUtils.getNumberOtimized(this.messageQuote.quantity, 0);
          if (this.messageQuote.high) {
            this.highOptimized = this.messageQuote.high.toString();
          }
          if (this.messageQuote.low) {
            this.lowOptimized = this.messageQuote.low.toString();
          }
          if (this.messageQuote.symbol.indexOf('win') === 0 || this.messageQuote.symbol.indexOf('wdo') === 0 ||
            this.messageQuote.symbol.indexOf('dol') === 0 || msg.symbol.toLowerCase().indexOf('ind') === 0) {
            this.indiceFuture = true;
          } else {
            this.indiceFuture = false;
          }
          if (Number(this.messageQuote.status) === 3) {
            this.auctionHammer = true;
          } else {
            this.auctionHammer = false;
          }
          if (this.messageQuote.lastTrade) {
            if (this.messageQuote.lastTrade < this.last) {
              this.style = 2;
            }
            if (this.messageQuote.lastTrade > this.last) {
              this.style = 1;
            }
          }
          if (this.messageQuote.lastTrade && this.messageQuote.lastTrade.toString() === '-') {
            this.showVariation = false;
            this.messageQuote.lastTrade = this.messageQuote.previous;
          }
          if (this.messageQuote.dateLastNegotiation) {
            const today = this.datePipe.transform(new Date(), 'dd/MM/yyyy');
            if (today === this.messageQuote.dateLastNegotiation) {
              this.showVariation = true;
            } else {
              this.showVariation = false;
            }
          } else {
            this.showVariation = true;
          }

          // if (this.messageQuote.errorMessage !== undefined && this.messageQuote.errorMessage !== '') {
          //   this.imgName = '';
          // }
          // this.imgName = '/assets/img/assets/' + AppUtils.getImageSymbol(msg.symbol).toUpperCase() + '.PNG';
          if (msg.description && msg.description !== '' && this.messageQuote.marketType === Constants.MARKET_CODE_BOVESPA) {
            this.company = this.messageQuote.description + ' ' + this.messageQuote.classification;
          } else {
            this.company = this.messageQuote.description;
          }
        }


        this.loading = false;
      });
  }

  private subscrobeBusinessBook(symbol: string, marketType: number) {
    if (this.symbol !== '') {
      this.businessBookService.unsubscribeBook(this.symbol);
      if (this.subscriptionBusinessBook) {
        this.subscriptionBusinessBook.unsubscribe();
      }
    }
    if (symbol && (marketType === 1 || marketType === 3)) {
      this.businessBookService.subscribeBook(symbol);
      this.symbol = symbol;
      this.showEmpty = false;
      this.subscriptionBusinessBook = this.businessBookService.messagesBusinessBook
        .subscribe((msg: any) => {
          if (msg !== undefined && msg.symbol !== undefined && msg.symbol !== null && msg.symbol === this.symbol) {
            this.rowOffersData = [];
            let dateL;
            let aggressor;
            if (msg.quoteTrade.L !== []) {
              for (let i = msg.quoteTrade.L.length - 1; i >= 0; i--) {
                if (msg.quoteTrade.L[i]) {
                  if (msg.quoteTrade.L[i].CDA === 'A') {
                    aggressor = 'C';
                  } else if (msg.quoteTrade.L[i].CDA === 'V') {
                    aggressor = 'V';
                  } else {
                    aggressor = 'D';
                  }
                  if (msg.quoteTrade.L[i]) {
                    dateL = msg.quoteTrade.L[i].T;
                  }
                  this.tickSize = AppUtils.GetTickSize(symbol);
                  const data = {
                    time: new Date(dateL),
                    price: AppUtils.convertNumberToCurrency(msg.quoteTrade.L[i].P, this.tickSize),
                    quantity: AppUtils.getNumberOtimized(msg.quoteTrade.L[i].QT).replace(',', '.'),
                    buy: this._authServer.iQuote.getPlayerName(msg.quoteTrade.L[i].PCB) || msg.quoteTrade.L[i].PCB,
                    buy_short: this._authServer.iQuote.getPlayerName(msg.quoteTrade.L[i].PCB) || msg.quoteTrade.L[i].PCB,
                    buyId: msg.quoteTrade.L[i].PCB,
                    sell: this._authServer.iQuote.getPlayerName(msg.quoteTrade.L[i].PCL) || msg.quoteTrade.L[i].PCL,
                    sell_short: this._authServer.iQuote.getPlayerName(msg.quoteTrade.L[i].PCL) || msg.quoteTrade.L[i].PCL,
                    sellId: msg.quoteTrade.L[i].PCL,
                    going_for_growth: aggressor,
                    class: aggressor === 'C' ? 'buy-aggressor' : aggressor === 'V' ? 'sell-aggressor' : ''
                  };
                  if (data.buy_short.length > 5) {
                    data.buy_short = data.buy_short.substr(0, 4) + '...';
                  }
                  if (data.sell_short.length > 5) {
                    data.sell_short = data.sell_short.substr(0, 4) + '...';
                  }
                  this.rowOffersData.push(data);
                }
              }
            }
          }
        });
    } else {
      this.showEmpty = true;
    }
  }

  selectAsset(event) {
    this.setQuote(event.symbol);
  }

  openNewWindow() {
    const options = 'width=470px, height=500';
    const parameters = '?symbol=' + this.symbol;

    if (this.windowActive == null || this.windowActive.closed) {
      super.getNewWindow('timestrades', parameters, options, this.data.id)
        .subscribe(window => {
          this.windowActive = window;
        });
    } else {
      this.windowActive.focus();
    }
  }

  closeSheet() {
    if (this.windowActive != null) {
      this.windowActive.close();
    }
  }

  mouseEnterBrokerRow() {
    this.tooltiphover = true;

    setTimeout(() => {
      this.mouseLeaveBrokerRow();
    }, 8000);
  }
  mouseLeaveBrokerRow() {
    this.tooltiphover = false;
  }
}
