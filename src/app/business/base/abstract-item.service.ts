import { Injectable } from '@angular/core';
import { BehaviorSubject, Subject, Observable } from 'rxjs';

import { TokenService } from '@services/webFeeder/token.service';
import { ConfigService } from '@services/webFeeder/config.service';
import { LoginWsResponse } from '@model/webFeeder/loginws-response.model';
import { NotificationsService } from 'angular2-notifications';
import { Box } from '@business/home/workspace-new/workspace-new.component';
import { CedroAccountService } from '@services/cedro-account.service';
import { AuthenticationService } from '@services/authentication.service';

@Injectable({
  providedIn: 'root'
})
export class AbstractItemService {

  private data = new BehaviorSubject<Box>(null);
  data$ = this.data.asObservable();

  constructor(
    private tokenService: TokenService,
    private configService: ConfigService,
    private notificationsService: NotificationsService,
    private authSvc: AuthenticationService,
  ) { }

  openNewToken(nameWindow, parameters, options, id): Observable<any> {
    const subject = new Subject<any>();
    if (this.configService.isLoginTypeWSO2()) {
      this.authSvc.iLogin.refresh()
        .subscribe(data => {
          if (data.access_token) {
            parameters += '&token=' + data.access_token;
            const windows = window.open(
              nameWindow + parameters,
              nameWindow.toUpperCase() + id,
              options
            );
            subject.next(windows);
          } else {
            this.notificationsService.error('', 'Não foi possível abrir uma nova janela!');
          }
        }, () => {
          this.notificationsService.error('', 'Não foi possível abrir uma nova janela!');
        });
    } else {
      this.tokenService.loginWS(this.configService.getGuidUser()).subscribe((resp: LoginWsResponse) => {
        if (resp.codeMessage === true) {
          parameters += '&token=' + resp.tokenWS;
          const windows = window.open(
            nameWindow + parameters,
            nameWindow.toUpperCase() + id,
            options
          );
          subject.next(windows);
        } else {
          this.notificationsService.error('', 'Não foi possível abrir uma nova janela!');
        }
      });
    }
    return subject.asObservable();
  }
}
