import { WorkspaceNewService } from '@services/workspace-new.service';
import { MatDialog } from '@angular/material';
import { WorkspaceNewDto } from '@dto/workspacenew.dto';
import { WorkspaceItemContentDto } from '@dto/workspace-item-content-dto';
import { WorkspaceNewSaveDto } from '@dto/workspace-new-save-dto';
import { NormalModalComponent } from '@component/normal-modal/normal-modal.component';
import { Constants } from '@utils/constants';
import { Inject, forwardRef } from '@angular/core';
import { AbstractItemService } from '@business/base/abstract-item.service';
import { Observable, Subject } from 'rxjs';
import { Box } from '@business/home/workspace-new/workspace-new.component';

export abstract class AbstractItem {

  dialog: MatDialog;
  data: Box;
  protected workspaceService: WorkspaceNewService;
  protected itemService: AbstractItemService;

  constructor(@Inject(forwardRef(() => WorkspaceNewService)) workspaceService: WorkspaceNewService,
    dialog: MatDialog,
    @Inject(forwardRef(() => AbstractItemService)) itemService: AbstractItemService
  ) {
    this.workspaceService = workspaceService;
    this.itemService = itemService;
    this.dialog = dialog;
    itemService.data$.subscribe(data => {
      this.data = data;
    });
    // this.validateOnlyOneAssetAssigned(null, null, null);
  }

  removeItemFromWorkspace() {
    const userWorkspaceSelected: WorkspaceNewDto = this.workspaceService.getUserWorkspaceById(this.workspaceService.getWksSelectedId());
    const contentToUpdate: Array<WorkspaceItemContentDto> = JSON.parse(userWorkspaceSelected.JsonData).content;
    const workspaceUpdated: WorkspaceNewSaveDto = new WorkspaceNewSaveDto(userWorkspaceSelected.Name, [],
      userWorkspaceSelected.IndexTab);
    if (contentToUpdate) {
      contentToUpdate.splice(contentToUpdate.findIndex(x => x.id === this.data.id), 1);
      workspaceUpdated.content = contentToUpdate;
    }
    this.dialog.open(NormalModalComponent, {
      data: {
        title: '',
        body: Constants.WORKSPACE_MSG004,
        redirectUrl: Constants.HOME_WKS_PATH,
        queryParams: {
          workspaceId: this.workspaceService.getWksSelectedId()
          , deleteCmp: true, workspaceUpdated: JSON.stringify(workspaceUpdated)
        }
      },
      panelClass: 'mat-modal-normal',
    });
  }

  updateOnDemand(oldSymbol: string, newSymbol: string, isExistSymbol?: boolean) {
    if (isExistSymbol) {
      this.data.symbol = oldSymbol;
    }
    if (!oldSymbol && newSymbol) {
      this.data.symbol = newSymbol;
      this.workspaceService.workspaceSaveOnDemand(this.data);
    } else if (oldSymbol !== null && oldSymbol !== undefined && oldSymbol.trim().toLowerCase() !== newSymbol.trim().toLowerCase()) {
      this.workspaceService.workspaceSaveOnDemand(this.data);
    }
  }

  updateContentOnDemand() {
    this.workspaceService.workspaceSaveOnDemand(this.data);
  }

  getNewWindow(nameWindow, parameters, options, id): Observable<any> {
    const subject = new Subject<any>();
    this.itemService.openNewToken(nameWindow, parameters, options, id)
      .subscribe(data => {
        subject.next(data);
      });
    return subject.asObservable();
  }

  // public static WINDOW_NAME_ASSET_SUMMARY = 'asset-summary';
  // public static WINDOW_NAME_BOOK = 'book';
  // public static WINDOW_NAME_WORK_SHEET = 'worksheet';
  // public static WINDOW_NAME_NEWS_SEARCH = 'news-search';
  // public static WINDOW_NAME_MARKET_HIGHLIGHTS = 'market';
  // public static WINDOW_NAME_QUOTE_BOX = 'quoted-box';
  // public static WINDOW_NAME_TIMES_TRADES = 'times-trades';
  // public static WINDOW_NAME_BILLET_DAY_TRADE = 'billet-day-trade';
  // public static WINDOW_NAME_POSITION_DAY = 'position-day';
  // public static WINDOW_NAME_QUOTE_CHART = 'quote-chart';
  // public static WINDOW_NAME_VOLUME_AT_PRICE = 'volume-at-price';
  // for (let i = 0; i < this.userWorkspaces.length; i++) {
  //     if (this.userWorkspaces[i].JsonData) {
  //       const workspace = JSON.parse(this.userWorkspaces[i].JsonData);
  //       if ((workspace.title.trim() == newName.trim()) && (this.userWorkspaces[i].Id !== workspaceChangedId)) {
  //         return false;
  //       }
  //     }
  //   }

  validateOnlyOneAssetAssigned(assetName: string, type: string, idWksSelected?: number) {
    let isExist;
    const userWorkspaces = this.workspaceService.getUserWorkspaces();
    for (let i = 0; i < userWorkspaces.length; i++) {
      if (userWorkspaces[i].Id !== undefined && userWorkspaces[i].Id === idWksSelected) {
        const workspace = JSON.parse(userWorkspaces[i].JsonData);
        for (let j = 0; j < workspace.content.length; j++) {
          if (workspace.content[j].type === type && workspace.content[j].symbol.toLowerCase() === assetName.toLowerCase()) {
            isExist = true;
            break;
          }
        }
      }
    }
    if (isExist) {
      return isExist;
    } else {
      return isExist = false;
    }
  }

  validateMaxComponents(numberOfCompToInsert: number, idWksSelected: number) {
    const userWorkspace = this.workspaceService.getUserWorkspaceById(idWksSelected);
    if (userWorkspace && userWorkspace.JsonData) {
      const workspace = JSON.parse(userWorkspace.JsonData);
      numberOfCompToInsert += workspace.content.length;
    }
    if (numberOfCompToInsert >= Constants.NUM_MAX_WORKSPACE_CARDS) {
      return true;
    } else {
      return false;
    }
  }

}
