import { Constants } from '@utils/constants';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { News } from '@model/webFeeder/news.model';

@Component({
  selector: 'app-news-card',
  templateUrl: './news-card.component.html'
})
export class NewsCardComponent implements OnInit {

  public new: News;
  public urlImg: string;
  public description: string;
  public companyImg: string;

  @Input() data: any;
  @Output() dataDetail = new EventEmitter();

  constructor() { }

  ngOnInit() {
    this.new = this.data;
    const d = document.createElement('div');
    // this.new.description = this.new.description + ' <img src=\"https://www.w3schools.com/images/w3schools_green.jpg\">';
    d.innerHTML = this.new.description;
    let image;
    if (this.data.agencyCode === 0) {
      this.urlImg = this.new.img;
    } else {
      image = d.getElementsByTagName('img').item(0);
      if (image) {
        this.urlImg = image.getAttribute('src');
      }
    }
    this.description = d.innerText;
    this.companyImg =  Constants.NEWS_COMPANIES_URL.find(x => x.name.toLowerCase() === this.new.agencyDescription.toLowerCase()).url;
  }

  openDetails() {
    this.dataDetail.emit(this.new);
  }

  public CutString(text: string, numberOfCaracter: number): string {
    if (text === undefined) {
      return '';
    }
    if (text.length > numberOfCaracter) {
      return text.substring(0, numberOfCaracter) + ' ...';
    }
    return text;
  }
}
