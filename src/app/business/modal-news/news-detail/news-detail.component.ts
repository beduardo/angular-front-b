import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { News } from '@model/webFeeder/news.model';
import { FastMarket } from '@services/fast-market/fast-market';
import { Constants } from '@utils/constants';

@Component({
  selector: 'app-news-detail',
  templateUrl: './news-detail.component.html'
})
export class NewsDetailComponent implements OnInit {

  public newWF: News = new News();
  public newFastMarketBranch: any = [];
  public newFastMarketNews: any = [];
  public urlImg: string;
  public companyImg = '';
  public close: boolean;
  public origin: string;
  public typeNews: number;
  @Input() data: { new: News, close: boolean, origin: string };

  @Output() closeDetail = new EventEmitter();

  constructor() { }

  ngOnInit() {
    const d = document.createElement('div');
    this.close = this.data.close;
    this.origin = this.data.origin;
    if (this.data.origin === 'workspace') {
      this.typeNews = 0;
      this.newFastMarketBranch = this.data.new[0];
      d.innerHTML = this.newFastMarketBranch.content.rendered;
      const image = this.newFastMarketBranch._embedded['wp:featuredmedia'][0].source_url;
      if (image) {
        this.urlImg = image;
        document.getElementById('div-detail').style.height = '41vh';
      }
    } else if (this.data.new.agencyCode === 0) {
      this.typeNews = 1;
      this.newFastMarketNews = this.data.new;
      d.innerHTML = this.newFastMarketNews.description;
      const image = this.newFastMarketNews.img;
      if (image) {
        this.urlImg = image;
        document.getElementById('div-detail').style.height = '45vh';
      }
    } else {
      this.typeNews = 2;
      this.newWF = this.data.new;
      // this.close = this.data.close;
      // this.new.description = this.new.description + ' <img src=\"https://www.w3schools.com/images/w3schools_green.jpg\">';
      d.innerHTML = this.newWF.description;
      const image = d.getElementsByTagName('img').item(0);
      if (image) {
        this.urlImg = image.getAttribute('src');
        document.getElementById('div-detail').style.height = '45vh';
      }

      this.newFastMarketNews.agencyDescription = this.newWF.agencyDescription;
    }
    if (this.newFastMarketNews.agencyDescription) {
      this.companyImg = Constants.NEWS_COMPANIES_URL.find(x => x.name.toLowerCase() ===
        this.newFastMarketNews.agencyDescription.toLowerCase()).url;
    }
    setTimeout(() => {
      this.scrollTopInit();
    }, 500);
  }

  scrollTopInit() {
    const mainDiv = document.getElementById('scrollable-area');
    if (mainDiv && mainDiv.scrollTop && mainDiv.scrollTop > 0) {
      mainDiv.scrollTo({ top: 0, behavior: 'smooth' });
    }
  }

  closeDetails() {
    if (this.newFastMarketNews && this.typeNews !== 2) {
      this.newWF.newsCode = this.newFastMarketNews.newsCode;
      this.newWF.agencyCode = this.newFastMarketNews.agencyCode;
      this.newWF.agencyDescription = this.newFastMarketNews.agencyDescription;
      this.newWF.title = this.newFastMarketNews.title;
      this.newWF.description = this.newFastMarketNews.description;
      this.newWF.categoryCode = this.newFastMarketNews.categoryCode;
      this.newWF.newsDate = this.newFastMarketNews.newsDate;
      this.newWF.fullDescription = this.newFastMarketNews.fullDescription;
      this.newWF.img = this.newFastMarketNews.img;
    }

    this.closeDetail.emit(this.newWF);
  }

  scrollTop() {
    const mainDiv = document.getElementById('scrollable-area');
    mainDiv.scrollTo({ top: 0, behavior: 'smooth' });
  }

  onScroll(event) {
    if (event.target.scrollTop > 0) {
      document.getElementById('btn-scroll').classList.remove('btn-hidden');
    } else {
      document.getElementById('btn-scroll').classList.add('btn-hidden');
    }
  }
}
