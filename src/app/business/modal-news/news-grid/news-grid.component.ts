import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

import { News } from '@model/webFeeder/news.model';

@Component({
  selector: 'app-news-grid',
  templateUrl: './news-grid.component.html'
})
export class NewsGridComponent implements OnInit {

  public new: News;
  public date: any;
  public description: string;

  @Input() data: any;

  @Output() dataDetail = new EventEmitter();

  constructor() {
  }

  ngOnInit() {
    this.new = this.data;
    this.date = new Date(this.new.newsDate);
    const d = document.createElement('div');
    d.innerHTML = this.new.description;
    this.description = d.innerText;
  }

  openDetails() {
    this.dataDetail.emit(this.new);
  }
}
