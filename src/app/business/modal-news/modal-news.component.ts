import { AppUtils } from './../../utils/app.utils';
import { ConfigService } from '@services/webFeeder/config.service';
import { animate, style, transition, trigger } from '@angular/animations';
import { Component, OnInit, ViewChild, ViewContainerRef, HostListener, Inject, OnDestroy } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { News } from '@model/webFeeder/news.model';
import { CustomSnackbarService } from '@services/custom-snackbar-service.service';
import { Constants } from '@utils/constants';
import { NgxMasonryOptions } from 'ngx-masonry';
import { AutoLogoutService } from '@auth/auto-logout-service.service';
import { Subject } from 'rxjs';
import { FastMarketService } from '@services/fast-market.service';
import { NotificationsService } from 'angular2-notifications';
import { AuthServer } from '@services/auth-server.service';

@Component({
  selector: 'app-modal-news',
  templateUrl: './modal-news.component.html',
  animations: [
    trigger('valueAnimation', [
      transition('void => *', [
        style({ transform: 'translateX(0) scale(0)' }),
        animate('0.2s ease-in')
      ])
    ]),
    trigger('valueListAnimation', [
      transition('* => void', [
        animate('0.3s ease-in-out', style({ transform: 'translateX(-100%)' }))
      ]),
      transition('void => *', [
        style({
          transform: 'translateX(-100%)'
        }),
        animate('0.3s 100ms ease-in-out')
      ])
    ]),
    trigger('valueLDetailAnimation', [
      transition('* => void', [
        animate('0.3s ease-in-out', style({ transform: 'translateX(100%)' }))
      ]),
      transition('void => *', [
        style({
          transform: 'translateX(100%)'
        }),
        animate('0.3s 100ms ease-in-out')
      ])
    ]),
    trigger('valueAnimationCards', [
      transition('grid => void', [
        animate('0.3s ease-in-out', style({ transform: 'translateX(-100%)' }))
      ]),
      // TODO transition('void => grid', [
      // TODO   style({
      // TODO     transform: 'translateX(-100%)'
      // TODO   }),
      // TODO   animate('0.3s 200ms ease-in-out')
      // TODO ]),
      transition('card => void', [
        animate('0.3s ease-in-out', style({ transform: 'translateX(100%)' }))
      ]),
      transition('void => card', [
        style({
          transform: 'translateX(100%)'
        }),
        animate('0.3s 200ms ease-in-out')
      ])
    ])
  ]
})
export class ModalNewsComponent implements OnInit, OnDestroy {

  @ViewChild('modalNews', { read: ViewContainerRef }) parent: ViewContainerRef;

  title = 'NOTÍCIAS';
  listNewsFastMarket: any[] = [];
  listNewsFastMarketAux: any[] = [];
  listNews: News[] = [];
  listNewsAux: any[] = [];
  selectedItem: any;
  page = 1;
  qtdPage = 12;
  nPages = 0;
  listPages = [];
  cardsPage = 5;
  dateStartFilter: Date;
  dateEndFilter: Date;
  agencyFilter = '0';
  textFilter = '';
  nResize = 1;
  cardsScroll = 8;
  listAgencys = [
    { value: 0, desc: 'FAST MARKETS' },
    { value: 1, desc: 'CEDRO' },
    { value: 3, desc: 'BOVESPA' },
    { value: 4, desc: 'BMF' }
  ];
  isList = 1;
  loading = true;
  loadingSearch = false;
  typeNews = 1;
  timeOut: any;
  scroll: any;
  public changingValue: Subject<boolean> = new Subject();
  public myOptions: NgxMasonryOptions = {
    originLeft: true,
    transitionDuration: '1.17s',
  };

  public intervalLasNews: any;
  public loadingGet: boolean;
  public pageTemp: number;
  public whiteLabel = this._configService.getWhiteLabel();
  filterDateRange: number;

  @HostListener('mousemove')
  onMouseMove() {
    this._autoLogoutService.resetTime();
  }

  @HostListener('click')
  onClick() {
    this._autoLogoutService.resetTime();
  }

  // @HostListener('document:keydown', ['$event'])
  // onkeydown(event) {
  //   if (event.key == 'Escape') {
  //     if ((document.getElementsByTagName('ngb-modal-window').length == 0)) {
  //       this.close();
  //     }
  //   }
  // }

  constructor(
    public _NotificationService: NotificationsService,
    public dialogRef: MatDialogRef<ModalNewsComponent>,
    private customSnackBar: CustomSnackbarService,
    private _autoLogoutService: AutoLogoutService,
    private fastMarketService: FastMarketService,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private _authServer: AuthServer,
    private _configService: ConfigService
  ) {
    this.filterDateRange = this._configService.getModalNewsFilterRange();
  }

  ngOnInit() {
    if (this.data && this.data !== undefined) {
      this.selectedItem = this.data.new;
      this.openDetails(this.selectedItem, this.data.close, this.data.origin);
      this.loading = false;
    }
    // this.getLastNews();
  }

  ngOnDestroy() {
    if (this.intervalLasNews) {
      clearInterval(this.intervalLasNews);
    }
    this.changingValue.unsubscribe();
  }

  getPagination() {
    let i;
    this.listPages = [];
    this.nPages =
      this.listNews.length % this.cardsPage === 0
        ? Math.floor(this.listNews.length / this.cardsPage)
        : Math.floor(this.listNews.length / this.cardsPage) + 1;

    if (this.page > 2 && this.nPages - this.page > 2) {
      for (i = 1; i <= this.nPages; i++) {
        const dif = i - this.page;
        if (dif <= 2 && dif >= -2 && i > 0 && i <= this.nPages) {
          this.listPages.push(i);
        }
      }
    } else if (this.page < 3) {
      if (this.nPages > 5) {
        for (i = 1; i <= 5; i++) {
          if (i > 0 && i <= this.nPages) {
            this.listPages.push(i);
          }
        }
      } else {
        for (i = 1; i <= this.nPages; i++) {
          if (i > 0 && i <= this.nPages) {
            this.listPages.push(i);
          }
        }
      }
    } else if (this.nPages - this.page < 3) {
      for (i = this.nPages - 4; i <= this.nPages; i++) {
        if (i > 0 && i <= this.nPages) {
          this.listPages.push(i);
        }
      }
    }
    if (this.pageTemp) {
      this.page = this.pageTemp;
    }
  }

  close() {
    this.dialogRef.close();
  }

  changePage(page) {
    if (page !== 'back' && page !== 'next') {
      if (page >= 1) {
        this.pageTemp = page;
        if (page === this.nPages) {
          this.page = page;
          this.pageTemp = page;
        }
        if (this.page === this.nPages) {
          if (this.agencyFilter === '0' && !this.textFilter) {
            this.loadingGet = true;
            this.fastMarketService.getLastFastMarketNews(this.qtdPage, this.page + 1, null, this.dateStartFilter, this.dateEndFilter)
              .subscribe(lastNewFast => {
                if (lastNewFast.length === 0) {
                  this._NotificationService.info('', Constants.DATE_MSG004);
                  this.loadingGet = false;
                } else {
                  if (lastNewFast) {
                    lastNewFast.forEach(element => {
                      const news = {
                        newsCode: element.id,
                        agencyCode: 0,
                        agencyDescription: 'Fast Markets',
                        title: element.title.rendered,
                        description: element.excerpt.rendered,
                        categoryCode: element.categories,
                        newsDate: element.date,
                        fullDescription: element.content.rendered,
                        img: null
                      };
                      if (element._embedded && element._embedded['wp:featuredmedia']) {
                        news.img = element._embedded['wp:featuredmedia'][0].source_url;
                      }
                      this.listNews.push(news);
                    });
                    // this.changePage(1);
                    this.listNewsAux = this.listNews;
                  }
                }
                this.getPagination();
                this.loadingGet = false;
              }, error => {
                this.loadingGet = false;
              });
          }
        }
      }
    } else if (page === 'back') {
      if (this.page > 1) {
        this.page = this.page - 1;
        this.pageTemp = this.page;
      }
    } else if (page === 'next') {
      if (this.page < this.nPages) {
        this.page = this.page + 1;
        this.pageTemp = this.page;
      }
      if (this.page === this.nPages) {
        if (this.agencyFilter === '0' && !this.textFilter) {
          this.loadingGet = true;
          this.fastMarketService.getLastFastMarketNews(this.qtdPage, this.page + 1, null, this.dateStartFilter, this.dateEndFilter)
            .subscribe(lastNewFast => {
              if (lastNewFast.length === 0) {
                this._NotificationService.info('', Constants.DATE_MSG004);
                this.loadingGet = false;
              } else {
                if (lastNewFast) {
                  lastNewFast.forEach(element => {
                    const news = {
                      newsCode: element.id,
                      agencyCode: 0,
                      agencyDescription: 'Fast Markets',
                      title: element.title.rendered,
                      description: element.excerpt.rendered,
                      categoryCode: element.categories,
                      newsDate: element.date,
                      fullDescription: element.content.rendered,
                      img: null
                    };
                    if (element._embedded && element._embedded['wp:featuredmedia']) {
                      news.img = element._embedded['wp:featuredmedia'][0].source_url;
                    }
                    this.listNews.push(news);
                  });
                  this.changePage(1);
                  this.listNewsAux = this.listNews;
                }
              }
              this.getPagination();
              this.loadingGet = false;
            }, error => {
              this.loadingGet = false;
            });
        }
      }
    }
    // else {
    //   this.page = page;
    // }
    this.getPagination();
  }

  searchFilter() {
    if ((this.dateStartFilter && !this.dateEndFilter) || (!this.dateStartFilter && this.dateEndFilter)) {
      this.customSnackBar.createSnack(this.parent, Constants.DATE_MSG003, Constants.ALERT_TYPE.warn);
      return;
    } else if (this.dateStartFilter > this.dateEndFilter) {
      this.customSnackBar.createSnack(this.parent, Constants.DATE_MSG001, Constants.ALERT_TYPE.warn);
      return;
    } else if (this.dateEndFilter > new Date()) {
      this.customSnackBar.createSnack(this.parent, Constants.DATE_MSG002, Constants.ALERT_TYPE.warn);
      return;
    } else if (this.dateStartFilter !== this.dateEndFilter && !AppUtils.validateDateRange(this.dateStartFilter, this.dateEndFilter, this.filterDateRange)) {
      const msg = Constants.DATE_MSG005.replace('{{range}}', this.filterDateRange.toString());
      this.customSnackBar.createSnack(this.parent, msg, Constants.ALERT_TYPE.warn);
      return;
    }
    this.resetResize();
    this.loadingSearch = true;
    if (this.agencyFilter === '0') {
      this.getNewsAll(false, null, null, this.textFilter, this.dateStartFilter, this.dateEndFilter);
    } else {
      this._authServer.iNews.getNewsByParameters(this.textFilter, this.agencyFilter, this.dateStartFilter, this.dateEndFilter)
        .subscribe(data => {
          this.loadingSearch = false;
          if (data) {
            if (data.length < 1) {
              // this.customSnackBar.createSnack(this.parent, Constants.DATE_MSG004, Constants.ALERT_TYPE.info);
              this._NotificationService.info('', Constants.DATE_MSG004);
            }
            this.changePage(1);
            this.listNews = data.slice(0, Constants.QTD_NEWS);
            this.listNewsAux = this.listNews;
          }
        });
    }

    // else if (this.agencyFilter !== '0' && !this.dateStartFilter && !this.dateEndFilter) {
    //   if (this.textFilter && this.textFilter !== '') {
    //     this.listNews = [];
    //     this.getNewsAll(true);
    //   } else {
    //     this.listNews = [];
    //     this.getNewsAll(false);
    //   }
    // }
  }

  clearFilter() {
    this.changingValue.next(true);
    this.agencyFilter = '0';
    this.dateStartFilter = null;
    this.dateEndFilter = null;
    if (this.textFilter !== '') {
      this.changePage(1);
      this.textFilter = '';
      this.listNews = this.listNewsAux;
    }
    this.searchFilter();
  }

  getNewsAll(istextFilter: boolean, qtdPage?: number, page?: number, search?: string, dateStartFilter?: Date, dateEndFilter?: Date) {

    if (!qtdPage) {
      qtdPage = this.qtdPage;
    }
    // NEWS FAST MARKETS
    this.fastMarketService.getLastFastMarketNews(qtdPage, page, search, dateStartFilter, dateEndFilter)
      .subscribe(lastNewFast => {
        if (lastNewFast) {
          if (lastNewFast.length <= 0) {
            this.listNews = [];
            this.listNewsAux = [];
            // this.customSnackBar.createSnack(this.parent, Constants.DATE_MSG004, Constants.ALERT_TYPE.info);
            this._NotificationService.info('', Constants.DATE_MSG004);
            this.loading = false;
            this.loadingSearch = false;
            return;
          }
          if (this.loadingSearch) {
            this.listNews = [];
            this.listNewsAux = [];
          }
          setTimeout(() => {
            lastNewFast.forEach(element => {
              const news = {
                newsCode: element.id,
                agencyCode: 0,
                agencyDescription: 'Fast Markets',
                title: element.title.rendered,
                description: element.excerpt.rendered,
                categoryCode: element.categories,
                newsDate: element.date,
                fullDescription: element.content.rendered,
                img: element._embedded['wp:featuredmedia'][0].source_url
              };
              this.listNews.push(news);
            });
            this.changePage(1);
            if (istextFilter) {
              this.filterNewsByString(this.textFilter);
            }
            this.listNewsAux = this.listNews;
            this.loading = false;
            this.loadingSearch = false;
          }, 1000);
        }
      });

    // NEWS WF
    // this.newsService.getLastNews(Constants.QTD_NEWS)
    //   .subscribe(data => {
    //     this.changePage(1);
    //     this.listNews = data;
    //     if (istextFilter) {
    //       this.filterNewsByString(this.textFilter);
    //     }
    //     // TODO this.listNews = data.slice(0, Constants.QTD_NEWS);
    //     this.listNewsAux = this.listNews;
    //     this.loading = false;
    //     this.loadingSearch = false;
    //   });
  }

  openDetails(event, close, origin?: string) {
    this.selectedItem = { new: event, close: close, origin: origin };
    this.isList = null;
    setTimeout(() => {
      this.isList = 0;
    }, 300);
  }

  openList() {
    const list = this.listNews;
    this.listNews = [];
    this.isList = null;
    setTimeout(() => {
      this.isList = 1;
    }, 300);
    setTimeout(() => {
      this.listNews = list;
      this.selectedItem = null;
    }, 500);
  }

  toggleType(type: number) {
    if (this.typeNews === type) {
      return;
    }
    this.page = 1;
    if (type === 1) {
      document
        .getElementById('col-overflow')
        .classList.remove('col-cards-overflow');
      this.changePage(1);
    } else {
      this.resetResize();
    }
    this.typeNews = null;
    setTimeout(() => {
      this.typeNews = type;
    }, 300);
  }

  keyUpSearch(event) {
    if (this.agencyFilter !== '0') {
      clearTimeout(this.timeOut);
      if (event.target.value === '') {
        this.changePage(1);
        this.listNews = this.listNewsAux;
        return;
      }
      const self = this;
      this.timeOut = setTimeout(() => {
        self.filterNewsByString(event.target.value);
        this.getPagination();
      }, 500);
    }
  }

  filterNewsByString(value) {
    const list = this.listNews.filter(element =>
      element.title.toLowerCase().includes(value.toLowerCase())
    );
    if (list && list.length < 1) {
      // this.customSnackBar.createSnack(this.parent, Constants.DATE_MSG004, Constants.ALERT_TYPE.info);
      this._NotificationService.info('', Constants.DATE_MSG004);
    }
    this.listNews = list;
  }

  scrollTop() {
    if (this.typeNews === 0) {
      const mainDiv = document.getElementById('col-overflow');
      mainDiv.scrollTo({ top: 0, behavior: 'smooth' });
    }
  }

  scrollEvent(event) {
    if (event.target.scrollTop > (event.target.scrollHeight - 650) && this.listNews.length > (this.nResize * this.cardsScroll)) {
      this.loadingGet = true;
      this.nResize++;
      // NEWS FAST MARKETS
      if (this.agencyFilter === '0' && !this.textFilter) {
        this.page = this.page + 1;
        this.fastMarketService.getLastFastMarketNews(this.qtdPage, this.page)
          .subscribe(lastNewFast => {
            if (lastNewFast) {
              lastNewFast.forEach(element => {
                const news = {
                  newsCode: element.id,
                  agencyCode: 0,
                  agencyDescription: 'Fast Markets',
                  title: element.title.rendered,
                  description: element.excerpt.rendered,
                  categoryCode: element.categories,
                  newsDate: element.date,
                  fullDescription: element.content.rendered,
                  img: null
                };
                if (element._embedded && element._embedded['wp:featuredmedia']) {
                  news.img = element._embedded['wp:featuredmedia'][0].source_url;
                }
                this.listNews.push(news);
              });
              this.changePage(1);
              // if (istextFilter) {
              //   this.filterNewsByString(this.textFilter);
              // }
              // TODO this.listNews = data.slice(0, Constants.QTD_NEWS);
              this.listNewsAux = this.listNews;
              this.loading = false;
              this.loadingSearch = false;
            }
            this.loadingGet = false;
          });
      }
    }
  }

  resetResize() {
    this.nResize = 1;
  }

  public onKeyPressLetter(e) {
    return false;
  }

  public changeDateIni(value) {
    if (value) {
      this.dateStartFilter = new Date(value);
    }
    // this.dataIniAux = new Date(value);
  }

  public changeDateFim(value) {
    if (value) {
      this.dateEndFilter = new Date(value);
    }
    // this.dataFimAux = new Date(value);
  }

  public getLastNews() {
    this.intervalLasNews = setInterval(() => {
      if (this.listNews) {
        if (this.agencyFilter === '0' && !this.textFilter) {
          this.fastMarketService.getLastFastMarketNews(1, 1)
            .subscribe(res => {
              if (res && this.listNews) {
                if (this.listNews[0] && this.listNews[0].agencyCode === 0) {
                  if (this.listNews[0].newsCode !== res[0].id) {
                    this.loadingSearch = true;
                    const news = {
                      newsCode: res[0].id,
                      agencyCode: 0,
                      agencyDescription: 'Fast Markets',
                      title: res[0].title.rendered,
                      description: res[0].excerpt.rendered,
                      categoryCode: res[0].categories,
                      newsDate: res[0].date,
                      fullDescription: res[0].content.rendered,
                      img: res[0]._embedded['wp:featuredmedia'][0].source_url
                    };
                    this.listNews.unshift(news);
                    this.loadingSearch = false;
                    this.typeNews = null;
                    setTimeout(() => {
                      this.typeNews = 0;
                    }, 300);
                  }
                }
              }
            });
        }
      }
    }, 10000);
  }
}
