import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { BookControllerComponent } from '@business/book/book.controller';
import { MatIconModule, MatDividerModule, MatTabsModule, MatTooltipModule, MatProgressSpinnerModule, MatFormFieldModule, MatAutocompleteModule, MatOptionModule, MatInputModule, MatCardModule, MatDialogModule, MatBottomSheetModule } from '@angular/material';
import { AutoCompleteComponent } from '@component/auto-complete/auto-complete.component';
import { CurrencyComponent } from '@component/currency/currency.component';
import { SpinnerComponent } from '@component/spinner/spinner.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AutoSizeInputDirective } from '@shared/directives/autosize-input.directive';
import { RouterTestingModule } from '@angular/router/testing';
import { ConfigService } from '@services/webFeeder/config.service';
import { ConfigServiceMock, CustomSnackbarServiceMock } from '@services/_test-mock-services/_mock-services';
import { HttpModule } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';
import { NotificationsService, SimpleNotificationsModule } from 'angular2-notifications';
import { CustomSnackbarService } from '@services/custom-snackbar-service.service';
import { DatePipe } from '@angular/common';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

describe('BookController', () => {
  let component: BookControllerComponent;
  let fixture: ComponentFixture<BookControllerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        BookControllerComponent,
        AutoCompleteComponent,
        CurrencyComponent,
        SpinnerComponent,
        AutoSizeInputDirective
      ],
      imports: [
        MatIconModule,
        MatDividerModule,
        MatTabsModule,
        MatTooltipModule,
        MatProgressSpinnerModule,
        MatFormFieldModule,
        MatAutocompleteModule,
        MatOptionModule,
        MatInputModule,
        ReactiveFormsModule,
        MatCardModule,
        RouterTestingModule,
        HttpModule,
        HttpClientModule,
        SimpleNotificationsModule.forRoot(),
        MatDialogModule,
        MatBottomSheetModule,
        BrowserAnimationsModule
      ],
      providers: [
        DatePipe,
        NotificationsService,
        { provide: ConfigService, useClass: ConfigServiceMock },
        { provide: CustomSnackbarService, useClass: CustomSnackbarServiceMock },

      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BookControllerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
