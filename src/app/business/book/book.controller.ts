import { ConfigService } from '@services/webFeeder/config.service';
import { DatePipe } from '@angular/common';
import { AuthServer } from '@services/auth-server.service';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/startWith';
import { Component, Input, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { AbstractItem } from '@business/base/abstract-item';
import { AbstractItemService } from '@business/base/abstract-item.service';
import { MessageBook } from '@model/webFeeder/message-book.model';
import { MessageQuote } from '@model/webFeeder/message-quote.model';
import { AggregatedBookService } from '@services/webFeeder/sckt-aggregated-book.service';
import { BookService } from '@services/webFeeder/sckt-book.service';
import { QuoteService } from '@services/webFeeder/sckt-quotes.service';
import { WorkspaceNewService } from '@services/workspace-new.service';
import { AppUtils } from '@utils/app.utils';
import { Constants } from '@utils/constants';
import { GridOptions } from 'ag-grid';
import { NotificationsService } from 'angular2-notifications';
import { environment } from 'environments/environment';
import { Subscription, Subject } from 'rxjs';
import { BilletOrderModalService } from '@services/billet-order-modal.service';

@Component({
  selector: Constants.WINDOW_NAME_BOOK,
  templateUrl: './book.template.html'
})
export class BookControllerComponent extends AbstractItem implements OnInit, OnDestroy {
  public instance: any;
  public viewContainerId: string;
  public gridOptions: GridOptions;
  public columnDefs: any[];
  public rowData: any[];
  @Input() data: any;
  @ViewChild('bookDiv') bookDiv;
  public width = 380;
  public height = 474;
  public left;
  public top;
  public windowIcons: Array<string>;
  public BUY = Constants.ORDER_DIRECTION_BUY;
  public SELL = Constants.ORDER_DIRECTION_SELL;
  public executeFirstTime = false;
  public imgName: any;
  public lastTrade: any;
  public lastTradePrevious: any;
  public variation: any;
  public tickSize: any;
  public asset: any;
  public index: number;
  public showAggregatedBook = false;
  public company: string;
  public filteredAssets: any[];
  public assetValid = false;
  public symbol: string;
  public messageQuote = new MessageQuote();
  private subscriptionQuote: Subscription;
  private subscriptionBook: Subscription;
  private subscriptionAggregatedBook: Subscription;
  public status: any;
  public showStatus = false;
  public style = 0;
  public last;
  public indiceFuture: boolean;
  public closedMarketmsg = Constants.CLOSED_MARKET_MESSAGE;
  public rowOffersData: any[];
  public rowPriceData: any[];
  public valueBuy: number;
  public valueSell: number;
  public percentageBuy: number;
  public percentageSell: number;
  public classPressure: string;
  public headerAssetNameColor: string;
  public borderColor: string;
  public loading: boolean;
  public loadingDetailBook: boolean;
  public loadingAggregatedBook: boolean;
  public showBoxSearch: boolean;
  public showDetalhado: boolean;
  public searchDisplay = false;
  public activeIsValid = true;
  public windowActive: any;
  public newWindow: boolean;
  public low: number;
  public high: number;
  public tooltiphover: boolean;
  public hiddenInvalid: boolean;
  public dialog: MatDialog;
  public pressureBarPercentageLeft: string;
  public pressureBarPercentageRight: string;
  public msgInvalidAsset = Constants.WORKSPACE_MSG007;
  public changingValue: Subject<boolean> = new Subject();
  protected workspaceService: WorkspaceNewService;
  public msgMarketClosed = Constants.MARKET_OPEN_CLOSED_MSG001;
  public msgNoData = Constants.WORKSPACE_MSG018;
  public marketClosed = false;
  public isSubscribeBook: boolean;
  public tabChange: any;
  public auctionHammer: boolean;
  public showVariation = true;
  public isUpdateOnDemand: boolean;
  public isLoginPI: boolean;

  constructor(
    private _route: Router,
    private quoteService: QuoteService,
    private bookService: BookService,
    private aggregatedBookService: AggregatedBookService,
    private route: ActivatedRoute,
    workspaceService: WorkspaceNewService,
    itemService: AbstractItemService,
    dialog: MatDialog,
    public _NotificationService: NotificationsService,
    private _authServer: AuthServer,
    private datePipe: DatePipe,
    private configService: ConfigService,
    private billetOrderModalService: BilletOrderModalService
  ) {
    super(workspaceService, dialog, itemService);
    this.workspaceService = workspaceService;
    this.showStatus = false;
    this.status = '';
    this.symbol = '';
    this.lastTrade = 100;
    this.lastTradePrevious = 100;
    this.variation = 50;
    this.tickSize = 2;
    this.asset = { symbol: '', company: '' };
    this.index = 1;
    this.isLoginPI = this.configService.isLoginPi();
    this.classPressure = 'linear-gradient(to right, #ffb300 50%, #78bd36 50%)';
    this.percentageBuy = 50;
    this.percentageSell = 50;
    this.valueBuy = 0;
    this.valueSell = 0;
    this.showBoxSearch = true;
    this.borderColor = '';
    this.showAggregatedBook = false;
    this.dialog = dialog;
    this.tooltiphover = false;
  }

  ngOnInit(self = this) {
    this.windowIcons = ['close'];
    if (this.data !== undefined && this.data) {
      if (this.data.company !== undefined) {
        this.asset.company = this.data.company;
      }
      if (this.data.symbol !== undefined) {
        this.asset.symbol = this.data.symbol;
        if (this.data.symbol !== '') {
          this.loading = true;
          this.loadingDetailBook = false;
          this.loadingAggregatedBook = false;
          this.showBoxSearch = false;
        }
        this.symbol = this.data.symbol;
      }
      if (this.data.position !== undefined) {
        if (this.data.position.left !== undefined) {
          this.left = this.data.position.left;
        }
        if (this.data.position.top !== undefined) {
          this.top = this.data.position.top;
        }
      } else {
        this.left = 3;
        this.top = 18;
      }
      this.setQuote(this.symbol);
    } else {
      this.newWindow = true;
      this.route.queryParams.subscribe(params => {
        if (params['symbol']) {
          this.loading = true;
          this.symbol = params['symbol'];
          setTimeout(() => {
            this.setQuote(this.symbol);
          }, 2000);
        }
      });
    }
  }

  ngOnDestroy() {
    this.closeSheet();
    if (this.subscriptionQuote) {
      this.subscriptionQuote.unsubscribe();
    }
    if (this.subscriptionAggregatedBook) {
      this.subscriptionAggregatedBook.unsubscribe();
    }
    if (this.subscriptionBook) {
      this.subscriptionBook.unsubscribe();
    }

    if (this.symbol !== '') {
      if (this.aggregatedBookService) {
        this.aggregatedBookService.unsubscribeAggregatedBook(this.symbol);
      }
      if (this.bookService) {
        this.bookService.unsubscribeBook(this.symbol);
      }
      if (this.quoteService) {
        this.quoteService.unsubscribeQuote(this.symbol, Constants.WINDOW_NAME_BOOK);
      }
    }
    this.changingValue.unsubscribe();
  }

  calculatePressure() {
    if (this.valueBuy && this.valueSell) {
      const totalValue = this.valueBuy + this.valueSell;
      this.percentageBuy = ((this.valueBuy * 100) / totalValue);
      this.percentageSell = ((this.valueSell * 100) / totalValue);
      if (this.percentageBuy > this.percentageSell) {
        const gradientSell = this.percentageBuy + 10;
        switch (environment.WHITELABEL) {
          case 1: // Cedro
            this.classPressure = `linear-gradient(to right, #ffb300, ${this.percentageBuy.toFixed(2)}%, #78bd36 ${gradientSell.toFixed(2)}%)`;
            break;
          case 2: // PI
            this.classPressure = `linear-gradient(to right, #6740ff, ${this.percentageBuy.toFixed(2)}%, #ff2c79 ${gradientSell.toFixed(2)}%)`;
            break;
          case 3: // RB
            this.classPressure = `linear-gradient(to right, #ff9204, ${this.percentageBuy.toFixed(2)}%, #78bd36 ${gradientSell.toFixed(2)}%)`;
            break;
          case 5: // UM
            this.classPressure = `linear-gradient(to right, #6298d5, ${this.percentageBuy.toFixed(2)}%, #f06666 ${gradientSell.toFixed(2)}%)`;
            break;
          case 6: // DAYCOVAL
            this.classPressure = `linear-gradient(to right, #0088ca, ${this.percentageBuy.toFixed(2)}%, #009e73 ${gradientSell.toFixed(2)}%)`;
            break;
          case 8: // TRINUS
            this.classPressure = `linear-gradient(to right, #fcaf17, ${this.percentageBuy.toFixed(2)}%, #78bd36 ${gradientSell.toFixed(2)}%)`;
            break;
          case 9: // TRINUS
            this.classPressure = `linear-gradient(to right, #ffb44a, ${this.percentageBuy.toFixed(2)}%, #00af73 ${gradientSell.toFixed(2)}%)`;
            break;
          default:
            this.classPressure = `linear-gradient(to right, #ffb300, ${this.percentageBuy.toFixed(2)}%, #78bd36 ${gradientSell.toFixed(2)}%)`;
            break;
        }
      } else {
        const gradientBuy = this.percentageSell + 10;
        switch (environment.WHITELABEL) {
          case 1:
            this.classPressure = `linear-gradient(to right, #ffb300, ${this.percentageBuy.toFixed(2)}%, #78bd36 ${gradientBuy.toFixed(2)}%)`;
            break;
          case 2:
            this.classPressure = `linear-gradient(to right, #6740ff, ${this.percentageBuy.toFixed(2)}%, #ff2c79 ${gradientBuy.toFixed(2)}%)`;
            break;
          case 3:
            this.classPressure = `linear-gradient(to right, #ff9204, ${this.percentageBuy.toFixed(2)}%, #78bd36 ${gradientBuy.toFixed(2)}%)`;
            break;
          case 5:
            this.classPressure = `linear-gradient(to right, #6298d5, ${this.percentageBuy.toFixed(2)}%, #f06666 ${gradientBuy.toFixed(2)}%)`;
            break;
          case 6:
            this.classPressure = `linear-gradient(to right, #0088ca, ${this.percentageBuy.toFixed(2)}%, #009e73 ${gradientBuy.toFixed(2)}%)`;
            break;
          case 8:
            this.classPressure = `linear-gradient(to right, #fcaf17, ${this.percentageBuy.toFixed(2)}%, #78bd36 ${gradientBuy.toFixed(2)}%)`;
            break;
          case 9:
            this.classPressure = `linear-gradient(to right, #ffb44a, ${this.percentageBuy.toFixed(2)}%, #00af73 ${gradientBuy.toFixed(2)}%)`;
            break;
          default:
            this.classPressure = `linear-gradient(to right, #ffb300, ${this.percentageBuy.toFixed(2)}%, #78bd36 ${gradientBuy.toFixed(2)}%)`;
            break;
        }
      }
    } else {
      switch (environment.WHITELABEL) {
        case 1:
          this.classPressure = 'linear-gradient(to right, #ffb300 50%, #78bd36 55%)';
          break;
        case 2:
          this.classPressure = 'linear-gradient(to right, #6740ff 50%, #ff2c79 55%)';
          break;
        case 3:
          this.classPressure = 'linear-gradient(to right, #ff9204 50%, #78bd36 55%)';
          break;
        case 5:
          this.classPressure = 'linear-gradient(to right, #6298d5 50%, #f06666 55%)';
          break;
        case 6:
          this.classPressure = 'linear-gradient(to right, #0088ca 50%, #009e73 55%)';
          break;
        case 8:
          this.classPressure = 'linear-gradient(to right, #fcaf17 50%, #78bd36 55%)';
          break;
        case 9:
          this.classPressure = 'linear-gradient(to right, #ffb44a 50%, #00af73 55%)';
          break;
        default:
          this.classPressure = 'linear-gradient(to right, #ffb300 50%, #78bd36 55%)';
          break;
      }
      this.percentageBuy = 0;
      this.percentageSell = 0;
    }
  }

  selectAsset(stock) {
    this.asset = stock;
    if (stock.symbol !== '') {
      this.setQuote(stock.symbol);
    }
  }

  resetQuote(symbol: string) {
    if (this.symbol && this.symbol !== '') {
      this.bookService.unsubscribeBook(this.symbol);
      this.aggregatedBookService.unsubscribeAggregatedBook(symbol);
      this.quoteService.unsubscribeQuote(this.symbol, Constants.WINDOW_NAME_BOOK);

      if (this.subscriptionQuote) {
        this.subscriptionQuote.unsubscribe();
      }
      if (this.subscriptionBook) {
        this.subscriptionBook.unsubscribe();
      }
      if (this.subscriptionAggregatedBook) {
        this.subscriptionAggregatedBook.unsubscribe();
      }
      this.asset.symbol = '';
      // this.imgName = '';
      this.status = '';
      this.showStatus = false;
      this.lastTrade = 0;
      this.lastTradePrevious = 0;
      this.variation = 0;
      this.rowOffersData = [];
      this.rowPriceData = [];
      this.percentageBuy = 0;
      this.percentageSell = 0;
    }
    if (symbol === 'invalid') {
      this.asset.symbol = '';
    }
    this.setHeaderAssetNameColor();
  }

  setQuote(symbol: string, assetWithoutSubscribe: boolean = true) {
    symbol = AppUtils.trimStart(symbol);
    if (this.symbol == null) {
      this.symbol = '';
    }
    const symbolTemp = this.symbol;
    if (symbol.toLowerCase() === 'invalid') {
      this.assetValid = false;
      // this.imgName = '';
      this.hiddenInvalid = true;
      this.resetQuote(this.symbol);
      this.asset.symbol = '';
      return;
    }
    // with this logic book-offer doesn't work
    // if (this.symbol.toLowerCase() === symbol.toLowerCase() && this.subscriptionQuote !== undefined) {
    //   return;
    // }
    this.asset.symbol = symbol;
    this.showStatus = false;
    this.status = '';
    // this.imgName = '';
    this.lastTrade = null;
    this.variation = null;
    this.executeFirstTime = false;
    if ((this.symbol !== null || this.symbol !== '') && this.symbol.toLowerCase() !== symbol.toLowerCase()) {
      if (symbol.toLowerCase() && symbolTemp.toLowerCase() && symbol.toLowerCase() === symbolTemp.toLowerCase()) {
        return;
      }
      if (this.data && super.validateOnlyOneAssetAssigned(symbol, this.data.type, this.data.workspaceId) === true) {
        this._NotificationService.error('Erro', Constants.WORKSPACE_MSG015);
        this.symbol = '';
        super.updateOnDemand(symbolTemp, this.symbol, true);
        setTimeout(() => {
          this.symbol = symbolTemp;
          this.asset.symbol = symbolTemp;
          symbol = symbolTemp;
        }, 0);
        return;
      }
      if (assetWithoutSubscribe) {
        this.quoteService.unsubscribeQuote(this.symbol, Constants.WINDOW_NAME_BOOK);
        // if (this.subscriptionQuote) {
        // this.subscriptionQuote.unsubscribe(); // TODO remover
        // }
      }
      this.aggregatedBookService.unsubscribeAggregatedBook(this.symbol);
      this.bookService.unsubscribeBook(this.symbol);

      if (this.subscriptionBook) {
        this.subscriptionBook.unsubscribe();
      }
      if (this.subscriptionAggregatedBook) {
        this.subscriptionAggregatedBook.unsubscribe();
      }
    }

    if (assetWithoutSubscribe) {
      this.messageQuote = new MessageQuote();
      const msgItem = this.quoteService.subscribeQuote(symbol, Constants.WINDOW_NAME_BOOK);
      if (msgItem !== undefined && msgItem !== '') {
        this.assetValid = false;
        this.resetQuote(this.symbol);
        return;
      }
    } else {
      this.loadMessageQuote();
    }

    // this.aggregatedBookService.subscribeAggregatedBook(symbol);
    // this.bookService.subscribeBook(symbol);

    const oldSymbol = this.symbol;
    this.symbol = symbol;
    // if (this.data) {
    //   this.data.symbol = this.symbol;
    //   super.updateOnDemand(oldSymbol, this.symbol);
    // }

    this.rowOffersData = [];
    this.rowPriceData = [];
    this.tickSize = AppUtils.GetTickSize(symbol);
    this.isSubscribeBook = false;
    this.isUpdateOnDemand = false;
    this.subscriptionQuote = this.quoteService.messagesQuote
      .subscribe((msg: MessageQuote) => {
        if (msg && this.symbol && msg.errorMessage && msg.symbol.toLowerCase() === this.symbol.toLowerCase()) {
          this.hiddenInvalid = true;
          this.symbol = null;
          this.company = null;
          this.quoteService.unsubscribeQuote(msg.symbol.toLowerCase(), Constants.WINDOW_NAME_BOOK);
        } else if (msg !== undefined && msg.symbol != null && msg.symbol === this.symbol) {
          if (!this.isUpdateOnDemand) {
            this.isUpdateOnDemand = true;
            if (this.data) {
              this.data.symbol = this.symbol;
              super.updateOnDemand(oldSymbol, this.symbol);
            }
          }
          this.hiddenInvalid = false;
          this.messageQuote = msg;
          if (this.messageQuote.symbol.indexOf('win') === 0 || this.messageQuote.symbol.indexOf('wdo') === 0 ||
            this.messageQuote.symbol.indexOf('dol') === 0 || this.messageQuote.symbol.toLowerCase().indexOf('ind') === 0) {
            this.indiceFuture = true;
          } else {
            this.indiceFuture = false;
          }
          if (Number(this.messageQuote.status) === 3) {
            this.auctionHammer = true;
          } else {
            this.auctionHammer = false;
          }
          if (!this.isSubscribeBook && (this.messageQuote.marketType === 1 || this.messageQuote.marketType === 3)) {
            this.isSubscribeBook = true;
            if (this.tabChange && this.tabChange.index === 1) {
              this.subscriptionScktAggregatedBook(this.symbol.toLowerCase());
            } else {
              this.subscriptionScktBook(this.symbol.toUpperCase());
            }
          }
          if (this.messageQuote.errorMessage !== undefined && this.messageQuote.errorMessage !== '') {
            this.assetValid = false;
            // this.imgName = '';
            this.resetQuote(this.symbol);
          } else {
            this.loadMessageQuote();
          }
          if (msg.description && msg.description !== '' && this.messageQuote.marketType === Constants.MARKET_CODE_BOVESPA) {
            this.company = this.messageQuote.description + ' ' + this.messageQuote.classification;
          } else {
            this.company = this.messageQuote.description;
          }
          if (this.messageQuote.lastTrade && this.messageQuote.lastTrade.toString() === '-') {
            this.showVariation = false;
            this.messageQuote.lastTrade = this.messageQuote.previous;
          }
          if (this.messageQuote.dateLastNegotiation) {
            const today = this.datePipe.transform(new Date(), 'dd/MM/yyyy');
            if (today === this.messageQuote.dateLastNegotiation) {
              this.showVariation = true;
            } else {
              this.showVariation = false;
            }
          } else {
            this.showVariation = true;
          }
          // this.imgName = '/assets/img/assets/' + AppUtils.getImageSymbol(msg.symbol).toUpperCase() + '.PNG';
          this.calculatePressure();
        }
        this.loading = false;
      });
  }

  changeTab($event) {
    this.tabChange = $event;
    if (this.tabChange.index === 0) {
      if (this.messageQuote && this.messageQuote.symbol && (this.messageQuote.marketType === 1 || this.messageQuote.marketType === 3)) {
        this.loadingDetailBook = true;
        this.loadingAggregatedBook = false;
        if (this.subscriptionAggregatedBook) {
          this.subscriptionAggregatedBook.unsubscribe();
          if (this.symbol !== '') {
            if (this.aggregatedBookService) {
              this.aggregatedBookService.unsubscribeAggregatedBook(this.symbol);
              this.rowPriceData = [];
            }
          }
        }
        this.subscriptionScktBook(this.symbol);
      }
    } else {
      if (this.messageQuote && this.messageQuote.symbol && (this.messageQuote.marketType === 1 || this.messageQuote.marketType === 3)) {
        this.loadingAggregatedBook = true;
        this.loadingDetailBook = false;
        if (this.subscriptionBook) {
          this.subscriptionBook.unsubscribe();
          if (this.symbol !== '') {
            if (this.bookService) {
              this.bookService.unsubscribeBook(this.symbol);
              this.rowOffersData = [];
            }
          }
        }
        this.subscriptionScktAggregatedBook(this.symbol);
      }
    }
  }

  private loadMessageQuote() {
    this.assetValid = true;
    this.changingValue.next(true);
    if (!this.executeFirstTime) {
      this.setHeaderAssetNameColor();
      // this.imgName = '/assets/img/assets/' + AppUtils.getImageSymbol(this.messageQuote.symbol).toUpperCase() + '.PNG';
      this.status = '1';

      this.tickSize = this.messageQuote.tickSize;
      this.tickSize = AppUtils.GetTickSize(this.messageQuote.symbol, this.tickSize);
      if (this.messageQuote.description && this.messageQuote.description !== '') {
        this.company = this.messageQuote.description;
      }
      if (this.messageQuote.marketType === Constants.MARKET_CODE_BOVESPA) {
        this.company = this.messageQuote.description + ' ' + this.messageQuote.classification;
      }
    }
    this.executeFirstTime = true;

    this.last = this.lastTrade;
    if (this.messageQuote.lastTrade && this.messageQuote.lastTrade.toString() === '-') {
      this.messageQuote.lastTrade = this.messageQuote.previous;
    }
    this.lastTrade = AppUtils.convertNumberToCurrency(this.messageQuote.lastTrade, this.tickSize);
    this.changeBorderColor(this.messageQuote.lastTrade, this.lastTradePrevious);
    this.lastTradePrevious = this.messageQuote.lastTrade;

    this.variation = this.messageQuote.variation;
    if (this.messageQuote.status != null) {
      this.status = this.messageQuote.status.toString();
    } else {
      this.status = '0';
    }
    this.showStatus = this.status !== '0';
    this.low = this.messageQuote.low;
    this.high = this.messageQuote.high;
    if (this.messageQuote.lastTrade) {
      if (this.lastTrade < this.last) {
        this.style = 2;
      }
      if (this.lastTrade > this.last) {
        this.style = 1;
      }
    }
  }

  private subscriptionScktBook(symbol: string) {
    this.loadingDetailBook = true;
    this.loadingAggregatedBook = false;
    this.bookService.subscribeBook(symbol);
    this.subscriptionBook = this.bookService.messagesBook
      .subscribe((msg: MessageBook) => {
        if (msg !== undefined && msg.symbol != null && msg.symbol === this.symbol) {
          this.rowOffersData = [];
          let broker_purchase_short = '';
          let broker_sale_short = '';
          if (!msg.buy && !msg.sell) {
            return;
          }
          for (let i = 0; i < 15; i++) {
            // buy
            let broker_id = '';
            let broker_purchase = '';
            let qty_purchase = '';
            let purchase = '';
            // sell
            let sale = '';
            let qty_sale = '';
            let broker_sale_id = '';
            let broker_sale = '';

            if (msg.buy && msg.buy.length > i) {
              this.valueBuy = this.valueBuy + msg.buy[i].Q;
            }
            if (msg.sell && msg.sell.length > i) {
              this.valueSell = this.valueSell + msg.sell[i].Q;
            }

            if (msg.buy && msg.buy.length > i) {
              broker_id = msg.buy[i].C;
              broker_purchase = this._authServer.iQuote.getPlayerName(msg.buy[i].C);
              qty_purchase = msg.buy[i].FQ;
              purchase = AppUtils.convertNumberToCurrency(
                msg.buy[i].P,
                this.tickSize
              );
              if (broker_purchase.length > 11) {
                broker_purchase_short = broker_purchase.substr(0, 10) + '...';
              } else {
                broker_purchase_short = broker_purchase;
              }
            } else {
              broker_purchase_short = null;
            }

            if (msg.sell && msg.sell.length > i) {
              sale = AppUtils.convertNumberToCurrency(
                msg.sell[i].P,
                this.tickSize
              );
              qty_sale = msg.sell[i].FQ;
              broker_sale_id = msg.sell[i].C;
              broker_sale = this._authServer.iQuote.getPlayerName(msg.sell[i].C);
              if (broker_sale.length > 11) {
                broker_sale_short = broker_sale.substr(0, 10) + '...';
              } else {
                broker_sale_short = broker_sale;
              }
            } else {
              broker_sale_short = null;
            }

            if (broker_id || broker_sale_id) {
              this.rowOffersData.push({
                broker_id: broker_id,
                broker_purchase: broker_purchase,
                qty_purchase: qty_purchase,
                purchase: purchase,
                sale: sale,
                qty_sale: qty_sale,
                broker_sale_id: broker_sale_id,
                broker_sale: broker_sale,
                broker_purchase_short: broker_purchase_short,
                broker_sale_short: broker_sale_short
              });
            }
          }
          this.loadingDetailBook = false;
        }
      }, error => {
        this.loadingDetailBook = false;
      });
  }

  private subscriptionScktAggregatedBook(symbol: string) {
    this.loadingAggregatedBook = true;
    this.loadingDetailBook = false;
    this.aggregatedBookService.subscribeAggregatedBook(symbol);
    this.subscriptionAggregatedBook = this.aggregatedBookService.messagesAggregatedBook
      .subscribe((msg: MessageBook) => {
        if (msg !== undefined && msg.symbol != null && msg.symbol === this.symbol) {
          this.rowPriceData = [];
          this.valueBuy = 0;
          this.valueSell = 0;
          for (let i = 0; i < 15; i++) {
            if (msg.buy && msg.buy.length > i) {
              this.valueBuy = this.valueBuy + msg.buy[i].Q;
            }
            if (msg.sell && msg.sell.length > i) {
              this.valueSell = this.valueSell + msg.sell[i].Q;
            }
            let offers = '';
            let qty_purchase = '';
            let purchase = '';
            if (msg.buy && msg.buy.length > i) {
              offers = msg.buy[i].QTD;
              qty_purchase = msg.buy[i].FQ;
              purchase = AppUtils.convertNumberToCurrency(msg.buy[i].P, this.tickSize);
            }
            let sale = '';
            let qty_sale = '';
            let offers2 = '';
            if (msg.sell && msg.sell.length > i) {
              sale = AppUtils.convertNumberToCurrency(msg.sell[i].P, this.tickSize);
              qty_sale = msg.sell[i].FQ;
              offers2 = msg.sell[i].QTD;
            }
            if ((msg.buy && msg.buy.length > i) || (msg.sell && msg.sell.length > i)) {
              this.rowPriceData.push({
                offers: offers,
                qty_purchase: qty_purchase,
                purchase: purchase,
                sale: sale,
                qty_sale: qty_sale,
                offers2: offers2
              });
            }
          }
          this.loadingAggregatedBook = false;
        }
      }, error => {
        this.loadingAggregatedBook = false;
      });
  }

  setHeaderAssetNameColor() {
    if (this.assetValid) {
      this.headerAssetNameColor = '#888888';
    } else {
      this.headerAssetNameColor = '#c25150';
    }
  }

  openBillet(type: string, price: any) {
    if (type === 'buy') {
      this.billetOrderModalService.buySellOrder('buy', this.asset.symbol || '', price || 0.0);
    } else {
      this.billetOrderModalService.buySellOrder('sell', this.asset.symbol || '', price || 0.0);
    }
  }

  changeBorderColor(newValue: number, oldValue: number) {
    const refreshField = newValue !== oldValue;
    if (refreshField) {
      if (newValue > oldValue) {
        this.borderColor = '#419BF9';
      } else {
        this.borderColor = '#c14e4e';
      }
    } else {
      this.borderColor = '#898989';
    }
    setTimeout(() => {
      this.borderColor = '';
    }, 300);
  }

  openNewWindow() {
    const options = 'width=470px, height=500';
    const parameters = '?symbol=' + this.symbol;
    if (this.windowActive == null || this.windowActive.closed) {
      super.getNewWindow('book', parameters, options, this.data.id).subscribe(window => {
        this.windowActive = window;
      });
    } else {
      this.windowActive.focus();
    }
  }

  closeSheet() {
    if (this.windowActive != null) {
      this.windowActive.close();
    }
  }

  setActiveValid(value: boolean) {
    this.activeIsValid = value;
  }

  mouseEnterBrokerRow() {
    this.tooltiphover = true;

    setTimeout(() => {
      this.mouseLeaveBrokerRow();
    }, 8000);
  }

  mouseLeaveBrokerRow() {
    this.tooltiphover = false;
  }

  // save() {
  //   let varLeft = this.bookDiv.nativeElement.getBoundingClientRect().left;
  //   if (this.bookDiv.nativeElement.getBoundingClientRect().left > 100) {
  //     varLeft = this.bookDiv.nativeElement.getBoundingClientRect().left - 100;
  //   }
  //   let varTop = this.bookDiv.nativeElement.getBoundingClientRect().top;
  //   if (this.bookDiv.nativeElement.getBoundingClientRect().top > 75) {
  //     varTop = this.bookDiv.nativeElement.getBoundingClientRect().top - 75;
  //   }
  //   return {
  //     position: {
  //       left: varLeft,
  //       top: varTop
  //     },
  //     symbol: this.symbol,
  //     company: this.asset.company,
  //     type: Constants.WINDOW_NAME_BOOK,
  //     workspace: 'HOME'
  //   };
  // }

  // getViewContainerId(): string {
  //   return this.viewContainerId;
  // }

  // public onUpdateSymbol(textInput: any) {
  //   let symbol: string;
  //   if (textInput.target === undefined) {
  //     symbol = textInput();
  //   } else {
  //     symbol = textInput.target.value;
  //   }
  //   if (symbol.length >= environment.MIN_CARACTER_QUOTE) {
  //     this.setQuote(symbol);
  //   } else {
  //     this.assetValid = false;
  //     this.resetQuote(this.symbol);
  //   }
  // }

  // changeToogle(value) {
  //   if (value && value.index === 0) {
  //     this.showAggregatedBook = false;
  //   } else {
  //     this.showAggregatedBook = true;
  //   }
  // }

  // close() {
  //   this._route.navigate(['/home/main']);
  // }

  // getSymbolSearch(msgValue: MessageQuote) {
  //   const symbol = msgValue.symbol;
  //   this.assetValid = true;
  //   this.messageQuote = msgValue;
  //   this.setQuote(symbol, false);
  //   this.showBoxSearch = false;
  // }

  // getRightAlignment() {
  //   if (this.showBoxSearch) {
  //     return '-205px';
  //   } else {
  //     return '15px';
  //   }
  // }

  // public slice(value: number) {
  //   this.index = value;
  //   this.showAggregatedBook = value === 1;
  // }

}
