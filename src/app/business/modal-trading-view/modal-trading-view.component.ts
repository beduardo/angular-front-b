import { AutoLogoutService } from './../../auth/auto-logout-service.service';
import { SidebarService } from '@services/sidebar.service';
import { ConfigService } from '@services/webFeeder/config.service';
import { Component, OnInit, ViewChild, HostListener, OnDestroy } from '@angular/core';
import { MatDialogRef } from '@angular/material';
import { TradingViewComponent } from '@business/trading-view/trading-view.component';
import { WorkspaceNewService } from '@services/workspace-new.service';

@Component({
  selector: 'app-modal-trading-view',
  templateUrl: './modal-trading-view.component.html'
})
export class ModalTradingViewComponent implements OnInit, OnDestroy {

  title = 'GRÁFICOS';
  @ViewChild('tradingView') public tradingView: TradingViewComponent;

  private symbol;
  private windowActive: Window;


  constructor(
    public dialogRef: MatDialogRef<ModalTradingViewComponent>,
    public wksService: WorkspaceNewService,
    private sidebarService: SidebarService,
    private configService: ConfigService,
    private _autoLogoutService: AutoLogoutService
  ) { }

  @HostListener('document:keydown', ['$event'])
  onKeyDownHandler(event: KeyboardEvent) {
  }

  @HostListener('mousemove')
  onMouseMove() {
    this._autoLogoutService.resetTime();
  }

  @HostListener('click')
  onClick() {
    this._autoLogoutService.resetTime();
  }

  ngOnInit() {
    this.wksService._openTradingView$.subscribe(res => {
      if (!res) {
        this.closeSheet();
      }
    });
  }

  ngOnDestroy() {
  }

  closeSheet() {
    if (this.windowActive) {
      this.windowActive.close();
    }
  }

  call(value) {
    this.symbol = value;
  }

  close() {
    this.dialogRef.close();
  }
  createChart() {
    this.tradingView.createChart();
  }

  expand() {
    if (!window.closed) {
      const options = 'width=1203px, height=768px';
      if (this.symbol) {
        const token = this.configService.getTokenGraphic();
        const parameters = 'chart?symbol=' + this.symbol + '&token=' + token;
        this.windowActive = window.open(parameters, '', options);
      } else {
        this.windowActive = window.open('chart', '', options);
      }
      this.sidebarService.saveDetachedWindow(this.windowActive);
      this.dialogRef.close();
    }
  }
}
