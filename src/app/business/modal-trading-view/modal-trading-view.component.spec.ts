import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalTradingViewComponent } from '@business/modal-trading-view/modal-trading-view.component';

describe('ModalTradingViewComponent', () => {
  let component: ModalTradingViewComponent;
  let fixture: ComponentFixture<ModalTradingViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalTradingViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalTradingViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
