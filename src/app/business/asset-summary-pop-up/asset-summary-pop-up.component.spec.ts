import { HttpClientModule } from '@angular/common/http';
import { HttpModule } from '@angular/http';
import { RouterTestingModule } from '@angular/router/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { MatIconModule, MatDividerModule, MatTabsModule, MatProgressSpinnerModule, MatTooltipModule, MatFormFieldModule, MatOptionModule, MatAutocompleteModule, MatCardModule, MatDialogModule, MatDialogRef, MatBottomSheetModule } from '@angular/material';
import { AssetSummaryComponent } from './../asset-summary/asset-summary.component';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { AssetSummaryPopUpComponent } from '@business/asset-summary-pop-up/asset-summary-pop-up.component';
import { BrowserDynamicTestingModule } from '@angular/platform-browser-dynamic/testing';
import { AutoCompleteComponent } from '@component/auto-complete/auto-complete.component';
import { SpinnerComponent } from '@component/spinner/spinner.component';
import { AutoSizeInputDirective } from '@shared/directives/autosize-input.directive';
import { ConfigService } from '@services/webFeeder/config.service';
import { ConfigServiceMock, CustomSnackbarServiceMock } from '@services/_test-mock-services/_mock-services';
import { NotificationsService, SimpleNotificationsModule } from 'angular2-notifications';
import { CustomSnackbarService } from '@services/custom-snackbar-service.service';
import { DatePipe } from '@angular/common';

describe('AssetSummaryComponentPopUp', () => {
  let component: AssetSummaryPopUpComponent;
  let fixture: ComponentFixture<AssetSummaryPopUpComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        AssetSummaryPopUpComponent,
        AssetSummaryComponent,
        AutoCompleteComponent,
        SpinnerComponent,
        AutoSizeInputDirective
      ],
      imports:[
        BrowserDynamicTestingModule,
        MatIconModule,
        MatDividerModule,
        MatTabsModule,
        MatProgressSpinnerModule,
        MatTooltipModule,
        MatFormFieldModule,
        MatOptionModule,
        MatAutocompleteModule,
        ReactiveFormsModule,
        MatCardModule,
        RouterTestingModule,
        HttpModule,
        HttpClientModule,
        SimpleNotificationsModule.forRoot(),
        MatDialogModule,
        MatBottomSheetModule
      ],
      providers:[
        DatePipe,
        NotificationsService,
        { provide: ConfigService, useClass: ConfigServiceMock },
        { provide: CustomSnackbarService, useClass: CustomSnackbarServiceMock },
        { provide: MatDialogRef, useValue: {} },
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AssetSummaryPopUpComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
