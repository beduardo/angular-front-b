import { Box } from '@business/home/workspace-new/workspace-new.component';
import { ActivatedRoute } from '@angular/router';
import { AssetSummary } from '@model/asset-summary.model';
import { Component, OnInit, Input, Inject, Optional } from '@angular/core';
import { MessageQuote } from '@model/webFeeder/message-quote.model';
import { Constants } from '@utils/constants';
import { WorkspaceNewDto } from '@dto/workspacenew.dto';
import { WorkspaceItemContentDto } from '@dto/workspace-item-content-dto';
import { WorkspaceNewSaveDto } from '@dto/workspace-new-save-dto';
import { NormalModalComponent } from '@component/normal-modal/normal-modal.component';
import { WorkspaceNewService } from '@services/workspace-new.service';
import { MatDialog, MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { PositionDto } from '@dto/position.dto';
import { AbstractItemService } from '@business/base/abstract-item.service';
import { AbstractItem } from '@business/base/abstract-item';
import { NotificationsService } from 'angular2-notifications';

@Component({
  selector: Constants.WINDOW_NAME_ASSET_SUMMARY + '-pop-up',
  templateUrl: './asset-summary-pop-up.component.html'
})
export class AssetSummaryPopUpComponent extends AbstractItem implements OnInit {
  @Input()
  set assetSymbol(value: any) {
    this.symbol = value;
  }
  get assetSymbol(): any {
    return this.symbol;
  }
  loading = true;
  public setSymbol;
  public example: Box;
  @Input() data: any;
  @Input()
  set quote(value: MessageQuote) {
    if (value.theoryQuantity === '-') {
      value.theoryQuantity = '0';
    }
    if (value.timeToOpen === '00:00:00') {
      value.timeToOpen = '-';
    }

    this._messageQuote = value;
  }
  get quote(): MessageQuote {
    return this._messageQuote;
  }
  public symbol: string = null;
  public _messageQuote = new MessageQuote();
  public assetModel: AssetSummary;
  dialog: MatDialog;

  constructor(
    workspaceService: WorkspaceNewService,
    dialog: MatDialog,
    public dialogRef: MatDialogRef<AssetSummaryPopUpComponent>,
    private route: ActivatedRoute,
    itemService: AbstractItemService,
    @Optional() @Inject(MAT_DIALOG_DATA) public injectedSymbol,
    public _NotificationService: NotificationsService
  ) {
    super(workspaceService, dialog, itemService);
    this.dialog = dialog;
  }

  ngOnInit() {
    if (this.injectedSymbol) {
      this.data = this.injectedSymbol.injectedSymbol;
    }
  }

  addItemToWorkspace() {
    const userWorkspaceSelected: WorkspaceNewDto = this.workspaceService.getUserWorkspaceById(this.workspaceService.getWksSelectedId());
    const contentToUpdate: Array<WorkspaceItemContentDto> = JSON.parse(userWorkspaceSelected.JsonData).content;
    const workspaceUpdated: WorkspaceNewSaveDto = new WorkspaceNewSaveDto(userWorkspaceSelected.Name, [], userWorkspaceSelected.IndexTab);
    const itemSettings = this.workspaceService.getItemDefaultSettings(Constants.WINDOW_NAME_ASSET_SUMMARY);
    const wksItemPos: PositionDto = this.workspaceService.mountItemPositionDefaultSettings(itemSettings);
    const newContent = new WorkspaceItemContentDto(this.workspaceService.getNextContentItemId(contentToUpdate),
      this.data.symbol, this.data.company, Constants.WINDOW_NAME_ASSET_SUMMARY, wksItemPos,
      (itemSettings && itemSettings.showInGridster) || false, userWorkspaceSelected.Id || 0);
    if (this.validateOnlyOneAssetAssigned(newContent.symbol.toString(), newContent.type, userWorkspaceSelected.Id) === true) {
      this._NotificationService.error('Erro', Constants.WORKSPACE_MSG015);
      return;
    }
    if (contentToUpdate) {
      contentToUpdate.push(newContent);
      workspaceUpdated.content = contentToUpdate;
      if (workspaceUpdated.content.length > 10) {
        this._NotificationService.warn('', Constants.WORKSPACE_MSG011);
      } else {
        this.dialog.closeAll();
        const myEl = document.getElementById('toFocus') as HTMLInputElement;
        myEl.value = '';
        this.dialog.open(NormalModalComponent, {
          data: {
            title: '',
            body: Constants.WKS_NEW_COMPOENENT,
            redirectUrl: Constants.HOME_WKS_PATH,
            queryParams: {
              workspaceId: this.workspaceService.getWksSelectedId(),
              deleteCmp: true, workspaceUpdated: JSON.stringify(workspaceUpdated)
            }
          },
          panelClass: 'mat-modal-normal',
        });
      }
    }
  }

}
