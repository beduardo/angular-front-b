import { Component, OnInit, ViewChild, ElementRef, Renderer } from '@angular/core';
import { MatDialogRef } from '@angular/material';

@Component({
  selector: 'app-assessor-data',
  templateUrl: './assessor-data.component.html'
})
export class AssessorDataComponent implements OnInit {

  @ViewChild('index') index: ElementRef;
  @ViewChild('body') body: ElementRef;
  @ViewChild('filter') filterEl: ElementRef;
  @ViewChild('blankSpace') blankSpaceEl: ElementRef;
  public selected = 0;
  public scrollTop: number;
  public loading: boolean;

  constructor(
    public dialogRef: MatDialogRef<AssessorDataComponent>,
    private _renderer: Renderer,
  ) { }

  ngOnInit() {
  }

  closeDialog() {
    this.dialogRef.close();
    const elem = document.getElementById('sidebar-left');
    elem.style.zIndex = '1';
  }

  onScroll(event) {
    if (this.scrollTop === undefined || this.scrollTop === null) {
      this.scrollTop = this.body.nativeElement.scrollTop;
    }

    if (this.selected === 1) {
      const filterHeight = this.filterEl.nativeElement.offsetHeight;
      if (this.body.nativeElement.scrollTop >= 166) {
        this._renderer.setElementStyle(this.index.nativeElement, 'position', 'fixed');
        this._renderer.setElementStyle(this.index.nativeElement, 'top', '138px');
        this._renderer.setElementStyle(this.index.nativeElement, 'width', 'calc(100% - 209px)');
        this._renderer.setElementStyle(this.index.nativeElement, 'z-index', '1');

        if (this.scrollTop > this.body.nativeElement.scrollTop) {
          this._renderer.setElementStyle(this.filterEl.nativeElement, 'position', 'fixed');
          this._renderer.setElementStyle(this.filterEl.nativeElement, 'top', '93px');
          this._renderer.setElementStyle(this.filterEl.nativeElement, 'width', 'calc(100% - 209px)');
          this._renderer.setElementStyle(this.filterEl.nativeElement, 'z-index', '1');

          this._renderer.setElementStyle(this.index.nativeElement, 'top', `${136 + filterHeight}px`);
        } else {
          this._renderer.setElementStyle(this.filterEl.nativeElement, 'position', 'fixed');
          this._renderer.setElementStyle(this.filterEl.nativeElement, 'top', '-500px');
          this._renderer.setElementStyle(this.filterEl.nativeElement, 'width', 'calc(100% - 209px)');
          this._renderer.setElementStyle(this.filterEl.nativeElement, 'z-index', '1');

          this._renderer.setElementStyle(this.index.nativeElement, 'top', '138px');
        }

        this._renderer.setElementStyle(this.blankSpaceEl.nativeElement, 'height', `${50 + filterHeight}px`);
      } else {

        if (this.body.nativeElement.scrollTop <= 166 - filterHeight - 50) {
          this._renderer.setElementStyle(this.index.nativeElement, 'position', 'unset');
          this._renderer.setElementStyle(this.index.nativeElement, 'width', 'unset');
          this._renderer.setElementStyle(this.index.nativeElement, 'z-index', 'unset');

          this._renderer.setElementStyle(this.filterEl.nativeElement, 'position', 'unset');
          this._renderer.setElementStyle(this.filterEl.nativeElement, 'width', 'unset');
          this._renderer.setElementStyle(this.filterEl.nativeElement, 'z-index', 'unset');
          this._renderer.setElementStyle(this.blankSpaceEl.nativeElement, 'height', '0px');
        }
      }
    } else {
      const filterHeight = this.filterEl.nativeElement.offsetHeight;
      if (this.body.nativeElement.scrollTop >= 208) {
        this._renderer.setElementStyle(this.index.nativeElement, 'position', 'fixed');
        this._renderer.setElementStyle(this.index.nativeElement, 'top', '138px');
        this._renderer.setElementStyle(this.index.nativeElement, 'width', 'calc(100% - 209px)');
        this._renderer.setElementStyle(this.index.nativeElement, 'z-index', '1');

        if (this.scrollTop > this.body.nativeElement.scrollTop) {
          this._renderer.setElementStyle(this.filterEl.nativeElement, 'position', 'fixed');
          this._renderer.setElementStyle(this.filterEl.nativeElement, 'top', '93px');
          this._renderer.setElementStyle(this.filterEl.nativeElement, 'width', 'calc(100% - 209px)');
          this._renderer.setElementStyle(this.filterEl.nativeElement, 'z-index', '1');

          this._renderer.setElementStyle(this.index.nativeElement, 'top', `${136 + filterHeight}px`);
        } else {
          this._renderer.setElementStyle(this.filterEl.nativeElement, 'position', 'fixed');
          this._renderer.setElementStyle(this.filterEl.nativeElement, 'top', '-500px');
          this._renderer.setElementStyle(this.filterEl.nativeElement, 'width', 'calc(100% - 209px)');
          this._renderer.setElementStyle(this.filterEl.nativeElement, 'z-index', '1');

          this._renderer.setElementStyle(this.index.nativeElement, 'top', '138px');
        }

        this._renderer.setElementStyle(this.blankSpaceEl.nativeElement, 'height', `${50 + filterHeight}px`);
      } else {

        if (this.body.nativeElement.scrollTop <= 208 - filterHeight - 50) {
          this._renderer.setElementStyle(this.index.nativeElement, 'position', 'unset');
          this._renderer.setElementStyle(this.index.nativeElement, 'width', 'unset');
          this._renderer.setElementStyle(this.index.nativeElement, 'z-index', 'unset');

          this._renderer.setElementStyle(this.filterEl.nativeElement, 'position', 'unset');
          this._renderer.setElementStyle(this.filterEl.nativeElement, 'width', 'unset');
          this._renderer.setElementStyle(this.filterEl.nativeElement, 'z-index', 'unset');
          this._renderer.setElementStyle(this.blankSpaceEl.nativeElement, 'height', '0px');
        }
      }
    }
    this.scrollTop = this.body.nativeElement.scrollTop;
  }

  changeScreen(value) {
    // this.setDefaultValues();
    if (this.selected !== value) {
      // this.setDefaultValues();
      this.selected = value;
      // if (this.selected === 0) {
      //   this._dailyOrderService.unsubscribeDailyOrder(this.account);
      //   this.getHistory();
      // } else {
      //   this._dailyOrderService.subscribeDailyOrderBovespa(this.account);
      //   this._dailyOrderService.subscribeDailyOrderBmf(this.account);
      // }
    }
  }

}
