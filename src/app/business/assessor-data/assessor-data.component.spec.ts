import { SpinnerComponent } from '@component/spinner/spinner.component';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AssessorDataComponent } from './assessor-data.component';
import { MatIconModule, MatProgressSpinnerModule, MatCardModule, MatDialogRef } from '@angular/material';

describe('AssessorDataComponent', () => {
  let component: AssessorDataComponent;
  let fixture: ComponentFixture<AssessorDataComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AssessorDataComponent,
      SpinnerComponent ],
      imports:[
        MatIconModule,
        MatProgressSpinnerModule,
        MatCardModule

      ],
      providers:[
        { provide: MatDialogRef, useValue: {} },
      ]

    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AssessorDataComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
