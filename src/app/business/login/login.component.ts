import { TransitionService, TransitionUser } from './../../services/transition.service';
import { Component, Inject, Input, OnInit, ViewChild, ViewContainerRef, Renderer2, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthService } from '@auth/auth.service';
import { AuthenticationService } from '@services/authentication.service';
import { CustomSnackbarService } from '@services/custom-snackbar-service.service';
import { LayoutService } from '@services/layout.service';
import { ConfigService } from '@services/webFeeder/config.service';
import { TokenService } from '@services/webFeeder/token.service';
import { WorkspaceNewService } from '@services/workspace-new.service';
import { Constants, Profile } from '@utils/constants';
import { NgForm } from '@angular/forms';
import { MatCheckbox, MatDialog } from '@angular/material';
import { UserCA } from '@model/wso2/userca';
import { Assessor } from '@model/wso2/assessor';
import { Client } from '@model/wso2/client';
import { AppUtils } from '@utils/app.utils';
import { ModalPasswordComponent } from '@component/modal-password/modal-password.component';
import { environment } from 'environments/environment';
import { ErrorMessage } from '@model/error-message';
import { SocketService } from '@services/webFeeder/socket.service';
import { AuthServer } from '@services/auth-server.service';
import { NotificationsService } from 'angular2-notifications';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html'
})
export class LoginComponent implements OnInit, OnDestroy {

  @ViewChild('snackBar', { read: ViewContainerRef }) parent: ViewContainerRef;
  @ViewChild('loginForm') loginForm: NgForm;
  @ViewChild('checkboxButton') checkboxButton: MatCheckbox;
  @Input() logo = './assets/images/logo.png';
  private returnUrl = 'home';
  private client: Client;
  private assessor: Assessor;
  private transitionUser: TransitionUser;
  loading: boolean;
  loadingTransition = true;
  showpassword = false;
  icon = 'crossed-eye';
  model: any = {};
  brokers: string[];
  isUserLogged = false;
  dialog: MatDialog;
  typeLogin: string;
  tokenPi: string;
  isRequest = false;
  timerLoginWSO2: any;
  countTimerLoginWSO2 = 0;
  whitelabaleActive: number;
  urlHbWhitelabel: string;
  title = Constants.LOGIN_MSG_TITLE;
  subTitle = Constants.LOGIN_MSG_SUBTITLE;
  titleRanking = Constants.LOGIN_MSG_TITLE_RANKING;
  subTitleRanking = Constants.LOGIN_MSG_SUBTITLE_RANKING;
  titleDoubleAuthentication = Constants.LOGIN_MSG_TITLE_AUTHENTICATION;
  phone1: string;
  showPhone: boolean;
  showDoubleAuthetication: boolean;
  listQuestionDoubleAuthentication: any;
  questionActive: any = {};
  dataLoginDoubleAuth: any = {};

  constructor(
    dialog: MatDialog,
    private configService: ConfigService,
    private authService: AuthService,
    private router: Router,
    private workService: WorkspaceNewService,
    private authenticationService: AuthenticationService,
    public snackBar: CustomSnackbarService,
    private tokentService: TokenService,
    private layoutService: LayoutService,
    private renderer: Renderer2,
    @Inject(ViewContainerRef) public msgPlaceholder: ViewContainerRef,
    private transitionService: TransitionService,
    private _route: ActivatedRoute,
    private modalService: MatDialog,
    private socketService: SocketService,
    private _authServer: AuthServer,
    private _notificationService: NotificationsService,
    private _titleService: Title
  ) {
    this.dialog = dialog;
  }

  ngOnInit(): void {
    this.questionActive.questionResp = '';
    Constants.WHITELABEL.forEach(element => {
      if (element.cod === environment.WHITELABEL) {
        this._titleService.setTitle(element.whitelabel_name);
        this.configService.writeLog('LOGIN > WHITELABEL: [' + element.cod + '][' + element.whitelabel_name + '][' + element.api + ']');
        this.whitelabaleActive = element.cod;
        if (element.api !== 'WSO2' && element.whitelabel_name !== 'PI') {
          this.loadingTransition = false;
        } else if (element.api === 'WSO2' && element.cod === 6) {
          this.loadingTransition = false;
        }
        if (element.phone1 && element.phone1 !== '') {
          this.showPhone = true;
          this.phone1 = element.phone1;
        }
        if (element.urlHbWhitelabel) {
          this.urlHbWhitelabel = element.urlHbWhitelabel;
        }
      }
    });

    // WSO2
    if (this.configService.isLoginTypeWSO2()) {
      this.configService.writeLog('LOGIN > LoginWSO2');
      this.transitionUser = this.transitionService.get();
      if (this.transitionUser) {
        this.configService.writeLog('\n\nLOGIN > ' + new Date().toString().slice(16, 25) + ' - CARREGA PAGINA DE LOGIN\n\n');
        this.timerLoginWSO2 = setInterval(() => {
          this.configService.writeLog('\nLOGIN > - TEMPO TELA DE LOGIN (5s) - ' + new Date().toString().slice(16, 25)
            + ' >>>> countTimerLoginWSO2: ' + this.countTimerLoginWSO2);
          this.countTimerLoginWSO2++;
          if (this.countTimerLoginWSO2 === 1) {
            // transition - RB - Cedro
            if (this.transitionUser && (this.transitionUser.HBA_USERNAME || this.transitionUser.SYSTEM_USERNAME)
              && this.transitionUser.TKNAPIREFRESH) {
              this.configService.writeLog('LOGIN > START LOCAL_TRANSITION >>>');
              this.configService.writeLog(this.transitionUser);
              this.configService.setTokenRefreshApi(this.transitionUser.TKNAPIREFRESH);
              this.loginTransition();
              this.configService.writeLog('LOGIN > END LOCAL_TRANSITION >>>\n');
            } else {
              this.workService.updateShowMessageToAddWks(false);
              this.configService.deleteBrokerCookies();
              this.configService.deleteCookie();
              this.configService.deleteHbBrokerUser();
            }
          }
          if (this.countTimerLoginWSO2 > 1) {
            clearInterval(this.timerLoginWSO2);
          }
        }, 1000);
      } else if (this.transitionUser) {
        // Redireciona o usuario para a tela de login (WSO2)
        window.location.href = this.configService.REDIRECT_LOGIN;
      }
    } else {
      // Captura o token da url
      this._route.queryParams.subscribe(params => {
        this.tokenPi = params.token;
      });

      // Valida se o login é via Token
      if (this.tokenPi) {
        this.configService.writeLog('LOGIN > LoginToken: ' + this.tokenPi);
        this.authenticationService.iLogin.loginToken(this.tokenPi)
          .subscribe(data => {
            if (data && data.accountNumber && data.token) {
              const _user = new UserCA();
              _user.name = data.accountNumber;
              const user = { username: data.accountNumber, firstName: 'Juvenal', lastName: 'Silva' }; // TODO retirar mock
              this.configService.setHbCurrentUser(user);
              this.setConfigLogin(data, _user);
            } else if (data && (data.code === 401 || data.code === '401')) {
              this.socketService.disconnect();
              this.authService.logoutWithoutRouter();
              this.modalService.closeAll();
              this.router.navigate(['error'], {
                queryParams: new ErrorMessage(Constants.ERROR_EXPIRED_ICON, Constants.ERROR_EXPIRED_TITLE, Constants.ERROR_EXPIRED_TEXT,
                  Constants.ERROR_EXPIRED_BUTTON, 'login')
              });
            }
          }, error => {
            this.socketService.disconnect();
            this.authService.logoutWithoutRouter();
            this.modalService.closeAll();
            this.router.navigate(['error'], {
              queryParams: new ErrorMessage(Constants.ERROR_EXPIRED_ICON, Constants.ERROR_EXPIRED_TITLE, Constants.ERROR_EXPIRED_TEXT,
                Constants.ERROR_EXPIRED_BUTTON, 'login')
            });
          });
      } else if (this.whitelabaleActive !== 2) {
        this.configService.writeLog('LOGIN > LoginWF');
        // Limpa o cache
        const firstnameTemp: any = this.configService.getHbCurrentUser();
        if (firstnameTemp) {
          this.configService.deleteHbCurrentUser();
        }
        if (window.sessionStorage.length > 0) {
          this.workService.updateShowMessageToAddWks(false);
          this.configService.deleteBrokerCookies();
          this.configService.deleteCookie();
          this.configService.deleteHbBrokerUser();
          this.configService._authHbLoginType.next(3);
          if (this.configService.isLoginTypeWSO2() && this.configService.isUserAssessorLogged()) {
            window.location.href = this.configService.REDIRECT_LOGOUT + '/assessor/login';
          } else if (this.configService.isLoginTypeWSO2() && !this.configService.isUserAssessorLogged()) {
            window.location.href = this.configService.REDIRECT_LOGOUT + '/client/login';
          } else {
            this.router.navigate(['/login']);
          }
        }
      }
    }
    this.loading = false;
    this.brokers = ['Cedro'];
    this.typeLogin = this.configService.getLoginTypeEnabled();
  }

  ngOnDestroy() {
    if (this.timerLoginWSO2) {
      clearInterval(this.timerLoginWSO2);
    }
  }

  private getIp(timeoutErrorPostLogin = 3000) {

    const tryGetIp = () => {
      setTimeout(() => {
        this.getIp(timeoutErrorPostLogin * 1.5);

      }, timeoutErrorPostLogin);
    };

    this.authenticationService
      .getIp()
      .subscribe((resp) => {
        if (resp) {
          this.configService.setIP(resp);
        }
      },
        (error) => {
          tryGetIp();
        }
      );
  }

  login() {
    this.loading = true;
    if (!(this.model.username || this.model.password)) {
      this.setSnackBar();
      this.snackBar.createSnack(this.parent, Constants.MSG_LOGIN_MSG001, Constants.ALERT_TYPE.info);
      this.loading = false;
    }
    this.tryLogin(this.model.username, this.model.password);
  }

  private loginTransition() {
    this.setParamsCookieTransition();
    this.authenticationService.iLogin.refreshTransition(this.transitionUser.TKNAPIREFRESH)
      .subscribe(res => {
        this.configService.writeLog('LOGIN > LOGINTRANSITION >>> REFRESH VALIDO: ' + res);
        if (res) {
          this.workService.setLogged();
          setTimeout(() => {
            this.isUserLogged = true;
            this.loading = !this.loading;
          }, 700);
          setTimeout(() => {
            const leftElement = document.getElementById('left-content');
            if (leftElement) {
              leftElement.classList.add('slideOutLeft');
            }
            const carouselExampleIndicators = document.getElementById('carouselExampleIndicators');
            if (carouselExampleIndicators) {
              carouselExampleIndicators.classList.add('slideOutRight');
            }
            const fullPage = document.getElementById('app-login');
            if (fullPage) {
              fullPage.classList.remove('fadeIn');
              fullPage.classList.add('fadeOut');
            }
          }, 1500);
          this.configService.setFavoriteThemeUser('false');
          this.renderer.removeClass(document.body, 'darkTheme');
          this.configService._authHbLoginType.next(Number(this.transitionUser.USER_LOGIN_TYPE));
          if (!this.transitionUser.IS_ASSESSOR) {
            this.configService.setUserLoginType(Profile.Client);
            this.configService.setHbAccount(this.transitionUser.HBA.toString());
            this.configService.setHbAccountId(this.transitionUser.HBA_ID);
            this.configService.setHbAccountName(this.transitionUser.HBA_USERNAME);
            this.configService.setHbEmail(this.transitionUser.HBA_EMAIL);
            this.configService.setUserSystemLogged(this.transitionUser.SYSTEM_USERNAME);
            this.authService.setUserLogged(this.transitionUser.HBA_USERNAME.split(/\s+/)[0]);
          } else {
            this.configService.setUserLoginType(Profile.Assessor);
            this.authService.setUserLogged(this.transitionUser.SYSTEM_USERNAME);
            this.configService.setUserSystemLogged(this.transitionUser.SYSTEM_USERNAME);
            if (this.transitionUser.ASSESSOR_LIST_CLIENT) {
              const clients = JSON.parse(this.transitionUser.ASSESSOR_LIST_CLIENT);
              if (clients) {
                clients.forEach(element => {
                  element.user.first_name = element.user.full_name.split(' ')[0];
                  element.user.account_id = element.user.advisor_client_accounts[0].account_id;
                  if (element.user.profile_name && element.user.profile_name.toLowerCase() === 'moderado') {
                    element.user.profile_color = 'yellow';
                  } else if (element.user.profile_name && element.user.profile_name.toLowerCase() === 'balanceado') {
                    element.user.profile_color = 'purple';
                  } else if (element.user.profile_name && element.user.profile_name.toLowerCase() === 'arrojado') {
                    element.user.profile_color = 'orange';
                  } else if (element.user.profile_name && element.user.profile_name.toLowerCase() === 'agressivo') {
                    element.user.profile_color = 'red';
                  } else if (element.user.profile_name && element.user.profile_name.toLowerCase() === 'conservador') {
                    element.user.profile_color = 'blue';
                  } else {
                    element.user.profile_color = 'blue';
                  }
                });
              }
              this.configService.setAssessorListClients(clients);
              const clientSelected = JSON.parse(this.transitionUser.ASSESSOR_CLIENT_SELECTED);
              if (clientSelected) {
                this.configService.setAssessorClientSelected(clientSelected);
              } else {
                this.configService.setAssessorClientSelected(clients[0]);
              }
            }
          }
          this.existsUserWorkspace()
            .then(res => {
              setTimeout(() => {
                this.router.navigate([this.returnUrl], {
                  queryParams: {
                    existsUserWorkspace: res
                  }
                });
              }, 2400);
            });

          this.tokentService.assignServices();
          this.tokentService.sendStatus(true);
          const typeLoginHB = this.transitionUser.HB_LOGIN_TYPE;
          this.configService.setHbLoginTypeEnable(Constants.LOGIN_W);
          this.configService.setBrokerAccount(this.configService.getHbAccount(), typeLoginHB);
          this.configService._authHbLoginType.next(Number(typeLoginHB));
          this.configService._authHbLogged.next(true);
          this.renderer.removeClass(document.body, 'darkTheme');
        } else {
          this.dialog.closeAll();
          this.configService.deleteHbBrokerUser();
          this.configService.deleteFavoriteThemeUser();
          window.sessionStorage.clear();
          this.configService.redirectExpireSession();
        }
      });
  }

  setParamsCookieTransition() {
    this.configService._authHbLoginType.next(Number(this.transitionUser.USER_LOGIN_TYPE));
    if (!this.transitionUser.IS_ASSESSOR) {
      this.configService.setUserLoginType(Profile.Client);
      this.configService.setHbAccount(this.transitionUser.HBA.toString());
      this.configService.setHbAccountId(this.transitionUser.HBA_ID);
      this.configService.setHbAccountName(this.transitionUser.HBA_USERNAME);
      this.configService.setHbEmail(this.transitionUser.HBA_EMAIL);
      this.configService.setUserSystemLogged(this.transitionUser.SYSTEM_USERNAME);
      this.configService.setProfileName(this.transitionUser.CLIENT_PROFILE_NAME);
      this.authService.setUserLogged(this.transitionUser.HBA_USERNAME.split(/\s+/)[0]);
      this.configService.setHbUserId(this.transitionUser.HBA_USER_ID);
    } else {
      this.configService.setUserLoginType(Profile.Assessor);
      this.configService.setAssessorId(this.transitionUser.HBA_ASSESSOR_ID);
      this.authService.setUserLogged(this.transitionUser.SYSTEM_USERNAME);
      this.configService.setUserSystemLogged(this.transitionUser.SYSTEM_USERNAME);
      if (this.transitionUser.ASSESSOR_LIST_CLIENT) {
        const clients = JSON.parse(this.transitionUser.ASSESSOR_LIST_CLIENT);
        if (clients) {
          clients.forEach(element => {
            element.user.first_name = element.user.full_name.split(' ')[0];
            element.user.account_id = element.user.advisor_client_accounts[0].account_id;
            if (element.user.profile_name && element.user.profile_name.toLowerCase() === 'moderado') {
              element.user.profile_color = 'yellow';
            } else if (element.user.profile_name && element.user.profile_name.toLowerCase() === 'balanceado') {
              element.user.profile_color = 'purple';
            } else if (element.user.profile_name && element.user.profile_name.toLowerCase() === 'arrojado') {
              element.user.profile_color = 'orange';
            } else if (element.user.profile_name && element.user.profile_name.toLowerCase() === 'agressivo') {
              element.user.profile_color = 'red';
            } else if (element.user.profile_name && element.user.profile_name.toLowerCase() === 'conservador') {
              element.user.profile_color = 'blue';
            } else {
              element.user.profile_color = 'blue';
            }
          });
        }
        this.configService.setAssessorListClients(clients);
        const clientSelected = JSON.parse(this.transitionUser.ASSESSOR_CLIENT_SELECTED);
        if (clientSelected) {
          this.configService.setAssessorClientSelected(clientSelected);
        } else {
          this.configService.setAssessorClientSelected(clients[0]);
        }
      }
    }
    this.configService.setHbLoginTypeEnable(Constants.LOGIN_W);
    const typeLoginHB = this.transitionUser.HB_LOGIN_TYPE;
    this.configService.setBrokerAccount(this.configService.getHbAccount(), typeLoginHB);
    this.configService.setToken(this.transitionUser.TKNWF);
    this.configService.setTokenApi(this.transitionUser.TKNWF);
    this.configService.setTokenGraphic(this.transitionUser.TKNWF);
    this.configService.setTokenRefreshApi(this.transitionUser.TKNAPIREFRESH);
    this.configService.setScope('default');
    this.configService.setTokenTp('Bearer');
    this.configService.setBrokerAccountSelectedTransition(this.transitionUser.HB_BROKER_SELECT);
    this.configService.setHbCurrentUser(this.transitionUser.HBA_CURRENT_USER);
  }

  private tryLogin(user: string, password: string) {
    try {
      this.getIp();
      if (!this.isRequest) {
        this.isRequest = true;
        const _user = new UserCA();
        if (Constants.LOGIN_TYPE.LoginW === this.typeLogin) {
          if (user && password) {
            this.model.username = this.model.username.trim();
            this.model.password = this.model.password.trim();
            _user.login = this.model.username;
            _user.password = this.model.password;
            if (isNaN(this.model.username)) {
              _user.profile = Profile.Assessor;
            } else {
              _user.profile = Profile.Client;
            }
          } else {
            this.setSnackBar();
            this.snackBar.createSnack(this.parent, Constants.MSG_LOGIN_MSG001, Constants.ALERT_TYPE.error);
            this.loginForm.controls['password'].setErrors({ 'incorrect': true });
            this.loginForm.controls['username'].setErrors({ 'incorrect': true });
            this.isRequest = false;
          }
        } else {
          if (user && password) {
            user = user.trim();
            password = password.trim();
          } else {
            this.setSnackBar();
            this.snackBar.createSnack(this.parent, Constants.MSG_LOGIN_MSG001, Constants.ALERT_TYPE.info);
            this.loginForm.controls['password'].setErrors({ 'incorrect': true });
            this.loginForm.controls['username'].setErrors({ 'incorrect': true });
            this.isRequest = false;
            this.loading = false;
            return false;
          }
          _user.login = user;
          _user.password = password;
        }
        this.authenticationService.login(_user.login, _user.password, _user.profile)
          .subscribe((data: any) => {
            if (data === 'INVALID_CREDENTIALS') {
              this.setSnackBar();
              this.snackBar.createSnack(this.parent, Constants.MSG_LOGIN_MSG002, Constants.ALERT_TYPE.error);
              this.loginForm.controls['password'].setErrors({ 'incorrect': true });
              this.loginForm.controls['username'].setErrors({ 'incorrect': true });
              this.isRequest = false;
              this.loading = false;
            } else if (data.code) {
              this.setSnackBar();
              if (data.codeMessage === 'UNAUTHORIZED') {
                let errorMsg = Constants.MSG_LOGIN_MSG002;
                if (data.message && data.message != '') {
                  errorMsg = data.message;
                }
                this.snackBar.createSnack(this.parent, errorMsg, Constants.ALERT_TYPE.error);
                this.loginForm.controls['password'].setErrors({ 'incorrect': true });
                this.loginForm.controls['username'].setErrors({ 'incorrect': true });
              } else {
                this.snackBar.createSnack(this.parent, data.message, Constants.ALERT_TYPE.error);
              }
              this.isRequest = false;
              this.loading = false;
            } else {
              // Dupla Autenticacao
              if (this.typeLogin === 'LoginO') {
                this.configService.setTokenApi(data.token);
                this._authServer.iNegotiation.getAfcSearchQuestions()
                  .subscribe(res => {
                    if (res.code === 0 && res.questions && res.questions.length > 0) {
                      this.model.questionType = '';
                      this.listQuestionDoubleAuthentication = res.questions;
                      const index = Math.floor(Math.random() * res.questions.length);
                      this.questionActive = res.questions[index];
                      this.questionActive.indexActived = [];
                      this.questionActive.questionResp = '';
                      this.questionActive.indexActived.push(index);
                      this.showDoubleAuthetication = true;
                      this.isRequest = false;
                      this.loading = false;
                      this.dataLoginDoubleAuth = [];
                      this.dataLoginDoubleAuth.push(data);
                      this.dataLoginDoubleAuth.push(_user);
                    }
                  },
                    error => {
                      this.setSnackBar();
                      this.snackBar.createSnack(this.parent, Constants.MSG_LOGIN_MSG003, Constants.ALERT_TYPE.error);
                      this.isRequest = false;
                      this.loading = false;
                    });
              } else {
                this.setConfigLogin(data, _user);
              }
            }
          }, () => {
            this.setSnackBar();
            this.snackBar.createSnack(this.parent, Constants.MSG_LOGIN_MSG003, Constants.ALERT_TYPE.error);
            this.isRequest = false;
            this.loading = false;
          });
      }
    } catch (e) {
      this.setSnackBar();
      this.snackBar.createSnack(this.parent, Constants.MSG_LOGIN_MSG003, Constants.ALERT_TYPE.error);
      this.isRequest = false;
      this.loading = false;
    }
  }

  setConfigLogin(data: any, _user: UserCA) {
    this.workService.setLogged();
    setTimeout(() => {
      this.isUserLogged = true;
      this.loading = !this.loading;
    }, 700);
    setTimeout(() => {
      const leftElement = document.getElementById('left-content');
      if (leftElement) {
        leftElement.classList.add('slideOutLeft');
      }
      const carouselExampleIndicators = document.getElementById('carouselExampleIndicators');
      if (carouselExampleIndicators) {
        carouselExampleIndicators.classList.add('slideOutRight');
      }
      const fullPage = document.getElementById('app-login');
      if (fullPage) {
        fullPage.classList.remove('fadeIn');
        fullPage.classList.add('fadeOut');
      }
    }, 1500);
    // Cedro Accounts
    if (this.configService.getLoginTypeEnabled() === Constants.LOGIN_TYPE.LoginW) {
      this.configService.setTokenApi(data.access_token);
      this.configService.setTokenRefreshApi(data.refresh_token);
      this.configService.setScope(data.scope);
      this.configService.setTokenTp(data.token_type);
      // TODO: Valores comum entre assessor e cliente
      this.configService.setTokenExpiration(AppUtils.getDateTimeLocaleFormat
        (new Date((new Date()).getTime() + (data.expires_in * 1000))));
      const themeDarkCookie = this.configService.getFavoriteThemeUser();
      if (themeDarkCookie) {
        this.configService.setFavoriteThemeUser(themeDarkCookie);
      } else {
        this.configService.setFavoriteThemeUser('true');
      }
      this.configService.setToken(data.access_token);
      this.configService.setTokenGraphic(data.access_token);
      _user = data.user;
      if (data.client) {
        this.configService.setIsAssessor(Profile.Client);
        this.client = data.client;
        this.configService.setUserLoginType(Profile.Client);
        this.configService.setHbAccount(this.client.selectedAccount.account.account);
        this.configService.setHbUserId(data.client.user_id);
        this.configService.setHbAccountId(data.client.id);
        this.configService.setHbAccountName(this.client.name);
        this.configService.setHbEmail(this.client.email);
        this.authService.setUserLogged(_user.name.split(/\s+/)[0]);
        this.configService.setUserSystemLogged(_user.name.split(/\s+/)[0]);
        this.configService.setProfileName(data.client.profile_name);
      } else if (data.assessor) {
        this.configService.setAssessorId(data.assessor.id);
        this.configService.setIsAssessor(Profile.Assessor);
        this.assessor = data.assessor;
        this.configService.setUserLoginType(Profile.Assessor);
        this.configService.setHbAccount(data.assessor.login);
        this.authService.setUserLogged(data.assessor.name.split(/\s+/)[0]);
        this.configService.setUserSystemLogged(data.assessor.name.split(/\s+/)[0]);
        this.configService._authHbLoginType.next(3);
        if (data.clients) {
          data.clients.forEach(element => {
            element.user.first_name = element.user.full_name.split(' ')[0];
            element.user.account_id = element.user.advisor_client_accounts[0].account_id;
            if (element.user.profile_name && element.user.profile_name.toLowerCase() === 'moderado') {
              element.user.profile_color = 'yellow';
            } else if (element.user.profile_name && element.user.profile_name.toLowerCase() === 'balanceado') {
              element.user.profile_color = 'purple';
            } else if (element.user.profile_name && element.user.profile_name.toLowerCase() === 'arrojado') {
              element.user.profile_color = 'orange';
            } else if (element.user.profile_name && element.user.profile_name.toLowerCase() === 'agressivo') {
              element.user.profile_color = 'red';
            } else if (element.user.profile_name && element.user.profile_name.toLowerCase() === 'conservador') {
              element.user.profile_color = 'blue';
            } else {
              element.user.profile_color = 'blue';
            }
          });
          this.configService.setAssessorListClients(data.clients);
          this.configService.setAssessorClientSelected(data.clients[0]);
        }
      }
    } else {
      const themeDarkCookie = this.configService.getFavoriteThemeUser();
      if (themeDarkCookie) {
        this.configService.setFavoriteThemeUser(themeDarkCookie);
      } else {
        this.configService.setFavoriteThemeUser('true');
        this.layoutService.setDarkTheme();
      }
      this.configService.setTokenApi(data.token);
      this.configService.setToken(data.tokenWS);
      this.configService.setTokenGraphic(data.tokenWsGraphic);
      this.configService.setHbAccount(data.accountNumber);
      this.configService.setTokenExpiration(data.expiration);
      this.configService.setGuidUser(data.guidUser);
      this.authService.setUserLogged(data.accountNumber);
    }
    this.layoutService.isDark$.subscribe(isDark => {
      if (isDark) {
        this.renderer.addClass(document.body, 'darkTheme');
      } else {
        this.renderer.removeClass(document.body, 'darkTheme');
      }
    });
    // verify if exists user workspaces to user logging
    this.existsUserWorkspace()
      .then(res => {
        setTimeout(() => {
          this.router.navigate([this.returnUrl], {
            queryParams: {
              existsUserWorkspace: res
            }
          });
        }, 2400);
      });
    if (window.localStorage.length > 0) {
      this.configService.deleteHba();
    }
    const index = '0';
    this.configService.setHba(this.configService.getGuidUser(), index);
    this.tokentService.assignServices();
    this.tokentService.sendStatus(true);
    // autentica na corretora
    const hbLoginTypeEnable = this.configService.getHbLoginTypeEnable();
    if (hbLoginTypeEnable === Constants.LOGIN_O || hbLoginTypeEnable === Constants.LOGIN_W) {
      let typeLoginHB;
      if (this.assessor) {
        typeLoginHB = Constants.COOKIE_HB_TYPE_ACCESSOR;
      } else {
        typeLoginHB = Constants.COOKIE_HB_TYPE_CEDRO;
      }
      if (hbLoginTypeEnable === Constants.LOGIN_W) {
        this.configService.setHbLoginTypeEnable(Constants.LOGIN_W);
        this.configService.setBrokerAccount(this.configService.getHbAccount(), typeLoginHB);
      } else {
        this.configService.setHbLoginTypeEnable(Constants.LOGIN_O);
        this.configService.setBrokerAccount(data.accountNumber, typeLoginHB);
      }
      this.configService._authHbLoginType.next(Number(typeLoginHB));
      this.configService._authHbLogged.next(true);
    }
    this.configService.setHbLoginTypeEnable(this.configService.getLoginTypeEnabled());
  }

  private existsUserWorkspace(): Promise<boolean> {
    return this.workService.loadWorkspaces(this.authService.getUserLogged(), false)
      .toPromise()
      .then(data => {
        if (data) {
          return data.length > 0;
        } else {
          return false;
        }
      },
        () => {
          return false;
        }
      );
  }

  resetValidity(value) {
    if (value.key !== 'Enter') {
      let valueBefore = this.loginForm.controls['username'].value;
      this.loginForm.controls['username'].setValue(valueBefore);
      valueBefore = this.loginForm.controls['password'].value;
      this.loginForm.controls['password'].setValue(valueBefore);
      const snackDiv = document.getElementById('snack-bar-msg');
      snackDiv.classList.remove('active');
      snackDiv.style.display = 'hidden';
    }
  }

  togglePassword() {
    this.showpassword = !this.showpassword;
    this.icon = this.showpassword ? 'eye' : 'crossed-eye';
  }

  setSnackBar() {
    const element = document.getElementById('snack-bar-msg');
    element.classList.add('active');
    element.style.visibility = 'visible';
    setTimeout(() => {
      if (element) {
        if (element.classList.contains('active')) {
          element.classList.remove('active');
        }
      }
    }, 8000);
  }

  createAccount() {
    if (this.configService.isLoginTypeWSO2()) {
      window.open(this.configService.CADASTRO);
    } else {
      window.open(Constants.CREATE_NEW_ACCOUNT_API);
    }
  }

  createAccountRanking() {
    window.open('http://fast.cedrotech.com/FastCloudWebapp/?broker=desafiodeinvestimentos');
  }

  openLinkCvm() {
    window.open('http://www.cvm.gov.br/');
  }

  openRanking() {
    window.open('https://data.fastmarkets.com.br/ranking/desafiodeinvestimentos');
  }

  openLinkBmfBovespa() {
    window.open('http://www.b3.com.br/pt_br/');
  }

  forgotPassword() {
    const dialogConfig = this.dialog.open(ModalPasswordComponent, {
      data: {
        title: Constants.MSG01_FORGOT_TITLE,
        content: Constants.MSG01_FORGOT_CONTENT,
      },
      panelClass: Constants.NORMAL_MODAL,
      width: '590px',
      height: '315px'
    });
  }

  respDoubleAuth() {
    this.loading = true;
    if (!this.questionActive.questionResp) {
      this.setSnackBar();
      this.snackBar.createSnack(this.parent, Constants.MSG_LOGIN_MSG001, Constants.ALERT_TYPE.info);
      this.loading = false;
      return false;
    }
    this.configService.setTokenApi(this.dataLoginDoubleAuth[0].token);
    this._authServer.iNegotiation.postAfcSearchQuestions(this.questionActive.questionResp, this.questionActive.questionType)
      .subscribe(res => {
        if (res && res.code === 0 && res.success) {
          this.setConfigLogin(this.dataLoginDoubleAuth[0], this.dataLoginDoubleAuth[1]);
        } else {
          if (this.questionActive.indexActived.length === this.listQuestionDoubleAuthentication.length) {
            this.configService._authHbLogged.next(false);
            this.model = {};
            this.setSnackBar();
            this.snackBar.createSnack(this.parent, 'Resposta incorreta. Você excedeu o limite de tentativas.', Constants.ALERT_TYPE.error);
            this.configService.deleteHbBrokerUser();
            this.configService.deleteFavoriteThemeUser();
            window.sessionStorage.clear();
            this.showDoubleAuthetication = false;
            this.questionActive = {};
            this.listQuestionDoubleAuthentication = {};
            this.loading = false;
          } else {
            this.setSnackBar();
            this.snackBar.createSnack(this.parent, 'Resposta incorreta.', Constants.ALERT_TYPE.error);
            let index = Math.floor(Math.random() * this.listQuestionDoubleAuthentication.length);
            for (let i = 0; i < this.listQuestionDoubleAuthentication.length; i++) {
              for (let y = 0; y < this.questionActive.indexActived.length; y++) {
                if (index === this.questionActive.indexActived[y]) {
                  index = Math.floor(Math.random() * this.listQuestionDoubleAuthentication.length);
                  break;
                }
              }
            }
            this.questionActive.afcQuestionCode = this.listQuestionDoubleAuthentication[index].afcQuestionCode;
            this.questionActive.question = this.listQuestionDoubleAuthentication[index].question;
            this.questionActive.questionType = this.listQuestionDoubleAuthentication[index].questionType;
            this.questionActive.questionResp = '';
            this.questionActive.indexActived.push(index);
            this.loading = false;
          }
        }
      }, error => {
        this.configService._authHbLogged.next(false);
        this.model = {};
        this._notificationService.error('', Constants.BROKER_MSG002);
        this.configService.deleteHbBrokerUser();
        this.configService.deleteFavoriteThemeUser();
        window.sessionStorage.clear();
        this.showDoubleAuthetication = false;
        this.questionActive = {};
        this.loading = false;
      });
  }

  openHb() {
    if (this.urlHbWhitelabel) {
      window.open(this.urlHbWhitelabel);
    }
  }

  validChar(e) {
    if (AppUtils.validCharSystem(e)) {
      this._notificationService.warn('', 'Caracter (' + e.key + ') inválido.');
      return false;
    }
  }

}
