import { environment } from 'environments/environment';
import { Component, OnInit, HostListener } from '@angular/core';
import { MatDialogRef } from '@angular/material';
import { AutoLogoutService } from '@auth/auto-logout-service.service';

@Component({
  selector: 'app-manual-online',
  templateUrl: './manual-online.component.html'
})
export class ManualOnlineComponent implements OnInit {

  public loading = true;
  public title = 'Manual do Usuário';
  public broker = environment.BROKER_PATH;

  @HostListener('mousemove')
  onMouseMove() {
    this._autoLogoutService.resetTime();
  }

  @HostListener('click')
  onClick() {
    this._autoLogoutService.resetTime();
  }

  constructor(
    public _dialogRef: MatDialogRef<ManualOnlineComponent>,
    private _autoLogoutService: AutoLogoutService,
  ) {
    setTimeout(() => {
      this.loading = false;
    }, 1000);
  }

  ngOnInit() {
  }

  close() {
    this._dialogRef.close();
  }

  scrollTopInit() {
    const mainDiv = document.getElementById('scrollable-area');
    if (mainDiv && mainDiv.scrollTop && mainDiv.scrollTop > 0) {
      mainDiv.scrollTo({ top: 0, behavior: 'smooth' });
    }
  }

  scrollTop() {
    const mainDiv = document.getElementById('scrollable-area');
    mainDiv.scrollTo({ top: 0, behavior: 'smooth' });
  }

  onScroll(event) {
    if (event.target.scrollTop > 0) {
      document.getElementById('btn-scroll').classList.remove('btn-hidden');
    } else {
      document.getElementById('btn-scroll').classList.add('btn-hidden');
    }
  }

}
