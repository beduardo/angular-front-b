import { ConfigService } from '@services/webFeeder/config.service';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material';
import { Constants } from '@utils/constants';
import { AuthenticationService } from '@services/authentication.service';
import { NotificationsService } from 'angular2-notifications';
import { Subscription } from 'rxjs';
import { SidebarService } from '@services/sidebar.service';

@Component({
  selector: 'app-broker-authentication',
  templateUrl: './broker-authentication.component.html',
})

export class BrokerAuthenticationComponent implements OnInit, OnDestroy {

  public loadingSimulator: boolean;
  public loadingBroker: boolean;
  public showAuth: boolean;
  public showAuthBroker: boolean;
  public showAuthSimulator: boolean;
  public model: any = {};

  public clickOpenTabOrder: Subscription;
  public tabClickCallBack: number;

  constructor(
    public _dialogRef: MatDialogRef<BrokerAuthenticationComponent>,
    private _authenticationService: AuthenticationService,
    private _notificationService: NotificationsService,
    private _configSerice: ConfigService,
    private sidebarService: SidebarService,
  ) {
    this.showAuth = true;
    this.showAuthBroker = false;
    this.showAuthSimulator = false;
  }

  ngOnInit() {
    this.clickOpenTabOrder = this.sidebarService.clickOpenTabAuthCallBack$
      .subscribe(x => {
        if (x) {
          this.tabClickCallBack = x;
        }
      });
  }

  ngOnDestroy() {
    this.tabClickCallBack = 0;
  }

  closeClick(): void {
    this._dialogRef.close();
  }

  backClick() {
    this.showAuth = true;
    this.showAuthBroker = false;
    this.showAuthSimulator = false;
  }

  openAuthSimulator() {
    this.loadingSimulator = true;
    const username = this._configSerice.getHbAccount();
    const password = this._configSerice.getHbAccount();
    this._authenticationService.brokerServiceLogin(username, password, this.model.broker)
      .subscribe((data: any) => {
        if (data && data.isAuthenticated === 'N') {
          this._authenticationService.clientRegisterSimulator(username, password)
            .subscribe(res => {
              if (res && (res.success === 'true' || res.success === true)) {
                this.openAuthSimulator();
              } else {
                this.loadingSimulator = false;
                this._notificationService.error('', Constants.BROKER_MSG002);
              }
              this.loadingSimulator = false;
            }, error => {
              this.loadingSimulator = false;
              this._notificationService.error('', Constants.BROKER_MSG002);
            });
        } else if (data.isAuthenticated === 'Y') {
          const typeLoginHB = Constants.COOKIE_HB_TYPE_SIMULATOR;
          if (data.authenticationReqId.split(',').length > 1) {
            data.authenticationReqId = data.authenticationReqId.split(',')[0];
          }
          this._configSerice.setTokenNegotiation(data.tokenNegotiation);
          this._configSerice.setBrokerAccount(data.authenticationReqId, typeLoginHB);
          this._configSerice._authHbLoginType.next(Number(typeLoginHB));
          this.closeClick();
          this.sidebarService._clickOpenTabAuth.next(this.tabClickCallBack);
          this._configSerice._authHbLogged.next(true);
          this._configSerice.setHbLoginTypeEnable(Constants.LOGIN_S);
          this.tabClickCallBack = 0;
          this._notificationService.success('', Constants.BROKER_MSG005);
        } else {
          this._notificationService.error('', Constants.BROKER_MSG002);
          this._configSerice._authHbLogged.next(false);
        }
        this.loadingSimulator = false;
      },
        error => {
          this.loadingSimulator = false;
          this._notificationService.error('', Constants.BROKER_MSG002);
        });
  }

  openAuthBroker() {
    this.showAuth = false;
    this.showAuthBroker = true;
    this.showAuthSimulator = false;
  }


}
