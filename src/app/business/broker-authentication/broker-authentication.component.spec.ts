import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { BrokerAuthenticationComponent } from '@business/broker-authentication/broker-authentication.component';
import { MatIconModule, MatProgressSpinnerModule, MatInputModule, MatFormFieldModule, MatAutocompleteModule, MatOptionModule, MatDialogRef, MatDialogModule } from '@angular/material';
import { AuthBrokerComponent } from '@business/auth-broker/auth-broker.component';
import { FormsModule } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { ConfigService } from '@services/webFeeder/config.service';
import { ConfigServiceMock, CustomSnackbarServiceMock } from '@services/_test-mock-services/_mock-services';
import { HttpModule } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';
import { NotificationsService, SimpleNotificationsModule } from 'angular2-notifications';
import { CustomSnackbarService } from '@services/custom-snackbar-service.service';
import { DatePipe } from '@angular/common';

describe('BrokerAuthenticationComponent', () => {
  let component: BrokerAuthenticationComponent;
  let fixture: ComponentFixture<BrokerAuthenticationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        BrokerAuthenticationComponent,
        AuthBrokerComponent
      ],
      imports: [
        MatIconModule,
        MatProgressSpinnerModule,
        MatInputModule,
        FormsModule,
        MatAutocompleteModule,
        MatOptionModule,
        RouterTestingModule,
        HttpModule,
        HttpClientModule,
        SimpleNotificationsModule.forRoot(),
        MatDialogModule
      ],
      providers:[
        DatePipe,
        NotificationsService,
        { provide: MatDialogRef, useValue: {} },
        { provide: ConfigService, useClass: ConfigServiceMock },
        { provide: CustomSnackbarService, useClass: CustomSnackbarServiceMock },
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BrokerAuthenticationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
