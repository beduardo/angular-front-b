import { ConfigService } from '@services/webFeeder/config.service';
import {
  Component,
  OnInit,
  Input,
  OnDestroy,
  ElementRef,
  ViewChild
} from '@angular/core';
import { MatDialog } from '@angular/material';
import { Subscription } from 'rxjs';
import { Constants } from '@utils/constants';
import { ChartService } from '@services/webFeeder/sckt-chart.service';
import { Chart } from 'angular-highcharts';
import { WorkspaceNewService } from '@services/workspace-new.service';
import { AbstractItemService } from '@business/base/abstract-item.service';
import { AbstractItem } from '@business/base/abstract-item';
import { isUndefined } from 'util';
import { NotificationsService } from 'angular2-notifications';
import { QuoteService } from '@services/webFeeder/sckt-quotes.service';
import { environment } from 'environments/environment';
// tslint:disable:max-line-length
@Component({
  selector: Constants.WINDOW_NAME_QUOTE_CHART,
  templateUrl: './quote-chart.component.html'
})
export class QuoteChartComponent extends AbstractItem implements OnInit, OnDestroy {
  protected workspaceService: WorkspaceNewService;
  dialog: MatDialog;
  errorMsg = false;
  chart: Chart;
  symbol: string;
  previousSymbol;
  inputAsset = false;
  chartQuote: any;
  windowActive: any;
  differenceH = 40;
  loading = true;
  currentInformation;
  private subscriptionChart: Subscription;
  private SubscriptionQuote: Subscription;
  private marketStateSubscription: Subscription;
  private initConfig = true;
  private period = environment.period_candle_chart;
  private startDate: any;
  @Input() data: any;
  @ViewChild('quoteChart') quoteChartEl: ElementRef;
  @ViewChild('header') headerEl: ElementRef;

  constructor(
    private chartService: ChartService,
    workspaceService: WorkspaceNewService,
    itemService: AbstractItemService,
    dialog: MatDialog,
    public _NotificationService: NotificationsService,
    private configService: ConfigService,
    private quoteService?: QuoteService,
  ) {
    super(workspaceService, dialog, itemService);
    this.dialog = dialog;
    this.workspaceService = workspaceService;
  }

  ngOnInit() {
    this.loading = true;
    const today = new Date();
    this.startDate = `${today.getFullYear()}${(today.getMonth() + 1) < 10 ? '0' + (today.getMonth() + 1) : (today.getMonth() + 1)}${today.getDate() < 10 ? '0' + today.getDate() : today.getDate()}0600`;
    this.symbol = this.data.symbol;
    this.quoteService.subscribeQuote(this.symbol, Constants.WINDOW_NAME_QUOTE_CHART);
    this.marketStateSubscription = this.quoteService.messagesQuote.subscribe(x => {
      if (x && x.errorMessage && x.symbol.toLowerCase() === this.symbol.toLowerCase()) {
        this.quoteService.unsubscribeQuote(x.symbol.toLowerCase(), Constants.WINDOW_NAME_QUOTE_CHART);
        this.chartService.unsubscribeChart(x.symbol, this.period);
        this.loading = false;
      } else if (x && this.symbol && x.symbol.toLocaleLowerCase() === this.symbol.toLocaleLowerCase()) {
      }
    });
    this.workspaceService.windowResized$.subscribe(data => {
      if (data > 0) {
        this.differenceH = this.headerEl.nativeElement.offsetHeight;
        const a = this.quoteChartEl.nativeElement.offsetWidth;
        const b = this.quoteChartEl.nativeElement.offsetHeight - this.differenceH;
        if (!this.loading) {
          if (this.chart && !isUndefined(this.chart.ref)) {
            this.chart.ref.setSize(a, b, false);
          }
        }
      }
    });
    this.getCandleChart();
  }

  getCandleChart() {
    if (this.symbol) {
      this.chartService.subscribeChart(this.symbol, this.period, this.startDate);
      this.subscriptionChart = this.chartService.messagesChart
        .subscribe(x => {
          if (x && x.errorMessage && x.quote.toUpperCase() === this.symbol.toUpperCase()) {
            this.chartService.unsubscribeChart(this.symbol, this.period);
            this.quoteService.unsubscribeQuote(this.symbol, Constants.WINDOW_NAME_QUOTE_CHART);
            this.loading = false;
            this.errorMsg = true;
          } else if (x && x.quote.toUpperCase() === this.symbol.toUpperCase() && !x.errorMessage) {
            if (!!!this.currentInformation ||  this.currentInformation !== x) {
              this.errorMsg = false;
              this.getHistory(x.candleChart);
              x = this.currentInformation;
            }
          }
        });
    } else {
      this.loading = false;
    }
  }

  ngOnDestroy() {
    if (this.quoteService && this.chartService && this.symbol) {
      this.quoteService.unsubscribeQuote(this.symbol.toLowerCase(), Constants.WINDOW_NAME_QUOTE_CHART);
      this.chartService.unsubscribeChart(this.symbol, this.period);
      if (this.SubscriptionQuote) {
        this.SubscriptionQuote.unsubscribe();
        this.SubscriptionQuote = null;
      }
      if (this.subscriptionChart) {
        this.subscriptionChart.unsubscribe();
        this.subscriptionChart = null;
      }
      if (this.marketStateSubscription) {
        this.marketStateSubscription.unsubscribe();
        this.marketStateSubscription = null;
      }
    }
    this.closeSheet();
    if (this.chartQuote) {
      this.chartQuote.destroy();
    }
  }

  getHistory(data) {
    this.errorMsg = false;
    if (this.initConfig) {
      const chart = new Chart({
        chart: {
          height: this.quoteChartEl.nativeElement.offsetHeight - this.differenceH, type: 'line'
        },
        title: {
          text: null
        },
        tooltip: {
          headerFormat: ''
        },
        legend: {
          enabled: false
        },
        xAxis: {
          type: 'datetime'
        },
        yAxis: {
          title: null
        },
        credits: {
          enabled: false
        },
        series: []
      });

      this.chart = chart;
      const timesArray = [];

      for (let i = 0; i < data.length; i++) {
        const utcString = data[i].date.toString();
        const year = utcString.substring(0, 4);
        const month = utcString.substring(4, 6);
        const day = utcString.substring(6, 8);
        const hour = utcString.substring(8, 10);
        const minute = utcString.substring(10, 12);
        const timeValue = Date.UTC(parseInt(year, 10), parseInt(month, 10), parseInt(day, 10), parseInt(hour, 10), parseInt(minute, 10));
        timesArray.push([timeValue, data[i].price]);
      }
      this.chart.addSerie({ name: 'Valor', data: timesArray });
      this.initConfig = false;
      this.loading = false;
    } else {
      if (data.length > 0) {
        const lastCandle = data.length - 1;
        const utcString = data[lastCandle].date.toString();
        const year = utcString.substring(0, 4);
        const month = utcString.substring(4, 6);
        const day = utcString.substring(6, 8);
        const hour = utcString.substring(8, 10);
        const minute = utcString.substring(10, 12);
        const timeValue = Date.UTC(parseInt(year, 10), parseInt(month, 10), parseInt(day, 10), parseInt(hour, 10), parseInt(minute, 10));
        this.chart.addPoint([timeValue, data[lastCandle].price]);
      }
    }
  }

  selectAsset(event) {
    if (event.symbol) {
      this.previousSymbol = this.symbol;
      this.symbol = event.symbol;
      if (this.previousSymbol && this.symbol && this.previousSymbol.toLowerCase() !== this.symbol.toLowerCase()) {
        this.loading = true;
        this.quoteService.unsubscribeQuote(this.previousSymbol, Constants.WINDOW_NAME_QUOTE_CHART);
        this.chartService.unsubscribeChart(this.previousSymbol, this.period);
        this.quoteService.subscribeQuote(event.symbol, Constants.WINDOW_NAME_QUOTE_CHART);
        this.SubscriptionQuote = this.quoteService.messagesQuote
          .subscribe(x => {
            if (x && x.symbol.toLocaleLowerCase() === event.symbol.toLocaleLowerCase() && !x.errorMessage) {
              this.SubscriptionQuote.unsubscribe();
              const oldSymbol = this.previousSymbol;
              if (super.validateOnlyOneAssetAssigned(this.symbol, this.data.type, this.data.workspaceId) === true) {
                this._NotificationService.error('Erro', Constants.WORKSPACE_MSG015);
                this.symbol = '';
                super.updateOnDemand(oldSymbol, this.symbol, true);
                this.symbol = oldSymbol;
              } else {
                super.updateOnDemand(this.symbol, this.previousSymbol, true);
              }
              this.chartService.subscribeChart(this.symbol, this.period, this.startDate);
              this.initConfig = true;
              this.subscriptionChart = this.chartService.messagesChart
                .subscribe(dataChart => {
                  if (dataChart && this.symbol && dataChart.quote.toUpperCase() === this.symbol.toUpperCase() && !dataChart.errorMessage) {
                    if (!!!this.currentInformation || this.currentInformation !== x) {
                      this.previousSymbol = this.symbol;
                      x = this.currentInformation;
                      this.loading = false;
                    } 
                  }
                });
              this.inputAsset = true;
              this.toggleInput();
            } else if (x && x.symbol.toLocaleLowerCase() === event.symbol.toLocaleLowerCase() && x.errorMessage) {
              super.updateOnDemand(this.symbol, this.previousSymbol, true);
              this.SubscriptionQuote.unsubscribe();
              this.errorMsg = true;
              this.loading = false;
              this._NotificationService.warn('', Constants.WORKSPACE_MSG007);
            }
          });
      } else {
        this.toggleInput();
      }
    }
  }

  toggleInput() {
    this.inputAsset = !this.inputAsset;
  }

  openNewWindow() {
    const options = 'width=1203px, height=768';
    const token = this.configService.getTokenGraphic();
    if (this.windowActive == null) {
      this.windowActive = window.open(
        'chart?symbol=' + this.symbol + '&token=' + token,
        'QuoteChart' + this.data.id,
        options
      );
    } else {
      this.windowActive.focus();
    }
    if (this.windowActive.closed) {
      this.windowActive = window.open('chart?symbol=' + this.symbol + '&token=' + this.configService.getTokenGraphic(), 'QuoteChart' + this.data.id, options);
    }
  }

  closeSheet() {
    if (this.windowActive != null) {
      this.windowActive.close();
    }
  }
}
