import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { QuoteChartComponent } from '@business/quote-chart/quote-chart.component';

describe('QuoteChartComponent', () => {
  let component: QuoteChartComponent;
  let fixture: ComponentFixture<QuoteChartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ QuoteChartComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(QuoteChartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
