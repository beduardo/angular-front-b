import { OmsAdmin } from './../../model/recover-password';
import { AuthenticationService } from '@services/authentication.service';
import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Constants } from '@utils/constants';
import { NotificationsService } from 'angular2-notifications';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { environment } from 'environments/environment';
import { AppUtils } from '@utils/app.utils';

@Component({
  selector: 'app-recover-password',
  templateUrl: './recover-password.component.html'
})
export class RecoverPasswordComponent implements OnInit {

  @ViewChild('loginForm') loginForm: NgForm;
  public loading: boolean;
  public showpassword = false;
  public showpasswordConfirm = false;
  icon = 'crossed-eye';
  iconConfirm = 'crossed-eye';
  model: OmsAdmin = new OmsAdmin();
  validPass = true;
  public errorMsg01: string = Constants.MSG03_FORGOT_ERROR;
  private subscription: Subscription;
  public isUserLogged = false;
  public showPhone: boolean;
  public phone1: string;

  public passwordComplexyLength: boolean;
  public passwordComplexySpecial: boolean;
  public passwordComplexyNumber: boolean;
  public passwordComplexyLetterUp: boolean;
  public passwordComplexyLetterDown: boolean;
  public passwordWeak: boolean;
  public passwordMedium: boolean;
  public passwordGood: boolean;
  public passwordStrong: boolean;
  public isPasswordStrong: boolean;

  constructor(
    public _NotificationService: NotificationsService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private _authenticationService: AuthenticationService
  ) {
    this.model.newPassword = '';
    this.model.passwordConfirm = '';
    this.model.token = '';
    // configura o WHITELABEL
    Constants.WHITELABEL.forEach(element => {
      if (element.cod === environment.WHITELABEL) {
        if (element.phone1 && element.phone1 !== '') {
          this.showPhone = true;
          this.phone1 = element.phone1;
        }
      }
    })
  }

  ngOnInit() {
    this.subscription = this.activatedRoute.queryParams
      .subscribe((param: any) => {
        this.model.token = param['token'];
        this.model.broker = param['broker'];
      });
    if (this.model.token) {
      const url: string = this.router.url.substring(0, this.router.url.indexOf('?'));
      // TODO: descomentar
      // this.router.navigateByUrl(url);
    }
  }

  public resetValidity(value) {
    if (value.key !== 'Enter') {
      let valueBefore = this.loginForm.controls['password'].value;
      this.loginForm.controls['password'].setValue(valueBefore);
      valueBefore = this.loginForm.controls['passwordConfirm'].value;
      this.loginForm.controls['passwordConfirm'].setValue(valueBefore);
      const snackDiv = document.getElementById('snack-bar-msg');
      snackDiv.classList.remove('active');
      snackDiv.style.display = 'hidden';
    }
  }

  public togglePassword() {
    this.showpassword = !this.showpassword;
    this.icon = this.showpassword ? 'eye' : 'crossed-eye';
  }

  public togglePasswordConfirm() {
    this.showpasswordConfirm = !this.showpasswordConfirm;
    this.iconConfirm = this.showpasswordConfirm ? 'eye' : 'crossed-eye';
  }

  validatorPassword(value) {
    if (this.model.newPassword !== this.model.passwordConfirm) {
      this.validPass = false;
      this.resetValidity(value);
    } else {
      this.validPass = true;
    }
    if (value.key !== 'Enter') {
      const valueBefore = this.loginForm.controls['passwordConfirm'].value;
      this.loginForm.controls['passwordConfirm'].setValue(valueBefore);
      const snackDiv = document.getElementById('snack-bar-msg');
      snackDiv.classList.remove('active');
      snackDiv.style.display = 'hidden';
    }

  }

  validatorPassword2(value) {
    if (this.model.passwordConfirm) {
      if (this.model.newPassword !== this.model.passwordConfirm) {
        this.validPass = false;
        // this._NotificationService.error('Erro', Constants.MSG03_FORGOT_ERROR);
        this.resetValidity(value);
      } else {
        this.validPass = true;
      }

      if (value.key !== 'Enter') {
        const valueBefore = this.loginForm.controls['password'].value;
        this.loginForm.controls['password'].setValue(valueBefore);
        const snackDiv = document.getElementById('snack-bar-msg');
        snackDiv.classList.remove('active');
        snackDiv.style.display = 'hidden';
      }
    }
    if (value) {
      this.complexyPassoword(value);
    } else {
      this.passwordWeak = false;
      this.passwordMedium = false;
      this.passwordGood = false;
      this.passwordStrong = false;
      this.passwordComplexyLength = false;
      this.passwordComplexyNumber = false;
      this.passwordComplexySpecial = false;
      this.passwordComplexyLetterUp = false;
      this.passwordComplexyLetterDown = false;
    }
  }

  public send(model: OmsAdmin) {
    this.loading = true;
    if (!this.model.newPassword || !this.model.newPassword) {
      this._NotificationService.error('Erro', Constants.MSG06_FORGOT_ERROR);
      this.loading = false;
      return;
    }

    if (!this.model.token) {
      this._NotificationService.error('Erro', Constants.MSG07_FORGOT_ERROR);
      this.loading = false;
      return;
    }
    if (!this.model.broker) {
      this._NotificationService.error('Erro', Constants.MSG08_FORGOT_ERROR);
      this.loading = false;
      return;
    }
    this._authenticationService.changePassword(this.model.token, Constants.FORGOT_TYPE_02, this.model.passwordConfirm, this.model.broker)
      .subscribe(res => {
        if (res && res.success === 'true') {
          this._NotificationService.success('', Constants.MSG_RECOVER_PASSWORD);
          setTimeout(() => {
            this.router.navigate(['/login']);
            this.loading = false;
          }, 500);
        } else {
          this.loading = false;
          if (res && res.message && res.messageDetails) {
            this._NotificationService.error(res.message, res.messageDetails);
          } else if (res && res.messageDetails) {
            this._NotificationService.error('', res.messageDetails);
          } else if (res && res.message) {
            this._NotificationService.error('', res.message);
          } else {
            this._NotificationService.error('', Constants.MSG_LOGIN_MSG003);
          }
          this.loading = false;
        }
      }, error => {
        this.loading = false;
        this._NotificationService.error('', Constants.MSG_LOGIN_MSG003);
      });
  }

  back() {
    this.router.navigate(['/login']);
  }

  validChar(e) {
    if (AppUtils.validCharSystem(e)) {
      this._NotificationService.warn('', 'Caracter (' + e.key + ') inválido.');
      return false;
    }
  }

  complexyPassoword(value) {
    if (value && value !== '') {
      const passwordComplexyLengthTemp = new RegExp('^(?=.{8,})').test(value);
      const passwordComplexySpecialTemp = new RegExp('^(?=.*[#$^+=!*()@%&])').test(value);
      const passwordComplexyNumberTemp = new RegExp('^(?=.*[0-9])').test(value);
      const passwordComplexyLetterUpTemp = new RegExp('^(?=.*[A-Z])').test(value);
      const passwordComplexyLetterDownTemp = new RegExp('^(?=.*[a-z])').test(value);
      if (passwordComplexyLengthTemp) {
        this.passwordComplexyLength = true;
      } else {
        this.passwordComplexyLength = false;
      }
      if (passwordComplexySpecialTemp) {
        this.passwordComplexySpecial = true;
      } else {
        this.passwordComplexySpecial = false;
      }
      if (passwordComplexyNumberTemp) {
        this.passwordComplexyNumber = true;
      } else {
        this.passwordComplexyNumber = false;
      }
      if (passwordComplexyLetterUpTemp) {
        this.passwordComplexyLetterUp = true;
      } else {
        this.passwordComplexyLetterUp = false;
      }
      if (passwordComplexyLetterDownTemp) {
        this.passwordComplexyLetterDown = true;
      } else {
        this.passwordComplexyLetterDown = false;
      }

      if (value.length >= 8 && passwordComplexyLengthTemp && passwordComplexyNumberTemp && passwordComplexyLetterUpTemp
        && passwordComplexyLetterDownTemp && passwordComplexySpecialTemp) {
        this.passwordWeak = true;
        this.passwordMedium = true;
        this.passwordGood = true;
        this.passwordStrong = true;
        // this.isPasswordStrong = true;
      } else if (value.length >= 8 && passwordComplexyLengthTemp && passwordComplexyNumberTemp
        && (passwordComplexyLetterUpTemp || passwordComplexySpecialTemp)) {
        this.passwordWeak = true;
        this.passwordMedium = true;
        this.passwordGood = true;
        this.passwordStrong = false;
      } else if (value.length >= 8 && passwordComplexyLengthTemp && passwordComplexyNumberTemp
        && (passwordComplexyLetterDownTemp || passwordComplexyLetterUpTemp)) {
        this.passwordWeak = true;
        this.passwordMedium = true;
        this.passwordGood = false;
        this.passwordStrong = false;
      } else {
        this.passwordWeak = true;
        this.passwordMedium = false;
        this.passwordGood = false;
        this.passwordStrong = false;
      }
    }
  }

}
