import { Constants } from '@utils/constants';
import { Component, Input, OnDestroy, OnInit, HostListener, Output, EventEmitter, DoCheck } from '@angular/core';
import { ConfigService } from '@services/webFeeder/config.service';
import { AutoLogoutService } from '@auth/auto-logout-service.service';
import { ActivatedRoute } from '@angular/router';
import { Cookie } from 'ng2-cookies/ng2-cookies';

declare var $: any;
declare const TradingView: any;
declare const Datafeeds: any;

@Component({
  selector: 'app-trading-view',
  templateUrl: './trading-view.component.html'
})
export class TradingViewComponent implements OnInit, OnDestroy, DoCheck {

  public widget: any;
  public symbol: string;
  public fromModal: boolean;
  public differ: any;
  public active = false;
  private firstChange: boolean;
  // private tokenChart = '7bbabbd0-c2ca-4dfc-83a4-278c08bc78ce';

  @Output() currentSymbol = new EventEmitter<any>();

  @HostListener('mousemove')
  onMouseMove() {
    this._autoLogoutService.resetTime();
  }

  @HostListener('click')
  onClick() {
    this._autoLogoutService.resetTime();
  }

  constructor(
    private configService: ConfigService,
    private _autoLogoutService: AutoLogoutService,
    private route: ActivatedRoute
  ) {
    this.symbol = '';
    this.fromModal = false;
  }

  ngOnInit() {
    this.route.queryParams.subscribe(params => {
      if (params['workspaceId']) {
        this.symbol = '';
        this.fromModal = true;
      } else {
        this.symbol = params['symbol'];
        this.fromModal = false;
      }
      this.createChart();
    });
  }

  ngDoCheck() {
    if (this.widget) {
      try {
        const value = this.widget.chart().symbolExt().symbol;
        if (!this.firstChange) {
          this.widget.chart().setChartType(Constants.TRADINGVIEW_DEFAULT_TYPE);
          this.firstChange = true;
        }
        if (this.symbol !== value) {
          this.symbol = value;
          this.currentSymbol.next(this.symbol);
        }
      } catch (err) {
      }
    }
  }

  ngOnDestroy() {
    this.cleanChart();
  }

  createChart() {
    function getParameterByName(name) {
      name = name.replace(/[\[]/, '\\[').replace(/[\]]/, '\\]');
      const regex = new RegExp('[\\?&]' + name + '=([^&#]*)'), results = regex.exec(location.search);
      return results === null ? '' : decodeURIComponent(results[1].replace(/\+/g, ' '));
    }

    let overrides;
    let toolbar_bg;
    let loading_screen;
    const layoutTheme = this.configService.getFavoriteThemeUser();
    let tokenWS = '';
    if (this.configService.isLoginTypeWSO2()) {
      tokenWS = this.configService.getToken();
    } else {
      tokenWS = this.configService.getTokenGraphic();
    }
    const url_ws_chart = this.configService.getUrlWebSocketWebFeeder(1, true);
    const symbol = this.symbol ? this.symbol : 'ibov';

    if (layoutTheme === 'true') {
      toolbar_bg = '#232323';
      loading_screen = { backgroundColor: '#232323' };
      overrides = {
        'paneProperties.legendProperties.showLegend': false,
        'paneProperties.background': '#232323',
        'paneProperties.vertGridProperties.color': '#232323',
        'paneProperties.horzGridProperties.color': '#232323',
        'mainSeriesProperties.candleStyle.upColor': '#61bf6f',
        'mainSeriesProperties.candleStyle.downColor': '#dc4b50',
        'mainSeriesProperties.candleStyle.wickUpColor': '#61bf6f',
        'mainSeriesProperties.candleStyle.wickDownColor': '#dc4b50',
        'mainSeriesProperties.candleStyle.borderColor': 'rgba(0,0,0,0)',
        'mainSeriesProperties.candleStyle.borderUpColor': 'rgba(0,0,0,0)',
        'mainSeriesProperties.candleStyle.borderDownColor': 'rgba(0,0,0,0)',
        'mainSeriesProperties.hollowCandleStyle.upColor': '#61bf6f',
        'mainSeriesProperties.hollowCandleStyle.downColor': '#dc4b50',
        'mainSeriesProperties.areaStyle.color1': '#0094FF',
        'mainSeriesProperties.areaStyle.color2': '#212429',
        'mainSeriesProperties.areaStyle.linecolor': '#04a2fe'
      };
    } else {
      toolbar_bg = '#fff';
      loading_screen = { backgroundColor: '#fff' };
      overrides = {
        'paneProperties.legendProperties.showLegend': false,
        'paneProperties.background': '#fff',
        'paneProperties.vertGridProperties.color': '#fff',
        'paneProperties.horzGridProperties.color': '#fff',
        'mainSeriesProperties.candleStyle.upColor': '#61bf6f',
        'mainSeriesProperties.candleStyle.downColor': '#dc4b50',
        'mainSeriesProperties.candleStyle.wickUpColor': '#61bf6f',
        'mainSeriesProperties.candleStyle.wickDownColor': '#dc4b50',
        'mainSeriesProperties.candleStyle.borderColor': 'rgba(0,0,0,0)',
        'mainSeriesProperties.candleStyle.borderUpColor': 'rgba(0,0,0,0)',
        'mainSeriesProperties.candleStyle.borderDownColor': 'rgba(0,0,0,0)',
        'mainSeriesProperties.hollowCandleStyle.upColor': '#61bf6f',
        'mainSeriesProperties.hollowCandleStyle.downColor': '#dc4b50'
      };
    }
    let library_path_url = '';
    if (this.configService.isLoginTypeWSO2()) {
      library_path_url = '../../fasttrade/assets/charting_library/';
    } else {
      library_path_url = '../../assets/charting_library/';
    }
    const widget = new TradingView.widget({
      fullscreen: false,
      symbol: symbol,
      interval: 'D',
      container_id: 'tv_chart_container',
      datafeed: new Datafeeds.UDFCompatibleDatafeed(
        this.configService.getApiNodeCandleRequest(), url_ws_chart, tokenWS, symbol),
      library_path: library_path_url,
      debug: false,
      locale: getParameterByName('lang') || 'pt',
      timezone: 'America/Sao_Paulo',
      theme: 'Dark',
      colorTheme: 'dark',
      showLastValue: true,
      showCountdown: true,
      showPriceLine: true,
      priceLineWidth: 1,
      extendedHours: false,
      silentIntervalChange: false,
      drawings_access: { type: 'black', tools: [{ name: 'Regression Trend' }] },
      disabled_features: ['use_localstorage_for_settings'],
      charts_storage_url: 'http://saveload.tradingview.com', // TODO this.configService.getApiWorkspaceCandle(),
      charts_storage_api_version: '1.1',
      client_id: 'tradingview.com',
      user_id: this.configService.getHbAccount(),
      height: '100%',
      width: '100%',
      toolbar_bg,
      loading_screen,
      overrides,
      time_frames: [
        { text: '5y', resolution: '1W' },
        { text: '1y', resolution: '1W' },
        { text: '6m', resolution: '60' },
        { text: '3m', resolution: '60' },
        { text: '1m', resolution: '15' },
        { text: '5D', resolution: '15' },
        { text: '1D', resolution: '5' },
      ]
    });
    this.widget = widget;
  }

  cleanChart() {
    this.widget.remove();
  }

}
