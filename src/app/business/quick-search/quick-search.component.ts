import { Component, OnInit } from '@angular/core';
import { MatDialog, MatDialogConfig } from '@angular/material';
import { AssetSummaryPopUpComponent } from '@business/asset-summary-pop-up/asset-summary-pop-up.component';
import { NotificationsService } from 'angular2-notifications';
import { Constants } from '@utils/constants';

export interface DialogData {
  data: any;
}

@Component({
  selector: 'app-quick-search',
  templateUrl: './quick-search.component.html'
})
export class QuickSearchComponent implements OnInit {
  constructor(public dialog: MatDialog, public _NotificationService: NotificationsService) { }
  injectedSymbol: any;
  ngOnInit() {
  }


  selectAsset(value): void {
    if (value.symbol !== '') {
      const element = document.getElementById('searchBar');
      const position = element.getBoundingClientRect();
      const x = position.left;
      const dialogConfig = new MatDialogConfig();
      dialogConfig.data = { injectedSymbol: value };
      dialogConfig.minHeight = 300;
      dialogConfig.position = { 'top': '62px', left: (x - 72) + 'px' };
      dialogConfig.panelClass = 'custom-dialog-container';
      dialogConfig.id = 'quickml';
      dialogConfig.autoFocus = false;
      const dialogRef = this.dialog.open(AssetSummaryPopUpComponent, dialogConfig);
      dialogRef.afterClosed().subscribe(result => {
      });
    }
    // else {
    //     this._NotificationService.error('Erro', Constants.WORKSPACE_MSG007);
    //   }
  }
}

