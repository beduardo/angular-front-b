import { Component, Inject, Input, OnDestroy, OnInit, Optional, HostListener, ViewChild, ChangeDetectorRef } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialog, MatTooltip } from '@angular/material';
import { ActivatedRoute } from '@angular/router';
import { AbstractItem } from '@business/base/abstract-item';
import { AbstractItemService } from '@business/base/abstract-item.service';
import { AssetSummary } from '@model/asset-summary.model';
import { MessageQuote } from '@model/webFeeder/message-quote.model';
import { BilletOrderModalService } from '@services/billet-order-modal.service';
import { QuoteService } from '@services/webFeeder/sckt-quotes.service';
import { WorkspaceNewService } from '@services/workspace-new.service';
import { AppUtils } from '@utils/app.utils';
import { Constants } from '@utils/constants';
import { Subscription, Subject } from 'rxjs';
import { NotificationsService } from 'angular2-notifications';
import { DatePipe } from '@angular/common';

@Component({
  selector: Constants.WINDOW_NAME_ASSET_SUMMARY,
  templateUrl: './asset-summary.component.html'
})

export class AssetSummaryComponent extends AbstractItem implements OnInit, OnDestroy {
  public setSymbol;
  @Input() data: any;
  public home = 'color-green';
  public newWindow = false;
  public _messageQuote = new MessageQuote();
  public exemplo: string;
  public windowActive: any;
  public assetModel: AssetSummary;
  public assetSymbol = null;
  public assetValid = true;
  private subscriptionQuote: Subscription;
  public disableTitle = false;
  public loading = true;
  public style = 0;
  public wksSelectedId;
  dialog: MatDialog;
  public currentSymbol;
  protected workspaceService: WorkspaceNewService;
  isThirdWindowSelected = false;
  clickOpenTabOrder: Subscription;
  valueTemp: any;
  public theoryPercentage: number;
  public theoryPercentageDisplay: string;
  public indiceFuture: boolean;
  public oldSymbol;
  public hiddenInvalid: boolean;
  public hiddenInvalidActive: boolean;
  public msgInvalidAsset = Constants.WORKSPACE_MSG007;
  public directionUnansweredQty: number;
  public changingValue: Subject<boolean> = new Subject();
  public company: string;
  public isOverFlowed: boolean;
  public symbolTemp: string;
  public broadcastChannel: BroadcastChannel;
  public auctionHammer: boolean;
  public showVariation: boolean;
  public isUpdateOnDemand: boolean;
  public isIndex = false;

  @ViewChild('tooltipMax') tooltipMax: MatTooltip;
  @ViewChild('tooltipMin') tooltipMin: MatTooltip;

  constructor(
    private quoteService: QuoteService,
    workspaceService: WorkspaceNewService,
    private route: ActivatedRoute,
    itemService: AbstractItemService,
    dialog: MatDialog,
    @Optional() @Inject(MAT_DIALOG_DATA) public injectedSymbol,
    private _billetOrderService: BilletOrderModalService,
    public _NotificationService: NotificationsService,
    private datePipe: DatePipe,
    private _cdRef: ChangeDetectorRef,
  ) {
    super(workspaceService, dialog, itemService);
    this.dialog = dialog;
    this.workspaceService = workspaceService;
    this.indiceFuture = false;
    this.loading = false;
  }

  @HostListener('window:resize', ['$event'])
  onResize() {
    if (window.innerWidth <= 1440) {
      this.isOverFlowed = true;
    } else {
      this.isOverFlowed = false;
    }
  }

  ngOnInit() {
    if (window.innerWidth <= 1440) {
      this.isOverFlowed = true;
    } else {
      this.isOverFlowed = false;
    }
    if (this.injectedSymbol) {
      this.data = this.injectedSymbol.injectedSymbol;
      this.disableTitle = true;
    }

    if (this.route.snapshot.queryParams['ativo']) {
      this.data = this.route.snapshot.queryParams['ativo'];
    }

    if (this.data && this.data.symbol) {
      this.currentSymbol = this.data.symbol;
      this.selectAsset(this.currentSymbol);
    } else {
      this.newWindow = true;
      this.route.queryParams.subscribe(params => {
        if (params['ativo']) {
          this.currentSymbol = params['ativo'];
          this.broadcastChannel = new BroadcastChannel(Constants.WINDOW_NAME_ASSET_SUMMARY + '-' + this.currentSymbol);
          this.data = { symbol: this.currentSymbol };
          setTimeout(() => {
            this.selectAsset(this.currentSymbol);
          }, 2000);
        }
      });
    }
  }

  verifyResolution() {
    setTimeout(() => {
      if (this.isOverFlowed) {
        this.tooltipMax.show();
        this.tooltipMin.show();
      } else if (document.getElementsByClassName('baloon').length === 1) {
        this.tooltipMax.show();
        this.tooltipMin.show();
      } else {
        this.tooltipMax.hide();
        this.tooltipMin.hide();
      }
    }, 0);
  }

  closeTooltip() {
    setTimeout(() => {
      this.tooltipMax.hide();
      this.tooltipMin.hide();
    }, 0);
  }

  hiddenInvalidOutput(value: any) {
    this.assetValid = value;
  }

  setThirdBoxClass(tabNumber) {
    if (tabNumber === 2) {
      this.isThirdWindowSelected = true;
    } else {
      this.isThirdWindowSelected = false;
    }
  }

  ngOnDestroy() {
    if (this.quoteService && this.currentSymbol) {
      this.quoteService.unsubscribeQuote(this.currentSymbol, Constants.WINDOW_NAME_ASSET_SUMMARY);
      if (this.subscriptionQuote) {
        this.subscriptionQuote.unsubscribe();
      }
    }
    if (this.broadcastChannel) {
      this.broadcastChannel.close();
    }
    this.closeSheet();
    this.changingValue.unsubscribe();
  }

  closeSheet() {
    if (this.windowActive != null) {
      this.windowActive.close();
    }
  }

  selectAsset(value: any) {
    if (value) {
      if (value.symbol !== undefined && value.symbol !== '') {
        if (value.symbol.toLowerCase() === 'invalid') {
          this.assetValid = false;
          this.hiddenInvalidActive = true;
          this.currentSymbol = '';
          return;
        }
      }

      if (value.symbol && this.data) {
        this.oldSymbol = this.data.symbol;
        value = value.symbol;
        if (this.route.snapshot.queryParams['ativo']) {
          this.oldSymbol = this.data;
        } else {
          this.data.symbol = value;
        }
        if (this.currentSymbol && value && this.subscriptionQuote !== undefined
          && this.currentSymbol.toLowerCase() === value.toLowerCase()) {
          return;
        }
        if (super.validateOnlyOneAssetAssigned(value, this.data.type, this.data.workspaceId) === true) {
          this._NotificationService.error('Erro', Constants.WORKSPACE_MSG015);
          this.currentSymbol = '';
          super.updateOnDemand(this.oldSymbol, this.currentSymbol, true);
          setTimeout(() => {
            this.currentSymbol = this.oldSymbol;
          }, 0);
          return;
        }
        this.quoteService.unsubscribeQuote(this.currentSymbol, Constants.WINDOW_NAME_ASSET_SUMMARY);
        this._messageQuote = new MessageQuote();
        // super.updateOnDemand(this.oldSymbol, value);
      }
      if (value !== undefined) {
        if (value !== '') {
          this.quoteService.subscribeQuote(value.toLocaleLowerCase(), Constants.WINDOW_NAME_ASSET_SUMMARY);
        }
      }
      this.symbolTemp = value;
      this.isUpdateOnDemand = false;
      this.loading = false;
      this.subscriptionQuote = this.quoteService.messagesQuote
        .subscribe((data: MessageQuote) => {
          if (data && data.errorMessage && data.symbol.toLowerCase() === this.symbolTemp.toLowerCase()) {
            this.hiddenInvalidActive = true;
            this.assetValid = false;
            this.currentSymbol = null;
            this.company = null;
            this.quoteService.unsubscribeQuote(data.symbol.toLowerCase(), Constants.WINDOW_NAME_ASSET_SUMMARY);
            this.loading = false;
          } else if (data !== undefined && data.symbol != null && data.symbol.toLowerCase() === this.symbolTemp.toLowerCase()) {
            if (!this.isUpdateOnDemand) {
              this.isUpdateOnDemand = true;
              if (this.data) {
                super.updateOnDemand(this.oldSymbol, this.symbolTemp);
              }
            }
            this.hiddenInvalidActive = false;
            this.assetValid = true;
            this.changingValue.next(true);
            if (data && (data.symbol.indexOf('win') === 0 || data.symbol.indexOf('wdo') === 0 ||
              data.symbol.indexOf('dol') === 0 || data.symbol.toLowerCase().indexOf('ind') === 0)) {
              this.indiceFuture = true;
            } else {
              this.indiceFuture = false;
            }
            if (Number(data.status) === 3) {
              this.auctionHammer = true;
            } else {
              this.auctionHammer = false;
            }
            if (data && this.currentSymbol && data !== undefined && data.symbol.toLowerCase() === this.currentSymbol.toLowerCase()) {
              if (this._messageQuote.lastTrade) {
                if (this._messageQuote.lastTrade > data.lastTrade) {
                  this.style = 2;
                }
                if (this._messageQuote.lastTrade < data.lastTrade) {
                  this.style = 1;
                }
              }
            }
            if (data && data.ask && data.ask.toString() === '-' && data.bid && data.bid.toString() === '-') {
              this.isIndex = true;
            } else {
              this.isIndex = false;
            }

            this._messageQuote = data;
            if (this._messageQuote.lastTrade && this._messageQuote.lastTrade.toString() === '-') {
              this.showVariation = false;
              this._messageQuote.lastTrade = this._messageQuote.previous;
            }
            if (this._messageQuote.dateLastNegotiation) {
              const today = this.datePipe.transform(new Date(), 'dd/MM/yyyy');
              if (today === this._messageQuote.dateLastNegotiation) {
                this.showVariation = true;
              } else {
                this.showVariation = false;
              }
            } else {
              this.showVariation = true;
            }
            this.loading = false;
            if (this._messageQuote.theoryPrice && this._messageQuote.previous) {
              if ((this._messageQuote.theoryPrice.toString() !== '-') && (this._messageQuote.theoryPrice.toString() !== '-')) {
                this.theoryPercentage = (AppUtils.convertToNumber02(this._messageQuote.theoryPrice.toString()) /
                  AppUtils.convertToNumber02(this._messageQuote.previous.toString()) - 1) * 100;
                this.theoryPercentage = Number(this.theoryPercentage.toFixed(2));
                this.theoryPercentageDisplay = this.theoryPercentage.toString().replace('.', ',');
              } else {
                this.theoryPercentage = null;
                this.theoryPercentageDisplay = null;
              }
            }
            if (!this._messageQuote.unansweredQty || this._messageQuote.unansweredQty.toString() === '-') {
              this._messageQuote.unansweredQty = 0;
            } else {
              this._messageQuote.unansweredQty = AppUtils.convertToNumber02(this._messageQuote.unansweredQty.toString());
            }
            if (this._messageQuote.directionUnansweredQty) {
              this.directionUnansweredQty = this._messageQuote.directionUnansweredQty;
            }
            if (this._messageQuote.marketType === Constants.MARKET_CODE_BOVESPA) {
              this.company = this._messageQuote.description + ' ' + this._messageQuote.classification;
            } else {
              this.company = this._messageQuote.description;
            }
            this.currentSymbol = value;
          }
        });
    }
  }

  openInNewWindow() {
    window.open(Constants.WINDOW_NAME_ASSET_SUMMARY + '?ativo=' + this.currentSymbol);
  }

  openNewWindow() {
    const options = 'width=425px, height=245';
    window.name = Constants.WINDOW_NAME_MAIN_WINDOW;
    if (!this.broadcastChannel) {
      this.broadcastChannel = new BroadcastChannel(Constants.WINDOW_NAME_ASSET_SUMMARY + '-' + this.currentSymbol);
      this.broadcastChannel.onmessage = ((message) => this.buySellOrder(message.data.value, message.data.asset));
    }

    const parameters = '?ativo=' + this.currentSymbol;
    if (this.windowActive == null || this.windowActive.closed) {
      super.getNewWindow('asset-summary', parameters, options, this.data.id)
        .subscribe(window => {
          this.windowActive = window;
        });
    } else {
      this.windowActive.focus();
    }
  }

  buySellOrder(value, asset?: string, price?: string): void {
    if (this.newWindow) {
      window.open('', Constants.WINDOW_NAME_MAIN_WINDOW).focus();
      this.broadcastChannel.postMessage({ value: value, asset: this.currentSymbol });
    } else {
      this._billetOrderService.buySellOrder(value, this.currentSymbol || asset, price);
    }
  }

  parseFloat(value) {
    return parseFloat(value);
  }

  isNaN(value) {
    return isNaN(value);
  }

}
