import { CustomSnackbarServiceMock } from './../../services/_test-mock-services/custom-snackbar-service.service.mock';
import { HttpClientModule } from '@angular/common/http';
import { HttpModule } from '@angular/http';
import { RouterTestingModule } from '@angular/router/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { MatIconModule, MatTabsModule, MatDividerModule, MatTooltipModule, MatProgressSpinnerModule, MatFormFieldModule, MatOptionModule, MatAutocompleteModule, MatCardModule, MatDialogModule, MatBottomSheetModule } from '@angular/material';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { AssetSummaryComponent } from '@business/asset-summary/asset-summary.component';
import { AutoCompleteComponent } from '@component/auto-complete/auto-complete.component';
import { SpinnerComponent } from '@component/spinner/spinner.component';
import { AutoSizeInputDirective } from '@shared/directives/autosize-input.directive';
import { ConfigService } from '@services/webFeeder/config.service';
import { ConfigServiceMock } from '@services/_test-mock-services/_mock-services';
import { NotificationsService, SimpleNotificationsModule } from 'angular2-notifications';
import { CustomSnackbarService } from '@services/custom-snackbar-service.service';
import { DatePipe } from '@angular/common';

describe('AssetSummaryComponent', () => {
  let component: AssetSummaryComponent;
  let fixture: ComponentFixture<AssetSummaryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
         AssetSummaryComponent, 
        AutoCompleteComponent,
         SpinnerComponent,
        AutoSizeInputDirective
       ],
      imports:[
        MatIconModule,
        MatTabsModule,
        MatDividerModule,
        MatTooltipModule,
        MatProgressSpinnerModule,
        MatFormFieldModule,
        MatOptionModule,
        MatAutocompleteModule,
        ReactiveFormsModule,
        MatCardModule,
        RouterTestingModule,
        HttpModule,
        HttpClientModule,
        SimpleNotificationsModule.forRoot(),
        MatDialogModule,
        MatBottomSheetModule
      ],
      providers:[
        DatePipe,
        NotificationsService,
        { provide: ConfigService, useClass: ConfigServiceMock },
        { provide: CustomSnackbarService, useClass: CustomSnackbarServiceMock },

      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AssetSummaryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
