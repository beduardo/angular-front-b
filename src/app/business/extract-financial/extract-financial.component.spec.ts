import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgbDatepickerModule } from '@ng-bootstrap/ng-bootstrap';
import { MatIconModule, MatSelectModule, MatOptionModule, MatFormFieldModule, MatDatepickerModule, MatProgressSpinnerModule, MatCardModule, MatDialogRef, MatInputModule } from '@angular/material';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ExtractFinancialComponent } from './extract-financial.component';
import { NgbdDatepickerRange } from '@component/datepicker-range/datepicker-range';
import { SpinnerComponent } from '@component/spinner/spinner.component';
import { FormsModule } from '@angular/forms';
import { NgbToDatePipe } from '@shared/pipes/ngbToDate.pipe';
import { RouterTestingModule } from '@angular/router/testing';
import { ConfigServiceMock, CustomSnackbarServiceMock } from '@services/_test-mock-services/_mock-services';
import { ConfigService } from '@services/webFeeder/config.service';
import { HttpModule } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';
import { DatePipe } from '@angular/common';
import { NotificationsService, SimpleNotificationsModule } from 'angular2-notifications';
import { CustomSnackbarService } from '@services/custom-snackbar-service.service';

describe('ExtractFinancialComponent', () => {
  let component: ExtractFinancialComponent;
  let fixture: ComponentFixture<ExtractFinancialComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        ExtractFinancialComponent,
        NgbdDatepickerRange,
        SpinnerComponent,
        NgbToDatePipe
      ],
      imports: [
        MatIconModule,
        MatSelectModule,
        MatOptionModule,
        MatFormFieldModule,
        FormsModule,
        NgbDatepickerModule,
        MatDatepickerModule,
        MatProgressSpinnerModule,
        MatCardModule, 
        RouterTestingModule,
        HttpModule,
        HttpClientModule,
        SimpleNotificationsModule.forRoot(),
        MatInputModule,
        BrowserAnimationsModule
      ],
      providers:[
        DatePipe,
        NotificationsService,
        { provide: MatDialogRef, useValue: {}, },
        { provide: ConfigService, useClass: ConfigServiceMock },
        { provide: CustomSnackbarService, useClass: CustomSnackbarServiceMock },
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExtractFinancialComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
