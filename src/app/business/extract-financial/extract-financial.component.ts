import { Constants } from '@utils/constants';
import { Component, OnInit, OnDestroy, HostListener, ViewChild, ViewContainerRef } from '@angular/core';
import { MatDialogRef } from '@angular/material';
import { Subject, Subscription } from 'rxjs';
import { NgxMasonryOptions } from 'ngx-masonry';
import { NotificationsService } from 'angular2-notifications';
import { AuthServer } from '@services/auth-server.service';
import { ConfigService } from '@services/webFeeder/config.service';
import { AppUtils } from '@utils/app.utils';
import { AutoLogoutService } from '@auth/auto-logout-service.service';
import { CustomSnackbarService } from '@services/custom-snackbar-service.service';
import { StatusConnection } from '@model/status-connection';
import { StatusOmsService } from '@services/webFeeder/sckt-status-oms.service';

@Component({
  selector: 'app-extract-financial',
  templateUrl: './extract-financial.component.html'
})
export class ExtractFinancialComponent implements OnInit, OnDestroy {

  public title = 'EXTRATO FINANCEIRO';
  public loading = false;
  public dateStartFilter: Date;
  public dateEndFilter: Date;
  public account: string;
  public market: string;
  public changingValue: Subject<boolean> = new Subject();
  public myOptions: NgxMasonryOptions = {
    originLeft: true,
    transitionDuration: '1.17s'
  };
  public listMarket = [
    { value: 'XBSP', desc: 'Bovespa' },
    { value: 'XBMF', desc: 'BMF' }
  ];
  public listExtract: any = [];
  private subscriptionStatusOms: Subscription;
  public statusOmsOnline: boolean;
  public msgStatusOmsOffile = Constants.MSG_STATUS_OFFLINE_OMS;


  @HostListener('mousemove')
  onMouseMove() {
    this._autoLogoutService.resetTime();
  }

  @HostListener('click')
  onClick() {
    this._autoLogoutService.resetTime();
  }
  @ViewChild('ExtractFinancialComponent', { read: ViewContainerRef })
  parent: ViewContainerRef;
  filterDateRange: number;

  // @HostListener('document:keydown', ['$event'])
  // onkeydown(event) {
  //   if (event.key == 'Escape') {
  //     if ((document.getElementsByTagName('ngb-modal-window').length == 0)) {
  //       this.close();
  //     }
  //   }
  // }

  constructor(
    public dialogRef: MatDialogRef<ExtractFinancialComponent>,
    private _configService: ConfigService,
    private _authServer: AuthServer,
    private _notificationService: NotificationsService,
    private _autoLogoutService: AutoLogoutService,
    private _snackBar: CustomSnackbarService,
    private _statusOmsService: StatusOmsService
  ) {
    this.account = this._configService.getBrokerAccountSelected();
    this.market = this.listMarket[0].value;
    this.filterDateRange = this._configService.getExtractFinancialFilterRange();
    this.subscriptionStatusOms = this._statusOmsService.messagesStatusOms
      .subscribe(statusOms => {
        if (statusOms === StatusConnection.ERROR) {
          this.loading = false;
          this.statusOmsOnline = false;
        } else {
          this.statusOmsOnline = true;
        }
      });
  }

  ngOnInit() {
  }

  ngOnDestroy() {
    this.changingValue.unsubscribe();
    if (this.subscriptionStatusOms) {
      this.subscriptionStatusOms.unsubscribe();
    }
  }

  close() {
    this.dialogRef.close();
  }

  changeDateIni(value) {
    if (value) {
      this.dateStartFilter = new Date(value);
    }
  }

  changeDateFim(value) {
    if (value) {
      this.dateEndFilter = new Date(value);
    }
  }

  clearFilter() {
    this.changingValue.next(true);
    this.dateStartFilter = null;
    this.dateEndFilter = null;
  }

  searchFilter() {
    if (this.dateStartFilter !== this.dateEndFilter && !AppUtils.validateDateRange(this.dateStartFilter, this.dateEndFilter, this.filterDateRange)) {
      const msg = Constants.DATE_MSG005.replace('{{range}}', this.filterDateRange.toString());
      this._snackBar.createSnack(this.parent, msg, Constants.ALERT_TYPE.warn);
      return;
    }
    this.loading = true;
    this.listExtract = [];
    this._authServer.iNegotiation.statementAccount(this.account, this.market, this.dateStartFilter, this.dateEndFilter, null)
      .subscribe(res => {
        if (res && res.listBeans) {
          this.listExtract = res.listBeans;
          this.listExtract.forEach(element => {
            if (element.balance) {
              const tickSize = element.balance.toString().split(',')[1].length;
              const isDebPositive = AppUtils.convertToNumber(element.amount.toString(), tickSize, ',');
              const isAmountPositive = AppUtils.convertToNumber(element.balance.toString(), tickSize, ',');
              if (isDebPositive > 0) {
                element.isDebPositive = true;
              }
              if (isAmountPositive > 0) {
                element.isAmountPositive = 2;
              } else if (isAmountPositive < 0) {
                element.isAmountPositive = 1;
              } else {
                element.isAmountPositive = 0;
              }
            }
          });
        } else {
          this._notificationService.error('', 'Extrato financeiro não encontrado para o período selecionado.');
        }
        this.loading = false;
      }, error => {
        this._notificationService.error('', Constants.BROKER_MSG002);
        this.loading = false;
      });
  }

}
