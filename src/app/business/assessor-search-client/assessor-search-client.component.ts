import { ConfigService } from './../../services/webFeeder/config.service';
import { Component, OnInit, Input, ViewChild, Output, EventEmitter, ViewContainerRef, AfterViewInit, OnDestroy, HostListener } from '@angular/core';
import { MatDialogRef } from '@angular/material';
import { Subject } from 'rxjs';
import { ICellEditorAngularComp } from 'ag-grid-angular';
import { NotificationsService } from 'angular2-notifications';
import { Constants } from '@utils/constants';
import { AutoLogoutService } from '@auth/auto-logout-service.service';

@Component({
  selector: 'app-assessor-search-client',
  templateUrl: './assessor-search-client.component.html'
})
export class AssessorSearchClientComponent implements OnInit, OnDestroy, AfterViewInit, ICellEditorAngularComp {

  public inputName = false;
  public username = '';
  public image = '';
  public listAssessorClient: any;
  public listAssessorClientAux: any;
  public loadingInit: any;
  public loading = true;

  // autoComplete
  @Input() autoSizeActive = false;
  @Input() nameAutoComplete: string;
  @Input() autoFocus: boolean;
  @Input() reset: Subject<boolean>;

  @ViewChild('autoInput', { read: ViewContainerRef }) public input;

  @Output() hiddenInvalidOutput = new EventEmitter<Boolean>();
  @Output() rawInformation = new EventEmitter<String>();

  public hiddenInvalid = true;
  public isWorksheet = false;
  private params: any;
  private selectedFlag = false;
  public isPaste: boolean;

  @HostListener('mousemove')
  onMouseMove() {
    this._autoLogoutService.resetTime();
  }

  @HostListener('click')
  onClick() {
    this._autoLogoutService.resetTime();
  }

  constructor(
    public _dialogRef: MatDialogRef<AssessorSearchClientComponent>,
    public _configService: ConfigService,
    private _notificationService: NotificationsService,
    private _autoLogoutService: AutoLogoutService,
  ) {
    this.nameAutoComplete = '';
    this.listAssessorClientAux = this._configService.getAssessorListClients();
    this.listAssessorClient = this._configService.getAssessorListClients();
    this.image = './assets/images/assets/avatar.svg';
    this.loadingInit = setTimeout(() => {
      this.loading = false;
    }, 1000);
  }

  toggleInput() {
    this.hiddenInvalid = !this.hiddenInvalid;
  }

  closeDialog() {
    this._dialogRef.close();
    const elem = document.getElementById('sidebar-left');
    elem.style.zIndex = '1';
  }

  // autoComplete
  ngOnInit() {
    this.nameAutoComplete = '';
    if (this.reset) {
      setTimeout(() => {
        this.reset.subscribe(v => {
          this.hiddenInvalid = true;
        });
      }, 0);
    }
  }

  ngOnDestroy() {
    if (this.reset) {
      this.reset.unsubscribe();
    }
    if (this.loadingInit) {
      clearTimeout(this.loadingInit);
    }
  }

  ngAfterViewInit() {
    if (this.autoFocus) {
      setTimeout(() => {
        this.focusInput();
      });
    }
  }

  focusInput() {
    this.input.element.nativeElement.focus();
  }

  getValue(): any {
    return this.nameAutoComplete;
  }

  onInputEnter(event) {
    setTimeout(() => {
      if (!this.selectedFlag) {
        if (event.target.value === '' && event.key.includes('Enter')) {
          this._notificationService.warn('', Constants.WORKSPACE_MSG019);
          return;
        }
      } else {
        this.selectedFlag = false;
      }
    }, 200);
  }

  onAutoCompletAsset(textInput: any) {
    this.hiddenInvalid = true;
    this.hiddenInvalidOutput.emit(this.hiddenInvalid);
    if (textInput.key !== 'ArrowDown' && textInput.key !== 'ArrowUp' && textInput.key !== 'Enter') {
      if (textInput.target === undefined) {
        this.nameAutoComplete = textInput.toLowerCase();
      } else {
        this.nameAutoComplete = textInput.target.value.toLowerCase();
      }
      if (!this.isPaste) {
        this.rawInformation.emit(this.nameAutoComplete);
      }
      this.listAssessorClient = this.listAssessorClientAux;
      this.listAssessorClient = this.listAssessorClient.filter(element =>
        element.user.full_name.toLowerCase().indexOf(this.nameAutoComplete) === 0);
      this.isPaste = true;
      return this.listAssessorClient;
    }
  }

  callBackWorksheet() {
    if (this.isWorksheet) {
      this.params.api.stopEditing();
    }
  }

  agInit(params: any): void {
    this.params = params;
    this.nameAutoComplete = this.params.value;
    this.isWorksheet = true;
    setTimeout(() => {
      this.input.element.nativeElement.focus();
    });
  }

  selecClient(client) {
    this._configService.setAssessorClientSelected(client);
    this._configService.setBrokerAccountSelected(client.user.account_id);
    this._configService.setBrokerAccountSelectedTransition(client.user.account_id);
    this._configService._clientAssessorChangeSelect.next(true);
    setTimeout(() => {
      this._notificationService.success('', 'Cliente ' + client.user.first_name.toUpperCase() + ' selecionado!');
    }, 1000);
  }

}
