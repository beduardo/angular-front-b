import { CustomSnackbarServiceMock } from './../../services/_test-mock-services/custom-snackbar-service.service.mock';
import { CustomSnackbarService } from './../../services/custom-snackbar-service.service';
import { ConfigService } from '@services/webFeeder/config.service';
import { AutoSizeInputDirective } from './../../shared/directives/autosize-input.directive';
import { SpinnerComponent } from './../../component/spinner/spinner.component';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { AssessorSearchClientComponent } from './assessor-search-client.component';
import { MatIconModule, MatFormFieldModule, MatProgressSpinnerModule, MatCardModule, MatDialogRef, MatDialogModule } from '@angular/material';
import { RouterTestingModule } from '@angular/router/testing';
import { ConfigServiceMock } from '@services/_test-mock-services/_mock-services';
import { NotificationsService, SimpleNotificationsModule } from 'angular2-notifications';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

describe('AssessorSearchClientComponent', () => {
  let component: AssessorSearchClientComponent;
  let fixture: ComponentFixture<AssessorSearchClientComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        AssessorSearchClientComponent,
        SpinnerComponent,
        AutoSizeInputDirective
      ],
      imports: [
        MatIconModule,
        MatFormFieldModule,
        MatProgressSpinnerModule,
        MatCardModule,
        RouterTestingModule,
        SimpleNotificationsModule.forRoot(),
        BrowserAnimationsModule,
        MatDialogModule,
      ],
      providers: [
        NotificationsService,
        { provide: MatDialogRef, useValue: {} },
        { provide: ConfigService, useClass: ConfigServiceMock },
        { provide: CustomSnackbarService, useClass: CustomSnackbarServiceMock },
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AssessorSearchClientComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
