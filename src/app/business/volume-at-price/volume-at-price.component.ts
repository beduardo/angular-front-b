import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import { ActivatedRoute } from '@angular/router';
import { AbstractItem } from '@business/base/abstract-item';
import { AbstractItemService } from '@business/base/abstract-item.service';
import { MessageQuote } from '@model/webFeeder/message-quote.model';
import { ConfigService } from '@services/webFeeder/config.service';
import { QuoteService } from '@services/webFeeder/sckt-quotes.service';
import { TokenService } from '@services/webFeeder/token.service';
import { WorkspaceNewService } from '@services/workspace-new.service';
import { AppUtils } from '@utils/app.utils';
import { Constants } from '@utils/constants';
import { NotificationsService } from 'angular2-notifications';
import { Subscription } from 'rxjs';
import { AuthServer } from '@services/auth-server.service';
import { DatePipe } from '@angular/common';
import { environment } from 'environments/environment';

@Component({
  selector: Constants.WINDOW_NAME_VOLUME_AT_PRICE,
  templateUrl: './volume-at-price.component.html'
})
export class VolumeAtPriceComponent extends AbstractItem implements OnInit, OnDestroy {
  @Input() data: any;

  public assetValid = false;
  public symbol: string;
  public messageQuote = new MessageQuote();
  public rowOffersData: any[] = [];
  public windowActive: any;
  public newWindow: boolean;
  public closedMarket: boolean;
  public loading = true;
  public executeFirstTime = false;
  public lastTrade: any;
  public lastTradePrevious: any;
  public variation: any;
  public tickSize: any;
  public period: any;
  public low: number;
  public high: number;
  public open: number;
  public previous: number;
  public adjustment: number;
  public valueBuy: number;
  public valueSell: number;
  public percentageBuy: number;
  public percentageSell: number;
  public classPressure: string;
  public style = 0;
  public last;
  public periodValue = 0;
  public classBarVolumeWidth: string;
  public classBarVolumeWidthOfBuyer: any;
  public classBarVolumeWidthOfSeller: any;
  public classBarVolumeWidthOfDirect: any;
  public classBarVolumeWidthOfUndetermined: any;
  public setInterval;
  pressureBarPercentageLeft: string;
  pressureBarPercentageRight: string;
  tooltiphover: boolean;
  volumeBuy: number;
  volumeSell: number;
  public indiceFuture: boolean;
  public hiddenInvalid: boolean;
  private _subscriptionQuote: Subscription;
  dialog: MatDialog;
  msgInvalidAsset = Constants.WORKSPACE_MSG007;
  closedMarketmsg = Constants.CLOSED_MARKET_MESSAGE;
  public msgNoData = Constants.WORKSPACE_MSG018;
  maxVolume = 0;
  public company: string;
  public oldSymbolTemp: string;
  protected workspaceService: WorkspaceNewService;
  public timeUpdate: any;
  public isGetVap: boolean;
  public showEmpty: boolean;
  public auctionHammer: boolean;
  public showVariation: boolean;
  public isUpdateOnDemand: boolean;
  public isLoginPI: boolean;

  constructor(
    private _quoteService: QuoteService,
    private _route: ActivatedRoute,
    workspaceService: WorkspaceNewService,
    itemService: AbstractItemService,
    dialog: MatDialog,
    public _NotificationService: NotificationsService,
    private _authServer: AuthServer,
    private datePipe: DatePipe,
    private _configService: ConfigService
  ) {
    super(workspaceService, dialog, itemService);
    this.isLoginPI = this._configService.isLoginPi();
    this.workspaceService = workspaceService;
    this.dialog = dialog;
    this.periodValue = 0;
    this.period = new Array();
    this.period.push({ description: 'DIA', value: 0, selected: true });
    this.period.push({ description: '60M', value: 60, selected: false });
    this.period.push({ description: '30M', value: 30, selected: false });
    this.period.push({ description: '15M', value: 15, selected: false });
    this.period.push({ description: '10M', value: 10, selected: false });
    this.period.push({ description: '5M', value: 5, selected: false });
    this.period.push({ description: '1M', value: 1, selected: false });
    this.classPressure = 'linear-gradient(to right, #ffb300 50%, #78bd36 50%)';
    this.percentageBuy = 50;
    this.percentageSell = 50;
    this.valueBuy = 0;
    this.valueSell = 0;
    this.volumeBuy = 0;
    this.volumeSell = 0;
    this.pressureBarPercentageRight = '50%';
    this.pressureBarPercentageLeft = '50%';
    this.tooltiphover = false;
  }

  ngOnInit() {
    if (this.data && this.data.symbol) {
      this.symbol = this.data.symbol;
      this.setQuote(this.data.symbol);
    } else {
      this.newWindow = true;
      this._route.queryParams.subscribe(params => {
        if (params['symbol']) {
          this.symbol = params['symbol'];
          setTimeout(() => {
            this.setQuote(this.symbol);
          }, 2000);
        }
      });
    }
    this.setInterval = setInterval(() => {
      if (this.messageQuote.symbol && (this.messageQuote.marketType === 1 || this.messageQuote.marketType === 3)) {
        this.getVap(this.messageQuote.symbol, this.messageQuote.marketType);
      } else {
        clearInterval(this.setInterval);
      }
    }, Constants.INTERVAL_VOLUME_AT_PRICE);
  }


  ngOnDestroy() {
    if (this.setInterval) {
      clearInterval(this.setInterval);
    }
    this.unsubscribeQuote();
    if (this._subscriptionQuote) {
      this._subscriptionQuote.unsubscribe();
    }
    this.closeSheet();
  }

  getVap(symbol: string, marketType: number) {
    if (symbol && (marketType === 1 || marketType === 3)) {
      this.loading = true;
      this._authServer.iQuote.getVolumeAtPrice(this.symbol, this.periodValue)
        .subscribe(data => {
          if (data && data.values && data.values.length > 0) {
            this.rowOffersData = data.values;
            this.rowOffersData.forEach((value, id) => {
              this.rowOffersData[id].class = this.getCssForPrice(value.price);
              this.rowOffersData[id].tooltipTitle = this.getTooltipTitle(value.price);
              this.volumeSell += Number(this.rowOffersData[id].volumeSell);
              this.volumeBuy += Number(this.rowOffersData[id].volumeBuy);
              this.rowOffersData[id].price = AppUtils.convertNumberToCurrency(Number(this.rowOffersData[id].price), 2);
              if (this.rowOffersData[id].price && this.rowOffersData[id].price.split(',')[1] === '00') {
                this.rowOffersData[id].price = this.rowOffersData[id].price.split(',')[0];
              }
            });
            this.calculateWidthOfBarVolume();
            AppUtils.orderObjectDesc(this.rowOffersData, 'price');
            this.timeUpdate = this.datePipe.transform(new Date(), Constants.DATE_UPDATE_VOLUME);
          } else {
            this.rowOffersData = [];
          }
          this.showEmpty = false;
          this.loading = false;
          this.calculatePressure();
        });
    } else {
      this.showEmpty = true;
      this.loading = false;
    }
  }

  selectAsset(event) {
    if (this.data) {
      this.data.symbol = event.symbol;
    }
    this.resetQuote();
    this.setQuote(event.symbol);
    // this.getVap(this.messageQuote.symbol, this.messageQuote.marketType);
  }

  setQuote(symbol: string) {
    const oldSymbol = this.symbol;
    this.oldSymbolTemp = oldSymbol;
    symbol = AppUtils.trimStart(symbol);
    this.symbol = symbol;
    // if (!this.newWindow) {
    //   super.updateOnDemand(this.oldSymbolTemp, this.symbol);
    // }
    this.executeFirstTime = false;
    this.subscribeQuote();
  }

  openNewWindow() {
    const options = 'width=470px, height=500';
    const parameters = '?symbol=' + this.symbol;
    if (this.windowActive == null || this.windowActive.closed) {
      super.getNewWindow('volume-at-price', parameters, options, this.data.id).subscribe(window => {
        this.windowActive = window;
      });
    } else {
      this.windowActive.focus();
    }
  }

  closeSheet() {
    if (this.windowActive != null) {
      this.windowActive.close();
    }
  }

  resetQuote() {
    if (!this.symbol) {
      return;
    }

    const oldSymbol = this.symbol;
    if (this.data && oldSymbol.toLowerCase() === this.data.symbol.toLowerCase()) {
      return;
    } else if (this.data && super.validateOnlyOneAssetAssigned(this.data.symbol, this.data.type, this.data.workspaceId) === true) {
      this._NotificationService.error('Erro', Constants.WORKSPACE_MSG015);
      this.symbol = '';
      super.updateOnDemand(oldSymbol, this.symbol, true);
      setTimeout(() => {
        this.symbol = oldSymbol;
      }, 0);
      return;
    }

    this.unsubscribeQuote();
    this.percentageBuy = 50;
    this.percentageSell = 50;
    this.lastTrade = null;
    this.lastTradePrevious = null;
    this.variation = null;
    this.messageQuote = new MessageQuote();
    this.rowOffersData = [];
    this.messageQuote.high = 50;
    this.messageQuote.low = 50;
  }

  onChangePeriod(item) {
    if (this.periodValue === item.value) {
      return;
    }
    this.loading = true;
    clearInterval(this.setInterval);
    this.periodValue = item.value;
    this.getVap(this.messageQuote.symbol, this.messageQuote.marketType);

    for (let i = 0; i < this.period.length; i++) {
      if (this.period[i].value === item.value) {
        this.period[i].selected = true;
      } else {
        this.period[i].selected = false;
      }
    }

    this.setInterval = setInterval(() => {
      if (this.messageQuote.symbol && (this.messageQuote.marketType === 1 || this.messageQuote.marketType === 3)) {
        this.getVap(this.messageQuote.symbol, this.messageQuote.marketType);
      } else {
        clearInterval(this.setInterval);
      }
    }, Constants.INTERVAL_VOLUME_AT_PRICE);
  }

  getCssForPrice(value) {
    value = value.replace(',', '.').trim();
    if (this.messageQuote.tickSize) {
      value = AppUtils.convertNumberToCurrency(
        Number(value),
        this.messageQuote.tickSize
      );
    } else {
      value = AppUtils.convertNumberToCurrency(
        Number(value),
        this.tickSize
      );
    }
    if (value.trim() === this.lastTrade) {
      return 'price-current-color';
    }

    if (value.trim() === this.high) {
      return 'price-high-color';
    }

    if (value.trim() === this.low) {
      return 'price-low-color';
    }

    if (value.trim() === this.open) {
      return 'price-open-color';
    }

    if (value.trim() === this.previous) {
      return 'price-close-color';
    }

    if (value.trim() === this.adjustment) {
      return 'price-adjustment-color';
    }
  }

  getTooltipTitle(value) {
    value = value.replace(',', '.').trim();
    value = AppUtils.convertNumberToCurrency(
      Number(value),
      this.tickSize
    );
    if (value.trim() === this.lastTrade) {
      return 'Preço atual';
    }

    if (value.trim() === this.high) {
      return 'Preço máximo';
    }

    if (value.trim() === this.low) {
      return 'Preço mínimo';
    }

    if (value.trim() === this.open) {
      return 'Preço de abertura';
    }

    if (value.trim() === this.previous) {
      return 'Preço de fechamento';
    }

    if (value.trim() === this.adjustment) {
      return 'Preço de Ajuste';
    }
  }

  private unsubscribeQuote() {
    if (this._quoteService) {
      this._quoteService.unsubscribeQuote(this.symbol, Constants.WINDOW_NAME_VOLUME_AT_PRICE);
    }
    if (this._subscriptionQuote) {
      // this._subscriptionQuote.unsubscribe();
    }
  }

  private subscribeQuote() {
    if (this.oldSymbolTemp && this.symbol && this.oldSymbolTemp.toLowerCase() === this.symbol.toLowerCase() && this._subscriptionQuote) {
      return;
    }
    if (this.symbol.toLowerCase() === 'invalid') {
      this.hiddenInvalid = true;
      this.resetQuote();
      this.symbol = '';
      return;
    }
    const msgItem = this._quoteService.subscribeQuote(this.symbol, Constants.WINDOW_NAME_VOLUME_AT_PRICE);
    if (msgItem !== undefined && msgItem !== '') {
      this.assetValid = false;
      this.resetQuote();
      return;
    }
    this.isUpdateOnDemand = false;
    this.isGetVap = false;
    this._subscriptionQuote = this._quoteService.messagesQuote
      .subscribe((msg: MessageQuote) => {
        if (msg && msg.errorMessage && (this.symbol && msg.symbol.toLowerCase() === this.symbol.toLowerCase())) {
          this.hiddenInvalid = true;
          this.symbol = null;
          this.company = null;
          this._quoteService.unsubscribeQuote(msg.symbol.toLowerCase(), Constants.WINDOW_NAME_VOLUME_AT_PRICE);
        } else if (msg !== undefined && msg.symbol != null && msg.symbol === this.symbol) {
          this.messageQuote = msg;
          if (!this.isUpdateOnDemand) {
            this.isUpdateOnDemand = true;
            if (!this.newWindow) {
              super.updateOnDemand(this.oldSymbolTemp, this.symbol);
            }
          }
          if (!this.isGetVap) {
            this.isGetVap = true;
            this.getVap(this.messageQuote.symbol, this.messageQuote.marketType);
          }
          this.hiddenInvalid = false;
          if (this.messageQuote.symbol.indexOf('win') === 0 || this.messageQuote.symbol.indexOf('wdo') === 0 ||
            this.messageQuote.symbol.indexOf('dol') === 0 || this.messageQuote.symbol.toLowerCase().indexOf('ind') === 0) {
            this.indiceFuture = true;
          } else {
            this.indiceFuture = false;
          }
          if (Number(this.messageQuote.status) === 3) {
            this.auctionHammer = true;
          } else {
            this.auctionHammer = false;
          }
          if (msg.lastTrade && msg.lastTrade.toString() === '-') {
            this.showVariation = false;
            msg.lastTrade = msg.previous;
          }
          if (msg.dateLastNegotiation) {
            const today = this.datePipe.transform(new Date(), 'dd/MM/yyyy');
            if (today === msg.dateLastNegotiation) {
              this.showVariation = true;
            } else {
              this.showVariation = false;
            }
          } else {
            this.showVariation = true;
          }

          if (this.messageQuote.errorMessage !== undefined && this.messageQuote.errorMessage !== '') {
            this.assetValid = false;
            this.resetQuote();
          } else {
            this.loadMessageQuote();
          }
          if (msg.description && msg.description !== '' && this.messageQuote.marketType === Constants.MARKET_CODE_BOVESPA) {
            this.company = this.messageQuote.description + ' ' + this.messageQuote.classification;
          } else {
            this.company = this.messageQuote.description;
          }
        }
      }
      );
  }

  private loadMessageQuote() {
    this.assetValid = true;
    if (!this.executeFirstTime) {
      this.loadMessageQuoteFirsTime();
    }
    this.executeFirstTime = true;
    this.last = this.lastTrade;
    if (this.messageQuote.lastTrade && this.messageQuote.lastTrade.toString() === '-') {
      this.messageQuote.lastTrade = this.messageQuote.previous;
    }
    this.lastTrade = AppUtils.convertNumberToCurrency(this.messageQuote.lastTrade, 2);
    this.lastTradePrevious = this.messageQuote.lastTrade;
    this.variation = this.messageQuote.variation;
    this.low = this.messageQuote.low;
    this.high = this.messageQuote.high;
    this.open = this.messageQuote.open;
    this.adjustment = this.messageQuote.adjustment;
    this.previous = this.messageQuote.previous;

    if (this.messageQuote.lastTrade) {
      if (this.lastTrade < this.last) {
        this.style = 2;
      }
      if (this.lastTrade > this.last) {
        this.style = 1;
      }
    }
  }

  private loadMessageQuoteFirsTime() {
    this.tickSize = this.messageQuote.tickSize;
    this.tickSize = AppUtils.GetTickSize(
      this.messageQuote.symbol,
      this.tickSize
    );
    this.style = 0;
  }

  private calculateWidthOfBarVolume() {
    let left = 0;
    for (let i = 0; i < this.rowOffersData.length; i++) {

      let totalVol = 0;
      const volumeBuy = this.rowOffersData[i].volumeBuy ?
        AppUtils.convertBrCurrencyToNumber(this.rowOffersData[i].volumeBuy, ',') : 0;
      const volumeSell = this.rowOffersData[i].volumeSell ?
        AppUtils.convertBrCurrencyToNumber(this.rowOffersData[i].volumeSell, ',') : 0;
      const volumeDirect = this.rowOffersData[i].volumeDirect ?
        AppUtils.convertBrCurrencyToNumber(this.rowOffersData[i].volumeDirect, ',') : 0;
      const volumeUndefined = this.rowOffersData[i].volumeUndefined ?
        AppUtils.convertBrCurrencyToNumber(this.rowOffersData[i].volumeUndefined, ',') : 0;
      totalVol = volumeBuy + volumeSell + volumeDirect + volumeUndefined;

      let balance = 0;
      balance = volumeBuy - volumeSell;

      this.rowOffersData[i].totalVolumeUnformatted = totalVol;
      this.rowOffersData[i].totalVolume = AppUtils.convertNumberToOptimizedNumber(totalVol);
      this.rowOffersData[i].balance = balance;

      left = 0;
      // Buyer
      const barVolumeBuyerPerc = (volumeBuy * 100) / totalVol;
      this.rowOffersData[i]['classBarVolumeWidthOfBuyer'] = { 'width': barVolumeBuyerPerc + '%', 'left': left + '%' };
      left += barVolumeBuyerPerc;

      // Seller
      const barVolumeSellerPerc = (volumeSell * 100) / totalVol;
      this.rowOffersData[i]['classBarVolumeWidthOfSeller'] = { 'width': barVolumeSellerPerc + '%', 'left': left + '%' };
      left += barVolumeSellerPerc;

      // Direct
      const barVolumeDirectPerc = (volumeDirect * 100) / totalVol;
      this.rowOffersData[i]['classBarVolumeWidthOfDirect'] = { 'width': barVolumeDirectPerc + '%', 'left': left + '%' };
      left += barVolumeDirectPerc;

      // Undetermined
      const barVolumeUndeterPerc = (volumeUndefined * 100) / totalVol;
      this.rowOffersData[i]['classBarVolumeWidthOfUndetermined'] = { 'width': barVolumeUndeterPerc + '%', 'left': left + '%' };
    }
    this.rowOffersData.map(elem => {
      if (elem.totalVolumeUnformatted !== undefined && elem.totalVolumeUnformatted > this.maxVolume) {
        this.maxVolume = elem.totalVolumeUnformatted;
      }
    });
  }

  private calculatePressure() {
    if (this.volumeBuy && this.volumeSell) {
      const totalValue = this.volumeBuy + this.volumeSell;
      this.percentageBuy = ((this.volumeBuy * 100) / totalValue);
      this.percentageSell = ((this.volumeSell * 100) / totalValue);
      if (this.percentageBuy > this.percentageSell) {
        const gradientSell = this.percentageBuy + 10;
        switch (environment.WHITELABEL) {
          case 1:
            this.classPressure = 'linear-gradient(to right, #ffb300 ' + (this.percentageBuy).toFixed(2) + '%, #78bd36 ' + (gradientSell).toFixed(2) + '%)';
            break;
          case 2:
            this.classPressure = 'linear-gradient(to right, #6740ff ' + (this.percentageBuy).toFixed(2) + '%, #ff2c79 ' + (gradientSell).toFixed(2) + '%)';
            break;
          case 3:
            this.classPressure = 'linear-gradient(to right, #ff9204 ' + (this.percentageBuy).toFixed(2) + '%, #78bd36 ' + (gradientSell).toFixed(2) + '%)';
            break;
          case 5:
            this.classPressure = 'linear-gradient(to right, #6298d5 ' + (this.percentageBuy).toFixed(2) + '%, #f06666 ' + (gradientSell).toFixed(2) + '%)';
            break;
          case 6:
            this.classPressure = 'linear-gradient(to right, #0088ca ' + (this.percentageBuy).toFixed(2) + '%, #009e73 ' + (gradientSell).toFixed(2) + '%)';
            break;
          case 8:
            this.classPressure = 'linear-gradient(to right, #fcaf17 ' + (this.percentageBuy).toFixed(2) + '%, #78bd36 ' + (gradientSell).toFixed(2) + '%)';
            break;
          case 9:
            this.classPressure = 'linear-gradient(to right, #ffb44a ' + (this.percentageBuy).toFixed(2) + '%, #00af73 ' + (gradientSell).toFixed(2) + '%)';
            break;
          default:
            this.classPressure = 'linear-gradient(to right, #ffb300 ' + (this.percentageBuy).toFixed(2) + '%, #78bd36 ' + (gradientSell).toFixed(2) + '%)';
            break;
        }
      } else {
        const gradientBuy = this.percentageSell + 10;
        switch (environment.WHITELABEL) {
          case 1:
            this.classPressure = 'linear-gradient(to left, #78bd36 ' + (this.percentageSell).toFixed(2) + '%, #ffb300 ' + (gradientBuy).toFixed(2) + '%)';
            break;
          case 2:
            this.classPressure = 'linear-gradient(to left, #ff2c79 ' + (this.percentageSell).toFixed(2) + '%, #6740ff ' + (gradientBuy).toFixed(2) + '%)';
            break;
          case 3:
            this.classPressure = 'linear-gradient(to left, #78bd36 ' + (this.percentageSell).toFixed(2) + '%, #ff9204 ' + (gradientBuy).toFixed(2) + '%)';
            break;
          case 5:
            this.classPressure = 'linear-gradient(to left, #f06666 ' + (this.percentageSell).toFixed(2) + '%, #6298d5 ' + (gradientBuy).toFixed(2) + '%)';
            break;
          case 6:
            this.classPressure = 'linear-gradient(to left, #009e73 ' + (this.percentageSell).toFixed(2) + '%, #0088ca ' + (gradientBuy).toFixed(2) + '%)';
            break;
          case 8:
            this.classPressure = 'linear-gradient(to left, #78bd36 ' + (this.percentageSell).toFixed(2) + '%, #fcaf17 ' + (gradientBuy).toFixed(2) + '%)';
            break;
          case 9:
            this.classPressure = 'linear-gradient(to left, #00af73 ' + (this.percentageSell).toFixed(2) + '%, #ffb44a ' + (gradientBuy).toFixed(2) + '%)';
            break;
          default:
            this.classPressure = 'linear-gradient(to left, #78bd36 ' + (this.percentageSell).toFixed(2) + '%, #ffb300 ' + (gradientBuy).toFixed(2) + '%)';
            break;
        }
      }
    } else {
      switch (environment.WHITELABEL) {
        case 1:
          this.classPressure = 'linear-gradient(to right, #ffb300 50%, #78bd36 55%)';
          break;
        case 2:
          this.classPressure = 'linear-gradient(to right, #6740ff 50%, #ff2c79 55%)';
          break;
        case 3:
          this.classPressure = 'linear-gradient(to right, #ff9204 50%, #78bd36 55%)';
          break;
        case 5:
          this.classPressure = 'linear-gradient(to right, #6298d5 50%, #f06666 55%)';
          break;
        case 6:
          this.classPressure = 'linear-gradient(to right, #0088ca 50%, #009e73 55%)';
          break;
        case 8:
          this.classPressure = 'linear-gradient(to right, #fcaf17 50%, #78bd36 55%)';
          break;
        case 9:
          this.classPressure = 'linear-gradient(to right, #ffb44a 50%, #00af73 55%)';
          break;
        default:
          this.classPressure = 'linear-gradient(to right, #ffb300 50%, #78bd36 55%)';
          break;
      }
      this.percentageBuy = 0;
      this.percentageSell = 0;
    }
  }

  mouseEnterBrokerRow() {
    this.tooltiphover = true;

    setTimeout(() => {
      this.mouseLeaveBrokerRow();
    }, 8000);
  }

  mouseLeaveBrokerRow() {
    this.tooltiphover = false;
  }
}
