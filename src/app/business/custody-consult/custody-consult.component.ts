
import { AppUtils } from './../../utils/app.utils';
import { animate, style, transition, trigger } from '@angular/animations';
import { Component, ElementRef, HostListener, OnInit, Renderer, ViewChild, OnDestroy } from '@angular/core';
import { MatDialogRef } from '@angular/material';
import { Router } from '@angular/router';
import { AuthService } from '@auth/auth.service';
import { AutoLogoutService } from '@auth/auto-logout-service.service';
import { BmfModel } from '@dto/custody-model-bmf.dto';
import { BovModel } from '@dto/custody-model-bov.dto';
import { ConfigService } from '@services/webFeeder/config.service';
import { Constants } from '@utils/constants';
import { PositionService } from '@services/webFeeder/sckt-position.service';
import { Subscription } from 'rxjs-compat';
import { MarketEnum } from '@model/enum/market.enum';
import { StatusOmsService } from '@services/webFeeder/sckt-status-oms.service';
import { StatusConnection } from '@model/status-connection';

@Component({
  selector: 'app-custody-consult',
  templateUrl: './custody-consult.component.html',
  animations: [
    trigger('valueAnimation', [
      transition('void => *', [
        style({ transform: 'translateX(0) scale(0)' }),
        animate('0.2s ease-in')
      ])
    ]),
    trigger('valueListAnimation', [
      transition('* => void', [
        animate('0.3s ease-in-out', style({ transform: 'translateX(-100%)' }))
      ]),
      transition('void => *', [
        style({
          transform: 'translateX(-100%)'
        }),
        animate('0.3s 300ms ease-in-out')
      ])
    ]),
    trigger('valueLDetailAnimation', [
      transition('* => void', [
        animate('0.3s ease-in-out', style({ transform: 'translateX(100%)' }))
      ]),
      transition('void => *', [
        style({
          transform: 'translateX(100%)'
        }),
        animate('0.3s 300ms ease-in-out')
      ])
    ])
  ]
})

export class CustodyConsultComponent implements OnInit, OnDestroy {

  public market = [
    { cod: 0, descFilter: 'Todos', webfeeder: 'Todos' },
    { cod: 1, descFilter: 'Bovespa', webfeeder: 'Bovespa' },
    { cod: 2, descFilter: 'BM&F', webfeeder: 'BM&F' }
  ];

  public title = 'CONSULTA DE CUSTÓDIA';
  public loading = false;
  public selected = 0;
  public selectedVector = 0;
  public tabBMF = 'BM&F';
  public tabBov = 'Bovespa';
  public idDetails = -1;
  public emptyList = false;
  public lastPositionLeftBovespa = 0;
  public lastPositionLeftBMF = 0;
  public lastPositionTopBovespa = -58;
  public lastPositionTopBMF = -58;
  public positionRight = 10;
  public showPositionBMF = false;
  public showPositionBovespa = false;
  public quoteFilter = null;
  public positionDayList = [];
  public resultListBmf = [];
  public resultListBov = [];
  public resultListPositionBmf = [];
  public resultListPositionBov = [];
  public listBmf: Array<BmfModel> = [];
  public listBov: Array<BovModel> = [];
  public scrollTop: number;
  public whiteLabel = this._configService.getWhiteLabel();
  private subscriptionPositionBOV: Subscription;
  private subscriptionPositionBMF: Subscription;
  public account: string;
  public intervalAccount: any;
  private subscriptionStatusOms: Subscription;
  public statusOmsOnline: boolean;
  public msgStatusOmsOffile = Constants.MSG_STATUS_OFFLINE_OMS;

  @ViewChild('index') index: ElementRef;
  @ViewChild('index2') index2: ElementRef;
  @ViewChild('body') body: ElementRef;
  @ViewChild('position') position: ElementRef;
  @ViewChild('positionHeader') positionHeader: ElementRef;
  @ViewChild('ghostHeader') ghostHeader: ElementRef;
  @ViewChild('ghostCol') ghostCol: ElementRef;
  @ViewChild('ghostColBmf') ghostColBmf: ElementRef;
  @ViewChild('ghostIndex') ghostIndex: ElementRef;
  @ViewChild('filter') filterEl: ElementRef;
  @ViewChild('bovespa') tabBovespa: ElementRef;

  @HostListener('mousemove')
  onMouseMove() {
    this._autoLogoutService.resetTime();
  }

  @HostListener('click')
  onClick() {
    this._autoLogoutService.resetTime();
  }

  constructor(
    public dialogRef: MatDialogRef<CustodyConsultComponent>,
    private _renderer: Renderer,
    private _authService: AuthService,
    private _configService: ConfigService,
    private _router: Router,
    private _autoLogoutService: AutoLogoutService,
    private _positionService: PositionService,
    private _statusOmsService: StatusOmsService
  ) {
    this.listBmf = [];
    this.listBov = [];
  }

  ngOnInit() {
    if (!this._authService.isAuthenticated) {
      this.dialogRef.close();
      this._configService.deleteCookie();
      if (this._configService.isLoginTypeWSO2() && this._configService.isUserAssessorLogged()) {
        window.location.href = this._configService.REDIRECT_LOGOUT + '/assessor/login';
      } else if (this._configService.isLoginTypeWSO2() && !this._configService.isUserAssessorLogged()) {
        window.location.href = this._configService.REDIRECT_LOGOUT + '/client/login';
      } else {
        this._router.navigate(['/login']);
      }
    } else {
      this.subscriptionStatusOms = this._statusOmsService.messagesStatusOms
        .subscribe(statusOms => {
          if (statusOms === StatusConnection.ERROR) {
            this.loading = false;
            this.statusOmsOnline = false;
          } else {
            this.statusOmsOnline = true;
            const intervalAccount = setInterval(() => {
              this.account = this._configService.getBrokerAccountSelected();
              if (this.account) {
                clearInterval(intervalAccount);
                if (this.selected === 0) {
                  this._positionService.subscribePosition(this.account, MarketEnum.BOVESPA);
                  this.changeBovespa();
                } else {
                  this._positionService.subscribePosition(this.account, MarketEnum.BMF);
                  this.changeBMF();
                }
              }
            }, 1000);
          }
        });
    }
  }

  ngOnDestroy() {
    if (this._positionService && this.account) {
      this._positionService.unsubscribePosition(this.account, MarketEnum.BMF);
      this._positionService.unsubscribePosition(this.account, MarketEnum.BOVESPA);
      if (this.subscriptionPositionBOV) {
        this.subscriptionPositionBOV.unsubscribe();
      }
      if (this.subscriptionPositionBMF) {
        this.subscriptionPositionBMF.unsubscribe();
      }
    }
    if (this.subscriptionStatusOms) {
      this.subscriptionStatusOms.unsubscribe();
    }
    if (this.intervalAccount) {
      clearInterval(this.intervalAccount);
    }
  }

  changeBovespa(tab?: number, quoteFilter?: string) {
    if (tab === 0 && this.selected !== tab && this.subscriptionPositionBMF) {
      this.loading = false;
      this.showPositionBovespa = false;
      this.showPositionBMF = false;
      this._positionService.unsubscribePosition(this.account, MarketEnum.BMF);
      this.selected = 0;
      this.listBmf = [];
      setTimeout(() => {
        this._positionService.subscribePosition(this.account, MarketEnum.BOVESPA);
      }, 0);
    }
    if (!quoteFilter) {
      this.quoteFilter = null;
    }
    this.subscriptionPositionBOV = this._positionService.messagesPosition
      .subscribe(msgBov => {
        if (msgBov) {
          if (msgBov && msgBov.length > 0) {
            for (let i = 0; i < msgBov.length; i++) {
              if (msgBov[i].marketCode === 'XBSP') {
                const modelBovAux: BovModel = new BovModel();
                modelBovAux.averagePrice = msgBov[i].averagePrice;
                modelBovAux.currentQuantity = msgBov[i].currentQuantity.replace('.', '').replace(',', '.');
                modelBovAux.executedBuy = msgBov[i].executedBuy.replace('.', '').replace(',', '.');
                modelBovAux.executedSell = msgBov[i].executedSell.replace('.', '').replace(',', '.');
                if (modelBovAux.executedBuy === '0' && modelBovAux.executedSell === '0') {
                  this.positionDayList.push('-');
                } else {
                  this.positionDayList.push(msgBov[i].netDay);
                }
                modelBovAux.initQuantity = msgBov[i].initQuantity.replace('.', '').replace(',', '.');
                modelBovAux.lastTradeDate = msgBov[i].lastTradeDate ? msgBov[i].lastTradeDate : '-';
                modelBovAux.lastTrade = msgBov[i].lastTrade ? msgBov[i].lastTrade : '-';
                modelBovAux.netDay = msgBov[i].netDay ? msgBov[i].netDay : '-';
                modelBovAux.avgValueDaytradeToday = msgBov[i].avgValueDaytradeToday ? msgBov[i].avgValueDaytradeToday : '-';
                modelBovAux.openProfitLoss = msgBov[i].openProfitLoss ? msgBov[i].openProfitLoss : '-';
                modelBovAux.openProfitLossColor = Number(modelBovAux.openProfitLoss.replace('.', '').replace(',', '.'));
                modelBovAux.closedProfitLoss = msgBov[i].closedProfitLoss ? msgBov[i].closedProfitLoss : '-';
                modelBovAux.closedProfitLossColor = Number(modelBovAux.closedProfitLoss.replace('.', '').replace(',', '.'));
                modelBovAux.openBuy = msgBov[i].openBuy;
                modelBovAux.openSell = msgBov[i].openSell;
                modelBovAux.profitLoss = msgBov[i].profitLoss;
                modelBovAux.profitLossPercent = msgBov[i].profitLossPercent;
                modelBovAux.quantityBlocked = msgBov[i].quantityBlocked.replace('.', '').replace(',', '.');
                modelBovAux.quantityD1 = msgBov[i].quantityD1.replace('.', '').replace(',', '.');
                modelBovAux.quantityD2 = msgBov[i].quantityD2.replace('.', '').replace(',', '.');
                modelBovAux.quantityD3 = msgBov[i].quantityD3.replace('.', '').replace(',', '.');
                modelBovAux.quantityReleased = msgBov[i].quantityReleased.replace('.', '').replace(',', '.');
                modelBovAux.quote = msgBov[i].quote;
                modelBovAux.valueCurrent = msgBov[i].valueCurrent;
                modelBovAux.valueInit = msgBov[i].valueInit;
                modelBovAux.quoteType = msgBov[i].quoteType;
                modelBovAux.precoMedioTomadorBTC = msgBov[i].precoMedioTomadorBTC;
                modelBovAux.precoMedioDoadorBTC = msgBov[i].precoMedioDoadorBTC;
                if ((modelBovAux.quoteType === 'BTC-T' && Number(modelBovAux.currentQuantity) !== 0) ||
                  (modelBovAux.quoteType === 'BTC-D' && Number(modelBovAux.currentQuantity) !== 0) ||
                  (modelBovAux.quoteType === 'Cust.' && Number(modelBovAux.currentQuantity) !== 0 &&
                    Number(modelBovAux.initQuantity) !== 0 || Number(modelBovAux.quantityBlocked) !== 0 ||
                    Number(modelBovAux.quantityD1) !== 0 || Number(modelBovAux.quantityD2) !== 0 ||
                    Number(modelBovAux.quantityD3) !== 0 || Number(modelBovAux.quantityReleased))) {
                  if (this.listBov.length > 0) {
                    const filterQuoteTemp = this.listBov.filter(element => element.quote === modelBovAux.quote);
                    if (filterQuoteTemp.length) {
                      this.listBov.forEach((element, index) => {
                        if (element.quote === modelBovAux.quote) {
                          this.listBov[index] = modelBovAux;
                        }
                      });
                    } else {
                      this.listBov.push(modelBovAux);
                    }
                  } else {
                    this.listBov.push(modelBovAux);
                  }
                }
              }
            }
          }
          AppUtils.orderObjectAsc(this.listBov, 'quote');
          setTimeout(() => {
            if (this.listBov.length === 0) {
              this.emptyList = true;
            } else {
              this.emptyList = false;
            }
            this.loading = false;
          }, 1000);
        }
      });
  }

  changeBMF(tab?: number, quoteFilter?: string) {
    if (tab === 1 && this.selected !== tab && this.subscriptionPositionBOV) {
      this.loading = false;
      this.showPositionBovespa = false;
      this.showPositionBMF = false;
      this._positionService.unsubscribePosition(this.account, MarketEnum.BOVESPA);
      this.selected = 1;
      this.listBov = [];
      this.resultListBov = [];
      this.positionDayList = [];
      setTimeout(() => {
        this._positionService.subscribePosition(this.account, MarketEnum.BMF);
      }, 0);
    }
    if (!quoteFilter) {
      this.quoteFilter = null;
    }
    this.subscriptionPositionBMF = this._positionService.messagesPosition
      .subscribe(msgBmf => {
        if (msgBmf) {
          if (msgBmf && msgBmf.length > 0) {
            for (let i = 0; i < msgBmf.length; i++) {
              if (msgBmf[i].marketCode === 'XBMF') {
                const modelBmf: BmfModel = new BmfModel();
                modelBmf.initQuantity = msgBmf[i].initQuantity ? msgBmf[i].initQuantity : '-';
                modelBmf.quote = msgBmf[i].quote ? msgBmf[i].quote : '-';
                modelBmf.position = msgBmf[i].netPosition ? msgBmf[i].netPosition : '-';
                modelBmf.executedBuy = msgBmf[i].executedBuy ? msgBmf[i].executedBuy : '-';
                modelBmf.executedSell = msgBmf[i].executedSell ? msgBmf[i].executedSell : '-';
                modelBmf.valueCurrent = msgBmf[i].valueCurrent ? (Number(msgBmf[i].valueCurrent).toFixed(2)) : '-';
                modelBmf.netDay = msgBmf[i].netDay ? msgBmf[i].netDay : '-';
                modelBmf.averagePricePos = msgBmf[i].averagePricePos ? (Number(msgBmf[i].averagePricePos).toFixed(2)) : '-';
                modelBmf.openProfitLoss = msgBmf[i].openProfitLoss ? (Number(msgBmf[i].openProfitLoss).toFixed(2)) : '-';
                modelBmf.closedProfitLoss = msgBmf[i].closedProfitLoss ? (Number(msgBmf[i].closedProfitLoss).toFixed(2)) : '-';
                modelBmf.currentPrice = msgBmf[i].lastTrade ? (Number(msgBmf[i].lastTrade).toFixed(3)) : '-';
                modelBmf.previousAdjustment = msgBmf[i].valueAdjustedAmount ? (Number(msgBmf[i].valueAdjustedAmount).toFixed(2)) : '-';
                modelBmf.positionAdjustment = msgBmf[i].bmfPositionAdjust ? msgBmf[i].bmfPositionAdjust : '-';
                modelBmf.precoAjuste = msgBmf[i].precoAjuste ? msgBmf[i].precoAjuste : '-';
                if (modelBmf.executedBuy === '0' && modelBmf.executedSell === '0') {
                  this.positionDayList.push('-');
                } else {
                  this.positionDayList.push(msgBmf[i].position);
                }
                modelBmf.lastTradeDate = msgBmf[i].lastTradeDate;
                if (this.listBmf.length > 0) {
                  const filterQuoteTemp = this.listBmf.filter(element => element.quote === modelBmf.quote);
                  if (filterQuoteTemp.length) {
                    this.listBmf.forEach((element, index) => {
                      if (element.quote === modelBmf.quote) {
                        this.listBmf[index] = modelBmf;
                      }
                    });
                  } else {
                    this.listBmf.push(modelBmf);
                  }
                } else {
                  this.listBmf.push(modelBmf);
                }
              }
            }
          }
          AppUtils.orderObjectAsc(this.listBmf, 'quote');

          setTimeout(() => {
            if (this.listBmf.length === 0) {
              this.emptyList = true;
            } else {
              this.emptyList = false;
            }
            this.loading = false;
          }, 1000);
        }
      });
  }

  close() {
    this.dialogRef.close();
  }

  mouseLeave(i) {
    if (i === this.idDetails) {
      document.getElementById('tr' + i).classList.remove('hover-up');
      document.getElementById('trd' + i).classList.remove('hover-down');
    }
  }
  mouseEnter(i) {
    if (i === this.idDetails) {
      document.getElementById('tr' + i).classList.add('hover-up');
      document.getElementById('trd' + i).classList.add('hover-down');
    }
  }

  onScroll() {
    if (this.scrollTop === undefined || this.scrollTop === null) {
      this.scrollTop = this.body.nativeElement.scrollTop;
    }
    const filterHeight = this.filterEl.nativeElement.offsetHeight;
    // TODO Configurações de scroll para a primeira aba (BM&F)
    if (this.selected === 1) {
      // TODO Faz com que a coluna fixa siga o scroll do container proporcionalmente (Vertical)
      this.lastPositionTopBMF = this.body.nativeElement.scrollTop;
      if (this.body.nativeElement.scrollTop !== this.lastPositionTopBMF) {
        const aux = (this.body.nativeElement.scrollTop * this.index.nativeElement.height) / this.body.nativeElement.height;
        this._renderer.setElementStyle(this.position.nativeElement, 'top', aux + 'px');
      }
      // TODO Faz com que o índice da tabela fique fixo ao atingir scroll igual a 166
      if (this.body.nativeElement.scrollTop >= 166) {
        if (this.scrollTop !== this.body.nativeElement.scrollTop) {
          this._renderer.setElementStyle(this.index.nativeElement, 'position', 'fixed');
          this._renderer.setElementStyle(this.index.nativeElement, 'width', 'calc(100% - 209px)');
          this._renderer.setElementStyle(this.index.nativeElement, 'min-width', 'unset');
          this._renderer.setElementStyle(this.index.nativeElement, 'overflow-x', 'scroll');
          this._renderer.setElementStyle(this.index.nativeElement, 'z-index', '3');
          this._renderer.setElementStyle(this.index.nativeElement, 'margin-top', '0');
          this._renderer.setElementStyle(this.index.nativeElement, 'top', '138px');
          this._renderer.setElementStyle(this.ghostIndex.nativeElement, 'display', 'flex');
          this.index.nativeElement.scrollLeft = this.body.nativeElement.scrollLeft;
          if (this.scrollTop > this.body.nativeElement.scrollTop) {
            this._renderer.setElementStyle(this.filterEl.nativeElement, 'z-index', '4');
            this._renderer.setElementStyle(this.filterEl.nativeElement, 'top', '138px');
            this._renderer.setElementStyle(this.index.nativeElement, 'top', 138 + filterHeight + 'px');
            this._renderer.setElementStyle(this.positionHeader.nativeElement, 'top', 130 + filterHeight + 'px');
          } else {
            this._renderer.setElementStyle(this.filterEl.nativeElement, 'z-index', '0');
            this._renderer.setElementStyle(this.filterEl.nativeElement, 'top', '200px');
            this._renderer.setElementStyle(this.positionHeader.nativeElement, 'top', '130px');
          }
          // TODO Faz a Header da coluna fixa ser fixado no topo quando scroll atingir o topo
          this._renderer.setElementStyle(this.positionHeader.nativeElement, 'position', 'fixed');
          this._renderer.setElementStyle(this.positionHeader.nativeElement, 'right', '41px');
          this._renderer.setElementStyle(this.positionHeader.nativeElement, 'width', '184px');
          this._renderer.setElementStyle(this.positionHeader.nativeElement, 'z-index', '5');
          this._renderer.setElementStyle(this.ghostHeader.nativeElement, 'display', 'flex');
        }
        // TODO Faz com que o índice da tabela acompanhe o scroll horizontal do container
        if (this.body.nativeElement.scrollLeft !== this.lastPositionLeftBMF) {
          this.index.nativeElement.scrollLeft = this.body.nativeElement.scrollLeft;
          this.lastPositionLeftBMF = this.body.nativeElement.scrollLeft;
        }
        // TODO Faz com que o índice da tabela volte ao normal quando scroll menor que 166
      } else {
        if (this.body.nativeElement.scrollTop <= 166 - filterHeight - 50) {
          this._renderer.setElementStyle(this.index.nativeElement, 'position', 'relative');
          this._renderer.setElementStyle(this.index.nativeElement, 'width', 'unset');
          this._renderer.setElementStyle(this.index.nativeElement, 'overflow-x', 'unset');
          this._renderer.setElementStyle(this.index.nativeElement, 'z-index', '3');
          this._renderer.setElementStyle(this.index.nativeElement, 'margin-top', '0');
          this._renderer.setElementStyle(this.index.nativeElement, 'top', '0px');
          this._renderer.setElementStyle(this.ghostIndex.nativeElement, 'display', 'none');
          this._renderer.setElementStyle(this.filterEl.nativeElement, 'z-index', '0');
          this._renderer.setElementStyle(this.filterEl.nativeElement, 'top', '200px');
          // TODO Atualiza a variável de posicionamento Left
          this.lastPositionLeftBMF = this.body.nativeElement.scrollLeft;
        }
      }
      // TODO Configurações de scroll para a segunda aba (Bovespa)
    } else {
      // TODO Faz com que a coluna fixa siga o scroll do container proporcionalmente (Vertical)
      this.lastPositionTopBovespa = this.body.nativeElement.scrollTop;
      if (this.body.nativeElement.scrollTop !== this.lastPositionTopBovespa) {
        const aux = (this.body.nativeElement.scrollTop * this.index2.nativeElement.height) / this.body.nativeElement.height;
        this._renderer.setElementStyle(this.position.nativeElement, 'top', aux + 'px');
      }
      // TODO Faz com que o índice da tabela fique fixo ao atingir scroll igual a 166
      if (this.body.nativeElement.scrollTop >= 166) {
        if (this.scrollTop !== this.body.nativeElement.scrollTop) {
          this._renderer.setElementStyle(this.index2.nativeElement, 'position', 'fixed');
          this._renderer.setElementStyle(this.index2.nativeElement, 'width', 'calc(100% - 209px)');
          this._renderer.setElementStyle(this.index2.nativeElement, 'min-width', 'unset');
          this._renderer.setElementStyle(this.index2.nativeElement, 'overflow-x', 'scroll');
          this._renderer.setElementStyle(this.index2.nativeElement, 'z-index', '3');
          this._renderer.setElementStyle(this.index2.nativeElement, 'margin-top', '0');
          this._renderer.setElementStyle(this.index2.nativeElement, 'top', '138px');
          this._renderer.setElementStyle(this.ghostIndex.nativeElement, 'display', 'flex');
          this.index2.nativeElement.scrollLeft = this.body.nativeElement.scrollLeft;
          if (this.scrollTop > this.body.nativeElement.scrollTop) {
            this._renderer.setElementStyle(this.filterEl.nativeElement, 'z-index', '4');
            this._renderer.setElementStyle(this.filterEl.nativeElement, 'top', '138px');
            this._renderer.setElementStyle(this.index2.nativeElement, 'top', 138 + filterHeight + 'px');
            this._renderer.setElementStyle(this.positionHeader.nativeElement, 'top', 130 + filterHeight + 'px');
          } else {
            this._renderer.setElementStyle(this.filterEl.nativeElement, 'z-index', '0');
            this._renderer.setElementStyle(this.filterEl.nativeElement, 'top', '200px');
            this._renderer.setElementStyle(this.positionHeader.nativeElement, 'top', '130px');
          }
          // TODO Faz a Header da coluna fixa ser fixado no topo quando scroll atingir o topo
          this._renderer.setElementStyle(this.positionHeader.nativeElement, 'position', 'fixed');
          this._renderer.setElementStyle(this.positionHeader.nativeElement, 'right', '41px');
          this._renderer.setElementStyle(this.positionHeader.nativeElement, 'width', '184px');
          this._renderer.setElementStyle(this.positionHeader.nativeElement, 'z-index', '5');
          this._renderer.setElementStyle(this.ghostHeader.nativeElement, 'display', 'flex');
        }
        // TODO Faz com que o índice da tabela acompanhe o scroll horizontal do container
        if (this.body.nativeElement.scrollLeft !== this.lastPositionLeftBovespa) {
          this.index2.nativeElement.scrollLeft = this.body.nativeElement.scrollLeft;
          this.lastPositionLeftBovespa = this.body.nativeElement.scrollLeft;
        }
        // TODO Faz com que o índice da tabela volte ao normal quando scroll menor que 166
      } else {
        if (this.body.nativeElement.scrollTop <= 166 - filterHeight - 50) {
          this._renderer.setElementStyle(this.index2.nativeElement, 'position', 'relative');
          this._renderer.setElementStyle(this.index2.nativeElement, 'width', 'unset');
          this._renderer.setElementStyle(this.index2.nativeElement, 'overflow-x', 'unset');
          this._renderer.setElementStyle(this.index2.nativeElement, 'z-index', '3');
          this._renderer.setElementStyle(this.index2.nativeElement, 'margin-top', '0');
          this._renderer.setElementStyle(this.index2.nativeElement, 'top', '0px');
          this._renderer.setElementStyle(this.ghostIndex.nativeElement, 'display', 'none');
          this._renderer.setElementStyle(this.filterEl.nativeElement, 'z-index', '0');
          this._renderer.setElementStyle(this.filterEl.nativeElement, 'top', '200px');
          // TODO Atualiza a variável de posicionamento Left
          this.lastPositionLeftBovespa = this.body.nativeElement.scrollLeft;
        }
      }
    }
    this.scrollTop = this.body.nativeElement.scrollTop;
  }
}
