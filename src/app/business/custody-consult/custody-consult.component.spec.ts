import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatIconModule, MatAutocompleteModule, MatCheckboxModule, MatProgressSpinnerModule, MatFormFieldModule, MatCardModule, MatDialogRef, MatDialogModule, MatInputModule } from '@angular/material';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { CustodyConsultComponent } from '@business/custody-consult/custody-consult.component';
import { AutoCompleteComponent } from '@component/auto-complete/auto-complete.component';
import { OrdersHistoryPaginatorComponent } from '@business/orders-history/orders-history-paginator/orders-history-paginator.component';
import { SpinnerComponent } from '@component/spinner/spinner.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AutoSizeInputDirective } from '@shared/directives/autosize-input.directive';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpModule } from '@angular/http';
import { ConfigService } from '@services/webFeeder/config.service';
import { ConfigServiceMock, CustomSnackbarServiceMock } from '@services/_test-mock-services/_mock-services';
import { HttpClientModule } from '@angular/common/http';
import { NotificationsService, SimpleNotificationsModule } from 'angular2-notifications';
import { CustomSnackbarService } from '@services/custom-snackbar-service.service';
import { DatePipe } from '@angular/common';

describe('CustodyConsultComponent', () => {
  let component: CustodyConsultComponent;
  let fixture: ComponentFixture<CustodyConsultComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        CustodyConsultComponent,
        AutoCompleteComponent,
        OrdersHistoryPaginatorComponent,
        SpinnerComponent,
        AutoSizeInputDirective
      ],
      imports: [
        MatIconModule,
        MatAutocompleteModule,
        MatCheckboxModule,
        MatProgressSpinnerModule,
        MatCheckboxModule,
        FormsModule,
        MatFormFieldModule,
        ReactiveFormsModule,
        MatCardModule,
        RouterTestingModule,
        HttpModule,
        HttpClientModule,
        SimpleNotificationsModule.forRoot(),
        MatDialogModule,
        MatInputModule, 
        BrowserAnimationsModule
      ],
      providers: [
        DatePipe,
        NotificationsService,
        { provide: MatDialogRef, useValue: {} },
        { provide: ConfigService, useClass: ConfigServiceMock },
        { provide: CustomSnackbarService, useClass: CustomSnackbarServiceMock },
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CustodyConsultComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
