import { ConfigService } from '@services/webFeeder/config.service';
import { Component, OnInit } from '@angular/core';
import { MatDialogRef, MatDialog } from '@angular/material';
import { NotificationsService } from 'angular2-notifications';
import { Constants } from '@utils/constants';
import { AuthenticationService } from '@services/authentication.service';
import { Signature } from '@model/signature.model';
import { ModalPasswordComponent } from '@component/modal-password/modal-password.component';

@Component({
  selector: 'app-update-signature',
  templateUrl: './update-signature.component.html'
})
export class UpdateSignatureComponent implements OnInit {

  public icon = 'visibility_off';
  public iconNewSignature = 'visibility_off';
  public iconConfirmSignature = 'visibility_off';
  public model: Signature = new Signature();
  public errorMsg01: string = Constants.MSG01_SIGNATURE_ERROR;
  public dialog: MatDialog;
  public validSignature = true;
  public loadingReset: boolean;
  public loadingForgot: boolean;

  public showSignature = false;
  public showNewSignature = false;
  public showConfirmSignature = false;

  public isForgot = false;

  // forgot
  public titleUpdate = 'Nova assinatura eletrônica';
  public subTitleUpdate = 'Para definir uma nova assinatura eletrônica, será necessário informar a sua assinatura eletrônica atual.';
  public showConfirmForgot = false;

  // reset
  public titleForgot = 'Recuperar assinatura eletrônica';
  public subTitleSignature = 'Uma mensagem com uma nova assinatura eletrônica vai ser enviada para e-mail cadastrado.';
  public showConfirmReset = false;
  public email: string;

  // error
  public showError = false;
  public titleError = Constants.MSG01_FORGOT_SIGNATURE_TITLE_ERROR;
  public contentError = Constants.MSG01_FORGOT_SIGNATURE_CONTENT_ERROR;
  public helpPhone1: string;
  public helpEmail: string;

  constructor(
    public dialogRef: MatDialogRef<UpdateSignatureComponent>,
    public _notificationService: NotificationsService,
    private _authenticationService: AuthenticationService,
    private _configService: ConfigService,
    dialog: MatDialog,
  ) {
    this.dialog = dialog;
    this.model.signature = '';
    this.model.newSignature = '';
    this.model.confirmSignature = '';
    this.showSignature = false;
    this.showNewSignature = false;
    this.showConfirmSignature = false;
    this.isForgot = false;
    this.showConfirmForgot = false;
    this.showConfirmReset = false;
    this.email = this._configService.getHbEmail();
    this.helpPhone1 = Constants.SIDEBAR_HELP_WHATSAPP;
    this.helpEmail = Constants.SIDEBAR_HELP_EMAIL;
  }

  ngOnInit() {
  }

  closeClick(): void {
    this.dialogRef.close();
  }

  toggleSignature() {
    this.showSignature = !this.showSignature;
    this.icon = this.showSignature ? 'visibility' : 'visibility_off';
  }

  toggleNewSignature() {
    this.showNewSignature = !this.showNewSignature;
    this.iconNewSignature = this.showNewSignature ? 'visibility' : 'visibility_off';
  }

  toggleConfirmSignature() {
    this.showConfirmSignature = !this.showConfirmSignature;
    this.iconConfirmSignature = this.showConfirmSignature ? 'visibility' : 'visibility_off';
  }

  validatorSignature(value) {
    if (this.model.newSignature !== this.model.confirmSignature) {
      this.validSignature = false;
    } else {
      this.validSignature = true;
    }
  }

  validatorSignature2(value) {
    if (this.model.confirmSignature) {
      if (this.model.newSignature !== this.model.confirmSignature) {
        this.validSignature = false;
      } else {
        this.validSignature = true;
      }
    }
  }

  validChar(e) {
    const isNumber = new RegExp('^[0-9]+$');
    if (!(isNumber.exec(e.key))) {
      return false;
    }
  }

  changeSignature() {
    this.loadingReset = true;
    this.loadingForgot = false;

    if (this.model.signature === this.model.newSignature) {
      this._notificationService.warn('', Constants.MSG02_SIGNATURE_ERROR);
      this.loadingReset = false;
      return;
    }

    this._authenticationService.resetSignature(this.model.signature, this.model.newSignature)
      .subscribe(res => {
        if (res && (res.success === 'true' || res.success === true)) {
          this.model = new Signature();
          this.showConfirmReset = true;
        } else {
          if (res && res.message) {
            this._notificationService.error('', res.message);
          } else {
            this._notificationService.error('', Constants.MSG_LOGIN_MSG003);
          }
          this.showError = true;
        }
        this.model = new Signature();
        this.loadingReset = false;
      }, error => {
        this._notificationService.error('', Constants.MSG_LOGIN_MSG003);
        this.showError = true;
        this.loadingReset = false;
      });
  }

  forgotSignature() {
    this.isForgot = !this.isForgot;
  }

  sendForgotSignature() {
    this.loadingForgot = true;
    this.loadingReset = false;
    if (!this.email) {
      this._notificationService.warn('', Constants.MSG04_SIGNATURE_ERROR);
      this.loadingForgot = false;
      return;
    }
    this._authenticationService.forgotSignature(this.email)
      .subscribe(res => {
        if (res && (res.success === 'true' || res.success === true)) {
          this.model = new Signature();
          this.showConfirmForgot = true;
        } else if (res && !res.error && res.data && res.data.status >= 200) {
          this.model = new Signature();
          this.showConfirmForgot = true;
        } else {
          if (res && res.message) {
            this._notificationService.error('', res.message);
          } else {
            this._notificationService.error('', Constants.MSG_LOGIN_MSG003);
          }
          this.showError = true;
        }
        this.loadingForgot = false;
      }, error => {
        this._notificationService.error('', Constants.MSG_LOGIN_MSG003);
        this.showError = true;
        this.loadingForgot = false;
      });
    // this.showConfirmForgot = true;
  }

  goBack() {
    if (this.showError) {
      this.showError = false;
    } else if (!this.showConfirmReset && !this.showConfirmForgot) {
      this.isForgot = !this.isForgot;
    } else {
      this.showConfirmReset = false;
      this.showConfirmForgot = false;
    }
    this.loadingReset = false;
    this.loadingForgot = false;
  }

  openWhatsappWindow() {
    window.open(Constants.SIDEBAR_HELP_WHATSAPP_LINK);
  }

}
