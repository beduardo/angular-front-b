import { animate, group, keyframes, style, transition, trigger } from '@angular/animations';
import { Component, ElementRef, HostListener, OnDestroy, OnInit, Renderer, ViewChild, ViewContainerRef, ChangeDetectorRef } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material';
import { AutoLogoutService } from '@auth/auto-logout-service.service';
import { OrdersHistoryPresenter } from '@business/orders-history/orders-history.presenter';
import { NormalModalComponent } from '@component/normal-modal/normal-modal.component';
import { HistoryFilterOrder } from '@model/webFeeder/history-filter-order';
import { HistoryOrder } from '@model/webFeeder/history-order.model';
import { CancelOrder } from '@model/webFeeder/message-cancel-order';
import { MessageQuoteQuery } from '@model/webFeeder/message-quote-query.model';
import { CustomSnackbarService } from '@services/custom-snackbar-service.service';
import { ConfigService } from '@services/webFeeder/config.service';
import { DailyOrderService } from '@services/webFeeder/sckt-daily-order.service';
import { Constants } from '@utils/constants';
import { NotificationsService } from 'angular2-notifications';
import { NgxDrpOptions, PresetItem, Range } from 'ngx-mat-daterange-picker';
import { Subject, Subscription } from 'rxjs';
import { NgZone } from '@angular/core';
import { AuthServer } from '@services/auth-server.service';
import { DatePipe } from '@angular/common';
import { environment } from 'environments/environment';
import { SideEnum } from '@model/enum/side.enum';
import { AppUtils } from '@utils/app.utils';
import { StatusOmsService } from '@services/webFeeder/sckt-status-oms.service';
import { StatusConnection } from '@model/status-connection';

@Component({
  selector: 'app-orders-history',
  templateUrl: './orders-history.component.html',
  animations: [
    trigger('animateDetails', [
      transition('void => *', [
        group([
          animate(
            '.5s ease-in-out',
            keyframes([
              style({
                opacity: 0,
                transform: 'translateY(-200%)',
                height: 0,
                offset: 0
              }),
              style({
                opacity: 0,
                transform: 'translateY(-50%)',
                height: 10,
                offset: 0.4
              }),
              style({
                opacity: 1,
                transform: 'translateY(0)',
                height: '*',
                offset: 1
              })
            ])
          )
        ])
      ])
    ]),
    trigger('animateDetailsClose', [
      transition('* => void', [
        style({ transform: 'translateY(0)' }),
        group([
          animate(
            '.5s ease',
            keyframes([
              style({
                opacity: 1,
                transform: 'translateY(0)',
                height: '*',
                offset: 0
              }),
              style({
                opacity: 0,
                transform: 'translateY(-50%)',
                height: 10,
                offset: 0.4
              }),
              style({
                opacity: 0,
                transform: 'translateY(-200%)',
                height: 0,
                offset: 1
              })
            ])
          )
        ])
      ])
    ])
  ]
})
export class OrdersHistoryComponent implements OnInit, OnDestroy {
  public statusFilter2 = [
    {
      cod: 0,
      descFilter: 'Todas',
      webfeeder: [
        { cod: '0', desc: 'Recebida', webfeeder: 'New' },
        {
          cod: '1',
          desc: 'Parcialmente Executada',
          webfeeder: 'Partially Filled'
        },
        { cod: '2', desc: 'Completamente Executada', webfeeder: 'Filled' },
        { cod: '4', desc: 'Cancelada', webfeeder: 'Canceled' },
        { cod: '5', desc: 'Editada', webfeeder: 'Replaced' },
        {
          cod: '6',
          desc: 'Cancelamento Pendente',
          webfeeder: 'Pending Cancel'
        },
        { cod: '8', desc: 'Rejeitada', webfeeder: 'Rejected' },
        {
          cod: 'A',
          desc: 'Pendente - esperando abertura do mercado para ser enviada',
          webfeeder: 'Pending New'
        },
        { cod: 'C', desc: 'Expirada', webfeeder: 'Expired' },
        { cod: 'E', desc: 'Esperando Edição', webfeeder: 'Pending Replace' }
      ]
    },
    {
      codFIlter: 1,
      descFilter: 'Abertas',
      webfeeder: [
        { cod: '0', desc: 'Recebida', webfeeder: 'New' },
        { cod: '1', desc: 'Parcialmente Executada', webfeeder: 'Partially Filled' },
        { cod: '5', desc: 'Editada', webfeeder: 'Replaced' },
        { cod: '6', desc: 'Cancelamento Pendente', webfeeder: 'Pending Cancel' },
        { cod: 'A', desc: 'Pendente', webfeeder: 'Pending New' },
        { cod: 'E', desc: 'Edição Pendente', webfeeder: 'Pending Replace' }
      ]
    },
    {
      codFIlter: 2,
      descFilter: 'Executadas',
      webfeeder: [
        { cod: '2', desc: 'Completamente Executada', webfeeder: 'Filled' }
      ]
    },
    {
      codFIlter: 3,
      descFilter: 'Canceladas',
      webfeeder: [{ cod: '4', desc: 'Cancelada', webfeeder: 'Canceled' }]
    },
    {
      codFIlter: 4,
      descFilter: 'Rejeitadas',
      webfeeder: [{ cod: '8', desc: 'Rejeitada', webfeeder: 'Rejected' }]
    },
    {
      codFIlter: 5,
      descFilter: 'Expiradas',
      webfeeder: [{ cod: 'C', desc: 'Expirada', webfeeder: 'Expired' }]
    }
  ];

  public typeFilter = [
    {
      cod: 1,
      descFilter: 'Todos',
      webfeeder: '',
      dailyFilter: 'Todos'
    },
    {
      cod: 2,
      descFilter: 'Limitada',
      webfeeder: 'Limited',
      dailyFilter: 'Limitada'
    },
    {
      cod: 3,
      descFilter: 'Mercado com proteção',
      webfeeder: 'Market',
      dailyFilter: 'Mercado com proteção'
    },
    {
      cod: 4,
      descFilter: 'Mercado limite',
      webfeeder: 'Mercado limite',
      dailyFilter: 'Mercado limite'
    },
    { cod: 5, descFilter: 'Stop', webfeeder: 'Stop', dailyFilter: 'Stop' }
  ];

  public typeFilter2 = [
    { cod: 1, descFilter: 'Todos', webfeeder: '' },
    {
      cod: 2,
      descFilter: 'Limitada',
      webfeeder: [{ cod: 2, desc: 'Limitada' }]
    },
    {
      cod: 3,
      descFilter: 'Mercado com proteção',
      webfeeder: []
    },
    {
      cod: 4,
      descFilter: 'Mercado limite',
      webfeeder: []
    },
    {
      cod: 5,
      descFilter: 'Stop',
      webfeeder: [{ cod: 4, desc: 'Stop Limit' }]
    }
  ];

  public buySellFilter = [
    { cod: 1, descFilter: 'Todas', webfeeder: '' },
    { cod: 2, descFilter: 'Compra', webfeeder: 'buy' },
    { cod: 3, descFilter: 'Venda', webfeeder: 'sell' }
  ];

  public MarketFilter = [
    { cod: 0, descFilter: 'Todos', webfeeder: 'Todos' },
    { cod: 1, descFilter: 'Bovespa', webfeeder: 'Bovespa' },
    { cod: 2, descFilter: 'BM&F', webfeeder: 'BM&F' }
  ];
  public statusFilter = [
    { cod: 0, descFilter: 'Todas', webfeeder: null, dailyFilter: ['Todas'] },
    {
      cod: 1,
      descFilter: 'Abertas',
      webfeeder: 'New',
      dailyFilter: [
        'Aberta',
        'Nova',
        'Parcialmente Executada',
        'Editada',
        'Cancelamento Pendente',
        'Pendente',
        'Edição Pendente',
        'Suspensa'
      ]
    },
    {
      cod: 2,
      descFilter: 'Executadas',
      webfeeder: 'Filled',
      dailyFilter: ['Executada']
    },
    {
      cod: 3,
      descFilter: 'Canceladas',
      webfeeder: 'Canceled',
      dailyFilter: ['Cancelada']
    },
    {
      cod: 4,
      descFilter: 'Rejeitadas',
      webfeeder: 'Rejected',
      dailyFilter: ['Rejeitada']
    },
    {
      cod: 5,
      descFilter: 'Expirada',
      webfeeder: 'Expired',
      dailyFilter: ['Expirada']
    }
  ];
  public changingValue: Subject<boolean> = new Subject();
  public symbol = '';
  public currentFilter = '';
  public filterDirection = 0;
  public filteredAssets: MessageQuoteQuery[];
  public hiddenInvalid: boolean;
  public loading = true;
  public loading2 = false;
  public model: HistoryFilterOrder;
  private historyOrderPresenter: OrdersHistoryPresenter;
  // TODO Desmock to public account = this._configService.getHbAccount();
  public account: string;
  rowData: any[] = [];
  public rowAllData: any[];
  public defaultColDef;
  public showDailyOrder = true;

  private rowOpenOrdersData: any[];
  private rowExecutedOrdersData: any[];
  private rowCancelledRejectedOrdersData: any[];
  private rowOpenExecutedOrdersData: any[];
  public listOrders = ['1', '2', '3', '4'];
  public listOrdersFilter = [];
  public idDetails = -1;
  public idEdit = -1;
  public stateDetail = false;
  public stateDetailClose = true;
  public list;
  public historyList: HistoryOrder[];
  public selected = 1;
  public subscriptionDailyOrder: Subscription;
  public listDaily = [];
  public auxListDaily = [];
  public noRecords;
  public selectedVector = 0;
  public pageVector = [0, 1, 2, 3, 4];
  public middleVector = false;
  public numberOfLines = 20;
  public currentList;
  public filterPrice = 'A';
  public lastOffset: any;

  public auxHistoryList = [];

  public dataIni = new Date();
  public dataIniAux = new Date();
  public dataFim = new Date();
  public dataFimAux = new Date();
  public scrollingSubscription: any;

  public filteredList = [];
  public filteredHistory = [];

  public errors;

  range: Range = { fromDate: new Date(), toDate: new Date() };
  options: NgxDrpOptions;
  presets: Array<PresetItem> = [];

  public dailyStatusFilter;
  public dailyTypeFilter;
  public dailyDirectionFilter;
  public dailyMarketFilter;
  public dailySymbolFilter;
  public dailyStatusGroupFilter;
  public currentDailyList = [];
  public auxCurrentDailyList = [];
  public dialog: MatDialog;
  private gridApi;
  private gridColumnApi;

  public scrollTop: number;

  public notificationOptions = {
    preventDuplicates: true,
    timeOut: 2000,
  };
  public whiteLabel = this._configService.getWhiteLabel();
  public openStatus = Constants.ORDER_HISTORY_STATUS[1];

  private subscriptionStatusOms: Subscription;
  public statusOmsOnline: boolean;
  public msgStatusOmsOffile = Constants.MSG_STATUS_OFFLINE_OMS;

  filterDateRange: number;

  columnDefs = [
    { headerName: 'STATUS', field: 'state' },
    { headerName: 'ATIVO', field: 'quote' },
    { headerName: 'TIPO', field: 'type' },
    { headerName: 'DIREÇÃO', field: 'side' },
    { headerName: 'QTD.', field: 'quantitySupplied' },
    { headerName: 'EXEC.', field: 'quantityExecuted' },
    { headerName: 'RESTANTE', field: 'quantityRemaining' },
    {
      headerName: 'PREÇO',
      field: 'price',
      valueFormatter: this.priceFormatter
    },
    {
      headerName: 'P. MÉDIO',
      field: 'priceAverage',
      valueFormatter: this.priceFormatter
    },
    { headerName: 'MERCADO', field: 'mercado' },
    { headerName: 'DATA/HORA', field: 'transactTime' }
  ];

  @ViewChild('historyOrdens', { read: ViewContainerRef })
  parent: ViewContainerRef;
  @ViewChild('index')
  index: ElementRef;
  @ViewChild('header')
  header: ElementRef;
  @ViewChild('body')
  body: ElementRef;
  @ViewChild('filter')
  filterEl: ElementRef;
  @ViewChild('blankSpace')
  blankSpaceEl: ElementRef;

  @HostListener('mousemove')
  onMouseMove() {
    this._autoLogoutService.resetTime();
  }

  @HostListener('click')
  onClick() {
    this._autoLogoutService.resetTime();
  }


  // @HostListener('document:keydown', ['$event'])
  // onkeydown(event) {
  //   if (event.key == 'Escape') {
  //     if ((document.getElementsByTagName('ngb-modal-window').length == 0)) {
  //       this.closeDialog();
  //     }
  //   }
  // }

  constructor(
    dialog: MatDialog,
    private zone: NgZone,
    private _renderer: Renderer,
    private _snackBar: CustomSnackbarService,
    private _configService: ConfigService,
    private _dailyOrderService: DailyOrderService,
    public dialogRef: MatDialogRef<OrdersHistoryComponent>,
    private _notificationService: NotificationsService,
    private _autoLogoutService: AutoLogoutService,
    private _authServer: AuthServer,
    private datePipe: DatePipe,
    private _statusOmsService: StatusOmsService,
    private cdRef: ChangeDetectorRef
  ) {
    this.account = this._configService.getBrokerAccountSelected();
    this.filterDateRange = this._configService.getOrderHistoryFilterRange();
    this.dialog = dialog;
    this.model = new HistoryFilterOrder();
    this.historyOrderPresenter = new OrdersHistoryPresenter();
    this.model.typeDateFilter = 'today';
    this.setDefaultValues();

    this.subscriptionStatusOms = this._statusOmsService.messagesStatusOms
      .subscribe(statusOms => {
        if (statusOms === StatusConnection.ERROR) {
          this.loading = false;
          this.statusOmsOnline = false;
        } else {
          this.statusOmsOnline = true;
        }
      });
  }

  ngOnInit() {
    //TODO Remover quando refatorar o FT para parametrização dos tipos de mercado.
    if (environment.WHITELABEL === 2) {
      this.MarketFilter.shift();
      this.MarketFilter.pop();
    }
  }

  ngOnDestroy() {
    if (this.subscriptionDailyOrder) {
      this._dailyOrderService.unsubscribeDailyOrder(this.account);
      this.subscriptionDailyOrder.unsubscribe();
    }
    if (this.subscriptionStatusOms) {
      this.subscriptionStatusOms.unsubscribe();
    }
    this.changingValue.unsubscribe();
  }

  initializeOrderHistory() {
    this.loading2 = true;
    this.dataIni.setDate(this.dataIni.getDate() - 1);
    this.dataIniAux = this.dataIni;
    this.loading = true;
    this._dailyOrderService.subscribeDailyOrderBovespa(this.account);
    this._dailyOrderService.subscribeDailyOrderBmf(this.account);
    this.subscriptionDailyOrder = this._dailyOrderService.messagesDailyOrder
      .subscribe(res => {
        if (res && res.length > 0) {
          const presenterRes = [...this.historyOrderPresenter.transformDailyOrder(res)];
          for (let i = 0; i < presenterRes.length; i++) {
            this.currentDailyList = this.currentDailyList.filter(
              element => element.orderID != presenterRes[i].orderID
            );
            this.currentDailyList.push(presenterRes[i]);
          }
          this.currentDailyList = [...this.sortWebfeederDate(this.currentDailyList)];
          this.auxCurrentDailyList = [...this.currentDailyList];
          this.onSearchDaily();
        }
        this.loading2 = false;
        this.cdRef.detectChanges();
      });
  }

  getHistory() {
    const dataFim = new Date();
    const dataIni = new Date(this.dataIni.setDate(this.dataFim.getDate() - 3));
    this._authServer.iNegotiation.getHistoryOrder(this.account, '0', dataIni.toUTCString(), dataFim.toUTCString(),
      null, null, null, null, null, null)
      .subscribe(data => {
        this.auxHistoryList = data.listBeans;
        this.auxHistoryList = this.historyOrderPresenter.transformHistoryOrder(data);
        this.historyList = this.sortWebfeederDate(this.auxHistoryList);
        setTimeout(x => {
          this.loading = false;
        }, 2000);
      });
  }

  sortWebfeederDate(value) {
    if (value) {
      return value.sort(function (obj1, obj2) {
        let datearray = obj1.transactTime.substring(0, 10).split('/');
        let newdate = datearray[1] + '/' + datearray[0] + '/' + datearray[2];
        const dateobj1 = new Date(newdate + obj1.transactTime.substring(10, 19));
        datearray = obj2.transactTime.substring(0, 10).split('/');
        newdate = datearray[1] + '/' + datearray[0] + '/' + datearray[2];
        const dateobj2 = new Date(newdate + obj2.transactTime.substring(10, 19));
        return dateobj2.getTime() - dateobj1.getTime();
      });
    }
  }

  public postFilterType(list) {
    if (list) {
      return list.filter(x => this.model.type == 'Todos' || x.type.includes(this.model.type));
    } else {
      return [];
    }
  }

  onSearch() {
    const dataInicio = this.dataIni.toUTCString();
    const dataFim = this.dataFim.toUTCString();
    if (dataInicio !== dataFim && !AppUtils.validateDateRange(dataInicio, dataFim, this.filterDateRange)) {
      const msg = Constants.DATE_MSG005.replace('{{range}}', this.filterDateRange.toString());
      this._snackBar.createSnack(this.parent, msg, Constants.ALERT_TYPE.warn);
      return;
    }
    this.loading = true;
    const market = this.MarketFilter.find(
      p => p.descFilter == this.model.market
    ).cod.toString();
    const direction = this.buySellFilter.find(
      p => p.descFilter == this.model.direction
    ).webfeeder;
    const status = this.statusFilter.find(
      p => p.descFilter === this.model.status
    ).webfeeder;
    const statusGroup = null; // TODO add this filter
    const type = this.typeFilter
      .find(p => p.descFilter == this.model.type)
      .webfeeder.toString();
    this._authServer.iNegotiation.getHistoryOrder(this.account, market, dataInicio, dataFim,
      this.model.symbol, status, direction, null, type, statusGroup)
      .subscribe((historyOrder: any) => {
        if (!historyOrder.listBeans) {
          this.noRecords = true;
          setTimeout(x => {
            this.loading = false;
          }, 2000);
          this.historyList = [];
          this.currentList = [];
          this.pageVector = [];
        } else {
          this.historyList = this.historyOrderPresenter.transformHistoryOrder(
            historyOrder
          );
          this.historyList = this.sortWebfeederDate(this.historyList);
          this.historyList = this.postFilterType(this.historyList);
          this.noRecords = false;
          setTimeout(x => {
            this.loading = false;
          }, 2000);
        }
      });
  }

  onScroll(event) {
    if (this.scrollTop === undefined || this.scrollTop === null) {
      this.scrollTop = this.body.nativeElement.scrollTop;
    }

    if (this.selected == 1) {
      const filterHeight = this.filterEl.nativeElement.offsetHeight;
      if (this.body.nativeElement.scrollTop >= 166) {
        this._renderer.setElementStyle(this.index.nativeElement, 'position', 'fixed');
        this._renderer.setElementStyle(this.index.nativeElement, 'top', '138px');
        this._renderer.setElementStyle(this.index.nativeElement, 'width', 'calc(100% - 209px)');
        this._renderer.setElementStyle(this.index.nativeElement, 'z-index', '1');

        if (this.scrollTop > this.body.nativeElement.scrollTop) {
          this._renderer.setElementStyle(this.filterEl.nativeElement, 'position', 'fixed');
          this._renderer.setElementStyle(this.filterEl.nativeElement, 'top', '93px');
          this._renderer.setElementStyle(this.filterEl.nativeElement, 'width', 'calc(100% - 209px)');
          this._renderer.setElementStyle(this.filterEl.nativeElement, 'z-index', '1');

          this._renderer.setElementStyle(this.index.nativeElement, 'top', `${136 + filterHeight}px`);
        } else {
          this._renderer.setElementStyle(this.filterEl.nativeElement, 'position', 'fixed');
          this._renderer.setElementStyle(this.filterEl.nativeElement, 'top', '-500px');
          this._renderer.setElementStyle(this.filterEl.nativeElement, 'width', 'calc(100% - 209px)');
          this._renderer.setElementStyle(this.filterEl.nativeElement, 'z-index', '1');

          this._renderer.setElementStyle(this.index.nativeElement, 'top', '138px');
        }

        this._renderer.setElementStyle(this.blankSpaceEl.nativeElement, 'height', `${50 + filterHeight}px`);
      } else {

        if (this.body.nativeElement.scrollTop <= 166 - filterHeight - 50) {
          this._renderer.setElementStyle(this.index.nativeElement, 'position', 'unset');
          this._renderer.setElementStyle(this.index.nativeElement, 'width', 'unset');
          this._renderer.setElementStyle(this.index.nativeElement, 'z-index', 'unset');

          this._renderer.setElementStyle(this.filterEl.nativeElement, 'position', 'unset');
          this._renderer.setElementStyle(this.filterEl.nativeElement, 'width', 'unset');
          this._renderer.setElementStyle(this.filterEl.nativeElement, 'z-index', 'unset');
          this._renderer.setElementStyle(this.blankSpaceEl.nativeElement, 'height', '0px');
        }
      }
    } else {
      const filterHeight = this.filterEl.nativeElement.offsetHeight;
      if (this.body.nativeElement.scrollTop >= 208) {
        this._renderer.setElementStyle(this.index.nativeElement, 'position', 'fixed');
        this._renderer.setElementStyle(this.index.nativeElement, 'top', '138px');
        this._renderer.setElementStyle(this.index.nativeElement, 'width', 'calc(100% - 209px)');
        this._renderer.setElementStyle(this.index.nativeElement, 'z-index', '1');

        if (this.scrollTop > this.body.nativeElement.scrollTop) {
          this._renderer.setElementStyle(this.filterEl.nativeElement, 'position', 'fixed');
          this._renderer.setElementStyle(this.filterEl.nativeElement, 'top', '93px');
          this._renderer.setElementStyle(this.filterEl.nativeElement, 'width', 'calc(100% - 209px)');
          this._renderer.setElementStyle(this.filterEl.nativeElement, 'z-index', '1');

          this._renderer.setElementStyle(this.index.nativeElement, 'top', `${136 + filterHeight}px`);
        } else {
          this._renderer.setElementStyle(this.filterEl.nativeElement, 'position', 'fixed');
          this._renderer.setElementStyle(this.filterEl.nativeElement, 'top', '-500px');
          this._renderer.setElementStyle(this.filterEl.nativeElement, 'width', 'calc(100% - 209px)');
          this._renderer.setElementStyle(this.filterEl.nativeElement, 'z-index', '1');

          this._renderer.setElementStyle(this.index.nativeElement, 'top', '138px');
        }

        this._renderer.setElementStyle(this.blankSpaceEl.nativeElement, 'height', `${50 + filterHeight}px`);
      } else {

        if (this.body.nativeElement.scrollTop <= 208 - filterHeight - 50) {
          this._renderer.setElementStyle(this.index.nativeElement, 'position', 'unset');
          this._renderer.setElementStyle(this.index.nativeElement, 'width', 'unset');
          this._renderer.setElementStyle(this.index.nativeElement, 'z-index', 'unset');

          this._renderer.setElementStyle(this.filterEl.nativeElement, 'position', 'unset');
          this._renderer.setElementStyle(this.filterEl.nativeElement, 'width', 'unset');
          this._renderer.setElementStyle(this.filterEl.nativeElement, 'z-index', 'unset');
          this._renderer.setElementStyle(this.blankSpaceEl.nativeElement, 'height', '0px');
        }
      }
    }
    this.scrollTop = this.body.nativeElement.scrollTop;
  }

  private setDefaultValues() {
    this.changingValue.next(true);
    this.filterDirection = 0;
    this.dataFim = new Date();
    this.dataIni = new Date(this.dataIni.setDate(this.dataFim.getDate() - 1));
    this.symbol = '';
    this.idDetails = -1;
    this.idEdit = -1;
    this.dailySymbolFilter = this.model.symbol = '';
    this.dailyTypeFilter = this.model.type = 'Todos';
    if (environment.WHITELABEL === 2) {
      this.dailyMarketFilter = this.model.market = 'Bovespa';
    } else {
      this.dailyMarketFilter = this.model.market = 'Todos';
    }
    this.dailyStatusFilter = this.model.status = 'Todas';
    this.dailyDirectionFilter = this.model.direction = 'Todas';
  }

  onSearchDaily() {
    this.dailyStatusFilter = this.statusFilter.find(
      p => p.descFilter === this.model.status
    ).dailyFilter;
    this.dailyTypeFilter = this.typeFilter
      .find(p => p.descFilter === this.model.type)
      .dailyFilter.toString();
    this.dailyDirectionFilter = this.buySellFilter.find(
      p => p.descFilter === this.model.direction
    ).descFilter;
    this.dailyMarketFilter = this.MarketFilter.find(
      p => p.descFilter === this.model.market
    ).descFilter.toString();
    this.dailySymbolFilter = this.model.symbol;
    this.currentDailyList = [...this.filterDaily(this.auxCurrentDailyList)];
  }

  filterDaily(list) {
    const symbol = this.dailySymbolFilter;
    const market = this.dailyMarketFilter;
    const direction = this.dailyDirectionFilter;
    const status = this.dailyStatusFilter;
    const type = this.dailyTypeFilter;
    const filteredArray = list.filter(
      value => (symbol == '' || value.quote.toLowerCase() == symbol.toLowerCase()) &&
        (market == 'Todos' || value.mercado.toLowerCase() == market.toLowerCase()) &&
        (direction == 'Todas' || value.buyOrSell.toLowerCase() == direction.toLocaleLowerCase()) &&
        (status == 'Todas' || status.includes(value.state) || status == value.state) &&
        (type == 'Todos' || value.type.includes(type))
    );
    return filteredArray;
  }

  openEditOrder(i, event) {
    event.stopPropagation();
    this.idDetails = -1;
    this.idEdit = i;
    document.getElementById('tr' + i).classList.add('hover-up');
    document.getElementById('trd' + i).classList.add('hover-down');
    this.toggleAnimate();
  }

  closeEditOrder(i, event?) {
    if (event) { event.stopPropagation(); }
    this.idEdit = -1;
    document.getElementById('tr' + i).classList.remove('hover-up');
    document.getElementById('trd' + i).classList.remove('hover-down');
    this.toggleAnimateClose();
  }

  priceFormatter(params) {
    if (params.value === undefined) {
      return '0,00';
    } else {
      return 'R$ ' + params.value;
    }
  }

  onClearSearch() {
    this.setDefaultValues();
  }

  onClearDailySearch() {
    this.setDefaultValues();
    this.onSearchDaily();
  }

  alertWarn(text) {
    this._snackBar.createSnack(this.parent, text, Constants.ALERT_TYPE.warn);
  }

  timeFormatter(params) {
    if (params.value.length === 12) {
      return params.value.substring(0, params.value.length - 4);
    } else {
      return params.value;
    }
  }

  onGridReady(param) {
    this.gridApi = param.api;
    this.gridColumnApi = param.columnApi;

    this.gridApi.gridOptionsWrapper.gridOptions.getRowNodeId = function (data) {
      return data.id;
    };
  }

  loadDailyOrder() {
    if (this.model.status === 'Abertas') {
      this.rowData = this.rowOpenOrdersData;
    } else if (this.model.status === 'Executadas') {
      this.rowData = this.rowExecutedOrdersData;
    } else if (this.model.status === 'Canceladas') {
      this.rowData = this.rowCancelledRejectedOrdersData;
    } else if (this.model.status === 'Abertas + Executadas') {
      this.rowData = this.rowOpenExecutedOrdersData;
    } else {
      this.rowData = this.rowAllData;
    }

    if (this.model.type === 'Normal') {
      this.rowData = this.rowData.filter(x => x.typeName === 'Normal');
    } else if (this.model.type === 'Disparo') {
      this.rowData = this.rowData.filter(x => x.typeName === 'Disparo');
    }

    if (this.model.direction === 'buy') {
      this.rowData = this.rowData.filter(x => x.buyOrSell === 'C');
    } else if (this.model.direction === 'sell') {
      this.rowData = this.rowData.filter(x => x.buyOrSell === 'V');
    }

    if (this.model.market === 'Bovespa') {
      this.rowData = this.rowData.filter(x => x.market === 1);
    } else if (this.model.market === 'BM&F') {
      this.rowData = this.rowData.filter(x => x.market === 2);
    }

    if (
      this.model.symbol !== '' &&
      this.model.symbol !== undefined &&
      this.model.symbol !== null
    ) {
      this.rowData = this.rowData.filter(
        x => x.quote.toUpperCase() === this.model.symbol.toUpperCase()
      );
    }

    if (this.rowData !== undefined) {
    } else {
      this.rowData = [];
    }
    this.rowData = [...this.rowData];
  }

  closeDialog() {
    this.dialogRef.close();
    const elem = document.getElementById('sidebar-left');
    elem.style.zIndex = '1';
  }

  mouseLeave(i) {
    if (i === this.idDetails) {
      document.getElementById('tr' + i).classList.remove('hover-up');
      document.getElementById('trd' + i).classList.remove('hover-down');
    }
  }

  mouseEnter(i) {
    if (i === this.idDetails) {
      document.getElementById('tr' + i).classList.add('hover-up');
      document.getElementById('trd' + i).classList.add('hover-down');
    }
  }

  openDetail(i) {
    this.idEdit = -1;
    this.idDetails = i;
    document.getElementById('tr' + i).classList.add('hover-up');
    document.getElementById('trd' + i).classList.add('hover-down');
    this.toggleAnimate();
  }

  closeDetail(i) {
    this.idDetails = -1;
    document.getElementById('tr' + i).classList.remove('hover-up');
    document.getElementById('trd' + i).classList.remove('hover-down');
    this.toggleAnimateClose();
  }

  toggleAnimate() {
    this.stateDetail = !this.stateDetail;
  }

  get stateDetails() {
    return this.stateDetail ? 'show' : 'hide';
  }

  toggleAnimateClose() {
    this.stateDetailClose = !this.stateDetailClose;
  }

  get stateDetailsClose() {
    return this.stateDetailClose ? 'showClose' : 'hideClose';
  }

  changeScreen(value) {
    this.setDefaultValues();
    if (this.selected !== value) {
      this.setDefaultValues();
      this.selected = value;
      if (this.selected === 0) {
        this._dailyOrderService.unsubscribeDailyOrder(this.account);
        this.loading = false;
      } else {
        this._dailyOrderService.subscribeDailyOrderBovespa(this.account);
        this._dailyOrderService.subscribeDailyOrderBmf(this.account);
      }
    }
  }

  public changeDateIni(value) {
    this.dataIni = new Date(value);
    this.dataIniAux = new Date(value);
  }

  public changeDateFim(value) {
    this.dataFim = new Date(value);
    this.dataFimAux = new Date(value);
  }

  public sortBy(value, alternative = null) {
    if (this.currentFilter == value) {
      switch (this.filterDirection) {
        case 0:
          this.filterDirection = 1;
          this.historyList = this.doSorting(this.historyList, value, 0, alternative);
          if (value == 'stateName') {
            value = 'state';
          }
          this.currentDailyList = this.doSorting(this.currentDailyList, value, 0, alternative);
          break;
        case 1:
          this.filterDirection = 2;
          this.historyList = this.doSorting(this.historyList, value, 1, alternative);
          if (value == 'stateName') {
            value = 'state';
          }
          this.currentDailyList = this.doSorting(this.currentDailyList, value, 1, alternative);
          break;
        case 2:
          this.filterDirection = 0;
          this.historyList = this.sortWebfeederDate(this.historyList);
          if (value == 'stateName') {
            value = 'state';
          }
          this.currentDailyList = this.sortWebfeederDate(this.currentDailyList);
          break;
      }
    } else {
      this.currentFilter = value;
      this.filterDirection = 1;
      this.historyList = this.doSorting(this.historyList, value, 0, alternative);
      if (value == 'stateName') {
        value = 'state';
      }
      this.currentDailyList = this.doSorting(this.currentDailyList, value, 0, alternative);
    }
  }

  doSorting(list, value, direction, alternative = null) {
    if (direction == 1) {
      if (alternative == 'date') {
        return list.sort(function (obj1, obj2) {
          let datearray = obj1.transactTime.substring(0, 10).split('/');
          let newdate = datearray[1] + '/' + datearray[0] + '/' + datearray[2];
          const dateobj1 = new Date(newdate + obj1.transactTime.substring(10, 19));
          datearray = obj2.transactTime.substring(0, 10).split('/');
          newdate = datearray[1] + '/' + datearray[0] + '/' + datearray[2];
          const dateobj2 = new Date(newdate + obj2.transactTime.substring(10, 19));
          return dateobj2.getTime() - dateobj1.getTime();
        });
      }

      return (list.sort(function (a, b) {
        if (isNaN(parseFloat(a[value])) || isNaN(parseFloat(b[value]))) {
          if (a[value] > b[value]) { return -1; }
          if (a[value] < b[value]) { return 1; }
        }
        if (parseFloat(a[value].replace('.', '')) > parseFloat(b[value].replace('.', ''))) { return -1; }
        if (parseFloat(a[value].replace('.', '')) < parseFloat(b[value].replace('.', ''))) { return 1; }
      }));
    } else {

      if (alternative == 'date') {
        return list.sort(function (obj1, obj2) {
          let datearray = obj1.transactTime.substring(0, 10).split('/');
          let newdate = datearray[1] + '/' + datearray[0] + '/' + datearray[2];
          const dateobj1 = new Date(newdate + obj1.transactTime.substring(10, 19));
          datearray = obj2.transactTime.substring(0, 10).split('/');
          newdate = datearray[1] + '/' + datearray[0] + '/' + datearray[2];
          const dateobj2 = new Date(newdate + obj2.transactTime.substring(10, 19));
          return dateobj1.getTime() - dateobj2.getTime();
        });
      }

      return (list.sort(function (a, b) {
        if (isNaN(parseFloat(a[value])) || isNaN(parseFloat(b[value]))) {
          if (a[value] < b[value]) { return -1; }
          if (a[value] > b[value]) { return 1; }
        }
        if (parseFloat(a[value].replace('.', '')) < parseFloat(b[value].replace('.', ''))) { return -1; }
        if (parseFloat(a[value].replace('.', '')) > parseFloat(b[value].replace('.', ''))) { return 1; }
      }));
    }
  }

  public checkStatus(value) {
    return true;
  }

  selectAsset(value) {
    this.model.symbol = value.symbol;
  }

  checkForBlank(value) {
    this.model.symbol = '';
  }

  updateRange(range: Range) {
    this.range = range;
  }

  setupPresets() {
    const backDate = numOfDays => {
      const today = new Date();
      return new Date(today.setDate(today.getDate() - numOfDays));
    };
    const today = new Date();
    const yesterday = backDate(1);
    const minus7 = backDate(7);
    const minus30 = backDate(30);
    const currMonthStart = new Date(today.getFullYear(), today.getMonth(), 1);
    const currMonthEnd = new Date(today.getFullYear(), today.getMonth() + 1, 0);
    const lastMonthStart = new Date(
      today.getFullYear(),
      today.getMonth() - 1,
      1
    );
    const lastMonthEnd = new Date(today.getFullYear(), today.getMonth(), 0);
    this.presets = [
      {
        presetLabel: 'Yesterday',
        range: { fromDate: yesterday, toDate: today }
      },
      {
        presetLabel: 'Last 7 Days',
        range: { fromDate: minus7, toDate: today }
      },
      {
        presetLabel: 'Last 30 Days',
        range: { fromDate: minus30, toDate: today }
      },
      {
        presetLabel: 'This Month',
        range: { fromDate: currMonthStart, toDate: currMonthEnd }
      },
      {
        presetLabel: 'Last Month',
        range: { fromDate: lastMonthStart, toDate: lastMonthEnd }
      }
    ];
  }

  public cancelOrder(data, event) {
    event.stopPropagation();
    let isLoginClientWSO2 = false;
    if (this._configService.isLoginTypeWSO2() && !this._configService.isUserAssessorLogged()) {
      isLoginClientWSO2 = true;
    }
    const modalOrders = this.dialog.open(NormalModalComponent, {
      data: {
        title: '',
        body: Constants.ORDER_HISTORY_MSG002,
        isHistoryOrderWSO2: isLoginClientWSO2
      },
      panelClass: 'mat-modal-normal',
    });
    modalOrders.afterClosed().subscribe(result => {
      if (result) {
        const cancelOrder: CancelOrder = this.transformCancelOrder(data);
        data.side =
          data.side === 'Compra'
            ? Constants.ORDER_SIDE_BUY
            : Constants.ORDER_SIDE_SELL;
        data.market = Constants.ORDER_HISTORY_MARKETS.find(x => x.desc === data.mercado).cod;

        if (this._configService.isLoginTypeWSO2()) {
          // WSO2
          this._authServer.iNegotiation.cancelOrderWso2(this.account, cancelOrder.username, cancelOrder.orderid,
            cancelOrder.clOrdID, cancelOrder.market, cancelOrder.quote, cancelOrder.qtd, cancelOrder.side, this.account)
            .subscribe(res => {
              if (res.code === 1) {
                this._notificationService.success('', 'Ordem Cancelada com sucesso!');
                this.onSearch();
              } else {
                this._notificationService.error('OPS', 'Algum erro ocorreu!');
              }
            },
              error => {
                this.errors = error;
              });
        } else {
          // API
          this._authServer.iNegotiation.cancelOrderApi(this.account, cancelOrder.username, cancelOrder.orderid,
            cancelOrder.clOrdID, cancelOrder.market, cancelOrder.quote, cancelOrder.qtd, cancelOrder.side, this.account)
            .subscribe(res => {
              if (res.code === 1) {
                this._notificationService.success('', 'Ordem Cancelada com sucesso!');
                this.onSearch();
              } else {
                this._notificationService.error('OPS', 'Algum erro ocorreu!');
              }
            },
              error => {
                this.errors = error;
              });
        }

      }
    });

  }

  public transformCancelOrder(data: any): any {
    try {
      const messageReturn: CancelOrder = new CancelOrder;
      messageReturn.orderid = data.orderID;
      messageReturn.origclordid = data.origclordid;
      messageReturn.clOrdID = data.clOrdID;
      messageReturn.market = Constants.ORDER_HISTORY_MARKETS.find(x => x.desc === data.mercado).cod.toString();
      messageReturn.quote = data.quote;
      messageReturn.qtd = data.quantitySupplied;
      messageReturn.side = data.side;
      if (data.side.toString() == 'Compra') {
        messageReturn.side = 'BUY';
      } else {
        messageReturn.side = 'SELL';
      }
      messageReturn.username = data.username;
      messageReturn.enteringtrader = data.enteringtrader;

      return messageReturn;
    } catch (error) {
    }
  }

  public adjustPrice(value: string) {
    return value.replace('R$', '').replace(',', '.').trim();
  }

  public confirmSend(element, event) {
    event.stopPropagation();
    let isLoginClientWSO2 = false;
    if (this._configService.isLoginTypeWSO2() && !this._configService.isUserAssessorLogged()) {
      isLoginClientWSO2 = true;
    }
    const modalOrders = this.dialog.open(NormalModalComponent, {
      data: {
        title: '',
        body: Constants.ORDER_HISTORY_MSG004,
        isHistoryOrderWSO2: isLoginClientWSO2
      },
      panelClass: 'mat-modal-normal',
    });
    modalOrders.afterClosed().subscribe(result => {
      if (result) {
        element.timeInForce = Constants.TIME_IN_FORCES.find(p => p.desc == element.timeInForce ||
          p.webfeeder == element.timeInForce).webfeeder;
        element.side = element.side = Constants.ORDER_HISTORY_DIRECTIONS.find(x => x.desc === element.side).webfeeder;
        if (element.mercado.toLowerCase() === 'bovespa') {
          element.mercado = 1;
          element.loteDefault = 100;
        } else {
          element.mercado = 2;
          element.loteDefault = 1;
        }
        if (element.type.toLowerCase() === 'limitada') {
          element.type = 'Limited';
        } else if (element.type.toLowerCase() === 'Mercado') {
          element.type = 'Market';
        } else {
          this._notificationService.warn('OPS', 'Mercado não encontrado!');
        }
        const transactTimeTemp = new Date(element.transactTime.split('/')[1] + '/' + element.transactTime.split('/')[0] + '/' +
          element.transactTime.split('/')[2].split(' ')[0]);
        if (element.quote === null) {
          this._notificationService.warn('OPS', Constants.WORKSPACE_MSG005);
        } else if (element.typeCode != 4) {
          if (this._configService.isLoginTypeWSO2()) {
            this._authServer.iNegotiation.sendOrderWso2(element.account, element.quote, null, element.quantitySupplied,
              element.price.replace(',', '.'), element.mercado, element.type, element.side, transactTimeTemp,
              element.orderStrategy, element.timeInForce, null, null, element.loteDefault,
              element.maxfloor > 0 ? `${element.maxfloor}` : null, element.minqtd > 0 ? `${element.minqtd}` : null, element.orderTag,
              null, null, null, null, false, false)
              .subscribe(res => {
                if (res.code === 1) {
                  this._notificationService.alert('', 'Ordem enviada com sucesso!');
                  this.onSearch();
                } else {
                  this._notificationService.error('OPS', 'Algum erro ocorreu!');
                }
              }, error => {
                this.errors = error;
              });
          } else {
            this._authServer.iNegotiation.sendOrderApi(element.account, element.quote, element.quantitySupplied,
              this.adjustPrice(element.price), element.market, element.type, element.side,
              element.validityOrder, element.orderStrategy, element.timeInForce,
              null, null, element.loteDefault)
              .subscribe(res => {
                if (res.code === 1) {
                  this._notificationService.alert('', 'Ordem enviada com sucesso!');
                  this.onSearch();
                } else {
                  this._notificationService.error('OPS', 'Algum erro ocorreu!');
                }
              }, error => {
                this.errors = error;
              });
          }
        } else {
          let GainTrigger: string;
          let GainExecution: string;
          let LossTrigger: string;
          let LossExecution: string;
          if (element.priceStopGain && element.price2 && element.priceStop && element.price) {
            if (element.side === SideEnum.BUY) {
              LossTrigger = element.priceStop.toString();
              LossExecution = element.price.toString();
              GainTrigger = element.priceStopGain.toString();
              GainExecution = element.price2.toString();
            } else if (element.side === SideEnum.SELL) {
              GainTrigger = element.priceStopGain.toString();
              GainExecution = element.price2.toString();
              LossTrigger = element.priceStop.toString();
              LossExecution = element.price.toString();
            }
          } else if (element.priceStopGain && element.price2) {
            // Only for up
            if (element.side === SideEnum.BUY) {
              LossTrigger = null;
              LossExecution = null;
              GainTrigger = element.priceStopGain.toString();
              GainExecution = element.price2.toString();
            } else if (element.side === SideEnum.SELL) {
              GainTrigger = element.targettrigger.toString();
              GainExecution = element.targetlimit.toString();
              LossTrigger = '0';
              LossExecution = '0';
            }
          } else {
            // Only for down
            if (element.side === SideEnum.BUY) {
              LossTrigger = element.priceStop.toString();
              LossExecution = element.price.toString();
              GainTrigger = '0';
              GainExecution = '0';
            } else if (element.side === SideEnum.SELL) {
              GainTrigger = null;
              GainExecution = null;
              LossTrigger = element.priceStop.toString();
              LossExecution = element.price.toString();
            }
          }
          if (this._configService.isLoginTypeWSO2()) {
            this._authServer.iNegotiation.sendOrderWso2(element.account, element.quote, null, element.quantitySupplied,
              null, element.mercado, element.type, element.side, transactTimeTemp,
              element.orderStrategy, element.timeInForce, null, null, element.loteDefault,
              element.maxfloor > 0 ? `${element.maxfloor}` : null, element.minqtd > 0 ? `${element.minqtd}` : null, element.orderTag,
              GainTrigger, GainExecution, LossTrigger, LossExecution, true, true)
              .subscribe(res => {
                if (res.code === 1) {
                  this._notificationService.alert('', 'Ordem enviada com sucesso!');
                } else {
                  this._notificationService.error('OPS', 'Algum erro ocorreu!');
                }
              }, error => {
                this.errors = error;
              });
          } else {
            this._authServer.iNegotiation.sendOrderStopApi(element.account, element.validityOrder, element.orderStrategy,
              element.quote, element.quantitySupplied, this.adjustPrice(element.priceStopGain), this.adjustPrice(element.price2),
              this.adjustPrice(element.priceStop), this.adjustPrice(element.price), element.market,
              element.side, element.timeInForce, null, null, element.loteDefault)
              .subscribe(res => {
                if (res.code === 1) {
                  this._notificationService.alert('', 'Ordem enviada com sucesso!');
                } else {
                  this._notificationService.error('OPS', 'Algum erro ocorreu!');
                }
              }, error => {
                this.errors = error;
              });
          }
        }
      }
    });
  }

}
