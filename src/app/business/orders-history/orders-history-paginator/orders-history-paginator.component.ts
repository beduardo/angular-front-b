import { Component, DoCheck, EventEmitter, Input, OnChanges, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-paginator',
  templateUrl: './orders-history-paginator.component.html',
  styleUrls: ['./orders-history-paginator.component.scss']
})
export class OrdersHistoryPaginatorComponent implements DoCheck, OnChanges, OnInit {

  constructor() { }

  @Input() list: any;
  @Input() list2: any;
  @Input() alternativeKeys: any;
  @Input() numberOfLines: number;
  @Input() useAntProx = true;
  @Output() result = new EventEmitter<any>();
  @Output() result2 = new EventEmitter<any>();

  public data: any;
  public keys: any;
  public useAltKeys = false;
  public value = 0;
  public currentPage = 0;

  ngDoCheck() {
    setTimeout(x => {
      this.goToPage(this.currentPage);
    }, 0);
  }

  ngOnChanges() {
    setTimeout(x => {
      this.goToPage(0);
    }, 0);
  }

  ngOnInit() {
    setTimeout(x => {
      this.data = this.list;
      if (!this.numberOfLines || this.numberOfLines < 0) {
        this.numberOfLines = 20;
      }

      if (this.alternativeKeys) {
        this.useAltKeys = true;
      }
      this.goToPage(0);
    }, 0);
  }

  public numberOfPages() {
    if (!this.list) {
      return 0;
    }
    if (this.list.length <= 0 || this.numberOfLines <= 0) {
      return 0;
    }
    let numberOfPages = 1;
    let auxListLength = this.list.length;
    while (auxListLength / this.numberOfLines > 1) {
      auxListLength -= this.numberOfLines;
      numberOfPages++;
      if (auxListLength == 0) {
        break;
      }
    }
    return numberOfPages;
  }

  public goToPage(page) {
    if (!this.list) {
      return [];
    }
    if (page > this.numberOfPages()) {
      Error('FastTrade: Page does not exist');
    }
    this.currentPage = page;
    const auxList = [];
    const auxList2 = [];
    const rangeL = this.numberOfLines * page;
    const rangeU = this.numberOfLines * (page + 1);

    if (this.list) {
      for (let i = 0; i < this.list.length; i++) {
        if (i >= rangeL && i < rangeU) {
          auxList.push(this.list[i]);
        }
      }
    }
    if (this.list2) {
      for (let i = 0; i < this.list2.length; i++) {
        if (i >= rangeL && i < rangeU) {
          auxList2.push(this.list2[i]);
        }
      }
    }


    this.result.emit(auxList);
    this.result2.emit(auxList2);
    return auxList;
  }

  generatePagination() {
    const numberOfPages = this.numberOfPages() - 1;
    if (numberOfPages == -1) {
      return [];
    }

    if (numberOfPages == 0) {
      return [];
    }
    if (numberOfPages == 0) {
      return [0];
    }
    if (numberOfPages == 1) {
      return [0, 1];
    }
    if (numberOfPages == 2) {
      return [0, 1, 2];
    }
    if (numberOfPages == 3) {
      return [0, 1, 2, 3];
    }
    if (numberOfPages == 4) {
      return [0, 1, 2, 3, 4];
    }
    if (numberOfPages >= 5 && this.currentPage > numberOfPages - 3) {
      return [0, numberOfPages - 3, numberOfPages - 2, numberOfPages - 1, numberOfPages];
    } else if (numberOfPages >= 5 && this.currentPage < 3) {
      return [0, 1, 2, 3, numberOfPages];
    } else {
      return [0, this.currentPage - 1, this.currentPage, this.currentPage + 1, numberOfPages];
    }
  }

  chooseNext() {
    if (this.numberOfPages() > (this.currentPage + 1)) {
      this.goToPage(this.currentPage + 1);
    }

  }
  choosePrevious() {
    if (this.currentPage > 0) {
      this.goToPage(this.currentPage - 1);
    }
  }

}
