import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OrdersHistoryPaginatorComponent } from '@business/orders-history/orders-history-paginator/orders-history-paginator.component';

describe('OrdersHistoryPaginatorComponent', () => {
  let component: OrdersHistoryPaginatorComponent;
  let fixture: ComponentFixture<OrdersHistoryPaginatorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OrdersHistoryPaginatorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrdersHistoryPaginatorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
