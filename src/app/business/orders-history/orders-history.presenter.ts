import { MessageDailyOrder } from '@model/webFeeder/daily-message.model';
import { HistoryOrder } from '@model/webFeeder/history-order.model';
import { AppUtils } from '@utils/app.utils';
import { Constants } from '@utils/constants';
import { element } from 'protractor';

export class OrdersHistoryPresenter {
  private listAccounts: any[];
  public accountsObservable;

  constructor() { }
  // cmments
  // COD WEB FEEDER             BTG	    COR
  //  0	 Nova	                  Aberta	Branco
  //  1	 Parcialmente Executada	Parcialmente Executada	Verde/Branca
  //  5	 Editada	              Editada	Branca
  //  6	 Pendente Cancelar	    Cancelamento Pendente	Amerela
  //  9	 Suspensa	              Suspensa	Amerela
  //  A	 Pendente Nova	        Pendente 	Branco
  //  E	 Pendente Editar	      Pendente Editar	Amerela
  //  R	 Recebida	              Enviando	Branco
  // EXECUTADAS
  // 2	Completamente Executada	Executada	Verde
  // CANCELADAS
  // 4	Cancelada	              Cancelada	Cinza
  // 8	Rejeitada	              Rejeitada	Vermelho
  // C	Expirada	              Expirada	Cinza

  canCancelEdit(stateCode: string): boolean {
    if (
      stateCode === '0' || stateCode === '1' ||
      stateCode === '5' || stateCode === '6' ||
      stateCode === '9' || stateCode === 'A' ||
      stateCode === 'E' || stateCode === 'R'
    ) {
      return true;
    }

    return false;
  }

  openOrders(stateCode: string): boolean {
    if (
      stateCode === '0' || stateCode === '1' ||
      stateCode === '5' || stateCode === '6' ||
      stateCode === '9' || stateCode === 'A' ||
      stateCode === 'E' || stateCode === 'R'
      // TODO state === "Nova" ||
      // TODO state === "Parcialmente executada" ||
      // TODO state === "Editada" ||
      // TODO state === "Cancelamento pendente" ||
      // TODO state === "Pendente" ||
      // TODO state === "Edição pendente" ||
      // TODO state === "Recebida" ||
      // TODO state === "Pendente Nova"
    ) {
      return true;
    }

    return false;
  }

  openExecutedOrders(stateCode: string): boolean {
    if (
      stateCode === '0' || stateCode === '1' ||
      stateCode === '5' || stateCode === '6' ||
      stateCode === '9' || stateCode === 'A' ||
      stateCode === 'E' || stateCode === 'R' ||
      stateCode === '2'
    ) {
      return true;
    }

    return false;
  }

  executedOrders(stateCode: string): boolean {
    if (stateCode === '2') {
      return true;
    }

    return false;
  }

  cancelledRejectedOrders(stateCode: string): boolean {
    if (
      stateCode === '4' ||
      stateCode === '8' ||
      stateCode === 'C'
    ) {
      return true;
    }

    return false;
  }

  formatDateTimeDailyOrder(d: string) {
    const date: string = d + '';
    const day: string = date.substring(0, 2);
    const month: string = date.substring(3, 5);
    const year: string = date.substring(6, 10);
    const hour: string = date.substring(10, 13);
    const minutes: string = date.substring(14, 16);
    const dateTime: string = day + '/' + month + ' - ' + hour + 'h' + minutes;
    return dateTime;
  }

  getDiffDaysHistoryOrder(dateStart: any, dateEnd: any): Number {
    const date1 = new Date(
      dateStart.year + '/' + dateStart.month + '/' + dateStart.day
    );
    const date2 = new Date(
      dateEnd.year + '/' + dateEnd.month + '/' + dateEnd.day
    );
    return Math.ceil((date2.getTime() - date1.getTime()) / (1000 * 3600 * 24));
  }

  getTypeName(typeCode): string {
    if (typeCode === 'A' || typeCode === '1' || typeCode === '2' || typeCode === 'K') {
      return 'Normal';
    }
    return 'Disparo';
  }

  getTypeNameByCode(typeCode, name = null): string {
    switch (typeCode) {
      case '1':
        return 'Mercado com proteção';
      case '2':
        return 'Limitada';
      case '4':
        return 'Stop';
      case 'K':
        return 'Mercado Limite';
      default:
        if (name) {
          return `Check ${typeCode} maybe is ${name}`;
        } else {
          return `Checar ${typeCode}`;
        }
    }
  }

  getTimeInForce(timeInForceCode, timeInForceValue, transactTimeValue) {
    try {
      if (timeInForceCode === undefined) {
        timeInForceCode = timeInForceValue;
      }
      switch (timeInForceCode) {
        case '0':
          return 'Hoje';
        case '1':
          return 'VAC';
        case '2':
          return 'Na Abertura';
        case '3':
          return 'Executa/Cancela';
        case '4':
          return 'Tudo ou Nada';
        case '6':
          return transactTimeValue.split(' ')[0];
        case '7':
          return 'No Fechamento';
        case 'A':
          return 'Leilão';
      }
      if (timeInForceValue) {
        return timeInForceValue;
      }
      return '-';
    } catch (error) {
      return '-';
    }
  }

  transformHistoryOrder(data: any, fromHistory = true): any {
    try {
      const messageReturn: HistoryOrder[] = [];

      if (data.listBeans !== undefined) {
        data.listBeans.forEach(element => {
          const today = new Date().setHours(0, 0, 0, 0);
          const date = new Date(
            Number(element.transactTime.value.split(' ')[0].split('/')[2]),
            Number(element.transactTime.value.split(' ')[0].split('/')[1]) - 1,
            Number(element.transactTime.value.split(' ')[0].split('/')[0]),
            Number(element.transactTime.value.split(' ')[1].split(':')[0]),
            Number(element.transactTime.value.split(' ')[1].split(':')[1]),
            Number(element.transactTime.value.split(' ')[1].split(':')[2])
          );
          const dateHelper = new Date(date);

          let revertDecimal = false;
          if (element.quote.value.toLowerCase().indexOf('win') === 0 || element.quote.value.toLowerCase().indexOf('wdo') === 0 ||
            element.quote.value.toLowerCase().indexOf('dol') === 0 || element.quote.value.toLowerCase().indexOf('ind') === 0) {
            revertDecimal = false;
          } else {
            revertDecimal = true;
          }

          messageReturn.push({
            quote: element.quote.value,
            buyOrSell: element.buyOrSell.value,
            account: element.account.value,
            state: element.state.value,
            stateCode: element.state.code,
            stateClass: this.getStatusClass(element.state.code),
            stateName: this.getStatusName(
              element.state.value,
              element.state.code
            ),
            quantitySupplied: element.quantitySupplied.value.replace('.', '').replace(',', '.'),
            quantityExecuted: element.quantityExecuted.value.replace('.', '').replace(',', '.'),
            quantityRemaining: element.quantityRemaining.value.replace('.', '').replace(',', '.'),
            price: AppUtils.formatPriceHistoryOrder(element.price.value, element.quote.value, true),
            pricePercentage: element.pricePercentage ? element.pricePercentage.value : '',
            priceAverage: AppUtils.formatPriceHistoryOrder(element.priceAverage.value, element.quote.value, revertDecimal),
            type: this.getTypeNameByCode(element.type.code, element.type.value),
            typeCode: element.type.code,
            typeName: this.getTypeName(element.type.code),
            maturity: element.maturity.value,
            market: element.mercado.code,
            marketName:
              element.mercado.code === 'XBSP'
                ? Constants.MARKET_NAME_BOVESPA
                : Constants.MARKET_NAME_BMF,
            canCancelEdit: this.canCancelEdit(element.state.code),
            timeInForce: element.timeInForce.value,
            timeInForceCode: element.timeInForce.code,
            timeInForceName: this.getTimeInForce(
              element.timeInForce.code,
              element.timeInForce.value,
              element.maturity.value
            ),
            transactTime: element.transactTime.value.substring(0, 19),
            side: element.side ? element.side.value : '',
            sideCode: element.side ? element.side.code : '',
            currentCumQty: element.currentCumQty ? element.currentCumQty.value : '',
            currentAvgPx: element.currentAvgPx ? element.currentAvgPx.value : '',
            orderID: element.orderID ? element.orderID.value : '',
            clOrdID: element.clOrdID ? element.clOrdID.value : '',
            soldQty: element.soldQty ? element.soldQty.value : '',
            boughtQty: element.boughtQty ? element.boughtQty.value : '',
            buyAvgPx: element.buyAvgPx ? element.buyAvgPx.value : '',
            sellAvgPx: element.sellAvgPx ? element.sellAvgPx.value : '',
            factor: element.factor ? element.factor.value : '',
            username: element.username ? element.username.value : '',
            maxFloor: element.maxFloor ? element.maxFloor.value : '',
            minQty: element.minQty ? element.minQty.value : '',
            orderTag: element.orderTag ? element.orderTag.value : '',
            text: element.text ? element.text.value : '',
            sourceAddress: element.sourceAddress ? element.sourceAddress.value : '',
            orderStrategy: element.orderStrategy ? element.orderStrategy.value : '',
            movingStart: element.movingStart ? element.movingStart.value : '',
            initialChange: element.initialChange ? element.initialChange.value : '',
            initialChangeType: element.initialChangeType ? element.initialChangeType.value : '',
            priceStopGain: AppUtils.formatPriceHistoryOrder(element.priceStopGain ? element.priceStopGain.value : '', element.quote.value, revertDecimal),
            priceStop: AppUtils.formatPriceHistoryOrder(element.priceStop ? element.priceStop.value : '', element.quote.value, revertDecimal),
            price1: element.price.value,
            price2: AppUtils.formatPriceHistoryOrder(element.price2 ? element.price2.value : '', element.quote.value, revertDecimal),
            showDate: dateHelper.setHours(0, 0, 0, 0) === today ? element.transactTime.value.substring(11, 19) : AppUtils.getDateFormat(date),
            applicationDate: element.applicationDate ? element.applicationDate.value : '',
            mercado: element.mercado.value
          });
        });
      }
      return messageReturn;
    } catch (error) {
    }
  }


  transformDailyOrder(data: any, fromHistory = true): any {
    try {
      const messageReturn: MessageDailyOrder[] = [];
      data.forEach(element => {
        // const today = new Date().setHours(0, 0, 0, 0);
        // const date = new Date(
        //   Number(element.transactTime.value.split(' ')[0].split('/')[2]),
        //   Number(element.transactTime.value.split(' ')[0].split('/')[1]) - 1,
        //   Number(element.transactTime.value.split(' ')[0].split('/')[0]),
        //   Number(element.transactTime.value.split(' ')[1].split(':')[0]),
        //   Number(element.transactTime.value.split(' ')[1].split(':')[1]),
        //   Number(element.transactTime.value.split(' ')[1].split(':')[2])
        // );
        // const dateHelper = new Date(date);

        let revertDecimal = false;
        if (element.quote.toLowerCase().indexOf('win') === 0 || element.quote.toLowerCase().indexOf('wdo') === 0 ||
          element.quote.toLowerCase().indexOf('dol') === 0 || element.quote.toLowerCase().indexOf('ind') === 0) {
          revertDecimal = false;
        } else {
          revertDecimal = true;
        }

        messageReturn.push({
          quote: element.quote,
          buyOrSell: element.buyOrSell,
          account: element.account,
          state: this.getStatusName(element.state, element.stateCode),
          stateCode: element.stateCode,
          quantitySupplied: element.quantitySupplied.replace('.', '').replace(',', '.'),
          quantityExecuted: element.quantityExecuted.replace('.', '').replace(',', '.'),
          quantityRemaining: element.quantityRemaining.replace('.', '').replace(',', '.'),
          price: AppUtils.formatPriceHistoryOrder(element.price, element.mercado, true),
          priceAverage: AppUtils.formatPriceHistoryOrder(element.priceAverage, element.mercado, revertDecimal),
          typeCode: element.typeCode,
          type: this.getTypeNameByCode(element.typeCode),
          // type: element.type, TODO não deletar, validação
          maturity: element.maturity,
          mercado: element.mercado === 'XBSP'
            ? Constants.MARKET_NAME_BOVESPA
            : Constants.MARKET_NAME_BMF,
          transactTime: element.transactTime.substring(0, 19),
          side: element.side,
          currentCumQty: element.currentCumQty,
          currentAvgPx: element.currentAvgPx,
          orderID: element.orderID,
          orderStrategy: element.orderStrategy,
          timeInForceCode: element.timeInForceCode,
          timeInForce: element.timeInForce,
          clOrdID: element.clOrdID,
          username: element.username,
          text: element.text,
          maxFloor: element.maxFloor,
          minQty: element.minQty,
          orderTag: element.orderTag,
          movingStart: element.movingStart,
          initialChange: element.initialChange,
          priceStopGain: AppUtils.formatPriceHistoryOrder(element.priceStopGain || '', element.quote.value, revertDecimal),
          priceStop: AppUtils.formatPriceHistoryOrder(element.priceStop || '', element.quote.value, revertDecimal),
          price2: AppUtils.formatPriceHistoryOrder(element.price2 || '', element.quote.value, revertDecimal),
        });
      });
      return messageReturn;
    } catch (error) {
    }
  }


  // TODO'status-label-white': history.state.code == '0'
  // TODO || history.state.code == 'R' || history.state.code == '5' || history.state.code == 'A',
  // TODO'status-label-green-white': history.state.code == '1',
  // TODO 'status-label-green': history.state.code == '2',
  // TODO 'status-label-red': history.state.code == '8',
  // TODO 'status-label-gray': history.state.code == '4' || history.state.code == 'C',
  // TODO 'status-label-yellow': history.state.code == '6' || history.state.code == '9' || history.state.code == 'E'
  public getStatusClass(stateCode: string) {
    try {
      if (stateCode) {
        // TODO switch (stateCode) {
        // TODO  case "0":
        // TODO    return "status-label-white";
        // TODO  case "R":
        // TODO    return "status-label-white";
        // TODO  case "5":
        // TODO    return "status-label-yellow";
        // TODO  case "A":
        // TODO    return "status-label-white";
        // TODO  case "1":
        // TODO    return "status-label-green-white";
        // TODO  case "2":
        // TODO    return "status-label-green";
        // TODO  case "8":
        // TODO    return "status-label-red";
        // TODO case "4":
        // TODO    return "status-label-gray";
        // TODO  case "C":
        // TODO    return "status-label-gray";
        // TODO  case "6":
        // TODO    return "status-label-yellow";
        // TODO  case "9":
        // TODO    return "status-label-yellow";
        // TODO  case "E":
        // TODO    return "status-label-yellow";
        // }
        switch (stateCode) {
          case '0':
            return 'status-label-gray';
          case 'R':
            return 'status-label-green';
          case '5':
            return 'status-label-gray';
          case 'A':
            return 'status-label-green';
          case '1':
            return 'status-label-green';
          case '2':
            return 'status-label-green';
          case '8':
            return 'status-label-red';
          case '4':
            return 'status-label-red';
          case 'C':
            return 'status-label-gray';
          case '6':
            return 'status-label-red';
          case '9':
            return 'status-label-gray';
          case 'E':
            return 'status-label-gray';
        }
      }
      return '';
    } catch (error) {
      return '';
    }
  }

  // TODO history.state.code === '2' ? 'Executada' :
  // TODO history.state.code === '0' ? 'Aberta' :
  // TODO history.state.code === '6' ? 'Cancelamento Pendente' :
  // TODO history.state.code === 'A' ? 'Pendente' :
  // TODO history.state.code === 'R' ? 'Enviando' :
  // TODO history.state.value
  public getStatusName(state: string, stateCode: string) {
    try {
      if (stateCode) {
        switch (stateCode) {
          case '2':
            return 'Executada';
          case '0':
            return 'Aberta';
          case '6':
            return 'Cancelamento Pendente';
          case '8':
            return 'Rejeitada';
          case 'A':
            return 'Pendente';
          case 'R':
            return 'Enviando';
          case '5':
            return 'Editada';
          default:
            if (state === 'Pendente Editar') {
              return 'Edição Pendente';
            } else {
              return state;
            }
        }
      }
      return '';
    } catch (error) {
      return '';
    }
  }
}
