import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OrdersHistoryDetailComponent } from '@business/orders-history/orders-history-detail/orders-history-detail.component';

describe('OrdersHistoryDetailComponent', () => {
  let component: OrdersHistoryDetailComponent;
  let fixture: ComponentFixture<OrdersHistoryDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OrdersHistoryDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrdersHistoryDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
