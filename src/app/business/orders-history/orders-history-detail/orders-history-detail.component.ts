import { ConfigService } from './../../../services/webFeeder/config.service';
import { Component, OnInit, Input } from '@angular/core';
import { OrdersHistoryPresenter } from '@business/orders-history/orders-history.presenter';
import { AuthServer } from '@services/auth-server.service';
import { AppUtils } from '@utils/app.utils';

@Component({
  selector: 'app-orders-history-detail',
  templateUrl: './orders-history-detail.component.html'
})
export class OrdersHistoryDetailComponent implements OnInit {

  constructor(
    private _authServer: AuthServer,
    private _configService: ConfigService,
  ) { }

  @Input() dataOrder: any;
  public data;
  private historyOrderPresenter: OrdersHistoryPresenter;
  public listHistoryOrder;
  public loading: boolean;
  public futureIndice: boolean;

  ngOnInit() {
    this.loading = true;
    this.data = this.dataOrder;
    if (this.dataOrder.maturity) {
      this.dataOrder.maturity = this.dataOrder.maturity.substring(0, 10);
    }
    if (this.dataOrder.minQty) {
      this.dataOrder.minQty = AppUtils.numberToFormattedNumber(Number(this.dataOrder.minQty), 0);
    }
    if (this.dataOrder.quote.toLowerCase().indexOf('win') === 0 || this.dataOrder.quote.toLowerCase().indexOf('wdo') === 0 ||
      this.dataOrder.quote.toLowerCase().indexOf('dol') === 0 || this.dataOrder.quote.toLowerCase().indexOf('ind') === 0) {
      this.futureIndice = true;
    } else {
      this.futureIndice = false;
    }
    this._authServer.iNegotiation.getHistoryOrder(this.dataOrder.account, null, null, null, null, null, null, this.dataOrder.orderID)
      .subscribe((historyOrder: any) => {
        if (historyOrder && historyOrder.listBeans) {
          this.listHistoryOrder = this.convertValues(historyOrder.listBeans);
        }
        this.loading = false;
      });
  }

  public convertValues(list) {
    for (let i = 0; i < list.length; i++) {
      list[i].transactTime.value = list[i].transactTime.value.substring(0, 19);
      list[i].state.value = this.getStatusName(list[i].state.value, list[i].state.code);
    }
    return list;
  }

  public getStatusName(state: string, stateCode: string) {
    try {
      if (stateCode) {
        switch (stateCode) {
          case '2':
            return 'Executada';
          case '0':
            return 'Aberta';
          case '6':
            return 'Cancelamento Pendente';
          case '8':
            return 'Rejeitada';
          case 'A':
            return 'Pendente';
          case 'R':
            return 'Enviando';
          case '5':
            return 'Editada';
          default:
            if (state === 'Pendente Editar') {
              return 'Edição Pendente';
            } else {
              return state;
            }
        }
      }
      return '';
    } catch (error) {
      return '';
    }
  }


}
